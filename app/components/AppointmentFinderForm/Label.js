import fontSetting from 'components/styles/font';
import styled from 'styled-components';

const Label = styled.label`
  height: 18px;
  width: 187.24px;
  color: #596372;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  @media (max-width: 1160px) {
    width: 120px;
  }

`;


export default Label;

