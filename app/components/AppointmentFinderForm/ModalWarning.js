import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import messages from  './messages';

const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(19,24,32,0.3);
  display: flex;
  z-index: 9999;
`;

const ModalWindow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin: auto;
  height: 200px;
  width: 400px;
  border: 1px solid #DADFEA;
  border-radius: 4px;
  background-color: #FFFFFF;
  box-shadow: 0 4px 4px 2px rgba(0,0,0,0.13);
  
  z-index: 9999;
`;

const ModalHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  width: 100%;
  height: 50px;
  color: #546E7A;
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
`;

const ModalBody = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 90px;
  width: 100%;
  color: #3B4950;
  font-size: ${fontSetting.baseSizePlus1}px;
  line-height: 19px;
  padding: 40px;
  text-align: center;
`;

const ModalFooter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60px;
  width: 100%;
  border: 1px solid #DBDFEA;
  border-radius: 0 0 4px 4px;
  background-color: #FAFAFA;
`;

const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 10px;
  height: 40px;
  width: 120px;
  border-radius: 4px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  cursor: pointer;
`;

const ButtonDelete = styled(Button)`
  background-color: #D46659;
  color: #596372;
  font-size: ${fontSetting.baseSizePlus1}px;
  color: #FFFFFF;
`;

const ButtonCancel = styled(Button)`
  border: 1px solid #B6BDCE;
  color: #596372;
`;

const DeleteModal = (props, context) => {
  const { handleContinue, handleCancel, headerText, bodyText } = props;
  return (
    <Background>
      <ModalWindow>
        <ModalHeader>{headerText}</ModalHeader>
        <ModalBody>{bodyText}</ModalBody>
        <ModalFooter>
          <ButtonCancel onClick={handleCancel}>{context.intl.formatMessage(messages.Cancel)}</ButtonCancel>
          <ButtonDelete onClick={handleContinue}>{context.intl.formatMessage(messages.Continue)}</ButtonDelete>
        </ModalFooter>
      </ModalWindow>
    </Background>
  );
};

DeleteModal.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
DeleteModal.propTypes = {
  handleContinue: React.PropTypes.func.isRequired,
  handleCancel: React.PropTypes.func.isRequired,
  headerText: React.PropTypes.string.isRequired,
  bodyText: React.PropTypes.string.isRequired,
};


export default DeleteModal;
