import fontSetting from 'components/styles/font';
import React from 'react';
import { Field } from 'redux-form';
import styled from 'styled-components';
import { CardFooter } from 'components/Card';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea, SelectWithoutStyledPlaceholder, Checkbox as CheckboxInput, Select } from 'components/Input';
import ModalWarning from './ModalWarning';
import _ from 'lodash';

import DatePicker from 'react-datepicker';
import CalendarIcon from 'react-icons/lib/fa/calendar';
import CheckIcon from 'react-icons/lib/md/check';
import CloseIcon from 'react-icons/lib/fa/close';

import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import TimePicker from 'rc-time-picker';
import AppointmentTable from 'components/AppointmentTable';

import FormGroup from './FormGroup';
//import 'react-select/dist/react-select.css';
//import './assets/style.scss';
import messages from  './messages';

import { FieldsWrap } from 'components/styles/Forms';
import { ContentWrap } from 'components/styles/Containers';
import { Row, Col } from 'components/styles/Grid';

import { APPOINTMENT_TABLE_COLUMN_NAME as columnNames } from 'constants/tableColumns';

const renderSelectField = (renderProps) => {
  const { input } = renderProps;
  return (
    <Select {...input} {...renderProps} />
  );
};
const CheckboxWrapper = styled.div`
  display: inline-block;
`;

const renderCheckboxField = (inputProps) => {
  const { input } = inputProps;

  return <CheckboxWrapper><CheckboxInput {...input} {...inputProps} /></CheckboxWrapper>;
};

const renderCustomSelectField = (renderProps) => {
  const { input } = renderProps;
  return (
    <Select
      {...renderProps}
      className="appointment-scheduler-select"
      multi
      simpleValue
      clearable={false}
      value={input.value}
      filterOptions={options => options}
      optionRenderer={(valueObject, index) => {
        const thisValue = valueObject.value;
        const currentSelected = _.uniq(input.value.split(','));
        const isSelected = currentSelected.find((x) => x === thisValue);
        console.log(valueObject);
        return (
          <div style={{ display: 'flex' }}>
            {/*<span style={{ width: '20px' }}>{isSelected && <CheckIcon />}</span>*/}
            <span>
              <span>{valueObject.label}&nbsp;</span>
              <span style={{ fontWeight: 500 }}>{valueObject.labelAdditional}</span>
              </span>
          </div>
        );
      }}
      //menuRenderer={(...args) => { console.log('MENU:',args)} }
      valueComponent={(props, ...args) => {
        //console.log(props);
        return (<div className="Select-value">
        <span className="Select-value-label" role="option" aria-selected="true" id="react-select-3--value-0">
          {props.value.label}
        </span>
          <span className="Select-value-icon" aria-hidden="true" onClick={props.onRemove.bind(null, props.value)}>×</span>
        </div>) }}
      overrideProps={{
        onChange: (newValue) => {
          const uniqNewValue = _.uniq(newValue.split(',')).join(',');
          input.onChange(uniqNewValue) ;
        },
        onBlur: () => input.onBlur(input.value),
      }}
    />
  );
};


const CustomFieldsWrap = styled(FieldsWrap)`
  display: flex;
`;

const CalendarWrapper = styled.div`
  border: 1px solid #BDBDBD;
  border-radius: 4px;
  height: 36px;
  display: flex;
  justify-content: space-between;
  padding: 8px 5px;
`;

const CalendarText = styled.div`
  opacity: 0.9;
  color: #7E7E7E;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  margin-right: 15px;
`;

const ConfirmSchedule = styled.button`
  //width: 180px;
  height: 38px;
  margin-left:15px;
  margin-right:10px;
  padding: 5px 20px;
	border-radius: 4px;
	background-color: white;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  color: #4B5360;
  align-items: center;
  display: flex;
  cursor: pointer;
  justify-content: center;
  border: 1px solid #B5BDCF;
  > svg {
    margin-right: 5px;
  }
`;
const Cancel = styled.button`
  padding: 5px 20px;
  margin-left:15px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  color: #455A64;
  display: flex;
  align-items: center;
  cursor: pointer;
  justify-content: center;
	height: 36px;
	//width: 94px;
	border: 1px solid #B5BDCF;
	border-radius: 4px;
	background: linear-gradient(0deg, #FFFFFF 0%, #FFFFFF 100%);
	> svg {
    margin-right: 5px;
  }
`;

const FilterButton = styled(GreenButton)`
	height: 40px;
	width: 100%;
	border-radius: 4px;
	background-color: #A0C2B4;
`;

const ControlAppointments = styled.div`
	width: 176px;
	//margin-right: auto;
	background-color:white; 
	color: #4B5360;
	.Select-placeholder,.Select--single > .Select-control .Select-value {
    padding-right: 20px;
    padding-left: 20px;
  }
`;

const ControlBlock = styled.div`
  padding: 10px 30px 10px 15px;
  background-color: #D7DAE1;
  display: flex;
  align-items: center;
`;

const FormHeader = styled.div`
	color: #49846D;
	font-size: ${fontSetting.baseSizePlus3}px;
	font-weight: 600;
	line-height: 22px;
	padding: 0px 0px 20px 10px;
`;

const Label = styled.label`
  flex-grow: 1;
  text-align: right;
  font-weight: 600;
  color: #828282;
`;

const LabelHeader = styled.label`
  color: #5F5F5F;
  text-align: ${props => props.right ? 'right': 'left'};
  padding: ${props => props.padding ? props.padding: '0px'};
`;

const CustomIssueInput = (props) => (
  <CalendarWrapper
    onClick={props.onClick}
    style={{
      cursor: 'pointer',
      border: '1px solid #BDBDBD',
      borderRadius: '4px',
      height: '38px',
      backgroundColor: '#F8FAFC',
      display: 'flex',
      justifyContent: 'space-between',
      padding: '8px 5px',
    }}
  >
    <CalendarText style={{ 'color': props.value ? '#455A64' : '#8D8D8D' }}>
      {props.value ? props.value : 'DD.MM.YYYY' }
    </CalendarText>
    <CalendarIcon color="#6C7B91" />
  </CalendarWrapper>
);

const renderDatePicker = (props) => (
  <DatePicker
    selected={props.input.value ? moment(props.input.value) : null}
    onChange={props.input.onChange}
    placeholderText="DD.MM.YYYY"
    customInput={<CustomIssueInput />}
    disabled={props.disabled}
    dateFormat="DD.MM.YYYY"
  />
);


class AppointmentFinderForm extends React.Component {
  constructor(props) {
    super(props);

    this.onInnerTabClick = this.onInnerTabClick.bind(this);
    this.fromTimeChange = this.fromTimeChange.bind(this);
    this.toTimeChange = this.toTimeChange.bind(this);
    this.findPrescriptionById = this.findPrescriptionById.bind(this);
    this.onPrescriptionNumberChange = this.onPrescriptionNumberChange.bind(this);
    this.getScheduledAmount = this.getScheduledAmount.bind(this);
    this.triggerOnPrescriptionNumberChange = this.triggerOnPrescriptionNumberChange.bind(this);
    this.isAnyCheckboxChecked = this.isAnyCheckboxChecked.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.onFindAlternative = this.onFindAlternative.bind(this);
    this.toggleAppointmentCancel = this.toggleAppointmentCancel.bind(this);
    this.toggleAllAppointmentsCancel = this.toggleAllAppointmentsCancel.bind(this);
    this.cancelAppointments = this.cancelAppointments.bind(this);
    this.getMessageForAvailableOptions = this.getMessageForAvailableOptions.bind(this);

    this.state = {
      selectedInnerTab: 1,
      updateOnce: false,
      currentPrescription: {},
      appointmentsSuggestion: [],
      showModal: false,
      changedPrescriptionId: '',
      scheduledAppointments: [],
      toggleAllAppointmentsCancel: false,

      selectValue: ""
    };
  }

  componentDidUpdate() {
    let pres = this.findPrescriptionById(this.props.prescriptionId);
    const {
      providedServices,
    } = this.props;
    let filteredProvidedService = pres && providedServices && providedServices.split(",")
      && providedServices.split(",").length !== 0 && pres.providedServices.filter(provService => {
        return provService ? providedServices.split(",").some(id => id === provService._id)
          : false;
      });
    let scheduledAppointments = pres && pres.providedServices && filteredProvidedService ?
      this.getScheduledAppointments(filteredProvidedService, pres) : [];

    //TODO: What will happen if the length doesn't change but the appointments will be different?
    if (this.state.scheduledAppointments.length !== scheduledAppointments.length) {
      scheduledAppointments = scheduledAppointments.map((item) => ({ ...item, date: moment(item.date) }))
        .sort((a, b) => {
          return a.date - b.date;
        });
      this.setState({ scheduledAppointments: scheduledAppointments });
    }
  }

  componentWillUnmount() {
    this.props.clearAppointmentsSuggestion();
  }

  onInnerTabClick(tabNumber) {
    this.setState({ selectedInnerTab: tabNumber });
  }

  fromTimeChange(time) {
    const dt = new Date(time.format());
    this.props.change('preferredTimeFrom', dt.getMinutes() + (60 * dt.getHours()));
  }

  toTimeChange(time) {
    const dt = new Date(time.format());
    this.props.change('preferredTimeTo', dt.getMinutes() + (60 * dt.getHours()));
  }

  onPrescriptionNumberChange(unscheduledCount, e, value) {
    if (unscheduledCount > 0) {
      e.preventDefault();
      this.setState({
        changedPrescriptionId: value,
        showModal: true,
      });
    } else {
      let pres = this.findPrescriptionById(value);
      let options = [];

      pres.providedServices.forEach((providedService) => {
        if (providedService.service) {
          options.push(providedService._id);
        }
      });

      this.props.change('providedServices', options.length > 0 ? options.join(','): '');
      this.props.change('issuedDate', pres.issuedDate ? pres.issuedDate : '');
      this.props.change('prescriptionId', value);
      this.props.clearAppointmentsSuggestion();
    }
  }

  onFindAlternative(e) {

    let app = this.props.appointmentsSuggestion.find(a => a.checked);
    let data = {};
    data.serviceId = app.service._id;
    data.date = app.date;
    data.preferredTimeFrom = this.props.preferredTimeFrom;
    data.preferredTimeTo = this.props.preferredTimeTo;
    data.prescriptionId = this.props.prescriptionId;
    data.count = 2;
    data.appointmentsSelected = this.props.appointmentsSuggestion;

    if (e.target.value === 'Same Day') {
      this.props.findAlternativeSameDay(data);
    }
    else if (e.target.value === 'With same Staff') {
      data.staffId = app.staff._id;
      this.props.findAlternativeSameDay(data);
    }
  }

  triggerOnPrescriptionNumberChange() {
    let pres = this.findPrescriptionById(this.state.changedPrescriptionId);
    let options = [];
    pres.providedServices.forEach((providedService) => {
      if (providedService.service) {
        options.push(providedService._id);
      }
    });
    this.props.change('providedServices', options.join(','));
    this.props.change('issuedDate', pres.issuedDate ? pres.issuedDate : '');
    this.props.change('prescriptionId', this.state.changedPrescriptionId);
    this.props.clearAppointmentsSuggestion();

    this.setState({ showModal: false });
  }

  toggleAllAppointmentsCancel() {
    let scheduledAppointments = [...this.state.scheduledAppointments];
    scheduledAppointments.forEach((item, index) => {
      scheduledAppointments[index].checked = !this.state.toggleAllAppointmentsCancel;
    });
    this.setState({
      scheduledAppointments: scheduledAppointments,
      toggleAllAppointmentsCancel: !this.state.toggleAllAppointmentsCancel,
    });
  }

  toggleAppointmentCancel(id) {
    let scheduledAppointments = [...this.state.scheduledAppointments];
    scheduledAppointments[id].checked = !scheduledAppointments[id].checked;
    let toggleAllAppointmentsCancel = scheduledAppointments.every(a => a.checked);
    this.setState({
      scheduledAppointments: scheduledAppointments,
      toggleAllAppointmentsCancel: toggleAllAppointmentsCancel,
    });
  }

  findPrescriptionById(id) {
    return this.props.prescriptions.find((p) => p._id === id);
  }

  getScheduledAmount(provServices, prescription) {
    const {
      appointments,
    } = this.props;
    let scheduled = 0;
    appointments.forEach(appointment => {
      scheduled += provServices.some((provService) => appointment.service && provService.service && appointment.prescription ?
      appointment.service._id === provService.service._id &&
      appointment.prescription._id === prescription._id &&
      appointment.status === 'SCHEDULED'
        : false);
    });

    return scheduled;
  }

  getScheduledAppointments(provServices, prescription) {
    const {
      appointments,
    } = this.props;
    return appointments.filter(appointment =>
      provServices.length > 0 && provServices.some((provService) => {
        return provService.service && appointment.prescription && appointment.service
          ? appointment.service._id === provService.service._id &&
        appointment.prescription._id === prescription._id &&
        appointment.status === 'SCHEDULED'
          : false;
      })
    );
  }

  getUnscheduledAmount(providedServices, scheduledAmount) {
    let amount = providedServices.reduce((acc, service) => acc + service.quantity, 0);
    return amount - scheduledAmount;
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  isAnyCheckboxChecked() {
    return this.props.appointmentsSuggestion.some(appointment => appointment.checked);
  }

  getMessageForAvailableOptions(message, unscheduledCount) {
    const string = unscheduledCount !== 0 ? (' ( ' + unscheduledCount + ' )') : '';
    return message + string;
  }

  cancelAppointments() {
    let scheduledAppointments = [...this.state.scheduledAppointments];
    scheduledAppointments = scheduledAppointments.filter(a => a.checked);
    scheduledAppointments.map(a => {
      a.status = 'CANCELLED';
      return a;
    });
    const ids = scheduledAppointments.map(a => a._id);
    this.props.cancelAppointments({ data: { status: 'CANCELLED' }, ids, scheduledAppointments });
    this.setState({ toggleAllAppointmentsCancel: false });
  }

  renderTabContent(){
    const TABS = {
      PROPOSED: 1,
      SCHEDULED: 2
    };
    switch(this.state.selectedInnerTab){
      case TABS.PROPOSED: {
        return this.renderProposedContent();
      }
      case TABS.SCHEDULED: {
        return this.renderScheduledContent();
      }
    }
  }

  getColumnsData(){
    return [
      {
        id: columnNames.CHECKBOX,
        width: 7,
      },
      {
        id: columnNames.TIME,
        width: 8,
      },
      {
        id: columnNames.SERVICE,
        width: 15,
      },
      {
        id: columnNames.DURATION,
        width: 20,
      },
      {
        id: columnNames.STAFF,
        width: 30,
        cell: {
          inline: true,
        }
      },
      {
        id: columnNames.ROOM,
        width: 20,
      },
    ];
  }

  renderProposedContent(){
    return [
      this.renderProposedAppointments(),
      this.renderControlBlock(),
      <AppointmentTable
        items={this.props.appointmentsSuggestion}
        patient={this.props.currentPatient}
        onToggleAppointmentChecked={(id) => { this.props.toggleAppointmentChecked(id) }}
        isEdit
        push={this.props.push}

        orderColumns={[
          columnNames.CHECKBOX,
          columnNames.TIME,
          columnNames.SERVICE,
          columnNames.DURATION,
          columnNames.STAFF,
          columnNames.ROOM,
        ]}
        dataColumns={this.getColumnsData()}
      />
    ];
  }
  renderScheduledContent(){
    const formatMessage = this.context.intl.formatMessage;
    const scheduledAppointments = this.state.scheduledAppointments;
    return [
      <FormGroup
        key="0"
        style={{
          backgroundColor: '#E5E7EE',
          padding: '8px 16px',
          marginBottom: '0px',
          alignItems: 'center',
          marginTop: '30px'
        }}>
        <div>
          <Field
            name='allCancelledChecked'
            type="checkbox"
            component={renderCheckboxField}
            checked={this.state.toggleAllAppointmentsCancel}
            onChange={this.toggleAllAppointmentsCancel}
          />
        </div>
        <Cancel type="button" onClick={this.cancelAppointments}>
          <CloseIcon />
          <span>{formatMessage(messages.CancelAppointment)}</span>
        </Cancel>
      </FormGroup>,
      <AppointmentTable
        key="1"
        items={scheduledAppointments}
        patient={this.props.currentPatient}
        onToggleAppointmentChecked={this.toggleAppointmentCancel}
        isEdit
        push={this.props.push}

        orderColumns={[
          columnNames.CHECKBOX,
          columnNames.TIME,
          columnNames.SERVICE,
          columnNames.DURATION,
          columnNames.STAFF,
          columnNames.ROOM,
        ]}
        dataColumns={this.getColumnsData()}
      />
    ];
  }
  renderProposedAppointments(){
    const formatMessage = this.context.intl.formatMessage;
    return (
      <ContentWrap padding="20px">
        <CustomFieldsWrap padding="20px 40px 10px 40px">
          <Row>
            <Col>
              <Row>
                <LabelHeader padding="0px 10px">
                  {formatMessage(messages.PreferredDate)}
                </LabelHeader>
              </Row>
              <Row center>
                <Col grow="1" padding="0px">
                  <Label>{formatMessage(messages.from)}</Label>
                </Col>
                <Col width="168px">
                  <Field
                    name="preferredDateFrom"
                    component={renderDatePicker}
                  />
                </Col>
              </Row>
              <Row center>
                <Col grow="1" padding="0px">
                  <Label>{formatMessage(messages.to)}</Label>
                </Col>
                <Col width="168px">
                  <Field
                    name="preferredDateTo"
                    component={renderDatePicker}
                    style={{ width: '168px' }}
                  />
                </Col>
              </Row>
            </Col>
            <Col>
              <Row>
                <LabelHeader padding="0px 10px">
                  {formatMessage(messages.PreferredTime)}
                </LabelHeader>
              </Row>
              <Row center>
                <Col grow="1" padding="0px">
                  <Label>{formatMessage(messages.from)}</Label>
                </Col>
                <Col width="100px">
                  <TimePicker
                    defaultOpenValue={moment()}
                    showSecond={false}
                    format="HH:mm"
                    onChange={this.fromTimeChange}
                    defaultValue={moment().hour(0).minute(0)}
                  />
                </Col>
              </Row>
              <Row center>
                <Col grow="1" padding="0px">
                  <Label>{formatMessage(messages.to)}</Label>
                </Col>
                <Col width="100px">
                  <TimePicker
                    defaultOpenValue={moment()}
                    showSecond={false}
                    format="HH:mm"
                    defaultValue={moment().hour(23).minute(0)}
                    onChange={this.toTimeChange}
                    use12Hours={false}
                    style={{ height: '38px' }}
                  />
                </Col>
              </Row>
            </Col>
            <Col grow="1">
              <Row>
                <Col width="147px">
                  <LabelHeader right>
                    {formatMessage(messages.StaffGender)}
                  </LabelHeader>
                </Col>
                <Col/>
              </Row>
              <Row center>
                <Col width="147px">
                  <Field
                    name="staffGender"
                    component={renderSelectField}
                    options={[
                      { value: '', label: 'Any'},
                      { value: 'Female', label: 'Female'},
                      { value: 'Male', label: 'Male'},
                    ]}
                    style={{ height: '38px' }}
                  />
                </Col>
                <Col grow="1">
                  <FilterButton type="submit">{formatMessage(messages.Check)}</FilterButton>
                </Col>
              </Row>
            </Col>
          </Row>
        </CustomFieldsWrap>
      </ContentWrap>)
  }
  renderControlBlock(){
    const formatMessage = this.context.intl.formatMessage;
    let isAnyCheckboxChecked = this.isAnyCheckboxChecked();
    let count = this.props.appointmentsSuggestion.reduce((acc, item) => acc + (item.checked ? 1 : 0), 0);
    return (<ControlBlock>
      <div style={{ marginRight:'22px' }}>
        <Field
          name='allChecked'
          type="checkbox"
          component={renderCheckboxField}
          checked={this.props.allAppointmentsSuggestionChecked}
          onChange={this.props.toggleAllAppointmentsChecked}
          disabled={this.props.appointmentsSuggestion.some(a => a.isRadio)
          || this.props.appointmentsSuggestion.length === 0}
        />
      </div>
      <ControlAppointments
        style={{
          opacity: isAnyCheckboxChecked ? 1 : 0.5,
          borderRadius: '4px'
        }}>
        <Field
          name="findAlternative"
          component={renderSelectField}
          placeholder={formatMessage(messages.FindAlternatives)}
          disabled={!isAnyCheckboxChecked || count !== 1}
          onClick={this.onFindAlternative}
          options={[
            { value: 'Same Day', label: formatMessage(messages.SameDay)},
            { value: 'With same Staff', label: formatMessage(messages.WithSameStaff)},
          ]}
        />
      </ControlAppointments>
      <ConfirmSchedule
        type="button"
        onClick={this.props.confirmScheduling}
        style={{ opacity: isAnyCheckboxChecked ? 1 : 0.5 }}
        disabled={!isAnyCheckboxChecked}
      >
        <CheckIcon />
        <span>{formatMessage(messages.ConfirmScheduling)}</span>
      </ConfirmSchedule>
    </ControlBlock>)
  }

  handleSelectChange (value) {
    console.log(value);
    this.setState({ selectValue: value });
  }

  renderServiceAndPrescriptionPicker(){
    const { providedServices } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const options = this.getOptions();
    const activeValuesLength = (providedServices || 0) && providedServices.split(',').length;
    return (
      <ContentWrap padding="20px">
        <FormHeader>{formatMessage(messages.AppointmentScheduler)}</FormHeader>
        <CustomFieldsWrap padding="20px">
          <Row>
            <Col width="160px">
              <div>
                <LabelHeader>{formatMessage(messages.Prescription)}</LabelHeader>
              </div>
              <div>
                <Field
                  name="prescriptionId"
                  component={renderSelectField}
                  placeholder=" "
                  onChange={(...args) => this.onPrescriptionNumberChange(this.getUnscheduledCount(), ...args)}
                  options={this.getPrescriptionToShow()}
                />
              </div>
            </Col>
            <Col grow={2}>
              <div>
                <LabelHeader>
                  {formatMessage(messages.ProvidedServices)}
                  {activeValuesLength > 0 && `: ${activeValuesLength} ${formatMessage(messages.Of)} ${options.length}`}
                </LabelHeader>
              </div>
              <div>
                <Field
                  name="providedServices"
                  component={renderCustomSelectField}
                  placeholder="  "
                  options={options}
                  value={providedServices}
                />
              </div>
            </Col>
            <Col width="160px">
              <div>
                <LabelHeader>{formatMessage(messages.DateIssued)}</LabelHeader>
              </div>
              <div>
                <Field
                  name="issuedDate"
                  component={renderDatePicker}
                  disabled
                />
              </div>
            </Col>
          </Row>
        </CustomFieldsWrap>
      </ContentWrap>
    );
  }
  getUnscheduledCount(){
    const { prescriptionId } = this.props;
    let pres = this.findPrescriptionById(prescriptionId);
    let filteredProvidedService = this.getFilteredProvidedServices();

    return pres && filteredProvidedService ?
      this.getUnscheduledAmount(filteredProvidedService, this.getScheduledCount()) : 0;
  }
  getFilteredProvidedServices(){
    const {
      prescriptionId,
      providedServices,
    } = this.props;
    let pres = this.findPrescriptionById(prescriptionId);
    return pres && providedServices && providedServices.split(",")
      && providedServices.split(",").length !== 0 && pres.providedServices.filter(provService => {
        return provService.service ? providedServices.split(",").some(id => id === provService._id)
          : false;
      });
  }
  getScheduledCount(){
    const { prescriptionId } = this.props;
    let pres = this.findPrescriptionById(prescriptionId);
    let filteredProvidedService = this.getFilteredProvidedServices();

    return pres && pres.providedServices && filteredProvidedService
      ? this.getScheduledAmount(filteredProvidedService, pres) : 0;
  }
  getPrescriptionToShow(){
    const {
      prescriptions,
      currentPatient,
    } = this.props;
    let prescriptionsToShow = [];
    prescriptions.forEach((prescription, i) => {
      if (prescription.patient === currentPatient._id) {
        prescriptionsToShow.push({
          value: prescription._id,
          label: `${currentPatient.id}-${prescription.id}`
        });
      }
    });
    prescriptionsToShow.sort((a, b)=>a.value > b.value);
    return prescriptionsToShow;
  }
  getOptions(){
    const { prescriptionId } = this.props;
    let pres = this.findPrescriptionById(prescriptionId);
    let options = [];
    pres && pres.providedServices.forEach((providedService) => {
      if (providedService && providedService.shortName) {
        options.push({
          value: providedService._id,
          label: `${providedService.quantity}x${providedService.shortName[0].toUpperCase() + providedService.shortName.substr(1)}`,
          labelAdditional: `(${providedService.frequencyMin}-${providedService.frequencyMax} per week)`
        });
      }
    });
    return options;
  }

  render() {
    const formatMessage = this.context.intl.formatMessage;
    let messageForAvailableOptions = this.getMessageForAvailableOptions(formatMessage(messages.AvailableOptions),
      this.props.appointmentsSuggestion.length);
    let messageForScheduledAppointments = this.getMessageForAvailableOptions(formatMessage(messages.ScheduledAppointments), this.getScheduledCount());
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div>
          {this.renderServiceAndPrescriptionPicker()}
          <Tabs
            theme="inner-entity"
            value={this.state.selectedInnerTab}
            onTabClick={this.onInnerTabClick}
          >
            <Tab
              value={1}
              label={messageForAvailableOptions}
            />
            <Tab
              value={2}
              label={messageForScheduledAppointments}
            />
          </Tabs>
          {this.renderTabContent()}
        </div>
        {this.state.showModal && <ModalWarning
          handleContinue={this.triggerOnPrescriptionNumberChange}
          handleCancel={this.hideModal}
          headerText={formatMessage(messages.ChangingPrescription)}
          bodyText={formatMessage(messages.WarningFinder)}
        />}
      </form>
    );
  }
}

AppointmentFinderForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

AppointmentFinderForm.propTypes = {
  onDelete: React.PropTypes.func,
  onCancel: React.PropTypes.func,
  toggleAppointmentCancel: React.PropTypes.func,
  pristine: React.PropTypes.bool.isRequired,
  change: React.PropTypes.func.isRequired,
  isEdit: React.PropTypes.bool,
  handleSubmit: React.PropTypes.func,
  currentPatient: React.PropTypes.object.isRequired,
};


export { default as validate } from './validate';
export default AppointmentFinderForm;
