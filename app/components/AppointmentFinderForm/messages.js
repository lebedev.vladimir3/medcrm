import { defineMessages } from 'react-intl';

export default defineMessages({
  NextAppointment: {
    id: 'AppointmentFinder.NextAppointment',
    defaultMessage: 'Next Appointment',
  },
  ProvidedServices: {
    id: 'AppointmentFinder.ProvidedServices',
    defaultMessage: 'Provided Services',
  },
  UnscheduledServices: {
    id: 'AppointmentFinder.UnscheduledServices ',
    defaultMessage: 'Unscheduled Services ',
  },
  AvailableOptions: {
    id: 'AvailableOptions',
    defaultMessage: 'Available Options',
  },
  ScheduledAppointments: {
    id: 'ScheduledAppointments',
    defaultMessage: 'Scheduled Appointments',
  },
  FindAlternatives: {
    id: 'AppointmentFinder.FindAlternatives ',
    defaultMessage: 'Find Alternatives ',
  },
  SameDay: {
    id: 'AppointmentFinder.SameDay ',
    defaultMessage: 'Same Day ',
  },
  WithSameStaff: {
    id: 'AppointmentFinder.WithSameStaff ',
    defaultMessage: 'With same Staff ',
  },
  ConfirmScheduling: {
    id: 'AppointmentFinder.ConfirmScheduling ',
    defaultMessage: 'Confirm & Schedule ',
  },
  ChangingPrescription: {
    id: 'AppointmentFinder.ChangingPrescription ',
    defaultMessage: 'Changing Prescription ',
  },
  WarningFinder: {
    id: 'AppointmentFinder.WarningFinder',
    defaultMessage: 'Please note that all unscheduled suggested appointments will be reset and can be lost. Continue anyway? ',
  },
  Pending: {
    id: 'Pending',
    defaultMessage: 'Pending',
  },
  Check: {
    id: 'Check',
    defaultMessage: 'Check',
  },
  Prescription: {
    id: 'Prescription',
    defaultMessage: 'Prescription',
  },
  DateIssued: {
    id: 'DateIssued',
    defaultMessage: 'Date Issued',
  },
  from: {
    id: 'from',
    defaultMessage: 'From',
  },
  PreferredDate: {
    id: 'PreferredDate',
    defaultMessage: 'Preferred Date',
  },
  PreferredTime: {
    id: 'Preferred Time',
    defaultMessage: 'Preferred Time',
  },
  StaffGender: {
    id: 'StaffGender',
    defaultMessage: 'Staff Gender',
  },
  to: {
    id: 'to',
    defaultMessage: 'to',
  },
  Search: {
    id: 'Search',
    defaultMessage: 'Search',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  CancelAppointment: {
    id: 'AppointmentFinder.CancelAppointment',
    defaultMessage: 'Cancel Appointment',
  },
  Continue: {
    id: 'Continue',
    defaultMessage: 'Continue',
  },
  Of: {
    id: 'Of',
    defaultMessage: 'of',
  },
  AppointmentScheduler: {
    id: 'AppointmentScheduler',
    defaultMessage: 'Appointment Scheduler',
  },
});
