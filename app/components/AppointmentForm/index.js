import React, { PropTypes } from 'react';
import moment from 'moment';
import { Field } from 'redux-form';
import styled from 'styled-components';
import { ContentWrap } from 'components/styles/Containers';
import { Card, CardFooter } from 'components/styles/Card';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea, Select, DatePicker } from 'components/Input';
import { Table, TR, TD } from 'components/Table';
import TimePicker from 'rc-time-picker';
import messages from './messages';

import Divider from './../common/Divider';
import { Row, RowsWrap, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';

import renderHelper from 'components/common/renderHelper';
import WarnIcon from 'assets/icons/AlertIcon/Warning.svg';
import Alert from 'components/styles/Alerts';
//import 'react-select/dist/react-select.css';
import { EMPTY_STRING }from 'constants/fillers';

import {
  EntityContent as ReportContent,
  EntityLabel as ReportLabel,
} from 'components/styles/Forms';

import theme from 'components/styles/colorScheme';
import { Tag } from 'components/styles/common';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { genderParser } from "./../../utils";

const AppointmentLabelCol = styled(LabelCol).attrs({
  width: '161px'
})``;

const renderDatePicker = (props) => (
  <DatePicker
    selected={props.input.value ? moment(props.input.value) : null}
    onChange={props.input.onChange}
    placeholderText="DD.MM.YYYY"
    dateFormat="DD.MM.YYYY"
    style={{ width: '100px' }}
    disabled={props.disabled}
  />
);


class AppointmentCardForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleTimeChange = this.handleTimeChange.bind(this);
  }

  handleTimeChange(time) {
    this.props.change(
      'date',
      moment(this.props.chosenDate).set({
        hour: time.hours(),
        minute: time.minutes(),
        second: time.seconds(),
      }),
    );
  }

  renderAppointmentCardTabContent(){
    const {
      roles,
      users,
      services,
      serviceCategories,
      equipmentList,
      rooms,
      chosenRole,
      chosenServiceCategory,
      chosenService,
      chosenDate,
      appointmentId,
      appointments,
      isAdmin,
    } = this.props;
    const { formatMessage } = this.context.intl;

    const roleOptions = roles.map((item, index) => ({
      value: item._id,
      label: item.name
    }));
    const staffOptions = users.filter((item) => item.role._id === chosenRole).map((item, index) => ({
      value: item._id,
      label: `${item.lastName}, ${item.firstName}`
    }));
    const serviceOptions = services.filter((item) => item.category === chosenServiceCategory).map((item, index) => ({
      value: item._id,
      label: item.name.shortName
    }));
    const serviceCategoryOptions = serviceCategories.map((item, index) => ({
      value: item._id,
      label: item.name
    }));
    const equipmentOptions = equipmentList.map((item, index) => ({
      value: item._id,
      label: item.name
    }));
    const roomOptions = rooms.map((item, index) => ({
      value: item._id,
      label: item.number
    }));

    const chosenServiceObject = services.find((item) => item._id === chosenService);
    const sameDayAppointments = appointments ? appointments.filter((item) => moment(item.date).isSame(chosenDate, 'day')) : [];

    return <RowsWrap>
      <Row center>
        <Col width="calc(100% - 55px)">
          <Row paddingBottom="4px">
            <AppointmentLabelCol>
              <FormLabel />
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  {formatMessage(messages.Date)}
                </Col>
                <Col width="25%" >
                  {formatMessage(messages.Time)}
                </Col>
                <Col width="25%" >
                  {formatMessage(messages.Duration)}
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <AppointmentLabelCol>
              <FormLabel>{formatMessage(messages.Schedule)}</FormLabel>
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field name="date" component={renderHelper.datePickerField} />
                </Col>
                <Col width="25%" >
                  <TimePicker
                    defaultOpenValue={moment()}
                    showSecond={false}
                    value={moment(chosenDate)}
                    format="hh:mm"
                    defaultValue={moment().hour(0).minute(0)}
                    onChange={this.handleTimeChange}
                  />
                </Col>
                <Col width="25%" >
                  <Field name="duration" component={renderHelper.inputField} isNotIndented />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </Col>
      </Row>
      <Divider dashed marginTop={0} marginBottom={15}/>
      <Row center paddingBottom="0px">
        <Col width="calc(100% - 55px)">
          <Row paddingBottom="4px">
            <AppointmentLabelCol>
              <FormLabel />
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  {formatMessage(messages.Name)}
                </Col>
                <Col width="50%" >
                  {formatMessage(messages.Category)}
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center paddingBottom="0px">
            <AppointmentLabelCol>
              <FormLabel>{formatMessage(messages.MedicalService)}</FormLabel>
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="service._id"
                    component={renderHelper.selectField}
                    placeholder={formatMessage(messages.ServiceName)}
                    options={serviceOptions}
                  />
                </Col>
                <Col width="50%">
                  <Field
                    name="service.category"
                    component={renderHelper.selectField}
                    placeholder={formatMessage(messages.ServiceCategory)}
                    options={serviceCategoryOptions}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </Col>
      </Row>
      <Row center style={{ paddingBottom: '20px' }}>
        <Col width="calc(100% - 55px)">
          <Row paddingBottom="0px">
            <AppointmentLabelCol>
              <FormLabel />
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="25%">
                  <Input value={chosenServiceObject ? chosenServiceObject.name.shortName : ''} disabled />
                </Col>
                <Col width="75%" >
                  <Input value={chosenServiceObject ? chosenServiceObject.name.defaultName : ''} disabled />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </Col>
      </Row>
      <Divider dashed marginTop={0} marginBottom={15}/>
      <Row center style={{ paddingBottom: '20px' }}>
        <Col width="calc(100% - 55px)">
          <Row paddingBottom="4px">
            <AppointmentLabelCol>
              <FormLabel />
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  {formatMessage(messages.Role)}
                </Col>
                <Col width="50%">
                  {formatMessage(messages.Name)}
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center paddingBottom="0px">
            <AppointmentLabelCol>
              <FormLabel>{formatMessage(messages.Staff)}</FormLabel>
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field name="staff.role" component={renderHelper.selectField} options={roleOptions} />
                </Col>
                <Col width="50%">
                  <Field name="staff._id" component={renderHelper.selectField} options={staffOptions} />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </Col>
      </Row>
      <Divider dashed marginTop={0} marginBottom={15}/>
      <Row center style={{ paddingBottom: '24px' }}>
        <Col width="calc(100% - 55px)">
          <Row paddingBottom="4px">
            <AppointmentLabelCol>
              <FormLabel />
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  {formatMessage(messages.Room)}
                </Col>
                <Col width="50%">
                  {formatMessage(messages.Equipment)}
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center paddingBottom="0px">
            <AppointmentLabelCol>
              <FormLabel>{formatMessage(messages.Resources)}</FormLabel>
            </AppointmentLabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field name="room" component={renderHelper.selectField} options={roomOptions} />
                </Col>
                <Col width="50%">
                  <Field
                    name="equipment"
                    component={renderHelper.selectField}
                    options={[
                      ...equipmentOptions
                    ]}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </Col>
      </Row>
      <Divider title={formatMessage(messages.Notes)} marginTop={0}/>
      <Row center>
        <Col grow="1" padding="0">
          <Field
            name="notes"
            component={renderHelper.textAreaField}
            placeholder={formatMessage(messages.NotesForInternalUsage)}
          />
        </Col>
      </Row>
    </RowsWrap>
  }
  renderTreatmentReportTabContent(){
    const { appointment: { service, staff } } = this.props;
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Alert
          type="warning"
          icon={<WarnIcon />}
          contentWrapSetting={{
            padding: "10px 15px 20px 15px"
          }}
        >
          <strong>{formatMessage(messages.Note)}:&nbsp;</strong>
          <span style={{ opacity: 0.95 }}>
                  {formatMessage(messages.WarningMessage)}
                </span>
        </Alert>
        <ReportContent>
          <Row justifyContent="space-between">
            <span>
              <ReportLabel>{formatMessage(messages.Service)}</ReportLabel>
              <strong>{service ? [
                <span key="0" style={{ marginRight: '6px' }}>{service.name.longName}</span>,
                <span key="1">{"( " + service.name.shortName + " )"}</span>
              ]: EMPTY_STRING}
              </strong>
            </span>
            <span />
          </Row>
          <Field
            name="treatmentReport"
            component={renderHelper.textAreaField}
            //placeholder={formatMessage(messages.AssignedStaff)}
            style={{
              minHeight: '308px'
            }}
          />
          <Row justifyContent="space-between" style={{ paddingBottom: 0 }}>
            <span>
              <ReportLabel>{formatMessage(messages.Provider)}</ReportLabel>
              {staff ? (
                <span>
                  <strong> {staff.lastName},</strong>
                  <span> {staff.firstName} {genderParser(staff.title.personal)}</span>
                </span>
              ): <span> {EMPTY_STRING} </span>}
            </span>
          </Row>
        </ReportContent>
      </div>
    );
  }
  render() {
    const {
      isAdmin,
    } = this.props;
    const { formatMessage } = this.context.intl;

    const isAppointmentCardTab = this.props.activeTab === 1;
    console.log(this.props);
    return (
      <Form onSubmit={this.props.handleSubmit}>
        <Tabs
          theme="svg"
          value={this.props.activeTab}
          onTabClick={this.props.onTabClick}
        >
          <Tab
            value={1}
            label={formatMessage(messages.AppointmentCard)}
          />
          <Tab
            value={2}
            label={formatMessage(messages.TreatmentReport)}
          />
        </Tabs>
        <ContentWrap padding={isAppointmentCardTab ? "35px 32px 35px 32px": "10px 32px 35px 32px"}>
          {isAppointmentCardTab ? this.renderAppointmentCardTabContent(): this.renderTreatmentReportTabContent()}
        </ContentWrap>
        <CardFooter>
          <ContentWrap padding="10px 30px 10px 30px">
            <Row justifyContent="space-between" center noPadding>
              <Col>
                { this.props.onDelete && isAppointmentCardTab && <DeleteButton type="button" disabled={!isAdmin} onClick={this.props.onDelete}>
                  {formatMessage(messages.DeleteAppointment)}
                </DeleteButton> }
              </Col>
              <Col>
                <div>
                  <GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                  <GreenButton type="submit" disabled={this.props.pristine}>
                    { this.props.onDelete ? formatMessage(messages.SaveChanges) : 'Add Patient' }
                  </GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </Form>
    );
  }
}

AppointmentCardForm.defaultProps = {
  staff: [],
  serviceCategories: [],
  services: [],
  rooms: [],
  equipmentList: [],
  roles: [],
};

AppointmentCardForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

AppointmentCardForm.propTypes = {
  onDelete: PropTypes.func,
  onCancel: PropTypes.func,
  pristine: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  roles: PropTypes.array,
  users: PropTypes.array,
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  equipmentList: PropTypes.array,
  rooms: PropTypes.array,
  chosenService: PropTypes.string,
  chosenDate: PropTypes.any,
  activeTab: PropTypes.number,
};


export { default as validate } from './validate';
export default AppointmentCardForm;
