import { defineMessages } from 'react-intl';

export default defineMessages({
  AppointmentCard: {
    id: 'AppointmentForm.AppointmentCard',
    defaultMessage: 'Appointment Card',
  },
  TreatmentReport: {
    id: 'AppointmentForm.TreatmentReport',
    defaultMessage: 'Treatment Report',
  },
  AppointmentsFor: {
    id: 'AppointmentForm.AppointmentsFor',
    defaultMessage: 'appointments for',
  },
  AssignedStaff: {
    id: 'AppointmentForm.AssignedStaff',
    defaultMessage: 'Treatment Report can only be edited by the staff assigned to this appointment',
  },
  Pending: {
    id: 'Pending',
    defaultMessage: 'Pending',
  },
  Date: {
    id: 'Date',
    defaultMessage: 'Date',
  },
  other: {
    id: 'other',
    defaultMessage: 'other',
  },
  Time: {
    id: 'Time',
    defaultMessage: 'Time',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  min: {
    id: 'min',
    defaultMessage: 'min',
  },
  All: {
    id: 'All',
    defaultMessage: 'All',
  },
  NoYes: {
    id: 'No',
    defaultMessage: 'No',
  },
  to: {
    id: 'to',
    defaultMessage: 'to',
  },
  Search: {
    id: 'Search',
    defaultMessage: 'Search',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Continue: {
    id: 'Continue',
    defaultMessage: 'Continue',
  },
  MedicalService: {
    id: 'MedicalService',
    defaultMessage: 'Medical Service',
  },
  ServiceCategory: {
    id: 'ServiceCategory',
    defaultMessage: 'Service Category',
  },
  ServiceName: {
    id: 'ServiceName',
    defaultMessage: 'Service Name',
  },
  LongName: {
    id: 'LongName',
    defaultMessage: 'Long Name',
  },
  ShortName: {
    id: 'ShortName',
    defaultMessage: 'Short Name',
  },
  Resources: {
    id: 'Resources',
    defaultMessage: 'Resources',
  },
  Equipment: {
    id: 'Equipment',
    defaultMessage: 'Equipment',
  },
  Room: {
    id: 'Room',
    defaultMessage: 'Room',
  },
  StaffRole: {
    id: 'StaffRole',
    defaultMessage: 'Staff Role',
  },
  StaffName: {
    id: 'StaffName',
    defaultMessage: 'Staff Name',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  DeleteAppointment: {
    id: 'DeleteAppointment',
    defaultMessage: 'Delete Appointment',
  },
  CancelAppointment: {
    id: 'CancelAppointment',
    defaultMessage: 'Cancel Appointment',
  },
  Schedule: {
    id: 'Schedule',
    defaultMessage: 'Schedule',
  },
  Name: {
    id: 'Name',
    defaultMessage: 'Name',
  },
  Category: {
    id: 'Category',
    defaultMessage: 'Category',
  },
  Notes: {
    id: 'Notes',
    defaultMessage: 'Notes',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  Role: {
    id: 'Role',
    defaultMessage: 'Role',
  },
  Note: {
    id: 'Note',
    defaultMessage: 'Note',
  },
  NotesForInternalUsage: {
    id: 'NotesForInternalUsage',
    defaultMessage: 'Notes for internal usage',
  },
  WarningMessage: {
    id: "TreatmentReport.Warning.RestrictEditByStaff",
    defaultMessage: "Treatment Report can only be edited by the Staff who provided the appointment",
  },
  Provider: {
    id: 'Provider',
    defaultMessage: 'Provider'
  },
  Service: {
    id: 'Service',
    defaultMessage: 'Service',
  },
});
