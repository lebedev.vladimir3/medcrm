import moment from 'moment';


const validate = values => {
  const errors = {};
  const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
  const phonePattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  if (!values.email) {
    errors.email = 'Required';
  } else if (!emailPattern.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (values.personalEmail && !emailPattern.test(values.personalEmail)) {
    errors.personalEmail = 'Invalid email address';
  }

  if (values.birth) {
    const { dd, mm, yyyy } = values.birth;
    if (dd && mm && yyyy) {
      const now = new Date();
      const inputDate = moment(`${dd} ${mm} ${yyyy}`, 'DD MM YYYY');
      const isFuture = inputDate._d > now; // eslint-disable-line no-underscore-dangle

      if (!inputDate.isValid() || isFuture) {
        const message = 'Incorrect date';
        errors.birth = {};
        errors.birth.dd = message;
        errors.birth.mm = message;
        errors.birth.yyyy = message;
      }
    }
  }

  if (values.phone) {
    errors.phone = {};

    if (values.phone.work && !phonePattern.test(values.phone.work)) {
      errors.phone.work = 'Invalid phone number';
    }

    if (values.phone.personal && !phonePattern.test(values.phone.personal)) {
      errors.phone.personal = 'Invalid phone number';
    }
  }

  return errors;
};


export default validate;
