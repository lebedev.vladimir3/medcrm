import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { TimeTable, Table, TR, TD, TH } from 'components/Table';
import { Checkbox, RadioCheckbox } from 'components/Input';
import { Row, Col } from 'components/styles/Grid';
import moment from 'moment';
import styled from 'styled-components';
import { APPOINTMENT_ALL as AppointmentStatuses }  from '../../../common/constants/statuses';
import messages from  './messages';
import { genderParser } from "../../utils/genderParser";
import { APPOINTMENT_TABLE_COLUMN_NAME as columnNames } from 'constants/tableColumns';
import { EMPTY_STRING } from "constants/fillers";

const WeekTD = styled(TD).attrs({
  className: 'td-week'
})`
  background-color: #EAEBEE;
  position: relative;
  padding: ${props => props.padding ? props.padding : '0px'};
`;

const KW = styled.span`
  width: 64px;
`;

const TableExt = styled(TimeTable)`
  > thead > tr > td {
    height: ${props => props.isEdit ? '60px': '76px'};
  }
  > tbody > tr > td {
    height: ${props => props.isEdit ? '60px': '82px'};
    &.td-week {
      height: 35px;
    }
  }
`;

const THEAD = styled.thead`
  //background-color: #F3F5F8;
  font-size: ${fontSetting.baseSize}px;
  line-height: 18px;
  font-weight: bold;
  > th {
    height: 67px;
  }
`;

const Point = styled.div`
  background-color: #4687D6;
  display: inline-block;
  margin-left: 10px;
  height: 10px;
  width: 10px;
  border-radius: 5px;
`;

const ONE_DAY = 24 * 60 * 60 * 1000;

const WeekTRContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #495262;
  font-size: ${fontSetting.baseSizePlus1}px;
  opacity: .9;
`;

const TDWrap = styled(TD).attrs({
  className: 'td-wrap'
})`
  //height: 
  padding: ${props => props.padding ? props.padding : '0px'};
`;

const THWrap = styled(TH).attrs({
  className: 'td-wrap'
})`
  //height: 
  padding: ${props => props.padding ? props.padding : '0px'};
`;

const WeekTR = (props) => (
  <TR height="40px">
    <WeekTD colSpan="9" padding="0px 0px 0px 15px" >
      <WeekTRContent>
        <strong>{props.date.format('dddd, DD MMMM')}</strong>
        <KW><strong>KW {moment(props.date).week()}</strong></KW>
      </WeekTRContent>
    </WeekTD>
  </TR>
);


const getPadding = (isEdit) => {
  return isEdit ? "0px 0px 0px 15px": "0px 0px 0px 30px";
};

const renderCells = (orderColumns, cells, dataColumns) => {
  return orderColumns.map((id, index) => {
    const foundTH = cells.find(cell => cell.id === id),
      cellProps = dataColumns ? dataColumns.find(cell => cell.id === id).cell || {}: {};
    return foundTH ? React.cloneElement(foundTH.component(cellProps), { key: id }): null;
  })
};

const TableRow = ({ item, index, patient, onClick, onCheckedChange, isEdit, isRadio, orderColumns, dataColumns, link }) => {
  const firstName = item.staff ? item.staff.firstName : '';
  const lastName = item.staff ? item.staff.lastName : '';
  const gender = item.staff && item.staff.title && item.staff.title.personal && genderParser(item.staff.title.personal);
  const createdAt = moment(item.createdAt).format('DD.MM.YYYY');
  const status = AppointmentStatuses.GENERAL.find((element) => element.value === item.status);
  const financialStatus = AppointmentStatuses.FINANCIAL.find((status) => item.financialStatus && status.value === item.financialStatus);
  const dateDelta = item.date - (new Date());


  const TDs = [
    { id: columnNames.CHECKBOX, component: () => (
      <TDWrap padding="0px 0px 0px 15px">
        <div style={{
          display: 'flex',
          alignItems: 'center'
        }}>
          {isRadio ? <RadioCheckbox
            name="aa"
            checked={item.checked}
            onChange={() => onCheckedChange(!isEdit ? item._id : index)}
            value={index}
          />: <Checkbox
            checked={item.checked}
            onChange={() => onCheckedChange(!isEdit ? item._id : index)}
            style={{ marginRight: '0px', marginLeft: '-8px' }}
            checkboxStyle={{ width: '30px' }}
          />}
          { dateDelta < ONE_DAY && dateDelta > 0 && <Point />}
        </div>
      </TDWrap>
    ) },
    { id: columnNames.TIME, component: () => <TDWrap centered>{item.date.format('HH:mm')}</TDWrap>},
    { id: columnNames.SERVICE, component: () => (
      <TDWrap centered>
        <strong>{(item.service && item.service.name ? item.service.name.shortName : EMPTY_STRING)}</strong>
      </TDWrap>
    ) },
    { id: columnNames.DURATION, component: () => <TDWrap centered>{`${item.duration}min`}</TDWrap> },
    { id: columnNames.STAFF, component: (props) => {
      const Element = props.inline ? 'span': 'div';
      return (
      <TDWrap padding={getPadding(!isEdit)}>
        {lastName || firstName ? (
          <div>
            <Element><strong>{lastName + (firstName && `, `)}</strong></Element>
            <Element>{firstName ? firstName: ''} {gender}</Element>
          </div>
        ): EMPTY_STRING}
      </TDWrap>
    )} },
    { id: columnNames.ROOM, component: () => (
      <TDWrap>
        <Row paddingBottom="0px">
          { item.room ?
            <Col>
              <span>{item.room.number}</span>
              <span>{item.room.name}</span>
            </Col>: EMPTY_STRING}
        </Row>
      </TDWrap>
    ) },
    { id: columnNames.PRESCRIPTION, component: () => <TDWrap centered>{`${patient.id}-${item.prescription.id}`}</TDWrap>},
    { id: columnNames.STATUS, component: () => (
      <TDWrap>
        <Row justifyContent="center" paddingBottom="0px">
          <Col style={{ width: '125px' }}>
            {status && (
              <strong>
                <span style={{ marginRight: 8 }}>{status.icon}</span>
                <span>{status.label}</span>
              </strong>
            )}
            <span style={{ paddingLeft: 24 }}>{item.statusChangedAt ? moment(item.statusChangedAt).format('DD.MM.YYYY') : createdAt}</span>
          </Col>
        </Row>
      </TDWrap>
    ) },
    { id: columnNames.FINANCIAL_STATUS, component: () => (
      <TDWrap>
        <Row justifyContent="center" paddingBottom="0px">
          <Col>
            {financialStatus && <strong>{financialStatus.icon} {financialStatus.label}</strong>}
            <span style={{ paddingLeft: 20 }}>{item.financialStatusChangedAt ? moment(item.financialStatusChangedAt).format('DD.MM.YYYY') : createdAt}</span>
          </Col>
        </Row>
      </TDWrap>
    ) },
  ];

  return (
    <TR key={index} link={link} style={{}}>
      {renderCells(orderColumns, TDs, dataColumns)}
    </TR>
  );
};

const AppointmentTable = ({ items, isEdit, patient, onToggleAppointmentChecked, push, orderColumns, dataColumns }, context) => {
  const elements = items
    .map((item) => ({ ...item, date: moment(item.date) }))
    .sort((a, b) => {
      if (a.isRadio && b.isRadio) {
        return a.date - b.date;
      } else if (a.date.isSame(b.date, 'day') && (a.isRadio || b.isRadio)) {
        return a.isRadio ? 1 : -1;
      }
      return a.date - b.date;
    })
    .reduce((acc, item, index, array) => {
      if (index === 0 || !item.date.isSame(array[index - 1].date, 'day')) {
        // Add day heading row
        const { date } = item;
        acc.push(
          <WeekTR
            date={date}
            key={-index - 1}
          />)
        ;
      }
      return (
        [
          ...acc,
          <TableRow
            item={item}
            patient={patient}
            onCheckedChange={onToggleAppointmentChecked}
            link={!isEdit ?`/main/patients/${patient._id}/appointments/${item._id}` : undefined}
            isEdit={isEdit}
            index={index}
            isRadio={item.isRadio}
            key={index + 1}
            orderColumns={orderColumns}
            dataColumns={dataColumns}
          />,
        ]);
    }, []);

  const THs = [
    { id: columnNames.CHECKBOX, component: () => <TDWrap></TDWrap> },
    { id: columnNames.TIME, component: () => <THWrap centered>{context.intl.formatMessage(messages.Time)}</THWrap>},
    { id: columnNames.SERVICE, component: () => <THWrap centered>{context.intl.formatMessage(messages.Service)}</THWrap> },
    { id: columnNames.DURATION, component: () => <THWrap centered>{context.intl.formatMessage(messages.Duration)}</THWrap> },
    { id: columnNames.STAFF, component: () => <THWrap padding={getPadding(!isEdit)}><strong>{context.intl.formatMessage(messages.Staff)}</strong></THWrap> },
    { id: columnNames.ROOM, component: () => <THWrap>{context.intl.formatMessage(messages.Room)}</THWrap> },
    { id: columnNames.PRESCRIPTION, component: () => <THWrap centered>{context.intl.formatMessage(messages.Prescription)}</THWrap>},
    { id: columnNames.STATUS, component: () => (
      <THWrap centered>
        <Row justifyContent="center" paddingBottom="0px" bold>
          <Col style={{ textAlign: 'left', paddingLeft: '1em'}}>
            <strong>{context.intl.formatMessage(messages.Appointment)}</strong>
            <strong>{context.intl.formatMessage(messages.Status)}</strong>
          </Col>
        </Row>
      </THWrap>
    ) },
    { id: columnNames.FINANCIAL_STATUS, component: () => (
      <THWrap centered>
        <Row justifyContent="center" paddingBottom="0px" bold>
          <Col style={{ textAlign: 'left', paddingLeft: '1em'}}>
            <strong>{context.intl.formatMessage(messages.Financial)}</strong>
            <strong>{context.intl.formatMessage(messages.Status)}</strong>
          </Col>
        </Row>
      </THWrap>
    ) },
  ];

  return (
    <TableExt isEdit={isEdit}>
      {orderColumns.map((id, index) => {
        const found = dataColumns.find(th => th.id === id);
        return found ? <col key={index} width={`${found.width}%`}/>: null;
      })}
      <THEAD>
        <TR>
          {renderCells(orderColumns, THs, dataColumns)}
        </TR>
      </THEAD>
      <tbody>
        {elements}
      </tbody>
    </TableExt>
  );
};

AppointmentTable.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
AppointmentTable.propTypes = {
  push: PropTypes.func,
  onToggleAppointmentChecked: PropTypes.func,
  items: PropTypes.array,
  patient: PropTypes.object,
  orderColumns: PropTypes.array.isRequired,
  dataColumns: PropTypes.array.isRequired
};

AppointmentTable.defaultProps = {
  items: [],
  onToggleAppointmentChecked: () => {
  },
};


export default AppointmentTable;
