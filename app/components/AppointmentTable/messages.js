import { defineMessages } from 'react-intl';

export default defineMessages({
  Time: {
    id: 'Time',
    defaultMessage: 'Time',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Continue: {
    id: 'Continue',
    defaultMessage: 'Continue',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  Resources: {
    id: 'Resources',
    defaultMessage: 'Resources',
  },
  Equipment: {
    id: 'Equipment',
    defaultMessage: 'Equipment',
  },
  Room: {
    id: 'Room',
    defaultMessage: 'Room',
  },
  StaffRole: {
    id: 'StaffRole',
    defaultMessage: 'Staff Role',
  },
  StaffName: {
    id: 'StaffName',
    defaultMessage: 'Staff Name',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  DeleteAppointment: {
    id: 'DeleteAppointment',
    defaultMessage: 'Delete Appointment',
  },
  Service: {
    id: 'Service',
    defaultMessage: 'Service',
  },
  Prescription: {
    id: 'Prescription',
    defaultMessage: 'Prescription',
  },
  Status: {
    id: 'Status',
    defaultMessage: 'Status',
  },
  FinancialStatus: {
    id: 'FinancialStatus',
    defaultMessage: 'Financial Status',
  },
  Financial: {
    id: 'Financial',
    defaultMessage: 'Financial',
  },
  Appointment: {
    id: 'Appointment',
    defaultMessage: 'Appointment',
  },
});
