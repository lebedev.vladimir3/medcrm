import React, { PropTypes } from 'react';

import DayPicker from 'react-day-picker';
import './style.scss';

class MyCalendar extends React.Component {

  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);

    this.state = {
      selectedDay: this.props.selectedDay,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedDay !== nextProps.selectedDay) {
      this.setState({
        selectedDay: nextProps.selectedDay,
      });
    }
  }

  handleDayClick(day, { disabled, selected }) {
    if (disabled) {
      return;
    }
    this.setState({
      selectedDay: selected ? null : day,
    });
    this.props.dateSelected(day);
  }

  render() {
    const date = new Date(this.props.selectedDay);
    const dat = new Date(this.props.selectedDay.getTime() + 24 * 60 * 60 * 1000 * 6);

    return (
      <div className="menu-calendar">
        <DayPicker
          selectedDays={this.props.AgendaOrCalendar === 'agenda' ?
            this.props.selectedDay :
            { from: this.props.selectedDay, to: dat }
          }
          firstDayOfWeek={1}
          onDayClick={this.handleDayClick}
          enableOutsideDays
        />
      </div>
      );
  }
}

MyCalendar.propTypes = {
  dateSelected: PropTypes.func.isRequired,
  selectedDay: PropTypes.object,
};

export default MyCalendar;
