import styled from 'styled-components';


const StyledCardFooter = styled.div`
  border-radius: 0 0 4px 4px;
  background-color: white;
  padding: 10px 0px;
  @media (max-width: 900px) {
    height: auto;
  }
`;


export default StyledCardFooter;
