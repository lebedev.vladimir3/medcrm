/**
*
* Card
*
*/


import styled from 'styled-components';

const StyledCard = styled.div`
  max-width: 980px;
  margin: auto;
  border: 1px solid #DBDFEA;
  border-radius: 4px 4px;
  background-color: #FFFFFF;
`;

export const Card = styled.div`
  border: 1px solid #DBDFEA;
  border-radius: 4px 4px;
  background-color: #FFFFFF;
`;


export default StyledCard;

export { default as CardFooter } from './CardFooter';
