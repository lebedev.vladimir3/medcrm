import React from 'react';
import { Field } from 'redux-form';
import styled from 'styled-components';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea } from 'components/Input';
import messages from './messages';

import renderHelper from 'components/common/renderHelper';
import { ContentWrap } from 'components/styles/Containers';
import { Card, CardFooter } from 'components/styles/Card';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel, RequiredStar } from 'components/styles/common';
import Divider from 'components/common/Divider';

const PatientForm = (props, context) => {
  const { handleSubmit, onDelete, pristine, reset } = props;
  const { formatMessage } = context.intl;

  return (
    <Card>
      <Form onSubmit={handleSubmit}>
        <ContentWrap padding="40px 30px">
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Title)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="title"
                    component={renderHelper.inputField}
                    //placeholder={formatMessage(messages.ShortNamePlaceholder)}
                    required />
                </Col>
                <Col width="50%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.BusinessName)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="100%">
                  <Field
                    name="businessName"
                    component={renderHelper.inputField}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Specialization)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="100%">
                  <Field
                    name="specialization"
                    component={renderHelper.inputField}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Description)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="100%">
                  <Field
                    name="description"
                    component={renderHelper.textAreaField}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Divider marginTop={3} dashed color="#AAB2B9"/>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Phone)} & {formatMessage(messages.FaxNumber)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="phone"
                    type="phone"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.Phone)}
                    required
                  />
                </Col>
                <Col width="50%">
                  <Field
                    name="fax"
                    type="text"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.FaxNumber)}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Email)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="email"
                    type="email"
                    component={renderHelper.inputField}
                  />
                </Col>
                <Col width="50%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Website)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="75%">
                  <Field
                    name="website"
                    type="url"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.WebsitePlaceholder)}
                  />
                </Col>
                <Col width="25%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Divider marginTop={10} dashed color="#AAB2B9"/>
          <Row center>
            <LabelCol>
              <FormLabel>{formatMessage(messages.Address)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="75%">
                  <Field
                    name="address.street"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.StreetHouse)}
                  />
                </Col>
                <Col width="25%">
                  <Field
                    name="address.post"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.Postbox)}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol />
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="address.city"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.City)}
                  />
                </Col>
                <Col width="50%">
                  <Field
                    name="address.country"
                    component={renderHelper.inputField}
                    placeholder={formatMessage(messages.Country)}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </ContentWrap>
        <CardFooter>
          <ContentWrap padding="10px 30px 10px 30px">
            <Row justifyContent="space-between" center noPadding>
              <Col />
              <Col>
                <div>
                  <GreyButton onClick={reset}>{formatMessage(messages.Cancel)}</GreyButton>
                  <GreenButton type="submit" disabled={pristine}>
                    { onDelete ? formatMessage(messages.SaveChanges) : formatMessage(messages.SaveChanges) }</GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </Form>
    </Card>
  );
};

PatientForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
PatientForm.propTypes = {
  handleSubmit: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func,
  pristine: React.PropTypes.bool.isRequired,
  reset: React.PropTypes.func.isRequired,
};


export { default as validate } from './validate';
export default PatientForm;

