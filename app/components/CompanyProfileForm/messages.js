/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  Mr: {
    id: 'Mr',
    defaultMessage: 'Mr',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  Email: {
    id: 'Email',
    defaultMessage: 'E-mail',
  },
  Description: {
    id: 'Description',
    defaultMessage: 'Description',
  },
  Phone: {
    id: 'Phone',
    defaultMessage: 'Phone',
  },
  Website: {
    id: 'Website',
    defaultMessage: 'Website',
  },
  WebsitePlaceholder: {
    id: 'Website.Placeholder',
    defaultMessage: 'http://www.yoursite.de',
  },
  Address: {
    id: 'Address',
    defaultMessage: 'Address',
  },
  City: {
    id: 'City',
    defaultMessage: 'City',
  },
  Country: {
    id: 'Country',
    defaultMessage: 'Country',
  },
  Region: {
    id: 'Region',
    defaultMessage: 'Region',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Postbox: {
    id: 'Postbox',
    defaultMessage: 'Postbox',
  },
  StreetHouse: {
    id: 'StreetHouse',
    defaultMessage: 'Street, House number',
  },
  CompanyTitle: {
    id: 'CompanyTitle',
    defaultMessage: 'Company Title',
  },
  Specialization: {
    id: 'Specialization',
    defaultMessage: 'Specialization',
  },
  BusinessName: {
    id: 'BusinessName',
    defaultMessage: 'Business name',
  },
  SpecializationCenter: {
    id: 'SpecializationCenter',
    defaultMessage: 'Specialization of med. center',
  },
  DescriptionCenter: {
    id: 'DescriptionCenter',
    defaultMessage: 'Description of med. center',
  },
  FaxNumber: {
    id: 'FaxNumber',
    defaultMessage: 'Fax number',
  },
  PersonalTitle: {
    id: 'PersonalTitle',
    defaultMessage: 'Personal title',
  },
});
