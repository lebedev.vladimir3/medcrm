
const validate = values => {
  const errors = {};
  const phonePattern = /^[\d \+\-]+$/;

  if (values.phone && !phonePattern.test(values.phone)) {
    errors.phone = 'Invalid phone number';
  }
  return errors;
};


export default validate;
