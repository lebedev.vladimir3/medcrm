import { defineMessages } from 'react-intl';

export default defineMessages({
  Delete: {
    id: 'Delete',
    defaultMessage: 'Delete',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
});
