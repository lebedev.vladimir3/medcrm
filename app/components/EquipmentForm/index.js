import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { Field } from 'redux-form';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, Select, TextArea, Checkbox as CheckboxInput, RadioInput } from 'components/Input';

import { ContentWrap } from 'components/styles/Containers';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';
import { Card, CardFooter, CardFooterCommon } from 'components/styles/Card';
import Obligatory from 'components/common/obligatoryLegend';
import renderHelper from 'components/common/renderHelper';

import messages from './messages';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';

import theme from 'components/styles/colorScheme';

const Label = styled.label`
  color: ${theme.Text.secondAccentLow};
  text-align: right;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
`;

const InputBlock = styled.div`
  width: 100%;
  border: 1px solid #BDBDBD;
  background-color: ${theme.Input.background};
  border-radius: 2px;
  position: relative;
  margin-left: 10px;
  padding: 12px 12px 0;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
`;

const CheckboxWrapper = styled.div`
  display: inline-block;
  margin-right: 15px;
`;

const InputWrapperItem = styled.div`
  margin-bottom: 14px;
`;

const InputWrapperItemIndented = styled(InputWrapperItem)`
  padding-left: 20px;
`;

const renderInputField = (inputProps) => <Input {...inputProps.input} {...inputProps} />;
const renderTextAreaField = (inputProps) => <TextArea {...inputProps.input} {...inputProps} />;
const renderSelectField = (renderProps) => <Select {...renderProps.input} {...renderProps} />;
const renderCheckboxField = (inputProps) => <CheckboxWrapper><CheckboxInput {...inputProps.input} {...inputProps} /></CheckboxWrapper>;
const renderRadioCheckbox = (inputProps) => <RadioInput {...inputProps} />;

const EquipmentForm = (props, context) => {
  const {
    handleSubmit,
    onSubmit,
    onCancel,
    onDelete,
    rooms,
    serviceCategories,
    roomValue,
    handleChangeService,
    handleChangeServiceCategory,
    handleChangeAllServices,
    chosenServices,
    chosenServiceCategories,
    allServices,
    allRoomsAreAlternativeValue,
    isEdit,
    mobility,
  } = props;

  const { formatMessage } = context.intl;

  return (
    <Card>
      <Form onSubmit={handleSubmit}>
        <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 30px`}>
          {!isEdit && <Obligatory />}
          <Row center>
            <LabelCol>
              <FormLabel required={!isEdit}>{formatMessage(messages.EquipmentName)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="name"
                    component={renderHelper.inputField}
                    required />
                </Col>
                <Col width="50%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel required={!isEdit}>{formatMessage(messages.ShortName)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="25%">
                  <Field
                    name="shortName"
                    component={renderHelper.inputField}
                    required
                  />
                </Col>
                <Col width="75%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row paddingBottom="16px">
            <LabelCol>
              <FormLabel>{formatMessage(messages.Specification)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col grow="1">
                  <Field
                    name="specification"
                    component={renderHelper.inputField}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center paddingBottom="23px">
            <LabelCol>
              <FormLabel>{formatMessage(messages.Mobility)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="12.5%">
                  <Field
                    name="mobility"
                    component={renderHelper.radioField}
                    type="radio"
                    value="true"
                    label={formatMessage(messages. Yes )}
                    aria-label="mobility"
                  />
                </Col>
                <Col width="12.5%">
                  <Field
                    name="mobility"
                    component={renderHelper.radioField}
                    type="radio"
                    value="false"
                    label={formatMessage(messages. No )}
                    aria-label="mobility"
                  />
                </Col>
                <Col width="75%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row >
            <LabelCol height="40px" middle>
              <FormLabel required={!isEdit}>{formatMessage(messages.Room)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="50%">
                  <Field
                    name="room"
                    component={renderHelper.selectField}
                    placeholder={formatMessage(messages.ChoosePrimaryRoom)}
                    options={[
                      ...rooms.map((item, index) => ({
                        value: item._id,
                        label: `${item.number} - ${item.name}`
                      }))
                    ]}
                    required
                  />
                </Col>
                <Col width="50%">
                  { mobility &&
                    <InputBlock style={{ marginLeft: '0px' }}>
                      <InputWrapperItem>
                        <Label>{formatMessage(messages.AlternativeRooms)}</Label>
                      </InputWrapperItem>
                      <InputWrapperItem>
                        <Field
                          name="allRoomsAreAlternative"
                          type="checkbox"
                          component={renderHelper.checkboxField}
                          label={formatMessage(messages.AllRooms)}
                        />
                      </InputWrapperItem>
                      {rooms.map((item, index) => {
                        if (roomValue === item._id) { return null; }
                        return (
                          <InputWrapperItemIndented key={index}>
                            <Field
                              name="alternativeRooms"
                              type="checkbox"
                              component={renderHelper.multiCheckboxField}
                              id={item._id}
                              disabled={allRoomsAreAlternativeValue || false}
                              label={`${item.number} - ${item.name}`}
                            />
                          </InputWrapperItemIndented>
                        );
                      })}
                    </InputBlock>
                  }
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </ContentWrap>
        <CardFooterCommon
          deleteText={formatMessage(messages.DeleteEquipment)}
          cancelText={formatMessage(messages.Cancel)}
          applyText={onDelete ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddEquipment)}
          onDelete={onDelete}
          onCancel={onCancel}
          pristine={false}
        />
      </Form>
    </Card>
  );
};

EquipmentForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EquipmentForm.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  onDelete: PropTypes.func,
  rooms: PropTypes.array,
  serviceCategories: PropTypes.array,
  handleChangeService: PropTypes.func,
  handleChangeServiceCategory: PropTypes.func,
  handleChangeAllServices: PropTypes.func,
  chosenServices: PropTypes.array,
  chosenServiceCategories: PropTypes.array,
  allServices: PropTypes.bool,
  allRoomsAreAlternativeValue: PropTypes.any,
  isEdit: PropTypes.bool,
};


EquipmentForm.defaultProps = {
  rooms: [],
  services: [],
  serviceCategories: [],
};

export default EquipmentForm;
export { default as validate } from './validate';
