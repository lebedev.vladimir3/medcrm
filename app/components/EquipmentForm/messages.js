/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  EquipmentName: {
    id: "EquipmentName",
    defaultMessage: "Equipment Name"
  },
  Specification: {
    id: "EquipmentForm.Specification",
    defaultMessage: "Specification"
  },
  Mobility: {
    id: "EquipmentForm.Mobility",
    defaultMessage: "Mobility"
  },
  ChoosePrimaryRoom: {
    id: "EquipmentForm.ChoosePrimaryRoom",
    defaultMessage: "Choose primary room"
  },
  EquipmentNamePlaceholder: {
    id: "EquipmentForm.EquipmentNamePlaceholder",
    defaultMessage: "Equipment name"
  },
  EquipmentShortNamePlaceholder: {
    id: "EquipmentForm.EquipmentShortNamePlaceholder",
    defaultMessage: "Short name"
  },
  SpecificationPlaceholder: {
    id: "EquipmentForm.SpecificationPlaceholder",
    defaultMessage: "Equipment specification"
  },
  AlternativeRooms: {
    id: "EquipmentForm.AlternativeRooms",
    defaultMessage: "Alternative Room(s)"
  },
  AllOtherRooms: {
    id: "EquipmentForm.AllOtherRooms",
    defaultMessage: "All Other rooms"
  },
  AllRooms: {
    id: "EquipmentForm.AllRooms",
    defaultMessage: "All Rooms"
  },
  Yes: {
    id: "Yes",
    defaultMessage: "Yes"
  },
  No: {
    id: "No",
    defaultMessage: "No"
  },
  Room: {
    id: "Room",
    defineMessages: "Room",
    defaultMessage: "Room",
  },
  Cancel: {
    id: "Cancel",
    defineMessages: "Cancel",
    defaultMessage: "Cancel",
  },
  DeleteEquipment: {
    id: "ResourcesPage.DeleteEquipment",
    defineMessages: "Delete Equipment",
    defaultMessage: "Delete Equipment"
  },
  SaveChanges: {
    id: "SaveChanges",
    defineMessages: "Save Changes",
    defaultMessage: "Save Changes"
  },
  AddEquipment: {
    id: "EquipmentForm.AddEquipment",
    defineMessages: "Add Equipment",
    defaultMessage: "Add Equipment"
  },
  ShortName: {
    id: "ShortName",
    defineMessages: "Short Name",
    defaultMessage: "Short Name"
  }
});
