const validate = (values) => {
  const errors = {};
  if (values.name) {
    if (values.name.length > 30) {
      errors.name = 'Name must nit be longer than 30 letters';
    }
  }
  if (values.shortName) {
    if (values.shortName.length > 10) {
      errors.shortName = 'Short Name must nit be longer than 30 letters';
    }
  }
  if(!values.room){
    errors.room = 'The room field is required'
  }
  return errors
};

export default validate;
