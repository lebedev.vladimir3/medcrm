import fontSetting from 'components/styles/font';
import styled from 'styled-components';
import React, { PropTypes }  from 'react';


const TabPanel = styled.div`
  margin: 0 auto 15px;
  height: 50px;
  position: relative;
`;

const Legend = styled.div`
  bottom:-1px;
  align-items: center;
  display: flex;
  //justify-content: center;
  height: 100%;
  z-index:100;
`;

const FirstTabText = styled.div`
  color: #49545A;
  
  font-size: ${fontSetting.baseSizePlus7}px;
  line-height: 29px;
`;

const FormLegend = (props) => {
  return (
    <TabPanel style={{ width: props.width }} >
      <Legend>
        <props.Icon
          //size="24"
          style={{ marginRight: '14px' }}
          color={props.iconColor ? props.iconColor : '#3E7DBC'}
          {...props.iconProps}
        />
        <FirstTabText>{props.text}</FirstTabText>
        {props.secondText && <FirstTabText style={{ margin: '0 10px' }}>/</FirstTabText>}
        <FirstTabText style={{ color: (props.iconColor ? props.iconColor : '#3E7DBC') }}>{props.secondText}</FirstTabText>
      </Legend>
    </TabPanel>
  );
};

FormLegend.propTypes = {
  text: PropTypes.string,
  secondText: PropTypes.string,
  iconColor: PropTypes.string,
};


export default FormLegend;
