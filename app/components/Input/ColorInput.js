import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { GithubPicker } from 'react-color';
import AngleDown from 'react-icons/lib/fa/caret-down';
import { COLORS } from './constants';
import theme from 'components/styles/colorScheme';
import { CustomColor } from 'components/styles/common';


const Absolute = styled.div`
  position: absolute;
  z-index: 10;
`;

const Button = styled.div`
  display: inline-block;
`;
const Div = styled.div`
  position: relative;
  margin-right: 5px;
`;

const ButtonColor = styled.div`
  display: flex;
  background-color: ${theme.Input.background};
  flex-direction: row;
  padding: 8px;
  height: 40px;
  align-items: center;
  border: 1px solid #CED0DA;
  border-radius: 4px;
  .color-input {
    border: 1px solid #CED0DA;
    width: 17px;
    height: 17px;
  }
  &:hover {
    cursor: pointer;
  }
`;


class ColorInput extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.colorChange = this.colorChange.bind(this);
    this.toggleActive = this.toggleActive.bind(this);
    this.colorChange = this.colorChange.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);

    this.state = { active: false, color: 'white' };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
      this.wrapperRef = node;
  }

  handleClickOutside() {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ active: false });
    }
  }

  toggleActive() {
    this.setState({ active: !this.state.active });
  }

  colorChange(color) {
    this.setState({ active: false });
    this.props.onChange(color.hex);
  }

  render() {
    return (
      <Button innerRef={this.setWrapperRef}>
        <ButtonColor onClick={this.toggleActive} >
          <Div>
            <CustomColor color={this.props.color ? this.props.color : 'white' }></CustomColor>
          </Div>
          <AngleDown />
        </ButtonColor>
        <Absolute style={{ display: this.state.active ? 'block' : 'none' }} >
          <GithubPicker width={163} colors={COLORS} onChange={this.colorChange} />
        </Absolute>
      </Button>
    );
  }
}


ColorInput.propTypes = {
  color: PropTypes.string,
  onChange: PropTypes.func,
};


export default ColorInput;
