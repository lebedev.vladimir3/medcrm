import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import DatePicker from 'react-datepicker';
import CalendarIcon from 'assets/icons/Calendar/Calendar.svg';


const CalendarWrapper = styled.div`
  border: 1px solid #CED0DA;
  border-radius: 4px;
  height: 36px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 8px 5px;
  cursor: pointer;
`;

const CalendarText = styled.div`
  opacity: 0.9;
  color: #7E7E7E;
  
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  margin-right: 15px;
`;


const DateInput = (props) => (
  <CalendarWrapper onClick={props.onClick}>
    <CalendarText style={{ color: props.value ? '#455A64' : '#8D8D8D' }}>
      {props.value ? props.value : 'DD.MM.YYYY' }
    </CalendarText>
    <CalendarIcon />
  </CalendarWrapper>
);


export default (props) => (
  <DatePicker {...props} customInput={<DateInput />} />
);

