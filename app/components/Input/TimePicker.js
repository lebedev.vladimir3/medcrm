import React from 'react';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import theme from 'components/styles/colorScheme';


const getMinutes = (time) => (time.hours() * 60) + time.minutes();

const getDateWithMinutesAsTime = (minutes) => {
  const d = moment.duration(minutes, 'minutes');
  return moment().hours(d.hours()).minutes(d.minutes()).seconds(0);
};

// border: hasError ? `1px solid ${theme.Input.error}` : `${theme.Input.border}`,

export default ({ input, disabled, hasError }) => (
  <TimePicker
    defaultOpenValue={moment().hour(0).minute(0)}
    showSecond={false}
    value={input.value ? getDateWithMinutesAsTime(input.value) : moment().hours(0).minutes(0)}
    format="HH:mm"
    defaultValue={moment().hour(0).minute(0)}
    onChange={(value) => { input.onChange(getMinutes(value)); }}
    disabled={disabled}
    style={{
      border: hasError ? `1px solid ${theme.Input.error}` : `1px solid #CED0DA`,
      borderRadius: '4px',
    }}
  />
);
