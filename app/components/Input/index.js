import fontSetting from 'components/styles/font';
/**
 *
 * Input
 *
 */

import React from 'react';
import styled from 'styled-components';

import CheckboxIcon from 'assets/icons/Checkbox/Active.svg'
import CheckboxBlankIcon from 'assets/icons/Checkbox/Inactive.svg'
import CheckboxDisableIcon from 'assets/icons/Checkbox/Locked.svg'

import RadioIcon from 'assets/icons/RadioButton/Active.svg';
import RadioBlankIcon from 'assets/icons/RadioButton/Inactive.svg';
import MdSync from 'react-icons/lib/md/sync';

import SelectArrowActive from 'assets/icons/Arrows/Select/Active.svg';
import SelectArrowDisabled from 'assets/icons/Arrows/Select/Disabled.svg';

import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap_white.css';

import Checkbox from 'material-ui/Checkbox';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormControlLabel } from 'material-ui/Form';
import theme from 'components/styles/colorScheme';
import { withStyles } from 'material-ui/styles';

const styles = {
  default: {
    color: theme.Checkbox.border.main,
    width: '20px',
    height: '20px',
    marginRight: 9,
    '& circle.background_inactive': {
      fill: theme.Input.background
    },
    '& .border_inactive': {
      fill: theme.Input.border
    },
    '& .svg-path-checkbox.border_inactive': {
      fill: theme.Checkbox.border.accent
    },
    '& .svg-wrap.active': {
      backgroundColor: theme.Checkbox.background,
    }
  },
  checked: {
    color: theme.Button.main.active.background,
  },
  disabled: {
    color: theme.Checkbox.disabled
  },
  label: {
    fontFamily: fontSetting.fontFamily,
    fontSize: fontSetting.baseSize,
    fontWeight: 'bold',
  },
  labelActive: {
    color: theme.Checkbox.label.active
  },
  labelDisabled: {
    color: theme.Checkbox.label.disabled
  },
  root: {
    marginLeft: 0,
    marginRight: 0,
  },
  checkboxIconStyle: {
    width: '20px',
    height: '20px',
    //backgroundColor: 'white',
  },
  radioIconStyle: {
    width: '20px',
    height: '20px',
  }
};

import DatePicker from 'react-datepicker';
import CalendarIcon from 'assets/icons/Calendar/Calendar.svg';
import ArrowDown from 'react-icons/lib/md/keyboard-arrow-down';
import ReactSelect from 'components/common/ReactSelect';
import '../../assets/styles/custom/react-select/default.scss';
import 'react-datepicker/dist/react-datepicker.css';
import './assets/style.scss';

const InputWrapper = styled.div`
  position: relative;
  width: 100%;
  ${props => props.height && ('height: ' + props.height + ';') }
`;

const CheckboxWrapper = styled.div`
  display: inline-block;
  position: relative;
`;

const StyledInput = styled.input`
  width: 100%;
  height: 40px;
  border: 1px solid ${props => {
    let result = theme.Input.border;
    if (props.hasError) { 
      result = theme.Input.error 
    } else if (props.disabled){
      result = props.hasValue ? theme.Input.disabled.value.border: theme.Input.disabled.noValue.border;
    } 
    return result
  }};
  border-radius: 2px;
  // background: linear-gradient(0deg, #FFFFFF 0%, #FFFFFF 100%);
  background-color: ${props => props.disabled ? (props.hasValue ?
  theme.Input.backgroundDisabledWithValue : theme.Input.backgroundDisabled) : theme.Input.background};
  padding-left: 10px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  color: ${theme.Input.color};
  &::placeholder {
    opacity: 0.9;
    line-height: 19px;
    font-weight: normal;
    color: ${theme.Input.colorPlaceholder};
  }
  &:focus {
    outline: none;
    border-color: ${(props) => props.hasError ? 'none' : '#5D93CA'};
    background-color: #FFFFFF;
    box-shadow: ${(props) => props.hasError ? 'none' : '0 0 2px 1px #D3EBF9'};
  }
`;


const StyledTextArea = styled.textarea`
  width: 100%;
  height: ${props => props.height || '100px'};
  border: 1px solid #CED0DA;
  border-radius: 2px;
  // background: linear-gradient(0deg, #FFFFFF 0%, #FFFFFF 100%);
  background: ${props => props.input && props.input.value ? theme.TextArea.hasValue.background : theme.Input.background};
  padding-left: 10px;
  padding-top: 10px;
  font-weight: 600;
  font-size: ${fontSetting.baseSize}px;
  line-height: 18px;
  color: #455A64;
  &::placeholder {
    opacity: 0.9;
    line-height: 19px;
    font-weight: normal;
    color: ${theme.Input.colorPlaceholder};
  }
  &:focus {
    outline: none;
    border-color: #5D93CA;
    background: #FFFFFF !important;
    box-shadow: 0 0 2px 1px #D3EBF9;
  }
`;


const StyledSelect = styled(ReactSelect)`
  width: ${props => props.width ? props.width : '100%'};
  min-height: 40px;
  border-radius: 2px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  color: #455A64;
  cursor: pointer;
  appearance: none;
  > option {
    font-size: ${fontSetting.baseSize}px;
    font-weight: 600;
    line-height: 18px;
    color: #455A64;
  }
  .Select-control {
    background-color: ${props => props.disabled ? (props.hasValue ?
  theme.Input.backgroundDisabledWithValue : theme.Input.backgroundDisabled) : theme.Input.background} !important;
    min-height: 40px;
    border-radius: 2px !important;
    border-color: ${props => props.hasError ? theme.Input.error : theme.Input.border};
    .Select-placeholder {
        font-weight: normal;
        color: ${theme.Input.colorPlaceholder};
    }
    .Select-arrow {
      border-color: ${theme.Select.arrow} transparent transparent;
    }
    //border-left: ${(props) => props.isRequired ? '4px solid #fcc048' : '1px solid #ccc;'} ;
  }
  &.is-open > .Select-control .Select-arrow {
      border-color: transparent transparent ${theme.Select.arrow};
    }
  .Select-input {
    height: 40px;
    line-height: 40px;
  }
  &.is-focused:not(.is-open) > .Select-control {
    border-color: #5D93CA;
    box-shadow: none;
  }
  &.is-open > .Select-control {
    background-color: ${theme.Input.focus};
  }
`;


const Stripe = styled.div`
  height: 100%;
  width: 4px;
  border-radius: 4px 0 0 4px;
  position: absolute;
  top: 0px;
`;


const ErrorStripe = styled(Stripe)`
  background-color: #E17E28;
`;


const RequiredStripe = styled(Stripe)`
  background-color: #FCC048;
`;


const StyledCalendarIcon = styled(CalendarIcon)`
  position: absolute;
  top: 5px;
  right: 8px;
`;


const Input = (inputProps) => {
  const { required, ...otherInputProps } = inputProps;
  const hasReduxFormError = inputProps.meta && (inputProps.meta.submitFailed || inputProps.instantValidate) && inputProps.meta.error;
  const hasHtmlValidErrorMin = inputProps.value && +inputProps.value < +inputProps.min;
  const hasHtmlValidErrorMax = inputProps.value && +inputProps.value > +inputProps.max;
  const hasError = hasReduxFormError || hasHtmlValidErrorMax || hasHtmlValidErrorMin;
  const hasValue = otherInputProps.input && otherInputProps.input.value && otherInputProps.input.value.length > 0;
  let placeholder = (inputProps.placeholder ? inputProps.placeholder : '') + (inputProps.requiredShow && required && !inputProps.value ? ' *' : '');
  return (
    <Tooltip
      visible={hasError}
      placement={inputProps.tooltipPlacement || 'bottomLeft'}
      trigger={['click']}
      overlay={<span>{inputProps.meta && inputProps.meta.error}</span>}
      align={{
        offset: inputProps.tooltipOffset || [0, 8],
      }}
    >
    <InputWrapper style={{ width: inputProps.width ? inputProps.width : '100%' }} isNotIndented={inputProps.isNotIndented}>
        <StyledInput hasError={hasError} hasValue={hasValue} {...otherInputProps} placeholder={placeholder} />
        {/*{ inputProps.required && !inputProps.value && <RequiredStripe /> }*/}
        { inputProps.isDatePicker && <StyledCalendarIcon color="#6C7B91" /> }
    </InputWrapper>
    </Tooltip>
  );
};

// Maybe will move to this component
const CheckboxInput = withStyles(styles)((inputProps) => {
  const { checked, disabled, classes, input, checkboxStyle, wrapperStyles, ...otherAllProps } = inputProps;
  const { meta, label, ...otherInputProps } = otherAllProps;
  return (
    <CheckboxWrapper style={wrapperStyles}>
      <FormControlLabel
        control={<Checkbox
          checked={checked}
          disabled={disabled}
          classes={{
            checked: classes.checked,
            disabled: classes.disabled,
            default: classes.default
          }}
          style={checkboxStyle}
          disableRipple={true}
          checkedIcon={disabled ? <CheckboxDisableIcon/> : <CheckboxIcon />}
          icon={<CheckboxBlankIcon
            //style={styles.checkboxIconStyle}
            //viewBox="74.4 197.7 441.6 446.5"
          />}
        />}
        label={<span className={checked && !disabled ? classes.labelActive: classes.labelDisabled}>{label}</span>}
        classes={{
          label: classes.label,
          root: classes.root,
        }}
        {...input}
        {...otherInputProps}
        value=""
      />
    </CheckboxWrapper>
  );
});

const CheckboxRadioInput = (inputProps) => {
  const { checked, disabled, onChange, name } = inputProps;
  return (
    <CheckboxWrapper>
      <Radio
        checked={checked}
        disabled={disabled}
        name={name}
        onChange={(e, isChecked) => onChange(isChecked)}
        radioClass="iradio_minimal"
        increaseArea="20%"
      />
    </CheckboxWrapper>
  );
};

const RadioInput = withStyles(styles)((inputProps) => {
  const { checked, disabled, input, classes, name,  ...otherProps } = inputProps;
  const { meta, label, ...otherInputProps } = otherProps;
  //console.log(inputProps);
  return <FormControlLabel
    control={<Radio
      classes={{
        checked: classes.checked,
        disabled: classes.disabled,
        default: classes.default
      }}
      disableRipple={true}
      name={name}
      checkedIcon={<RadioIcon
        //style={styles.radioIconStyle}
        //viewBox="2 125.5 593.3 594.3"
      />}
      icon={<RadioBlankIcon
        //style={styles.radioIconStyle}
        //viewBox="-23.2 53.9 26.9 27"
      />}
    />}
    label={<span className={checked && !disabled ? classes.labelActive: classes.labelDisabled}>{label}</span>}
    classes={{
      label: classes.label,
      root: classes.root,
    }}
    {...input}
    {...otherInputProps}
  />
});


const TextArea = (inputProps) => (
  <InputWrapper {...inputProps}>
    <StyledTextArea {...inputProps} />
    { inputProps.required && !inputProps.error && <RequiredStripe /> }
    { inputProps.meta && inputProps.meta.error && inputProps.meta.touched && <ErrorStripe /> }
  </InputWrapper>
);

const SelectArrow = styled(ArrowDown)`
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
  pointer-events: none;
`;

const Select = (inputProps) => {
  const isRequired = inputProps.required && !inputProps.input.value && true;
  const hasError = inputProps.meta.touched && inputProps.meta && inputProps.meta.error && !inputProps.input.value || inputProps.meta.submitFailed && inputProps.meta.error;
  let placeholder = (inputProps.placeholder ? inputProps.placeholder : '') + (inputProps.requiredShow && inputProps.required && !inputProps.value ? ' *' : '');
  const { overrideProps } = inputProps;
  return (
    <InputWrapper style={inputProps.style}>
      <StyledSelect
        {...inputProps}
        placeholder={placeholder}
        value={inputProps.input.value}
        onChange={(e) => {
          inputProps.input.onChange(e && e.value);
        }}
        onBlur={(e) => {
          inputProps.input.onBlur(inputProps.input.value);
        }}
        clearable={inputProps.clearable ? inputProps.clearable : false}
        hasError={hasError}
        isRequired={isRequired}
        searchable={false}
        {...overrideProps}
      />
    </InputWrapper>
  );
};

const SelectWithoutStyledPlaceholder = (inputProps) => (
  <InputWrapper style={inputProps.style}>
    <StyledSelect {...inputProps} style={null} />
    { inputProps.required && !inputProps.value && <RequiredStripe /> }
    { inputProps.meta && inputProps.meta.error && inputProps.value && <ErrorStripe /> }
    {/*<SelectArrow />*/}
  </InputWrapper>
);


const CustomDatePicker = (inputProps) => (
  <DatePicker
    {...inputProps}
    customInput={<Input isDatePicker meta={inputProps.meta}/>}
    dateFormat="DD.MM.YYYY"
  />
);

Input.propTypes = {
  required: React.PropTypes.bool,
};


const Option = styled.div`
  display: flex;
  align-items: center;
  > svg {
    width: 17px;
    height: 17px;
    margin-right: 7px;
    margin-top: -1px;
  }
  > span {
    font-weight: 600;
  }
`;

const Renderer = (option) => (<Option>{option.icon} <span>{option.label}</span></Option>);

const StatusSelect = (props) => {
  const { options, value, onChange, disabled } = props;
  return (
    <StyledSelect
      options={options}
      disabled={disabled}
      value={value}
      onChange={onChange}
      className="select"
      clearable={false}
      searchable={false}
      optionRenderer={Renderer}
      valueRenderer={Renderer}
      arrowRenderer={({ onMouseDown, isOpen }) => !disabled ? <SelectArrowActive/>: <SelectArrowDisabled/>}
      placeholder={<Renderer icon={<MdSync />} label="Update Status" />}
    />
  );
};

export {
  Input,
  TextArea,
  Select,
  SelectWithoutStyledPlaceholder,
  RadioInput,
  CheckboxInput as Checkbox,
  CheckboxRadioInput as RadioCheckbox,
  CustomDatePicker as DatePicker,
  StyledTextArea,
  StatusSelect
};
export { default as ColorInput } from './ColorInput';
export { default as TimePicker } from './TimePicker';
