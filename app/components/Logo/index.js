/**
 *
 * Logo
 *
 */
import FaBeer from 'react-icons/lib/fa/user';
import styled from 'styled-components';

import React, { PropTypes } from 'react';
// import styled from 'styled-components';
import './style.scss';

const IconHuman = styled.div`
  height: 14px;
  width: 12px;
  display:block;
`;

const Line = styled.div`
  display:block;
  height: 1px;
`;
const LineWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  height:10px;

`;


function Logo(color) {
  return (
    <div className="IconLogo" style={{
      backgroundColor: `${color.backgroundColor}`,
      }}>
      <div className="IconLogo--arrowDonw" style={{
      borderColor: `${color.backgroundColor} transparent transparent transparent`,
      }} ></div>
      <IconHuman>
        <FaBeer
          style={{
            display: 'block',
            color: color.color,
            width: '100%',
            height: '100%',
          }}
        />
      </IconHuman>
      <LineWrapper>
        <Line style={{ width: '8px', backgroundColor: color.color }} />
        <Line style={{ width: '5px', backgroundColor: color.color }} />
        <Line style={{ width: '8px', backgroundColor: color.color }} />
      </LineWrapper>
    </div>
  );
}

Logo.propTypes = {
  color: PropTypes.string,
};

export default Logo;
