import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import GoTriangleRight from 'react-icons/lib/fa/caret-right';
import GoTriangleDown from 'react-icons/lib/fa/caret-down';
import styled from 'styled-components';
//import { Checkbox } from 'react-icheck';
import { Checkbox } from "components/Input";
import { CustomColor } from 'components/styles/common';


const Color = styled(CustomColor)`
  margin-right: 15px;
  margin-left: 5px;
`;

const TriangleRight = styled(GoTriangleRight)`
  margin-right: 11px;
`;
const TriangleDown = styled(GoTriangleDown)`
  margin-right: 11px;
`;

const InputWrapperItem = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 4px;
  padding-top: 4px;
  background-color:${props=> props.isChecked ? '#E1E7EB' : 'white'};
  padding-left: 30px;
`;

const Label = styled.label`
  color: #455A64;
  
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
`;

const MedServicesWrapper = styled.div`
  background-color:${props=> props.isChecked ? '#E1E7EB' : 'white'};
`;

class ServiceCategory extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.toggleOpen = this.toggleOpen.bind(this);
    this.state = { open: true };
  }

  toggleOpen() {
    this.setState({ open: !this.state.open });
  }

  render() {
    const {
      category,
      chosenServices,
      checked,
      onChangeServiceCategory,
      onChangeService,
      isAdmin,
      index,
    } = this.props;
    const isAllSubCategoryChecked = chosenServices.length === category.services.length && category.services.length > 0;

    const serviceCheckboxes = category.services ? category.services.map((service, index) => (
      <InputWrapperItem style={{ paddingLeft: '80px' }} key={index} isChecked={chosenServices.indexOf(service._id) > -1}>
        <Checkbox
          checked={chosenServices.indexOf(service._id) > -1}
          onChange={(e, newValue) => { onChangeService(service._id, newValue, category._id)}}
          disabled={!isAdmin}
          checkboxStyle={{ height: '20px', top:'4px' }}
        />
        <Color style={{ backgroundColor: category.color }} />
        <Label>{service.name.defaultName}</Label>
      </InputWrapperItem>
    )) : [];
    return (
      <div key={index}>
        <InputWrapperItem isChecked={checked}>
          {this.state.open && category.services && serviceCheckboxes.length > 0 ? <TriangleDown onClick={this.toggleOpen} /> :
            <TriangleRight onClick={this.toggleOpen} />
          }
          <Checkbox
            checked={checked}
            onChange={(e, newValue) => { onChangeServiceCategory(category._id, newValue)} }
            disabled={!isAdmin}
            checkboxStyle={{ height: '20px', top:'4px' }}
          />
          <Color style={{ backgroundColor: category.color }} />
          <Label>{category.name}</Label>
        </InputWrapperItem>
        {
          this.state.open && category.services && serviceCheckboxes
        }
      </div>);
  }
}


class MedServiceFormWidget extends React.PureComponent {
  render() {
    const {
      serviceCategories,
      chosenServices,
      chosenServiceCategories,
      allServices,
      onChangeService,
      onChangeServiceCategory,
      onChangeAllServices,
      isAdmin
    } = this.props;

    const categoryCheckboxes = serviceCategories.map((category, index) => (
      <ServiceCategory
        category={category}
        chosenServices={chosenServices}
        index={index}
        checked={chosenServiceCategories.indexOf(category._id) > -1}
        onChangeService={onChangeService}
        onChangeServiceCategory={onChangeServiceCategory}
        isAdmin={isAdmin}
      />
    ));

    return (
      <MedServicesWrapper isChecked={allServices}>
        <InputWrapperItem isChecked={allServices}>
          <Checkbox
            checked={allServices}
            onChange={onChangeAllServices}
            disabled={!isAdmin}
            checkboxStyle={{ height: '20px', top:'4px' }}
          />
          <Label>All Services</Label>
        </InputWrapperItem>
        {
          categoryCheckboxes
        }
      </MedServicesWrapper>);
  }
}


export default MedServiceFormWidget;
