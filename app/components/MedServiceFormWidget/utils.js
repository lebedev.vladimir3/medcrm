export const setServiceValue = (state, action) => {
  const { value, id, categoryId } = action.payload;
  const { serviceCategories, chosenServices, chosenServiceCategories } = state;

  const currentCategory = serviceCategories.find(category => category._id === categoryId);

  const categoryLength = currentCategory.services.filter(service=> {
    return chosenServices.includes(service._id);
  }).length;

  if (value) {
    if (categoryLength + 1 === currentCategory.services.length) {
      return { ...state, chosenServices: [...chosenServices, id], chosenServiceCategories: [...chosenServiceCategories, categoryId] };
    } else {
      return {
        ...state,
        chosenServices: [...chosenServices, id],
        chosenServiceCategories: chosenServiceCategories.filter((item)=>item !== categoryId)
      };
    }
  }
  return {
    ...state,
    chosenServices: chosenServices.filter((item) => item !== id),
    chosenServiceCategories: chosenServiceCategories.filter((item)=>item !== categoryId)
  };

};

export const setServiceCategoryValue = (state, action) => {
  const { value, id } = action.payload;
  const { chosenServices, chosenServiceCategories } = state;
  const { serviceCategories } = state;

  const categoryServiceIds = serviceCategories
    .find(category => category._id === id).services.map(service => service._id);

  if (value) {
    return {
      ...state,
      chosenServiceCategories: [...chosenServiceCategories, id],
      chosenServices: [
        ...chosenServices.filter((id) => categoryServiceIds.indexOf(id) < 0),
        ...categoryServiceIds,
      ],
    };
  }
  return {
    ...state,
    chosenServiceCategories: chosenServiceCategories.filter((item) => item !== id),
    chosenServices: chosenServices.filter((id) => categoryServiceIds.indexOf(id) < 0),
  };
};

export const setAllServicesValue = (state, action) => {
  const { serviceCategories } = state;
  const allServices = !state.allServices;

  return {
    ...state,
    allServices,
    chosenServiceCategories: allServices ? serviceCategories.map((item) => item._id) : [],
    chosenServices: allServices
      ? serviceCategories.reduce((acc, val) => [...acc, ...val.services.map((service) => service._id)], [])
      : [],
  };
};
