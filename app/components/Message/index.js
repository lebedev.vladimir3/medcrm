import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import InfoOutline from 'react-icons/lib/md/info-outline';


const MessageWrapper = styled.div`
  height: 50px;
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  padding-left: 20px;
  border: 1px solid #E2CC85;
  border-radius: 4px;
  background-color: #FBF8EA;
  color: #49545A;
  
  font-size: ${fontSetting.baseSize}px;
  line-height: 18px;
`;


const Message = (props) => {
  return (
    <MessageWrapper>
      <InfoOutline style={{ marginRight: '15px' }} size={27} color="#BB9624" />
      <strong>{props.type}:&nbsp;</strong>
      <span>{props.message}</span>
    </MessageWrapper>
  );
};


export default Message;
