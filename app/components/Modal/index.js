import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';


const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(19,24,32,0.3);
  display: flex;
  z-index: 9999;
`;

const ModalWindow = styled(Card)`
  margin: auto;
  height: ${(props) => props.height ? `${props.height}px` : 'auto' };
  width: ${(props) => props.width ? `${props.width}px` : 'auto' };
  
  z-index: 9999;
`;


const Modal = (props) => {
  const { width, height } = props;
  return (
    <Background>
      <ModalWindow width={width} height={height}>
        {props.children}
      </ModalWindow>
    </Background>
  );
};

Modal.propTypes = {
  width: React.PropTypes.number,
  height: React.PropTypes.number,
  children: React.PropTypes.any,
};


export default Modal;
