import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';

const MessageAllowed = styled.div`
  padding: 43px;
  width: 100%;
  display: flex;;
  color: #6A7B92;
  
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
`;

const NotAllowed = (props) =>
  <MessageAllowed>You don't have rights to be here</MessageAllowed>;

export default NotAllowed;
