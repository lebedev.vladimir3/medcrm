import React from 'react';

// import countries from "i18n-iso-countries";
// countries.registerLocale(require("i18n-iso-countries/langs/en.json"));
// countries.registerLocale(require("i18n-iso-countries/langs/de.json"));
// countries.registerLocale(require("i18n-iso-countries/langs/ru.json"));
// console.log(countries.getNames("de"));

import { Field, Fields } from 'redux-form';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea, Select } from 'components/Input';
import DeleteModal from 'components/DeleteModal';
import messages from './messages';
import { honorifics, personal } from './../../constants/users';
import { HonorificParser } from './../../utils';

import { ContentWrap } from 'components/styles/Containers';
import { Card, CardFooter } from 'components/styles/Card';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel, RequiredStar } from 'components/styles/common';
import { SelectDatePicker } from 'components/common/Datepicker';
import renderHelper from 'components/common/renderHelper';
import Obligatory from 'components/common/obligatoryLegend';
import Divider from './../common/Divider';

class PatientForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.onBirthDDUp = this.onBirthDDUp.bind(this);
    this.onBirthDDDown = this.onBirthDDDown.bind(this);
    this.onBirthMMUp = this.onBirthMMUp.bind(this);
    this.onBirthMMDown = this.onBirthMMDown.bind(this);
    this.onBirthYYYYUp = this.onBirthYYYYUp.bind(this);
    this.onBirthYYYYDown = this.onBirthYYYYDown.bind(this);
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  isValidDate(obj) {
    const string = `${obj.yyyy}-${obj.mm}-${obj.dd}`;
    const timestamp = Date.parse(string);
    return !isNaN(timestamp);
  }

  onBirthDDUp() {
    this.props.change('birth.dd', Number(this.props.birth.dd) + 1);
  }

  onBirthYYYYUp() {
    this.props.change('birth.yyyy', Number(this.props.birth.yyyy) + 1);
  }

  onBirthMMUp() {
    this.props.change('birth.mm', Number(this.props.birth.mm) + 1);
  }

  onBirthDDDown() {
    this.props.change('birth.dd', Number(this.props.birth.dd) - 1);
  }

  onBirthMMDown() {
    this.props.change('birth.mm', Number(this.props.birth.mm) - 1);
  }

  onBirthYYYYDown() {
    this.props.change('birth.yyyy', Number(this.props.birth.yyyy) - 1);
  }

  isValidDate(obj) {
    const string = `${obj.yyyy}-${obj.mm}-${obj.dd}`;
    const timestamp = Date.parse(string);
    return !isNaN(timestamp);
  }

  render() {
    const { handleSubmit, payerStatusValue, birth, isEdit } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const angleStyle = { marginTop: '-7px', marginLeft: '2px' };
    return (
      <Card>
        {this.props.onDelete && this.state.showModal &&
        <DeleteModal
          handleDelete={this.props.onDelete}
          handleCancel={this.toggleModal}
          headerText="Delete Patient’s Profile"
          bodyText="Are you sure you want to delete this profile?"
        />}
        <Form onSubmit={handleSubmit}>
          <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 30px`}>
            {!isEdit && <Obligatory />}
            <Divider title={formatMessage(messages.GeneralInfo)} marginTop={0}/>
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.Title)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="25%">
                      <Field
                        name="title.personal"
                        component={renderHelper.selectField}
                        required
                        options={[
                          { value: personal.HERR, label: personal.HERR },
                          { value: personal.FRAU, label: personal.FRAU },
                        ]}
                      />
                    </Col>
                    <Col width="25%">
                      <Field
                        name="title.honorific"
                        component={renderHelper.selectField}
                        //placeholder={formatMessage(messages.ProfessionalTitle)}
                        options={[
                          { value: '', label: '—' },
                          { value: honorifics.DR, label: HonorificParser.toClientSide(honorifics.DR, formatMessage) },
                          { value: honorifics.PROFF, label: HonorificParser.toClientSide(honorifics.PROFF, formatMessage) },
                          { value: honorifics.DR_MED, label: HonorificParser.toClientSide(honorifics.DR_MED, formatMessage) },
                        ]}
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.LastName)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field
                        name="lastName"
                        component={renderHelper.inputField}
                        //placeholder={formatMessage(messages.FirstName)}
                        required
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.FirstName)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field
                        name="firstName"
                        component={renderHelper.inputField}
                        //placeholder={formatMessage(messages.FirstName)}
                        required
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.DateofBirth)}</FormLabel>
                </LabelCol>
                <Col grow="1" padding="0">
                    <Fields
                      names={[
                        'birth.dd',
                        'birth.mm',
                        'birth.yyyy'
                      ]}
                      component={SelectDatePicker}
                      required
                      width="50%"
                      selectProps={[{ width: '80px'}, { width: '80px'}, { width: '94px'}]}
                      //columnProps={[{padding: 4}, {padding: 4}, {padding: 4}]}
                      wrapProps={{ padding: 4}}
                      monthView={isEdit && 'short'}
                    />
                </Col>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.EAddress)}</FormLabel>
                </LabelCol>
                <Col grow="1" padding="0">
                  <FieldsWrapCol width="100%">
                    <Row>
                      <Col width="50%">
                        <Field
                          name="email"
                          type="email"
                          component={renderHelper.inputField}
                          //placeholder={formatMessage(messages.EAddress)}
                        />
                      </Col>
                      <Col width="50%"/>
                    </Row>
                  </FieldsWrapCol>
                </Col>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.PhoneNumber)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field name="phone.main" type="phone" component={renderHelper.inputField}
                             placeholder={formatMessage(messages.MainMobilePhone)} />
                    </Col>
                    <Col width="50%">
                      <Field
                        name="phone.secondary"
                        type="phone"
                        component={renderHelper.inputField}
                        placeholder={formatMessage(messages.SecondMobilePhone)}
                      />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </RowsWrap>
            <Divider width={2} />
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.Address)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="75%">
                      <Field name="address.street" component={renderHelper.inputField} placeholder={formatMessage(messages.Street) + ', ' +
                      formatMessage(messages.HouseNumber)} />
                    </Col>
                    <Col width="25%">
                      <Field name="address.postbox" component={renderHelper.inputField} placeholder={formatMessage(messages.PostBox)} />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol/>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field name="address.city" component={renderHelper.inputField} placeholder={formatMessage(messages.City)} />
                    </Col>
                    <Col width="50%">
                      <Field name="address.country" component={renderHelper.inputField} placeholder={formatMessage(messages.Country)} />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </RowsWrap>
            <Divider title={formatMessage(messages.PaymentInfo)}/>
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.payStatus)}</FormLabel>
                </LabelCol>
                <Col grow="1" padding="0">
                  <FieldsWrapCol width="100%">
                    <Row>
                      <Col width="50%">
                        <Field
                          name="payerStatus"
                          component={renderHelper.selectField}
                          placeholder={formatMessage(messages.payStatusPlaceholder)}
                          required
                          options={[
                            { value: '1', label: '1 - Government Insurance' },
                            { value: '3', label: '3 - Government Insurance' },
                            { value: '5', label: '5 - Government Insurance' },
                            { value: '7', label: '7 - Government Insurance' },
                            { value: 'P', label: 'P - Private Insurance' },
                          ]}
                        />
                      </Col>
                      {payerStatusValue ? (
                        <Col width="50%">
                          <Field
                            name="payerName"
                            component={renderHelper.selectField}
                            placeholder={formatMessage(messages.payerName)}
                            required={this.props.payerStatusValue !== 'P'}
                            options={this.props.payers.map((item) => ({ value: item.name, label: item.name }))}
                          />
                        </Col>
                      ): <Col width="50%"/>}
                    </Row>
                  </FieldsWrapCol>
                </Col>
              </Row>
              {payerStatusValue && (
                <Row center>
                  <LabelCol>
                    <FormLabel required={!isEdit}>{formatMessage(messages.insNumber)}</FormLabel>
                  </LabelCol>
                  <Col grow="1" padding="0">
                    <FieldsWrapCol width="100%">
                      <Row>
                        <Col width="50%">
                          <Field name="insuranceNumber" component={renderHelper.inputField} placeholder={formatMessage(messages.insNumber)} required/>
                        </Col>
                        <Col width="50%"/>
                      </Row>
                    </FieldsWrapCol>
                  </Col>
                </Row>
              )}
            </RowsWrap>
            <Divider title={formatMessage(messages.Notes)}/>
            <RowsWrap>
              <Row center>
                <Col grow="1" padding="0">
                  <Field
                    name="notes"
                    component={renderHelper.textAreaField}
                    placeholder={formatMessage(messages.NotesForInternalUsage)}
                  />
                </Col>
              </Row>
            </RowsWrap>
          </ContentWrap>
          <CardFooter>
            <ContentWrap padding="10px 30px 10px 30px">
              <Row justifyContent="space-between" center noPadding>
                <Col>
                  { this.props.onDelete && <DeleteButton type="button" onClick={this.toggleModal}>
                    {formatMessage(messages.DeleteProfile)}
                  </DeleteButton>}
                </Col>
                <Col>
                  <div>
                    <GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                    <GreenButton type="submit" disabled={this.props.pristine}>
                      { this.props.onDelete ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddPatient)}
                    </GreenButton>
                  </div>
                </Col>
              </Row>
            </ContentWrap>
          </CardFooter>
        </Form>
      </Card>
    );
  }
}

PatientForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

PatientForm.propTypes = {
  handleSubmit: React.PropTypes.func.isRequired,
  onSubmit: React.PropTypes.func,
  onDelete: React.PropTypes.func,
  pristine: React.PropTypes.bool.isRequired,
  onCancel: React.PropTypes.func,
  birth: React.PropTypes.object,
  locale: React.PropTypes.string,
};


export { default as validate } from './validate';
export default PatientForm;

