/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  mr: {
    id: 'Mr',
    defaultMessage: 'Mr',
  },
  mrs: {
    id: 'Mrs',
    defaultMessage: 'Mrs',
  },
  ms: {
    id: 'PatientForm.parsonalTitle.ms',
    defaultMessage: 'Ms',
  },
  insuranceNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  Country: {
    id: 'Country',
    defaultMessage: 'Country',
  },
  Region: {
    id: 'Region',
    defaultMessage: 'Region',
  },
  Street: {
    id: 'Street',
    defaultMessage: 'Street',
  },
  HouseNumber: {
    id: 'House number',
    defaultMessage: 'House number',
  },
  City: {
    id: 'City',
    defaultMessage: 'City',
  },
  PostBox: {
    id: 'PostBox',
    defaultMessage: 'PostBox',
  },
  NotesForInternalUsage: {
    id: 'NotesForInternalUsage',
    defaultMessage: 'Notes for internal usage',
  },
  payStatus: {
    id: 'payStatus',
    defaultMessage: 'Payer',
  },
  payStatusPlaceholder: {
    id: 'payStatus.Placeholder',
    defaultMessage: 'Select payer status'
  },
  payerName: {
    id: 'PayerName.Placeholder',
    defaultMessage: 'Name',
  },
  insNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  PaymentInfo: {
    id: 'PaymentInfo',
    defaultMessage: 'Payment Info',
  },
  Notes: {
    id: 'Notes',
    defaultMessage: 'Notes',
  },
  MainMobilePhone: {
    id: 'MainMobilePhone',
    defaultMessage: 'Main mobile phone',
  },
  SecondMobilePhone: {
    id: 'SecondMobilePhone',
    defaultMessage: 'Secondary mobile phone',
  },
  PhoneNumber: {
    id: 'PhoneNumber',
    defaultMessage: 'Phone number',
  },
  EAddress: {
    id: 'EAddress',
    defaultMessage: 'E-Mail address',
  },
  DateofBirth: {
    id: 'DateofBirth',
    defaultMessage: 'Date of birth',
  },
  LastName: {
    id: 'LastName',
    defaultMessage: 'Last name',
  },
  FirstName: {
    id: 'FirstName',
    defaultMessage: 'First name',
  },
  Name: {
    id: 'Name',
    defaultMessage: 'Name',
  },
  ProfessionalTitle: {
    id: 'ProfessionalTitle',
    defaultMessage: 'Professional title',
  },
  PersonalTitle: {
    id: 'PersonalTitle',
    defaultMessage: 'Personal title',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  GeneralInfo: {
    id: 'GeneralInfo',
    defaultMessage: 'General Info',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  AddPatient: {
    id: 'AddPatient',
    defaultMessage: 'Add Patient',
  },
  DeleteProfile: {
    id: 'EditPatientsPage.DeleteProfile',
    defaultMessage: 'Delete Profile',
  },
  Address: {
    id: 'Address',
    defaultMessage: 'Address',
  }
});
