import moment from 'moment';


const validate = values => {
  const errors = {};
  const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
  const phonePattern = /^[\d \+\-]+$/;
  const requiredMessage = 'It\'s required field';

  if (values.email && !emailPattern.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (values.personalEmail && !emailPattern.test(values.personalEmail)) {
    errors.personalEmail = 'Invalid email address';
  }

  if (values.birth) {
    const { dd, mm, yyyy } = values.birth;

    errors.birth = {};
    if (!dd) {
      errors.birth.dd = requiredMessage;
    }
    if (!mm) {
      errors.birth.mm = requiredMessage;
    }
    if (!yyyy) {
      errors.birth.yyyy = requiredMessage;
    }
  } else {
    errors.birth = {};
    errors.birth.dd = requiredMessage;
    errors.birth.mm = requiredMessage;
    errors.birth.yyyy = requiredMessage;
  }
  if(errors.birth && Object.keys(errors.birth).length === 0){
    delete errors.birth;
  }

  if (values.phone) {
    errors.phone = {};

    if (values.phone.work && !phonePattern.test(values.phone.work)) {
      errors.phone.work = 'Invalid phone number';
    }

    if (values.phone.personal && !phonePattern.test(values.phone.personal)) {
      errors.phone.personal = 'Invalid phone number';
    }
  }

  if (values.phone) {
    errors.phone = {};

    if (values.phone.main && !phonePattern.test(values.phone.main)) {
      errors.phone.main = 'Invalid phone number';
    }

    if (values.phone.secondary && !phonePattern.test(values.phone.secondary)) {
      errors.phone.secondary = 'Invalid phone number';
    }
  }

  if(!values.payerStatus){
    errors.payerStatus = "It's required field"
  }

  if (!values.firstName) {
    errors.firstName = 'Required';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  }


  return errors;
};


export default validate;
