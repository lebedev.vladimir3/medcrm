//@flow
import fontSetting from 'components/styles/font';
import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { initialize, reset, Field } from 'redux-form';

import { Card, CardFooterCommon } from 'components/styles/Card';
import messages from './../messages';

import renderHelper from 'components/common/renderHelper';
import { HeaderPanel, HeaderItem, SecondaryHeaderPanel } from 'components/common/HeaderPanel';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { ContentWrap, PrescriptionCategory as Category } from "components/styles/Containers";
import { Row, Col, LabelCol, Form, FieldsWrapCol, BorderRightCol } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';

type Props = {
}

class NewPrescriptionPhysiotherapiePage extends React.Component<Props> {
  static defaultProps = {};
  renderTitle(){
    return (
      <div>
        <strong>New Prescription / </strong>
        <span>Physiotherapie</span>
      </div>
    );
  }
  renderPayerInfo(){
    const options = Array(10).fill(null).map((x, i) => ({ value: i + 1, label: i + 1}));
    return (
      <Category title="Kostenträger">
        <FieldsWrapCol padding={[0, 30]}>
          <Row alignItems="stretch">
            <BorderRightCol width="22%">
              <Row center paddingBottom="18px">
                <Col>
                  <Field
                    name="payerType"
                    component={renderHelper.radioField}
                    type="radio"
                    value="payment"
                    label='Gebührpflichtig'
                  />
                </Col>
              </Row>
              <Row center paddingBottom="0px">
                <Col>
                  <Field
                    name="payerType"
                    component={renderHelper.radioField}
                    type="radio"
                    value="free"
                    label='Gebührfrei'
                  />
                </Col>
              </Row>
            </BorderRightCol>
            <BorderRightCol width="28%">
              <Row center paddingBottom="18px">
                <Col>
                  <Field
                    name="accident"
                    component={renderHelper.checkboxField}
                    type="checkbox"
                    label='Unfall'
                  />
                </Col>
              </Row>
              <Row center paddingBottom="0px">
                <Col>
                  <Field
                    name="BVG"
                    component={renderHelper.checkboxField}
                    type="checkbox"
                    label='BVG'
                  />
                </Col>
              </Row>
            </BorderRightCol>
            <Col width="50%" middle alignItems="flex-end">
              <Col width="272px">
                <span>Krankenkasse bzw. Kostenträger</span>
                <Field
                  name="payerName"
                  component={renderHelper.selectField}
                  //placeholder={formatMessage(messages.CapacityPlaceholder)}
                  options={options}
                  value={"1"}
                />
              </Col>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Category>
    );
  }
  renderDoctorInfo(){
    return (
      <Category title="Arzt">
        <FieldsWrapCol padding={[0, 30]}>
          <Row alignItems="stretch">
            <Col width="22%" style={{ paddingRight: 10 }}>
              <Row
                center
                justifyContent="flex-end"
              >
                <span>
                  Betriebstätten-Nr.
                </span>
              </Row>
              <Row
                center
                justifyContent="flex-end"
              >
                <span>
                  Arzt-Nr.
                </span>
              </Row>
              <Row
                center
                justifyContent="flex-end"
                textRight
                style={{ flexGrow: '0' }}
              >
                <span>
                  Arztunterschrift
                  vorhanden
                </span>
              </Row>
            </Col>
            <BorderRightCol width="28%">
              <Row
                width="132px"
              >
                <Field
                  name="bsnr"
                  component={renderHelper.inputField}
                  disabled
                  //placeholder={formatMessage(messages.DescriptionPlaceholder)}
                />
              </Row>
              <Row
                width="132px"
              >
                {React.createElement(renderHelper.inputField)}
              </Row>
              <Row
                width="132px"
              >
                <Col width="48%">
                  {React.createElement(renderHelper.radioField, {
                    label: 'Ja',
                    name: 'test1',
                  })}
                </Col>
                <Col width="52%">
                  {React.createElement(renderHelper.radioField, {
                    label: 'Nein',
                    name: 'test1',
                  })}
                </Col>
              </Row>
            </BorderRightCol>
            <Col width="50%">
              <Row center>
                <LabelCol width="33.333%" textRight>
                      <span>
                        Anrede
                      </span>
                </LabelCol>
                <FieldsWrapCol width="66.666%">
                  <Row>
                    <Col width="50%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                    <Col width="50%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol width="33.333%" textRight>
                      <span>
                        Nachname
                      </span>
                </LabelCol>
                <FieldsWrapCol width="66.666%">
                  <Row>
                    <Col width="100%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol width="33.333%" textRight>
                        <span>
                          Vorname
                        </span>
                </LabelCol>
                <FieldsWrapCol width="66.666%">
                  <Row>
                    <Col width="100%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Category>
    );
  }
  renderRegulationInfo(){
    return (
      <Category title="Verordnung">
        <FieldsWrapCol padding={[0, 30]}>
          <Row alignItems="stretch">
            <BorderRightCol width="22%">
              <Row center>
                <Col>
                  <span>Rezeptdatum</span>
                  {React.createElement(renderHelper.datePickerField, { placeholder: '' })}
                </Col>
              </Row>
              <Row center>
                <Col>
                  <span>Beginn Spätestens</span>
                  {React.createElement(renderHelper.datePickerField, { placeholder: '' })}
                </Col>
              </Row>
            </BorderRightCol>
            <BorderRightCol width="28%">
              <Row center>
                {React.createElement(renderHelper.radioField, {
                  label: 'Erst-verordnung',
                  name: 'test1',
                })}
              </Row>
              <Row center>
                {React.createElement(renderHelper.radioField, {
                  label: 'Folge-verordnung',
                  name: 'test1',
                })}
              </Row>
              <Row center>
                {React.createElement(renderHelper.radioField, {
                  label: 'Außerhalb des Regelfalls',
                  name: 'test1',
                })}
              </Row>
            </BorderRightCol>
            <Col width="50%">
              <Row center paddingBottom="18px">
                <LabelCol width="63%" textRight>
                      <span>
                        Gruppentherapie
                      </span>
                </LabelCol>
                <FieldsWrapCol width="37%">
                  <Row>
                    <Col width="40%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Ja',
                        name: 'test1',
                      })}
                    </Col>
                    <Col width="60%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Nein',
                        name: 'test1',
                      })}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center paddingBottom="18px">
                <LabelCol width="67%" textRight>
                      <span>
                        Hausbesuch
                      </span>
                </LabelCol>
                <FieldsWrapCol width="37%">
                  <Row>
                    <Col width="40%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Ja',
                        name: 'test1',
                      })}
                    </Col>
                    <Col width="60%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Nein',
                        name: 'test1',
                      })}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center paddingBottom="18px">
                <LabelCol width="63%" textRight>
                      <span>
                        Therapiebericht
                      </span>
                </LabelCol>
                <FieldsWrapCol width="37%">
                  <Row>
                    <Col width="40%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Ja',
                        name: 'test1',
                      })}
                    </Col>
                    <Col width="60%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Nein',
                        name: 'test1',
                      })}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center paddingBottom="0px">
                <LabelCol width="63%" textRight>
                      <span>
                        Paralleler Regelfall
                      </span>
                </LabelCol>
                <FieldsWrapCol width="37%">
                  <Row>
                    <Col width="40%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Ja',
                        name: 'test1',
                      })}
                    </Col>
                    <Col width="60%">
                      {React.createElement(renderHelper.radioField, {
                        label: 'Nein',
                        name: 'test1',
                      })}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Category>
    );
  }
  renderDiagnosisInfo(){
    return (
      <Category title="Diagnose">
        <Row>
          <FieldsWrapCol padding={[0, 5]}>
            <Row>
              <Col width="36%"/>
              <Col width="64%">
                {React.createElement(renderHelper.checkboxField, {
                  label: 'Diagnose manuell eingeben bzw. ändern',
                  name: 'test1',
                })}
              </Col>
            </Row>
          </FieldsWrapCol>
        </Row>
        <Row>
          <FieldsWrapCol padding={[0, 5]}>
            <Row alignItems="stretch">
              <Col width="36%">
                <FieldsWrapCol padding={[0, 18]} paddingBottom="13px">
                  <Row center>
                    <Col width="54%" textRight>
                      Indik. schlüssel
                    </Col>
                    <Col width="46%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                  <Row center>
                    <Col width="54%" textRight>
                      1. ICD-10 - Code
                    </Col>
                    <Col width="46%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                  <Row center style={{ paddingBottom: 0 }}>
                    <Col width="54%" textRight>
                      2. ICD-10 - Code
                    </Col>
                    <Col width="46%">
                      {React.createElement(renderHelper.inputField)}
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Col>
              <Col width="64%">
                {React.createElement(renderHelper.textAreaField, { height: '100%' })}
              </Col>
            </Row>
          </FieldsWrapCol>
        </Row>
      </Category>
    );
  }
  renderServicesInfo(){
    return (
      <Category title="Heilmittel nach Maßgabe des Kataloges">
        <Row paddingBottom="4px">
          <FieldsWrapCol padding={[0, 5]}>
            <Row>
              <Col width="67px">
                Menge
              </Col>
              <Col width="312px">
                Heilmittel
              </Col>
              <Col width="102px">
                Kurzzeichen
              </Col>
              <Col width="92px">
                Dauer, min
              </Col>
              <Col grow="1" textCenter>
                Anzahl pro Woche
              </Col>
            </Row>
          </FieldsWrapCol>
        </Row>
        <Row center>
          <FieldsWrapCol padding={[0, 5]}>
            <Row center>
              <Col width="67px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="312px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="102px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="92px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="72px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="31px">
                bis
              </Col>
              <Col width="72px">
                {React.createElement(renderHelper.inputField)}
              </Col>
            </Row>
          </FieldsWrapCol>
        </Row>
        <Row center>
          <FieldsWrapCol padding={[0, 5]}>
            <Row center>
              <Col width="67px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="312px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="102px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="92px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="72px">
                {React.createElement(renderHelper.inputField)}
              </Col>
              <Col width="31px">
                bis
              </Col>
              <Col width="72px">
                {React.createElement(renderHelper.inputField)}
              </Col>
            </Row>
          </FieldsWrapCol>
        </Row>
      </Category>
    );
  }
  renderNote(){
    return (
      <Category title="Medizinische Begründung">
        <Row center>
          <Col grow="1" padding="0">
            {React.createElement(renderHelper.textAreaField)}
          </Col>
        </Row>
      </Category>
    );
  }

  render() {
    //const { intl: { formatMessage } } = this.props;
    return (
      <Form>
        <SecondaryHeaderPanel
          brandTitle={this.renderTitle()}
          headerProps={{
            fontWeight: 500
          }}
          noneBg
        />
        <ContentWrap padding="20px 30px 10px 15px">
          {this.renderPayerInfo()}
          {this.renderDoctorInfo()}
          {this.renderRegulationInfo()}
          {this.renderDiagnosisInfo()}
          {this.renderServicesInfo()}
          {this.renderNote()}
        </ContentWrap>
        <CardFooterCommon
          cancelText="Cancel"
          applyText="Add Prescription"
          onDelete={false}
          onCancel={false}
          pristine={false}
        />
      </Form>
    );
  }
}

NewPrescriptionPhysiotherapiePage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default NewPrescriptionPhysiotherapiePage;
