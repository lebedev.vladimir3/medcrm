// Deprecated component
import fontSetting from 'components/styles/font';
import React from 'react';
import { Field } from 'redux-form';
import styled from 'styled-components';
import { CardFooter } from 'components/styles/Card';
import { Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import renderHelper from 'components/common/renderHelper';

import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea, Select, Checkbox as CheckboxInput } from 'components/Input';
import DatePicker from 'react-datepicker';
import CalendarIcon from 'react-icons/lib/fa/calendar';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

// import FormGroup from './FormGroup';
// import Label from './Label';
// import LabelColumn from './LabelColumn';
// import InputColumn from './InputColumn';
// import HalfColumn from './HalfColumn';
import UserSearch from './components/SearchDoctor';
import messages from './messages';

import { FieldsWrap } from 'components/styles/Forms';
import { ContentWrap } from "components/styles/Containers";

const renderInputField = (inputProps) => <Input {...inputProps.input} {...inputProps} />;
const renderInputShort = (inputProps) => <Input {...inputProps.input} {...inputProps} width="170px" />;
const renderTextAreaField = (inputProps) => <TextArea {...inputProps.input} {...inputProps} />;
const renderSelectField = (renderProps) => <Select {...renderProps.input} {...renderProps} />;
const renderCheckboxField = (inputProps) => <CheckboxInput {...inputProps.input} {...inputProps} />;

const CheckboxOuter = styled.div`
  width: 100%;
  padding: 5px 10px;
`;

const LabelCalendar = styled.div`
  width: 90px;
  margin-left:10px;
  padding-top: 8px;
  opacity: 0.9;
  color: #7E7E7E;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
`;

const CalendarWrapper = styled.div`
  border: 1px solid #BDBDBD;
  border-radius: 4px;
  height: 36px;
  width: 170px;
  background-color: #F8FAFC;
  display: flex;
  justify-content: space-between;
  padding: 8px 5px;

`;

const CalendarText = styled.div`
  opacity: 0.9;
  color: #7E7E7E;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  margin-right: 15px;
`;

class CustomIssueInput extends React.Component {
  render() {
    return (
      <CalendarWrapper onClick={this.props.onClick} style={{ cursor: 'pointer' }}>
        <CalendarText style={{ 'color': this.props.value ? '#455A64' : '#8D8D8D' }}>
          {this.props.value ? this.props.value : 'DD.MM.YYYY' }
        </CalendarText>
        <CalendarIcon color="#6C7B91" />
      </CalendarWrapper>
    );
  }
};

const renderDatePicker = (props) => (
  <DatePicker
    selected={props.input.value ? moment(props.input.value) : null}
    onChange={props.input.onChange}
    placeholderText="DD.MM.YYYY"
    dateFormat="DD.MM.YYYY"
    style={{ width: '100px' }}
    customInput={<CustomIssueInput />}
  />
);


class PrescriptionCardForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedInnerTab: 1,
      updateOnce: false,
    };
    this.userChose = this.userChose.bind(this);
  }

  componentDidUpdate() {
    const {
      change,
    } = this.props;
    if (!this.state.updateOnce) {
      change('payerName', this.props.currentPatient.payerName);
      change('patient', this.props.currentPatient._id);
      this.setState({ updateOnce: true });
    }
    if (this.props.payerName !== 'SZ' && this.props.currentPatient.payerStatus !== 'P') {
      change('regulationType', 'not available');
    }
    if (this.props.changePayer && this.props.payerName === 'SZ') {
      this.props.changePayer('SZ');
    }
    if (this.props.changePayer && this.props.payerName !== 'SZ') {
      this.props.changePayer(this.props.currentPatient.payerStatus);
    }
  }

  userChose(user) {
    const {
      change,
    } = this.props;
    if (user.title && user.title.personal) {
      change('title.personal', user.title.personal);
    }
    if (user.title && user.title.honorific) {
      change('title.honorific', user.title.honorific);
    }
    if (user.lastName) {
      change('doctorNameLast', user.lastName);
    }
    if (user.firstName) {
      change('doctorNameFirst', user.firstName);
    }
    change('staff', user._id);
  }

  render() {
    const submitter = this.props.handleSubmit(this.props.onSubmit);
    const {
      isAdmin,
    } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const payerNameOptions = [
      {
        value: this.props.currentPatient.payerName,
        label: this.props.currentPatient.payerName
      }
    ];
    if (this.props.currentPatient.payerStatus !== 'P') {
      payerNameOptions.push({
        value: "SZ",
        label: "SZ - Selbstzahler"
      })
    }
    return (
      <Form onSubmit={this.props.handleSubmit}>
        In development
        {/*<ContentWrap>*/}
          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.payerName)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<Field*/}
                {/*name="payerName"*/}
                {/*component={renderSelectField}*/}
                {/*placeholder={formatMessage(messages.payerName)}*/}
                {/*value="qwe"*/}
                {/*options={payerNameOptions}*/}
              {/*/>*/}
              {/*<CheckboxOuter>*/}
                {/*<Field*/}
                  {/*name="isBefreit"*/}
                  {/*type="checkbox"*/}
                  {/*component={renderCheckboxField}*/}
                  {/*label="Befreit"*/}
                {/*/>*/}
              {/*</CheckboxOuter>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}
          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.Date)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<div*/}
              {/*style={{*/}
                {/*width: '550px',*/}
                {/*display: 'flex',*/}
                {/*flexDirection: 'column',*/}
              {/*}}*/}
            {/*>*/}
              {/*<InputColumn style={{ marginBottom: '15px' }}>*/}
                {/*<LabelCalendar>{formatMessage(messages.Issued)}</LabelCalendar>*/}
                {/*<Field*/}
                  {/*name="issuedDate"*/}
                  {/*component={renderDatePicker}*/}
                {/*/>*/}
              {/*</InputColumn>*/}
              {/*<InputColumn>*/}
                {/*<LabelCalendar>{formatMessage(messages.MaxStart)}</LabelCalendar>*/}
                {/*<Field*/}
                  {/*name="maxStartDate"*/}
                  {/*component={renderDatePicker}*/}
                {/*/>*/}
              {/*</InputColumn>*/}
            {/*</div>*/}
          {/*</FormGroup>*/}
          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.DoctorTitle)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<Field*/}
                {/*name="title.personal"*/}
                {/*component={renderSelectField}*/}
                {/*placeholder={formatMessage(messages.PersonalTitle)}*/}
                {/*options={[*/}
                  {/*{ value: 'mr', label: formatMessage(messages.Mr) },*/}
                  {/*{ value: 'mrs', label: formatMessage(messages.Mrs) },*/}
                  {/*{ value: 'ms', label: formatMessage(messages.Ms) },*/}
                {/*]}*/}
                {/*disabled*/}
              {/*/>*/}
              {/*<Field*/}
                {/*name="title.honorific"*/}
                {/*component={renderSelectField}*/}
                {/*placeholder={formatMessage(messages.HonorificTitle)}*/}
                {/*options={[*/}
                  {/*{ value: 'first', label: 'First' },*/}
                  {/*{ value: 'second', label: 'Second' },*/}
                {/*]}*/}
                {/*disabled*/}
              {/*/>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}
          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.DoctorName)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<div style={{ width: '100%' }}>*/}
                {/*<UserSearch*/}
                  {/*users={this.props.users}*/}
                  {/*dispatch={this.props.dispatch}*/}
                  {/*userChose={this.userChose}*/}
                  {/*inputValue={this.props.doctorNameLast ? this.props.doctorNameLast : ''}*/}
                {/*/>*/}
              {/*</div>*/}
              {/*<Field*/}
                {/*name="doctorNameFirst"*/}
                {/*component={renderInputField}*/}
                {/*placeholder={formatMessage(messages.FirstName)}*/}
                {/*disabled*/}
              {/*/>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.ClinicCodes)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn style={{marginLeft:'-20px'}}>*/}
              {/*<LabelCalendar style={{width:'96px'}}>BSNR</LabelCalendar>*/}
              {/*<Field name="bsnr" component={renderInputShort} placeholder="BSNR code" />*/}
              {/*<LabelCalendar style={{width:'96px'}}>LANR </LabelCalendar>*/}
              {/*<Field name="lanr" component={renderInputShort} placeholder="LANR code" />*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>ICD Codes</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn style={{marginLeft:'-20px'}}>*/}
              {/*<LabelCalendar style={{width:'96px'}}>1. ICD-10</LabelCalendar>*/}
              {/*<Field name="icd1" component={renderInputShort} placeholder="1. ICD-10 code" />*/}
              {/*<LabelCalendar style={{width:'96px'}}>2. ICD-10</LabelCalendar>*/}
              {/*<Field name="icd2" component={renderInputShort} placeholder="2. ICD-10 code" />*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.RegulationType)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<HalfColumn>*/}
                {/*<Field*/}
                  {/*name="regulationType"*/}
                  {/*component={renderSelectField}*/}
                  {/*placeholder="Regulation type"*/}
                  {/*options={[*/}
                    {/*{ value: "X - Regulation a. R.", label: 'X - Regulation a. R.' },*/}
                  {/*]}*/}
                {/*/>*/}
              {/*</HalfColumn>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.IndicationCode)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<HalfColumn>*/}
                {/*<Field name="indicationCode" component={renderInputField} placeholder={formatMessage(messages.IndicationCode)} />*/}
              {/*</HalfColumn>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.SpecialActivity)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn style={{paddingTop:'5px', alignItems:'center', marginLeft: '5px'}}>*/}
              {/*<Field*/}
                {/*name="isHouseVisit"*/}
                {/*type="checkbox"*/}
                {/*component={renderCheckboxField}*/}
                {/*label={formatMessage(messages.HouseVisit)}*/}
              {/*/>*/}
              {/*<Field*/}
                {/*name="isGroup"*/}
                {/*type="checkbox"*/}
                {/*component={renderCheckboxField}*/}
                {/*label={formatMessage(messages.Group)}*/}
              {/*/>*/}
              {/*<Field*/}
                {/*name="isReport"*/}
                {/*type="checkbox"*/}
                {/*component={renderCheckboxField}*/}
                {/*label={formatMessage(messages.Report)}*/}
              {/*/>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.Diagnosis)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<Field*/}
                {/*name="diagnosis"*/}
                {/*component={renderTextAreaField}*/}
                {/*placeholder={formatMessage(messages.Diagnosis)}*/}
              {/*/>*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.Taxation)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<HalfColumn>*/}
                {/*{(this.props.payerName && this.props.payerName !== 'SZ' && this.props.currentPatient.payerStatus !== 'P') ?*/}
                  {/*<Field*/}
                    {/*name="taxationType"*/}
                    {/*component={renderSelectField}*/}
                    {/*placeholder="Taxation type"*/}
                    {/*options={[*/}
                      {/*{ value: "Template 13 - Phisiotherapi", label: 'Template 13 - Phisiotherapie' },*/}
                    {/*]}*/}
                  {/*/>*/}
                  {/*:*/}
                  {/*<Field name="taxationType" component={renderInputField} placeholder={formatMessage(messages.TaxationType)}*/}
                         {/*disabled={true}>*/}
                  {/*</Field>*/}
                {/*}*/}

              {/*</HalfColumn>*/}

            {/*</InputColumn>*/}
          {/*</FormGroup>*/}

          {/*<FormGroup>*/}
            {/*<LabelColumn>*/}
              {/*<Label>{formatMessage(messages.Notes)}</Label>*/}
            {/*</LabelColumn>*/}
            {/*<InputColumn>*/}
              {/*<Field name="notes" component={renderTextAreaField} placeholder={formatMessage(messages.NotesForInternalUsage)} />*/}
            {/*</InputColumn>*/}
          {/*</FormGroup>*/}
        {/*</ContentWrap>*/}
        {/*<CardFooter>*/}
          {/*<ContentWrap padding="10px 30px 10px 30px">*/}
            {/*<Row justifyContent="space-between" center noPadding>*/}
              {/*<Col>*/}
                {/*{ this.state.selectedTab === 2 &&*/}
                {/*<GreyButton onClick={()=>this.onTabClick(1)}>{formatMessage(messages.GoBack)}</GreyButton>*/}
                {/*}*/}
                {/*{ this.props.onDelete && <DeleteButton type="button" disabled={!isAdmin} onClick={this.props.onDelete}>*/}
                  {/*{formatMessage(messages.CancelPrescription)}*/}
                {/*</DeleteButton> }*/}
              {/*</Col>*/}
              {/*<Col>*/}
                {/*<div>*/}
                  {/*<GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>*/}
                  {/*<GreenButton*/}
                    {/*type="button"*/}
                    {/*disabled={this.props.pristine}*/}
                    {/*onClick={this.props.isEdit ? submitter : (this.props.selectedTab === 2 ? submitter : () => this.props.onTabClick(2))}*/}
                  {/*>*/}
                    {/*{ this.props.isEdit ? formatMessage(messages.SaveChanges) : formatMessage(messages.NextStep)}*/}
                  {/*</GreenButton>*/}
                {/*</div>*/}
              {/*</Col>*/}
            {/*</Row>*/}
          {/*</ContentWrap>*/}
        {/*</CardFooter>*/}
      </Form>
    );
  }
}

PrescriptionCardForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

PrescriptionCardForm.propTypes = {
  onDelete: React.PropTypes.func,
  onCancel: React.PropTypes.func,
  pristine: React.PropTypes.bool.isRequired,
  change: React.PropTypes.func.isRequired,
  isEdit: React.PropTypes.bool,
  handleSubmit: React.PropTypes.func,
  currentPatient: React.PropTypes.object.isRequired,
};


export { default as validate } from './validate';
export default PrescriptionCardForm;
