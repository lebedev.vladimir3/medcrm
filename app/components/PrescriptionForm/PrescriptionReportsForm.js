import React, { PropTypes } from 'react';
import { Field, FieldArray } from 'redux-form';
import { Link } from 'react-router';
import styled from 'styled-components';
import { CardFooter } from 'components/styles/Card';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { TextArea, StyledTextArea } from 'components/Input';
import moment from 'moment';
import messages from './messages';

import { ContentWrap } from "components/styles/Containers";
import { Row, Col } from "components/styles/Grid";
import { JustifyBetween, FlexEnd } from "components/styles/common";
import Divider from 'components/common/Divider';
import {
  EntityWrap as ReportWrap,
  EntityHeader as ReportHeader,
  EntityContent,
  EntityCounter as ReportCounter,
  EntityLabel as ReportLabel,
} from 'components/styles/Forms';

import { genderParser } from './../../utils';
import renderHelper from 'components/common/renderHelper';
import { EMPTY_STRING }from 'constants/fillers';
import { Tag } from 'components/styles/common'
import { FormHeaderTab } from 'components/common/Tabs';
import theme from 'components/styles/colorScheme';

const ReportContent = styled(EntityContent)`
  & > span, strong {
    opacity: 0.9;
    color: #242424;
  }
`;

const StyledLink = styled(Link)`
  color: ${theme.Tab.FormHeader.color};
  text-decoration: none;
`;

const renderReports = (props, context) => (
  <div>
    {props.fields.map((item, index, fields) => {
      const { _id, id, staff, date, service, createdAt, updatedAt } = fields.get(index);
      const updateIsEqualCreate = moment(createdAt).diff(moment(updatedAt)) === 0;
      return (
        <ReportWrap key={index}>
          <FormHeaderTab>
            <StyledLink to={`/main/patients/${props.patientId}/appointments/${_id}`}>
              <span style={{ marginRight: 5 }}>{context.intl.formatMessage(messages.AppointmentID)}</span>
              <span>{props.patientReadableId}-{props.prescriptionId}-{id}</span>
            </StyledLink>
          </FormHeaderTab>
          <ReportContent>
            <Row justifyContent="space-between">
              <span>
                <ReportLabel>{context.intl.formatMessage(messages.Service)}</ReportLabel>
                <strong>{service ? [
                  <span key="0" style={{ marginRight: '6px' }}>{service.name.longName}</span>,
                  <span key="1">{"( " + service.name.shortName + " )"}</span>
                ]: EMPTY_STRING}
                </strong>
              </span>
              <span>
                <ReportLabel>{context.intl.formatMessage(messages.ServiceProvided)}</ReportLabel>
                <strong>{moment(date).format('DD.MM.YYYY, hh:mm')}</strong>
              </span>
            </Row>
            <Field
              name={`appointments.${index}.treatmentReport`}
              component={renderHelper.textAreaField}
              placeholder={context.intl.formatMessage(messages.Notes)}
              disabled={staff && props.userId !== staff._id}
            />
            <Row justifyContent="space-between" style={{ paddingBottom: 0 }}>
              <span>
                <ReportLabel>{context.intl.formatMessage(messages.Provider)}</ReportLabel>
                {staff ? (
                  <span>
                    <strong> {staff.lastName},</strong>
                    <span> {staff.firstName} {genderParser(staff.title.personal)}</span>
                  </span>
                ): <span> {EMPTY_STRING} </span>}
              </span>
              <span>
                <span>
                  <ReportLabel>{context.intl.formatMessage(messages.ReportAdded)}</ReportLabel>
                  <strong>{moment(createdAt).format('DD.MM.YYYY, hh:mm')}</strong>
                </span>
                {
                  !updateIsEqualCreate && (
                    <span style={{ marginLeft: '25px'}}>
                      <ReportLabel>{context.intl.formatMessage(messages.ReportUpdated)}</ReportLabel>
                      <strong>{moment(updatedAt).format('DD.MM.YYYY, hh:mm')}</strong>
                    </span>
                  )
                }
              </span>
            </Row>

          </ReportContent>
        </ReportWrap>
      );
    })}
  </div>
);

renderReports.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
renderReports.propTypes = {
  fields: PropTypes.object,
  userId: PropTypes.string,
};

const PrescriptionReportsForm = (props, context) => (
  <form onSubmit={props.handleSubmit}>
    <ContentWrap padding="20px 16px 40px 16px">
        <FieldArray
          name="appointments"
          component={renderReports}
          patientReadableId={props.currentPatient.id}
          patientId={props.currentPatient._id}
          prescriptionId={props.prescription.id}
          userId={props.currentUser._id}
        />
    </ContentWrap>
    <CardFooter>
      <ContentWrap padding="10px 30px 10px 30px">
        <Row justifyContent="space-between" center noPadding>
          <Col>
            { props.onDelete &&
            <DeleteButton type="button" onClick={props.onDelete} disabled={!props.isAdmin}>
              {context.intl.formatMessage(messages.CancelPrescription)}
            </DeleteButton>
            }
          </Col>
          <Col>
            <div>
              <GreyButton onClick={props.onCancel}>{context.intl.formatMessage(messages.Cancel)}</GreyButton>
              <GreenButton type="submit">{context.intl.formatMessage(messages.SaveChanges)}</GreenButton>
            </div>
          </Col>
        </Row>
      </ContentWrap>
    </CardFooter>
  </form>
);
PrescriptionReportsForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

PrescriptionReportsForm.propTypes = {
  onDelete: PropTypes.func,
  onCancel: PropTypes.func,
  handleSubmit: PropTypes.func,
  currentPatient: PropTypes.object,
  prescription: PropTypes.object,
  currentUser: PropTypes.object,
  isAdmin: PropTypes.bool,
};


export default PrescriptionReportsForm;
