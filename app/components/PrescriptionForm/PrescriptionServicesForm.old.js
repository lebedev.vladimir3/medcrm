import fontSetting from 'components/styles/font';
import React from 'react';
import { Field, FieldArray } from 'redux-form';
import styled from 'styled-components';
import { CardFooter } from 'components/styles/Card';
import { Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, Select, Checkbox as CheckboxInput } from 'components/Input';
import Trash from 'react-icons/lib/fa/trash-o';
import PlusCircle from 'react-icons/lib/fa/plus-circle';
import 'react-datepicker/dist/react-datepicker.css';
//import FormGroup from './FormGroup';
import ServiceSearch from './components/ServiceSearch';
import ServiceTabs from './components/ServiceTabs';
import messages from './messages';

import { ContentWrap } from "components/styles/Containers";

import {
  EntityWrap as ServiceWrap,
  EntityHeader as ServiceHeader,
  EntityContent as ServiceContent,
} from 'components/styles/Forms';

const renderSelectField = (renderProps) => {
  const { input } = renderProps;
  return (
    <Select {...input} {...renderProps} />
  );
};

const renderCheckboxField = (inputProps) => {
  const { input } = inputProps;

  return <CheckboxInput {...input} {...inputProps} />;
};

const ProvidedService = styled.div`
  padding: 15px 40px;
  background-color: #E4E7EC;;
  //margin-bottom: 15px;
  display: flex;
  flex-direction: row;
  align-items: center;

  color: #454545;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
`;

const ServiceDeleteIconWrap = styled.span`
  float: right;
  width: 20px;
  padding: 0;
  cursor: pointer;
`;
const AdditionalServiceWrapper = styled.span`
  color: #3E7DBC;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
`;

class PrescriptionServicesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedInnerTab: 1,
      updateOnce: false,
      providedServicesField: '',
      prescribedServicesField: '',
    };
    this.handleIssueChange = this.handleIssueChange.bind(this);
    this.handleMaxStartChange = this.handleMaxStartChange.bind(this);
    this.onInnerTabClick = this.onInnerTabClick.bind(this);
    this.serviceProvChose = this.serviceProvChose.bind(this);
    this.servicePrescribedChose = this.servicePrescribedChose.bind(this);
    this.getPrescribedServicesString = this.getPrescribedServicesString.bind(this);
    this.getProvidedServicesString = this.getProvidedServicesString.bind(this);
  }

  onInnerTabClick(tabNumber) {
    this.setState({ selectedInnerTab: tabNumber });
  }

  componentDidMount() {
    if (this.props.providedServices.length === 0) {
      this.props.change('providedServices', [{}]);
    }
    if (this.props.prescribedServices.length === 0) {
      this.props.change('prescribedServices', [{}]);
      this.props.change('patient', this.props.currentPatient._id);
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (!this.props.isEdit && !this.state.updateOnce) {
      this.props.change('isAsPrescribed', true);
      this.setState({ updateOnce: true });
    }
  }

  getPrescribedServicesString() {
    const { prescribedServices } = this.props;
    let stringPrescr = '';
    if (prescribedServices) {
      prescribedServices.forEach((service, index) => {
        if (service.service && service.service.name && service.service.name.defaultName
          && prescribedServices[index].quantity && service.service.name.shortName) {
          stringPrescr += prescribedServices[index].quantity + 'x' + service.service.name.shortName;
          stringPrescr += (index < prescribedServices.length - 1) ? ' + ' : '';
        }
      });
    }
    return stringPrescr;
  }

  getProvidedServicesString() {
    const { providedServices } = this.props;
    let stringProv = '';
    if (providedServices) {
      providedServices.forEach((service, index) => {
        if (service.service && service.service.name && service.service.name.defaultName
          && providedServices[index].quantity && service.service.name.shortName) {
          stringProv += providedServices[index].quantity + 'x' + service.service.name.shortName;
          stringProv += (index < providedServices.length - 1) ? ' + ' : '';
        }
      });
    }
    return stringProv;
  }

  handleIssueChange(date) {
    this.props.change('issuedDate', date);
    this.setState({ issue: date });
  }

  handleMaxStartChange(date) {
    this.props.change('maxStartDate', date);
    this.setState({ maxStart: date });
  }

  serviceProvChose(index, service) {
    const { change } = this.props;

    if (service.name && service.name.shortName) {
      change(`providedServices[${index}].shortName`, service.name.shortName);
    }
    if (service.name && service.name.defaultName) {
      change(`providedServices[${index}].serviceName`, service.name.defaultName);
    }
    const payer = service.payers.filter((payer) => payer.payer === this.props.currentPatient.payerStatus);
    change(`providedServices[${index}].duration`, payer[0].duration);

    change(`providedServices[${index}].serviceId`, service._id);
    change(`providedServices[${index}].service`, service);
  }

  servicePrescribedChose(index, service) {
    const { change } = this.props;

    if (service.name && service.name.shortName) {
      change(`prescribedServices[${index}].shortName`, service.name.shortName);
    }
    if (service.name && service.name.defaultName) {
      change(`prescribedServices[${index}].serviceName`, service.name.defaultName);
    }

    change(`prescribedServices[${index}].serviceId`, service._id);
    change(`prescribedServices[${index}].service`, service);
  }

  medProvServicesFields = ({ fields }) => {
    const { providedServices } = this.props;
    const formatMessage = this.context.intl.formatMessage;

    const quantity = [];
    for (let i = 1; i < 21; i++) {
      quantity.push({ value: i, label: i });
    }
    const frequency = [];
    for (let i = 1; i < 8; i++) {
      frequency.push({ value: i, label: i });
    }

    let servicesToSearch = [];
    servicesToSearch = this.props.services.filter((service) =>
      service.payers.some((payer) =>
        this.props.payerName !== 'SZ'
          ? payer.payer === this.props.currentPatient.payerStatus
          : payer.payer === 'P'
      )
    );
    let addDisable = providedServices[0] && providedServices[0].service ? false : true;

    return (
      <div>
        In development
        {/*<ContentWrap>*/}
          {/*{*/}
            {/*fields.map((field, index) => {*/}
              {/*let staffList = [];*/}

              {/*if (providedServices[index] && providedServices[index].service) {*/}
                {/*providedServices[index].service.staffAllocation.forEach(staff => {*/}
                  {/*if (!staff.isAnyStaff && staff.staff) {*/}
                    {/*staffList.push(staff.staff);*/}
                  {/*}*/}
                {/*});*/}
              {/*}*/}
              {/*let staffToShow = [];*/}

              {/*if (staffList.length !== 0) {*/}
                {/*staffToShow = staffList.map((staff, index) => ({*/}
                  {/*value: staff._id, label: staff.lastName + ' ' + staff.firstName*/}
                {/*}));*/}
              {/*}*/}
              {/*let deleteDisable = providedServices[index] && providedServices[index].service ? false : true;*/}

              {/*return (*/}
                {/*<ServiceWrap key={index}>*/}
                  {/*<ServiceHeader>*/}
                    {/*<span>{index + 1}. </span>*/}
                    {/*<strong>{providedServices[index] && providedServices[index].shortName &&*/}
                    {/*(providedServices[index].shortName + (providedServices[index].duration ? ' - ' + providedServices[index].duration + ' ' + formatMessage(messages.min) : ''))}</strong>*/}
                    {/*<ServiceDeleteIconWrap*/}
                      {/*onClick={() => !deleteDisable ? fields.remove(index) : ''}*/}
                    {/*>*/}
                      {/*<Trash size="22" color="#D46659" style={{ opacity: deleteDisable ? '0.5' : '1' }} />*/}
                    {/*</ServiceDeleteIconWrap>*/}
                  {/*</ServiceHeader>*/}
                  {/*<ServiceContent>*/}
                    {/*<FormGroup>*/}
                      {/*<div style={{ width: '270px', marginRight: '10px' }}>*/}
                        {/*<ServiceSearch*/}
                          {/*services={servicesToSearch}*/}
                          {/*categories={this.props.categories}*/}
                          {/*dispatch={this.props.dispatch}*/}
                          {/*serviceChose={this.serviceProvChose.bind(null, index)}*/}
                          {/*inputValue={providedServices[index] && providedServices[index].serviceName*/}
                          {/*? providedServices[index].serviceName : ''}*/}
                        {/*/>*/}
                      {/*</div>*/}
                      {/*<div style={{ width: '70px', marginRight: '10px' }}>*/}
                        {/*<Field name={`${field}.quantity`} component={renderSelectField} placeholder=" " required options={quantity} />*/}
                      {/*</div>*/}
                      {/*<div style={{ width: '55px', marginRight: '10px' }}>*/}
                        {/*<Field name={`${field}.frequencyMin`} component={renderSelectField} placeholder=" " options={frequency} />*/}
                      {/*</div>*/}
                      {/*<div style={{ width: '55px', marginRight: '10px' }}>*/}
                        {/*<Field name={`${field}.frequencyMax`} component={renderSelectField} placeholder=" " options={frequency} />*/}
                      {/*</div>*/}
                      {/*<div style={{ display: 'flex', alignItems: 'center', marginLeft: '5px', flexGrow: '1' }}>*/}
                        {/*{*/}
                          {/*providedServices[index] && providedServices[index].hasPreferredStaff ?*/}
                            {/*<Field*/}
                              {/*name={`${field}.preferredStaff1`}*/}
                              {/*component={renderSelectField}*/}
                              {/*disabled={staffToShow.length === 0}*/}
                              {/*placeholder="Staff Name"*/}
                              {/*options={[...staffToShow]}*/}
                            {/*/>*/}
                            {/*:*/}
                            {/*<div style={{ marginRight: '40px' }}>*/}
                              {/*<Field*/}
                                {/*name={`${field}.hasPreferredStaff`}*/}
                                {/*type="checkbox"*/}
                                {/*component={renderCheckboxField}*/}
                              {/*/>*/}
                            {/*</div>*/}
                        {/*}*/}
                      {/*</div>*/}
                    {/*</FormGroup>*/}
                  {/*</ServiceContent>*/}
                {/*</ServiceWrap>*/}
              {/*);*/}
            {/*})*/}
          {/*}*/}
        {/*</ContentWrap>*/}
        {/*<FormGroup style={{ paddingLeft: '35px', marginBottom: '5px', paddingTop: '10px',*/}
          {/*paddingBottom: '10px', }}>*/}
          {/*<AdditionalServiceWrapper*/}
            {/*className="add-payer"*/}
            {/*onClick={() => !addDisable ? fields.push({}) : '' }*/}
            {/*style={{ paddingLeft: '0', cursor: 'pointer', opacity: addDisable ? '0.5' : '1' }}*/}
          {/*>*/}
            {/*<PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />*/}
            {/*Additional Service…*/}
          {/*</AdditionalServiceWrapper>*/}
        {/*</FormGroup>*/}
      </div>);
  };

  medPrescrServicesFields = ({ fields }) => {
    const { prescribedServices } = this.props;
    const formatMessage = this.context.intl.formatMessage;

    const quantity = [];
    for (let i = 1; i < 21; i++) {
      quantity.push({ value: i, label: i });
    }
    const frequency = [];
    for (let i = 1; i < 8; i++) {
      frequency.push({ value: i, label: i });
    }

    let servicesToSearch = [];
    servicesToSearch = this.props.services.filter((service) =>
      service.payers.some((payer) =>
        this.props.payerName !== 'SZ'
          ? payer.payer === this.props.currentPatient.payerStatus
          : payer.payer === 'P'
      )
    );
    let addDisable = prescribedServices[0] && prescribedServices[0].service ? false : true;

    return (
      <div>
        <ContentWrap>
          {
            fields.map((field, index) => {

              let staffList = [];

              if (prescribedServices[index] && prescribedServices[index].service) {
                prescribedServices[index].service.staffAllocation.forEach(staff => {
                  if (!staff.isAnyStaff) {
                    staffList.push(staff.staff);
                  }
                });
              }

              let deleteDisable = prescribedServices[index] && prescribedServices[index].service ? false : true;

              return (
                <ServiceWrap key={index}>
                  <ServiceHeader>
                    <span>{index + 1}. </span>
                    <strong>{prescribedServices[index] && prescribedServices[index].shortName &&
                    (prescribedServices[index].shortName + (prescribedServices[index].duration ? ' - ' + prescribedServices[index].duration + ' ' + formatMessage(messages.min) : ''))}</strong>
                    <ServiceDeleteIconWrap
                      onClick={() => !deleteDisable ? fields.remove(index) : ''}
                    >
                      <Trash size="22" color="#D46659" style={{ opacity: deleteDisable ? '0.5' : '1' }} />
                    </ServiceDeleteIconWrap>
                  </ServiceHeader>
                  <ServiceContent>
                    <FormGroup>
                      <div style={{ width: '270px', marginRight: '10px' }}>
                        <ServiceSearch
                          services={servicesToSearch}
                          categories={this.props.categories}
                          dispatch={this.props.dispatch}
                          serviceChose={this.servicePrescribedChose.bind(null, index)}
                          inputValue={prescribedServices[index] && prescribedServices[index].serviceName
                            ? prescribedServices[index].serviceName : ''}
                        />
                      </div>
                      <div style={{ width: '70px', marginRight: '10px' }}>
                        <Field name={`${field}.quantity`} component={renderSelectField} placeholder=" " required options={quantity} />
                      </div>
                      <div style={{ width: '55px', marginRight: '10px' }}>
                        <Field name={`${field}.frequencyMin`} component={renderSelectField} placeholder=" " options={frequency} />
                      </div>
                      <div style={{ width: '55px', marginRight: '10px' }}>
                        <Field name={`${field}.frequencyMax`} component={renderSelectField} placeholder=" " options={frequency} />
                      </div>
                    </FormGroup>
                  </ServiceContent>
                </ServiceWrap>);
            })
          }
        </ContentWrap>
        <FormGroup style={{
          paddingLeft: '35px',
          paddingTop: '10px',
          paddingBottom: '10px',
          marginBottom: '5px'
        }}
        >
          <AdditionalServiceWrapper
            className="add-payer"
            onClick={() => !addDisable ? fields.push({}) : ''}
            style={{ paddingLeft: '0', cursor: 'pointer', opacity: addDisable ? '0.5' : '1' }}
          >
            <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
            Additional Service…
          </AdditionalServiceWrapper>
        </FormGroup>
      </div>
    );
  };

  render() {
    const { isAdmin } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    return (
      <div style={{ paddingTop: '12px'}}>
        <form onSubmit={this.props.handleSubmit}>
          <div>
            <ServiceTabs
              active={this.state.selectedInnerTab}
              providedServices={this.getProvidedServicesString()}
              prescribedServices={this.getPrescribedServicesString()}
              onTabClick={this.onInnerTabClick}
              isAsPrescribed={this.props.isAsPrescribed}
              checkbox={<Field
                name="isAsPrescribed"
                type="checkbox"
                component={renderCheckboxField}
              />}
            />
            <ProvidedService>
              <div style={{ width: '260px' }}>
                {formatMessage(messages.ServiceName)}
              </div>
              <div style={{ display: 'flex', width: '77px', justifyContent: 'center' }}>
                {formatMessage(messages.Quantity)}
              </div>
              <div style={{
                display: 'flex',
                width: '124px',
                flexDirection: 'column',
                textAlign: 'center',
                margin: '0px 15px'
              }}>
                <div>{formatMessage(messages.FrequencyWeek)}</div>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                  <div >{formatMessage(messages.Min)}</div>
                  <div>{formatMessage(messages.Max)}</div>
                </div>
              </div>
              {(this.state.selectedInnerTab === 1) &&
              <div style={{ display: 'flex', width: '150px', textAlign: 'center' }}>
                {formatMessage(messages.PreferStaff)}
              </div>
              }
            </ProvidedService>
            {
              this.state.selectedInnerTab === 1 &&
              <FieldArray name="providedServices" component={this.medProvServicesFields} />
            }
            {
              this.state.selectedInnerTab === 2 &&
              <FieldArray name="prescribedServices" component={this.medPrescrServicesFields} />
            }
          </div>
          <CardFooter>
            <ContentWrap padding="10px 30px 10px 30px">
              <Row justifyContent="space-between" center noPadding>
                <Col>
                  { !this.props.isEdit &&
                  <GreyButton type="button" onClick={() => this.props.onTabClick(1)}>{formatMessage(messages.GoBack)}</GreyButton>
                  }
                  { this.props.onDelete &&
                  <DeleteButton type="button" onClick={this.props.onDelete} disabled={!isAdmin}>
                    {formatMessage(messages.CancelPrescription)}
                  </DeleteButton> }
                </Col>
                <Col>
                  <div>
                    <GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                    <GreenButton
                      type="submit"
                      disabled={this.props.pristine}
                    >
                      { this.props.isEdit ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddPrescription)}</GreenButton>
                  </div>
                </Col>
              </Row>
            </ContentWrap>
          </CardFooter>
        </form>
      </div>
    );
  }
}
PrescriptionServicesForm.defaultProps = {
  providedServices: [],
  prescribedServices: [],
  isAsPrescribed: true,
};

PrescriptionServicesForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
PrescriptionServicesForm.propTypes = {
  onDelete: React.PropTypes.func,
  onCancel: React.PropTypes.func,
  pristine: React.PropTypes.bool.isRequired,
  change: React.PropTypes.func.isRequired,
  isEdit: React.PropTypes.bool,
  handleSubmit: React.PropTypes.func,
  services: React.PropTypes.array.isRequired,
  currentPatient: React.PropTypes.object.isRequired,
  categories: React.PropTypes.array.isRequired,
};


export { default as validate } from './validate';
export default PrescriptionServicesForm;
