import React, { PropTypes } from 'react';
import { push } from 'react-router-redux';
import messages from '../messages';

import './searchDoctorStyle.scss';


export default class SearchPatient extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onInputChange = this.onInputChange.bind(this);
    this.onPatientClick = this.onPatientClick.bind(this);
    this.onNewPatientClick = this.onNewPatientClick.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);
    this.onFocusEvent = this.onFocusEvent.bind(this);

    this.state = {
      users: props.users,
      input: props.inputValue,
      disabled: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.users !== this.state.users) {
      this.setState({
        users: nextProps.users,
      });
    }
    if (nextProps.inputValue !== this.state.input || nextProps.inputValue !== '') {
      this.setState({
        input: nextProps.inputValue,
      });
    }
  }

  onInputChange(event) {
    const users = this.sortusers(this.state.users, event.target.value);

    this.setState({
      input: event.target.value,
      disabled: !(event.target.value.length > 0),
      users,
    });
  }

  onPatientClick(patientId) {
    this.props.userChose(patientId);
  }

  onNewPatientClick() {
    this.setState({
      input: '',
      disabled: true,
    });
    this.props.dispatch(push('/main/users/new'));
  }

  onBlurEvent() {
    this.setState({
      disabled: true,
    });
  }

  onFocusEvent() {
    this.setState({
      disabled: this.state.input.length <= 0,
    });
  }

  sortusers(array, string) {
    const users = array;
    users.sort((patient1, patient2) => {
      if (patient1.firstName.indexOf(string) !== 0 &&
        patient2.firstName.indexOf(string) === 0 ||
        patient1.lastName.indexOf(string) !== 0 &&
        patient2.lastName.indexOf(string) === 0
      ) {
        return 1;
      } else if (patient1.firstName.indexOf(string) === 0 &&
        patient2.firstName.indexOf(string) !== 0 ||
        patient1.lastName.indexOf(string) === 0 &&
        patient2.lastName.indexOf(string) !== 0) {
        return -1;
      }
    });
    return users;
  }

  renderThick(text, substring) {
    return text.indexOf(substring) === 0 ?
      <span><span className="selected-text" >{substring}</span>{text.substring(substring.length, text.length)}</span> :
      text;
  }

  render() {
    const users = this.state.users;
    const usersLength = users.length;
    const input = this.state.input;
    const options = [];

    for (let i = 0; i < usersLength; i++) {
      if (users[i].lastName && users[i].lastName.toLowerCase().indexOf(input.toLowerCase()) === 0) {
        options.push(
          <div className="dropdown--item" key={i} onMouseDown={() => this.onPatientClick(users[i])} >
            {users[i].title.personal ? users[i].title.personal : ''}&nbsp;
            {this.renderThick(users[i].lastName, input)},&nbsp;
            {users[i].firstName}
          </div>
        );
      }
      if (options.length === 4) { break; }
    }

    return (
      <div className="searchBoxWrapperDoctor" >
        <input
          className="searchBoxWrapperDoctor--input " type="text"
          placeholder={this.context.intl.formatMessage(messages.EnterDoctorName)}
          onChange={this.onInputChange}
          value={this.state.input}
          onFocus={this.onFocusEvent}
          onBlur={this.onBlurEvent}
        />
        <div className={this.state.disabled ? 'dropdown-hide' : 'dropdown'} >
          {options}
        </div>
      </div>
    );
  }
}

SearchPatient.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
SearchPatient.propTypes = {
  users: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
  userChose: PropTypes.func,
};
