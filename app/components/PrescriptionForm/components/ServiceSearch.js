import React, { PropTypes } from 'react';
import styled from 'styled-components';

import './searchServiceStyle.scss';

const Color = styled.div`
  height: 17px;
  width: 17px;
  border: 1px solid #DADFEA;
  border-radius: 3px;
`;

export default class SearchPatient extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onInputChange = this.onInputChange.bind(this);
    this.onPatientClick = this.onPatientClick.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);
    this.onFocusEvent = this.onFocusEvent.bind(this);

    this.state = {
      services: props.services,
      input: props.inputValue,
      disabled: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.services !== this.state.services) {
      this.setState({
        services: nextProps.services,
      });
    }
    if (nextProps.inputValue !== this.state.input || nextProps.inputValue !== '') {
      this.setState({
        input: nextProps.inputValue,
      });
    }
  }

  onInputChange(event) {
    let services = this.sortservices(this.state.services, event.target.value);

    this.setState({
      input: event.target.value,
      disabled: !(event.target.value.length > 0),
      services: services,
    });
  }

  onPatientClick(patientId) {
    this.props.serviceChose(patientId);

  }

  onBlurEvent() {
    this.setState({
      disabled: true,
    });

  }

  onFocusEvent() {
    this.setState({
      disabled: this.state.input.length <= 0,
    });
  }

  sortservices(array, string) {
    let services = array;
    services.sort((patient1, patient2) => {
      if (
        patient1.name.defaultName.toLowerCase().indexOf(string) !== 0 &&
        patient2.name.defaultName.toLowerCase().indexOf(string) === 0
      ) {
        return 1;
      } else if (
        patient1.name.defaultName.toLowerCase().indexOf(string) === 0 &&
        patient2.name.defaultName.toLowerCase().indexOf(string) !== 0) {
        return -1;
      }
    });
    return services;
  }

  findCategoryById(categories, id) {
    return categories.find((c)=>c._id === id);
  }

  render() {


    const services = this.state.services,
      servicesLength = services.length,
      input = this.state.input;

    const options = [];
    for (let i = 0; (i < 4) && i < servicesLength && input; i++) {
      if (services[ i ].name.defaultName.toLowerCase().indexOf(input) === 0) {
        const category = this.findCategoryById(this.props.categories, services[ i ].category)

        options.push(
          <div className="dropdown--item" key={i} onMouseDown={()=>this.onPatientClick(services[i])}>
            <Color style={{backgroundColor:category.color}}/>&nbsp;
            <span style={{fontWeight: '600'}}>{services[ i ].name.defaultName}</span>&nbsp;
            ({category.name})
          </div>
        );
      }
    }
    return (
      <div className="searchBoxWrapperService">
        <input className="searchBoxWrapperService--input " type="text"
               placeholder="Service name" onChange={this.onInputChange}
               value={this.state.input} onFocus={this.onFocusEvent} onBlur={this.onBlurEvent}/>

        <div className={this.state.disabled ? 'dropdown-hide' : 'dropdown'}>
          {options}
        </div>
      </div>
    );
  }
}

SearchPatient.propTypes = {
  services: PropTypes.array.isRequired,
  categories: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
  serviceChose: PropTypes.func,
};
