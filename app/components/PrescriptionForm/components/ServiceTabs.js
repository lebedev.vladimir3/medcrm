import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import messages from '../messages';
import HelpIcon from 'react-icons/lib/md/help-outline';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap_white.css';
import './checkboxStyles.scss';
import './tooltipStyles.scss';

const Wrapper = styled.div`
  display: flex;
  padding-left: 29px;
  padding-right: 29px;
  padding-bottoom: 15px;
`;

const Tab = styled.div`
  position: relative;
  width: 50%;
  margin-bottom: 20px;
  border: ${(props) => props.active ? '1px solid #CFD3DD' : 'none'};
  border-radius: 4px;
  background-color: ${(props) => props.active ? '#F5F7F6' : '#FFF'}; ;
  padding: 17px 15px 20px 15px;
  color: #455A64;
  
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  cursor: ${(props) => props.onClick ? 'pointer' : 'auto'};
`;

const Arrow = styled.div`
  position: absolute;
  width: 14px;
  height: 14px;
  margin-left: calc(50% - 14px);
  bottom: -8px;
  transform: rotate(45deg);
  border: ${(props) => props.active ? '1px solid #CFD3DD' : 'none'};
  border-top: none;
  border-left: none;
  background-color: ${(props) => props.active ? '#F5F7F6' : '#FFF'};
`;

const BriefList = styled.div`
  height: 36px;
  width: 100%;
  border: ${(props) => props.active ? '1px solid #7CA297' : '1px solid #D2D2D2'};
  border-radius: 4px;
  background-color: ${(props) => props.active ? '#F9F9F9' : '#FFF'} ;
  color: ${(props) => props.active ? '#5F5F5F' : '#5F5F5F'};
  
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  padding-top: 9px;
  padding-left: 11px;
  margin-top: 10px;
`;

const StyledHelpIcon = styled(HelpIcon)`
    float: right;
    height: 21px;
    width: 21px;
    color: #B7962D;
    cursor: pointer;
`;

class ServiceTabs extends React.Component {
  state = {
    tooltipShow: false
  };
  handleIconClick(){
    this.setState({ tooltipShow: !this.state.tooltipShow })
  }
  render(){
    const props = this.props,
      context = this.context;
    // const StyledCheckbox = props.checkbox && styled(props.checkbox)`
    //   box-shadow: ${props => props.visibleTooltip ? '0 0 2px 1px rgba(212,181,61,0.3);': 'none'};
    // `;
    return (
      <Wrapper>
        <Tab active={props.active === 1} onClick={() => props.onTabClick(1)}>
          <div style={{
            color: props.active === 1 ? '#414141': '#4A69AE'
          }}>{context.intl.formatMessage(messages.ProvidedServices)}</div>
          <BriefList active={props.active === 1}>
            {props.providedServices}
          </BriefList>
          <Arrow active={props.active === 1} />
        </Tab>
        { //
          <Tab
            active={props.active === 2}
            isAsPrescribed={props.isAsPrescribed}
            onClick={(() => {
              props.onTabClick(2)
            })}
          >
            <div>
              <Tooltip
                visible={this.state.tooltipShow}
                placement="top"
                trigger={['click']}
                overlay={<span>{context.intl.formatMessage(messages.AsPrescribedTooltip)}</span>}
                align={{
                  offset: [0, -12],
                }}
              >
                {React.cloneElement(props.checkbox, {
                  checkboxClass: (this.state.tooltipShow && 'icheckbox_square-blue icheckbox__highlight') || '',
                  onChange: (e, newValue, oldValue) => {
                    console.log('!');
                    props.onTabClick(newValue ? 2: 1)
                  },
                  checked: props.active === 2
                })}
              </Tooltip>
              <span style={{ marginLeft: '15px', color: props.active === 2 ? '#414141': '#4A69AE' }}>{context.intl.formatMessage(messages.PrescribedServices)}</span>
              <StyledHelpIcon onClick={::this.handleIconClick}/>
            </div>
            {//!props.isAsPrescribed &&
              [
                <BriefList key="0" active={props.active === 2}>{props.prescribedServices}</BriefList>,
                <Arrow key="1" active={props.active === 2} />
              ]
            }
          </Tab>
        }
      </Wrapper>
    );
  }
}
// const ServiceTabs = (props, context) => (
//
// );

ServiceTabs.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

ServiceTabs.propTypes = {
  providedServices: PropTypes.string,
  prescribedServices: PropTypes.string,
  onTabClick: PropTypes.func,
  active: PropTypes.number,
  isAsPrescribed: PropTypes.bool,
};


export default ServiceTabs;
