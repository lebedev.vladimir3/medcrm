export { default as validate } from './validate';
export { default as PrescriptionCardForm } from './PrescriptionCardForm.old.js';
export { default as PrescriptionServicesForm } from './PrescriptionServicesForm.old.js';
export PhysiotherapieForm from './Physiotherapie';
export { default as PrescriptionReportsForm } from './PrescriptionReportsForm';
