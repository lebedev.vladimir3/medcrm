/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  Mr: {
    id: 'Mr',
    defaultMessage: 'Mr',
  },
  Mrs: {
    id: 'Mrs',
    defaultMessage: 'Mrs',
  },
  Ms: {
    id: 'Ms',
    defaultMessage: 'Ms',
  },
  insuranceNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  Country: {
    id: 'Country',
    defaultMessage: 'Country',
  },
  Region: {
    id: 'Region',
    defaultMessage: 'Region',
  },
  Street: {
    id: 'Street',
    defaultMessage: 'Street',
  },
  HouseNumber: {
    id: 'House number',
    defaultMessage: 'House number',
  },
  City: {
    id: 'City',
    defaultMessage: 'City',
  },
  PostBox: {
    id: 'PostBox',
    defaultMessage: 'PostBox',
  },
  NotesForInternalUsage: {
    id: 'NotesForInternalUsage',
    defaultMessage: 'Notes for internal usage',
  },
  payStatus: {
    id: 'payStatus',
    defaultMessage: 'Payer status',
  },
  payerName: {
    id: 'payerName',
    defaultMessage: 'Payer Name',
  },
  insNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  PaymentInfo: {
    id: 'PaymentInfo',
    defaultMessage: 'Payment Info',
  },
  Notes: {
    id: 'Notes',
    defaultMessage: 'Notes',
  },
  MainMobilePhone: {
    id: 'MainMobilePhone',
    defaultMessage: 'Main mobile phone',
  },
  SecondMobilePhone: {
    id: 'SecondMobilePhone',
    defaultMessage: 'Secondary mobile phone',
  },
  PhoneNumber: {
    id: 'PhoneNumber',
    defaultMessage: 'Phone number',
  },
  EAddress: {
    id: 'EAddress',
    defaultMessage: 'E-Mail address',
  },
  DateofBirth: {
    id: 'DateofBirth',
    defaultMessage: 'Date of birth',
  },
  LastName: {
    id: 'LastName',
    defaultMessage: 'Last name',
  },
  FirstName: {
    id: 'FirstName',
    defaultMessage: 'First name',
  },
  Name: {
    id: 'Name',
    defaultMessage: 'Name',
  },
  HonorificTitle: {
    id: 'HonorificTitle',
    defaultMessage: 'Honorific title',
  },
  PersonalTitle: {
    id: 'PersonalTitle',
    defaultMessage: 'Personal title',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  GeneralInfo: {
    id: 'GeneralInfo',
    defaultMessage: 'General Info',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  AddPrescription: {
    id: 'AddPrescription',
    defaultMessage: 'Add Prescription',
  },
  NextStep: {
    id: 'NextStep',
    defaultMessage: 'Next step',
  },
  AddPatient: {
    id: 'AddPatient',
    defaultMessage: 'Add Patient',
  },
  Date: {
    id: 'Date',
    defaultMessage: 'Date',
  },
  Issued: {
    id: 'Issued',
    defaultMessage: 'Issued',
  },
  MaxStart: {
    id: 'MaxStart',
    defaultMessage: 'Max. Start',
  },
  DoctorTitle: {
    id: 'DoctorTitle',
    defaultMessage: 'Doctor’s Title',
  },
  DoctorName: {
    id: 'DoctorName',
    defaultMessage: 'Doctor’s Name',
  },
  EnterDoctorName: {
    id: 'EnterDoctorName',
    defaultMessage: 'Enter doctor’s name',
  },
  ClinicCodes: {
    id: 'ClinicCodes',
    defaultMessage: 'Clinic Codes',
  },
  RegulationType: {
    id: 'RegulationType',
    defaultMessage: 'Regulation Type',
  },
  IndicationCode: {
    id: 'IndicationCode',
    defaultMessage: 'Indication code',
  },
  SpecialActivity: {
    id: 'SpecialActivity',
    defaultMessage: 'Special Activity',
  },
  HouseVisit: {
    id: 'HouseVisit',
    defaultMessage: 'House visit',
  },
  Group: {
    id: 'Group',
    defaultMessage: 'Group',
  },
  Report: {
    id: 'Report',
    defaultMessage: 'Report',
  },
  Diagnosis: {
    id: 'Diagnosis',
    defaultMessage: 'Diagnosis',
  },
  Taxation: {
    id: 'Taxation',
    defaultMessage: 'Taxation',
  },
  TaxationType: {
    id: 'TaxationType',
    defaultMessage: 'TaxationType',
  },
  GoBack: {
    id: 'GoBack',
    defaultMessage: 'Go Back',
  },
  ProvidedSameAsPresc: {
    id: 'ProvidedSameAsPresc',
    defaultMessage: 'Provided services are the same as prescribed',
  },
  ProvidedServices: {
    id: 'ProvidedServices',
    defaultMessage: 'Provided Services',
  },
  PrescribedServices: {
    id: 'PrescribedServices',
    defaultMessage: 'Prescribed Services',
  },
  Service: {
    id: 'Service',
    defaultMessage: 'Service',
  },
  ServiceName: {
    id: 'ServiceName',
    defaultMessage: 'Service Name',
  },
  ShortName: {
    id: 'ShortName',
    defaultMessage: 'Short Name',
  },
  Quantity: {
    id: 'Quantity',
    defaultMessage: 'Quantity',
  },
  FrequencyWeek: {
    id: 'FrequencyWeek',
    defaultMessage: 'Frequency/week',
  },
  Min: {
    id: 'Min',
    defaultMessage: 'Min',
  },
  Max: {
    id: 'Max',
    defaultMessage: 'Max',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  DeletePrescription: {
    id: 'DeletePrescription',
    defaultMessage: 'Delete Prescription',
  },
  PreferStaff: {
    id: 'PreferStaff',
    defaultMessage: 'Preferred Staff',
  },
  AppointmentID: {
    id: 'AppointmentID',
    defaultMessage: 'Appointment ID',
  },
  PerformedBy: {
    id: 'PerformedBy',
    defaultMessage: 'Performed By',
  },
  AsPrescribedTooltip: {
    id: 'AsPrescribedTooltip',
    defaultMessage: 'You have an option to fill the provided services separately in case if they differ from the prescribed ones.'
  },
  Provider: {
    id: 'Provider',
    defaultMessage: 'Provider'
  },
  ServiceProvided: {
    id: 'ServiceProvided',
    defaultMessage: 'Provided'
  },
  ReportAdded: {
    id: 'ReportAdded',
    defaultMessage: 'Reported'
  },
  ReportUpdated: {
    id: 'ReportUpdated',
    defaultMessage: 'Updated'
  },
  min: {
    id: 'min',
    defaultMessage: 'min',
  },
  CancelPrescription: {
    id: 'CancelPrescription',
    defaultMessage: 'Cancel Prescription',
  },
});
