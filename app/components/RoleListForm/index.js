/**
 *
 * RoleListForm
 *
 */

import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import styled from 'styled-components';
import { Card, CardFooter } from 'components/styles/Card';
import { GreyButton, GreenButton } from 'components/common/Button';
import { Checkbox } from 'components/Input';
import { Table, TR, TH, TD } from 'components/Table';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { ContentWrap } from 'components/styles/Containers';

import messages from './messages';
const TRHead = styled(TR)`
  height: 50px;
  border-top-style: none;
`;

const THWithBackground = styled(TH)`
  background-color: #8A919A !important;
`;

const THCentered = styled(TH)`
  text-align: center;
  background-color: #8A919A !important;
`;

const TDCentered = styled(TD)`
  text-align: center;
`;

const renderCheckboxField = inputProps => {
  const { input } = inputProps;

  return <Checkbox {...input} {...inputProps} />;
};


const renderRows = (role, index) => {
  const isAdmin = role.name === 'Admin';

  return (
    <TR key={index}>
      <TD>{role.name}</TD>
      <TDCentered>
        <Field
          name={`[${index}].accessToSettings`}
          type="checkbox"
          component={renderCheckboxField}
          disabled={isAdmin}
        />
      </TDCentered>
      <TDCentered>
        <Field
          name={`[${index}].canMakeAppointments`}
          type="checkbox"
          component={renderCheckboxField}
          disabled={isAdmin}
        />
      </TDCentered>
      <TDCentered>
        <Field
          name={`[${index}].canSetOwnSchedule`}
          type="checkbox"
          component={renderCheckboxField}
          disabled={isAdmin}
        />
      </TDCentered>
    </TR>
  );
};

const RoleListForm = (props, context) => {
  const { handleSubmit, onSubmit, onCancel, pristine, roles } = props;

  return (
    <Card>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Table>
          <thead>
            <TRHead>
              <THWithBackground>{context.intl.formatMessage(messages.Role)}</THWithBackground>
              <THCentered>{context.intl.formatMessage(messages.AccessSettings)}</THCentered>
              <THCentered>{context.intl.formatMessage(messages.CanMakeAppointments)}</THCentered>
              <THCentered>{context.intl.formatMessage(messages.CanSetSchedule)}</THCentered>
            </TRHead>
          </thead>
          <tbody>
            {roles && roles.map(renderRows)}
          </tbody>
        </Table>
        <CardFooter>
          <ContentWrap padding="10px 30px 10px 30px">
            <Row justifyContent="space-between" center noPadding>
              <Col>
              </Col>
              <Col>
                <div>
                  <GreyButton onClick={onCancel}>Cancel</GreyButton>
                  <GreenButton type="submit" disabled={pristine}>{context.intl.formatMessage(messages.SaveChanges)}</GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </form>
    </Card>
  );
};


RoleListForm.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  pristine: PropTypes.bool,
  roles: PropTypes.array,
};

RoleListForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default RoleListForm;
