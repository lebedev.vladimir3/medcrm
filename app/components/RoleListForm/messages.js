/*
 * RoleListForm Messages
 *
 * This contains all the text for the RoleListForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  Role: {
    id: 'Role',
    defaultMessage: 'Role',
  },
  AccessSettings: {
    id: 'AccessSettings',
    defaultMessage: 'Access to Settings',
  },
  CanMakeAppointments: {
    id: 'CanMakeAppointments',
    defaultMessage: 'Can Make Appointments',
  },
  CanSetSchedule: {
    id: 'CanSetSchedule',
    defaultMessage: 'Can Set Own Schedule',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
});
