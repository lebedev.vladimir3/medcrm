import React, { PropTypes } from 'react';
import { injectIntl, intlShape } from 'react-intl';
import styled from 'styled-components';
import { Field } from 'redux-form';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import messages from './messages';

import renderHelper from 'components/common/renderHelper';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';
import { ContentWrap } from 'components/styles/Containers';
import { Card, CardFooter } from 'components/styles/Card';
import Obligatory from 'components/common/obligatoryLegend';

const RoomForm = (props, context) => {
  const { handleSubmit, onSubmit, onCancel, pristine, onDelete, isEdit, rooms } = props;
  const { formatMessage } = props.intl;
  console.log('asdadsa: ', props);
  return (
    <Card>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 30px`}>
          {!isEdit && <Obligatory />}
          <Row center>
            <LabelCol>
              <FormLabel required={!isEdit}>{formatMessage(messages.RoomNumber)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="25%">
                  <Field
                    style={{ width: '130px' }}
                    name="number"
                    component={renderHelper.inputField}
                    //placeholder={formatMessage(messages.RoomNumberPlaceholder)}
                    required
                  />
                </Col>
                <Col width="75%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel required={!isEdit}>{formatMessage(messages.ShortName)}</FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="25%">
                  <Field
                    style={{ width: '130px' }}
                    name="name"
                    component={renderHelper.inputField}
                    instantValidate
                    //placeholder={formatMessage(messages.ShortNamePlaceholder)}
                    required />
                </Col>
                <Col width="75%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>
                {formatMessage(messages.Capacity)}  <span>{formatMessage(messages.Patients)} </span>
              </FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="25%">
                  <Field
                    style={{ width: '130px' }}
                    name="capacity"
                    component={renderHelper.selectField}
                    //placeholder={formatMessage(messages.CapacityPlaceholder)}
                    options={Array(10).fill(null).map((x, i) => ({ value: i + 1, label: i + 1}))}
                    value={"1"}
                  />
                </Col>
                <Col width="75%" />
              </Row>
            </FieldsWrapCol>
          </Row>
          <Row center>
            <LabelCol>
              <FormLabel>
                {formatMessage(messages.Description)}
              </FormLabel>
            </LabelCol>
            <FieldsWrapCol>
              <Row>
                <Col width="100%">
                  <Field
                    name="description"
                    component={renderHelper.inputField}
                    //placeholder={formatMessage(messages.DescriptionPlaceholder)}
                  />
                </Col>
              </Row>
            </FieldsWrapCol>
          </Row>
        </ContentWrap>
        <CardFooter>
          <ContentWrap padding="10px 30px 10px 30px">
            <Row justifyContent="space-between" center noPadding>
              <Col>
                { onDelete &&
                <DeleteButton type="button" onClick={onDelete}>
                  {formatMessage(messages.DeleteRoom)}
                </DeleteButton>
                }
              </Col>
              <Col>
                <div>
                  <GreyButton onClick={onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                  <GreenButton type="submit" disabled={pristine}>
                    {onDelete ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddRoom)}
                  </GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </Form>
    </Card>
  );
};


// RoomForm.contextTypes = {
//   intl: React.PropTypes.object.isRequired,
// };

RoomForm.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  onDelete: PropTypes.func,
  pristine: PropTypes.bool,
  isEdit: PropTypes.bool,
  intl: intlShape.isRequired,
};


RoomForm.defaultProps = {
  categories: [],
};


export default injectIntl(RoomForm);
export { default as validate } from './validate';
