import { defineMessages } from 'react-intl';

export default defineMessages({
  RoomNumber: {
    id: 'RoomForm.RoomNumber',
    defaultMessage: 'Room Number',
  },
  RoomNumberPlaceholder: {
    id: 'RoomForm.RoomNumber.Placeholder',
    defaultMessage: 'Room no.',
  },
  ShortName: {
    id: 'RoomForm.ShortName',
    defaultMessage: 'Short Name',
  },
  ShortNamePlaceholder: {
    id: 'RoomForm.ShortName.Placeholder',
    defaultMessage: 'Short name',
  },
  Capacity: {
    id: 'RoomForm.Capacity',
    defaultMessage: 'Capacity',
  },
  CapacityPlaceholder: {
    id: 'RoomForm.Capacity.Placeholder',
    defaultMessage: 'Capacity',
  },
  Patients: {
    id: 'RoomForm.Patients',
    defaultMessage: '(patients)',
  },
  Description: {
    id: 'RoomForm.Description',
    defaultMessage: 'Description',
  },
  DescriptionPlaceholder: {
    id: 'RoomForm.Description.Placeholder',
    defaultMessage: 'Room description',
  },
  AddRoom: {
    id: 'RoomForm.AddRoom',
    defaultMessage: 'Add Room',
  },
  DeleteRoom: {
    id: 'RoomForm.DeleteRoom',
    defaultMessage: 'Delete Room',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
});
