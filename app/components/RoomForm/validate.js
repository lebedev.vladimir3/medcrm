import messages from './../common/messages/validation';

const validate = (values, props) => {
  const { formatMessage } = props.intl;
  const errors = {},
    existsNames = props.rooms.map(item => item.name);
  if(!values.number){
    errors.number = formatMessage(messages.Required);
  }
  if (values.name) {
    if(values.name.length > 10){
      errors.name = formatMessage(messages.ShortNameLength);
    } else if(existsNames.find(name => name === values.name)){
      errors.name = formatMessage(messages.ShortNameTaken);
    }
  } else if(props.submitFailed){
    errors.name = formatMessage(messages.Required);
  }
  if (!values.capacity) {
    errors.capacity = formatMessage(messages.Required);
  } else if(values.capacity < 1) {
    errors.capacity = formatMessage(messages.Required);
  }
  return errors;
};


export default validate;
