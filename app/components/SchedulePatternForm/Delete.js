import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import Cancel from 'react-icons/lib/md/cancel';


const StyledDelete = styled.span`
  height: 18px;
  width: 92px;
  color: #CE5648;
  
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  text-align: center;
`;


const Delete = function () {
  return (
    <div>
      <StyledDelete>
        <Cancel size="23" style={{ margin: '5px 5px 8px 0' }} />
        Delete Pattern
      </StyledDelete>
    </div>
  );
};


export default Delete;
