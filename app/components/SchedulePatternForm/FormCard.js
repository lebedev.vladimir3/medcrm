import styled from 'styled-components';
import Card from 'components/Card';


const StyledCard = styled(Card)`
  padding-top: 44px;
  width: 800px;
  margin: auto;
  @media (max-width: 1160px) {
    width: auto;
  }
`;

export default StyledCard;
