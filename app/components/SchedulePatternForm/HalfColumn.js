import styled from 'styled-components';


const HalfColumn = styled.div`
  width: 50%;
  display: flex;
  @media (max-width: 900px) {
    width: auto;
  }
`;


export default HalfColumn;
