import styled from 'styled-components';

const InputColumn = styled.div`
  display: flex;
  width: 600px;
  @media (max-width: 900px) {
    flex-direction: column;
    width: auto;
  }
`;


export default InputColumn;

