import fontSetting from 'components/styles/font';
import styled from 'styled-components';

const Label = styled.label`
  
  color: #596372;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  padding-top : 5px;
  @media (max-width: 1160px) {
    width: 120px;
  }
`;


export default Label;

