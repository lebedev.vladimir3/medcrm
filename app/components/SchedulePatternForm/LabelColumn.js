import styled from 'styled-components';

const LabelColumn = styled.div`
  display: flex;
  width: 200px;
  @media (max-width: 1160px) {
    width: auto;
  }
`;
export const HiddenLabelColumn = styled(LabelColumn)`
  @media (max-width: 900px) {
    display: none;
  }
`;



export default LabelColumn;
