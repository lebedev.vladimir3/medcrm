import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { Field } from 'redux-form';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, Select, Checkbox, DatePicker, TimePicker, RadioInput } from 'components/Input';
import moment from 'moment';
import FormGroup from './FormGroup';
import LabelColumnComponent from './LabelColumn';
import InputColumn from './InputColumn';
import HalfColumn from './HalfColumn';
import messages from './messages';
import Divider from 'components/common/Divider';
import { StyledDefaultDivider } from 'components/common/HeaderPanel/styles/index';
import { Card, CardFooter } from 'components/styles/Card';
import { ContentWrap } from 'components/styles/Containers';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import Obligatory from 'components/common/obligatoryLegend';
import { FormLabel, RequiredStar } from 'components/styles/common';
import { HeaderPanel, HeaderItem } from 'components/common/HeaderPanel';
import theme from 'components/styles/colorScheme';


const ObligatoryEnhanced = styled(Obligatory)`
  position: absolute;
  right: 30px;
  top:13px;
  z-index: 2;
`;
const CustomRequiredStar = styled(RequiredStar)`
  top: 5px;
`;

const Dash = styled.span`
  margin-left: 10px;
  margin-right: 10px;
`;

const LabelSubColumn = styled.div`
  margin-right: 30px;
	color: #818181;
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
  display: flex;
  align-items: center;
  position: relative;
  border-bottom: 1px solid;
  border-color: ${props => props.borderError ? theme.Input.error : 'white'};
`;

const WithLeftPadding = styled.div`
  padding-left: 10px;
`;

const Stripe = styled.div`
  height: 100%;
  width: 4px;
  border-radius: 4px 0 0 4px;
  position: absolute;
  top: 0px;
  background-color: #E17E28;
`;

const TimePickerWrapper = styled.div`
  border-color: ${props => props.hasError ? theme.Input.error : theme.Input.border};
  position: relative;
`;

const LabelColumn = styled(LabelColumnComponent)`
  width: 15%;
  margin-right: 40px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
	color: #5F5F5F;
	
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
	position: relative;
`;

const renderInputField = (inputProps) => <Input {...inputProps.input} {...inputProps} />;

const renderRadioField = (inputProps) => <RadioInput
  style={{
    height: '36px',
    marginRight: '24px'
  }}
  checked={inputProps.input.checked}
  {...inputProps}
/>;


const renderCheckboxField = (inputProps) => <Checkbox style={{display: 'flex'}}{...inputProps.input} {...inputProps}
                                                      checked={!!inputProps.input.value} />;
const renderSelectField = (renderProps) => <Select {...renderProps.input} {...renderProps} />;

const renderDatePicker = (props) => {

  return <DatePicker
    selected={props.input.value ? moment(props.input.value) : null}
    onChange={props.input.onChange}
    placeholderText="DD.MM.YYYY"
    dateFormat="DD.MM.YYYY"
    style={{ width: '100px' }}
    disabled={props.disabled}
    meta={props.meta}
  />;
};

const renderTimePicker = (props) => {
  const hasError = props.meta && props.meta.submitFailed && props.meta.error;
  return (
    <TimePicker hasError={hasError} {...props} />
  );
}

const SchedulePatternForm = (props, context) => {
  const parseBooleans = (value) => value === 'true';
  const formatBooleans = (value) => value ? 'true' : 'false';

  const {
    handleSubmit,
    onSubmit,
    onCancel,
    pristine,
    onDelete,
    repeated,
    allDay,
    forStaff,
    type,
    singleDay,
    canEdit,
    isEdit,
    repeatOn,
    isFormSubmitted,
    hasEndDate,
    schedulesLink,
  } = props;
  const { formatMessage } = context.intl;

  const textForPatternTitle = forStaff ? (onDelete ? formatMessage(messages.EditPattern) : formatMessage(messages.NewSchedulePattern)) :
    props.title ? (formatMessage(messages.Pattern) + ' ID ' + props.title)
      : formatMessage(messages.NewSchedulePattern)
  return (
    <Card>
      {!isEdit && <ObligatoryEnhanced />}
      <HeaderPanel height='72px' brandTitle={formatMessage(messages.SchedulePatterns)} brandLink={forStaff ? schedulesLink : '/main/opening-hours#patterns'}
                   backgroundLeft="#DEE1E3" background="#F6F6F6"><HeaderItem>{textForPatternTitle}</HeaderItem></HeaderPanel>
      <form onSubmit={handleSubmit(onSubmit)}>
        <ContentWrap padding={`25px 20px 22px 20px`}>
          <FormGroup>
            <LabelColumn>{formatMessage(messages.Type)}</LabelColumn>
            {
              forStaff ?
                <InputColumn>
                  <Field
                    name="type"
                    value="WORKING_HOURS"
                    type="radio"
                    component={renderRadioField}
                    label={formatMessage(messages.WorkingHours)}
                  />
                  <Field
                    name="type"
                    value="NON_WORKING_HOURS"
                    type="radio"
                    component={renderRadioField}
                    label={`${formatMessage(messages.NonWorking)} / ${formatMessage(messages.Pause)}`}
                  />
                  <Field
                    name="type"
                    value="ABSENSE"
                    type="radio"
                    component={renderRadioField}
                    label={formatMessage(messages.Absence)}
                  />
                </InputColumn> :
                <InputColumn>
                  <Field
                    name="type"
                    value="WORKING_HOURS"
                    type="radio"
                    component={renderRadioField}
                    label={formatMessage(messages.OpeningHours)}
                  />
                  <Field
                    name="type"
                    value="NON_WORKING_HOURS"
                    type="radio"
                    component={renderRadioField}
                    label={formatMessage(messages.ClosureHours)}
                  />
                  <Field
                    name="type"
                    value="PUBLIC_HOLIDAY"
                    type="radio"
                    component={renderRadioField}
                    label={formatMessage(messages.PublicHoliday)}
                  />
                </InputColumn>
            }
          </FormGroup>
          <Divider dashed color="#9EA7AF" />
          <FormGroup>
            <LabelColumn>{formatMessage(messages.StartDate)} {!isEdit && <CustomRequiredStar />}</LabelColumn>
            <InputColumn>
              <HalfColumn style={{width: '224px'}}>
                <Field name="startDate" component={renderDatePicker} placeholder="DD.MM.YYYY" required />
              </HalfColumn>
              {
                (type === 'PUBLIC_HOLIDAY' || type === 'ABSENSE') &&
                <HalfColumn>
                  <WithLeftPadding style={{paddingTop: '5px'}}>
                    <Field
                      name="singleDay"
                      component={renderCheckboxField}
                      label={formatMessage(messages.SingleDay)}
                    />
                  </WithLeftPadding>
                </HalfColumn>
              }
            </InputColumn>
          </FormGroup>
          {
            type !== 'PUBLIC_HOLIDAY' && type !== 'ABSENSE' &&
            <FormGroup>
              <LabelColumn>{formatMessage(messages.Time)} {!isEdit && <CustomRequiredStar />}</LabelColumn>
              <InputColumn>
                <HalfColumn>
                  <div style={{width: '100px', backgroundColor: '#EDF0F3'}}>
                    <Field
                      name="timeFrom"
                      component={renderTimePicker}
                      placeholder={formatMessage(messages.TimePlaceholder)}
                      required={!allDay}
                      disabled={allDay}
                    />
                  </div>
                  <Dash>-</Dash>
                  <div style={{width: '100px', backgroundColor: '#EDF0F3'}}>
                    <Field
                      name="timeTo"
                      component={renderTimePicker}
                      placeholder={formatMessage(messages.TimePlaceholder)}
                      required={!allDay}
                      disabled={allDay}
                    />
                  </div>
                </HalfColumn>
              </InputColumn>
            </FormGroup>
          }
          {
            (type === 'PUBLIC_HOLIDAY' || type === 'ABSENSE') &&
            <FormGroup>
              <LabelColumn>{formatMessage(messages.EndDate)}</LabelColumn>
              <InputColumn>
                <HalfColumn style={{width: '224px'}}>
                  <Field
                    name="endDate"
                    component={renderDatePicker}
                    disabled={singleDay && (type === 'PUBLIC_HOLIDAY' || type === 'ABSENSE')}
                  />
                </HalfColumn>
              </InputColumn>
            </FormGroup>
          }
          {
            type !== 'PUBLIC_HOLIDAY' && type !== 'ABSENSE' &&
            <div>
              <Divider dashed color="#9EA7AF" />
              <FormGroup>
                <LabelColumn>{formatMessage(messages.Repeat)}</LabelColumn>
                <InputColumn>
                  <Field
                    name="repeated"
                    value="true"
                    type="radio"
                    component={renderRadioField}
                    parse={parseBooleans}
                    format={formatBooleans}
                    label={formatMessage(messages.Yes)}
                  />
                  <Field
                    name="repeated"
                    value="false"
                    type="radio"
                    component={renderRadioField}
                    parse={parseBooleans}
                    format={formatBooleans}
                    label={formatMessage(messages.No)}
                  />
                </InputColumn>
              </FormGroup>
            </div>
          }
          {
            repeated && type !== 'PUBLIC_HOLIDAY' && type !== 'ABSENSE' &&
            <div>
              <FormGroup>
                <LabelColumn />
                <InputColumn>
                  <LabelSubColumn>{formatMessage(messages.Frequency)}{!isEdit && <CustomRequiredStar />}</LabelSubColumn>
                  <HalfColumn>
                    <Field
                      name="frequency"
                      component={renderSelectField}
                      placeholder={formatMessage(messages.FrequencyPlaceholder)}
                      required={repeated}
                      disabled={!repeated}
                      options={[
                        {
                          value: 1,
                          label: `${formatMessage(messages.SelectLabelEvery)} ${formatMessage(messages.SelectLabelWeek)}`
                        },
                        ...Array(11).fill(null).map((x, i) => ({
                          value: i + 2,
                          label: `${formatMessage(messages.SelectLabelEvery)} ${i + 2} ${formatMessage(messages.SelectLabelWeeks)}`
                        }))
                      ]}
                    />
                  </HalfColumn>
                </InputColumn>
              </FormGroup>
              <FormGroup>
                <LabelColumn />
                <InputColumn>
                  <LabelSubColumn
                    borderError={(!repeatOn || repeatOn && repeatOn.every(x=>!x)) && isFormSubmitted }>{formatMessage(messages.RepeatOn)}{!isEdit &&
                  <CustomRequiredStar />}</LabelSubColumn>
                  {['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'].map((day, index) =>
                    <Field
                      name={`repeatOn[${index}]`}
                      component={renderCheckboxField}
                      disabled={!repeated}
                      label={day}
                      checkboxStyle={{ width: '30px' }}
                      key={index}
                      style={{marginLeft: index===0 ? '-4px' :'0'}}
                    />
                  )}
                </InputColumn>
              </FormGroup>
              <FormGroup>
                <LabelColumn />
                <InputColumn>
                  <LabelSubColumn>{formatMessage(messages.EndDate)}</LabelSubColumn>
                  <div style={{display: 'flex'}}>
                    <HalfColumn>
                      <Field
                        name="hasEndDate"
                        type="radio"
                        component={renderRadioField}
                        value="true"
                        disabled={!repeated}
                        parse={parseBooleans}
                        format={formatBooleans}
                        style={{ width: '20px', height: '36px' }}
                      />
                      <WithLeftPadding>
                        <Field name="endDate" component={renderDatePicker} disabled={!repeated || !hasEndDate} />
                      </WithLeftPadding>
                    </HalfColumn>
                    <WithLeftPadding>
                      <Field
                        name="hasEndDate"
                        type="radio"
                        component={renderRadioField}
                        value="false"
                        disabled={!repeated}
                        parse={parseBooleans}
                        format={formatBooleans}
                        label={formatMessage(messages.NoEndDate)}
                      />
                    </WithLeftPadding>
                  </div>
                </InputColumn>
              </FormGroup>
            </div>
          }
          <Divider dashed color="#9EA7AF" />
          <FormGroup>
            <LabelColumn>{formatMessage(messages.Title)}</LabelColumn>
            <InputColumn>
              <HalfColumn>
                <Field name="title" component={renderInputField} placeholder={formatMessage(messages.PatternTitle)} />
              </HalfColumn>
            </InputColumn>
          </FormGroup>
        </ContentWrap>
        <CardFooter>
          <ContentWrap padding="10px 30px 10px 30px">
            <Row justifyContent="space-between" center noPadding>
              <Col>
                { onDelete && <DeleteButton type="button" disabled={!canEdit} onClick={onDelete}>
                  {formatMessage(messages.DeletePattern)}
                </DeleteButton>}
              </Col>
              <Col>
                <div>
                  <GreyButton onClick={onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                  <GreenButton type="submit" disabled={pristine || !canEdit} onClick={props.onSubmitClick}>
                    {onDelete ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddPattern)}
                  </GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </form>
    </Card>);
};

SchedulePatternForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
}

SchedulePatternForm.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  onDelete: PropTypes.func,
  pristine: PropTypes.bool,
  repeated: PropTypes.bool,
  allDay: PropTypes.bool,
  forStaff: PropTypes.bool,
};


export default SchedulePatternForm;
export { default as validate } from './validate';
