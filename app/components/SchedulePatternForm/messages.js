/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  SchedulePatterns: {
    id: 'SchedulePatterns',
    defaultMessage: 'Schedule Patterns',
  },
  NewSchedulePattern: {
    id: 'NewSchedulePattern',
    defaultMessage: 'New Schedule Pattern',
  },
  OpeningHours: {
    id: 'OpeningHours',
    defaultMessage: 'Opening Hours',
  },
  WorkingHours: {
    id: 'WorkingHours',
    defaultMessage: 'Working Hours',
  },
  ClosureHours: {
    id: 'ClosureHours',
    defaultMessage: 'Closure Hours',
  },
  PublicHoliday: {
    id: 'PublicHoliday',
    defaultMessage: 'Public Holiday',
  },
  NonWorking: {
    id: 'NonWorking',
    defaultMessage: 'Non-Working',
  },
  Pause: {
    id: 'Pause',
    defaultMessage: 'Pause',
  },
  Absence: {
    id: 'Absence',
    defaultMessage: 'Absence',
  },
  Type: {
    id: 'Type',
    defaultMessage: 'Type',
  },
  Time: {
    id: 'Time',
    defaultMessage: 'Time',
  },
  Repeat: {
    id: 'Repeat',
    defaultMessage: 'Repeat',
  },
  RepeatOn: {
    id: 'RepeatOn',
    defaultMessage: 'Repeat on',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  Pattern: {
    id: 'Pattern',
    defaultMessage: 'Pattern',
  },
  PatternTitle: {
    id: 'PatternTitle',
    defaultMessage: 'Pattern title',
  },
  EditPattern: {
    id: 'EditPattern',
    defaultMessage: 'Edit pattern',
  },
  StartDate: {
    id: 'SchedulePatternForm.StartDate',
    defaultMessage: '(Start) Date'
  },
  EndDate: {
    id: 'SchedulePatternForm.EndDate',
    defaultMessage: 'End Date'
  },
  NoEndDate: {
    id: 'SchedulePatternForm.NoEndDate',
    defaultMessage: 'No End Date'
  },
  SingleDay: {
    id: 'SchedulePatternForm.SingleDay',
    defaultMessage: 'Single Day'
  },
  TimePlaceholder: {
    id: 'TimePlaceholder',
    defaultMessage: 'HH:MM'
  },
  Yes: {
    id: 'Yes',
    defaultMessage: 'Yes'
  },
  No: {
    id: 'No',
    defaultMessage: 'No'
  },
  Frequency: {
    id: 'SchedulePatternForm.Frequency',
    defaultMessage: 'Frequency'
  },
  FrequencyPlaceholder: {
    id: 'SchedulePatternForm.Frequency.Placeholder',
    defaultMessage: 'Select Frequency'
  },
  SelectLabelEvery: {
    id: 'SchedulePatternForm.Frequency.SelectLabel.Every',
    defaultMessage: 'Every'
  },
  SelectLabelWeek: {
    id: 'SchedulePatternForm.Frequency.SelectLabel.Week',
    defaultMessage: 'Week'
  },
  SelectLabelWeeks: {
    id: 'SchedulePatternForm.Frequency.SelectLabel.Weeks',
    defaultMessage: 'Weeks'
  },
  DeletePattern: {
    id: 'SchedulePatternForm.DeletePattern',
    defaultMessage: 'Delete Pattern'
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes'
  },
  AddPattern: {
    id: 'SchedulePatternForm.AddPattern',
    defaultMessage: 'Add Pattern'
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel'
  }
});
