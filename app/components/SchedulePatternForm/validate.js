const validate = (values) => {
  const errors = {};
  if (values.timeFrom > values.timeTo) {
    errors.timeFrom = 'Previous time interval have to be less then next one';
    errors.timeTo = 'Previous time interval have to be less then next one';
  }
  values.repeatOn && values.repeatOn.forEach(x=>{
    });

  if (!values.timeTo) {
    errors.timeTo = 'not defined';
  }
  if (!values.timeFrom) {
    errors.timeFrom = 'not defined';
  }
  if (!values.startDate) {
    errors.startDate = 'not defined';
  }
  if (!values.frequency && values.repeated) {
    errors.frequency = 'frequency is not defined';
  }
  if ((!values.repeatOn || values.repeatOn && values.repeatOn.every(x=>!x)) && values.repeated) {
    errors.repeatOn = 'frequency is not defined';
  }

  return errors;
};


export default validate;
