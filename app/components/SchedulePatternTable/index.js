import React, { PropTypes } from 'react';
import { TimeTable, TR, TD as TDComponent, TH as THComponent } from 'components/Table';
import messages from './messages';
import moment from 'moment';
import CheckIcon from 'assets/icons/CheckDark.svg';
import OpenIcon from 'assets/icons/ScheduleType/Working.svg';
import HolidayIcon from 'assets/icons/ScheduleType/Holiday.svg';
import Closed from 'assets/icons/ScheduleType/Absent-Closed.svg';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';


const TD = styled(TDComponent)`
  padding: 15px 15px 15px 15px;
`;

const TDWithoutTextBreak = styled(TD)`
  white-space: nowrap;
`;


const TH = styled(THComponent)`
  padding: 15px 15px 15px 15px;
`;

const getTypeLabel = (type) => {
  switch (type) {
    case 'WORKING_HOURS':
      return <OpenIcon />;
    case 'NON_WORKING_HOURS':
      return <Closed />;
    case 'ABSENSE':
      return <Closed />;
    case 'PUBLIC_HOLIDAY':
      return <HolidayIcon />;
    default:
      return <span>&mdash;</span>;
  }
};

const getTextType = (type, tableType, formatMessage) => {
  const isCompanySchedule = tableType === 'OpeningHours';
  switch (type) {
    case 'WORKING_HOURS':
      return isCompanySchedule ? formatMessage(messages.Open) : formatMessage(messages.Working);
    case 'NON_WORKING_HOURS':
      return isCompanySchedule ? formatMessage(messages.Closed) : formatMessage(messages.Pause);
    case 'PUBLIC_HOLIDAY':
      return isCompanySchedule ? formatMessage(messages.Holiday) : formatMessage(messages.Absent);
    case 'ABSENSE':
      return formatMessage(messages.Absent);
    default:
      return <span>&mdash;</span>;
  }
};

const getColumnWidth = () => ([
    { name: 'Type', value: 15 },
    { name: 'Start Date', value: 17 },
    { name: 'Time', value: 20 },
    { name: 'Repeat', value: 5 },
    { name: 'End Date', value: 18 },
    { name: 'Title', value: 25 },
  ].map(item => item.value));

const SchedulePatternList = (props, context) => {
  const { onRowClick, tableType } = props;
  const { formatMessage } = context.intl;
  return (
    <TimeTable>
      {getColumnWidth(props.isEdit).map((width,index) => <col key={index} width={`${width}%`}/>)}
      <thead>
        <TR>
          <TH style={{paddingLeft:'35px'}}>{formatMessage(messages.Type)}</TH>
          <TH>{formatMessage(messages.StartDate)}</TH>
          <TH centered>{formatMessage(messages.Time)}</TH>
          <TH centered>{formatMessage(messages.Repeat)}</TH>
          <TH centered>{formatMessage(messages.EndDate)}</TH>
          <TH centered>{formatMessage(messages.Title)}</TH>
        </TR>
      </thead>
      <tbody>
      {props.items.map((row, index) => {
        const durationFrom = moment.duration(row.timeFrom, 'minutes');
        const durationTo = moment.duration(row.timeTo, 'minutes');

        return (
          <TR key={index} onClick={() => onRowClick(row._id)}>
            <TD style={{textAlign: 'left'}}>{getTypeLabel(row.type)} {getTextType(row.type, tableType, formatMessage)}</TD>
            <TDWithoutTextBreak>{moment(row.startDate).format('dd, D MMM YYYY')}</TDWithoutTextBreak>
            <TD centered>
              {moment().hours(durationFrom.hours()).minutes(durationFrom.minutes()).format('HH:mm')}
              {' - '}
              {moment().hours(durationTo.hours()).minutes(durationTo.minutes()).format('HH:mm')}
            </TD>
            <TD centered>{row.repeated ? <CheckIcon /> : <span>&mdash;</span>}</TD>
            <TD centered>
              {row.hasEndDate ? moment(row.endDate).format('dd, D MMM YYYY') : <span>&mdash;</span>}
            </TD>
            <TD centered>{row.title || <span>&mdash;</span>}</TD>
          </TR>
        );
      })}
      </tbody>
    </TimeTable>
  );
};

SchedulePatternList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

SchedulePatternList.propTypes = {
  items: PropTypes.array,
  onRowClick: PropTypes.func,
};


SchedulePatternList.defaultProps = {
  items: [],
  equipment: [],
};


export default SchedulePatternList;
