import { defineMessages } from 'react-intl';

export default defineMessages({
  StartDate: {
    id: 'SchedulePatternForm.StartDate',
    defaultMessage: '(Start) Date',
  },
  Time: {
    id: 'Time',
    defaultMessage: 'Time',
  },
  Type: {
    id: 'Type',
    defaultMessage: 'Type',
  },
  Repeat: {
    id: 'Repeat',
    defaultMessage: 'Repeat',
  },
  EndDate: {
    id: 'SchedulePatternForm.EndDate',
    defaultMessage: 'End Date',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  Open: {
    id: 'Open',
    defaultMessage: 'Open'
  },
  Working: {
    id: 'Working',
    defaultMessage: 'Working'
  },
  Closed: {
    id: 'Closed',
    defaultMessage: 'Closed'
  },
  Holiday: {
    id: 'Holiday',
    defaultMessage: 'Holiday'
  },
  Absent: {
    id: 'Absent',
    defaultMessage: 'Absent'
  },
  Pause: {
    id: 'Pause',
    defaultMessage: 'Pause'
  }
});
