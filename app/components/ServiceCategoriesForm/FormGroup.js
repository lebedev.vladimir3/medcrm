import styled from 'styled-components';

const FormGroup = styled.div`
  display: flex;
  margin-bottom: 15px;
  padding-left: 29px;
  padding-right: 29px;
  @media (max-width: 900px) {
    flex-direction: column;
    margin-bottom: 0;
  }
`;


export default FormGroup;
