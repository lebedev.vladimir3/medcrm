import fontSetting from 'components/styles/font';
/**
 *
 * ServiceCategoriesForm
 *
 */

import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { Field, FieldArray } from 'redux-form';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, ColorInput } from 'components/Input';
import { Table, TR, TH, TD } from 'components/Table';
import FaTrashO from 'assets/icons/RecycleBinRed.svg';
import AddCircle from 'assets/icons/AddIcon/Active.svg';
import { Card, CardFooter } from 'components/styles/Card';
import { ContentWrap } from 'components/styles/Containers';
import { Row, Col } from 'components/styles/Grid';

import messages from './messages';

const TRHead = styled(TR)`
  height: 50px;
  border-top-style: none;
`;

const THCentered = styled(TH)`
  text-align: center;
`;

const TDCentered = styled(TD)`
  text-align: center;
`;
const TDPadding = styled(TD)`
  padding-top: 12px;
  padding-bottom: 12px;
`;

const Trash = styled(FaTrashO)`
  color: #D46659;
  cursor: pointer; 
`;

const StyledAdd = styled.div`
  display: inline-block;
  color: #3E7DBC;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  text-align: center;
  cursor: pointer;
`;

const renderInputField = (inputProps) => {
  const { input } = inputProps;
  return <Input {...input} {...inputProps} />;
};


const renderColorInputField = (props) => {
  const { value, onChange } = props.input;
  return <ColorInput color={value} onChange={value => onChange(value)} />;
};


const renderCategories = (props, context) => (
  <tbody>
    {props.fields.map((category, index) => (
      <TR key={index}>
        <TDPadding style={{width: '100px'}} offLink={true}>
          <Field name={`categories[${index}].color`} component={renderColorInputField} />
        </TDPadding>
        <TDPadding>
          <Field name={`categories[${index}].name`} component={renderInputField} placeholder="Category name" required style={{width: '270px'}}/>
        </TDPadding>
        <TDCentered offLink={true}>
          <Trash size="23" onClick={() => {props.fields.remove(index)}} />
        </TDCentered>
      </TR>
    ))}
    <TR>
      <TD colSpan={5} offLink={true}>
        <StyledAdd {...props} onClick={() => props.fields.push({})}>
          <AddCircle size="23" style={{ margin: '5px 5px 8px -5px' }} />
          {context.intl.formatMessage(messages.AdditionalCategory)}...
        </StyledAdd>
      </TD>
    </TR>
  </tbody>
);


const ServiceCategoriesForm = (props, context) => {
  const { handleSubmit, onSubmit, onCancel, pristine } = props;
  return (
    <Card>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Table>
          <thead>
            <TRHead>
              <TH>{context.intl.formatMessage(messages.Colour)}</TH>
              <TH left>{context.intl.formatMessage(messages.CategoryName)}</TH>
              <THCentered>{context.intl.formatMessage(messages.Delete)}</THCentered>
            </TRHead>
          </thead>
          <FieldArray name="categories" component={renderCategories} />
        </Table>
        <CardFooter style={{ marginTop: 0 }}>
          <ContentWrap padding="10px 30px 10px 30px" style={{display: 'flex'}}>
            <Row justifyContent="space-between" center noPadding>
              <Col>
                { false &&
                <DeleteButton type="button">
                </DeleteButton>
                }
              </Col>
              <Col>
                <div>
                  <GreyButton onClick={onCancel}>{context.intl.formatMessage(messages.Cancel)}</GreyButton>
                  <GreenButton disabled={pristine} type="submit">{context.intl.formatMessage(messages.SaveChanges)}</GreenButton>
                </div>
              </Col>
            </Row>
          </ContentWrap>
        </CardFooter>
      </form>
    </Card>
  );
};

ServiceCategoriesForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
renderCategories.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

ServiceCategoriesForm.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  pristine: PropTypes.bool,
};


ServiceCategoriesForm.defaultProps = {
  categories: [],
};

export default ServiceCategoriesForm;
export { default as validate } from './validate';
