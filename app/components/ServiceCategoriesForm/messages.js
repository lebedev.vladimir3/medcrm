/*
 * ServiceCategoriesForm Messages
 *
 * This contains all the text for the ServiceCategoriesForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ServiceCategoriesForm.header',
    defaultMessage: 'This is the ServiceCategoriesForm component !',
  },
  Colour: {
    id: 'Colour',
    defaultMessage: 'Colour',
  },
  CategoryName: {
    id: 'CategoryName',
    defaultMessage: 'Category Name',
  },
  Delete: {
    id: 'Delete',
    defaultMessage: 'Delete',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  AdditionalCategory: {
    id: 'AdditionalCategory',
    defaultMessage: 'Additional Category',
  },
});
