const validate = (values) => {
  const errors = {};

  if (values.categories) {
    errors.categories = [];
    values.categories.forEach((category, index) => {
      if (!category.name) {
        errors.categories[index] = { name: 'Name value is required' };
      }
    });
  }
  console.log(errors)
  return errors;
};


export default validate;
