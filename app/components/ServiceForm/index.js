import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';

import styled from 'styled-components';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Field, FieldArray } from 'redux-form';
import PlusCircle from 'assets/icons/AddIcon/Active.svg';
import Trash from 'assets/icons/RecycleBinRed.svg';
import AlternativeAdd from 'assets/icons/AlternativeIcon/Active.svg';
import { Input, ColorInput, Select } from 'components/Input';
import messages from './messages';
import { Card, CardFooter } from 'components/styles/Card';
import { ContentWrap } from 'components/styles/Containers';
import { RowsWrap, Row as RowComp, Col } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';
import Obligatory from 'components/common/obligatoryLegend';
import Divider from 'components/common/Divider';

import './style.scss';
const Row = styled(RowComp)`
  position: relative;
`;

const renderInputField = (inputProps) => {
  const { input } = inputProps;
  return (
    <div style={{ width: '100%' }}>
      <Input {...input} {...inputProps} />
    </div>
  );
};

const renderSelectField = (renderProps) => {
  const { input } = renderProps;
  return (
    <Select {...input} {...renderProps} style={{marginLeft: 0}} />
  );
};

const FieldLabel = styled.div`
  color: #7E7E7E;
  font-size: ${fontSetting.baseSize}px;
  line-height: 19px;
`;

const TrashButton = styled.button`
  position: absolute;
  right: 0;
  padding: 0;
  visibility:${props=>props.disabled ? 'hidden' : 'visible'};
`;

const AddButton = styled.button`
  text-align: left;
  opacity:${props=>props.addDisable ? '0.5' : '1'};
	color: #4A69AE;
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
`;

const OrDiv = styled.div`
  position: absolute;
  top: 10px;
  color: #626262;
  left: -25px;
	font-size: ${fontSetting.baseSizePlus1}px;
	line-height: 19px;
`;

const FieldsWrapCol = styled(Col).attrs({
  padding: 0,
  grow: "1",
  width: '560px'
})`
  & > .grid__row {
    padding-bottom: 0px;
    & > .grid__column {
      padding: 0px 6px;
      &:first-child {
        padding-left: 0px;
      }
      &:last-child {
        padding-right: 0px;
      }
    }
  }
`;

const LabelCol = styled(Col).attrs({
  width: "213px",
  textRight: true,
  padding: [0, 40, 0, 0],
})``;

const LabelColTopOffset = styled(LabelCol)`
  top: 10px;
  position: relative;
`;

const renderColorInputField = (props) => {
  const { value, onChange } = props.input;
  return <ColorInput color={value} onChange={(value1) => onChange(value1)} />;
};


class MediclServicesContainer extends React.Component {
  constructor(props, context) {
    super(props, context);


    this.getRoleList = this.getRoleList.bind(this);
    this.onStaffChange = this.onStaffChange.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onInsuranceChange = this.onInsuranceChange.bind(this);
    this.getSuitableRooms = this.getSuitableRooms.bind(this);

    this.state = {
      hideColor: true,
      color: 'white',
      staff: this.props.staff ? this.props.staff : [],
      allInsuranceChosen: false,
      allInsuranceChosenIndex: -1,
      selectStaffForJoin: [],
      connectedJoin: [],
      staffForJoin: {},
      isAllStaffChosen: false,
      isAnyRoomChosen: false,
      arrayIndex: -1,
      arrayIndexRoom: -1,
      showModal: false,
    };
    this.state[`hideUserMenu${0}`] = false;
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.staff !== nextProps.staff) {
      this.setState({
        staff: nextProps.staff,
      });
    }
    const roomList = this.getSuitableRooms().map(room => ({ room: room._id }));

    if (JSON.stringify(this.props.suitableRooms) !== JSON.stringify(roomList)) {
      this.props.change('suitableRoom', roomList);
    }

    const staffAllocationDisabled = (typeof this.props.staffAllocation) === 'undefined';

    if (!staffAllocationDisabled && this.state.arrayIndex !== -1) {
      for (let i = this.state.arrayIndex - 1; i >= 0; i--) {
        this.props.array.shift('staffAllocation');
        this.setState({
          arrayIndex: this.state.arrayIndex - 1,
        });
        break;
      }
      if (this.state.arrayIndex === 0) {
        for (let i = 1; i < this.props.staffAllocation.length - 1; i++) {
          this.props.array.pop('staffAllocation');
          break;
        }
      }
    }
    for (let i = this.state.arrayIndexRoom - 1; i >= 0; i--) {
      this.props.array.shift('roomAllocation');
      this.setState({
        arrayIndexRoom: this.state.arrayIndexRoom - 1,
      });
      break;
    }
    if (nextProps.services.length !== 0) {
      this.props.change('services', nextProps.services);
    }
  }

  onCategoryChange(e, newValue) {
    const filter = this.props.categories.filter((category) => category._id === newValue);
    const color = filter[0].color;
    this.props.change('categoryColor', color);
  }

  onInsuranceChange(newValue) {
    let allInsuranceChosen = false;
    const allInsuranceChosenIndex = -1;

    this.props.payers.forEach((payer) => {
      if (payer.payer === 'ALL') {
        allInsuranceChosen = true;
      }
    });

    if (newValue === 'ALL') {
      allInsuranceChosen = true;
    }

    this.setState({ allInsuranceChosen, allInsuranceChosenIndex });
  }

  onStaffChange(newValue, index) {
    if (newValue === 'any staff of this role') {
      const staffToSave = this.props.staffAllocation[index];
      staffToSave.isAnyStaff = true;
      staffToSave.staff = 'any staff of this role';

      this.props.change(`staffAllocation[${index}]`, staffToSave);
      this.setState({
        isAllStaffChosen: true,
        arrayIndex: index,
      });
    } else {
      this.props.change(`staffAllocation[${index}].isAnyStaff`, false);
    }
  }

  onRoomChange(newValue, index) {
    if (newValue === 'Any passing room') {
      this.props.change('isAnyRoom', true);
      this.setState({
        arrayIndexRoom: index,
      });
    } else {
      this.props.change('isAnyRoom', false);
    }
  }

  getCategoryList() {
    return this.props.categories.map((role, index) => ({
      value: role._id,
      label: role.name
    }));
  }

  getRoleList() {
    return this.props.roles.map((role, index) => ({
      value: role._id,
      label: role.name
    }));
  }

  getSuitableRooms() {
    const locEq = this.props.equipments.filter((eq) => {
      let isProper = false;
      if (this.props.equipmentAllocation) {
        this.props.equipmentAllocation.forEach((eA) => {
          if (eA._id === eq._id && !eq.mobility) {
            isProper = true;
          }
        });
      }
      return isProper;
    });

    const roomListFrom = this.props.rooms.filter((room) => {
      let canUse = false;
      locEq.forEach((eq) => {
        if (eq.room._id === room._id) {
          canUse = true;
        }
      });
      return canUse;
    });

    return roomListFrom;
  }

  payersFields = ({ fields }) => {
    const payers = this.props.payers;
    const payerDisabled = (typeof this.props.payers) === 'undefined';
    let addDisable = true;

    if (!payerDisabled) {
      addDisable = !this.props.payers[fields.length - 1].duration || !this.props.payers[fields.length - 1].price;
    }

    return (<div>
      {
        fields.map((field, index) => {
          let durationDisable = true;
          let deleteDisable = true;
          if (!payerDisabled) {
            durationDisable = this.props.payers[index].payer ? false : true;
            deleteDisable = this.props.payers.length === 1 || !this.props.payers[index].duration || !this.props.payers[index].price;
          }

          const filterPayers = function(value){
            let helper = true;
            for (let i = 0; i < payers.length; i++){
              if (payers[i].payer == value.value && i != index) {
                helper = false;
                break;
              }
            }
            return helper;
          }

          let payerOptions = [
            { value: "ALL", label: "All Insurances" },
            { value: "1", label: "1 - Government Insurance" },
            { value: "3", label: "3 - Government Insurance" },
            { value: "5", label: "5 - Government Insurance" },
            { value: "7", label: "7 - Government Insurance" },
            { value: "P", label: "P - Private Insurance" },
          ].filter(filterPayers)
          //console.log('payers', this.props.payers, index)
          /*if (this.props.payers[index].payer === 'ALL' || !this.state.allInsuranceChosen) {
            payerOptions.unshift({ value: "ALL", label: "All Insurances" })
          }*/
          return (
            <Row center key={index}>
              <LabelColTopOffset style={{visibility: index !== 0 ? 'hidden' : 'visible'}}>
                <FormLabel required>{this.context.intl.formatMessage(messages.Payer)}</FormLabel>
              </LabelColTopOffset>
              <FieldsWrapCol>
                <Row>
                  <Col width="44%">
                    <FieldLabel style={{ display: index === 0 ? 'block' : 'none' }}>
                      {this.context.intl.formatMessage(messages.Status)}
                    </FieldLabel>
                    <Field
                      name={`${field}.payer`} component={renderSelectField}
                      placeholder={this.context.intl.formatMessage(messages.SelectStatus)}
                      onChange={(e, newValue) => this.onInsuranceChange(newValue, index)}
                      options={payerOptions}
                    />
                  </Col>
                  <Col width="25%" style={{visibility: durationDisable ?'hidden' : 'visible'}}>
                    <FieldLabel style={{ marginLeft:0,  display: index === 0 ? 'block' : 'none'   }}>
                      {this.context.intl.formatMessage(messages.ServiceDuration)}
                    </FieldLabel>

                    <Field
                      name={`${field}.duration`}
                      component={renderInputField}
                      type="number"
                      placeholder={this.context.intl.formatMessage(messages.Duration)}
                      disabled={durationDisable}
                      required
                    />
                  </Col>

                  <Col width="25%" style={{visibility: durationDisable ?'hidden' : 'visible'}}>

                    <FieldLabel style={{ marginLeft:0,  display: index === 0 ? 'block' : 'none'   }}>
                      {this.context.intl.formatMessage(messages.TotalPrice)}
                    </FieldLabel>
                    <Field
                      name={`${field}.price`}
                      component={renderInputField}
                      placeholder={this.context.intl.formatMessage(messages.Price)}
                      disabled={durationDisable}
                      required
                    />
                  </Col>

                  <TrashButton
                    type="button"
                    disabled={deleteDisable || fields.length <2}
                    onClick={() => { fields.remove(index); }}
                    style={{top: index===0 ? '25px' : '8px'}}
                  >
                    <Trash deleteDisable={deleteDisable} />
                  </TrashButton>
                </Row>
              </FieldsWrapCol>
            </Row>);
        })
      }
      {!addDisable && payers.length < 6 && <Row center key={-1}>
        <LabelCol style={{visibility: 'hidden' }}>
          <FormLabel required>{this.context.intl.formatMessage(messages.Payer)}</FormLabel>
        </LabelCol>
        <FieldsWrapCol>
          <Row>
            <Col width="44%">
              <AddButton
                onClick={() => {this.state.allInsuranceChosen ? fields.push({}) : fields.push({})}}
                addDisable={addDisable}
                disabled={addDisable}
                type="button"
              >
                <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
                {this.context.intl.formatMessage(messages.AdditionalPayer)}…
              </AddButton>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Row>}
    </div>);
  };

  staffAllocationFields = ({ fields }) => {
    const { staffAllocation } = this.props;
    const staffAllocationDisabled = (typeof staffAllocation) === 'undefined';

    let addDisable = true;
    if (!staffAllocationDisabled) {
      addDisable = !staffAllocation[fields.length - 1].role || !staffAllocation[fields.length - 1].staff;
    }
    let staffList = [];

    return (<div>
      {
        !staffAllocationDisabled && fields.map((field, index) => {
          let staffDisable = true;
          let deleteDisable = true;

          if (!staffAllocationDisabled) {
            staffDisable = !staffAllocation[index].role;
            deleteDisable = staffAllocation.length === 1 || !staffAllocation[index].staff;
            if (staffAllocation[index].role) {
              staffList = this.props.staff.filter((staff) =>
              staff.role._id === staffAllocation[index].role);

              staffList = staffList.map((staff, index1) => ({
                value: staff._id,
                label: staff.lastName + ' ' + staff.firstName
              }));

              if (staffList.length > 0) {
                staffList.unshift({
                  value: this.context.intl.formatMessage(messages.AnyStaffOfThisRole),
                  label: this.context.intl.formatMessage(messages.AnyStaffOfThisRole),
                });
              }
            }
          }

          return (
            <Row center key={index}>
              <LabelColTopOffset style={{visibility: index !== 0 ? 'hidden' : 'visible'}}>
                <FormLabel required>{this.context.intl.formatMessage(messages.Staff)}</FormLabel>
              </LabelColTopOffset>
              <FieldsWrapCol>
                <Row>
                  <Col width="44%" style={{position: 'relative'}}>
                    {index > 0 && <OrDiv>{this.context.intl.formatMessage(messages.Or)}</OrDiv>}
                    <FieldLabel style={{ display: index === 0 ? 'block' : 'none' }}>
                      {this.context.intl.formatMessage(messages.Role)}
                    </FieldLabel>
                    <Field
                      name={`${field}.role`}
                      component={renderSelectField}
                      placeholder={this.context.intl.formatMessage(messages.SelectRole)}
                      options={this.getRoleList()}
                      disabled={true}
                    />
                  </Col>
                  {!staffDisable && <Col width="50%">
                    <FieldLabel style={{ marginLeft:0,  display: index === 0 ? 'block' : 'none'   }}>
                      {this.context.intl.formatMessage(messages.Name)}
                    </FieldLabel>
                    <Field
                      name={`${field}.staff`}
                      component={renderSelectField}
                      placeholder={this.context.intl.formatMessage(messages.SelectStaff)}
                      disabled={staffDisable}
                      onChange={(e, newValue) => {
                      this.onStaffChange(newValue, index, fields)
                    }}
                      options={staffList}
                    />
                  </Col>}
                  <TrashButton
                    disabled={deleteDisable}
                    onClick={() => { fields.remove(index); }}
                    style={{top: index===0 ? '25px' : '8px'}}
                  >
                    <Trash deleteDisable={deleteDisable} />
                  </TrashButton>
                </Row>
              </FieldsWrapCol>
            </Row>);
        })
      }
      {!addDisable && <Row center key={-1}>
        <LabelCol style={{visibility: 'hidden' }}>
          <FormLabel required>{this.context.intl.formatMessage(messages.Payer)}</FormLabel>
        </LabelCol>
        <FieldsWrapCol>
          <Row>
            <Col width="44%">
              <AddButton
                type="button"
                onClick={() => {fields.push({ role: null })}}
                disabled={addDisable || this.state.isAllStaffChosen}
                addDisable={addDisable || this.state.isAllStaffChosen}
              >
                <AlternativeAdd style={{ marginRight: '10px' }} />
                {this.context.intl.formatMessage(messages.AlternativeStaff)}…
              </AddButton>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Row>}
    </div>);
  };

  equipmentFields = ({ fields }) => {
    let { equipmentAllocation } = this.props;
    const equipmentAllocationDisabled = (typeof equipmentAllocation) === 'undefined';
    let addDisable = true;
    let equipmentDisable = true;
    if (!equipmentAllocationDisabled) {
      addDisable = !equipmentAllocation[fields.length - 1].room || !equipmentAllocation[fields.length - 1].equipment;
    }

    let equipmentList = this.props.equipments.map((equipment, index) => ({
      value: equipment._id,
      label: equipment.name,
    }));

    const roomListFrom = this.props.rooms;

    const roomList = roomListFrom.map((room, index) => ({
      value: room._id,
      label: room.number
    }));

    return (<div>
      {
        fields.map((field, index) => {
          let deleteDisable = true;
          const currentRoom = equipmentAllocation && equipmentAllocation[index].room;

          equipmentList = this.props.equipments
            .filter((eq) => eq.room && (eq.room._id === currentRoom || eq.alternativeRooms.some((room) => room._id === currentRoom)))
            .map((equipment, index1) => ({
              value: equipment._id,
              label: equipment.name
            }));
          equipmentList.unshift({ value: '', label: this.context.intl.formatMessage(messages.WithoutEquipment) });
          if (!equipmentAllocationDisabled) {
            equipmentDisable = !equipmentAllocation[index].room;
            deleteDisable = equipmentAllocation.length === 1 || !equipmentAllocation[index].equipment;
          }
          return (
            <Row center key={index}>
              <LabelColTopOffset style={{visibility: index !== 0 ? 'hidden' : 'visible'}}>
                <FormLabel required>{this.context.intl.formatMessage(messages.Resources)}</FormLabel>
              </LabelColTopOffset>
              <FieldsWrapCol>
                <Row>
                  <Col width="44%">
                    {index > 0 && <OrDiv>{this.context.intl.formatMessage(messages.Or)}</OrDiv>}
                    <FieldLabel style={{ display: index === 0 ? 'block' : 'none' }}>
                      {this.context.intl.formatMessage(messages.Room)}
                    </FieldLabel>
                    <Field
                      name={`${field}.room`} component={renderSelectField}
                      placeholder={this.context.intl.formatMessage(messages.SelectRoom)}
                      onChange={(e, newValue) => this.onRoomChange(newValue, index, fields)}
                      options={roomList}
                    />
                  </Col>
                  {!equipmentDisable && <Col width="50%">
                    <FieldLabel style={{ marginLeft:0,  display: index === 0 ? 'block' : 'none'   }}>
                      {this.context.intl.formatMessage(messages.Equipment)}
                    </FieldLabel>
                    <Field
                      name={`${field}.equipment`}
                      placeholder={this.context.intl.formatMessage(messages.SelectEquipment)}
                      component={renderSelectField}
                      options={equipmentList}
                    />
                  </Col>}
                  <TrashButton
                    disabled={deleteDisable}
                    onClick={() => { fields.remove(index); }}
                    style={{top: index===0 ? '25px' : '8px'}}
                  >
                    <Trash deleteDisable={deleteDisable} />
                  </TrashButton>
                </Row>
              </FieldsWrapCol>
            </Row>);
        })
      }
      {!addDisable && <Row center key={-1}>
        <LabelCol style={{visibility: 'hidden' }}>
          <FormLabel>{this.context.intl.formatMessage(messages.Payer)}</FormLabel>
        </LabelCol>
        <FieldsWrapCol>
          <Row>
            <Col width="44%">
              <AddButton
                type="button"
                onClick={() => { fields.push({}); }}
                disabled={addDisable}
                addDisable={addDisable}
              >
                <AlternativeAdd style={{ marginRight: '10px' }} />
                {this.context.intl.formatMessage(messages.AlternativeResources)}…
              </AddButton>
            </Col>
          </Row>
        </FieldsWrapCol>
      </Row>}
    </div>);
  };

  render() {
    const { handleSubmit, submitClick, isEdit } = this.props;
    const { formatMessage } = this.context.intl;
    const maxLength = (max) => (value) =>
      value && value.length > max ? `Must be ${max} characters or less` : undefined;
    const maxDefLength = maxLength(30);
    const maxLongLength = maxLength(255);
    const maxShortLength = maxLength(10);

    return (
      <Card>
        <form onSubmit={handleSubmit(submitClick)}>
          <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 10px`}>
            <Obligatory />
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel required>{formatMessage(messages.Category)}, {formatMessage(messages.Colour)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="44%">
                      <Field
                        name="category"
                        component={renderSelectField}
                        placeholder={formatMessage(messages.Category)}
                        onChange={this.onCategoryChange}
                        options={this.getCategoryList()}
                        required
                      />
                    </Col>
                    <Col>
                      <Field
                        name="categoryColor"
                        component={renderColorInputField}
                      />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required>{formatMessage(messages.ServiceName)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="44%">
                      <Field
                        name="name.defaultName"
                        component={renderInputField}
                        placeholder={formatMessage(messages.DefaultName)}
                        validate={[maxDefLength]}
                        required
                      />
                    </Col>
                    <Col width="25%">
                      <Field
                        name="name.shortName"
                        component={renderInputField}
                        placeholder={formatMessage(messages.ShortName)}
                        validate={[maxShortLength]}
                        required
                      />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center style={{paddingBottom: '0px'}}>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.ServiceDescription)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="93%">
                      <Field
                        name="name.longName"
                        component={renderInputField}
                        placeholder={formatMessage(messages.ServiceDescription)}
                        validate={[maxLongLength]}
                      />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Divider dashed dotted marginBottom="15" marginTop="20" marginLeft="20" color="#aaa"/>
              <FieldArray name="payers" component={this.payersFields} />
              <Divider dashed dotted marginBottom="15" marginTop="12" marginLeft="20" color="#aaa"/>
              <FieldArray name="staffAllocation" component={this.staffAllocationFields} required />
              <Divider dashed dotted marginBottom="15" marginTop="12" marginLeft="20" color="#aaa"/>
              <FieldArray name="equipmentAllocation" component={this.equipmentFields} required />

            </RowsWrap>

          </ContentWrap>
          <CardFooter>
            <ContentWrap padding="10px 30px 10px 30px">
              <Row justifyContent="space-between" center noPadding>
                <Col>
                  { isEdit &&
                  <DeleteButton type="button" onClick={this.props.toggleModal}>
                    {formatMessage(messages.DeleteService)}
                  </DeleteButton>
                  }
                </Col>
                <Col>
                  <div>
                    <GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                    <GreenButton type="submit" disabled={this.props.pristine}>
                      {isEdit ? formatMessage(messages.SaveChanges) : formatMessage(messages.AddService)}
                    </GreenButton>
                  </div>
                </Col>
              </Row>
            </ContentWrap>
          </CardFooter>
        </form>
      </Card>);
  }
}

MediclServicesContainer.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

MediclServicesContainer.propTypes = {
  staff: PropTypes.array,
  payers: PropTypes.array,
  rooms: PropTypes.array,
  roles: PropTypes.array,
  equipments: PropTypes.array,
  equipmentAllocation: PropTypes.array,
  staffAllocation: PropTypes.array,
  categories: PropTypes.array,
  pristine: PropTypes.bool,
  duration: PropTypes.string,
  array: PropTypes.object,
  reset: PropTypes.func,
  handleSubmit: PropTypes.func,
  change: PropTypes.func,
  submitClick: PropTypes.func,
};

export { default as validate } from './validate';
export default MediclServicesContainer;
