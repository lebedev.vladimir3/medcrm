/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  Category: {
    id: 'ServiceForm.Category',
    defaultMessage: 'Category',
  },
  Colour: {
    id: 'ServiceForm.Colour',
    defaultMessage: 'Colour',
  },
  ServiceName: {
    id: 'ServiceForm.ServiceName',
    defaultMessage: 'Service Name',
  },
  ServiceDescription: {
    id: 'ServiceForm.ServiceDescription',
    defaultMessage: 'Service Description',
  },
  DefaultName: {
    id: 'ServiceForm.DefaultName',
    defaultMessage: 'Default name',
  },
  ShortName: {
    id: 'ServiceForm.ShortName',
    defaultMessage: 'Short name',
  },
  LongName: {
    id: 'ServiceForm.LongName',
    defaultMessage: 'Long name',
  },
  PayerSettings: {
    id: 'ServiceForm.PayerSettings',
    defaultMessage: 'Payer Settings',
  },
  SelectStatus: {
    id: 'ServiceForm.SelectStatus',
    defaultMessage: 'Select status',
  },
  PayerStatus: {
    id: 'ServiceForm.PayerStatus',
    defaultMessage: 'Payer Status',
  },
  Payer: {
    id: 'Payer',
    defaultMessage: 'Payer',
  },
  Status: {
    id: 'Status',
    defaultMessage: 'Status',
  },
  ServiceDuration: {
    id: 'ServiceForm.ServiceDuration',
    defaultMessage: 'Service Duration',
  },
  Duration: {
    id: 'ServiceForm.Duration',
    defaultMessage: 'Duration',
  },
  TotalPrice: {
    id: 'ServiceForm.TotalPrice',
    defaultMessage: 'Total Price',
  },
  Price: {
    id: 'ServiceForm.Price',
    defaultMessage: 'Price',
  },
  AdditionalPayer: {
    id: 'ServiceForm.AdditionalPayer',
    defaultMessage: 'Additional Payer',
  },
  StaffAllocation: {
    id: 'ServiceForm.StaffAllocation',
    defaultMessage: 'Staff allocation',
  },
  StaffRole: {
    id: 'ServiceForm.StaffRole',
    defaultMessage: 'Staff Role',
  },
  SelectRole: {
    id: 'ServiceForm.SelectRole',
    defaultMessage: 'Select role',
  },
  StaffName: {
    id: 'ServiceForm.StaffName',
    defaultMessage: 'Staff Name',
  },
  SelectStaff: {
    id: 'ServiceForm.SelectStaff',
    defaultMessage: 'Select Staff',
  },
  AlternativeStaff: {
    id: 'AlternativeStaff',
    defaultMessage: 'Alternative Staff',
  },
  ResourcesAllocation: {
    id: 'ServiceForm.ResourcesAllocation',
    defaultMessage: 'Resources Allocation',
  },
  Room: {
    id: 'Room',
    defaultMessage: 'Room',
  },
  Resources: {
    id: 'Resources',
    defaultMessage: 'Resources',
  },
  AlternativeResources: {
    id: 'AlternativeResources',
    defaultMessage: 'Alternative Resources',
  },
  Equipment: {
    id: 'Equipment',
    defaultMessage: 'Equipment',
  },
  SelectRoom: {
    id: 'ServiceForm.SelectRoom',
    defaultMessage: 'Select room',
  },
  SelectEquipment: {
    id: 'ServiceForm.SelectEquipment',
    defaultMessage: 'Select equipment',
  },
  AlternativeRoom: {
    id: 'ServiceForm.AlternativeRoom',
    defaultMessage: 'Alternative Room',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  AddService: {
    id: 'AddService',
    defaultMessage: 'Add Service',
  },
  DeleteService: {
    id: 'ServiceForm.DeleteService',
    defaultMessage: 'Delete Service',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Name: {
    id: 'Name',
    defaultMessage: 'Name',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  Or: {
    id: 'Or',
    defaultMessage: 'or',
  },
  Obligatory: {
    id: 'Obligatory',
    defaultMessage: 'Obligatory',
  },
  Role: {
    id: 'Role',
    defaultMessage: 'Role',
  },
  WithoutEquipment: {
    id: 'ServiceForm.WithoutEquipment',
    defaultMessage: 'Without Equipment',
  },
  AnyStaffOfThisRole: {
    id: 'ServiceForm.AnyStaffOfThisRole',
    defaultMessage: 'any staff of this role',
  }
});
