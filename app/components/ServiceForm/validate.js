const validate = (values, props) => {
  const errors = { payers: [], staffAllocation: [], equipmentAllocation: [] };
  const requiredMessage = 'It\'s required field';
  if (!values.category) {
    errors.category = requiredMessage;
  }
  console.log(values)
  if (!values.name) {
    errors.name = {};
    errors.name.defaultName = requiredMessage;
    errors.name.shortName = requiredMessage;
  } else {
    errors.name = {};
    if (!values.name.defaultName) {
      errors.name.defaultName = requiredMessage;
    }
    if (!values.name.shortName) {
      errors.name.shortName = requiredMessage;
    }
  }

  values.payers.forEach((payer, index)=> {
    if (!payer.payer) {
      if (!errors.payers[index]) {
        errors.payers[index] = {};
      }
      errors.payers[index].payer = requiredMessage;
    } else {
      if (!payer.duration) {
        if (!errors.payers[index]) {
          errors.payers[index] = {};
        }

        errors.payers[index].duration = requiredMessage;
      }
      if (!payer.price) {
        if (!errors.payers[index]) {
          errors.payers[index] = {};
        }

        errors.payers[index].price = requiredMessage;
      }
    }
  });
  values.staffAllocation.forEach((staff, index)=> {
    if (!staff.role) {
      if (!errors.staffAllocation[index]) {
        errors.staffAllocation[index] = {};
      }
      errors.staffAllocation[index].role = requiredMessage;
    }
    if (!staff.staff) {
      if (!errors.staffAllocation[index]) {
        errors.staffAllocation[index] = {};
      }
      errors.staffAllocation[index].staff = requiredMessage;
    }

  });

  values.equipmentAllocation.forEach((staff, index)=> {
    if (!staff.room) {
      if (!errors.equipmentAllocation[index]) {
        errors.equipmentAllocation[index] = {};
      }
      errors.equipmentAllocation[index].room = requiredMessage;
    }
  });

  if (values.services && values.name && values.name.shortName && values.name.shortName.length > 0) {
    const isShortNameFined = values.services.some((service) => (service.name.shortName === values.name.shortName &&
    (props.editMedService ? service._id !== props.editMedService._id : true)));

    if (isShortNameFined) {
      errors.name.shortName = 'Sorry, this Short name is already used for another medical service';
    }
  }

  return errors;
};


export default validate;
