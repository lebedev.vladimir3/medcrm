import React, { PropTypes } from 'react';
import fontSetting from 'components/styles/font';
import Mdarrowdrop from 'react-icons/lib/md/arrow-drop-down';
import Mdarrowup from 'react-icons/lib/md/arrow-drop-up';
import './style.scss';


// -1: Default
// 0: Ascending
// 1: Descending
function Arrow(props) {
  let upOpac = '0.5';
  let downOpac = '0.5';
  const selected = props.selected;

  if (selected !== -1) {
    upOpac = selected === 0 ? '0.5' : '1';
  }
  if (selected !== -1) {
    downOpac = selected === 1 ? '0.5' : '1';
  }

  return (
    <div className="arrow-wrapper" >
      <Mdarrowup
        style={{ marginBottom: '-7px', color: '#5D697B', opacity: upOpac, fontSize: fontSetting.baseSizePlus7 }}
      />
      <Mdarrowdrop
        style={{ marginTop: '-7px', color: '#5D697B', opacity: downOpac, fontSize: fontSetting.baseSizePlus7 }}
      />
    </div>
  );
}


Arrow.propTypes = {
  selected: PropTypes.number,
};

export default Arrow;
