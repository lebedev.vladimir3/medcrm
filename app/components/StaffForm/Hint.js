import styled from 'styled-components';

export default styled.div`
  height: 50px;
  width: 738px;
  border: 1px solid #E2CC85;
  border-radius: 3px;
  background-color: #FBF8EA;
  display: flex;
  align-items: center;
  justify-content: center;
`;
