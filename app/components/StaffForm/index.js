import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { Field, Fields } from 'redux-form';
import styled from 'styled-components';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Input, TextArea, Select } from 'components/Input';
import MedServiceFormWidget from 'components/MedServiceFormWidget';
import { Card, CardFooter } from 'components/styles/Card';
import { ContentWrap } from 'components/styles/Containers';
import { FormLabel, RequiredStar } from 'components/styles/common';
import { RowsWrap, Row, Col, LabelCol, FieldsWrapCol } from 'components/styles/Grid';
import { honorifics, personal } from './../../constants/users';
import { HonorificParser } from './../../utils';
import { SelectDatePicker } from 'components/common/Datepicker';

import messages from './messages';
import Divider from 'components/common/Divider';
import Obligatory from 'components/common/obligatoryLegend';

const FieldLabel = styled.div`
  color: #7E7E7E;
  font-size: ${fontSetting.baseSize}px;
  line-height: 19px;
  position: absolute;
  top: -25px;
`;


const yearOld = (obj) => {
  let year;
  if (obj && obj.dd && obj.mm && obj.yyyy) {
    year = new Date() - new Date(`${obj.yyyy}-${obj.mm}-${obj.dd}`);
    year = Math.floor(year / 31536000000);
    year += year % 10 === 1 ? ' year' : ' years';
  }
  return year;
};

const renderInputField = (inputProps) => <Input {...inputProps.input} {...inputProps} />;
const renderInputNumber = (inputProps) => <Input
  width="64px"
  height="38px"
  style={{width:'64px',borderRadius:'5px 0 0 5px'}}
  {...inputProps.input}
  {...inputProps}
/>;
const renderTextAreaField = (inputProps) => <TextArea {...inputProps.input} {...inputProps} />;
const renderSelectField = (renderProps) => <Select {...renderProps.input} {...renderProps} />;

class StaffForm extends React.Component {
  constructor(props) {
    super(props);
    this.onBirthDDUp = this.onBirthDDUp.bind(this);
    this.onBirthDDDown = this.onBirthDDDown.bind(this);
    this.onBirthMMUp = this.onBirthMMUp.bind(this);
    this.onBirthMMDown = this.onBirthMMDown.bind(this);
    this.onBirthYYYYUp = this.onBirthYYYYUp.bind(this);
    this.onBirthYYYYDown = this.onBirthYYYYDown.bind(this);
  }

  onBirthDDUp() {
    this.props.change('birth.dd', Number(this.props.birth.dd) + 1);
  }

  onBirthYYYYUp() {
    this.props.change('birth.yyyy', Number(this.props.birth.yyyy) + 1);
  }

  onBirthMMUp() {
    this.props.change('birth.mm', Number(this.props.birth.mm) + 1);
  }

  onBirthDDDown() {
    this.props.change('birth.dd', Number(this.props.birth.dd) - 1);
  }

  onBirthMMDown() {
    this.props.change('birth.mm', Number(this.props.birth.mm) - 1);
  }

  onBirthYYYYDown() {
    this.props.change('birth.yyyy', Number(this.props.birth.yyyy) - 1);
  }

  isValidDate(obj) {
    const string = `${obj.yyyy}-${obj.mm}-${obj.dd}`;
    const timestamp = Date.parse(string);
    return !isNaN(timestamp);
  }

  render() {
    const {
      serviceCategories,
      handleChangeService,
      handleChangeServiceCategory,
      handleChangeAllServices,
      chosenServices,
      chosenServiceCategories,
      allServices,
      pristine,
      isAdmin,
      selectedTab,
      isEdit,
      handleSubmit,
      onSubmit,
    } = this.props;

    const angleStyle = { marginTop: '-7px', marginLeft: '2px' };
    const formatMessage = this.context.intl.formatMessage;
    const roleOptions = this.props.roles && this.props.roles.map((role, index) => ({
        value: role._id,
        label: role.name
      }));

    return (
      <Card>
        <form onSubmit={handleSubmit}>
          {(!selectedTab || selectedTab === 0) &&
          <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 30px`}>
            {!isEdit && <Obligatory />}
            <Divider title={formatMessage(messages.GeneralInfo)} marginTop={0} />
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.Title)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="25%">
                      <Field
                        name="title.personal"
                        component={renderSelectField}
                        options={[
                          { value: personal.HERR, label: personal.HERR },
                          { value: personal.FRAU, label: personal.FRAU },
                        ]}
                      />
                    </Col>
                    <Col width="25%">
                      <Field
                        name="title.honorific"
                        component={renderSelectField}
                        //placeholder={formatMessage(messages.ProfessionalTitle)}
                        options={[
                          { value: '', label: '—' },
                          { value: honorifics.DR, label: HonorificParser.toClientSide(honorifics.DR, formatMessage) },
                          { value: honorifics.PROFF, label: HonorificParser.toClientSide(honorifics.PROFF, formatMessage) },
                          { value: honorifics.DR_MED, label: HonorificParser.toClientSide(honorifics.DR_MED, formatMessage) },
                        ]}
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.LastName)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field
                        name="lastName"
                        component={renderInputField}
                        //placeholder={formatMessage(messages.FirstName)}
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.FirstName)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field
                        name="firstName"
                        component={renderInputField}
                        //placeholder={formatMessage(messages.FirstName)}
                      />
                    </Col>
                    <Col width="50%" />
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.DateofBirth)}</FormLabel>
                </LabelCol>
                <Col grow="1" padding="0">
                  <Fields
                    names={[
                      'birth.dd',
                      'birth.mm',
                      'birth.yyyy'
                    ]}
                    component={SelectDatePicker}
                    required
                    width="50%"
                    selectProps={[{ width: '80px'}, { width: '80px'}, { width: '94px'}]}
                    //columnProps={[{padding: 4}, {padding: 4}, {padding: 4}]}
                    wrapProps={{ padding: 4}}
                    monthView={isEdit && 'short'}
                  />
                </Col>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={true}>{formatMessage(messages.Role)}/{formatMessage(messages.Speciality)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>

                  <Row>
                    <Col width="50%">
                      <Field
                        name="role._id"
                        component={renderSelectField}
                        placeholder={formatMessage(messages.ChooseRole)}
                        options={roleOptions}
                        required={true}
                      />
                    </Col>
                    <Col width="50%">
                      <Field name="speciality" component={renderInputField} placeholder={formatMessage(messages.Speciality)} />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>


              <Divider width={2} style={{marginBottom: '40px'}} />
              <Row center style={{position: 'relative'}}>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.EMail)}</FormLabel>
                </LabelCol>
                <Col grow="1" padding="0">
                  <FieldsWrapCol width="100%">
                    <Row>
                      <Col width="50%">
                        <Field
                          name="email"
                          type="email"
                          component={renderInputField}
                          //placeholder={formatMessage(messages.EAddress)}
                        />
                        <FieldLabel>
                          {this.context.intl.formatMessage(messages.Work)}
                          {!isEdit && <RequiredStar />}
                        </FieldLabel>
                      </Col>
                      <Col width="50%">
                        <Field
                          name="personalEmail"
                          type="email"
                          component={renderInputField}
                          //placeholder={formatMessage(messages.EAddress)}
                        />
                        <FieldLabel style={{ display: 'block'}}>
                          {this.context.intl.formatMessage(messages.Personal)}
                        </FieldLabel>
                      </Col>
                    </Row>
                  </FieldsWrapCol>
                </Col>
              </Row>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.PhoneNumber)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field name="phone.work" type="phone" component={renderInputField}
                             placeholder={formatMessage(messages.MainMobilePhone)} />
                    </Col>
                    <Col width="50%">
                      <Field
                        name="phone.personal"
                        type="phone"
                        component={renderInputField}
                        placeholder={formatMessage(messages.SecondMobilePhone)}
                      />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </RowsWrap>
            <Divider width={2} />
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel required={!isEdit}>{formatMessage(messages.AccountPassword)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field name="password" component={renderInputField}
                             placeholder={formatMessage(messages.EnterPassword)}
                             type="password"/>
                    </Col>
                    <Col width="50%">
                      <Field name="repeatPassword" component={renderInputField}
                             placeholder={formatMessage(messages.VerifyPassword)}
                             type="password"/>
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </RowsWrap>
            <Divider width={2} />
            <RowsWrap>
              <Row center>
                <LabelCol>
                  <FormLabel>{formatMessage(messages.Address)}</FormLabel>
                </LabelCol>
                <FieldsWrapCol>
                  <Row>
                    <Col width="75%">
                      <Field name="address.street" component={renderInputField} placeholder={formatMessage(messages.Street) + ', ' +
                      formatMessage(messages.HouseNumber)} />
                    </Col>
                    <Col width="25%">
                      <Field name="address.postbox" component={renderInputField} placeholder={formatMessage(messages.PostBox)} />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
              <Row center>
                <LabelCol />
                <FieldsWrapCol>
                  <Row>
                    <Col width="50%">
                      <Field name="address.city" component={renderInputField} placeholder={formatMessage(messages.City)} />
                    </Col>
                    <Col width="50%">
                      <Field name="address.country" component={renderInputField} placeholder={formatMessage(messages.Country)} />
                    </Col>
                  </Row>
                </FieldsWrapCol>
              </Row>
            </RowsWrap>
            <Divider title={formatMessage(messages.Notes)} />
            <RowsWrap>
              <Row center>
                <Col grow="1" padding="0">
                  <Field
                    name="notes"
                    component={renderTextAreaField}
                    placeholder={formatMessage(messages.NotesForInternalUsage)}
                  />
                </Col>
              </Row>
            </RowsWrap>
          </ContentWrap>
          }
          {selectedTab === 1 &&
          <ContentWrap padding={`${isEdit ? '25px': '13px'} 30px 22px 30px`}>
            <Divider title={formatMessage(messages.SupportedServices)} marginTop={0} />
            {
              this.props.isEdit &&
              <MedServiceFormWidget
                serviceCategories={serviceCategories}
                chosenServices={chosenServices}
                chosenServiceCategories={chosenServiceCategories}
                allServices={allServices}
                onChangeService={handleChangeService}
                onChangeServiceCategory={handleChangeServiceCategory}
                onChangeAllServices={handleChangeAllServices}
                isAdmin={isAdmin}
              />
            }
          </ContentWrap>
          }
          <CardFooter>
            <ContentWrap padding="10px 30px 10px 30px">
              <Row justifyContent="space-between" center noPadding>
                <Col>
                  { this.props.onDelete && <DeleteButton type="button" onClick={this.props.onDelete}>
                    {formatMessage(messages.DeleteStaff)}
                  </DeleteButton>}
                </Col>
                <Col>
                  <div>
                    <GreyButton onClick={this.props.onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                    <GreenButton type="submit" disabled={!isAdmin || (pristine && selectedTab !== 1)}>
                      { this.props.serviceCategories ?
                        formatMessage(messages.SaveChanges) :
                        formatMessage(messages.AddStaff)
                      }
                    </GreenButton>
                  </div>
                </Col>
              </Row>
            </ContentWrap>
          </CardFooter>
        </form>
      </Card>
    );
  }
}

StaffForm.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
StaffForm.propTypes = {
  handleChangeService: PropTypes.func,
  handleChangeServiceCategory: PropTypes.func,
  handleChangeAllServices: PropTypes.func,
  roles: PropTypes.array.isRequired,
  onDelete: PropTypes.func,
  onCancel: PropTypes.func,
  pristine: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool,
  handleSubmit: React.PropTypes.func.isRequired,
  serviceCategories: PropTypes.array,
  chosenServices: PropTypes.array,
  chosenServiceCategories: PropTypes.array,
  allServices: PropTypes.bool,
  isAdmin: PropTypes.bool,
};


export { default as validate } from './validate';
export default StaffForm;

