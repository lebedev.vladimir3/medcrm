import { defineMessages } from 'react-intl';

export default defineMessages({
  mr: {
    id: 'Mr',
    defaultMessage: 'Mr',
  },
  mrs: {
    id: 'Mrs',
    defaultMessage: 'Mrs',
  },
  ms: {
    id: 'PatientForm.parsonalTitle.ms',
    defaultMessage: 'Ms',
  },
  insuranceNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  Country: {
    id: 'Country',
    defaultMessage: 'Country',
  },
  GeneralInfo: {
    id: 'GeneralInfo',
    defaultMessage: 'General Info',
  },
  SupportedServices: {
    id: 'SupportedServices',
    defaultMessage: 'Supported Services',
  },
  DeleteStaff: {
    id: 'DeleteStaff',
    defaultMessage: 'Delete Staff',
  },
  Region: {
    id: 'Region',
    defaultMessage: 'Region',
  },
  Street: {
    id: 'Street',
    defaultMessage: 'Street',
  },
  AccountPassword: {
    id: 'AccountPassword',
    defaultMessage: 'Account Password',
  },
  EnterPassword: {
    id: 'EnterPassword',
    defaultMessage: 'Enter password',
  },
  RepeatPassword: {
    id: 'RepeatPassword',
    defaultMessage: 'Repeat Password',
  },
  HouseNumber: {
    id: 'House number',
    defaultMessage: 'House number',
  },
  City: {
    id: 'City',
    defaultMessage: 'City',
  },
  PostBox: {
    id: 'PostBox',
    defaultMessage: 'PostBox',
  },
  NotesForInternalUsage: {
    id: 'NotesForInternalUsage',
    defaultMessage: 'Notes for internal usage',
  },
  payStatus: {
    id: 'payStatus',
    defaultMessage: 'Payer status',
  },
  payerName: {
    id: 'payerName',
    defaultMessage: 'Payer name',
  },
  insNumber: {
    id: 'Insurance.number',
    defaultMessage: 'Insurance number',
  },
  PaymentInfo: {
    id: 'PaymentInfo',
    defaultMessage: 'Payment Info',
  },
  Notes: {
    id: 'Notes',
    defaultMessage: 'Notes',
  },
  MainMobilePhone: {
    id: 'MainMobilePhone',
    defaultMessage: 'Main mobile phone',
  },
  SecondMobilePhone: {
    id: 'SecondMobilePhone',
    defaultMessage: 'Secondary mobile phone',
  },
  PhoneNumber: {
    id: 'PhoneNumber',
    defaultMessage: 'Phone number',
  },
  VerifyPassword: {
    id: 'VerifyPassword',
    defaultMessage: 'Verify password',
  },
  WorkMobilePhone: {
    id: 'WorkMobilePhone',
    defaultMessage: 'Work mobile phone',
  },
  PersonalMobilePhone: {
    id: 'PersonalMobilePhone',
    defaultMessage: 'Personal mobile phone',
  },
  EAddress: {
    id: 'EAddress',
    defaultMessage: 'E-Mail address',
  },
  DateofBirth: {
    id: 'DateofBirth',
    defaultMessage: 'Date of birth',
  },
  LastName: {
    id: 'LastName',
    defaultMessage: 'Last name',
  },
  WorkEmail: {
    id: 'WorkEmail',
    defaultMessage: 'Work email',
  },
  PersonalEmail: {
    id: 'PersonalEmail',
    defaultMessage: 'Personal email',
  },
  FirstName: {
    id: 'FirstName',
    defaultMessage: 'First name',
  },
  Name: {
    id: 'Name',
    defaultMessage: 'Name',
  },
  Personal: {
    id: 'Personal',
    defaultMessage: 'Personal',
  },
  EMail: {
    id: 'EMail ',
    defaultMessage: 'E-Mail ',
  },
  Address: {
    id: 'Address',
    defaultMessage: 'Address',
  },
  ProfessionalTitle: {
    id: 'ProfessionalTitle',
    defaultMessage: 'Professional title',
  },
  PersonalTitle: {
    id: 'PersonalTitle',
    defaultMessage: 'Personal title',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Role: {
    id: 'Role',
    defaultMessage: 'Role',
  },
  Work: {
    id: 'Work',
    defaultMessage: 'Work',
  },
  Speciality: {
    id: 'Speciality',
    defaultMessage: 'Speciality',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  AddPatient: {
    id: 'AddPatient',
    defaultMessage: 'Add Patient',
  },
  AddStaff: {
    id: 'AddStaff',
    defaultMessage: 'Add Staff',
  },
  ChooseRole: {
    id: 'ChooseRole',
    defaultMessage: 'Choose Role',
  },
  HintChoose: {
    id: 'HintChoose ',
    defaultMessage: 'Hint: Choose medical services that can be provided by this staff',
  },
  payStatusPlaceholder: {
    id: 'payStatus.Placeholder',
    defaultMessage: 'Select payer status'
  },
  DeleteProfile: {
    id: 'EditPatientsPage.DeleteProfile',
    defaultMessage: 'Delete Profile',
  },
});
