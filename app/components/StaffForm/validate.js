import moment from 'moment';


const validate = values => {
  const errors = {};
  errors.role = {};
  errors.phone = {};
  const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
  const phonePattern = /^[\d \+\-]+$/;

  if (!values.email) {
    errors.email = 'Required';
  } else if (!emailPattern.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.firstName) {
    errors.firstName = 'Required';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  }
  if (!values.phone || values.phone && !values.phone.work) {
    errors.phone.work = 'Required';
  }

  if (values.role && (!values.role._id || values.role._id < 1)) {
    errors.role._id = 'Required';
  }

  if (values.personalEmail && !emailPattern.test(values.personalEmail)) {
    errors.personalEmail = 'Invalid email address';
  }

  if (values.phone) {
    errors.phone = {};

    if (values.phone.work && !phonePattern.test(values.phone.work)) {
      errors.phone.work = 'Invalid phone number';
    }

    if (values.phone.personal && !phonePattern.test(values.phone.personal)) {
      errors.phone.personal = 'Invalid phone number';
    }
  }

  if (values.password && values.password !== values.repeatPassword) {
    errors.password = "passwords don't match";
  }
  return errors;
};


export default validate;
