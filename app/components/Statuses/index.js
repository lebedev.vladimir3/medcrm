import React from 'react';
import styled from 'styled-components';
import MdNotifications from 'react-icons/lib/md/notifications';
import MdAccessTime from 'react-icons/lib/md/access-time';
import MdCheck from 'react-icons/lib/md/check';
import FaPaperPlane from 'react-icons/lib/fa/paper-plane';
import MdReceipt from 'react-icons/lib/md/receipt';
import MdClose from 'react-icons/lib/md/close';


const Unscheduled = styled(MdNotifications)`color: #D97118;`;
const Notifications = styled(MdNotifications)` color: #DBA22B; `;
const AccessTime = styled(MdAccessTime)` color: #DBA22B `;
const Receipt = styled(MdReceipt)` color: #4687D6 `;
const PaperPlane = styled(FaPaperPlane)` color: #4687D6 `;
const Check = styled(MdCheck)` color: #00896D `;
const Close = styled(MdClose)` color: #8D8D8D `;


const PrescriptionStatuses = [
  { value: 'unscheduled', label: 'Unscheduled', icon: React.createElement(Unscheduled) },
  { value: 'partlyScheduled', label: 'Partly Scheduled', icon: <Notifications />, disabled: true },
  { value: 'scheduled', label: 'Scheduled', icon: <AccessTime />, disabled: true },
  { value: 'paperwork', label: 'Paperwork', icon: <Receipt />, disabled: true },
  { value: 'invoiceSent', label: 'Invoice Sent', icon: <PaperPlane /> },
  { value: 'completed', label: 'Completed', icon: <Check /> },
  { value: 'cancelled', label: 'Cancelled', icon: <Close /> },
];

const AppointmentStatuses = [
  { value: 'SCHEDULED', label: 'Scheduled', icon: <Unscheduled /> },
  { value: 'RE_SCHEDULED', label: 'Re-Scheduled', icon: <Notifications /> },
  { value: 'NO_SHOW', label: 'No-Show', icon: <AccessTime /> },
  { value: 'ARRIVED', label: 'Arrived', icon: <Receipt /> },
  { value: 'PERFORMING', label: 'Performing', icon: <PaperPlane /> },
  { value: 'COMPLETED', label: 'Provided', icon: <Check /> },
  { value: 'CANCELLED', label: 'Cancelled', icon: <Close /> },
];


export { PrescriptionStatuses, AppointmentStatuses };
