import React, { PropTypes } from 'react';
import styled from 'styled-components';
import theme from './../styles/colorScheme';

import Mdarrowdrop from 'react-icons/lib/md/arrow-drop-down';
import Mdarrowup from 'react-icons/lib/md/arrow-drop-up';


const ArrowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 2px;
`;


// -1: Descending
// 0: Default
// 1: Ascending
const Arrow = (props) => {
  let upOpac = '0.5';
  let downOpac = '0.5';
  const selected = props.selected;

  if (selected !== 0 && selected !== undefined) {
    upOpac = selected === 1 ? '1' : '0.5';
    downOpac = selected === -1 ? '1' : '0.5';
  }

  const commonArrowStyle = {
    color: theme.Table.header.color,
    height: '6px',
    width: '18px'
  };

  return (
    <ArrowWrapper className={props.className}>
      <Mdarrowup
        viewBox="13 15 15 10"
        style={{ ...commonArrowStyle, opacity: upOpac }}
      />
      <Mdarrowdrop
        viewBox="13 15 15 10"
        style={{ ...commonArrowStyle, marginTop: '2px', opacity: downOpac }}
      />
    </ArrowWrapper>
  );
};


Arrow.propTypes = {
  selected: PropTypes.number,
};

export default Arrow;
