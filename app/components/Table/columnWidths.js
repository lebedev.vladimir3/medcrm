
export const orderColumnsAppointmentListInPrescription = [
  APPOINTMENT_TABLE_COLUMN_NAME.TIME,
  APPOINTMENT_TABLE_COLUMN_NAME.SERVICE,
  APPOINTMENT_TABLE_COLUMN_NAME.DURATION,
  APPOINTMENT_TABLE_COLUMN_NAME.STAFF,
  APPOINTMENT_TABLE_COLUMN_NAME.ROOM,
  APPOINTMENT_TABLE_COLUMN_NAME.STATUS,
  APPOINTMENT_TABLE_COLUMN_NAME.FINANCIAL_STATUS
];
export const columnsDataAppointmentListInPrescription = [
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.TIME,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.SERVICE,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.DURATION,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.STAFF,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.ROOM,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.STATUS,
    width: 10,
    display: true
  },
  {
    id: APPOINTMENT_TABLE_COLUMN_NAME.FINANCIAL_STATUS,
    width: 10,
    display: true
  },
];
export const appointmentScheduler = {};
export const appointmentListInPatient = {};

const getColumnWidth = (isEdit, showPrescription) => {
  let colWidths = [
    { name: 'Time', isEdit: 15, notEdit: 10},
    { name: 'Service', isEdit: 15, notEdit: 10},
    { name: 'Duration', isEdit: 15, notEdit: 10},
    { name: 'Staff', isEdit: 25, notEdit: 17},
    { name: 'Room', isEdit: 25, notEdit: 10},
  ];
  if(!isEdit){
    if(showPrescription) colWidths.push({ name: 'Prescription', isEdit: 0, notEdit: 10});
    colWidths.push({ name: 'Status', isEdit: 0, notEdit: 16.5});
    colWidths.push({ name: 'Financial Status', isEdit: 0, notEdit: 16.5})
  } else {
    colWidths.unshift({ name: 'Checkbox', isEdit: 5, notEdit: 0});
  }
  return colWidths.map(item => isEdit ? item.isEdit: item.notEdit);
};
