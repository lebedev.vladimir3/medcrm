//@flow
import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import theme from '../styles/colorScheme';
import SortArrows from './SortArrows';
import { Link } from 'react-router';


export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  thead > tr:first-child {
    border-top: none;
  }
  .card > &:first-child > thead > tr:first-child > th, td {
    &:first-child {
      border-top-left-radius: 4px;
    }
    &:last-child {
      border-top-right-radius: 4px;
    }
  }
`;

const TRWrap = (props)=> {
  const childrenWithProps = React.Children.map(props.children, (child)=>
    props.link ? React.cloneElement(child, { link: props.link, zIndexNeeded: props.zIndexNeeded }) : child);
  return <tr {...props}>{childrenWithProps}</tr>
}

export const TR = styled(TRWrap)`
  height: ${props => props.height ? props.height : '55px'};
  cursor: ${(props) => props.onClick ? 'pointer' : 'auto'};
  transition: all 250ms ease-out;
  &:not(:last-child){
    border-bottom: 1px solid ${theme.Table.body.row.borderBottom};
  }
  tbody > &:hover {
    background-color: ${theme.Table.body.row.hover};
  }
  thead > & > td, thead > & > th {
    background-color: ${theme.Table.header.background.main};
  }
  .table-time > thead > & > th {
    background-color: ${theme.Table.header.background.time};
    color: ${theme.Table.header.color.time};
    border-bottom: 1px solid ${theme.Table.header.border.time};
    border-top: 1px solid ${theme.Table.header.border.time};
  }
  .table-time > tbody > & > td {
    color: ${theme.Table.body.cell.time}
  }
`;

export const TimeTable = styled(Table).attrs({
  className: 'table-time'
})``;


type ThProps = {
  selected? : boolean,
  className? : string,
  children? : React.Node,
}
const SortWrap = styled.div`
  display: flex;
  align-items: center;
`;
class Th extends React.Component<ThProps> {
  render() {
    let result = this.props.children;
    if (this.props.selected !== undefined) {
      result =
        <SortWrap>
          <span>{result}</span>
          <SortArrows selected={this.props.selected} />
        </SortWrap>
    }
    return (
      <th {...this.props}>{result}</th>
    );
  }
}
export const TH = styled(Th)`
  height: 65px;
  padding-left: 30px;
  color: white;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  text-align: ${(props) => props.centered ? 'center' : 'left'};
  cursor: ${(props) => props.onClick ? 'pointer' : 'inherit'};
  position: relative;
`;

const TDWrap = (props)=> {
  return <td {...props}><div>
    {!props.offLink && <Link to={props.link}
          style={{display: 'flex', height: '100%', width: '100%', position: 'absolute', top: '0', left: '0', zIndex: props.zIndexNeeded ? '1': 'auto'}}>
    </Link>}
    {props.children}
  </div>
  </td>
};

export const TD = styled(TDWrap)`
  height: ${props => props.height ? props.height : '55px'};
  padding: ${props => props.padding ? props.padding : '0px 0px 0px 30px'};
  color: #596372;
  font-size: ${fontSetting.baseSize}px;
  line-height: 18px;
  position: relative;
  text-align: ${(props) => props.centered ? 'center' : 'left'};
  cursor: ${(props) => props.onClick ? 'pointer' : 'inherit'};
`;

export { default as SortArrows } from './SortArrows';

