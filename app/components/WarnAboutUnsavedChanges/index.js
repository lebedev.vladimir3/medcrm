import React, { PropTypes } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { isDirty, hasSubmitSucceeded } from 'redux-form';

function warnAboutUnsavedChanges(WrappedComponent, formName) {
  class WarnAboutUnsavedChanges extends React.Component {
    componentDidUpdate() {
      // if (!this.props.hasSubmitSucceeded) {
        this._promptUnsavedChange(this.props.isFormDirty);
      // }
    }

    componentWillUnmount() {
      window.onbeforeunload = null;
    }

    _promptUnsavedChange(isUnsaved = false, leaveMessage = 'Leave with unsaved change?') {
      // Detecting page transition (prevent leaving by setting true)
      // for React Router version > 2.4
      if (!this.props.hasSubmitSucceeded) {
        this.props.router.setRouteLeaveHook(this.props.route, () => (isUnsaved) && confirm(leaveMessage));
        window.onbeforeunload = (isUnsaved) && (() => leaveMessage);
      } else {
        this.props.router.setRouteLeaveHook(this.props.route, () => {});
        window.onbeforeunload = null;
      }
      // Detecting browser close
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  WarnAboutUnsavedChanges.propTypes = {
    isFormDirty: PropTypes.bool,

    // react-router
    router: PropTypes.object,
    route: PropTypes.object,
  };

  const mapStateToProps = (state) => ({
    isFormDirty: isDirty(formName)(state),
    hasSubmitSucceeded: hasSubmitSucceeded(formName)(state),
  });

  return withRouter(connect(
    mapStateToProps,
    null
  )(WarnAboutUnsavedChanges));
}

export default warnAboutUnsavedChanges;
