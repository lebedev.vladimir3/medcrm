import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { Table, TR, TD as TDComponent, TH as THComponent } from 'components/Table';
import styled from 'styled-components';
import moment from 'moment';
import { calculateSummaryFromMinutes, calculateWorkingHours } from 'utils';
// import 'moment-duration-format';
import { InputButton } from 'components/common/Button';
import messages from './messages';

const TRWEEK = styled(TR)`
  background-color: #ECECF1;
  position: relative;
	color: #495262;
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
	height: 40px;
`;

const TD = styled(TDComponent)`
  padding-right: 15px;
  height: auto;
`;

const TH = styled(THComponent)`
  padding: 15px 15px 15px 15px;
  color: #2C2D2F;
  opacity: 0.8;
  background-color: #CACED1 !important;
`;

const SubTD = styled.div`
  display: flex;
  height: 40px;
  align-items: center;
  justify-content: center;
`;
const Circle = styled.div`
	border-radius: 50%;
  width: 10px;
  height: 10px;
  position: absolute;
  background-color: #CE6D0F;
  top: calc(50%+10px);
  left: 23px;
`;

const TDWithRightBorder = styled(TD)`
  border-right: 1px solid #DADFEA;
  background-color: #ECECF1;
  text-align: ${(props) => props.centered ? 'center' : 'right'};
  width: 140px;
`;

const THWithRightBorder = styled(TH)`
  border-right: 1px solid #DADFEA;
  padding-left: ${(props) => props.centered ? '15px' : '0px'};
  text-align: ${(props) => props.centered ? 'center' : 'right'};
`;
const WEEKWithRightBorder = styled(TD)`
  border-top-left-radius: 0 !important;
  border-right: 1px solid #DADFEA;
  background-color: #A3A7B0;
  text-align: center;
  color: white;
  padding-left: ${(props) => props.centered ? '15px' : '0px'};
  text-align: ${(props) => props.centered ? 'center' : 'right'};
`;

const WeekTR = ({ weekNumber, summaryMinutes, openingHours }) => (
  <TRWEEK>
    <WEEKWithRightBorder centered={!!openingHours}>{openingHours ? `KW ${weekNumber}` : `Week ${weekNumber}`}</WEEKWithRightBorder>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    {openingHours && <TD></TD>}
    <TD centered>
      <strong>{calculateSummaryFromMinutes(summaryMinutes)}</strong>
    </TD>
  </TRWEEK>
);

const WorkingHoursList = (props, context) => {
  const { items, openingHours } = props;
  const { formatMessage } = context.intl;
  let workedHours = 0;
  let hoursWorkedInWeek = [];
  items.forEach((row)=> {
    if (moment(row.date).day() === 1) {
      hoursWorkedInWeek.push(workedHours);
      workedHours = 0;
    }
    workedHours += calculateWorkingHours(row.workingHours);
  });
  hoursWorkedInWeek.push(workedHours);
  let iterateOnHoursWorkedInWeek = 1;
  const rows = items.reduce((acc, cur, index) => {
    const { workingHours, summaryMinutes, holiday } = cur;
    const workingDay = cur.workingHours.length > 0;
    const accRows = [...acc];
    const weekNumber = moment(cur.date).week();
    const isItCurrentDate = moment(cur.date).format('MMMM Do YYYY') === moment().format('MMMM Do YYYY');

    if (moment(cur.date).day() === 1) {
      accRows.push(<WeekTR weekNumber={weekNumber} summaryMinutes={hoursWorkedInWeek[iterateOnHoursWorkedInWeek]} openingHours={openingHours}
                           key={`w-${index}`} />);
      iterateOnHoursWorkedInWeek++;
    }
    accRows.push(
      <TR key={index} style={{backgroundColor: index % 7 > 4 ? '#F8F8F8': 'white'}}>
        <TDWithRightBorder
          centered={!!openingHours}>{openingHours ? `${moment(cur.date).format('dddd').substring(0, 2)}.` : moment(cur.date).format('dddd')} </TDWithRightBorder>
        <TD centered>
          {isItCurrentDate && <Circle/>}
          {moment(cur.date).format('D. ') + moment(cur.date).format('MMMM').substring(0, 3)}
        </TD>
        {openingHours && openingHours.length > 0 && openingHours[index] && <TD centered>
          {
            openingHours[index].workingHours.length > 0 ? openingHours[index].workingHours.map((item, index1) => {
              const timeFrom = moment.utc().startOf('day').add(item.from, 'minutes');
              const timeTo = moment.utc().startOf('day').add(item.to, 'minutes');
              return (
                <SubTD key={index1}>
                  {`${timeFrom.format('HH:mm')} - ${timeTo.format('HH:mm')}`}
                </SubTD>
              );
            }) : '—'
          }
        </TD>
        }
        <TD centered>
          {
            workingDay ? workingHours.map((item, index1) => {
              const timeFrom = moment.utc().startOf('day').add(item.from, 'minutes');
              const timeTo = moment.utc().startOf('day').add(item.to, 'minutes');
              return (
                <SubTD key={index1}>
                  {`${timeFrom.format('HH:mm')} - ${timeTo.format('HH:mm')}`}
                </SubTD>
              );
            }) : '—'
          }
        </TD>
        <TD centered>{openingHours ?
          (workingDay ? workingHours.map((item, index1) => {
            return (
              <SubTD key={index1}>{item.title ? item.title : '—'}</SubTD>
            );
          }) : '—')
          : holiday ? holiday.title : '—'}</TD>
        <TD centered>{workingDay ? calculateSummaryFromMinutes(summaryMinutes) : '—'}</TD>
      </TR>
    );

    return accRows;
  }, []);

  return (
    <Table>
      <thead>
        <TR>
          <THWithRightBorder
            centered={!!openingHours}>{items.length > 0 ? (openingHours ? moment(items[0].date).format('YYYY') : moment(items[0].date).format('MMMM YYYY')) : ''}</THWithRightBorder>
          <TH centered>{formatMessage(messages.Date)}</TH>
          <TH centered>{formatMessage(messages.OpeningHours)}</TH>
          {openingHours && <TH centered>{formatMessage(messages.WorkingHours)}</TH>}
          <TH centered>{openingHours ? formatMessage(messages.Title) : formatMessage(messages.HolidayTitle)}</TH>
          <TH centered>{formatMessage(messages.SummaryHours)}</TH>
        </TR>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </Table>
  );
};

WorkingHoursList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

WorkingHoursList.propTypes = {
  items: PropTypes.array,
};


WorkingHoursList.defaultProps = {
  items: [],
};


export default WorkingHoursList;

