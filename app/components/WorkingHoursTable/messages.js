import { defineMessages } from 'react-intl';

export default defineMessages({
  Date: {
    id: 'Date',
    defaultMessage: 'Date',
  },
  OpeningHours: {
    id: 'OpeningHours',
    defaultMessage: 'Opening Hours',
  },
  WorkingHours: {
    id: 'WorkingHours',
    defaultMessage: 'Working Hours',
  },
  Title: {
    id: 'Title',
    defaultMessage: 'Title',
  },
  HolidayTitle: {
    id: 'HolidayTitle',
    defaultMessage: 'Holiday Title',
  },
  SummaryHours: {
    id: 'SummaryHours',
    defaultMessage: 'Summary Hours',
  },
});
