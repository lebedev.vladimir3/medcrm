import fontSetting from 'components/styles/font';
import styled from 'styled-components';
import theme from './../../../styles/colorScheme';

export const Button = styled.button`
  display: flex;
  margin: 0 10px;
  padding: 0 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 40px;
  border-radius: 20px;
  cursor: pointer;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  color: white;
  background-color: ${theme.Button.main.active.background};
  min-width: 136px;
  
`;

export const ButtonContent = styled.div`
  //color: #5383BD;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
`;

export const ButtonText = styled.span`
  margin-left: ${props => props.hasIcon ? '8px': '0px'};
`;
