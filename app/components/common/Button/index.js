import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import Cancel from 'assets/icons/Delete.svg';
import Print from 'assets/icons/Print.svg';
import Email from 'assets/icons/Email.svg';
import theme from '../../styles/colorScheme';
import { Link } from 'react-router';


export const Button = styled.button`
  height: 40px;
  ${props => props.fullWidth && 'width: 100%;'}
  //min-width: 120px;
  border-radius: 20px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  cursor: pointer;
  padding: 0 30px;
  &:not(:last-child){
    margin-right: 10px;
  }
  &[disabled] {
    cursor: not-allowed;
    background-color: ${theme.Button.main.disabled.background};
    color: ${theme.Button.main.disabled.color};
    border: none;
  }
`;

const ButtonWrap = (props)=> {
  return props.link ? (
    <Link to={props.link}>
      <Button {...props}>
        {props.children}
      </Button>
    </Link>) :
    <Button {...props}>
      {props.children}
    </Button>
};

export const SecondaryButton = styled(ButtonWrap)`
  background-color: ${theme.Button.main.active.color};
  color: ${theme.Button.main.active.background};
  border: 1px solid ${theme.Button.main.active.background};
`;

export const GreenButton = styled(Button)`
  background-color: ${theme.Button.apply.active};
  color: #EEEEEE;
  &[disabled] {
    background-color: ${theme.Button.apply.disabled};
    color: white;
  }
`;

export const GreyButton = styled(Button)`
  color: ${theme.Button.cancel.color};
  background-color: ${theme.Button.cancel.background};
  border: 1px solid ${theme.Button.cancel.border};
`;


export const BlueButton = styled(ButtonWrap)`
  background-color: ${theme.Button.main.active.background};
  color: ${theme.Button.main.active.color};
  &[disabled] {
    cursor: not-allowed;
    &::before {
      background-color: rgba(250,250,250,0.4);
    }
  }
`;

const Delete = (props) =>
  <button {...props}>
    <Cancel style={{ margin: '5px 5px 8px 0' }} />
    {props.children || ''}
  </button>;

export const DeleteButton = styled(Delete)`
  color: ${theme.Button.delete.active};
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 18px;
  text-align: center;
`;

export const SecondaryHeaderButton = styled.button`
  height: 40px;
	min-width: 40px;
	display: flex;
	align-items: center;
	justify-content: center;
	border: 1px solid ${theme.Button.headerPanel.main.border};
	border-radius: 4px;
	background-color: ${props => props.gray ? theme.Button.headerPanel.gray.background: theme.Button.headerPanel.main.background};
	&:not(:last-child){
    margin-right: 15px;
  }
`;

export const PrintButton = (props) => (
  <SecondaryHeaderButton {...props} >
    <Print />
  </SecondaryHeaderButton>
);

export const EmailButton = (props) => (
  <SecondaryHeaderButton {...props} >
    <Email />
  </SecondaryHeaderButton>
);

export const InputButton = styled.button`
  margin-left: 15px;
  height: 36px;
  border: ${(props) => props.active ? 'none' : '1px solid #B5BDCF'};
  border-radius: 4px;
  padding: 9px 15px 9px 15px;
  background: ${(props) => props.active ? '#7C909C' : '#FFFFFF'};
  color: ${(props) => props.active ? '#FFFFFF' : '#455A64'};
  font-size: ${fontSetting.baseSizeMinus1}px;
  font-weight: 600;
  line-height: 18px;
  cursor: ${(props) => props.active ? 'auto' : 'pointer'};
  &[disabled] {
    cursor: not-allowed;
  }
`;
