//@flow
import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import moment from 'moment';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';
import messages from '../../messages';
import ArrowIcon from 'assets/icons/Go.svg'

const Header = styled.h3`
  line-height: 18px;
  font-size: ${fontSetting.baseSizePlus1}px;
  font-weight: bold;
  padding: 9px 19px;
  margin: 0px;
  background-color: ${theme.MiniCard.backgrounds.accent};
  color: ${theme.Text.main};
`;

const StyledAppointment = styled.div`
  padding: 15px 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid ${theme.Borders.divider};
  &:last-child {
    border-bottom: none;
  }
`;

const IconWrap = styled.span`
  color: ${theme.Button.main.active.background};
  cursor: pointer;
`;

const Appointment = (props, context) => <StyledAppointment>
  <span style={{ width: '40px' }}>{moment(props.data.date).format('HH:mm')}</span>
  <strong style={{ width: '60px' }}>{props.data.service ? props.data.service.name.shortName: '—'}</strong>
  <span>{props.data.duration + ' ' + context.intl.formatMessage(messages.min)}</span>
  <IconWrap onClick={() => {
    props.onClick(`/main/patients/${props.patientId}/appointments/${props.data._id}`)
  }}>
    <ArrowIcon />
  </IconWrap>
</StyledAppointment>;

Appointment.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

type Props = {
  appointments: Array<any>,
  currentAppointment: any,
  //handlePush: any,
};

class LinkedAppointments extends React.Component<Props>{
  render(){
    const { appointments, currentAppointment, push, patient } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const linkedAppointments = appointments ? appointments.filter((item) =>
      item._id !== currentAppointment._id &&
      moment(item.date).isSame(currentAppointment.date, 'day')) : [];
    return linkedAppointments.length > 0 ? (
      <div>
        <Header>{formatMessage(messages.LinkedAppointments)}</Header>
        {linkedAppointments.map(item => <Appointment key={item._id} data={item} onClick={push} patientId={patient._id} />)}
      </div>
    ): null;
  }
}

LinkedAppointments.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  patient: state.editPatientPage.editPatient,
  currentAppointment: state.editPatientPage.editAppointment,
  appointments: state.editPatientPage.appointments,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ push }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LinkedAppointments);
