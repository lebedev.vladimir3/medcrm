//@flow

import React, { PropTypes } from 'react';
import { StyledCard, Row, HeaderRow, RightPart, Divider } from 'components/styles/Card/Mini';
import { ContentWrap } from 'components/styles/Containers';
import messages from '../../messages';
import { HonorificParser, genderParser } from '../../../../../utils';

type Props = {
  prescription: {
    staff: {
      firstName: string,
      title: {
        honorific: string,
        personal: string
      }
    }
  }
}

class ReferredBy extends React.Component<Props>{
  render(){
    const { prescription }  = this.props;
    const formatMessage = this.context.intl.formatMessage;
    return prescription && prescription.staff && prescription.staff.firstName ? (
      <ContentWrap padding="15px 20px">
        <Divider />
        <Row>{formatMessage(messages.ReferredBy)}</Row>
        <div style={{ wordWrap: 'break-word' }}>
          <span>{HonorificParser.toClientSide(prescription.staff.title.honorific, formatMessage)}</span>
          <strong> {prescription.staff.lastName}</strong>
          {prescription.staff.firstName && <span>, {prescription.staff.firstName}</span>}
          <span> {genderParser(prescription.staff.title.personal)}</span>
        </div>
      </ContentWrap>
    ): null;
  }
}

ReferredBy.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default ReferredBy;
