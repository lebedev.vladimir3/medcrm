//@flow

import React, { PropTypes } from 'react';
import moment from 'moment';
import Card from 'components/Card';
import styled from 'styled-components';
import { Field } from 'redux-form';
import { StatusSelect } from 'components/Input';
import * as STATUSES from '../../../../../../common/constants/statuses';
import { StyledCard, Row, HeaderRow, SubHeaderRow, RightPart, Divider } from 'components/styles/Card/Mini';
import { ContentWrap } from 'components/styles/Containers';
import messages from '../../messages';
import { types, entities } from "./../../constants";

import 'assets/styles/custom/react-select/default.scss';

import LinkedAppointments from './LinkedAppointments';
import ReferredBy from './ReferredBy';

const renderSelectField = (renderProps) => {
  const { input } = renderProps;
  return (
    <StatusSelect
      { ...input }
      { ...renderProps }
      onChange={option => input.onChange(option.value)}
      value={input.value}
    />
  );
};

type Props = {
  id: string,
  initialValues: any,
  showFinancial: boolean,
  disableFinancial: boolean,
  disableGeneral: boolean,
  isCancelled: boolean,
  appointment: any,
  appointments: Array<any>,
  prescription: any,
  showSpecificContent: boolean,
  type: string,
}

class MiniCard extends React.Component<Props> {
  static defaultProps = {
    showFinancial: true,
    disableFinancial: false,
    disableGeneral: false,
    isCancelled: false,
    showSpecificContent: false,
  };
  renderSpecificContent(){
    const { type, appointment, appointments, handlePush, ...props } = this.props;
    switch (type){
      case entities.APPOINTMENT:{
        return <LinkedAppointments
          appointments={appointments}
          currentAppointment={appointment}
          handlePush={handlePush}
        />
      }
      case entities.PRESCRIPTION:{
        return <ReferredBy prescription={appointment && appointment.prescription} />
      }
    }
  }
  render(){
    const {
      id,
      initialValues,
      statusChangedAt,
      financialStatusChangedAt,
      type,
      showFinancial,
      disableFinancial,
      disableGeneral,
      isCancelled,
      showSpecificContent,
    } = this.props;

    let statusKey = type.toUpperCase();
    if(isCancelled) statusKey += '_ALL';
    return (
      <ContentWrap padding="0px">
        <ContentWrap padding="15px 20px">
          <form>
            <HeaderRow>
              <span style={{ marginRight: 10, fontWeight: 600 }}>{this.context.intl.formatMessage(messages[type+'ID'])}</span>
              <RightPart bold>{id}</RightPart>
            </HeaderRow>
            <Row>
              <Field
                name="status"
                component={renderSelectField}
                options={STATUSES[statusKey].GENERAL}
                disabled={disableGeneral}
              />
            </Row>
            <Row>
              {this.context.intl.formatMessage(messages.StatusUpdated)}
              <RightPart>{moment(statusChangedAt).format('DD.MM.YYYY')}</RightPart>
            </Row>
            { showFinancial &&
            (<div>
              <Divider />
              <SubHeaderRow>
                <span>{this.context.intl.formatMessage(messages.FinancialStatus)}</span>
              </SubHeaderRow>
              <Row>
                <Field
                  name="financialStatus"
                  component={renderSelectField}
                  options={STATUSES[statusKey].FINANCIAL}
                  disabled={disableFinancial}
                />
              </Row>
              <Row>
                {this.context.intl.formatMessage(messages.StatusUpdated)}
                <RightPart>{moment(financialStatusChangedAt).format('DD.MM.YYYY')}</RightPart>
              </Row>
            </div>)
            }
          </form>
        </ContentWrap>
        {showSpecificContent && this.renderSpecificContent()}
      </ContentWrap>
    );
  }
}

MiniCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MiniCard;
