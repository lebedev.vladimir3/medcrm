//@flow

import * as React from 'react';
import { StyledCard, Row } from 'components/styles/Card/Mini';
import { types, entities } from "./constants";

import * as cards from './Specific';

type Props = {
  className?: string,
  type: string
};

export default class MiniCard extends React.Component<Props> {
  static defaultProps = {};
  renderConcreteCard(){
    const { type, ...props } = this.props;
    let targetProps = { ...props };

    switch (type){
      case types.APPOINTMENT_STATUS_CHANGER:{
        targetProps.type = entities.APPOINTMENT;
        return <cards.StatusChanger {...targetProps}/>
      }
      case types.PRESCRIPTION_STATUS_CHANGER:{
        targetProps.type = entities.PRESCRIPTION;
        return <cards.StatusChanger {...targetProps}/>
      }
    }
  }
  render(){
    return (
      <StyledCard padding="0px">
        {this.renderConcreteCard()}
      </StyledCard>
    );
  }
}

export const MiniCardPropsBinder = (props) => class extends React.Component {
  render(){
    const totalProps = { ...this.props, ...props};
    return <MiniCard {...totalProps}/>
  }
};
