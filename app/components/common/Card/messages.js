import { defineMessages } from 'react-intl';

export default defineMessages({
  NewPatient: {
    id: 'NewPatient',
    defaultMessage: 'New Patient',
  },
  NewPrescription: {
    id: 'NewPrescription',
    defaultMessage: 'New Prescription',
  },
  Step1: {
    id: 'Step1',
    defaultMessage: 'Step 1 - Prescription Card',
  },
  Step2: {
    id: 'Step2',
    defaultMessage: 'Step 2 - Medical Services',
  },
  PatientsDirectory: {
    id: 'PatientsPage.PatientsDirectory',
    defaultMessage: 'Patients Directory',
  },
  PatientNotAvailable: {
    id: 'PatientsPage.PatientNotAvailable',
    defaultMessage: 'This Patient is not available at the specified day or time',
  },
  PersonalDetails: {
    id: 'PersonalDetails',
    defaultMessage: 'Personal Details',
  },
  AppointmentScheduler: {
    id: 'AppointmentScheduler',
    defaultMessage: 'Appointment Scheduler',
  },
  StaffNotAvailable: {
    id: 'PatientsPage.StaffNotAvailable',
    defaultMessage: 'The selected Staff is not available at the specified day or time',
  },
  RoomNotAvailable: {
    id: 'PatientsPage.RoomNotAvailable',
    defaultMessage: 'The selected Room is not available at the specified day or time',
  },
  NextAppointment: {
    id: 'PatientsPage.NextAppointment',
    defaultMessage: 'Next Appointment',
  },
  Patients: {
    id: 'Patients',
    defaultMessage: 'Patients',
  },
  AddNewPatient: {
    id: 'AddNewPatient',
    defaultMessage: 'Add New Patient',
  },
  AppointmentsFinder: {
    id: 'AppointmentsFinder',
    defaultMessage: 'Appointments Finder',
  },
  AppointmentID: {
    id: 'AppointmentID',
    defaultMessage: 'Appointment ID ',
  },
  PrescriptionID: {
    id: 'PrescriptionID',
    defaultMessage: 'Prescription ID ',
  },
  StatusUpdated: {
    id: 'StatusUpdated',
    defaultMessage: 'Updated',
  },
  PatientID: {
    id: 'PatientID',
    defaultMessage: "Patient ID",
  },
  OpenPrescriptions: {
    id: 'OpenPrescriptions',
    defaultMessage: 'Open Prescriptions',
  },
  PartlyScheduled: {
    id: 'PartlyScheduled',
    defaultMessage: 'Partly Scheduled',
  },
  Scheduled: {
    id: 'Scheduled',
    defaultMessage: 'Scheduled',
  },
  Unscheduled: {
    id: 'Unscheduled',
    defaultMessage: 'Unscheduled',
  },
  Registered: {
    id: 'Registered',
    defaultMessage: 'Registered',
  },
  Years: {
    id: 'years',
    defaultMessage: 'years',
  },
  PayerStatus: {
    id: 'PayerStatus',
    defaultMessage: 'Payer',
  },
  PatientName: {
    id: 'PatientName',
    defaultMessage: 'Patient Name',
  },
  Age: {
    id: 'Age',
    defaultMessage: 'Age',
  },
  PhoneNumber: {
    id: 'PhoneNumber',
    defaultMessage: 'Phone Number',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Submit: {
    id: 'Submit',
    defaultMessage: 'Submit',
  },
  Appointments: {
    id: 'Appointments',
    defaultMessage: 'Appointments',
  },
  PatientProfile: {
    id: 'PatientProfile',
    defaultMessage: 'Patient Profile',
  },
  Prescriptions: {
    id: 'Prescriptions',
    defaultMessage: 'Prescriptions',
  },
  UnscheduledFinderAvailable: {
    id: 'UnscheduledFinderAvailable',
    defaultMessage: 'There’re unscheduled appointments available. You can schedule them using Appointments Finder.',
  },
  AppointmentsWithoutPrescr: {
    id: 'AppointmentsWithoutPrescr',
    defaultMessage: 'You should add a new prescription prior to be able to add appointments.',
  },
  FinancialStatus: {
    id: 'FinancialStatus',
    defaultMessage: 'Financial Status',
  },
  ReferredBy: {
    id: 'ReferredBy',
    defaultMessage: 'Referred By',
  },
  LinkedAppointments: {
    id: 'LinkedAppointments',
    defaultMessage: 'Linked Appointments',
  },
  min: {
    id: 'min',
    defaultMessage: 'min'
  }
});
