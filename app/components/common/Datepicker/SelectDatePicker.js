//@flow
import * as React from 'react';
import moment from 'moment';

import 'moment/src/locale/de.js';
import 'moment/src/locale/ru.js';
import 'moment/src/locale/en-gb.js';

import _ from 'lodash';
import styled from 'styled-components';
import theme from './../../styles/colorScheme';
import { Select } from 'components/Input';
import { Row, Col, FieldsWrapCol, PropsToCSSParser } from 'components/styles/Grid';
import { numbericComporator } from "../../../utils/sortComporators";

type Props = {
  dateFormat: string,
  liveYears: number,
  selectProps: Array<any>,
  columnProps: Array<any>,
  wrapProps: any,
  required: boolean,
  monthView: 'short' | 'number'
};

type State = {
  day: number,
  month: number,
  year: number,
};

class SelectDatePicker extends React.Component<Props, State>{
  static defaultProps = {
    dateFormat: 'DD.MM.YYYY',
    liveYears: 117,
    selectProps: [{},{},{}],
    columnProps: [{},{},{}],
    required: false,
    monthView: 'number',
  };
  state = {
    day: -1,
    month: -1,
    year: -1,
  };
  constructor(props){
    super(props);
    this.manually = false;
    this.beginYear = moment().year() - this.props.liveYears + 1;
    const date = this.getDateFromProps(props);
    this.state = {
      day: date.date(),
      month: date.month(),
      year: date.year(),
    }
  }
  getDateFromProps(props){
    const { input, birth } = props;
    let value = {
      dd: birth.dd.input.value,
      mm: birth.mm.input.value,
      yyyy: birth.yyyy.input.value
    };

    if(typeof value !== 'string' && value.dd && value.mm && value.yyyy){
      try {
        value = `${value.dd || '01'}.${value.mm || '01'}.${value.yyyy || '0001'}`;
      } catch(e){
        value = '';
      }
    } else {
      value = false;
    }
    return moment(value, this.props.dateFormat);
  }
  getDateFromState(state){
    const { day, month, year } = state;
    let resultDate = day && month && year ? `${day}.${month + 1}.${year}`: false;
    return moment(resultDate, this.props.dateFormat);
  }
  shouldComponentUpdate(nextProps, nextState){
    const oldDateProps = this.getDateFromProps(this.props);
    const newDateProps = this.getDateFromProps(nextProps);

    if(oldDateProps.isValid() && newDateProps.isValid() && oldDateProps.diff(newDateProps) !== 0) {
      return true;
    } else if (!oldDateProps.isValid() && newDateProps.isValid()){
      return true;
    } else if (!_.isEqual(nextState, this.state)){
      return true;
    } else if (!_.isEqual(nextProps.birth, this.props.birth)){
      return true;
    }
    return false;
  }
  componentDidUpdate(prevProps, prevState){
    let propsDate = this.getDateFromProps(this.props);
    let stateDate = this.getDateFromState(this.state);
    if(propsDate.isValid() && !this.manually){
      this.setState({
        day: propsDate.date(),
        month: propsDate.month(),
        year: propsDate.year(),
      });
    }
  }
  getDayInSelectedMonth(state) {
    const {day, month, year} = state;
    let daysOfMonth = moment(`${month + 1}.${year >= this.beginYear ? year: moment().year()}`, 'MM.YYYY').daysInMonth();
    return daysOfMonth < day ? daysOfMonth : day;
  }

  handleChange(input, field, selectData){
    this.manually = true;
    let currentState = { ...this.state };
    currentState[field] = selectData.value;
    this.setState(currentState, () => {
      const { day, month, year } = this.state;
      const targetValues = {
        dd: this.getDayInSelectedMonth(this.state),
        mm: month + 1,
        yyyy: year
      };
      input.onChange(targetValues[input.name.split('.')[1]]);
      this.manually = false;
    });
  }
  handleBlur(input, field, e){
    let currentState = { ...this.state };
    this.setState(currentState, () => {
      const { day, month, year } = this.state;
      const targetValues = {
        dd: day,
        mm: month + 1,
        yyyy: year
      };
      input.onBlur(targetValues[input.name.split('.')[1]]);
    });
  }
  render(){
    const { formatMessage, locale } = this.context.intl;
    const {
      input,
      birth,
      names,
      width,
      liveYears,
      selectProps,
      columnProps,
      wrapProps,
      monthView,
      ...props,
    } = this.props;
    moment.locale(locale);

    let parsedWidth = width && PropsToCSSParser.parseUnits(width)[0],
      cssStrWidth = parsedWidth ? PropsToCSSParser.parseValues([parsedWidth]): 0;
    const colProps = {
      //[parsedWidth ? 'width': 'grow']: parsedWidth ? (parsedWidth[0] / 3) + parsedWidth[1]: '1'
    };
    const dayInput = {
      ...birth.dd.input,
      value: this.state.day
    };
    const monthInput = {
      ...birth.mm.input,
      value: this.state.month
    };
    const yearInput = {
      ...birth.yyyy.input,
      value: this.state.year
    };
    const { day, month, year } = this.state;
    const date = month ? moment(`${day || '01'}.${month + 1}.${year || moment().year()}`, this.props.dateFormat): false;

    const monthOptions = monthView === 'number' ?
      Array(12).fill(null).map((x, i) => ({ value: i, label: i + 1})) :
      moment.monthsShort('-MMM-').map((x, i) => ({ value: i, label: x}));

    return (
      <FieldsWrapCol {...wrapProps}>
        <Row>
          <Col {...colProps} {...(columnProps[0] ? columnProps[0]: {})}>
            <Select
              {...props}
              options={Array(date && date.isValid() ? date.daysInMonth(): moment().daysInMonth()).fill(null).map((x, i) => ({ value: i + 1, label: i + 1}))}
              placeholder="DD"
              input={dayInput}
              meta={birth.dd.meta}
              name={names[0]}
              overrideProps={{
                onChange: this.handleChange.bind(this, dayInput, 'day'),
                onBlur: this.handleBlur.bind(this, dayInput, 'day'),
                ...(selectProps[0] ? selectProps[0]: {})
              }}
            />
          </Col>
          <Col {...colProps} {...(columnProps[1] ? columnProps[1]: {})}>
            <Select
              {...props}
              options={monthOptions}
              placeholder="MM"
              input={monthInput}
              meta={birth.mm.meta}
              name={names[1]}
              overrideProps={{
                onChange: this.handleChange.bind(this, monthInput, 'month'),
                onBlur: this.handleBlur.bind(this, monthInput, 'month'),
                ...(selectProps[1] ? selectProps[1]: {})
              }}
            />
          </Col>
          <Col {...colProps} {...(columnProps[2] ? columnProps[2]: {})}>
            <Select
              {...props}
              options={Array(liveYears).fill(null).map((x, i) => ({ value: i + this.beginYear, label: i + this.beginYear})).sort(numbericComporator('label', 'desc'))}
              placeholder="YYYY"
              input={yearInput}
              meta={birth.yyyy.meta}
              name={names[2]}
              overrideProps={{
                onChange: this.handleChange.bind(this, yearInput, 'year'),
                onBlur: this.handleBlur.bind(this, yearInput, 'year'),
                ...(selectProps[2] ? selectProps[2]: {})
              }}
            />
          </Col>
          {cssStrWidth !== '100%' && <Col width={`calc(100% - ${cssStrWidth})`}/>}
        </Row>
      </FieldsWrapCol>
    )
  }
}

SelectDatePicker.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default SelectDatePicker;
