//@flow
import * as React from 'react';
import { Row } from './../../styles/Grid';
import { VerticalCenter } from "./../../styles/common";
import styled from 'styled-components';
import theme from './../../styles/colorScheme';

type Props = {
  title?: string,
  width: number,
  marginTop: number,
  dashed: boolean,
  dotted: boolean,
  color: string,
};

const Common = styled.div`
  border-bottom: ${props => props.width}px 
    ${props => props.dashed ? props.dotted ? 'dotted': 'dashed' : 'solid'} 
    ${props => props.color ? props.color: theme.Divider.main};
  flex-grow: 1;
`;

const SimpleDivider = styled(Common)`
  border-bottom-color: ${props => props.color ? props.color: theme.Divider.simple};
`;
const TextWrap = styled.div`
  font-weight: 600;
  color: ${theme.Font.divider};
  padding: 0px 20px;
`;

const DividerWrap = styled(VerticalCenter)`
  margin-top: ${props => props.marginTop}px;
  margin-bottom: ${props => props.marginBottom}px;
  margin-left: ${props => props.marginLeft}px;
  margin-right: ${props => props.marginRight}px;
`;

export default class Divider extends React.Component<Props>{
  static defaultProps = {
    width: 1,
    marginTop: 24,
    marginBottom: 22,
    dashed: false,
    dotted: false
  };
  render(){
    const { title, width, marginTop, marginBottom, marginLeft, marginRight, dashed, dotted, color, ...props } = this.props;
    return(
      <DividerWrap
        marginTop={marginTop}
        marginBottom={marginBottom}
        marginLeft={marginLeft}
        marginRight={marginRight}
        {...props}
      >
        { !title ? <SimpleDivider width={width} dashed={dashed} dotted={dotted} color={color}/>: [
          <Common key="0" width={width} />,
          <TextWrap key="1">{title}</TextWrap>,
          <Common key="2" width={width} />
        ]}
      </DividerWrap>
    );
  }
};
