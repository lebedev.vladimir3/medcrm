//@flow
import * as React from 'react';
import './styles.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

type Props = {
  show?: boolean,
  animate: boolean,
};

const transitionName = 'dropdown';

class Dropdown extends React.Component<Props, State>{
  static defaultProps = {
    show: undefined,
    animate: false,
  };
  constructor(props){
    super(props);
    if(this.isUncontrolled()){
      this.state = {
        show: props.show,
        hover: false,
        mouseDown: false,
        mouseUp: false,
        button: '',
      }
    }
  }
  isRightMouseButton(){
    return this.state.button === 2;
  }
  isLeftMouseButton(){
    return this.state.button === 0;
  }
  isHovered(){
    return this.state.hover;
  }
  isUncontrolled(){
    return this.props.show === undefined;
  }
  componentDidUpdate(){
    if(this.isUncontrolled()){
      if(this.state.show) this.menuDom.focus()
    } else {
      this.props.show && this.menuDom.focus()
    }
  };
  defaultToggleHandler(){
    this.setState({ show: !this.state.show });
  }
  defaultBlurHandler(){
    if(this.isUncontrolled() && !this.isHovered()){
      this.setState({ show: false });
    } else if(this.isUncontrolled() && this.isHovered() && !this.isRightMouseButton() && !this.state.mouseDown){
      setTimeout(() => { this.setState({ show: false }); }, 250);
    } else if(!this.isUncontrolled()) {
      this.props.onBlur();
    }
  }
  handleMouseEnter(){
    this.setState({ hover: true });
  }
  handleMenuMouseLeave(){
    this.setState({ hover: false, mouseDown: false, mouseUp: false });
  }
  handleMouseDown(e){
    this.setState({
      mouseDown: true,
      mouseUp: false,
      button: e.button
    });
  }
  handleMouseUp(e){
    this.setState({
      mouseDown: false,
      mouseUp: true,
      button: e.button
    });
  }
  handleClick(e) {
    console.log(e.button);
    if (e.button === 'click') {
      console.log('Left click');
    } else if (e.button === 2) {
      console.log('Right click');
    }
  }
  handleItemMouseLeave(){
    this.setState({ mouseDown: false, mouseUp: false });
  }
  renderMenu(){
    const { menuComponent, options, className, menuStyle, onBlur } = this.props;
    let MenuComponent = <div className="top-panel--user-menu" />;
    if(menuComponent){
      return menuComponent(this.props);
    }
    return React.cloneElement(MenuComponent, {
      ref: (dom) => { this.menuDom = dom; },
      tabIndex: -1,
      key: 1,
      className: MenuComponent.props.className,
      style: menuStyle,
      onMouseEnter: ::this.handleMouseEnter,
      onMouseLeave: ::this.handleMenuMouseLeave,
      onBlur: onBlur || ::this.defaultBlurHandler,
      //onClick: ::this.handleClick

    }, this.renderOptions(options))
  }
  renderOptions(){
    const { optionComponent, options, className, optionStyle, onBlur } = this.props;
    let OptionComponent = <div className="top-panel--user-menu--item" />;
    if(optionComponent){
      return optionComponent(this.props);
    }
    return options.map((item, index) => React.cloneElement(OptionComponent, {
      key: index,
      style: optionStyle,
      onClick: ::this.defaultBlurHandler,
    }, React.cloneElement(item, {
      onMouseDown: ::this.handleMouseDown,
      onMouseUp: ::this.handleMouseUp,
      onMouseLeave: ::this.handleItemMouseLeave
    })))
  }
  render(){
    const { className, children, onClick, onBlur, animate, menu, options } = this.props;
    const child = React.Children.toArray(children);
    const show = this.isUncontrolled() ? this.state.show: this.props.show;
    const childProps = {
      onClick: onClick || ::this.defaultToggleHandler,
      //onBlur: onBlur || ::this.defaultBlurHandler
    };
    return (
      <ReactCSSTransitionGroup
        transitionName={transitionName}
        transitionEnterTimeout={250}
        transitionLeaveTimeout={250}
        transitionEnter={animate}
        transitionLeave={animate}
        style={{
          position: 'relative'
        }}
      >
        {React.cloneElement(child[0], { key: 0, ...childProps })}
        {show ? (this.renderMenu()): null}
      </ReactCSSTransitionGroup>)
  }
}

export default Dropdown;
