//@flow
import * as React from 'react';
import styled from 'styled-components';
import theme from './../../styles/colorScheme';
import { Link } from 'react-router';

const StyledHeaderItemWrap = styled(Link)`
  height: 100%;
  padding-right: 10px;
  display: flex;
  align-items: center;
  background-color: #DEE1E3;
  color: ${theme.TemplateHeader.sub.font.color};
  font-weight: ${props => props.fontWeight || 600};
  cursor: ${props => props.link ? 'pointer': 'default'};
	padding-left:${props => props.paddingLeft ? props.paddingLeft : '0px'};
	background-color: ${props => props.backgroundColor || 'transparent'};
	cursor: ${props => props.to ? 'pointer' : 'auto'};
	&:not(:first-child){
	  padding-left: 12px;
	}
`;

const StyledIcon = styled.span`
  margin-right: 10px;
`;

type Props = {
  className?: string,
  children?: React.Node,
  // rightSide?: React.Node,
  icon: React.Element<*>,
};

export default class HeaderItem extends React.Component{
  static defaultProps = {
    className: null,
    children: null,
    // rightSide: null,
    icon: null,
  };
  render(){
    const { children, icon, className, style, brandLink, ...props } = this.props;
    return (
      <StyledHeaderItemWrap {...props} to={brandLink}>
        {icon && <StyledIcon>{icon}</StyledIcon>}
        <span>{children}</span>
      </StyledHeaderItemWrap>
    );
  }
}
