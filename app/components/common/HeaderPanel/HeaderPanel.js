import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import HeaderPanelComponent from './HeaderWrap';

const mapStateToProps = (state) => ({
  brandTitle: state.main.company.title,
  //brandIcon: null,
});

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(HeaderPanelComponent);
