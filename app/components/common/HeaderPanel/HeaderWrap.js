//@flow

import * as React from 'react';
import MdSettings from 'react-icons/lib/md/settings';
import { Link } from 'react-router';
import styled from 'styled-components';

import {
  StyledNavigationMap as NavigationMap,
  StyledRightSideWrap as RightSideWrap,
  StyledDefaultDivider as DividerAngle,
} from './styles';
import HeaderItem from './HeaderItem';

const StyledHeaderWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: ${props => props.background || '#FFFFFF'};
  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1);
  padding-right: 20px;
  height: ${props => props.height || '59px'};
`;

type Props = {
  className?: string,
  children?: React.Node,
  rightSide?: React.Node,
  brandTitle: React.Node,
  brandIcon: React.Element<*>,
  backgroundLeft: string,
  borderColor: string,
  linkWrap: boolean
};

export default class HeaderWrap extends React.Component<Props> {
  static defaultProps = {
    className: null,
    children: null,
    rightSide: null,
    brandTitle: 'Default Title',
    borderColor: "#DEE1E3",
    linkWrap: true,
    //brandIcon: <MdSettings size={22} />
  };
  renderItem(item){
    const { children, className, rightSide, brandTitle, brandIcon, borderColor, linkWrap, ...props  } = this.props;
    return React.cloneElement(item, { brandLink: item.props.link, ...props });
  }
  render(){
    {/* TODO: activating todo-feature in JetBrains' JSX */}
    const {
      children,
      className,
      rightSide,
      brandTitle,
      brandIcon,
      borderColor,
      brandLink,
      headerProps,
      ...props } = this.props;
    const childrenArray = React.Children.toArray(children);

    const headerItemProps = {
      paddingLeft: '30px',
      icon: brandIcon,
      backgroundColor: this.props.backgroundLeft,
      ...headerProps
    };
    return (
      <StyledHeaderWrap {...props}>
        <NavigationMap>
          <HeaderItem {...headerItemProps} brandLink={brandLink}>
            {brandTitle}
          </HeaderItem>
          {childrenArray.map((child, index) => {
           //TODO: Different values must be defined for first-header index and other items (not header).
           return [
              <DividerAngle
                backgroundColor={this.props.backgroundLeft}
                borderColor={borderColor}
              />,
              this.renderItem(child)
            ]
          })}
        </NavigationMap>
        <RightSideWrap style={{}}>
          {rightSide}
        </RightSideWrap>
      </StyledHeaderWrap>
    );
  }
}
