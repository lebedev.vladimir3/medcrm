import { Link } from 'react-router';
import styled from 'styled-components';

export default styled(Link)`color: #737B8C;`;
