import React from 'react';

export default class HeaderPanelHelper {
  static disableRightButtonInOrder({ component, buttonNumber }){
    return React.cloneElement(component, {
      rightSide: component.props.rightSide.map((item, index) => React.cloneElement(item, { disabled: index === buttonNumber }))
    })
  }
}
