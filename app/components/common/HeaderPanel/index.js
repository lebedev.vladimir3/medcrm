import React from 'react';

import HeaderWrap from './HeaderWrap';
import ReduxHeaderPanel from './HeaderPanel';
import BrandIcon from 'assets/icons/Home/Home.svg';

export HeaderPanel from './HeaderWrap';
export HeaderItem from './HeaderItem';

export HeaderPanelHelper from './helper';
export Link from './Link';

import jss from 'jss';

const ReduxHeaderPanelWrap = (props) => (
  <ReduxHeaderPanel
    brandIcon={<BrandIcon
      viewBox="0 0 20 21"
    />}
    {...props}
  />
);

export default ReduxHeaderPanelWrap;

const createGenerateClassName = () => (rule, sheet) => `${rule.key}`;

jss.setup({createGenerateClassName});

const style = {
  'tabs.theme-entity > .tabs__tab_active': {
    height: '56px'
  },
  '.tabs.theme-entity > .tabs__tab > .tabs__tab__icon_active': {
    marginTop: '1px'
  }
};
const sheet = jss.createStyleSheet(style);

export class SecondaryHeaderPanel extends React.Component {
  componentWillMount(){
    sheet.attach();
  }
  componentWillUnmount(){
    sheet.detach();
  }
  render(){
    return(
      <HeaderWrap
        height='72px'
        backgroundLeft={this.props.noneBg ? 'transparent':"#DEE1E3"}
        //borderColor="transparent"
        style={{
          background: '#F6F6F6',
          boxShadow: 'none',
          borderBottom: '1px solid #C9C9C9'
        }}
        {...this.props}
      />
    )
  }
}
