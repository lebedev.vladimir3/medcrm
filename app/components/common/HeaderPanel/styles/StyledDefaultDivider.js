import React from 'react';
import styled from 'styled-components';

const ArrowRight = styled.div`
    width: 0px;
    height: 0px;
    border-top: 50px solid transparent;
    border-bottom: 50px solid transparent;
    border-left: 16px solid ${props=>props.borderColor ? props.borderColor : 'white'};
`;

const ArrowRightBackground = styled.div`
    position: absolute;
    top: 0;
    left: -2px;
    width: 0px;
    height: 0px;
    border-top: 50px solid transparent;
    border-bottom: 50px solid transparent;
    border-left: 16px solid ${props => props.backgroundColor || 'white'};
`;

const Div = styled.div`
    position: relative;
`;


const component = (props)=>(
  <Div>
    <ArrowRight {...props} />
    <ArrowRightBackground {...props} />
  </Div>);

export default component;
