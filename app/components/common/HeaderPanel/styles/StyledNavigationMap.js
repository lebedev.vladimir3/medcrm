import fontSetting from 'components/styles/font';
import styled from 'styled-components';

const StyledNavigationMap = styled.div`
  color: #737B8C;
  font-size: ${fontSetting.baseSizePlus1}px;
  line-height: 22px;
  display: flex;
  align-items: center;
  height: 100%;
  overflow: hidden;
`;

export default StyledNavigationMap;
