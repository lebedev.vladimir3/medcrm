import styled from 'styled-components';

const StyledRightSideWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  a:first-child{
    margin-right: 20px;
  }
`;

export default StyledRightSideWrap;
