import React, { PropTypes } from 'react';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';


const RightTriangle = styled.div`
  width: 0;
	height: 0;
	border-bottom: 36px solid ${theme.Tab.FormHeader.background};
	border-right: 10px solid transparent;
`;

const ContentWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px 20px 0px 15px;
  color: ${props => props.color || theme.Tab.FormHeader.color};
  background-color: ${props => props.backgroundColor || theme.Tab.FormHeader.background};
`;

const Wrap = styled.div`
  display: flex;
  ${props => props.underline && 'border-bottom: 1px solid ' + (props.backgroundColor || theme.Tab.FormHeader.background) + ';'}
`;

const FormHeaderTab = (props) => (
  <Wrap {...props}>
    <ContentWrap>
      {props.children}
    </ContentWrap>
    <RightTriangle/>
  </Wrap>
);

export default FormHeaderTab;
