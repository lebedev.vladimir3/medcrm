//@flow
import * as React from 'react';
import cn from 'classnames';
import { classes as cls, states, themes } from './constants';
import SvgTabBorder from 'assets/icons/tab-border.svg';
import './styles/styles.scss';

type Props = {
  className?: string,
  label?: string,
  icon?: React.Element<any>,
  children?: React.Node,
  active: boolean,
  disabled: boolean,
  onTabClick: () => {},
  value: string | number,
  theme: themes,
  activeColor: string,
  style: any,
  width: string,
};

const getStateClasses = (prefix: string, state: { active: boolean, disabled: boolean }, base?: string) => {
  return cn(base, prefix, {
    [`${prefix}_${states.ACTIVE}`]: state.active,
    [`${prefix}_${states.DISABLED}`]: state.disabled,
  });
};

export default class Tab extends React.Component<Props> {
  static defaultProps = {
    className: null,
    label: 'default',
    icon: null,
    active: false,
    disabled: false,
    onTabClick: () => {},
    theme: themes.ENTITY,
    activeColor: '#59927C',
    style: {},
    width: ''
  };

  renderBase(){
    const { icon, label, theme, active, disabled, activeColor, iconActiveColor } = this.props;
    let prfxIcon = `${cls.TABS}__${cls.TAB}__${cls.TAB_ICON}`,
      wrapElement = theme === themes.SVG ? 'div': 'span';
    let result = [
      icon && React.cloneElement(icon, {
        key: 1,
        className: getStateClasses(prfxIcon, { active, disabled }),
        style:{
          color: active && iconActiveColor
        }
      }),
      label && React.createElement(wrapElement, {
        key: 0,
        className: `${cls.TABS}__${cls.LABEL_WRAP}`
      }, this.props.label)
    ];
    if(theme === themes.STATUS){
      let prfx = `${cls.TABS}__${cls.TAB_INNER_WRAP}`;
      result = <div
        className={getStateClasses(prfx, { active, disabled })}
        style={{
          borderBottomColor: active && activeColor
        }}
      >{result}</div>
    };
    return result;
  }
  getSVGProps({ isLeft, active } = {}){
    return {
      className: 'svg-border',
      viewBox: '1 0 20 49',
      style: {
        height: '50px',
        width: '20px',
        strokeWidth: '1px',
        transform: isLeft && 'rotateY(180deg)'
      }
    }
  }
  render(){
    const { children, className, active, disabled, onTabClick, value, theme, activeColor, style, width, icon, ...props } = this.props;
    const prfx = `${cls.TABS}__${cls.TAB}`;
    let styles = {
      ...style,
      width,
    };
    return (
      <div
        className={getStateClasses(prfx, { active, disabled }, className)}
        onClick={value !== undefined && !disabled && onTabClick.bind(this, value)}
        style={styles}
        {...props}
      >
        {theme === themes.SVG && active && <div className={`${prfx}__${cls.SVG_FILLER}`}/>}
        {theme === themes.SVG && <SvgTabBorder {...this.getSVGProps({ isLeft: true, active })}/>}
        {children ? children: this.renderBase()}
        {theme === themes.SVG && <SvgTabBorder {...this.getSVGProps({ active })}/>}
      </div>
    );
  }
}
