//@flow

import * as React from 'react';
import cn from 'classnames';
import { classes as cls, themes } from './constants';
import './styles/styles.scss';

type Props = {
  className?: string,
  children?: React.Node,
  theme: string,
  value: number | string,
  onTabClick: () => {},
  rightRemainArea: React.Node,
  activeColor: string
};

export default class Tabs extends React.Component<Props> {
  static defaultProps = {
    className: null,
    children: null,
    theme: themes.ENTITY,
    value: 0,
    onTabClick: () => {},
    rightRemainArea: null,
    activeColor: '#59927C'
  };
  render(){
    const { children, className, theme, value, rightRemainArea, ...props } = this.props;
    const childrenArray = React.Children.toArray(children);
    let classes = cn(className, cls.TABS, `theme-${theme}`);
    return (
      <div className={classes}>
        {childrenArray.map((child, index)=> React.cloneElement(child, {
          ...props,
          active: value == child.props.value,
          width: theme === themes.INNER_ENTITY && (100 / childrenArray.length) + '%',
          theme,
        }))}
        {rightRemainArea && (
          <div className={`${cls.TABS}__${cls.RIGHT_AREA}`}>
            {rightRemainArea}
          </div>
        )}
      </div>
    );
  }
}
