export const classes = {
  TABS: "tabs",
  TAB: "tab",
  TAB_INNER_WRAP: "tab-inner-wrap",
  TAB_ICON: "icon",
  RIGHT_AREA: "right-area",
  LABEL_WRAP: "label-wrap",
  SVG_FILLER: "svg-filler"
};

export const states = {
  ACTIVE: 'active',
  DISABLED: 'disabled',
};

export const themes = {
  ENTITY: "entity",
  STATUS: "status",
  INNER_ENTITY: "inner-entity",
  SVG: "svg",
};
