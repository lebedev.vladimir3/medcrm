import { defineMessages } from 'react-intl';

export default defineMessages({
  Required: {
    id: "Validation.Required",
    defaultMessage: "This field is required"
  },
  ShortNameTaken: {
    id: "Validation.ShortName.Taken",
    defaultMessage: "This short name is already taken"
  },
  ShortNameLength: {
    id: "Validation.ShortName.Length",
    defaultMessage: "Short Name must nit be longer than 10 letters"
  }
});
