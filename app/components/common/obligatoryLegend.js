//@flow
import * as React from 'react';
import styled from 'styled-components';
import theme from './../styles/colorScheme';
import { RequiredStar } from 'components/styles/common';

import { defineMessages } from 'react-intl';

const messages = defineMessages({
  Obligatory: {
    id: 'Obligatory',
    defaultMessage: 'Obligatory',
  }
});

type Props = {
  position: 'center' | 'left' | 'right'
};

const Wrap = styled.div`
  text-align: ${props => props.position};
`;
const Text = styled.span`
  padding-left: 15px;
  color: ${theme.Text.accentLow}
`;
const WrapStar = styled.span`
  position: relative;
`;

class obligatoryLegend extends React.Component<Props>{
  static defaultProps = {
    position: 'right'
  };
  render(){
    const { formatMessage } = this.context.intl;
    return (
      <Wrap {...this.props}>
        <WrapStar>
          <RequiredStar top="-6px"/>
        </WrapStar>
        <Text>{formatMessage(messages.Obligatory)}</Text>
      </Wrap>
    )
  }
}

obligatoryLegend.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default obligatoryLegend;
