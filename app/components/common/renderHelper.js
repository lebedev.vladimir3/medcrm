import React from 'react';
import moment from 'moment';
import { Input, Select, TextArea, DatePicker, Checkbox, RadioInput } from 'components/Input';

class multiCheckbox extends React.Component {
  componentWillUnmount(){
    const { name, input, disabled, id, ...otherProps } = this.props;
    const { meta, ...otherInputProps } = otherProps;
    const inputValue = [...input.value];
    const index = inputValue.indexOf(id);
    if(index > -1){
      inputValue.splice(index, 1);
      input.onChange(inputValue || []);
    }
  }
  render(){
    const { name, input, disabled, id, ...otherProps } = this.props;
    const { meta, ...otherInputProps } = otherProps;
    return (
      <Checkbox
        {...input}
        {...otherInputProps}
        name={name}
        disabled={disabled}
        checked={input.value.indexOf(id) !== -1}
        onChange={(event, checked) => {
          const newValue = [...input.value];
          if (checked) {
            newValue.push(id);
          } else {
            newValue.splice(newValue.indexOf(id), 1);
          }
          input.onChange(newValue || []);
        }}
        onBlur={() => {}}
      />
    );
  }
}

export default class renderHelper {
  static inputField = inputProps => <Input {...inputProps.input} {...inputProps} />;
  static selectField = renderProps => <Select {...renderProps.input} {...renderProps} />;
  static textAreaField = renderProps => <TextArea {...renderProps.input} {...renderProps} />;
  static datePickerField = props => (
    <DatePicker
      selected={props.input && props.input.value ? moment(props.input.value) : null}
      onChange={props.input && props.input.onChange}
      placeholderText={ props.placeholder !== undefined ? props.placeholder: "DD.MM.YYYY"}
      dateFormat="DD.MM.YYYY"
      style={{ width: props.width || '100px' }}
      disabled={props.disabled}
    />
  );
  static checkboxField = (inputProps) => <Checkbox
    style={{display: 'flex'}}
    {...inputProps.input}
    {...inputProps}
    checked={inputProps.input && !!inputProps.input.value}
  />;
  static multiCheckboxField = multiCheckbox;
  static radioField = (inputProps) => <RadioInput
    style={{
      height: '36px',
      marginRight: '24px'
    }}
    checked={inputProps.input && inputProps.input.checked}
    {...inputProps}
  />;

}
