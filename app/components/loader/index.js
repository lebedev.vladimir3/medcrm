import React from 'react';
import styled from 'styled-components';
import  ReactLoader from 'react-loader';
const LoaderWrapper = styled.div`
  position:fixed;
  left: 50%;
  top: 40%;
  color: #5383BD;
  z-index: 999999999;
`;
const Loader = (props) => (
  <LoaderWrapper><ReactLoader loaded={!props.loading}/>
  </LoaderWrapper>
);

export default Loader;
