import React from 'react';
import { ContentWrap } from "../Containers";
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

const BasicAlert = styled.div`
  padding: 14px 22px;
  border: 1px solid ${theme.Alert.base.border};
  background-color: ${theme.Alert.base.background};
  display: flex;
  align-items: center;
  border-radius: 3px;
  > .alert-icon {
    width: 22px;
    height: 22px;
    margin-right: 9px;
    color: ${theme.Alert.base.icon};
  }
`;

const WarningAlert = styled(BasicAlert)`
  border-color: ${theme.Alert.warning.border};
  background-color: ${theme.Alert.warning.background};
  > .alert-icon {
    color: ${theme.Alert.warning.icon};
  }
`;

const AlertTypes = {
  warning: WarningAlert
};

const Alert = ({ children, icon, contentWrapSetting, alertProps, type }) => {
  const AlertComponent = AlertTypes[type];
  return (
  <ContentWrap {...contentWrapSetting}>
    <AlertComponent {...alertProps}>
      {React.cloneElement(icon, {
        className: icon.props.className || 'alert-icon',
        viewBox: icon.props.viewBox || '0 0 40 40'
      })}
      {children}
    </AlertComponent>
  </ContentWrap>
)};

export default Alert;
