import styled from 'styled-components';

const AccentColorRow = styled.span`
  color: #667083;
`;
export default AccentColorRow;
