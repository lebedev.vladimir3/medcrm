import styled from 'styled-components';

const Divider = styled.div`
  border-bottom: 1px #DBDFEA solid;
  width: 100%;
  margin-bottom: 16px;
`;

export default Divider;
