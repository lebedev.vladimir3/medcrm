import styled from 'styled-components';

const RightPart = styled.span`
  float: right;
  font-weight: ${props => props.bold ? 'bold': 'normal'};
`;

export default RightPart;
