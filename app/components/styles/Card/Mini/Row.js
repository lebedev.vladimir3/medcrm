import styled from 'styled-components';
import colorScheme from './../../colorScheme';

const Row = styled.div`
  font-weight: ${props => props.bold ? 'bold': 'normal'};
  padding-bottom: 16px;
  &:last-child{
    padding: 0px;
  }
`;

export const HeaderRow = styled(Row)`
  color: ${colorScheme.MiniCard.header}
`;

export const SubHeaderRow = styled(Row)`
  color: ${colorScheme.MiniCard.subheader}
`;

export default Row;
