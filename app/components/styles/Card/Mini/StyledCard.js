import fontSetting from 'components/styles/font';
import { Card } from './../index';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

const StyledCard = styled(Card).attrs({
  className: 'mini-card'
})`
  width: 100%;
  padding: ${props => props.padding ? props.padding: '15px 20px'};
  color: #3B4950;
  font-size: ${fontSetting.baseSize}px; 
  line-height: 18px;
  border-bottom: 2px solid ${theme.Card.Footer.border};
`;

export default StyledCard;
