import React from 'react';
import styled from 'styled-components';
import theme from '../colorScheme';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import { Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { ContentWrap } from 'components/styles/Containers';

export const Card = styled.div.attrs({
  className: 'card'
})`
  border: 1px solid ${theme.Card.border};
  border-radius: ${props => props.borderRadius || '4px 4px'};
  background-color: ${theme.Card.background};
  //box-shadow: 0 0 3px 0 rgba(0,0,0,0.12), 0 1px 2px 0 rgba(0,0,0,0.24);
  position: relative;
`;

export const CardFooter = styled.div`
  background-color: ${theme.Card.Footer.background};
  border-bottom: 2px solid ${theme.Card.Footer.border};
`;

export const CardFooterCommon = (props) => (
  <CardFooter>
    <ContentWrap padding="10px 30px 10px 30px">
      <Row justifyContent="space-between" center noPadding>
        <Col>
          { props.onDelete &&
          <DeleteButton type="button" onClick={props.onDelete}>
            {props.deleteText}
          </DeleteButton>
          }
        </Col>
        <Col>
          <div>
            <GreyButton onClick={props.onCancel}>{props.cancelText}</GreyButton>
            <GreenButton type="submit" disabled={props.pristine}>
              {props.applyText}
            </GreenButton>
          </div>
        </Col>
      </Row>
    </ContentWrap>
  </CardFooter>
);
