import React from 'react';
import styled from 'styled-components';
import { FormHeaderTab } from 'components/common/Tabs';

export const ContentWrap = styled.div.attrs({
  className: 'content-wrap'
})`
  padding: ${props => props.padding || '10px'};
  border-top: ${props => props.borderTop || 'none'};
  //background-color: ${props => props.backgroundColor || 'none'};
`;

export const PrescriptionCategory = (props) => (
  <ContentWrap padding={props.rootPadding || '0px'}>
    <FormHeaderTab underline>
      {props.title}
    </FormHeaderTab>
    <ContentWrap padding={props.contentPadding || '30px 0px 30px 15px'}>
      {props.children}
    </ContentWrap>
  </ContentWrap>
);
