import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

const EntitiesWrap = styled.div`
  padding: 10px;
`;

const EntityWrap = styled.div`
  //border: 1px solid ${theme.FormEntity.border};
  //border-radius: 4px 4px 0 0;
  //background-color: ${theme.FormEntity.Wrap.background};
  box-sizing: border-box;
  &:not(:first-child){
    margin-top: 30px;
  }
`;

const EntityHeader = styled.div`
	height: 42px;
	background-color: ${theme.FormEntity.Header.background};
	padding: 5px 20px;
	border-bottom: 1px solid ${theme.FormEntity.border};
	color: ${theme.FormEntity.Header.color};
	opacity: 0.8;
	font-weight: 600;
	display: flex;
  justify-content: center;
  align-items: center;
`;

const EntityContent = styled.div`
  padding: 14px 16px 15px 16px;
  background-color: ${theme.FormEntity.Content.background};
  border: 1px solid ${theme.FormEntity.border};
`;

const EntityCounter = styled.span`
  margin-right: 10px;
`;

const EntityLabel = styled.span`
  margin-right: 7px;
  color: ${theme.FormEntity.Label.color}
`;

export {
  EntitiesWrap,
  EntityWrap,
  EntityHeader,
  EntityContent,
  EntityCounter,
  EntityLabel,
};
