import React, { PropTypes } from 'react';
import styled from 'styled-components';

const FieldsWrap = styled.div`
  box-sizing: border-box;
  border: 1px solid #E6E6E6;
  background-color: #F5F5F5;
  padding: ${props => props.padding ? props.padding: '30px'};
`;

export default FieldsWrap;
