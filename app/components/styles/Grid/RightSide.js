import React, { PropTypes } from 'react';
import styled from 'styled-components';

const RightSide = styled.div`
  padding-top: 63px;
  width: 260px;
  margin-left: 22px;
  > div {
    margin-bottom: 22px;
  }
  > div.button-wrap {
    margin-bottom: 17px;
  }
`;

export default RightSide;
