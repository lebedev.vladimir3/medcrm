import React, { PropTypes } from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

export RightSide from "./RightSide";
export LeftSide from "./LeftSide";

export const Form = styled.form`
  .rows-wrap > .grid__row {
    padding-bottom: 12px;
    &:last-child {
      padding-bottom: 0px;
    }
  }
`;

export class PropsToCSSParser {
  static allowUnits = ['em','px','%','rem'];
  static allowIndentsDirections = ['vertical','horizontal','top','left','bottom','right','all'];
  static defaults = {
    unit: 'px'
  };
  static parseUnits(cssStr){
    if(PropsToCSSParser.checkType(cssStr)){
      let regExp = new RegExp(`(\\d+)(${PropsToCSSParser.allowUnits.join('|')})`, 'gi');
      return _.chunk((cssStr + '')
        .split(regExp).filter(item => item.trim() !== ''),2)
        .map(item => {
          if(item.length === 1){
            item.push(PropsToCSSParser.defaults.unit)
          }
          return item;
        });
    } else return false;

  }
  static checkType(cssStr){
    if(typeof cssStr === 'string'){
      return cssStr.split(' ');
    } else if (typeof cssStr === 'number'){
      return [cssStr];
    } else if (Array.isArray(cssStr)){
      return cssStr;
    } else return false;
  }
  static parseValues(values){
    return values.map(item => item[0] + item[1]).join(' ');
  }
  static getColUnits(cssStr, defaultValue){
    let splittedCssStr = PropsToCSSParser.checkType(cssStr);
    if(Array.isArray(splittedCssStr) && splittedCssStr.length === 1){
      return `0${PropsToCSSParser.defaults.unit} ${PropsToCSSParser.parseValues(PropsToCSSParser.parseUnits(splittedCssStr[0]))}`;
    } else if(Array.isArray(splittedCssStr) && (splittedCssStr.length === 2 || splittedCssStr.length === 4)){
      return splittedCssStr.map(item => PropsToCSSParser.parseValues(PropsToCSSParser.parseUnits(item))).join(' ').trim();
    } else {
      return `0${PropsToCSSParser.defaults.unit} ${defaultValue + PropsToCSSParser.defaults.unit}` || 0;
    }
  }
  static getTextAlign(props){
    const allowValues = ['Left', 'Right', 'Center'];
    for(let value of allowValues){
      if(`text${value}` in props) return value.toLowerCase();
    }
  }
}

export const TableContent = styled.div`
  display: flex; 
  justify-content: center;
`;

export const RowsWrap = styled.div.attrs({
  className: 'rows-wrap'
})``;

export const Row = styled.div.attrs({
  className: 'grid__row'
})`
  display: flex;
  width: ${props => props.width || '100%'};
  padding-bottom: ${props => props.paddingBottom ? props.paddingBottom: props.noPadding ? '0px': '10px'};
  align-items: ${props => props.alignItems ? props.alignItems : props.center ? 'center' : 'flex-start'};
  font-weight: ${props => props.bold ? 'bold': 'normal'};
  justify-content: ${props => props.justifyContent || 'flex-start'};
  text-align: ${props => PropsToCSSParser.getTextAlign(props)};
  ${props => props.minHeight && ('min-height: ' + props.minHeight + ';')}
  ${props => props.height && ('height: ' + props.height + ';')}
`;

export const Col = styled.div.attrs({
  className: 'grid__column'
})`
  display: flex;
  flex-direction: column;
  width: ${props => props.width || 'auto'};
  padding: ${props => PropsToCSSParser.getColUnits(props.padding, 5)};
  flex-grow: ${props => props.grow || 0};
  justify-content: ${props => {
    if(props.right){
      return 'flex-end';
    } else if(props.middle){
      return 'center';
    }
    return 'flex-start'
  }};
  ${props => props.alignItems && ('align-items: ' + props.alignItems + ';') }
  ${props => props.height && ('height: ' + props.height + ';') }
  text-align: ${props => PropsToCSSParser.getTextAlign(props)};
  border-right: ${props => props.borderRight || 'none'};
  border-left: ${props => props.borderLeft || 'none'};
  &:first-child {
    padding-left: 0px;
  }
  &:last-child {
    padding-right: 0px;
  }
`;

export const FieldsWrapCol = styled(Col).attrs({
  //padding: 0,
  grow: "1"
})`
  padding: 0;
  & > .grid__row {
    padding-bottom: ${props => props.paddingBottom || '0px'};
    & > .grid__column {
      padding: ${props => PropsToCSSParser.getColUnits(props.padding, 6)};
      &:first-child {
        padding-left: 0px;
      }
      &:last-child {
        padding-right: 0px;
      }
      & > .grid__row {
        flex-grow: 1;
      }
    }
  }
`;

export const PageWrapper = styled.div`
  padding: 30px 0px;
  width: ${props => {
    if(props.short) return '830px';
    return '980px'
  }};
  margin: 0 auto;
`;

export const FormWrapper = styled.div`
  padding: 30px 0px;
  width: 830px;
  margin: 0 auto;
`;

export const PaddingTop = styled.div`
  padding-top: 32px;  
  padding-bottom: 32px;
`;

export const LabelCol = styled(Col).attrs({
  width: props => props.width || "213px",
  textRight: true,
  padding: props => props.padding || [0,40,0,0]
})``;

export const BorderRightCol = styled(Col)`
  border-right: 1px dashed ${theme.Divider.main};
`;
