const border = '#CBCBCB';
const inputBorder = '#B7BFC5';

// Input's style section has experimental inner structure (one level of nesting). PLEASE, DON'T USE THIS STYLE!
// Use code style like Button's section, for example, if you want to create new style section. Thank you!
// P.s.: At the moment we do not have to rework it.

module.exports = {
  HeaderPanel: {
    main: {},
    secondary: {
      header: {
        color: '#485262'
      },
      item: {
        color: '#4D5462'
      }
    }
  },
  Link: {
    base: '#1D3D7A',
  },
  Alert: {
    base: {
      background: '#ccc',
      border: '#878787',
      icon: '#4d4d4d',
    },
    warning: {
      background: '#FBF8EA',
      border: '#E2CC85',
      icon: '#BB9624'
    }
  },
  Drawer: {
    blackout: 'rgba(19,24,32,0.3)',
    background: '#202C3C',
    Items: {
      text: {
        normal: '#B7BDD0',
        active: '#DFE2E5'
      },
      border: {
        active: {
          main: '#5E84CB',
          accent: '#3E8971'
        },
        divider: '#565F6C'
      }
    },
    Select: {
      Options: {},
      background: 'transparent',
      border: '#9ba0a4',
      color: '#9CA1A5',
    }
  },
  Font: {
    divider: '#475263'
  },
  TemplateHeader: {
    menu: {},
    sub: {
      font: {
        color: '#475263'
      },
      divider: {
        fill: '#E3E3E3'
      }
    }
  },
  Divider: {
    main: '#ccc',
    simple: '#efefef'
  },
  Input: {
    requiredStar: '#C6680D',
    background: '#EDF0F3',
    backgroundDisabled: '#F2F2F2',
    colorPlaceholder: '#757779',
    backgroundDisabledWithValue: '#EBEBEB',
    border: inputBorder,
    color: '#2D2B2B',
    focus: 'white',
    error: '#B67C5E',
    disabled: {
      value: {
        border: '#CBCBCB'
      },
      noValue: {
        border: '#DEDEDE'
      }
    }
  },
  TextArea: {
    hasValue: {
      background: '#F6F7F9'
    }
  },
  Select: {
    arrow: '#5F6A7B',
  },
  Tags: {
    background: '#ECF0F3',
    border: inputBorder,
  },
  Text: {
    main: '#5F5F5F',
    accentLow: '#626262',
    secondAccentLow: '#787878'
  },
  Button: {
    main: {
      active: {
        background: '#5C7FC7',
        color: 'white'
      },
      disabled: {
        background: '#D5D5D5',
        color: '#696969',
      }
    },
    apply: {
      active: '#2A8B5B',
      disabled: '#81A796'
    },
    delete: {
      active: '#475263',
      disabled: '',
    },
    cancel: {
      background: 'white',
      border: '#A2A6AE',
      color: '#596372'
    },
    headerPanel: {
      main: {
        background: '#FFFFFF',
        border: '#9A9EA7'
      },
      gray: {
        background: '#F6F7F9'
      }
    }
  },
  Table: {
    header: {
      border: {
        time: border,
        accentTab: '#AFB6C0'
      },
      background: {
        main: '#7E8690',
        time: 'rgba(200,204,207,0.8)'
      },
      color: {
        main: 'white',
        time: 'rgba(36,36,36,0.75)'
      },
    },
    body: {
      row: {
        hover: '#fafafa',
        borderBottom: '#CDCDCD'
      },
      cell: {
        time: '#1A1919'
      }
    }
  },
  Tab: {
    MainEntity: {
      Staff: {
        icon: {
          color: '#5576B9'
        }
      }
    },
    FormHeader: {
      background: '#A0A4AE',
      color: 'white'
    }
  },
  Checkbox: {
    disabled: 'rgba(0, 0, 0, 0.26)',
    border: {
      main: '#8894A6',
      accent: '#93969C'
    },
    background: 'white',
    label: {
      disabled: '#7F7F7F',
      active: 'rgba(69,69,69,0.9)'
    }
  },
  Borders: {
    main: '#CDCDCD',
    divider: '#D5D5D5'
  },
  MiniCard: {
    backgrounds: {
      accent: '#EDEDED'
    },
    header: '#4F5A6D',
    subheader: '#1D1C1C'
  },
  Card: {
    border: '#CDCDCD',
    background: '#FFFFFF',
    Footer: {
      background: '#CDD3D7',
      border: '#BEBEBE'
    }
  },
  FormEntity: {
    Header: {
      //background: '#DBDDDF',
      color: '#242424',
    },
    Wrap: {
      background: '#EEEEEE'
    },
    Content: {
      background: '#DBDDDF',
    },
    Label: {
      color: '#5A5A5A'
    },
    border: border
  },
  Statuses: {
    DRAFT: "#8D8D8D",
    PARTIALLY_ISSUED: "#DBA22B",
    ISSUED: "#4687D6",
    PARTIALLY_SENT: "#DBA22B",
    SENT: "#DBA22B",
    PARTIALLY_PAID: "#00896D",
    PAID: "#00896D",
    UNSCHEDULED: "#D97118",
    PARTIALLY_SCHEDULED: "#DBA22B",
    SCHEDULED: "#DBA22B",
    PROVIDED: "#00896D",
    CANCELLED: "#8D8D8D",
    RE_SCHEDULED: "#DBA22B",
    NO_SHOW: "#ca0000",
    ARRIVED: "#4687D6",
    PERFORMING: "#4687D6",
  },
  Other: {
    Oval: '#6281BC'
  },
  Gender: {
    Male: '#29407F',
    Female: '#6C3989'
  }
};
