import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

const Oval = (parent) => parent.extend`
    height: 25px;
    width: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    background-color: ${theme.Other.Oval};
    margin-top: -6px;
    margin-right: -6px;
    color: white;
`;

export default Oval;
