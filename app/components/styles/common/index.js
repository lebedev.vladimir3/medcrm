import fontSetting from 'components/styles/font';
import React from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';
import theme from '../colorScheme';

export Oval from './Oval';

export const RequiredStar = styled((props) => <span className={props.className}>*</span>)`
  color: ${theme.Input.requiredStar};
  position: absolute;
  top: ${props => props.top ? props.top: '-3px'};
  right: ${props => props.right ? props.right: '-10px'};
  font-size: ${fontSetting.baseSizeMinus1}px;
`;

const FormLabelComponent = (props) => (<span className={props.className}>
  {props.children}
  {props.required && <RequiredStar />}
</span>);

export const FormLabel = styled(FormLabelComponent)`
  position: relative;
  font-weight: bold;
  color: ${theme.Text.main};
  > span {
    font-weight: normal;
  }
`;

export const FloatRight = styled.div`
  float: right;
`;

export const FloatLeft = styled.div`
  float: left;
`;

export const JustifyBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

export const FlexEnd = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const Flex = styled.div`
  display: flex;
`;

export const VerticalCenter = styled(Flex)`
  align-items: center;
`;


const ColorInside = styled.div`
  height: 100%;
  width: 100%;
  background-color: ${props=>props.color};
`;
const ColorWrapper = styled.div`
  border: 4px solid #BDC4C9;
  height: 24px;
  width: 24px;
`;

export const CustomColor = (props)=>
  <ColorWrapper {...props}>
    <ColorInside color={props.color} />
  </ColorWrapper>

export const Tag = styled.span`
  background-color: ${theme.Tags.background};
  border: 1px solid ${theme.Tags.border};
  border-radius: 14px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 5px 8px;
  line-height: 15px;
  transition: all 250ms ease-out;
`;

export const StyledLink = styled(Link)`
  color: ${props => props.color || theme.Text.main};
  text-decoration: none;
`;

export const DropdownStyledLink = styled(StyledLink)`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;
