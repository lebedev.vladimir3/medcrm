const baseSize = 15;
export default {
  fontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
  logoFontFamily: 'uni_neuebold',
  baseSizeMinus4: baseSize - 4,
  baseSizeMinus3: baseSize - 3,
  baseSizeMinus2: baseSize - 2,
  baseSizeMinus1: baseSize - 1,
  baseSize,
  baseSizePlus1: baseSize + 1,
  baseSizePlus2: baseSize + 2,
  baseSizePlus3: baseSize + 3,
  baseSizePlus4: baseSize + 4,
  baseSizePlus5: baseSize + 5,
  baseSizePlus6: baseSize + 6,
  baseSizePlus7: baseSize + 7,
  baseSizePlus8: baseSize + 8,
  baseSizePlus9: baseSize + 9,
};
