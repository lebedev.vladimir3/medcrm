import styled from 'styled-components';
import theme from './colorScheme';

import Unscheduled from 'assets/icons/PrescriptionStatus/Unscheduled.svg';
import PartiallyScheduled from 'assets/icons/PrescriptionStatus/PartiallyScheduled.svg';
import Scheduled from 'assets/icons/PrescriptionStatus/Scheduled.svg';
import Draft from 'assets/icons/PrescriptionStatus/Draft.svg';
import Issued from 'assets/icons/PrescriptionStatus/Paperwork.svg';
import Arrived from 'assets/icons/PrescriptionStatus/Arrived.svg';
import Performing from 'assets/icons/PrescriptionStatus/Processing.svg';
import Provided from 'assets/icons/PrescriptionStatus/Provided.svg';
import Cancelled from 'assets/icons/PrescriptionStatus/Cancelled.svg';
import NoShow from 'assets/icons/PrescriptionStatus/No-Show.svg';
import Sent from 'assets/icons/PrescriptionStatus/InvoiceSent.svg';
import Paid from 'assets/icons/PrescriptionStatus/Paid.svg';

export {
  Unscheduled,
  PartiallyScheduled,
  Scheduled,
  Draft,
  Issued,
  Arrived,
  Performing,
  Provided,
  Cancelled,
  NoShow,
  Sent,
  Paid
}
