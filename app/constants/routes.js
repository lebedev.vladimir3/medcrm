export const MAIN = {
  PATIENTS: {
    HOME: '',
    PRESCRIPTIONS: 'prescriptions',
    NEW_PRESCRIPTION: 'new-prescription',
    APPOINTMENTS: 'appointments',
    APPOINTMENT_FINDER: 'appointment-finder',
  },
  PRESCRIPTIONS: {
    STATUSES: {
      OPEN: '#open',
      COMPLETED: '#completed',
      CANCELLED: '#cancelled', // Archieved
    },
    EDIT: {
      PRESCRIPTION_CARD: 'prescription-card',
      MEDICAL_SERVICES: 'medical-services',
      TREATMENT_REPORTS: 'treatment-report'
    }
  },
  APPOINTMENTS: {
    STATUSES: {
      PENDING: '#pending',
      DONE: '#done',
      CANCELLED: '#cancelled', // Archieved
    }
  },
  SCHEDULE: {
    PATTERNS: '#patterns',
    WORKING_HOURS: '#working-hours'
  },
  STAFF: {
    PERSONAL_DETAILS: '',
    WORKING_HOURS: '#working-hours'
  },
  RESOURCES: {
    EQUIPMENTS: 'equipments',
    ROOMS: 'rooms',
  }
};
