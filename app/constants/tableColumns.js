export const APPOINTMENT_TABLE_COLUMN_NAME = {
  TIME: 'Time',
  SERVICE: 'Service',
  DURATION: 'Duration',
  STAFF: 'Staff',
  ROOM: 'Room',
  PRESCRIPTION: 'Prescription',
  STATUS: 'Status',
  FINANCIAL_STATUS: 'Financial Status',
  CHECKBOX: 'Checkbox',
};
