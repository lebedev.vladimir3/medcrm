export const honorifics = {
  DR: "DR",
  DR_MED: "DR_MED",
  PROFF: "PROFF"
};

export const personal = {
  MR: "mr",
  MRS: "mrs",
  MS: "ms",
  HERR: "Herr",
  FRAU: "Frau"
};
