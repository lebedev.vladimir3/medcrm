import {
  RESOURCE_UNAUTHORIZED, CHECK_TOKEN,
  INCREASE_REQUESTS,
  DECREASE_REQUESTS,
  CLEAR_REQUESTS,
} from './constants';


export function resourceUnauthorized() {
  console.log('ACTION')
  return { type: RESOURCE_UNAUTHORIZED };
}


export function initApp(data) {
  return { type: CHECK_TOKEN, payload: data };
}

export function increaseRequests(data) {
  return { type: INCREASE_REQUESTS, data: data };
}

export function decreaseRequests(data) {
  return { type: DECREASE_REQUESTS, data: data };
}

export function clearNumberRequests() {
  return { type: CLEAR_REQUESTS};
}

