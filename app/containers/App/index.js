import { initApp } from './actions';
import { connect } from 'react-redux';
import React from 'react';
import Loader from '../../components/loader';
import { clearNumberRequests } from './actions';

class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
  };

  componentDidMount() {
    this.props.initApp(localStorage.medcrm);
  }

  render() {
    const isPending = this.props.pendingRequests > 0;
    return (
      <div
        style={{
        pointerEvents:isPending ? 'none' : 'auto',
        opacity: isPending ? 0.8 : 1
        }}
      >
        { <Loader loading={isPending} />}
        {this.props.appReady && React.Children.toArray(this.props.children)}
      </div>
    );
  }
}


const mapDispatchToProps = {
  initApp,
  clearNumberRequests,
};


function mapStateToProps(state) {
  return {
    appReady: state.App.appReady,
    pendingRequests: state.App.pendingRequests,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(App);