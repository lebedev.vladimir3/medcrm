import {
  LOGIN_SUCCESS,
} from '../AuthPage/constants';
import {
  SETUP_APP,
  LOAD_USER,
  INCREASE_REQUESTS,
  DECREASE_REQUESTS,
  CLEAR_REQUESTS,
} from './constants';
const initialState = {
  appReady: false,
  pendingRequests: 0,
};

function appReducer(state = initialState, action) {

  switch (action.type) {
    case LOGIN_SUCCESS:
      return { ...state, user: action.data };
    case SETUP_APP:
      return { ...state, appReady: action.data };
    case LOAD_USER:
      return { ...state, user: action.data };
    case INCREASE_REQUESTS:
      return { ...state, pendingRequests: action.data + 1 };
    case DECREASE_REQUESTS:
      return { ...state, pendingRequests: action.data - 1 };
    case CLEAR_REQUESTS:
      return { ...state, pendingRequests: 0 };
    default:
      return state;
  }
}

export default appReducer;
