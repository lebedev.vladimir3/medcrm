import { takeLatest, fork, take, cancel, put } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import axios from 'axios';

import {
  RESOURCE_UNAUTHORIZED,
  CHECK_TOKEN,
  SETUP_APP,
  LOAD_USER,
} from './constants';
import callApi from 'utils/callApi';

export function* unauthorize() {
  try {
    console.log('Unauthorizing');
    browserHistory.push('/auth');
    localStorage.removeItem('medcrm');
  } catch (error) {
    console.error(error);
  }
}

export function* checkToken(action) {
  try {
    const response = yield callApi(axios.post, '/api/auth/isAuth', { token: action.payload });
    if (response.data.user === null) {
      browserHistory.push('/auth');
    } else {
      yield put({ type: LOAD_USER, data: response.data.user });
    }
    yield put({ type: SETUP_APP, data: true });
    // localStorage.setItem('medcrm', response.data.token);
    // browserHistory.push('/main/staff');
  } catch (error) {
    // browserHistory.push('/main/staff');
  }
}


export function* checkTokenSaga() {
  const watcher = yield takeLatest(CHECK_TOKEN, checkToken);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

export function* topSaga() {
  const watcher = yield takeLatest(RESOURCE_UNAUTHORIZED, unauthorize);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}


export default [
  topSaga,
  checkTokenSaga,
];
