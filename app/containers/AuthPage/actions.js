/*
 *
 * AuthPage actions
 *
 */
import axios from 'axios';


import {
  DEFAULT_ACTION,
  LOGIN_REQUEST,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


export function auth(...data) {
  return { type: LOGIN_REQUEST, payload: data[0] };
}

export function login(email, password) {
  return axios.post('/api/auth/login', { email, password });
}
