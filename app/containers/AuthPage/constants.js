/*
 *
 * AuthPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AuthPage/DEFAULT_ACTION';
export const LOGIN_REQUEST = 'app/AuthPage/LOGIN_REQUEST';
export const LOGIN_ERROR = 'app/AuthPage/LOGIN_FAILURE';
export const LOGIN_SUCCESS = 'app/AuthPage/LOGIN_SUCCESS';
