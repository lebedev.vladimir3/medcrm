import fontSetting from 'components/styles/font';
/*
 *
 * AuthPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import styled from 'styled-components';

import { auth } from './actions';

import Logo from './../../components/Logo';
import messages from './messages';

import { BlueButton } from './../../components/common/Button';
import { Input } from './../../components/Input'
import { FieldsWrap } from './../../components/styles/Forms';
import { ContentWrap } from './../../components/styles/Containers';
import { Row, Col } from './../../components/styles/Grid';
import { Card } from './../../components/styles/Card';
import theme from './../../components/styles/colorScheme';

const AuthWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #EEEEEE;
  height: 100%;
  width: 100%;
  flex-direction: column;
`;

const TopPart = styled.div`
  display: flex;
  background-color: #FFFFFF;
  flex-direction: column;
  align-items: center;
  min-height: 277px;	
  width: 370px;	
  border-radius: 4px;
  box-shadow: 0 0 3px 0 rgba(0,0,0,0.12), 0 1px 2px 0 rgba(0,0,0,0.24);
`;

const FieldsWrapExt = FieldsWrap.extend`
  //height: 151px;
  width: 352px;
`;

const BottomPart = styled.div`
  height: 49px;
  width: 350px;
  border-radius: 0 0 4px 4px;
  background-color: #F8FAFC;
  box-shadow: 0 -1px 0 0 #E7E9ED;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #7F8FA4;
`;
const ErrorMessage = styled.div`
  margin-bottom: 15px;
  width: 290px;
  border: 1px solid;
  border-radius: 0 0 4px 4px;
  padding: 15px;
  background-color: #ffefe9;
  border-color: #f2ab99;
  box-shadow: 0 -1px 0 0 #E7E9ED;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  color: #7F8FA4;
`;

const Icon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  margin-top: -35px;
  height: 70px;
  width: 70px;
  background-color: #0066A6;
  margin-bottom: 15px;

`;

const ButtonWrap = styled.div`
  padding-top: 14px;
`;

const ForgotPasswordWrap = styled.div`
	text-align: center;
	span {
	  height: 19px;
    width: 126px;
    color: #4A69AE;
    font-size: ${fontSetting.baseSizePlus1}px;
    line-height: 19px;
	}
`;

const Button = styled(BlueButton)`
	height: 40px;
	width: 100%;
	border-radius: 4px;
`;

const MessageWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`


export class AuthPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.clickSubmit = this.clickSubmit.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);

    this.state = {
      email: '',
      password: '',
    };
  }

  clickSubmit(e) {
    e.preventDefault();
    this.props.dispatch(auth({ email: this.state.email, password: this.state.password }));
  }

  handleEmail(e) {
    this.setState({ email: e.target.value });
  }

  handlePassword(e) {
    this.setState({ password: e.target.value });
  }

  render() {
    const { loginFailed } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    return (
      <AuthWrapper>
        <Helmet title="MedORG - Login" />
        <MessageWrap>
          {loginFailed && <ErrorMessage>
            <span style={{ fontWeight: 'bold' }}>Unable to log in.</span>
            <span>Please check that you have entered your <b>login</b> and <b>password</b> correctly.</span>
          </ErrorMessage>}
        </MessageWrap>
        <form onSubmit={this.clickSubmit}>
          <Card style={{ width: '370px' }}>
            <ContentWrap padding="40px 40px 28px 40px">
              <Row paddingBottom="13px">
                <Input
                  placeholder={formatMessage(messages.EMail)}
                  value={this.state.email}
                  onChange={this.handleEmail}
                  style={{ height: '40px'}}
                />
              </Row>
              <Row paddingBottom="20px">
                <Input
                  placeholder={formatMessage(messages.Password)}
                  value={this.state.password}
                  onChange={this.handlePassword}
                  type="password"
                  style={{ height: '40px'}}
                />
              </Row>
              <Row paddingBottom="0px">
                <Button type="submit">
                  <span>{formatMessage(messages.SignIn)}</span>
                </Button>
              </Row>
              <Row paddingBottom="0px" justifyContent="center">
                <ContentWrap padding="30px 0px 0px 0px">
                  <ForgotPasswordWrap>
                    <span>{formatMessage(messages.ForgotPassword)}</span>
                  </ForgotPasswordWrap>
                </ContentWrap>
              </Row>
            </ContentWrap>
          </Card>
          {/*<BottomPart><span style={{ display: 'none' }}>{this.context.intl.formatMessage(messages.ForgotPassword)}?</span></BottomPart>*/}
        </form>
      </AuthWrapper>
    );
  }
}

AuthPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

AuthPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loginFailed: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    loginFailed: state.auth.loginFailed,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    auth,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
