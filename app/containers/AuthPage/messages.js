import { defineMessages } from 'react-intl';

export default defineMessages({
  EMail: {
    id: 'EMail',
    defaultMessage: 'E-mail',
  },
  Password: {
    id: 'Password',
    defaultMessage: 'Password',
  },
  SignIn: {
    id: 'SignIn',
    defaultMessage: 'Sign in',
  },
  ForgotPassword: {
    id: 'ForgotPassword',
    defaultMessage: 'Forgot Password?',
  },
});
