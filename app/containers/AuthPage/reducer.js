/*
 *
 * AuthPage reducer
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN_SUCCESS,
  LOGIN_REQUEST,
  LOGIN_ERROR,
} from './constants';

const initialState = {
  loginFailed: false,
};

function authPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOGIN_REQUEST:
      return { ...state, pending: true };
    case LOGIN_SUCCESS:
      return { ...state, user: action.data, pending: false };
    case LOGIN_ERROR:
      return { ...state, loginFailed: true, pending: false  };
    default:
      return state;
  }
}

export default authPageReducer;
