import { call, put, takeLatest, cancel, take, fork } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

import { login } from './actions';
import { LOGIN_REQUEST, LOGIN_ERROR, LOGIN_SUCCESS } from './constants';


export function* authorize(action) {
  const request = action.payload;
  try {
    const response = yield call(login, request.email, request.password);
    yield put({ type: LOGIN_SUCCESS, data: response.data.user });
    localStorage.setItem('medcrm', response.data.token);
    browserHistory.push('/main/staff');
  } catch (error) {
    yield put({ type: LOGIN_ERROR, error: error.message });
  }
}


export function* topSaga() {
  const watcher = yield fork(takeLatest, LOGIN_REQUEST, authorize);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

export default [
  topSaga,
];
