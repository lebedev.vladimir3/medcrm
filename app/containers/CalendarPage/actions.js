/*
 *
 * CalendarPage actions
 *
 */
import axios from 'axios';

import {
  FETCH_USERS_REQUEST,
  FETCH_APPOINTMENTS_REQUEST,
  CHANGE_CURRENT_STAFF,
  FETCH_EQUIPMENTS_REQUEST,
  CHANGE_CURRENT_EQUIPMENT,
  CHANGE_CURRENT_ROOM,
  FETCH_ROOM_LIST_REQUEST,
} from './constants';

export function loadUsers() {
  return { type: FETCH_USERS_REQUEST };
}

export function loadAppointments() {
  return { type: FETCH_APPOINTMENTS_REQUEST };
}

export function updateAppointment(id, data) {
  axios.put(`/api/appointments/${id}`, { ...data, force: true });
}

export function changeCurrentStaff(data) {
  return { type: CHANGE_CURRENT_STAFF, data: data };
}

export function changeCurrentEquipment(data) {
  return { type: CHANGE_CURRENT_EQUIPMENT, data: data };
}

export function changeCurrentRoom(data) {
  return { type: CHANGE_CURRENT_ROOM, data: data };
}

export function loadEquipments() {
  return { type: FETCH_EQUIPMENTS_REQUEST };
}

export const loadRooms = () => ({ type: FETCH_ROOM_LIST_REQUEST });