import fontSetting from 'components/styles/font';
import React from 'react';
import styled from 'styled-components';
import messages from '../MainContainer/messages';
const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(19,24,32,0.3);
  display: flex;
  z-index: 9999;
`;

const ModalWindow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin: auto;
  height: 200px;
  width: 400px;
  border: 1px solid #DADFEA;
  border-radius: 4px;
  background-color: #FFFFFF;
  box-shadow: 0 4px 4px 2px rgba(0,0,0,0.13);
  
  z-index: 9999;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const ModalHeader = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 200px;
  width: 400px;
   z-index:19999;

`;

const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 120px;
  margin: 0 auto;
  border-radius: 4px;
  font-size: ${fontSetting.baseSize}px;
  font-weight: 600;
  line-height: 19px;
  cursor: pointer;
  border: 1px solid #B6BDCE;
  color: #596372;
`;


const renderDate = (a, b)=> {
  const aHours = (a.getHours() < 10 ? '0' : '') + a.getHours();
  const bHours = (b.getHours() < 10 ? '0' : '') + b.getHours();
  const aMinutes = (a.getMinutes() < 10 ? '0' : '') + a.getMinutes();
  const bMinutes = (b.getMinutes() < 10 ? '0' : '') + b.getMinutes();

  return aHours + ':' + aMinutes + ' - ' + bHours + ':' + bMinutes;
};

class AppointmentModal extends React.Component {
  componentDidMount(){
    this.modal.focus();
  }
  render() {
    const { endTime, startTime, doctorName, service, handleClose, patientName, room } = this.props;
    return (
      <Background>
        <ModalWindow>

          <ModalHeader
            autoFocus
            onBlur={this.props.onBlurEvent}
            innerRef={(node) => {this.modal = node}}
            tabIndex="-1">
            <Row>
              <div>Time:</div>
              <div>{renderDate(startTime, endTime)}</div>
            </Row>
            <Row>
              <div>Doctor:</div>
              <div>
                {doctorName}
              </div>
            </Row>
            <Row>
              <div>Patient Name:</div>
              <div>
                {patientName}
              </div>
            </Row>
            <Row>
              <div>Service:</div>
              <div>
                {service}
              </div>
            </Row>
            <Row>
              <div>Room:</div>
              <div>
                {room}
              </div>
            </Row>
            <Button onClick={handleClose}>{this.context.intl.formatMessage(messages.Close)}</Button>
          </ModalHeader>
        </ModalWindow>
      </Background>
    );
  }
}
AppointmentModal.contextTypes ={
  intl:React.PropTypes.object.isRequired
}
export default AppointmentModal;
