/*
 *
 * CalendarPagePage constants
 *
 */

export const FETCH_USERS_REQUEST = 'app/CalendarPage/FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'app/CalendarPage/FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'app/CalendarPage/FETCH_USERS_FAILURE';

export const FETCH_APPOINTMENTS_REQUEST = 'app/CalendarPage/FETCH_APPOINTMENTS_REQUEST';
export const FETCH_APPOINTMENTS_SUCCESS = 'app/CalendarPage/FETCH_APPOINTMENTS_SUCCESS';
export const FETCH_APPOINTMENTS_ERROR = 'app/CalendarPage/FETCH_APPOINTMENTS_FAILURE';

export const FETCH_EQUIPMENTS_REQUEST = 'app/CalendarPage/FETCH_EQUIPMENTS_REQUEST';
export const FETCH_EQUIPMENTS_SUCCESS = 'app/CalendarPage/FETCH_EQUIPMENTS_SUCCESS';
export const FETCH_EQUIPMENTS_FAILURE = 'app/CalendarPage/FETCH_EQUIPMENTS_FAILURE';

export const FETCH_ROOM_LIST_REQUEST = 'app/CalendarPage/FETCH_ROOM_LIST_REQUEST';
export const FETCH_ROOM_LIST_SUCCESS = 'app/CalendarPage/FETCH_ROOM_LIST_SUCCESS';
export const FETCH_ROOM_LIST_FAILURE = 'app/CalendarPage/FETCH_ROOM_LIST_FAILURE';

export const CHANGE_CURRENT_STAFF = 'app/CalendarPage/CHANGE_CURRENT_STAFF';
export const CHANGE_CURRENT_EQUIPMENT = 'app/CalendarPage/CHANGE_CURRENT_EQUIPMENT';
export const CHANGE_CURRENT_ROOM = 'app/CalendarPage/CHANGE_CURRENT_ROOM';

