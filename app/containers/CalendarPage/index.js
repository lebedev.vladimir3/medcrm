/*
 *
 * CalendarPage
 *
 */
import Helmet from 'react-helmet';

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import * as jspdf from 'jspdf';
import moment from 'moment';
import 'moment/src/locale/en-gb';
import 'jspdf-autotable';
import 'react-select/dist/react-select.css';
import 'icheck/skins/all.css'; // or single skin css
import BigCalendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Modal from './appointModal';
import './style.scss';
import { fetchAppointments, setSelectedDayCalendar } from './../MainContainer/actions';
import {
  updateAppointment,
  loadAppointments,
  changeCurrentStaff,
  loadUsers,
  loadEquipments,
  loadRooms,
} from './actions';

moment.locale('en-gb');
BigCalendar.momentLocalizer(moment);

const TableWrapper = styled.div`
  padding: 30px 30px 0 30px;
  height: 85%;
  width: 100%;
`;

export class CalendarPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props, context) {
    super(props, context);
    let selectedTab = 0;
    switch (window.location.pathname) {
      case '/main/staff':
        selectedTab = 2;
        break;
      case '/main/profile':
        selectedTab = 1;
        break;
      default:
        selectedTab = 0;
        break;
    }
    this.changeAgenda = this.changeAgenda.bind(this);
    this.calendarSelect = this.calendarSelect.bind(this);
    this.onTimeSorted = this.onTimeSorted.bind(this);
    this.dispatchSelectedDayCalendar = this.dispatchSelectedDayCalendar.bind(this);
    this.dateSelected = this.dateSelected.bind(this);
    this.onEventClick = this.onEventClick.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);

    this.state = {
      selectedTab,
      toggleStaff: true,
      currentAgenda: 1,
      timeSorted: -1,
      appointments: this.props.appointments ? this.props.appointments : [],
      users: this.props.users,
      showModal: false,
      didUpdate: false,
    };
  }

  componentDidMount() {
    this.props.dispatch(this.props.loadAppointments());
    this.props.dispatch(this.props.loadUsers());
    this.props.dispatch(this.props.loadEquipments());
    this.props.dispatch(this.props.loadRooms());
    this.dispatchSelectedDayCalendar(this.getMonday(new Date()));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.appointments.length !== this.state.appointments.length || nextProps.users.length !== this.state.users.length) {
      this.setState({
        appointments: nextProps.appointments,
        users: nextProps.users,
      });
    }
  }

  componentDidUpdate() {
    if (this.props.users.length !== 0 && !this.state.didUpdate) {
      this.props.dispatch(this.props.changeCurrentStaff(this.props.users[0]._id));
      this.setState({ didUpdate: true });
    }
  }

  dispatchSelectedDayCalendar(date) {
    this.props.dispatch(this.props.setSelectedDayCalendar(date));
  }

  onTimeSorted() {
    const appointments = this.state.appointments;
    let timeSorted = this.state.timeSorted;

    if (timeSorted === -1 || timeSorted === 0) {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        if (date1 > date2) return 1;
        if (date2 > date1) return -1;
        return 0;
      });
      timeSorted = 1;
    } else {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);

        if (date1 < date2) return 1;
        if (date2 < date1) return -1;
        return 0;
      });
      timeSorted = 0;
    }

    this.setState({
      staffList: appointments,
      timeSorted,
    });
  }

  getMonday(d) {
    let day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  changeAgenda() {
    this.setState({
      currentAgenda: (this.state.currentAgenda + 1) % 2,
    });
  }

  calendarSelect(date) {
    this.setState({
      SelectedDayCalendar: date,
    });
  }

  dateSelected(date) {
    this.dispatchSelectedDayCalendar(this.getMonday(date));
    return date;
  }

  eventStyleGetter(event, start, end, isSelected) {
    const style = {
      backgroundColor: event.hexColor,
      color: event.color,
      border: '1px solid white',
    };
    return { style };
  }

  increaseBrightness(hex, percent) {
    // strip the leading # if it's there
    hex = hex.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if (hex.length == 3) {
      hex = hex.replace(/(.)/g, '$1$1');
    }

    let r = parseInt(hex.substr(0, 2), 16),
      g = parseInt(hex.substr(2, 2), 16),
      b = parseInt(hex.substr(4, 2), 16);

    return '#' +
      ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
      ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
      ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
  }

  componentToHex(c) {
    const hex = c.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
  }

  onEventClick(event) {
    this.setState({ showModal: true, event: event });
  }

  toggleModal() {
    this.setState({ showModal: false });
  }

  onBlurEvent() {
    this.setState({
      showModal: false,
    });
  }

  render() {
    const {
      currentEquipment,
      currentRoom,
      currentStaff,
    } = this.props;

    const eventsToShow = [];
    let localAppponitments = this.state.appointments;
    if (currentStaff) {
      localAppponitments = localAppponitments.filter(a => {
        return a.staff && a.staff._id === this.props.currentStaff;
      });
    }

    if (currentEquipment) {
      localAppponitments = localAppponitments.filter(a => {
        return a.equipment === currentEquipment;
      });
    }

    if (currentRoom) {
      localAppponitments = localAppponitments.filter(a => {
        return a.room && a.room._id === currentRoom;
      });
    }

    localAppponitments && localAppponitments.forEach((a) => {
      eventsToShow.push({
        title: !a.prescription ? '' : a.prescription.patient.firstName + ' ' + a.prescription.patient.lastName,
        patientName: !a.prescription ? '' : a.prescription.patient.firstName + ' ' + a.prescription.patient.lastName,
        doctorName: !a.staff ? '' : a.staff.firstName + ' ' + a.staff.lastName,
        room: !a.room ? '' : a.room.name + ' ' + a.room.number,
        service: !a.service ? '' : a.service.name.shortName + '(' + a.service.name.defaultName + ')',
        start: new Date(a.date),
        end: new Date(new Date(a.date).getTime() + a.duration * 60 * 1000),
        hexColor: this.increaseBrightness(this.componentToHex(a.service ? a.service.categoryColor : ''), 50),
        color: '#334052',
      });
    });

    return (

      <TableWrapper className="calendar-page">
        <Helmet
          title="MedORG - Calendar"
          meta={[
            { name: 'description', content: 'Description of Calendar' },
          ]}
        />
        {
          this.state.showModal && this.state.event && <Modal
            startTime={this.state.event.start}
            endTime={this.state.event.end}
            patientName={this.state.event.patientName}
            doctorName={this.state.event.doctorName}
            service={this.state.event.service}
            room={this.state.event.room}
            handleClose={this.toggleModal}
            onBlurEvent={this.onBlurEvent}
          />
        }
        <div className="table-wrapper" style={{ height: window.innerHeight * 0.8 }}>
          <BigCalendar
            {...this.props}
            events={eventsToShow}
            step={10}
            timeslots={6}
            defaultView="week"
            defaultDate={new Date()}
            eventPropGetter={this.eventStyleGetter}
            selectable
            onSelectSlot={(slotInfo) => {/*console.log('slotInfo.slots');console.log(slotInfo.slots);*/
            }}
            onSelectEvent={(event) => this.onEventClick(event)}
          />
        </div>
      </TableWrapper>
    );
  }
}

CalendarPage.propTypes = {
  SelectedDayCalendar: PropTypes.object,
  appointments: PropTypes.array,
  users: PropTypes.array,
  currentEquipment: PropTypes.string,
  currentRoom: PropTypes.string,
  currentStaff: PropTypes.string,
  setSelectedDayCalendar: PropTypes.func,
  updateAppointment: PropTypes.func,
  fetchAppointments: PropTypes.func,
  loadAppointments: PropTypes.func,
  loadUsers: PropTypes.func,
  changeCurrentStaff: PropTypes.func,
  loadEquipments: PropTypes.func,
  loadRooms: PropTypes.func,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    users: state.CalendarPage.users,
    appointments: state.CalendarPage.appointments ? state.CalendarPage.appointments : [],
    SelectedDayCalendar: state.main.selectedDayCalendar ? state.main.selectedDayCalendar : new Date(),
    currentStaff: state.CalendarPage.currentStaff,
    currentEquipment: state.CalendarPage.currentEquipment,
    currentRoom: state.CalendarPage.currentRoom,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    setSelectedDayCalendar,
    updateAppointment,
    fetchAppointments,
    loadAppointments,
    loadUsers,
    changeCurrentStaff,
    loadEquipments,
    loadRooms,
    jspdf,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarPage);
