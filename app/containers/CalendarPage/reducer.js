/*
 *
 * CalendarPage reducer
 *
 */

import {
  FETCH_USERS_SUCCESS,
  FETCH_APPOINTMENTS_SUCCESS,
  CHANGE_CURRENT_STAFF,
  FETCH_EQUIPMENTS_SUCCESS,
  CHANGE_CURRENT_EQUIPMENT,
  CHANGE_CURRENT_ROOM,
  FETCH_ROOM_LIST_SUCCESS,
} from './constants';

const initialState = {
  users: [],
  rooms: [],
  appointments: [],
  equipments: [],
  currentStaff: null,
  currentEquipment: null,
  currentRoom: null,
};

function CalendarPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS_SUCCESS:
      return { ...state, users: action.data };
    case FETCH_APPOINTMENTS_SUCCESS:
      return { ...state, appointments: action.data };
    case CHANGE_CURRENT_STAFF:
      return { ...state, currentStaff: action.data, currentEquipment: null, currentRoom: null };
    case CHANGE_CURRENT_EQUIPMENT:
      return { ...state, currentEquipment: action.data, currentStaff: null, currentRoom: null };
    case CHANGE_CURRENT_ROOM:
      return { ...state, currentRoom: action.data, currentStaff: null, currentEquipment: null };
    case FETCH_EQUIPMENTS_SUCCESS:
      return { ...state, equipments: action.data };
    case FETCH_ROOM_LIST_SUCCESS:
      return { ...state, rooms: action.data };
    default:
      return state;
  }
}

export default CalendarPageReducer;
