import { put, call, take, cancel, takeLatest, fork } from 'redux-saga/effects';

import axios from 'axios';
import callApi from 'utils/callApi';

import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_APPOINTMENTS_REQUEST,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_EQUIPMENTS_REQUEST,
  FETCH_EQUIPMENTS_SUCCESS,
  FETCH_EQUIPMENTS_FAILURE,
  FETCH_ROOM_LIST_SUCCESS,
  FETCH_ROOM_LIST_FAILURE,
  FETCH_ROOM_LIST_REQUEST,
  FETCH_USERS_ERROR,
  FETCH_APPOINTMENTS_ERROR,
} from './constants';

const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';

export function loadUsersRequest() {
  return axios.get('/api/users');
}
export function loadAppointmentsRequest() {
  return axios.get('/api/appointments');
}

function* loadUsers() {
  try {
    const response = yield callApi(loadUsersRequest);

    const users = response.data.map((user) => {
      user.active = true;
      return user;
    });

    yield put({ type: FETCH_USERS_SUCCESS, data: users });
  } catch (error) {
    yield put({ type: FETCH_USERS_ERROR, error: error.message });
  }
}

export function* loadAppointments() {
  try {
    const response = yield callApi(axios.post, '/api/appointments/withPatient');
    yield put({ type: FETCH_APPOINTMENTS_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_APPOINTMENTS_ERROR, error: error.message });
  }
}

function* fetchEquipmentList() {
  try {
    const response = yield callApi(axios.get, '/api/equipment');
    yield put({ type: FETCH_EQUIPMENTS_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENTS_FAILURE, error: error.message });
  }
}

function* loadAppointmentsSage() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENTS_REQUEST, loadAppointments);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

function* loadUsersSage() {
  const watcher = yield fork(takeLatest, FETCH_USERS_REQUEST, loadUsers);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

function* fetchRoomList() {
  try {
    const response = yield callApi(axios.get, '/api/rooms');
    yield put({ type: FETCH_ROOM_LIST_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROOM_LIST_FAILURE, error: error.message });
  }
}

function* fetchEquipmentListSaga() {
  const watcher = yield fork(takeLatest, FETCH_EQUIPMENTS_REQUEST, fetchEquipmentList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchRoomListSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROOM_LIST_REQUEST, fetchRoomList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  loadAppointmentsSage,
  loadUsersSage,
  fetchEquipmentListSaga,
  fetchRoomListSaga,
];
