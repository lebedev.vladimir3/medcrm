import {
  SET_NEW_PATIENT_NAME,
  SET_SELECTED_DAY,
  CHANGE_LEFT_MENU,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_USERS_REQUEST,
  FETCH_APPOINTMENTS_REQUEST,
  FETCH_PATIENTS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_COMPANY_REQUEST,
  CHANGE_PATIENT_SEARCH_NAME,
  LOGOUT_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_EQUIPMENT_REQUEST,
  FETCH_ROOMS_REQUEST,
  SET_SELECTED_DAY_CALENDAR,
  UPDATE_OR_CREATE_COMPANY_REQUEST,
} from './constants';


export function setNewPatientName(data) {
  return { type: SET_NEW_PATIENT_NAME, data };
}

export function setSelectedDay(data) {
  return { type: SET_SELECTED_DAY, data };
}

export function setSelectedDayCalendar(data) {
  return { type: SET_SELECTED_DAY_CALENDAR, data };
}

export function setLeftMenu(data) {
  return { type: CHANGE_LEFT_MENU, data };
}

export function changePatientSearchName(data) {
  return { type: CHANGE_PATIENT_SEARCH_NAME, data };
}

export function loadUsers(data) {
  return { type: FETCH_USERS_REQUEST, data };
}

export function setUsers(data, changedStaff) {
  return { type: FETCH_USERS_SUCCESS, data, changedStaff: changedStaff };
}

export function loadAppointments(data) {
  return { type: FETCH_APPOINTMENTS_REQUEST, data };
}

export function loadCompany() {
  return { type: FETCH_COMPANY_REQUEST};
}

export function fetchAppointments(data) {
  return { type: FETCH_APPOINTMENTS_SUCCESS, data };
}


export function fetchPatients() {
  return { type: FETCH_PATIENTS_REQUEST };
}

export function loadEquipment() {
  return { type: FETCH_EQUIPMENT_REQUEST };
}
export function loadRooms() {
  return { type: FETCH_ROOMS_REQUEST };
}

export function logout() {
  return { type: LOGOUT_REQUEST };
}

export function fetchRoles() {
  return { type: FETCH_ROLE_LIST_REQUEST };
}

export const updateOrCreateCompany = data => ({ type: UPDATE_OR_CREATE_COMPANY_REQUEST, payload: data });