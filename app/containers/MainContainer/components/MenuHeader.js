import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';

import FaClose from 'react-icons/lib/md/close';
import Logo from './../../../components/Logo';

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 63px;
  padding-left: 24px;
  padding-right: 24px;
  background-color: #192431;
`;
const HeaderLeft = styled.div`
  display:flex;
  align-items: center;
`;

const TextHeader = styled.span`
  display: inline;
  height: 22px;
  color: #A8B1C7;
  
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
  text-align: center
  margin-left: 10px;
`;

const Close = styled(FaClose)`
  color: white;
  cursor: pointer;
`;

function MenuLeftCommon(props) {
  return (
    <Header>
      <HeaderLeft>
        <Logo color="#313D4F " backgroundColor="#A8B1C7" />
        <TextHeader>{props.title}</TextHeader>
      </HeaderLeft>
      {
        props.onMenuClick && <Close size={32} onClick={props.onMenuClick} />
      }

    </Header>
  );
}

MenuLeftCommon.propTypes = {
  title: PropTypes.string,
  onMenuClick: PropTypes.func,
};

export default MenuLeftCommon;
