import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import SearchSimple from 'react-icons/lib/fa/search';
import { Checkbox } from 'components/Input';

import 'icheck/skins/all.css'; // or single skin css
import { setUsers, changePatientSearchName } from './../actions';
import CustomCalendar from '../../../components/Calendar';
import MenuHeader from './MenuHeader';
import { changeCurrentStaff, changeCurrentEquipment, changeCurrentRoom } from '../../CalendarPage/actions';

import './menuLeftCommon.scss';
import messages from './../messages';

const LeftMenu = styled.div`
  min-height: 100%;
  width: 100%;
  background-color: #202C3C;
`;

const CheckboxFlexAligned = {
  display: 'flex',
  alignItems: 'center',
};


class MenuLeftCommon extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onAllStaff = this.onAllStaff.bind(this);
    this.onStaffClick = this.onStaffClick.bind(this);
    this.onPatientChange = this.onPatientChange.bind(this);
    this.changeCurrentStaff = this.changeCurrentStaff.bind(this);
    this.changeCurrentEquipment = this.changeCurrentEquipment.bind(this);
    this.changeCurrentRoom = this.changeCurrentRoom.bind(this);

    this.state = {
      toggleStaff: true,
      currentEquipment: null,
    };
  }

  componentWillUnmount() {
    this.props.changePatientSearchName('');
  }

  onAllStaff() {
    const currentActiveStaff = this.props.usersAgenda.map((st) => {
      const staff = st;
      staff.active = !this.state.toggleStaff;
      return staff;
    });
    this.setState({
      toggleStaff: !this.state.toggleStaff,
    });

    this.props.setUsers(currentActiveStaff, !this.props.changedStaff);
  }

  onPatientChange(e) {
    this.props.changePatientSearchName(e.target.value);
  }

  onStaffClick(id) {
    const currentActiveStaff = this.props.usersAgenda;
    currentActiveStaff[id].active = !currentActiveStaff[id].active;
    this.props.setUsers(currentActiveStaff, !this.props.changedStaff);
  }

  changeCurrentStaff(id) {
    this.props.changeCurrentStaff(id);
  }

  changeCurrentEquipment(id) {
    this.props.changeCurrentEquipment(id);
  }

  changeCurrentRoom(id) {
    this.props.changeCurrentRoom(id);
  }


  render() {
    const {
      users,
      equipments,
      rooms,
      usersAgenda,
    } = this.props;

    const staffCheckbox = usersAgenda && usersAgenda.map((user, i) => (
        <div className="all-staff-list--list__item" key={i}>
          <Checkbox
            id="checkbox1"
            wrapperStyles={CheckboxFlexAligned}
            checked={usersAgenda[i].active}
            onChange={() => this.onStaffClick(i)}
          />
          <div style={{ marginLeft: '15px' }}>
            {user.firstName}
          </div>
        </div>));

    const staffList = users.map((user, i) => (
      <div className="all-staff-list--list__item" key={i} onClick={() => this.changeCurrentStaff(user._id)}>
        {this.props.currentStaff === user._id && <div className="staff-main__active" />}
        <div style={{ color: this.props.currentStaff === user._id ? 'white' : '#A8B1C8' }}>
          {user.firstName}
        </div>
      </div>)
    );

    const equipmentsList = equipments && equipments.map((equipment, i) => (
        <div className="all-staff-list--list__item" key={i} onClick={() => this.changeCurrentEquipment(equipment._id)}>
          {this.props.currentEquipment === equipment._id && <div className="staff-main__active" />}
          <div style={{ color: this.props.currentEquipment === equipment._id ? 'white' : '#A8B1C8' }}>
            {equipment.shortName}
          </div>
        </div>)
      );

    const roomsList = rooms && rooms.map((room, i) => (
        <div className="all-staff-list--list__item" key={i} onClick={() => this.changeCurrentRoom(room._id)}>
          {this.props.currentRoom === room._id && <div className="staff-main__active" />}
          <div style={{ color: this.props.currentRoom === room._id ? 'white' : '#A8B1C8' }}>
            {room.name}
          </div>
        </div>)
      );

    return (
      <LeftMenu>
        <Helmet
          title="StaffPage"
          meta={[{ name: 'description', content: 'Description of StaffPage' }]}
        />
        <MenuHeader title={this.props.company && this.props.company.title} />
        <CustomCalendar
          AgendaOrCalendar={this.props.AgendaOrCalendar}
          dateSelected={this.props.calendarSelect}
          selectedDay={this.props.AgendaOrCalendar === 'agenda'
                          ? this.props.selectedDay : this.props.selectedDayCalendar}
        />
        {this.props.AgendaOrCalendar === 'agenda' && <div className="menu-label">
          <Checkbox
            id="checkbox1"
            wrapperStyles={CheckboxFlexAligned}
            checkboxClass="icheckbox_square-blue"
            increaseArea="20%"
            onChange={this.onAllStaff}
            checked={this.state.toggleStaff}
          />
          <div style={{ marginLeft: '15px' }}>{this.context.intl.formatMessage(messages.AllStaff)}</div>
        </div>
        }
        {this.props.AgendaOrCalendar === 'agenda' && [
          <div key={0} className="all-staff-list">
            <div className="all-staff-list--list">
              {staffCheckbox}
            </div>
          </div>,
          <div key={1} className="menu-label">
            {this.context.intl.formatMessage(messages.Patients)}
          </div>,
          <div key={2} className="patient-search">
            <div className="patient-search__wrapper">
              <input
                type="text"
                className="patient-search--input"
                placeholder={this.context.intl.formatMessage(messages.PatientName)}
                onChange={this.onPatientChange}
              />
              <SearchSimple className="patient-search--icon"></SearchSimple>
            </div>
          </div>,
        ]}
        {this.props.AgendaOrCalendar === 'calendar' && [
          <div key={0} className="menu-label">
            <div>{this.context.intl.formatMessage(messages.Staff)}</div>
          </div>,
          <div key={1} className="all-staff-list">
            <div className="all-staff-list--list">
              {staffList}
            </div>
          </div>,
          <div key={2} className="menu-label">
            <div>{this.context.intl.formatMessage(messages.Equipment)}</div>
          </div>,
          <div key={3} className="all-staff-list">
            <div className="all-staff-list--list">
              {equipmentsList}
            </div>
          </div>,
          <div key={4} className="menu-label">
            <div>{this.context.intl.formatMessage(messages.Rooms)}</div>
          </div>,
          <div key={5} className="all-staff-list">
            <div className="all-staff-list--list">
              {roomsList}
            </div>
          </div>,
        ]}
      </LeftMenu>);
  }
}

MenuLeftCommon.propTypes = {
  users: PropTypes.array.isRequired,
  calendarSelect: PropTypes.func.isRequired,
  changePatientSearchName: PropTypes.func.isRequired,
  setUsers: PropTypes.func.isRequired,
  changedStaff: PropTypes.bool.isRequired,
  changeCurrentStaff: PropTypes.func.isRequired,
  changeCurrentEquipment: PropTypes.func.isRequired,
  changeCurrentRoom: PropTypes.func.isRequired,
  usersAgenda: PropTypes.array,
  equipments: PropTypes.array,
  rooms: PropTypes.array,
  currentStaff: PropTypes.string,
  currentEquipment: PropTypes.string,
  currentRoom: PropTypes.string,
  AgendaOrCalendar: PropTypes.string,
};
MenuLeftCommon.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    users: state.CalendarPage && state.CalendarPage.users ? state.CalendarPage.users : [],
    rooms: state.CalendarPage && state.CalendarPage.rooms ? state.CalendarPage.rooms : [],
    usersAgenda: state.SchedulePage && state.SchedulePage.users ? state.SchedulePage.users : [],
    currentStaff: state.CalendarPage && state.CalendarPage.currentStaff ? state.CalendarPage.currentStaff : '',
    currentEquipment: state.CalendarPage && state.CalendarPage.currentEquipment ? state.CalendarPage.currentEquipment : '',
    currentRoom: state.CalendarPage && state.CalendarPage.currentRoom ? state.CalendarPage.currentRoom : '',
    equipments: state.CalendarPage && state.CalendarPage.equipments ? state.CalendarPage.equipments : [],
    appointments: state.CalendarPage && state.CalendarPage.appointments ? state.main.appointments : [],
    selectedDay: state.main.selectedDay ? state.main.selectedDay : new Date(),
    selectedDayCalendar: state.main.selectedDayCalendar ? state.main.selectedDayCalendar : new Date(),
    changedStaff: state.main.changedStaff,
    company: state.main.company,
  };
}

const mapDispatchToProps = {
  setUsers,
  changePatientSearchName,
  changeCurrentStaff,
  changeCurrentEquipment,
  changeCurrentRoom,
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuLeftCommon);
