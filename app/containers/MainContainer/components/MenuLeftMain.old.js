import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import FaGlobe from 'react-icons/lib/md/language';
import Mdpeople from 'react-icons/lib/md/people';
import Patients from 'react-icons/lib/md/assignment-ind';
import Note from 'react-icons/lib/md/note-add';
import MenuIcon from 'react-icons/lib/fa/th-large';
import Gear from 'react-icons/lib/go/gear';
import FaClose from 'react-icons/lib/md/close';
import messages from './../messages';

import './menuLeftMain.old.scss';

const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(19,24,32,0.3);
  display: flex;
  z-index: 9999;
`;
const LeftMenu = styled.div`
  position: relative;
  min-height: 100%;
  width: 366px;
  position: absolute;
  background-color: #202C3C;
`;
const Menu = styled.div`
  //padding-left: 24px;
  //padding-top: 17px;
`;
const Close = styled(FaClose)`
  position: absolute;
  right: 20px;
  color: white;
  cursor: pointer;
`;

class MenuLeftMain extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);

  }

  componentDidMount() {
    this.leftmenu.focus();
  }

  handleClick(name) {
    this.setState({
      selectedTab: name,
    });
    switch (name) {
      case this.context.intl.formatMessage(messages.settings0):
        this.props.push('/main/profile');
        break;
      case this.context.intl.formatMessage(messages.settings1):
        this.props.push('/main/opening-hours');
        break;
      case this.context.intl.formatMessage(messages.settings2):
        this.props.push('/main/staff');
        break;
      case this.context.intl.formatMessage(messages.settings3):
        this.props.push('/main/med-services');
        break;
      case this.context.intl.formatMessage(messages.settings4):
        this.props.push('/main/resources');
        break;
      case this.context.intl.formatMessage(messages.PatientsDirectory):
        this.props.push('/main/patients');
        break;
      default:
    }
  }

  render() {
    const icons = [
      <FaGlobe key={0} className="menu-label-main--icon" />,
      <FaGlobe key={1} className="menu-label-main--icon" />,
      <Mdpeople key={2} className="menu-label-main--icon" />,
      <Note key={3} className="menu-label-main--icon" />,
      <Note key={4} className="menu-label-main--icon" />,
      // <Cube key={5} className="menu-label-main--icon" />,
    ];

    const listItems = [];
    const route = this.props.location.pathname.split('/')[2];
    let currentLabel;
    switch (route) {
      case 'profile':
        currentLabel = this.context.intl.formatMessage(messages['settings0']);
        break;
      case 'opening-hours':
        currentLabel = this.context.intl.formatMessage(messages['settings1']);
        break;
      case 'staff':
        currentLabel = this.context.intl.formatMessage(messages['settings2']);
        break;
      case 'med-services':
        currentLabel = this.context.intl.formatMessage(messages['settings3']);
        break;
      case 'resources':
        currentLabel = this.context.intl.formatMessage(messages['settings4']);
        break;
      case 'patients':
        currentLabel = this.context.intl.formatMessage(messages['PatientsDirectory']);
        break;
    }

    for (let i = 0; i < 5; i++) {
      listItems.push(
        <div
          key={i}
          className={currentLabel === this.context.intl.formatMessage(messages['settings' + i]) ?
            'menu-label-main menu-label-main--active' :
            'menu-label-main'}
          onClick={() => {
            this.handleClick(this.context.intl.formatMessage(messages['settings' + i]));
            this.props.onMenuClick();
          }}
        >
          {currentLabel === this.context.intl.formatMessage(messages['settings' + i]) &&
          <div className="menu-label-main__active" />}
          {icons[i]}
          {this.context.intl.formatMessage(messages['settings' + i])}
        </div>
      );
    }

    return (
      <Background>
        <LeftMenu tabIndex="-1" onBlur={this.props.onBlurLeftMenu} autoFocus innerRef={(node) => { this.leftmenu = node; }}>
          <Menu>
            <div className="menu-label-main not-clickable" style={{ color: '#00896D' }}>
              <MenuIcon className="menu-label-main--icon" />
              <span>{this.context.intl.formatMessage(messages.MainMenu)}</span>

            </div>
            <div
              className={currentLabel === this.context.intl.formatMessage(messages['PatientsDirectory']) ?
                'menu-label-main menu-label-main--active' :
                'menu-label-main'}
              onClick={() => {
                this.handleClick(this.context.intl.formatMessage(messages.PatientsDirectory));
                this.props.onMenuClick();
              }}
            >
              {currentLabel === this.context.intl.formatMessage(messages['PatientsDirectory'])}
              <Patients className="menu-label-main--icon" />
              <span>{this.context.intl.formatMessage(messages.PatientsDirectory)}</span>
            </div>
            <div className="menu-label-main not-clickable" style={{ color: '#3E7DBC' }}>
              <Gear className="menu-label-main--icon" />
              {this.context.intl.formatMessage(messages.Setup)}
            </div>
            {listItems}
            <div className="br-line"></div>
          </Menu>

        </LeftMenu>
      </Background>);
  }
}

MenuLeftMain.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
MenuLeftMain.propTypes = {
  onMenuClick: PropTypes.func,
  selectedTab: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    company: state.main.company,
  };
}
function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuLeftMain);
