import fontSetting from 'components/styles/font';
//@flow
import * as React from 'react';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';

type Props = {
  label: string,
  icon?: React.Node,
};

const Wrap = styled.div`
  display: flex;
  align-items: center;
`;

const IconWrap = styled.span`
  margin-right: 14px;
`;

const Label = styled.span`
  font-weight: 600;
  font-size: ${fontSetting.baseSizePlus9}px;
  font-family: ${fontSetting.logoFontFamily};
  color: ${theme.Drawer.Items.text.active}
`;

class MenuHeader extends React.Component<Props> {
  render(){
    const { label, icon, ...props } = this.props;
    return (
      <Wrap>
        {/*<Label>{label}</Label>*/}
      </Wrap>
    );
  }
}

MenuHeader.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MenuHeader;
