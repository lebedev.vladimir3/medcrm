//@flow
import fontSetting from 'components/styles/font';
import * as React from 'react';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';
import { Link } from 'react-router';

type Props = {
  color: string,
  label: string,
  icon: React.Element < * >,
  active: boolean,
  divider: boolean,
  onClick: () => mixed,
};

export const Wrap = styled.div`
  height: 40px;
  padding: 2px 0px 2px ${props => props.active ? '19px' : '24px'};
  margin: 11px 0px;
  ${props => props.active && ('border-left: 5px solid ' + props.color + ';')}
  display: flex;
  align-items: center;
  cursor: ${props => props.onClick ? 'pointer' : 'default'};
  position: relative;
`;

const Icon = styled.span`
  //width: 25px;
  //height: 25px;
  color: ${props => props.color};
  margin-right: 11px;
`;

const Label = styled.span`
  line-height: 16px;
  font-size: ${fontSetting.baseSize}px;
  //font-weight: 600;
  color: ${props => props.active ? theme.Drawer.Items.text.active : theme.Drawer.Items.text.normal}
`;

const Divider = styled.div`
  width: 270px;
  margin: 16px 24px 12px 24px;
  border-bottom: 1px solid ${theme.Drawer.Items.border.divider};
`;

class MenuLeftMainItem extends React.Component<Props> {
  static defaultProps = {
    color: theme.Drawer.Items.border.active.main
  };

  render() {
    const { divider, label, color, icon, active, index, ...props } = this.props;
    return divider ? <Divider key={index} /> : (
      <Wrap color={color} active={active} {...props} key={index}>
        <Link to={this.props.link} style={{position: 'absolute', height: '100%', width: '100%'}}>
        </Link>
        <Icon color={color}>
            {React.cloneElement(icon, {
              style: {
                width: '25px',
                height: '25px',
              }
            })}
          </Icon>
          <Label>{label}</Label>
      </Wrap>
    );
  }
}

MenuLeftMainItem.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MenuLeftMainItem;
