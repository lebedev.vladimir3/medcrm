//@flow
import * as React from 'react';
import Select from 'react-select';
import styled from 'styled-components';
import theme from 'components/styles/colorScheme';
import fontSetting from 'components/styles/font';
import { Wrap } from './Item';

import HomeIcon from 'assets/icons/MainMenu/Light.svg';

const Option = styled.div`
  display: flex;
  align-items: center;
  > svg {
    //width: 25px;
    //height: 25px;
    margin-left: 2px;
    margin-right: 14px;
    margin-top: -1px;
  }
  > span {
    font-weight: 600;
  }
`;

const SelectWrap = styled(Select)`
  width: 270px;
  .Select-value {
    padding-left: 0px !important;
    font-size: ${fontSetting.baseSizePlus1}px;
  }
  .Select-value-label {
    color: ${theme.Drawer.Select.color} !important;
  }
  
  .Select-control {
    height: 32px;
    background-color: ${theme.Drawer.Select.background};
    border: none;
    border-bottom: 1px solid ${theme.Drawer.Select.border};
    border-radius: 0px;
  }
`;

const ComponentWrap = styled(Wrap)`
  .Select.is-disabled > .Select-control {
    background-color: ${theme.Drawer.Select.background};
  }
  .is-focused:not(.is-open) > .Select-control {
    border-color: ${theme.Drawer.Select.border};
    box-shadow: none;
  }
`;

const Renderer = (option) => <Option>
  <HomeIcon /> <span>{option.label}</span>
</Option>;

type Props = {
  label: string,
  icon?: React.Node,
  options: [{
    value: string,
    label: string
  }]
};

class SelectLocation extends React.Component<Props> {
  render(){
    const { label, icon, options, ...props } = this.props;
    return (
      <ComponentWrap>
        <SelectWrap
          name="form-field-name"
          options={options}
          clearable={false}
          searchable={false}
          //optionRenderer={Renderer}
          valueRenderer={Renderer}
          arrowRenderer={({ onMouseDown, isOpen }) => null}
          disabled
          {...props}
        />
      </ComponentWrap>
    );
  }
}

SelectLocation.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default SelectLocation;
