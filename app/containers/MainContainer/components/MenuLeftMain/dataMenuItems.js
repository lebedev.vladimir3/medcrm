import React from 'react';

import LocationProfileIcon from 'assets/icons/MainMenu/LocationProfile.svg';
import OpeningHoursIcon from 'assets/icons/MainMenu/OpeningHours.svg';
import StaffIcon from 'assets/icons/MainMenu/StaffDirectory.svg';
import MedServiceIcon from 'assets/icons/MainMenu/Med.Services.svg';
import ResourcesIcon from 'assets/icons/MainMenu/Resources.svg';
import SettingsIcon from 'assets/icons/MainMenu/Settings.svg';
import PatientsIcon from 'assets/icons/MainMenu/Patients.svg';

import FaGlobe from 'react-icons/lib/md/language';
import Mdpeople from 'react-icons/lib/md/people';
import Note from 'react-icons/lib/md/note-add';
import messages from './../../messages';

import theme from 'components/styles/colorScheme';

export default [
  {
    route: 'profile',
    icon: <LocationProfileIcon />,
    messageKey: 'settings0'
  },
  {
    route: 'opening-hours',
    icon: <OpeningHoursIcon />,
    messageKey: 'settings1'
  },
  {
    route: 'staff',
    icon: <StaffIcon />,
    messageKey: 'settings2'
  },
  {
    route: 'med-services',
    icon: <MedServiceIcon />,
    messageKey: 'settings3'
  },
  {
    route: 'resources',
    icon: <ResourcesIcon />,
    messageKey: 'settings4'
  },
  {
    route: 'settings',
    icon: <SettingsIcon />,
    messageKey: 'settings5'
  },
  { divider: true },
  {
    route: 'patients',
    icon: <PatientsIcon />,
    messageKey: 'PatientsDirectory',
    color: theme.Drawer.Items.border.active.accent
  },
];
