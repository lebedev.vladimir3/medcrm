//@flow
import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Row } from 'components/styles/Grid';
import { ContentWrap } from 'components/styles/Containers';
import theme from 'components/styles/colorScheme';
import messages from './../../messages';

import FaClose from 'assets/icons/Close.svg';


import data from './dataMenuItems';


import MenuHeader from './Header';
import MenuItem from './Item';
import SelectLocation from './SelectLocation';

const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: ${theme.Drawer.blackout};
  display: flex;
  z-index: 9999;
`;

const LeftMenu = styled.div`
  min-height: 100%;
  width: 366px;
  position: absolute;
  background-color: ${theme.Drawer.background};
`;

const CloseIcon = styled(FaClose)`
  //color: white;
  cursor: pointer;
`;

type Props = {
  onBlurLeftMenu: () => mixed,
  onMenuClick: () => mixed,
  push: () => mixed,
  location: {
    pathname: string
  },
  company: {
    title: string
  }
};

class MenuLeftMain extends React.Component<Props> {
  menuClickHandler(e){
    e.stopPropagation();
  }
  itemClickHandler(item){
    const { push, onMenuClick } = this.props;
    onMenuClick();
  }
  render(){
    const { formatMessage } = this.context.intl;
    const { onMenuClick, location, push, company } = this.props;

    const route = location.pathname.split('/')[2];
    return (
      <Background onClick={this.props.onBlurLeftMenu}>
        <LeftMenu onClick={this.menuClickHandler}>
          <ContentWrap padding="18px 18px 0px 18px">
            <Row justifyContent="space-between">
              <div/>
              <div>
                <CloseIcon
                  size={32}
                  onClick={onMenuClick}
                />
              </div>
            </Row>
          </ContentWrap>
          <SelectLocation
            options={[{
              value: company.title, label: company.title
            }]}
            value={company.title}
          />
          {data.map((item, index) => <MenuItem
            label={!item.divider && formatMessage(messages[item.messageKey])}
            icon={item.icon}
            active={route === item.route}
            divider={item.divider}
            color={item.color}
            index={index}
            onClick={item.route && this.itemClickHandler.bind(this, item)}
            link={'/main/'+item.route}
          />)}
        </LeftMenu>
      </Background>
    );
  }
}

MenuLeftMain.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({ company: state.main.company });

export default connect(mapStateToProps, {})(MenuLeftMain);
