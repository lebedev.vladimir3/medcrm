import React, { PropTypes } from 'react';
import fontSetting from 'components/styles/font';
import SearchPlus from 'assets/icons/Search.svg';
import Female from 'react-icons/lib/fa/venus';
import Male from 'react-icons/lib/fa/mars';
import FaPlus from 'react-icons/lib/fa/plus';
import messages from './../messages';

import './searchstyle.scss';
import ReactDOM from 'react-dom';


export default class SearchPatient extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onInputChange = this.onInputChange.bind(this);
    this.onPatientClick = this.onPatientClick.bind(this);
    this.onNewPatientClick = this.onNewPatientClick.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);
    this.onFocusEvent = this.onFocusEvent.bind(this);
    this.onKeyDownInput = this.onKeyDownInput.bind(this);

    this.state = {
      patients: props.patients,
      input: '',
      disabled: true,
      isCurrentSearchFocused: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.patients !== this.state.patients) {
      this.setState({
        patients: nextProps.patients,
      });
    }
  }

  onInputChange(event) {
    let patients = this.sortPatients(this.state.patients, event.target.value);

    event.target.value !== ' ' && this.setState({
      input: event.target.value,
      disabled: !(event.target.value.length > 1),
      patients: patients,
    });
  }

  onPatientClick(patientId) {
    this.setState(() => {
      this.props.push('/main/patients/' + patientId);
      return {
        input: '',
        disabled: true,
      };
    });
  }

  onNewPatientClick() {
    this.setState({
      input: '',
      disabled: true,
    });
    this.props.setNewPatientName(this.state.input);
    this.props.push('/main/patients/new');
  }

  onBlurEvent() {
    this.setState({
      disabled: true,
    });
  }

  onFocusEvent() {
    this.setState({
      disabled: this.state.input.length <= 0,
    });
  }

  sortPatients(array, string) {
    const patients = array;
    patients.sort((patient1, patient2) => {
      const date1 = patient1.birth.dd + '.' + patient1.birth.mm + '.' + patient1.birth.yyyy;
      const date2 = patient2.birth.dd + '.' + patient2.birth.mm + '.' + patient2.birth.yyyy;
      if (typeof patient1.phone === 'undefined' || typeof patient2.phone === 'undefined') {
        if (patient1.firstName.indexOf(string) !== 0 &&
          patient2.firstName.indexOf(string) === 0 ||
          patient1.lastName.indexOf(string) !== 0 &&
          patient2.lastName.indexOf(string) === 0 ||
          date1.indexOf(string) !== 0 &&
          date2.indexOf(string) === 0) {
          return 1;
        } else if (patient1.firstName.indexOf(string) === 0 &&
          patient2.firstName.indexOf(string) !== 0 ||
          patient1.lastName.indexOf(string) === 0 &&
          patient2.lastName.indexOf(string) !== 0 ||
          date1.indexOf(string) === 0 &&
          date2.indexOf(string) !== 0) {
          return -1;
        }
      } else if (patient1.firstName.indexOf(string) !== 0 &&
        patient2.firstName.indexOf(string) === 0 ||
        patient1.lastName.indexOf(string) !== 0 &&
        patient2.lastName.indexOf(string) === 0 ||
        patient1.phone && patient1.phone.main && patient1.phone.main.indexOf(string) !== 0 &&
        patient2.phone && patient2.phone.main && patient2.phone.main.indexOf(string) === 0 ||
        date1.indexOf(string) !== 0 &&
        date2.indexOf(string) === 0) {
        return 1;
      } else if (patient1.firstName.indexOf(string) === 0 &&
        patient2.firstName.indexOf(string) !== 0 ||
        patient1.lastName.indexOf(string) === 0 &&
        patient2.lastName.indexOf(string) !== 0 ||
        patient1.phone && patient1.phone.main && patient1.phone.main.indexOf(string) === 0 &&
        patient2.phone && patient2.phone.main && patient2.phone.main.indexOf(string) !== 0 ||
        date1.indexOf(string) === 0 &&
        date2.indexOf(string) !== 0) {
        return -1;
      }
    });
    return patients;
  }

  renderThick(text, substring) {
    if (typeof text === 'undefined') {
      return '';
    }

    return text.indexOf(substring) === 0 ?
      <span>
        <span className="selected-text">{substring}</span>
        {text.substring(substring.length, text.length)}
      </span> :
      text;
  }

  onKeyDownInput(e) {
    if (e.keyCode === 13) {
      this.onNewPatientClick();
    }
  }


  render() {
    const patients = this.state.patients;
    const patientsLength = patients.length;
    const input = this.state.input.toLowerCase();

    const options = [
      <div className="dropdown--item" key={-12} onMouseDown={this.onNewPatientClick}>
        <FaPlus style={{ fontSize: fontSetting.baseSize, marginRight: '15px' }} />
        <span>{'add "'}</span>
        <div className="selected-text">{this.state.input}</div>
        <span>{'” as a new patient'}</span>
      </div>,
    ];

    for (let i = 0; (i < patientsLength) && options.length < 4; i++) {
      const dateToShow = patients[i].birth.dd + '.' + patients[i].birth.mm + '.' + patients[i].birth.yyyy;

      if (typeof patients[i].phone === 'undefined') {
        if (patients[i].firstName.toLowerCase().indexOf(input) === 0 ||
          patients[i].lastName.toLowerCase().indexOf(input) === 0 ||
          dateToShow.indexOf(input) === 0) {
          options.push(
            <div className="dropdown--item" key={i} onMouseDown={() => this.onPatientClick(patients[i]._id)}>
              {patients[i].title && patients[i].title.personal === 'mr' ?
                <Male style={{ fontSize: fontSetting.baseSize, marginRight: '15px' }} /> :
                <Female style={{ fontSize: fontSetting.baseSize, marginRight: '15px' }} />}
              {this.renderThick(patients[i].firstName, input)}{'\xa0'}{this.renderThick(patients[i].lastName, input)}{'\xa0'}
              /{'\xa0'}{this.renderThick(patients[i].birth.dd + '.' + patients[i].birth.mm + '.' + patients[i].birth.yyyy, input)}
            </div>
          );
        }
      } else if (patients[i].firstName.toLowerCase().indexOf(input) === 0 ||
        patients[i].lastName.toLowerCase().indexOf(input) === 0 ||
        patients[i].phone && patients[i].phone.main && patients[i].phone.main.indexOf(input) === 0 ||
        dateToShow.indexOf(input) === 0) {
        options.push(
          <div className="dropdown--item" key={i} onMouseDown={() => this.onPatientClick(patients[i]._id)}>
            {patients[i].title && patients[i].title.personal === 'mr' ?
              <Male style={{ fontSize: fontSetting.baseSize, marginRight: '15px' }} /> :
              <Female style={{ fontSize: fontSetting.baseSize, marginRight: '15px' }} />}
            {this.renderThick(patients[i].firstName, input)}{'\xa0'}{this.renderThick(patients[i].lastName, input)}
            {this.renderThick(patients[i].phone ? `\xa0/\xa0` + patients[i].phone.main : '', input)}{'\xa0'}
            /{'\xa0'}{this.renderThick(patients[i].birth.dd + '.' + patients[i].birth.mm + '.' + patients[i].birth.yyyy, input)}
          </div>
        );
      }
    }

    return (
      <div className="searchBoxWrapper">
        <input
          className="searchBoxWrapper--input " type="text"
          placeholder={this.context.intl.formatMessage(messages.SearchPlaceholder)}
          onChange={this.onInputChange}
          value={this.state.input} onFocus={this.onFocusEvent} onBlur={this.onBlurEvent}
          ref={(input) => { this.searchBox = input; }}
          onKeyDown={this.onKeyDownInput}
        />
        <div className={this.state.disabled ? 'dropdown-hide' : 'dropdown'}>
          {options}
        </div>
        <SearchPlus className="searchBoxWrapper--search-icon" />
      </div>
    );
  }
}

SearchPatient.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

SearchPatient.propTypes = {
  patients: PropTypes.array.isRequired,
  setNewPatientName: PropTypes.func.isRequired,
};
