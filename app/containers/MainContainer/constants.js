export const REQUEST_ERROR = 'app/MainContainer/REQUEST_FAILURE';
export const SET_NEW_PATIENT_NAME = 'app/MainContainer/SET_NEW_PATIENT_NAME';
export const SET_SELECTED_DAY = 'app/MainContainer/SET_SELECTED_DAY';
export const SET_SELECTED_DAY_CALENDAR = 'app/MainContainer/SET_SELECTED_DAY_CALENDAR';
export const CHANGE_LEFT_MENU = 'app/MainContainer/CHANGE_LEFT_MENU';
export const ADD_NEW_PATIENT = 'app/MainContainer/ADD_NEW_PATIENT';
export const DELETE_PATIENT = 'app/MainContainer/DELETE_PATIENT';
export const UPDATE_PATIENT = 'app/MainContainer/UPDATE_PATIENT';

export const CHANGE_PATIENT_SEARCH_NAME = 'app/MainContainer/CHANGE_PATIENT_SEARCH_NAME';

export const FETCH_COMPANY = 'app/MainContainer/FETCH_COMPANY';

export const FETCH_COMPANY_REQUEST = 'app/MainContainer/FETCH_COMPANY_REQUEST';
export const FETCH_COMPANY_SUCCESS = 'app/MainContainer/FETCH_COMPANY_SUCCESS';
export const FETCH_COMPANY_ERROR = 'app/MainContainer/FETCH_COMPANY_FAILURE';
export const UPDATE_OR_CREATE_COMPANY_REQUEST = 'app/MainContainer/UPDATE_OR_CREATE_COMPANY_REQUEST';

export const FETCH_USERS_REQUEST = 'app/MainContainer/FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'app/MainContainer/FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'app/MainContainer/FETCH_USERS_FAILURE';

export const FETCH_APPOINTMENTS_REQUEST = 'app/MainContainer/FETCH_APPOINTMENTS_REQUEST';
export const FETCH_APPOINTMENTS_SUCCESS = 'app/MainContainer/FETCH_APPOINTMENTS_SUCCESS';
export const FETCH_APPOINTMENTS_ERROR = 'app/MainContainer/FETCH_APPOINTMENTS_FAILURE';

export const LOGOUT_REQUEST = 'app/AuthPage/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'app/AuthPage/LOGOUT_SUCCESS';

export const FETCH_ROLE_LIST_REQUEST = 'app/MainContainer/FETCH_ROLE_LIST_REQUEST';
export const FETCH_ROLE_LIST_SUCCESS = 'app/MainContainer/FETCH_ROLE_LIST_SUCCESS';
export const FETCH_ROLE_LIST_ERROR = 'app/MainContainer/FETCH_ROLE_LIST_FAILURE';

export const FETCH_EQUIPMENT_REQUEST = 'app/MainContainer/FETCH_EQUIPMENT_REQUEST';
export const FETCH_EQUIPMENT_SUCCESS = 'app/MainContainer/FETCH_EQUIPMENT_SUCCESS';
export const FETCH_EQUIPMENT_ERROR = 'app/MainContainer/FETCH_EQUIPMENT_FAILURE';


export const FETCH_ROOMS_REQUEST = 'app/MainContainer/FETCH_ROOMS_REQUEST';
export const FETCH_ROOMS_SUCCESS = 'app/MainContainer/FETCH_ROOMS_SUCCESS';
export const FETCH_ROOMS_ERROR = 'app/MainContainer/FETCH_ROOMS_FAILURE';

export const ADD_NEW_USER_TO_STATE = 'app/MainContainer/ADD_NEW_USER';
export const DELETE_USER_FROM_STATE = 'app/MainContainer/DELETE_USER';
export const UPDATE_USER_IN_STATE = 'app/MainContainer/UPDATE_USER';

export const FETCH_PATIENTS_REQUEST = 'app/MainContainer/FETCH_PATIENTS_REQUEST';
export const FETCH_PATIENTS_SUCCESS = 'app/MainContainer/FETCH_PATIENTS_SUCCESS';
export const FETCH_PATIENTS_ERROR = 'app/MainContainer/FETCH_PATIENTS_FAILURE';
