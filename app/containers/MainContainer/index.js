import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import AngleDown from 'react-icons/lib/fa/angle-down';
import ArrowLeft from 'react-icons/lib/fa/angle-double-left';
import ArrowRight from 'react-icons/lib/fa/angle-double-right';
import IconMenu from 'assets/icons/Menu.svg';
import QuestionCircle from 'react-icons/lib/fa/question-circle';
import SignOut from 'react-icons/lib/fa/sign-out';
import { push } from 'react-router-redux';
import { FormattedMessage } from 'react-intl';
import SearchPatient from './components/SearchPatient';
import MenuLeftCommon from './components/MenuLeftCommon';
import MenuLeftMain from './components/MenuLeftMain';
import messages from './messages';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import Dropdown from 'components/common/Dropdown';

const TestStyled = styled.div`
  width: 350px;
  position: absolute;
  right: 30px;
  top: 53px;
  background: white;
  height: 20px;
`;

import {
  setLeftMenu,
  setNewPatientName,
  setSelectedDay,
  loadUsers,
  loadAppointments,
  fetchPatients,
  logout,
  fetchRoles,
  loadEquipment,
  loadRooms,
  loadCompany,
} from './actions';
import { changeLocale } from './../LanguageProvider/actions';
import './style.scss';

const FullPage = styled.div`
  display:flex;
  flex-direction: row;
  min-height: 100%;
  width: 100%;
`;

const CalendarWRapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  align-items: center;
  margin-right: 10px;
`;

const ActionBar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 36px;
  width: 60px;
  cursor: pointer;
  margin-left: 8%;
`;

const User = styled.div`
  height: 36px;
  width: 36px;
  border: 1px solid #E6E6E6;
  color: #E6E6E6;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right:5px;
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
`;


export class MainContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props, context) {
    super(props, context);
    let selectedTab = '';
    let showToggleArrow = false;
    if (window.location.pathname.indexOf('/main/staff') !== -1) {
      selectedTab = 'Staff';
    }
    if (window.location.pathname.indexOf('/main/profile') !== -1) {
      selectedTab = 'MedCRM Profile';
    }
    if (window.location.pathname.indexOf('main/agenda') !== -1) {
      showToggleArrow = true;
    }
    this.changeAgenda = this.changeAgenda.bind(this);
    this.changeCalendar = this.changeCalendar.bind(this);
    this.calendarSelect = this.calendarSelect.bind(this);
    this.toggleLeftMenu = this.toggleLeftMenu.bind(this);
    this.onMenuClick = this.onMenuClick.bind(this);
    this.toggleUserMenu = this.toggleUserMenu.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);
    this.signout = this.signout.bind(this);
    this.onBlurLeftMenu = this.onBlurLeftMenu.bind(this);

    this.state = {
      selectedTab,
      currentAgenda: -1,
      selectedDay: new Date(),
      hideLeftMenu: true,
      AgendaOrCalendar: 'agenda',
      hideUserMenu: true,
      showMainMenu: false,
      isSearchBoxFocused: false,
      showToggleArrow,
    };
  }

  componentDidMount() {
    this.props.loadUsers();
    this.props.loadAppointments();
    this.props.fetchPatients();
    this.props.fetchRoles();
    this.props.loadCompany();
  }

  componentWillReceiveProps() {
    this.setState({
      showToggleArrow: window.location.pathname.indexOf('main/agenda') !== -1 ||
      window.location.pathname.indexOf('main/calendar') !== -1,
      currentAgenda: window.location.pathname.indexOf('main/agenda') !== -1 ? 1 :
        window.location.pathname.indexOf('main/calendar') !== -1 ? 0 : -1,
      AgendaOrCalendar: window.location.pathname.indexOf('main/agenda') !== -1 ? 'agenda'
        : 'calendar',
    });
  }

  calendarSelect(date) {
    this.props.setSelectedDay(date);
  }


  changeAgenda() {
    this.setState({
      currentAgenda: 1,
    });
  }

  changeCalendar() {
    this.setState({
      currentAgenda: 0,
    });
  }

  onMenuClick() {
    this.setState({
      showMainMenu: !this.state.showMainMenu,
      hideLeftMenu: true,
    });
  }

  toggleLeftMenu() {
    this.setState({
      hideLeftMenu: !this.state.hideLeftMenu,
    });
  }

  toggleUserMenu() {
    this.setState({
      hideUserMenu: !this.state.hideUserMenu,
    });
  }

  onBlurEvent() {
    this.setState({
      hideUserMenu: true,
    });
  }

  onBlurLeftMenu() {
    this.setState({
      showMainMenu: false,
    });
  }

  signout() {
    this.props.logout();
  }

  render() {
    return (this.props.user) ? (
      <FullPage>

        <Helmet
          title="StaffPage"
          meta={[

            { name: 'description', content: 'Description of MainPage' },
          ]}
        />
        {
          this.state.showMainMenu &&
          <MenuLeftMain
            users={this.props.users}
            selectedTab={this.state.selectedTab}
            onMenuClick={this.onMenuClick}
            onBlurLeftMenu={this.onBlurLeftMenu}
            push={this.props.push}
            location={this.props.location}
          />
        }
        <div className={this.state.hideLeftMenu ? 'left-menu-wrapper__hide' : 'left-menu-wrapper'}>
          {
            <MenuLeftCommon
              users={this.props.users}
              calendarSelect={this.calendarSelect}
              AgendaOrCalendar={this.state.AgendaOrCalendar}
            />
          }
        </div>
        { this.state.showToggleArrow &&
        <div className={this.state.hideLeftMenu ? 'hide-left-menu-icon__full' : 'hide-left-menu-icon'} onClick={this.toggleLeftMenu}>
          {this.state.hideLeftMenu ? <ArrowRight className="toggle-arrow-left" />
            : <ArrowLeft className="toggle-arrow-left" />
          }
        </div>
        }
        <div className={this.state.hideLeftMenu ? 'RightMenu__full' : 'RightMenu'}>
          <div className="top-panel">
            <CalendarWRapper>
              <div className="">
                <IconMenu
                  style={{ cursor: 'pointer' }}
                  onClick={this.onMenuClick}
                  //size={36}
                />
              </div>
              <Link
                to="/main/calendar"
                style={{ marginLeft: '20px' }}
                className={this.state.currentAgenda === 0 ? 'top-panel--agenda activeAgenda' : 'top-panel--agenda'}
                onClick={this.changeCalendar}
              >
                <FormattedMessage {...messages.calendar} />
              </Link>
              <Link to="/main/agenda">
                <div
                  style={{ marginLeft: '20px' }}
                  className={this.state.currentAgenda === 1 ? 'top-panel--agenda activeAgenda' : 'top-panel--agenda'}
                  onClick={this.changeAgenda}
                >
                  <FormattedMessage {...messages.agenda} />
                </div>
              </Link>
            </CalendarWRapper>
            <SearchPatient
              patients={this.props.patients}
              push={this.props.push}
              setNewPatientName={this.props.setNewPatientName}
              isFocused={this.state.isSearchBoxFocused}
              onBlurEvent={this.onBlurEvent}
            />
            <Dropdown
              onClick={this.toggleUserMenu}
              onBlur={this.onBlurEvent}
              show={!this.state.hideUserMenu}
              //menuComponent={(props) => <TestStyled/>}
              optionStyle={{
                paddingLeft: '16px'
              }}
              options={[
                <div onClick={() => this.props.push(`/main/staff/${this.props.user._id}/working-hours`)}>
                  <QuestionCircle className="top-panel--user-menu--icon" />
                  {messages.MySchedule.defaultMessage}
                </div>,
                <div onClick={() => this.signout()}>
                  <SignOut className="top-panel--user-menu--icon" />
                  {messages.SignOut.defaultMessage}
                </div>
              ]}
            >
              <ActionBar
                tabIndex="-1">
                <User>LB</User>
                <AngleDown
                  style={{
                    display: 'block',
                    color: '#F4F4F4',
                    cursor: 'pointer',
                  }}
                />
              </ActionBar>
            </Dropdown>
            {/*<ReactCSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={250}
              transitionLeaveTimeout={250}
            >

              {!this.state.hideUserMenu ? (<div key="1" className="top-panel--user-menu">
                <div
                  className="top-panel--user-menu--item"
                  onMouseDown={() => this.props.push(`/main/staff/${this.props.user._id}/working-hours`)}
                >
                  <QuestionCircle className="top-panel--user-menu--icon" />
                  {messages.MySchedule.defaultMessage}
                </div>
                <div className="top-panel--user-menu--item" onMouseDown={() => this.signout()}>
                  <SignOut className="top-panel--user-menu--icon" />
                  {messages.SignOut.defaultMessage}
                </div>
              </div>): null}
            </ReactCSSTransitionGroup>*/}
          </div>
          {this.props.children}
        </div>
      </FullPage>
    ) : null;
  }
}

MainContainer.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

MainContainer.propTypes = {
  children: PropTypes.object,
  user: PropTypes.object,
  users: PropTypes.array,
  patients: PropTypes.array,
  logout: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  loadUsers: PropTypes.func.isRequired,
  loadAppointments: PropTypes.func.isRequired,
  fetchPatients: PropTypes.func.isRequired,
  fetchRoles: PropTypes.func.isRequired,
  setSelectedDay: PropTypes.func.isRequired,
  setNewPatientName: PropTypes.func.isRequired,
  appReady: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    patients: state.main.patients ? state.main.patients : [],
    users: state.schedule ? state.schedule.users : [],
    leftMenu: state.main.leftMenu ? state.main.leftMenu : 'main',
    appReady: state.App.appReady,
    user: state.App.user,
  };
}

const mapDispatchToProps = {
  setNewPatientName,
  changeLocale,
  setSelectedDay,
  push,
  setLeftMenu,
  loadUsers,
  loadAppointments,
  fetchPatients,
  logout,
  fetchRoles,
  loadEquipment,
  loadRooms,
  loadCompany,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
