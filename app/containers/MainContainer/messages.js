/*
 * FeaturePage Messages
 *
 * This contains all the text for the FeaturePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  Setup: {
    id: 'MainContainer.mainsettings.0',
    defaultMessage: 'Setup',
  },
  Dashboard: {
    id: 'MainContainer.Dashboard',
    defaultMessage: 'Dashboard',
  },
  MainMenu: {
    id: 'MainContainer.MainMenu',
    defaultMessage: 'Main Menu',
  },
  settings0: {
    id: 'MainContainer.mainsettings.LocationProfile',
    defaultMessage: 'Location Profile',
  },
  settings1: {
    id: 'MainContainer.mainsettings.OpeningHours',
    defaultMessage: 'Opening Hours',
  },
  settings2: {
    id: 'MainContainer.mainsettings.StaffDirectory',
    defaultMessage: 'Staff Directory',
  },
  settings3: {
    id: 'MainContainer.mainsettings.MedicalServices',
    defaultMessage: 'Medical Services',
  },
  settings4: {
    id: 'MainContainer.mainsettings.Resources',
    defaultMessage: 'Resources',
  },
  settings5: {
    id: 'MainContainer.mainsettings.Settings',
    defaultMessage: 'Settings',
  },
  Patients: {
    id: 'MainContainer.mainsettings.Patients',
    defaultMessage: 'Patients',
  },
  PatientsDirectory: {
    id: 'MainContainer.mainsettings.PatientsDirectory',
    defaultMessage: 'Patients Directory',
  },
  calendar: {
    id: 'MainContainer.header.calendar',
    defaultMessage: 'Calendar',
  },
  SearchPlaceholder: {
    id: 'MainContainer.SearchPlaceholder',
    defaultMessage: 'Find or add a patient',
  },
  MyProfile: {
    id: 'MainContainer.MyProfile',
    defaultMessage: 'My Profile',
  },
  Help: {
    id: 'MainContainer.Help',
    defaultMessage: 'Help',
  },
  MySchedule: {
    id: 'MySchedule',
    defaultMessage: 'My Schedule',
  },
  SignOut: {
    id: 'MainContainer.SignOut',
    defaultMessage: 'Sign Out',
  },
  agenda: {
    id: 'MainContainer.header.agenda',
    defaultMessage: 'Agenda',
  },
  generalSchedule: {
    id: 'MainContainer.settings.generalSchedule',
    defaultMessage: 'General Schedule',
  },
  nonMedical: {
    id: 'MainContainer.settings.nonMedical',
    defaultMessage: 'Non-Medical Services',
  },
  PatientName: {
    id: 'MainContainer.settings.PatientName',
    defaultMessage: 'Patient’s name',
  },
  AllStaff: {
    id: 'AllStaff',
    defaultMessage: 'All staff',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  Equipment: {
    id: 'Equipment',
    defaultMessage: 'Equipment',
  },
  Rooms: {
    id: 'Rooms',
    defaultMessage: 'Rooms',
  },
  Close: {
    id: 'Close',
    defaultMessage: 'Close',
  },
  ProductName: {
    id: 'ProductName',
    defaultMessage: 'MedORG'
  }
});
