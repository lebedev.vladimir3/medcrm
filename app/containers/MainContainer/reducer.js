import {
  FETCH_USERS_SUCCESS,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_PATIENTS_SUCCESS,
  SET_NEW_PATIENT_NAME,
  SET_SELECTED_DAY,
  CHANGE_LEFT_MENU,
  REQUEST_ERROR,
  FETCH_COMPANY_SUCCESS,
  ADD_NEW_PATIENT,
  DELETE_PATIENT,
  UPDATE_PATIENT,
  CHANGE_PATIENT_SEARCH_NAME,
  LOGOUT_SUCCESS,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_EQUIPMENT_SUCCESS,
  FETCH_ROOMS_SUCCESS,
  ADD_NEW_USER_TO_STATE,
  DELETE_USER_FROM_STATE,
  UPDATE_USER_IN_STATE,
  SET_SELECTED_DAY_CALENDAR,
} from './constants';

const initialState = {
  users: [],
  roleList: [],
  selectedDay: new Date(),
  selectedDayCalendar: new Date(),
  changedStaff: false,
  changedStaffCalendar: false,
  patientSearchName: '',
  currentStaff: null,
  company: {},
};

function MainContainerReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        users: action.data,
        changedStaff: typeof action.changedStaff !== 'undefined' ? action.changedStaff : state.changedStaff,
      };
    case FETCH_APPOINTMENTS_SUCCESS:
      return { ...state, appointments: action.data };
    case FETCH_PATIENTS_SUCCESS:
      return { ...state, patients: action.payload };
    case SET_NEW_PATIENT_NAME:
      return { ...state, newPatientName: action.data };
    case SET_SELECTED_DAY:
      return { ...state, selectedDay: action.data };
    case SET_SELECTED_DAY_CALENDAR:
      return { ...state, selectedDayCalendar: action.data };
    case CHANGE_LEFT_MENU:
      return { ...state, leftMenu: action.data };
    case FETCH_COMPANY_SUCCESS:
      return { ...state, company: action.data };
    case ADD_NEW_PATIENT:
      return { ...state, patients: [...state.patients, action.data] };
    case DELETE_PATIENT:
      return { ...state, patients: action.data };
    case UPDATE_PATIENT:
      return { ...state, patients: action.data };
    case REQUEST_ERROR:
      return { ...state, error: action.data };
    case CHANGE_PATIENT_SEARCH_NAME:
      return { ...state, patientSearchName: action.data };
    case LOGOUT_SUCCESS:
      return initialState;
    case FETCH_ROLE_LIST_SUCCESS:
      return { ...state, roleList: action.roles };
    case FETCH_EQUIPMENT_SUCCESS:
      return { ...state, equipments: action.equipments };
    case FETCH_ROOMS_SUCCESS:
      return { ...state, rooms: action.rooms };
    case ADD_NEW_USER_TO_STATE:
      return { ...state, users: [...state.users, action.data] };
    case DELETE_USER_FROM_STATE:
      return { ...state, users: action.data };
    case UPDATE_USER_IN_STATE:
      return { ...state, users: action.data };
    default:
      return state;
  }
}

export default MainContainerReducer;
