import { put, fork, take, cancel, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';
const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';

import {
  REQUEST_ERROR,
  FETCH_APPOINTMENTS_REQUEST,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_APPOINTMENTS_ERROR,
  FETCH_PATIENTS_REQUEST,
  FETCH_PATIENTS_SUCCESS,
  FETCH_COMPANY_REQUEST,
  FETCH_COMPANY_SUCCESS,
  FETCH_COMPANY_ERROR,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
  LOGOUT_SUCCESS,
  LOGOUT_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_ERROR,
  FETCH_EQUIPMENT_REQUEST,
  FETCH_EQUIPMENT_SUCCESS,
  FETCH_EQUIPMENT_ERROR,
  FETCH_ROOMS_REQUEST,
  FETCH_ROOMS_SUCCESS,
  FETCH_ROOMS_ERROR,
  FETCH_PATIENTS_ERROR,
  UPDATE_OR_CREATE_COMPANY_REQUEST,
} from './constants';

function loadUsersRequest() {
  return axios.get('/api/users');
}
function loadAppointmentsRequest() {
  return axios.get('/api/appointments');
}
function loadCompanyRequest() {
  return axios.get('/api/company');
}

function* loadUsers() {
  try {
    const response = yield callApi(loadUsersRequest);
    const users = response.data.map((user) => {
      user.active = true;
      return user;
    });

    yield put({ type: FETCH_USERS_SUCCESS, data: users });
  } catch (error) {
    yield put({ type: FETCH_USERS_ERROR, error: error.message });
  }
}

function* loadAppointments() {
  try {
    const response = yield callApi(loadAppointmentsRequest);
    yield put({ type: FETCH_APPOINTMENTS_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_APPOINTMENTS_ERROR, error: error.message });
  }
}

function* loadCompany() {
  try {
    const response = yield callApi(loadCompanyRequest);
    yield put({ type: FETCH_COMPANY_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_COMPANY_ERROR, error: error.message });
  }
}

const logoutRequest = () => axios.post('/api/auth/logout');
function* logout() {
  try {
    yield callApi(logoutRequest);
    yield put({ type: LOGOUT_SUCCESS });
    browserHistory.push('/auth');

    window.localStorage.removeItem('medcrm');
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

export function* fetchRoleList() {
  try {
    const response = yield callApi(axios.get, '/api/roles/');
    yield put({ type: FETCH_ROLE_LIST_SUCCESS, roles: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_ERROR, error: error.message });
  }
}

export function* fetchEquipment() {
  try {
    const response = yield callApi(axios.get, '/api/equipment');
    yield put({ type: FETCH_EQUIPMENT_SUCCESS, equipments: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENT_ERROR, error: error.message });
  }
}

export function* fetchRooms() {
  try {
    const response = yield callApi(axios.get, '/api/rooms');
    yield put({ type: FETCH_ROOMS_SUCCESS, rooms: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROOMS_ERROR, error: error.message });
  }
}

export function* fetchPatients() {
  try {
    const response = yield callApi(axios.get, '/api/patients');
    yield put({ type: FETCH_PATIENTS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PATIENTS_ERROR, payload: response.data });

    console.error(error.message);
  }
}

function* fetchUsersListSaga() {
  const watcher = yield fork(takeLatest, FETCH_USERS_REQUEST, loadUsers);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchAppointmentsListSaga() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENTS_REQUEST, loadAppointments);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchCompanySaga() {
  const watcher = yield fork(takeLatest, FETCH_COMPANY_REQUEST, loadCompany);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* logoutSaga() {
  const watcher = yield fork(takeLatest, LOGOUT_REQUEST, logout);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchRoleSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROLE_LIST_REQUEST, fetchRoleList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchEquipmentSaga() {
  const watcher = yield fork(takeLatest, FETCH_EQUIPMENT_REQUEST, fetchEquipment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchRoomsSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROOMS_REQUEST, fetchRooms);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}


function* fetchPatientsSaga() {
  const watcher = yield fork(takeLatest, FETCH_PATIENTS_REQUEST, fetchPatients);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* updateOrCreateCompany(action) {
  try {
    const response = yield callApi(axios.post, '/api/company', action.payload);
    yield put({ type: FETCH_COMPANY_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_COMPANY_ERROR, error: error.message });
  }
}

export function* profileSaga() {
  const updateOrCreate = yield takeLatest(UPDATE_OR_CREATE_COMPANY_REQUEST, updateOrCreateCompany);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(updateOrCreate);
}

// All sagas to be loaded
export default [
  fetchUsersListSaga,
  fetchAppointmentsListSaga,
  logoutSaga,
  fetchRoleSaga,
  fetchEquipmentSaga,
  fetchRoomsSaga,
  fetchPatientsSaga,
  fetchCompanySaga,
  profileSaga,
];
