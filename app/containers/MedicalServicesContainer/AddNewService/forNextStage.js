/*
 *
 * не удаляйте файл, он потом понадобится.
 *
 */

import React, { PropTypes } from 'react';
import { CardFooter } from 'components/Card';
import styled from 'styled-components';
import { GreyButton, GreenButton } from 'components/common/Button';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector, FieldArray } from 'redux-form';
import PlusCircle from 'react-icons/lib/fa/plus-circle';
import MoreButtom from 'react-icons/lib/md/remove-circle';
import JoinStaff from 'react-icons/lib/md/repeat';
import Grabber from 'react-icons/lib/fa/hand-grab-o';
import Trash from 'react-icons/lib/fa/trash-o';
import { ColorInput } from 'components/Input';
import { Checkbox } from 'react-icheck';


import '../../../components/ServiceForm/style.scss';
import { Input, Select } from '../components/Input';
import HeaderPanel from '../components/HeaderPanel';
import { createMedservice } from '../actions';


const TableWrapper = styled.div`
    padding: 30px;
    width: 800px;
    margin: auto;
`;

const FormGroup = styled.div`
  display: flex;
  margin-bottom: 15px;
  padding-left: 29px;
  padding-right: 29px;
  @media (max-width: 900px) {
    flex-direction: column;
    margin-bottom: 0;
  }
`;

const PulledRight = styled.div`
  margin-left: auto;
`;


const renderSelectField = renderProps => {
  const { input } = renderProps;
  return (
    <Select {...input} {...renderProps}>
      {renderProps.placeholder && !renderProps.value &&
      <option value="" disabled hidden >{ renderProps.placeholder }</option>}
      {renderProps.children}
    </Select>
  );
};

const renderInputField = renderProps => {
  const { input } = renderProps;
  return <Input {...input} {...renderProps} />;
};


const renderColorInputField = (props) => {
  const { value, onChange } = props.input;
  return <ColorInput color={value} onChange={value => onChange(value)} />;
};


class MediclServicesContainer extends React.Component {
  constructor(props, context) {
    super(props, context);


    this.toggleColor = this.toggleColor.bind(this);
    this.deletePayer = this.deletePayer.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.getRoleList = this.getRoleList.bind(this);
    this.onStaffChange = this.onStaffChange.bind(this);
    this.toggleUserMenu = this.toggleUserMenu.bind(this);
    this.onBlurEvent = this.onBlurEvent.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onJoinStaff = this.onJoinStaff.bind(this);
    this.onInsuranceChange = this.onInsuranceChange.bind(this);
    this.selectStaffForJoin = this.selectStaffForJoin.bind(this);
    this.onDoneJoin = this.onDoneJoin.bind(this);

    this.state = {
      hideColor: true,
      color: 'white',
      staff: this.props.staff ? this.props.staff : [],
      allInsuranceChosen: false,
      selectStaffForJoin: [],
      connectedJoin: [],
      staffForJoin: {},
    };
    this.state[`hideUserMenu${0}`] = false;
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.staff !== nextProps.staff) {
      this.setState({
        staff: nextProps.staff,
      });
    }
  }

  onBlurEvent(index) {
    this.props.change(`staffAllocation[${index}].toggle`, false);
  }

  onDoneJoin() {
    let currentConnectedJoin = this.state.connectedJoin;
    let selectStaffForJoin = this.state.selectStaffForJoin;
    let staffForJoin = this.state.staffForJoin;

    console.log('currentConnectedJoin');
    console.log(currentConnectedJoin);
    console.log(selectStaffForJoin);

    let step = currentConnectedJoin.map((staffs) => {
      let array = staffs.filter(staff => {
        let ret = selectStaffForJoin.every(staffId => {
          console.log('onDoneJoin');

          let localStaff = JSON.parse(JSON.stringify(staff));
          localStaff.toggle = true;
          let localstaffId = JSON.parse(JSON.stringify(staffId));
          localstaffId.toggle = true;
          console.log(localStaff);
          console.log(localstaffId);
          console.log(JSON.stringify(localStaff) !== JSON.stringify(localstaffId));
          return JSON.stringify(localStaff) !== JSON.stringify(localstaffId);
        });
        return ret;
      });
      console.log('array');
      console.log(array);
      return array;
      /*
       return staffs.every(staff => {
       let ret = selectStaffForJoin.every(staffId => {
       console.log('onDoneJoin');

       let localStaff = JSON.parse(JSON.stringify(staff));
       localStaff.toggle = true;
       let localstaffId = JSON.parse(JSON.stringify(staffId));
       localstaffId.toggle = true;
       console.log(localStaff);
       console.log(localstaffId);
       console.log(JSON.stringify(localStaff) !== JSON.stringify(localstaffId));
       return JSON.stringify(localStaff) !== JSON.stringify(localstaffId);
       });
       return ret;
       });*/
    });
    step = step.filter(array => array.length > 0);
    console.log('index');
    console.log(step);
    console.log(currentConnectedJoin.indexOf(step));

    step.push(selectStaffForJoin);
    console.log(step);
    this.props.change('showJoin', false);
    let toShow = [];
    this.props.array.removeAll('staffAllocation');

    step.forEach((array) => {
      array.forEach((item) => {
        item.length = 1;
        if (array.indexOf(item) === 0) {
          item.length = array.length;
        } else if (array.length > 1) {
          item.length = -1;
        }
        this.props.array.push('staffAllocation', item);

        toShow.push(item);
      });

    });
    console.log('toShow');
    console.log(toShow);
    // this.props.change('staffAllocation', []);
    // this.props.change('staffAllocation', toShow);
    this.setState({
      connectedJoin: step,
      selectStaffForJoin: [],
    });

  }

  onSubmit(formData) {
    console.log('formData');
    console.log(formData);
    formData.company = this.props.company._id;
    formData.connectedJoin = this.state.connectedJoin;
    // this.props.createMedservice(formData);
  }

  onCategoryChange(e) {
    const filter = this.props.categories.filter(category => category._id === e.target.value);
    const color = filter[0].color;
    this.props.change('categoryColor', color);
  }

  getRoleList() {
    return this.props.roles.map((role, index) =>
      <option value={role.name} key={index} >{role.name}</option>
    );
  }

  getCategoryList() {
    return this.props.categories.map((role, index) =>
      <option value={role._id} key={index} >{role.name}</option>
    );
  }

  toggleUserMenu(index) {

    let toggle = true;
    if (this.props.selector(this.props.currState, `staffAllocation[${index}].toggle`) === 'undefined') {
      toggle = true;
    } else {
      toggle = !this.props.selector(this.props.currState, `staffAllocation[${index}].toggle`);
    }

    this.props.change(`staffAllocation[${index}].toggle`, toggle);
  }


  selectStaffForJoin(index) {
    let staff = this.state.selectStaffForJoin;
    if (staff.indexOf(index) === -1) {
      staff.push(index);
    } else {
      staff.splice(staff.indexOf(index), 1);
    }

    console.log('staff');
    console.log(staff);
    this.setState({
      selectStaffForJoin: staff,
    });
  }

  toggleColor() {
    this.setState({
      hideColor: !this.state.hideColor,
    });
  }

  deletePayer() {
  }

  onJoinStaff(staff) {
    let connectedJoin = this.state.connectedJoin;
    console.log('this.props.staffAllocation');
    console.log(this.props.staffAllocation);
    console.log(staff);

    if (connectedJoin.length === 0) {
      connectedJoin = this.props.staffAllocation.map((staff)=>[staff]);
    }
    console.log(connectedJoin);

    this.props.change('showJoin', true);
    this.setState({
      staffForJoin: staff,
      connectedJoin: connectedJoin,
    });
  }

  onInsuranceChange(e) {
    if (e.target.value === 'ALL') {
      this.setState({
        allInsuranceChosen: true,
      });
    }
  }

  onStaffChange(e, index) {
    if (e.target.value === 'any staff of this role') {
      this.props.change(`staffAllocation[${index}].isAnyStaff`, true);
      let staffList = this.props.staff.filter((staff) =>
      staff.role.name === this.props.staffAllocation[index].role);

      staffList = staffList.map((staff) => staff._id);
      this.props.change(`staffAllocation[${index}].staffIds`, staffList);
    } else {
      this.props.change(`staffAllocation[${index}].isAnyStaff`, false);
      this.props.change(`staffAllocation[${index}].staffIds`, [e.target.value]);
    }
  }


  payersFields = ({ fields }) => {

    const payerDisabled = (typeof this.props.payers) === 'undefined';
    let addDisable = true;

    if (!payerDisabled) {
      addDisable = (typeof this.props.payers[fields.length - 1].duration) === 'undefined' ||
        (typeof this.props.payers[fields.length - 1].price) === 'undefined';
    }
    return (<div>
      {
        fields.map((field, index) => {
          let durationDisable = true;
          let deleteDisable = true;
          if (!payerDisabled) {
            durationDisable = (typeof this.props.payers[index].payer) === 'undefined';
            deleteDisable = (typeof this.props.payers[index].duration) === 'undefined' || (typeof this.props.payers[index].price) === 'undefined';
          }
          return (<div key={index} className="table-top--row" style={{ marginBottom: '20px' }} >
            <div className="table-top--row__col1" style={{ visibility: index !== 0 ? 'hidden' : 'visible' }} >
              Payers, Duration, Price
            </div>
            <div className="table-top--row__col2" >
              <div className="table-top--row__col2--left" >
                <Field name={`${field}.payer`} component={renderSelectField} placeholder="Payer"
                       onChange={this.onInsuranceChange}
                       required >
                  <option value="ALL" >All Insurances</option>
                  <option value="1" >1 - Government Insurance</option>
                  <option value="3" >3 - Government Insurance</option>
                  <option value="5" >5 - Government Insurance</option>
                  <option value="7" >7 - Goverment Insurance</option>
                  <option value="P" >P - Private Insurance</option>
                </Field>
              </div>
              <div
                className="table-top--row__col2--left"
                style={{ marginLeft: '10px', justifyContent: 'space-between', width: '55%' }}
              >
                <div style={{ width: '47%' }} >
                  <Field
                    name={`${field}.duration`}
                    component={renderInputField}
                    placeholder="Duration"
                    required
                    disabled={durationDisable}
                  />
                </div>
                <div style={{ width: '47%' }} >
                  <Field
                    name={`${field}.price`}
                    component={renderInputField}
                    placeholder="Price"
                    required
                    disabled={durationDisable}
                  />
                </div>
              </div>
            </div>

            <button
              type="button"
              className="table-top--row__col3"
              style={{ marginLeft: '20px' }}
              disabled={deleteDisable}
              onClick={() => { fields.remove(index); }}
            >
              <Trash size="22" color="#D46659" style={{ opacity: deleteDisable ? '0.5' : '1' }} />
            </button>
          </div>);
        })
      }
      <div className="table-top--row" >
        <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
        <div className="table-top--row__col4" style={{ marginLeft: '5px' }} >
          <button
            type="button"
            className="add-payer"
            onClick={() => { this.state.allInsuranceChosen ? fields.unshift({}) : fields.push({}); }}
            disabled={addDisable}
            style={{ opacity: addDisable ? '0.5' : '1' }}
          >
            <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
            Add Payer...
          </button>
        </div>
      </div>
    </div>);
  };

  staffAllocationFields = ({ fields }) => {

    const staffAllocationDisabled = (typeof this.props.staffAllocation) === 'undefined';

    let addDisable = true;

    if (!staffAllocationDisabled) {
      addDisable = (typeof this.props.staffAllocation[fields.length - 1].role) === 'undefined' ||
        (typeof this.props.staffAllocation[fields.length - 1].staffIds) === 'undefined';
    }
    let staffList = [];

    return (<div>
      {
        fields.map((field, index) => {
          let staffDisable = true,
            hideMenu = true,
            showHorBranch = this.props.staffAllocation[`${index}`].length > 1 ||
              this.props.staffAllocation[`${index}`].length === -1;

          const branchHeight = this.props.staffAllocation[`${index}`].length > 1 ?
            ((34 + 17) * (this.props.staffAllocation[`${index}`].length - 1)) : 0;

          const top = this.props.staffAllocation[`${index}`].length > 1 ? 17 : 0;

          if (typeof this.props.staffAllocation[`${index}`].toggle === 'undefined') {
            hideMenu = false;
          } else {
            hideMenu = this.props.staffAllocation[`${index}`].toggle;
          }

          if (!staffAllocationDisabled) {
            staffDisable = (typeof this.props.staffAllocation[index].role) === 'undefined';
            if ((typeof this.props.staffAllocation[index].role) !== 'undefined') {
              staffList = this.props.staff.filter((staff) =>
              staff.role.name === this.props.staffAllocation[index].role);

              staffList = staffList.map((staff, index) =>
                <option value={staff._id} key={index} >
                  {staff.lastName + ' ' + staff.firstName}
                </option>
              );

              if (staffList.length > 0) {
                staffList.unshift(<option value={'any staff of this role'} key={staffList.length} >
                  {'any staff of this role'}
                </option>);
              }
            }
          }
          return (<div key={index} className="table-top--row" >
            <div className="table-top--row__col1" style={{ visibility : index!==0 ? 'hidden' : 'visible' }} >
              Staff allocation

            </div>

            <div className="table-top--row__col2" >
              {
                !this.props.showJoin && <div className="join-rows" style={{
              height: branchHeight,
              top:top,
              }} >
                </div>
              }
              {
                !this.props.showJoin &&
                <div className="hor-branch" style={{
              visibility: showHorBranch ? 'visible':'hidden'
              }} />
              }
              {
                this.props.showJoin && <div className="table-top--row__checkbox" >
                  <Checkbox
                    id="checkbox1"
                    checkboxClass="icheckbox_minimal"
                    increaseArea="20%"
                    onChange={this.selectStaffForJoin.bind(null, this.props.staffAllocation[index])}
                  />
                </div>
              }
              <div className="table-top--row__col2--left" >
                <Field name={`${field}.role`} component={renderSelectField} placeholder="Role" required
                >
                  {this.getRoleList()}
                </Field>
              </div>
              <div className="table-top--row__col2--left" style={{ marginLeft: '10px', width: '55%' }} >
                <Field name={`${field}.staffff`} component={renderSelectField} placeholder="Staff" required
                       disabled={staffDisable} onChange={(e) => this.onStaffChange(e, index)} >
                  {staffList}
                </Field>

              </div>
            </div>

            {fields.length <= 1
              ?
              < button type="button" className="table-top--row__col3"
                       style={{ marginLeft: '20px', opacity: staffDisable ? '0.5' : '1' }} disabled={staffDisable}
                       onClick={() => {fields.remove(index) }} >
                <Trash size="22" color="#D46659" />
              </button>
              :
              <button
                type="button"
                className="table-top--row__col3"
                onClick={() => this.toggleUserMenu(index)}
                onBlur={() => this.onBlurEvent(index)}
              >
                <MoreButtom size="22" color="#78909C" />
              </button>
            }
            { hideMenu && <div className="top-panel--staff-menu" >
              <div className="top-panel--staff-menu--item"
                   onMouseDown={this.onJoinStaff.bind(null, this.props.staffAllocation[index])} >
                <JoinStaff size="22" color="#4687D6" className="top-panel--staff-menu--icon" />
                Appoint Alternative Staff
              </div>
              <div className="top-panel--staff-menu--item" >
                <Grabber size="22" color="#7C909C" className="top-panel--staff-menu--icon" />
                Move/Change Priority
              </div>
              <div className="top-panel--staff-menu--item" onMouseDown={() => this.signout()} >
                <Trash size="22" color="#D46659" className="top-panel--staff-menu--icon" />
                Delete
              </div>
            </div>
            }

          </div>);
        })
      }
      {
        !this.props.showJoin ? <div className="table-top--row" >
          <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
          <div className="table-top--row__col4" style={{ marginLeft: '5px' }} >
            <button
              type="button"
              className="add-payer"
              onClick={() => {fields.push({})}}
              disabled={addDisable}
              style={{ opacity: addDisable ? '0.5' : '1' }}
            >
              <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
              Add Staff…
            </button>
          </div>
        </div>
          :
          <div className="table-top--row" >
            <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
            <div className="table-top--row__col2" style={{ marginLeft: '5px', justifyContent: 'flex-end' }} >
              <button type="button" className="done-button" onClick={this.onDoneJoin} >
                Done
              </button>
            </div>
          </div>
      }


    </div>);
  };

  equipmentFields = ({ fields }) => {
    const equipmentAllocationDisabled = (typeof this.props.equipmentAllocation) === 'undefined';
    let addDisable = true;

    if (!equipmentAllocationDisabled) {
      addDisable = (typeof this.props.equipmentAllocation[fields.length - 1].equipmentId) === 'undefined';
    }

    let equipmentList = this.props.equipments.map((equipment, index) =>
      <option value={equipment._id} key={index} >
        {equipment.name}
      </option>
    );

    return (<div>
      {
        fields.map((field, index) => {

          let deleteDisable = true;
          if (!equipmentAllocationDisabled) {
            deleteDisable = (typeof this.props.equipmentAllocation[index].equipmentId) === 'undefined';
          }
          return (<div key={index} className="table-top--row" style={{ marginBottom: '20px' }} >
            <div className="table-top--row__col1" style={{ visibility: index !== 0 ? 'hidden' : 'visible' }} >Equipment
              allocation
            </div>
            <div className="table-top--row__col2" >
              <div className="table-top--row__col2--left" >
                <Field name={`${field}.equipmentId`} component={renderSelectField} placeholder="Equipment" required >
                  {equipmentList}
                </Field>
              </div>
              <button
                type="button"
                className="table-top--row__col3"
                style={{ marginLeft: '20px' }}
                disabled={deleteDisable}
                onClick={() => { fields.remove(index); }}
              >
                <Trash size="22" color="#D46659" style={{ opacity: deleteDisable ? '0.5' : '1' }} />
              </button>
            </div>


          </div>);
        })
      }
      <div className="table-top--row" >
        <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
        <div className="table-top--row__col4" style={{ marginLeft: '5px' }} >
          <button
            type="button"
            className="add-payer"
            onClick={() => { fields.push({}) }}
            disabled={addDisable}
            style={{ opacity: addDisable ? '0.5' : '1' }}
          >
            <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
            Add Equipment…
          </button>
        </div>
      </div>
    </div>);
  };

  roomFields = ({ fields }) => {

    const roomAllocationDisabled = (typeof this.props.roomAllocation) === 'undefined';
    let addDisable = true;

    if (!roomAllocationDisabled) {
      addDisable = (typeof this.props.roomAllocation[fields.length - 1].roomId) === 'undefined';
    }

    const roomList = this.props.rooms.map((room, index) =>
      <option value={room._id} key={index} >
        {room.number}
      </option>
    );

    return (<div>
      {
        fields.map((field, index) => {
          let deleteDisable = true;
          if (!roomAllocationDisabled) {
            deleteDisable = (typeof this.props.roomAllocation[index].roomId) === 'undefined';
          }
          return (<div key={index} className="table-top--row" style={{ marginBottom: '20px' }} >
            <div className="table-top--row__col1" style={{ visibility: index !== 0 ? 'hidden' : 'visible' }} >Room
              allocation
            </div>
            <div className="table-top--row__col2" >
              <div className="table-top--row__col2--left" >
                <Field name={`${field}.roomId`} component={renderSelectField} placeholder="Room" required >
                  {roomList}
                </Field>
              </div>
              <button
                type="button"
                className="table-top--row__col3"
                style={{ marginLeft: '20px' }}
                disabled={deleteDisable}
                onClick={() => {fields.remove(index)}}
              >
                <Trash size="22" color="#D46659" style={{ opacity: deleteDisable ? '0.5' : '1' }} />
              </button>
            </div>


          </div>);
        })
      }
      <div className="table-top--row" >
        <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
        <div className="table-top--row__col4" style={{ marginLeft: '5px' }} >
          <button
            type="button"
            className="add-payer"
            onClick={() => { fields.push({}); }}
            disabled={addDisable}
            style={{ opacity: addDisable ? '0.5' : '1' }}
          >
            <PlusCircle size="22" color="#4687D6" style={{ marginRight: '10px' }} />
            Add Room…
          </button>
        </div>
      </div>
    </div>);
  };

  render() {
    const maxLength = max => value =>
      value && value.length > max ? `Must be ${max} characters or less` : undefined;
    const maxDefLength = maxLength(30);
    const maxLongLength = maxLength(255);
    const maxShortLength = maxLength(10);

    console.log('this.state.connectedJoin');
    console.log(this.state.connectedJoin);
    console.log(this.props.staffAllocation);
    console.log(this.props);

    return (
      <div>
        <HeaderPanel selectedTab="Add New Service" showStaffButtons dispatch={this.props.dispatch} />
        <TableWrapper>
          <form onSubmit={this.props.handleSubmit(this.onSubmit)} >
            <div className="service-wrapper" style={{ paddingTop: '49px' }} >
              <div className="table-top" >
                <div className="table-top--row" >
                  <div className="table-top--row__col1" >Category, Colour</div>
                  <div className="table-top--row__col2" >
                    <div className="table-top--row__col2--left" >
                      <Field
                        name="category"
                        component={renderSelectField}
                        placeholder="Category"
                        required
                        onChange={this.onCategoryChange}
                      >
                        {this.getCategoryList()}
                      </Field>
                    </div>
                    <div className="button" >
                      <Field name="categoryColor" component={renderColorInputField} />
                    </div>
                  </div>

                </div>
                <div className="table-top--row" >
                  <div className="table-top--row__col1" >Name</div>
                  <div className="table-top--row__col2" >
                    <div className="table-top--row__col2--left" >

                      <Field
                        name="name.defaultName"
                        component={renderInputField}
                        placeholder="Default name"
                        required
                        validate={[maxDefLength]}
                      />
                    </div>
                    <div className="table-top--row__col2--left" style={{ marginLeft: '10px', width: '55%' }} >
                      <div style={{ width: '47%' }} >

                        <Field
                          name="name.shortName"
                          component={renderInputField}
                          placeholder="Short name"
                          validate={[maxShortLength]}
                        />

                      </div>
                    </div>
                  </div>
                </div>
                <div className="table-top--row" >
                  <div className="table-top--row__col1" style={{ visibility: 'hidden' }} >Name</div>
                  <div className="table-top--row__col2" >
                    <Field
                      name="name.longName"
                      component={renderInputField}
                      placeholder="Long name"
                      validate={[ maxLongLength ]}
                    />
                  </div>
                </div>
                <FieldArray name="payers" component={this.payersFields}
                />

              </div>
            </div>
            <div className="service-wrapper" style={{ paddingTop: '10px' }} >
              <div className="table-top" >

                <FieldArray name="staffAllocation" component={this.staffAllocationFields} />

              </div>

            </div>
            <div className="service-wrapper" style={{ paddingTop: '10px' }} >
              <div className="table-top" >
                <FieldArray name="equipmentAllocation" component={this.equipmentFields} />
              </div>

            </div>
            <div className="service-wrapper" style={{ paddingTop: '10px' }} >
              <div className="table-top" >
                <FieldArray name="roomAllocation" component={this.roomFields} />
              </div>

            </div>
            <CardFooter>
              <FormGroup>
                <PulledRight>
                  <GreyButton onClick={this.props.reset} >Cancel</GreyButton>
                  <GreenButton type="submit" disabled={this.props.pristine} >
                    {this.onDelete ? 'Save Changes' : 'Add Service'}
                  </GreenButton>
                </PulledRight>
              </FormGroup>
            </CardFooter>
          </form>
        </TableWrapper>
      </div>
    );
  }
}

MediclServicesContainer.propTypes = {
  staff: PropTypes.array,
  payers: PropTypes.array,
  rooms: PropTypes.array,
  roles: PropTypes.array,
  equipments: PropTypes.array,
  equipmentAllocation: PropTypes.array,
  staffAllocation: PropTypes.array,
  roomAllocation: PropTypes.array,
  categories: PropTypes.array,
  pristine: PropTypes.bool,
  duration: PropTypes.string,
  company: PropTypes.object,
  currState: PropTypes.object,
  reset: PropTypes.func,
  handleSubmit: PropTypes.func,
  change: PropTypes.func,
  selector: PropTypes.func,
};

const formValidate = (formProps) => {
};

const selector = formValueSelector('MediclServicesContainer');

const MediclService = reduxForm({
  form: 'MediclServicesContainer',
  validate: formValidate,
  initialValues: {
    payers: [{ payer: undefined, duration: undefined, price: undefined }],
    staffAllocation: [{ role: undefined, staff: undefined }],
    equipmentAllocation: [{ name: undefined }],
    roomAllocation: [{ number: undefined }],
    showJoin: false,
  },
}, mapStateToProps)(MediclServicesContainer);


function mapStateToProps(state) {
  return {
    payers: selector(state, 'payers'),
    staffAllocation: selector(state, 'staffAllocation') ? selector(state, 'staffAllocation') : [{
      role: undefined,
      staff: undefined,
    }],
    equipmentAllocation: selector(state, 'equipmentAllocation'),
    roomAllocation: selector(state, 'roomAllocation'),
    duration: selector(state, 'duration'),
    price: selector(state, 'price'),
    showJoin: selector(state, 'showJoin'),
    roles: state.main.roleList,
    staff: state.main.users,
    equipments: state.main.equipments,
    rooms: state.main.rooms,
    company: state.main.company,
    categories: state.medicalservice.categories,
    currState: state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    createMedservice,
    selector,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(MediclService);
