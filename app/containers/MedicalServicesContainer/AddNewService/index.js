/*
 *
 * MeidcalServicer
 *
 */

import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import NewMeServiceIcon from 'assets/icons/Title/NewMed.Service.svg';
import { push } from 'react-router-redux';
import FormLegend from 'components/FormLegend';
import Form from './Form';
import messages from '../messages';
import { FormWrapper } from 'components/styles/Grid';


import '../../../components/ServiceForm/style.scss';

import { HeaderPanelHelper } from 'components/common/HeaderPanel';

import { createMedservice } from '../actions';


class NewServiceContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);

    this.state = {
      hideColor: true,
      color: 'white',
      staff: this.props.staff ? this.props.staff : [],
      allInsuranceChosen: false,
      allInsuranceChosenIndex: -1,
      selectStaffForJoin: [],
      connectedJoin: [],
      staffForJoin: {},
      currentRole: '',
      isAllStaffChosen: false,
      isAnyRoomChosen: false,
      arrayIndex: -1,
      arrayIndexRoom: -1,
    };
    this.state[`hideUserMenu${0}`] = false;
  }

  componentDidMount() {
  }


  onSubmit(data) {
    const formData = data;
    formData.company = this.props.company._id;
    formData.staffAllocation = formData.staffAllocation.map(staf => {
        staf.staff = staf.isAnyStaff ? null : staf.staff;
        return staf;
      }
    );
    formData.roomAllocation = [];
    formData.equipmentAllocation.forEach(eq => {
      formData.roomAllocation.push({ room: eq.room });
    });
    formData.equipmentAllocation.forEach((eq, index) => {
      if (eq.equipment === '') {
        formData.equipmentAllocation[index].equipment = null;
      }
    });
    this.props.createMedservice(formData);
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.push('/main/med-services');
  }

  render() {
    const { formatMessage } = this.context.intl;
    const { headerPanel } = this.props;
    return (
      <div>
        <Helmet title="MedORG - Medical Services - New Service" />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 1 })}
        <FormWrapper>
          <FormLegend
            Icon={NewMeServiceIcon}
            text={formatMessage(messages.NewMedicalService)}
          />
          <Form
            submitClick={this.onSubmit}
            isEdit={false}
            onCancel={this.handleCancel}
          />
        </FormWrapper>
      </div>
    );
  }
}

NewServiceContainer.propTypes = {
  staff: PropTypes.array,
};

NewServiceContainer.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    roles: state.medicalservice.roleList,
    staff: state.main.users,
    equipments: state.medicalservice.equipment,
    rooms: state.medicalservice.rooms,
    company: state.main.company,
    categories: state.medicalservice.categories,
    services: state.medicalservice.services ? state.medicalservice.services : [],
    currState: state,
  };
}

const mapDispatchToProps = {
  createMedservice,
  push,
}


export default connect(mapStateToProps, mapDispatchToProps)(NewServiceContainer);
