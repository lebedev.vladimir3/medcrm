import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import ServiceForm, { validate }  from 'components/ServiceForm';


let MediclService = reduxForm({
  form: 'EditMedService',
  validate: validate,
  initialValues: {
    payers: [{ payer: undefined, duration: undefined, price: undefined }],
    staffAllocation: [{ role: undefined, staff: undefined }],
    equipmentAllocation: [{ name: undefined }],
    roomAllocation: [{ number: undefined }],
  },
})(ServiceForm);

const selector = formValueSelector('EditMedService');

MediclService = connect(
  state => {
    return {
      payers: selector(state, 'payers') ? selector(state, 'payers') : [{
        payer: undefined,
        duration: undefined,
        price: undefined,
      }],
      staffAllocation: selector(state, 'staffAllocation'),
      equipmentAllocation: selector(state, 'equipmentAllocation'),
      roomAllocation: selector(state, 'roomAllocation') ? selector(state, 'roomAllocation') : [{}],
      duration: selector(state, 'duration'),
      price: selector(state, 'price'),
      staff: state.main.users,
      roles: state.medicalservice.roleList,
      equipments: state.medicalservice.equipment,
      rooms: state.medicalservice.rooms,
      company: state.main.company,
      categories: state.medicalservice.categories,
      services: state.medicalservice.services ? state.medicalservice.services : [],
      currState: state,
    };
  }
)(MediclService);


export default MediclService;
