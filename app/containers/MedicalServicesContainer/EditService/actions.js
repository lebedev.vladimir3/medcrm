
import {
  FETCH_MED_SERVICE_REQUEST,
  EDIT_PAGE_UPDATE_MED_SERVICE_REQUEST,
  EDIT_PAGE_DELETE_MED_SERVICE_REQUEST,
} from './constants';


export const loadMedService = (id) => {
  return ({ type: FETCH_MED_SERVICE_REQUEST, payload: id });
};

export const updateMedService = ({ id, formData }) => ({ type: EDIT_PAGE_UPDATE_MED_SERVICE_REQUEST, payload: { id, formData } });

export const deleteMedService = ({ services, id }) => ({ type: EDIT_PAGE_DELETE_MED_SERVICE_REQUEST, payload: { services, id } });
