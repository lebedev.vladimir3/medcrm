/*
 *
 * MeidcalServicer
 *
 */

import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import DeleteModal from 'components/DeleteModal';
import MedServiceProfileIcon from 'assets/icons/Title/Med.ServicesProfile.svg';
import { push } from 'react-router-redux';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import messages from '../messages';
import FormLegend from 'components/FormLegend';
import { FormWrapper } from 'components/styles/Grid';

import './style.scss';
import { createMedservice } from '../actions';
import { loadMedService, updateMedService, deleteMedService } from './actions';
import Form from './Form';
import { HeaderPanelHelper } from 'components/common/HeaderPanel';


class MediclServicesContainer extends React.Component {
  constructor(props, context) {
    super(props, context);


    this.toggleColor = this.toggleColor.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.getRoleList = this.getRoleList.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancel = this.handleCancel.bind(this);

    this.state = {
      hideColor: true,
      color: 'white',
      staff: this.props.staff ? this.props.staff : [],
      allInsuranceChosen: false,
      allInsuranceChosenIndex: -1,
      selectStaffForJoin: [],
      connectedJoin: [],
      staffForJoin: {},
      currentRole: '',
      isAllStaffChosen: false,
      isAnyRoomChosen: false,
      arrayIndex: -1,
      arrayIndexRoom: -1,
      doesUpdate: false,
      showModal: false,
    };
    this.state[`hideUserMenu${0}`] = false;
  }

  componentDidMount() {
    this.props.dispatch(this.props.loadMedService({ id: this.props.params.id }));
    // this.props.initializeMedService(this.props.editMedService);
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.staff !== nextProps.staff) {
      this.setState({
        staff: nextProps.staff,
      });
    }
  }

  componentDidUpdate() {
    if (this.props.editMedService._id === this.props.routeParams.id && !this.state.doesUpdate) {
      if (this.props.editMedService.isAnyRoom) {
        this.props.editMedService.suitableRooms = this.props.editMedService.roomAllocation;
        this.props.editMedService.roomAllocation = [{ room: 'Any passing room' }];
      }

      const toInitialize = this.props.editMedService.equipmentAllocation.map((e, index) => {
        const copy = e;
        copy.equipment = !e._id ? '' : e._id;
        const room = this.props.editMedService.roomAllocation[index].room;
        copy.room = room || null;
        return copy;
      });

      const toStaff = this.props.editMedService.staffAllocation.map((staff) => {
        staff.staff = staff.isAnyStaff ? 'any staff of this role' : staff.staff;
        return staff;
      });

      this.props.editMedService.staffAllocation = toStaff;
      this.props.editMedService.equipmentAllocation = toInitialize;
      this.props.initializeMedService(this.props.editMedService);
      this.setState({ doesUpdate: true });
    }
  }

  handleDelete() {
    this.props.dispatch(this.props.deleteMedService({ id: this.props.params.id }));
  }

  onSubmit(formData) {
    formData.company = this.props.company._id;

    formData.staffAllocation = formData.staffAllocation.map(staf => {
        staf.staff = staf.isAnyStaff ? null : staf.staff;
        return staf;
      }
    );
    formData.roomAllocation = [];
    formData.equipmentAllocation.forEach(eq => {
      formData.roomAllocation.push({ room: eq.room });
    });
    formData.equipmentAllocation.forEach((eq, index) => {
      if (eq.equipment === '') {
        formData.equipmentAllocation[index].equipment = null;
      }
    });
    this.props.dispatch(this.props.updateMedService({ id: this.props.params.id, formData: formData }));
    this.props.dispatch(this.props.push('/main/med-services'));
  }

  getRoleList() {
    return this.props.roles.map((role, index) =>
      <option value={role.name} key={index}>{role.name}</option>
    );
  }

  toggleColor() {
    this.setState({
      hideColor: !this.state.hideColor,
    });
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.dispatch(this.props.push('/main/med-services'));
  }


  render() {
    const { headerPanel } = this.props;
    return (
      <div>
        <Helmet title="MedORG - Medical Services - Service Profile" />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 0 })}
        <FormWrapper>
          <FormLegend
            Icon={MedServiceProfileIcon}
            text={this.context.intl.formatMessage(messages.MedicalServices)}
            secondText={this.props.editMedService.name ? this.props.editMedService.name.defaultName : ''}
          />
          {this.state.showModal && <DeleteModal
            handleDelete={this.handleDelete}
            handleCancel={this.toggleModal}
            headerText={this.context.intl.formatMessage(messages.DeleteMedical)}
            bodyText={this.context.intl.formatMessage(messages.QuestService) + '?'}
          />}
          <Form
            submitClick={this.onSubmit}
            editMedService={this.props.editMedService}
            isEdit
            toggleModal={this.toggleModal}
            onCancel={this.handleCancel}
          />
        </FormWrapper>
      </div>
    );
  }
}

MediclServicesContainer.propTypes = {
  staff: PropTypes.array,
  roles: PropTypes.array,
  company: PropTypes.object,
  editMedService: PropTypes.object,
  initializeMedService: PropTypes.func,
  deleteMedService: PropTypes.func,
  dispatch: PropTypes.func,
};

MediclServicesContainer.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    roles: state.main.roleList,
    staff: state.main.users,
    equipments: state.medicalservice.equipment,
    rooms: state.medicalservice.rooms,
    company: state.main.company,
    categories: state.medicalservice.categories,
    editMedService: state.EditMedService.editMedService ? state.EditMedService.editMedService : {},
    services: state.medicalservice.services,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    createMedservice,
    loadMedService,
    updateMedService,
    deleteMedService,
    push,
    initializeMedService: (medService) => {
      dispatch(initialize('EditMedService', medService));
    },
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(MediclServicesContainer);
