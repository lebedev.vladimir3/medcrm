/*
 *
 * EditServicePage reducer
 *
 */

import { FETCH_MED_SERVICE_SUCCESS } from './constants';

const initialState = {
  editMedService: {},
};

function editMedServicePageReducer(state = initialState, action) {

  switch (action.type) {
    case FETCH_MED_SERVICE_SUCCESS:
      return { ...state, editMedService: action.currentMedService };
    default:
      return state;
  }
}

export default editMedServicePageReducer;
