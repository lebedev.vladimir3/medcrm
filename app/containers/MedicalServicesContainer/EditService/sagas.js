/*
 *
 * EditServicePage sagas
 *
 */

import { put, take, cancel, takeLatest, fork } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';

import {
  FETCH_MED_SERVICE_REQUEST,
  EDIT_PAGE_UPDATE_MED_SERVICE_REQUEST,
  EDIT_PAGE_DELETE_MED_SERVICE_REQUEST,
  FETCH_MED_SERVICE_SUCCESS,
  FETCH_MED_SERVICE_FAILURE,
  EDIT_PAGE_DELETE_MED_SERVICE_SUCCESS,
} from './constants';

export function* loadMedService(action) {
  try {
    const response = yield callApi(axios.get, `/api/med-services/id/${action.payload.id}`);
    
    yield put({ type: FETCH_MED_SERVICE_SUCCESS, currentMedService: response.data });
  } catch (error) {
    yield put({ type: FETCH_MED_SERVICE_FAILURE, error: error.message });
  }
}

function* updateMedService(action) {
  try {
    const { id, formData } = action.payload;
    const { data } = yield callApi(axios.put, `/api/med-services/id/${id}`, formData);
    yield put({ type: FETCH_MED_SERVICE_SUCCESS, data });
  } catch (error) {
    yield put({ type: FETCH_MED_SERVICE_FAILURE, error: error.message });
  }
}

function* deleteMedService(action) {
  try {
    const { id } = action.payload;
    yield callApi(axios.delete, `/api/med-services/id/${id}`);
    browserHistory.push('/main/med-services/');
    yield put({ type: EDIT_PAGE_DELETE_MED_SERVICE_SUCCESS});
  } catch (error) {
    yield put({ type: FETCH_MED_SERVICE_FAILURE, error: error.message });
  }
}

function* editMedServiceSagaUpdate() {
  const update = yield takeLatest(EDIT_PAGE_UPDATE_MED_SERVICE_REQUEST, updateMedService);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(update);
}
function* editMedServiceSagaDelete() {
  const watcher = yield fork(takeLatest, EDIT_PAGE_DELETE_MED_SERVICE_REQUEST, deleteMedService);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}
function* editMedServiceSagaLoad() {
  const watcher = yield fork(takeLatest, FETCH_MED_SERVICE_REQUEST, loadMedService);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

export default [
  editMedServiceSagaUpdate,
  editMedServiceSagaDelete,
  editMedServiceSagaLoad,
];
