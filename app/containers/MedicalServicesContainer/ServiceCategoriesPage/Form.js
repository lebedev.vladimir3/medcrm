import { reduxForm } from 'redux-form';
import ServiceCategoriesForm, { validate } from 'components/ServiceCategoriesForm';


export default reduxForm({
  form: 'categoriesForm',
  validate
})(ServiceCategoriesForm);
