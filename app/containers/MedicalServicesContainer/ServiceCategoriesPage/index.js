/*
 *
 * ServiceCategoriesPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';

import { HeaderPanelHelper } from 'components/common/HeaderPanel';

import MedCategoriesIcon from 'assets/icons/Title/Med.Categories.svg';
import Form from './Form';
import { PageWrapper } from 'components/styles/Grid';

import FormLegend from 'components/FormLegend';
import {
  fetchCategoryList,
  createCategory,
  updateCategoryList,
  deleteCategory,
} from '../actions';
import messages from '../messages';


export class ServiceCategoriesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(fetchCategoryList());
  }

  componentDidUpdate() {
    if (this.props.categories.length > 0) {
      this.props.initializeCategoryList({ categories: this.props.categories });
    }
  }

  handleSubmit(params) {
    this.props.dispatch(updateCategoryList(params.categories));
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.dispatch(push('/main/med-services'));
  }

  render() {
    const { headerPanel } = this.props;
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Helmet
          title="MedORG - Service Categories Management"
          meta={[
            { name: 'description', content: 'Service Categories' },
          ]}
        />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 0})}
        <PageWrapper>
          <FormLegend
            Icon={MedCategoriesIcon}
            text={formatMessage(messages.ServiceCategories)}
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            categories={this.props.categories}
          />
        </PageWrapper>
      </div>
    );
  }
}

ServiceCategoriesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  categories: PropTypes.array,
  initializeCategoryList: PropTypes.func,
};
ServiceCategoriesPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
}
function mapStateToProps(state) {
  return {
    categories: state.medicalservice.categories,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    createCategory,
    fetchCategoryList,
    updateCategoryList,
    deleteCategory,
    initializeCategoryList: categories => {
      dispatch(initialize('categoriesForm', categories));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceCategoriesPage);
