import fontSetting from 'components/styles/font';
/*
 *
 * ServiceCategoriesPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import Helmet from 'react-helmet';
import { Table, TR, TD, TH } from 'components/Table';
import { Card } from 'components/Card';

import FormLegend from 'components/FormLegend';
import styled from 'styled-components';
import { push } from 'react-router-redux';
import MedServicesIcon from 'assets/icons/Title/Med.Services.svg';
import PersonAdd from 'react-icons/lib/ti/document-add';
import { PageWrapper } from 'components/styles/Grid';
import { CustomColor } from 'components/styles/common';

import {
  fetchCategoryList,
  fetchRoomList,
  fetchEquipmentList,
  fetchServiceList,
  fetchRoleList,
} from '../actions';
import messages from '../messages';
import Select from 'react-select';
import 'react-select/dist/react-select.css';


const CategorySelectWrapper = styled.div`
  padding: 14px 30px;
  max-width: 980px;
  margin: auto;
  background-color: white;
  border-top: 1px solid #DADFEA;
  border-left: 1px solid #DADFEA;
  border-right: 1px solid #DADFEA;
  display: flex;
`;

const TableDiv = styled.div`
  padding: 13.5px 0 13.5px 0;
`;


const Label = styled.label`
  color: #455A64;
  font-size: ${fontSetting.baseSizePlus1}px;
  font-weight: 600;
  line-height: 19px;
  text-align: center;
  margin-right: 16px;
`;
const LabelVertCent = styled(Label)`
  display: flex;
  align-items: center;
  font-weight: normal;
`;
const Div = styled.div`
  position: absolute;
  left: -40px;
  top: 11px;
`;
const DivInsideTd = styled.div`
  height: 30x;
  position: relative;
`;

const WrapperSelect = styled.div`
  width: 270px;
  display: inline-block;
`;

export class ServicePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleChangeCategory = this.handleChangeCategory.bind(this);
    this.state = { category: null };
  }

  handleChangeCategory(e) {
    const category = this.props.categories.find((item) => item._id === e.value);
    this.setState({ category: category });
  }

  componentDidUpdate() {
    if (!this.state.category && this.props.categories.length > 0) {
      this.setState({ category: this.props.categories[0] });
    }
  }

  componentWillMount() {
    const isCategories = window.location.pathname === '/main/med-services/categories';
    !isCategories && this.props.dispatch(this.props.fetchCategoryList());
    this.props.dispatch(this.props.fetchRoomList());
    this.props.dispatch(this.props.fetchEquipmentList());
    this.props.dispatch(this.props.fetchServiceList());
    this.props.dispatch(this.props.fetchRoleList());
  }

  render() {
    const { category } = this.state;
    const { formatMessage } = this.context.intl;
    const { headerPanel } = this.props;
    const options = this.props.categories.map(
      (category, index) => ({
        value: category._id,
        label: category.name
      }));

    const rows = this.props.services
      .filter((item) => category && item.category === category._id)
      .map((service, index) => {
        let color = category.color;
        let payerStatusStringCommon = '';
        let payerStatusDuration = 0;
        let payerStatusPrice = 0;
        let payerPrivateStatusIndex = null;
        service.payers.forEach((payer, i) => {
          if (payer.payer === 'P') {
            payerPrivateStatusIndex = i;
          } else {
            payerStatusStringCommon += payer.payer + ', ';
            payerStatusDuration += payer.duration;
            payerStatusPrice += payer.price;
          }
        });
        payerStatusStringCommon = payerStatusStringCommon !== '' ? payerStatusStringCommon.substring(0, payerStatusStringCommon.length - 2) : '';
        const payerCommonDuration = payerStatusDuration / (payerPrivateStatusIndex ? (service.payers.length - 1) : service.payers.length);
        const payerCommonPrice = payerStatusPrice / (payerPrivateStatusIndex ? (service.payers.length - 1) : service.payers.length);

        //console.log('sv test', payerPrivateStatusIndex, payerStatusStringCommon)

        return (
          <TR key={index} link={`/main/med-services/edit/${service._id}`} zIndexNeeded>
            <TD style={{paddingLeft: '60px', fontWeight: '600'}}>
              <DivInsideTd>
                <Div><CustomColor color={color} /></Div>
                <TableDiv style={{display: 'inline-block'}}>{service.name ? service.name.defaultName : ''}</TableDiv>
              </DivInsideTd>
            </TD>
            <TD centered style={{fontWeight: '600'}}>
              {payerPrivateStatusIndex && <TableDiv>P</TableDiv>}
              {payerStatusStringCommon !== '' && <TableDiv>{payerStatusStringCommon}</TableDiv>}
            </TD>
            <TD centered>
              {payerPrivateStatusIndex && <TableDiv>{service.payers[payerPrivateStatusIndex].duration} min</TableDiv>}
              {payerStatusStringCommon !== '' && <TableDiv>{payerCommonDuration} min</TableDiv>}
            </TD>
            <TD centered>
              {payerPrivateStatusIndex && <TableDiv>{service.payers[payerPrivateStatusIndex].price} &euro;</TableDiv>}
              {payerStatusStringCommon !== '' && <TableDiv>{payerCommonPrice} &euro;</TableDiv>}
            </TD>
            <TD centered style={{paddingRight: '20px'}}>
              {payerPrivateStatusIndex &&
              <TableDiv>{(service.payers[payerPrivateStatusIndex].price / service.payers[payerPrivateStatusIndex].duration * 60).toFixed(1)} &euro;</TableDiv>
              }
              {payerStatusStringCommon !== '' && <TableDiv>{(payerCommonPrice / payerCommonDuration * 60).toFixed(1)} &euro;</TableDiv>}
            </TD>
          </TR>
        )
          ;
      });
    return (
      <div>
        <Helmet
          title="MedORG - Medical Services"
          meta={[
            { name: 'description', content: 'Service' },
          ]}
        />
        { headerPanel }
        <PageWrapper>
          <FormLegend
            Icon={MedServicesIcon}
            text={formatMessage(messages.MedicalServices)}
          />
          <CategorySelectWrapper>
            <LabelVertCent>{formatMessage(messages.Category)}</LabelVertCent>
            <WrapperSelect>
              <Select
                name="qqqq"
                options={options}
                onChange={this.handleChangeCategory}
                searchable={false}
                clearable={false}
                value={category ? { value: category._id, label: category.name }: null}
                placeholder={formatMessage(messages.ChooseCategory)}
              />
            </WrapperSelect>
          </CategorySelectWrapper>
          <Card>
            <Table>
              <thead>
                <TR>
                  <TH style={{paddingLeft: '60px'}}>{formatMessage(messages.ServiceName)}</TH>
                  <TH centered>{formatMessage(messages.PayerStatus)}</TH>
                  <TH centered>{formatMessage(messages.Duration)}</TH>
                  <TH centered>{formatMessage(messages.Price)}</TH>
                  <TH centered style={{paddingRight: '20px'}}>{formatMessage(messages.Price)}/h</TH>
                </TR>
              </thead>
              <tbody>
                {this.props.categories && category && category._id && rows}
              </tbody>
            </Table>
          </Card>
        </PageWrapper>
      </div>
    );
  }
}

ServicePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  services: PropTypes.array,
  categories: PropTypes.array,
};

ServicePage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    services: state.medicalservice.services,
    categories: state.medicalservice.categories,
    company: state.main.company,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchServiceList,
    push,
    fetchCategoryList,
    fetchRoomList,
    fetchEquipmentList,
    fetchRoleList,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServicePage);
