/*
 *
 * StaffPage actions
 *
 */

import {
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_CATEGORY_LIST_REQUEST,
  CREATE_CATEGORY_REQUEST,
  UPDATE_CATEGORY_LIST_REQUEST,
  DELETE_CATEGORY_REQUEST,
  CREATE_SERVICE_REQUEST,
  FETCH_ROOMS_REQUEST,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
} from './constants';


export function createCategory(category) {
  return { type: CREATE_CATEGORY_REQUEST, category };
}

export function fetchCategoryList() {
  return { type: FETCH_CATEGORY_LIST_REQUEST };
}

export function updateCategoryList(categoryList) {
  return { type: UPDATE_CATEGORY_LIST_REQUEST, categoryList };
}

export function deleteCategory(categoryId) {
  return {
    type: DELETE_CATEGORY_REQUEST,
    id: categoryId,
  };
}

export function fetchServiceList() {
  return { type: FETCH_SERVICE_LIST_REQUEST };
}

export const fetchRoomList = () => ({ type: FETCH_ROOMS_REQUEST });

export const fetchEquipmentList = () => ({ type: FETCH_EQUIPMENT_LIST_REQUEST });

export const createMedservice = (data) => {
  return { type: CREATE_SERVICE_REQUEST, data: data };
};


export const fetchRoleList = () =>
  ({
    type: FETCH_ROLE_LIST_REQUEST
  });