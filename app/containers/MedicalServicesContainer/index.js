/*
 *
 * MeidcalServicer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import MessageAllowed from 'components/NotAllowed';
import roles from '../../../server/utils/authorization/roles';

import {
  fetchCategoryList,
  fetchRoomList,
  fetchEquipmentList,
  fetchServiceList,
  fetchRoleList,
} from './actions';

import { push } from 'react-router-redux';
import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import { ButtonContent, ButtonText } from "components/common/Button/HeaderPanel";
import { BlueButton, SecondaryButton } from "components/common/Button";
import messages from './messages';

export class MediclServicesContainer extends React.PureComponent {  // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const isCategories = window.location.pathname === '/main/med-services/categories';
    if (window.location.pathname !== '/main/med-services') {
      !isCategories && this.props.dispatch(this.props.fetchCategoryList());

      this.props.dispatch(this.props.fetchRoomList());
      this.props.dispatch(this.props.fetchEquipmentList());
      this.props.dispatch(this.props.fetchServiceList());
      this.props.dispatch(this.props.fetchRoleList());
    }
  }

  render() {
    const isUserAllowed = this.props.user && this.props.user.role.name === roles.ADMIN;

    return isUserAllowed ?
      (<div>{React.cloneElement(React.Children.toArray(this.props.children)[0], {
        headerPanel: (
          <HeaderPanel
            rightSide={[
              <SecondaryButton
                link={'/main/med-services/categories'}
                key={1}
              >
                <ButtonContent>
                  <ButtonText >{this.context.intl.formatMessage(messages.ManageServiceCategories)}</ButtonText>
                </ButtonContent>
              </SecondaryButton>,
              <BlueButton
                link={'/main/med-services/new'}
                key={2}
              >
                <ButtonContent>
                  <ButtonText >{this.context.intl.formatMessage(messages.NewService)}</ButtonText>
                </ButtonContent>
              </BlueButton>
            ]}
          >
            <HeaderItem link={'/main/med-services'}>
              {`${this.context.intl.formatMessage(messages.MedicalServices)}`}</HeaderItem>
          </HeaderPanel>)
      })}</div>) :
      <MessageAllowed />;
  }
}


MediclServicesContainer.propTypes = {
  children: PropTypes.object,
  user: PropTypes.object,
  fetchCategoryList: PropTypes.func,
  fetchRoomList: PropTypes.func,
  fetchEquipmentList: PropTypes.func,
  fetchServiceList: PropTypes.func,
  fetchRoleList: PropTypes.func,
};

MediclServicesContainer.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.App.user,
  };
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  push,
  fetchCategoryList,
  fetchRoomList,
  fetchEquipmentList,
  fetchServiceList,
  fetchRoleList,
});

export default connect(mapStateToProps, mapDispatchToProps)(MediclServicesContainer);
