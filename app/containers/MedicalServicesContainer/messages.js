/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  NewService: {
    id: 'MedicalServicesContainer.NewService',
    defaultMessage: 'New Service',
  },
  ManageServiceCategories: {
    id: 'MedicalServicesContainer.ManageServiceCategories',
    defaultMessage: 'Manage Service Categories',
  },
  AddNewService: {
    id: 'MedicalServicesContainer.AddNewService',
    defaultMessage: 'Add New Service',
  },
  NewMedicalService: {
    id: 'MedicalServicesContainer.NewMedicalService',
    defaultMessage: 'New Medical Service',
  },
  MedicalServices: {
    id: 'MedicalServicesContainer.MedicalServices',
    defaultMessage: 'Medical Services',
  },
  MedicalService: {
    id: 'MedicalServicesContainer.MedicalService',
    defaultMessage: 'Medical Service',
  },
  QuestService: {
    id: 'MedicalServicesContainer.QuestService',
    defaultMessage: 'Are you sure you want to delete this medical service',
  },
  DeleteMedical: {
    id: 'MedicalServicesContainer.DeleteMedical',
    defaultMessage: 'Delete Medical Service Profile',
  },
  ChooseCategory: {
    id: 'ChooseCategory',
    defaultMessage: 'Choose Category',
  },
  ServiceCategories: {
    id: 'MedicalServicesContainer.ServiceCategories',
    defaultMessage: 'Service Categories',
  },
  ServiceName: {
    id: 'MedicalServicesContainer.ServiceName',
    defaultMessage: 'Service Name',
  },
  Colour: {
    id: 'Colour',
    defaultMessage: 'Colour',
  },
  Payers: {
    id: 'Payers',
    defaultMessage: 'Payers',
  },
  PayerStatus: {
    id: 'MedicalServicesContainer.PayerStatus',
    defaultMessage: 'Payer Status',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  Price: {
    id: 'Price',
    defaultMessage: 'Price',
  },
  Category: {
    id: 'Category',
    defaultMessage: 'Category',
  },
  WithoutEquipment: {
    id: 'ServiceForm.WithoutEquipment',
    defaultMessage: 'Without Equipment',
  },
});
