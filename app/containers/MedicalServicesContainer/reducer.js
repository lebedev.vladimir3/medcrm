/*
 *
 * SchedulePage reducer
 *
 */

import {
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_ROOMS_SUCCESS,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_ROLE_LIST_SUCCESS,
} from './constants';

const initialState = {
  roleList: [],
  categories: [],
  services: [],
  equipment: [],
  rooms: [],
};

function staffPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORY_LIST_SUCCESS:
      return { ...state, categories: action.categoryList };
    case FETCH_SERVICE_LIST_SUCCESS:
      return {
        ...state,
        services: action.serviceList.map((service) => {
          // Payers grouped by duration and price
          const groupedPayers = [];

          service.payers.forEach((payer) => {
            const { duration, price } = payer;
            const type = payer.payer;
            const group = groupedPayers.find((item) => item.duration === duration && item.price === price);

            if (!group) {
              const newGroup = { duration, price, pricePerHour: ((price / duration) * 60), types: [type] };
              groupedPayers.push(newGroup);
              return;
            }

            if (group.types.indexOf(type) === -1) {
              group.types.push(type);
            }
          });

          return { ...service, groupedPayers };
        }),
      };
    case FETCH_ROOMS_SUCCESS:
      return { ...state, rooms: action.payload };
    case FETCH_EQUIPMENT_LIST_SUCCESS:
      return { ...state, equipment: action.payload };
    case FETCH_ROLE_LIST_SUCCESS:
      return { ...state, roleList: action.roleList };
    default:
      return state;
  }
}

export default staffPageReducer;
