import { takeLatest, put, take, cancel } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';

import {
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_ERROR,
  UPDATE_CATEGORY_LIST_REQUEST,
  UPDATE_CATEGORY_LIST_SUCCESS,
  UPDATE_CATEGORY_LIST_ERROR,
  DELETE_CATEGORY_REQUEST,
  DELETE_CATEGORY_SUCCESS,
  DELETE_CATEGORY_ERROR,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_ERROR,
  CREATE_SERVICE_REQUEST,
  CREATE_SERVICE_ERROR,
  FETCH_ROOMS_REQUEST,
  FETCH_ROOMS_SUCCESS,
  FETCH_ROOMS_ERROR,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_EQUIPMENT_LIST_ERROR,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_ERROR,
} from './constants';


export function* fetchCategoryList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services/categories');
    yield put({ type: FETCH_CATEGORY_LIST_SUCCESS, categoryList: response.data });
  } catch (error) {
    yield put({ type: FETCH_CATEGORY_LIST_ERROR, error: error.message });
  }
}


export function* updateCategoryList(action) {
  try {
    const response = yield callApi(axios.put, '/api/med-services/categories', action.categoryList);
    yield put({ type: UPDATE_CATEGORY_LIST_SUCCESS, categoryList: response.data });
  } catch (error) {
    yield put({ type: UPDATE_CATEGORY_LIST_ERROR, error: error.message });
  }
}


export function* deleteCategory(action) {
  const id = action.id;

  try {
    const response = yield callApi(axios.delete, `/api/med-services/categories${id}`, action.category);
    yield put({ type: DELETE_CATEGORY_SUCCESS, role: response.data });
  } catch (error) {
    yield put({ type: DELETE_CATEGORY_ERROR, error: error.message });
  }
}

export function* fetchServiceList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services');
    yield put({ type: FETCH_SERVICE_LIST_SUCCESS, serviceList: response.data });
  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_ERROR, error: error.message });
  }
}
export function* createService(action) {
  try {
    yield callApi(axios.post, '/api/med-services', action.data);
    browserHistory.push('/main/med-services/');
  } catch (error) {
    yield put({ type: CREATE_SERVICE_ERROR, error: error.message });
  }
}

function* fetchRoomList() {
  try {
    const response = yield callApi(axios.get, '/api/rooms');
    yield put({ type: FETCH_ROOMS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROOMS_ERROR, error: error.message });
  }
}

export function* loadRooms() {
  const create = yield takeLatest(FETCH_ROOMS_REQUEST, fetchRoomList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}

function* fetchEquipmentList() {
  try {
    const response = yield callApi(axios.get, '/api/equipment');
    yield put({ type: FETCH_EQUIPMENT_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENT_LIST_ERROR, error: error.message });
  }
}

export function* fetchRoleList() {
  try {
    const response = yield callApi(axios.get, '/api/roles/');
    yield put({ type: FETCH_ROLE_LIST_SUCCESS, roleList: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_ERROR, error: error.message });
  }
}

function* fetchServiceListSaga() {
  const saga = yield takeLatest(FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}

function* fetchCategoryListSaga() {
  const saga = yield takeLatest(FETCH_CATEGORY_LIST_REQUEST, fetchCategoryList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}

function* updateCategoryListSaga() {
  const saga = yield takeLatest(UPDATE_CATEGORY_LIST_REQUEST, updateCategoryList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}

function* deleteCategorySaga() {
  const saga = yield takeLatest(DELETE_CATEGORY_REQUEST, deleteCategory);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}

export function* createSaga() {
  const create = yield takeLatest(CREATE_SERVICE_REQUEST, createService);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}

function* fetchEquipmentListSaga() {
  const saga = yield takeLatest(FETCH_EQUIPMENT_LIST_REQUEST, fetchEquipmentList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}

function* fetchRoleListSaga() {
  const saga = yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}


export default [
  createSaga,
  loadRooms,
  fetchEquipmentListSaga,
  fetchRoleListSaga,
  fetchCategoryListSaga,
  updateCategoryListSaga,
  deleteCategorySaga,
  fetchServiceListSaga,
];
