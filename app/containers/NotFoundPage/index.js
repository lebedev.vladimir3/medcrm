/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Link } from 'react-router';
import messages from './messages';
import Helmet from 'react-helmet';


class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Helmet
          title="MedORG - Not Found Page"
          meta={[
            { name: 'description', content: 'Description Not Found Page' },
          ]}
        />
        <h1>404 {formatMessage(messages.NotFound)}!</h1>
        {/*<p><small>We definitely need to customize this page</small></p>*/}
        <p>{formatMessage(messages.GoTo)} <Link to="/">{formatMessage(messages.Home)}</Link></p>
      </div>
    );
  }
}

NotFound.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default NotFound;
