/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.NotFoundPage.header',
    defaultMessage: 'This is NotFoundPage component!',
  },
  NotFound: {
    id: "Not Found",
    defaultMessage: "Not Found"
  },
  GoTo: {
    id: "GoTo",
    defaultMessage: "Go to"
  },
  Home: {
    id: "Home",
    defaultMessage: "home"
  }
});
