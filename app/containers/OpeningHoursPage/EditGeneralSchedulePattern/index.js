import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { initialize } from 'redux-form';
import Helmet from 'react-helmet';
import DeleteModal from 'components/DeleteModal';

import FormLegend from 'components/FormLegend';
import Clock from 'assets/icons/Title/OpeningHours.svg';
import messages from './../messages';
import { ButtonWrapper } from '../components/buttons';
import { InputButton, SecondaryButton } from 'components/common/Button';
import Calendar from 'components/WorkingHoursCalendar';
import { TableContent, RightSide, LeftSide, PaddingTop } from 'components/styles/Grid';

import OverbookingModal from '../components/OverbookingModal';
import Form from './Form';
import {
  updateSchedulePattern,
  deleteSchedulePattern,
  fetchSchedulePattern,
  hideOverbookingModal,
} from '../actions';
import roles from '../../../../server/utils/authorization/roles';


export class EditSchedulePattern extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleForceSubmit = this.handleForceSubmit.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);

    this.state = {
      showModal: false, initialized: false, formData: {}, isFormSubmitted: false,
    };
    this.props.hideOverbookingModal();
  }

  componentDidMount() {
    this.props.fetchSchedulePattern({
      userId: this.props.userId,
      patternId: this.props.routeParams.id,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pattern && nextProps.pattern._id === this.props.routeParams.id && !this.state.initialized) {
      this.props.initialize('schedulePatternForm', nextProps.pattern);
      this.setState({ initialized: true });
    }
  }

  handleCancel() {
    this.props.push('/main/opening-hours#patterns');
  }

  handleSubmit(formData) {
    const { userId, routeParams } = this.props;
    this.props.updateSchedulePattern({
      userId,
      patternId: routeParams.id,
      data: formData,
    });
    this.setState({ formData })
  }

  handleDelete() {
    const { userId, routeParams } = this.props;
    this.props.deleteSchedulePattern({ userId, patternId: routeParams.id });
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  onSubmitClick() {
    this.setState({ isFormSubmitted: true });
  }


  handleForceSubmit() {
    const { userId, routeParams } = this.props;
    this.props.updateSchedulePattern({
      userId,
      patternId: routeParams.id,
      data: { ...this.state.formData, force: true },
    });
  }

  render() {
    const { overbookingModalOpen, pattern, headerPanel } = this.props;
    const { formatMessage } = this.context.intl;

    return (
      <div>
        <Helmet title="MedORG - Opening Hours - Schedule Pattern" />
        {overbookingModalOpen &&
        <OverbookingModal
          onCancel={this.props.hideOverbookingModal}
          onSubmit={this.handleForceSubmit}
        />
        }
        { headerPanel }
        <PaddingTop>
          <TableContent>
            <LeftSide>
              <FormLegend
                Icon={Clock}
                text={formatMessage(messages.OpeningHours)}
              />
              <Form
                onSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
                onDelete={this.showModal}
                canEdit={this.props.user && this.props.user.role.name === roles.ADMIN}
                title={pattern && pattern.id && pattern.id}
                onSubmitClick={this.onSubmitClick}
                isFormSubmitted={this.state.isFormSubmitted}
              />
            </LeftSide>
            <RightSide>
              <ButtonWrapper>
                <SecondaryButton
                  fullWidth
                  onClick={() => this.props.push('/main/opening-hours/patterns/new')}>
                  {/* TODO: New? */}
                  {formatMessage(messages.NewSchedulePattern)}
                </SecondaryButton>
              </ButtonWrapper>
              <Calendar />
            </RightSide>
          </TableContent>
        </PaddingTop>
        {this.state.showModal && <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.hideModal}
          headerText={formatMessage(messages.DeleteModalTitle)}
          bodyText={formatMessage(messages.DeleteModalText)}
        />}
      </div>
    );
  }
}


EditSchedulePattern.propTypes = {
  routeParams: PropTypes.object,
  push: PropTypes.func,
  initialize: PropTypes.func.isRequired,
  fetchSchedulePattern: PropTypes.func,
  updateSchedulePattern: PropTypes.func,
  deleteSchedulePattern: PropTypes.func,
  hideOverbookingModal: PropTypes.func,
  overbookingModalOpen: PropTypes.bool,
  userId: PropTypes.string,
};

EditSchedulePattern.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    pattern: state.generalSchedulePage.pattern,
    overbookingModalOpen: state.generalSchedulePage.overbookingModalOpen,
    user: state.App.user,
    company: state.main.company,
  };
}


const mapDispatchToProps = {
  fetchSchedulePattern,
  updateSchedulePattern,
  deleteSchedulePattern,
  hideOverbookingModal,
  push,
  initialize,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditSchedulePattern);
