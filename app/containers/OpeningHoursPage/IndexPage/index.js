import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import { CardFooter } from 'components/Card';
import moment from 'moment';
import { HeaderItem } from 'components/common/HeaderPanel';

import { ButtonContent, ButtonText } from "components/common/Button/HeaderPanel";
import { InputButton, SecondaryButton } from 'components/common/Button';
import { ButtonWrapper, NavigationButton } from '../components/buttons';

import SchedulePatternTable from 'components/SchedulePatternTable';
import WorkingHoursTable from 'components/WorkingHoursTable';
import Calendar from 'components/WorkingHoursCalendar';
import FaAngelLeft from 'assets/icons/Arrows/DarkIcons/ArrowLeft.svg';
import FaAngelRight from 'assets/icons/Arrows/DarkIcons/ArrowRight.svg';

import { fetchSchedulePatternList, fetchWorkingHours } from '../actions';
import messages from './../messages';
import FaPrint from 'react-icons/lib/fa/print';
import Clock from 'assets/icons/Title/OpeningHours.svg';
import FormLegend from 'components/FormLegend';

import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { MAIN } from 'constants/routes';
const SCHEDULE = MAIN.SCHEDULE;
import { TableContent, RightSide, LeftSide, PaddingTop } from 'components/styles/Grid';
import { Card } from 'components/styles/Card';
import { calculateSummaryFromMinutes, calculateWorkingHours } from 'utils';

import * as jspdf from 'jspdf';
import 'jspdf-autotable';

const StyledCardFooter = styled(CardFooter)`
  margin-top: 0px;
  display: flex;
  justify-content: space-between;
  background-color: #F2F2F2;
`;
const PaginationButton = styled(InputButton)`
  margin-left: 0px;
  height: 40px;
  width: 40px;
  text-align: center;
  display: flex;
`;
const NavButtonGroup = styled.div`
  display: flex;
  margin-left: auto;
`;
const IconNav = styled.div`
	height: 40px;
	width: 40px;
	border: 1px solid #ABB2C4;
	border-radius: 4px;
	background-color: #F6F7F9;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: auto;
  margin-bottom: auto;
  margin-right: 20px;
`;

const NavButton = styled.div`
  height: 40px;
  margin-top: auto;
  margin-bottom: auto;
	padding: 9px 14px 12px 17px;
	border-radius: 4px 0 0 4px;
	border: 1px solid #ABB2C4;
	background-color: ${props => props.active ? '#8B96AD' : "#F6F7F9"};
	color: ${props => props.active ? '#FFFFFF' : '#4A5360'};
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
	text-align: center;
	&:not(:first-child){
		border-radius: 0 4px 4px 0;
	}
`;


export class GeneralScheduleIndexPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props, context) {
    super(props, context);
    this.handleFirstTabClick = this.handleFirstTabClick.bind(this);
    this.handleSecondTabClick = this.handleSecondTabClick.bind(this);
    this.handlePeriodChange = this.handlePeriodChange.bind(this);
    this.handleOffsetIncrement = this.handleOffsetIncrement.bind(this);
    this.handleOffsetDecrement = this.handleOffsetDecrement.bind(this);
    this.onPdfClick = this.onPdfClick.bind(this);

    this.state = { period: 'week', offset: 0 };
  }

  componentWillMount() {
    this.props.fetchSchedulePatternList();
    this.props.fetchWorkingHours(this.getDateRange(this.state.period));
  }

  getDateRange(value) {
    const { offset } = this.state;
    switch (value) {
      case 'week':
      default:
        return {
          dateFrom: moment().add(offset, value).startOf('isoweek').format('YYYY-MM-DD'),
          dateTo: moment().add(offset, value).endOf('isoweek').format('YYYY-MM-DD'),
        };
      case 'month':
        return {
          dateFrom: moment().add(offset, value).startOf('month').startOf('isoweek').format('YYYY-MM-DD'),
          dateTo: moment().add(offset, value).endOf('month').endOf('isoweek').format('YYYY-MM-DD'),
        };
    }
  }

  handleFirstTabClick() {
    this.props.push('/main/opening-hours#working-hours');
  }

  handleSecondTabClick() {
    this.props.push('/main/opening-hours#patterns');
  }

  handlePeriodChange(value) {
    this.setState({ period: value, offset: 0 }, () => {
      this.props.fetchWorkingHours(this.getDateRange(this.state.period));
    });
  }

  handleOffsetIncrement() {
    this.setState({ offset: this.state.offset + 1 }, () => {
      this.props.fetchWorkingHours(this.getDateRange(this.state.period));
    });
  }

  handleOffsetDecrement() {
    this.setState({ offset: this.state.offset - 1 }, () => {
      this.props.fetchWorkingHours(this.getDateRange(this.state.period));
    });
  }

  onPdfClick(table) {

    const columns = [moment(table[0].date).format('MMMM YYYY'),
      'Date',
      'Opening Hours',
      'Holiday Title',
      'Summary Hours',
    ];
    let rows = [],
      week = [],
      workedHours = 0,
      hoursWorkedInWeek = [];
    table.forEach((row)=> {
      if (moment(row.date).day() === 1) {
        hoursWorkedInWeek.push(workedHours);
        workedHours = 0;
      }
      workedHours += calculateWorkingHours(row.workingHours);
    });
    hoursWorkedInWeek.push(workedHours);
    let iterateOnHoursWorkedInWeek = 1;

    table.forEach((row, index)=> {
      let rowArray = [];
      const isWorkingDay = row.workingHours.length > 0;

      const workingHours = row.workingHours;
      if (moment(row.date).day() === 1) {
        week = [];
        week.push(`Week ${moment(row.date).week()}`);
        week.push('');
        week.push('');
        week.push('');
        week.push(calculateSummaryFromMinutes(hoursWorkedInWeek[iterateOnHoursWorkedInWeek]));
        rows.push(week);
        iterateOnHoursWorkedInWeek++;
      }

      let openingHoursString = workingHours.length > 0 ? workingHours.map((item, index1) => {
        const timeFrom = moment.utc().startOf('day').add(item.from, 'minutes');
        const timeTo = moment.utc().startOf('day').add(item.to, 'minutes');
        return `${timeFrom.format('HH:mm')} - ${timeTo.format('HH:mm')}`;
      }).join(' ') : '—';

      rowArray.push(moment(row.date).format('dddd'));
      rowArray.push(moment(row.date).format('D. MMMM'));

      rowArray.push(openingHoursString);
      rowArray.push(row.workingHours.length <= 0 && row.holiday ? row.holiday.title : '—');
      rowArray.push(isWorkingDay ? calculateSummaryFromMinutes(row.summaryMinutes) : '—');
      rows.push(rowArray);

    });
    const tableOptions = {
      styles: {
        overflow: 'linebreak',
        valign: 'middle',
        fontSize: 7,
        minCellWidth: 40,
      },
      columnStyles: {},
    };
    const doc = new jspdf('p', 'pt');
    doc.setFontSize(7);

    doc.text('wowowow', 270, doc.autoTable.previous.finalY - doc.autoTable.previous.height - 5);
    doc.autoTable(columns, rows, {
      ...tableOptions,
      startY: 25,
    });
    doc.save('table.pdf');
  }

  render() {
    const { workingHours, schedulePatterns, roomSorting, push, headerPanel } = this.props;
    const { hash } = this.props.location;
    const { period } = this.state;
    const formatMessage = this.context.intl.formatMessage;
    return (
      <div>
        <Helmet title="MedORG - Opening Hours" />
        { headerPanel }
        <PaddingTop>
          <TableContent>
            <LeftSide>
              <FormLegend
                Icon={Clock}
                text={formatMessage(messages.OpeningHours)}
              />
              <Card>
                <Tabs
                  value={hash}
                  activeColor="#738DC8"
                  theme="status"
                  rightRemainArea={hash !== '#patterns' &&
                  <NavButtonGroup>
                    <NavButton
                      active={period === 'week'}
                      onClick={() => this.handlePeriodChange('week')}
                    >
                      {formatMessage(messages.Week)}
                    </NavButton>
                    <NavButton
                      active={period === 'month'}
                      onClick={() => this.handlePeriodChange('month')}
                      style={{marginRight: '20px'}}
                    >
                      {formatMessage(messages.Month)}
                    </NavButton>
                    <IconNav onClick={() => this.onPdfClick(workingHours)}><FaPrint /></IconNav>
                  </NavButtonGroup>
                  }
                >
                  <Tab
                    value={hash ? SCHEDULE.WORKING_HOURS: ''}
                    onClick={this.handleFirstTabClick}
                    label={formatMessage(messages.OpeningSchedule)}
                  />
                  <Tab
                    value={SCHEDULE.PATTERNS}
                    onClick={this.handleSecondTabClick}
                    label={formatMessage(messages.SchedulePatterns)}
                  />
                </Tabs>
                {
                  hash !== '#patterns' &&
                  <WorkingHoursTable
                    items={workingHours}
                    period={this.state.period}
                    onPeriodChange={this.handlePeriodChange}
                  />
                }
                {
                  hash === '#patterns' &&
                  <SchedulePatternTable
                    items={schedulePatterns}
                    workingHours={workingHours}
                    push={this.props.push}
                    onRowClick={(id) => this.props.push(`/main/opening-hours/patterns/${id}`)}
                    tableType='OpeningHours'
                  />
                }
              </Card>

              {
                hash !== '#patterns' &&
                <StyledCardFooter>
                  <PaginationButton onClick={this.handleOffsetDecrement}>
                    <FaAngelLeft />
                  </PaginationButton>
                  <PaginationButton onClick={this.handleOffsetIncrement}>
                    <FaAngelRight />
                  </PaginationButton>
                </StyledCardFooter>
              }
            </LeftSide>
            <RightSide>
              <ButtonWrapper>
                <SecondaryButton
                  fullWidth
                  link={'/main/opening-hours/patterns/new'}
                  disabled={false}
                >
                  {this.context.intl.formatMessage(messages.NewSchedulePattern)}
                </SecondaryButton>
              </ButtonWrapper>
              <Calendar />
            </RightSide>
          </TableContent>
        </PaddingTop>
      </div>
    );
  }
}


GeneralScheduleIndexPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

GeneralScheduleIndexPage.propTypes = {
  location: PropTypes.object,
  workingHours: PropTypes.array,
  push: PropTypes.func,
  fetchSchedulePatternList: PropTypes.func,
  fetchWorkingHours: PropTypes.func,
};


function mapStateToProps(state) {
  return {
    workingHours: state.generalSchedulePage.workingHours,
    company: state.main.company,
    schedulePatterns: state.generalSchedulePage.patterns,
  };
}


const mapDispatchToProps = {
  push,
  fetchSchedulePatternList,
  fetchWorkingHours,
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralScheduleIndexPage);
