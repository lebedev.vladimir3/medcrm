/*
 *
 * NewSchedulePatternPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { push } from 'react-router-redux';

import { ButtonWrapper } from '../components/buttons';
import { InputButton, SecondaryButton } from 'components/common/Button';

import Form from './Form';
import { createSchedulePattern, hideOverbookingModal } from '../actions';
import roles from '../../../../server/utils/authorization/roles';
import FormLegend from 'components/FormLegend';
import Clock from 'assets/icons/Title/OpeningHours.svg';
import messages from './../messages';
import OverbookingModal from '../components/OverbookingModal';
import Calendar from 'components/WorkingHoursCalendar';
import { TableContent, RightSide, LeftSide, PaddingTop } from 'components/styles/Grid';


export class NewSchedulePatternPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props, context) {
    super(props, context);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleForceSubmit = this.handleForceSubmit.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);
    this.props.hideOverbookingModal();
    this.state = {
      formData: {},
      isFormSubmitted: false,
    };
  }

  handleCancel() {
    this.props.push('/main/opening-hours');
  }

  handleSubmit(formData) {
    this.props.createSchedulePattern({ userId: this.props.userId, data: formData });
    this.setState({ formData });
  }

  onSubmitClick() {
    this.setState({ isFormSubmitted: true });
  }

  handleForceSubmit() {
    const { userId } = this.props;
    this.props.createSchedulePattern({
      userId,
      data: { ...this.state.formData, force: true },
    });
  }

  render() {
    const formatMessage = this.context.intl.formatMessage;
    const { overbookingModalOpen, headerPanel } = this.props;

    return (
      <div>
        <Helmet title="MedORG - Opening Hours - New Schedule Pattern" />
        {overbookingModalOpen &&
        <OverbookingModal
          onCancel={this.props.hideOverbookingModal}
          onSubmit={this.handleForceSubmit}
        />
        }
        { headerPanel }
        <PaddingTop>
          <TableContent>
            <LeftSide>
              <FormLegend
                Icon={Clock}
                text={formatMessage(messages.OpeningHours)}
              />
              <Form
                onSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
                canEdit={this.props.user && this.props.user.role.name === roles.ADMIN}
                onSubmitClick={this.onSubmitClick}
                isFormSubmitted={this.state.isFormSubmitted}
              />
            </LeftSide>

            <RightSide>
              <ButtonWrapper>
                <SecondaryButton
                  fullWidth
                  link={'/main/opening-hours/patterns/new'}
                  disabled
                >
                  {this.context.intl.formatMessage(messages.NewSchedulePattern)}
                </SecondaryButton>
              </ButtonWrapper>
              <Calendar />
            </RightSide>
          </TableContent>
        </PaddingTop>
      </div>
    );
  }
}

NewSchedulePatternPage.propTypes = {
  createSchedulePattern: PropTypes.func,
  push: PropTypes.func,
  userId: PropTypes.string,
  user: PropTypes.object,
};

NewSchedulePatternPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.App.user,
    company: state.main.company,
    overbookingModalOpen: state.generalSchedulePage.overbookingModalOpen,
  };
}

const mapDispatchToProps = {
  push,
  createSchedulePattern,
  hideOverbookingModal,
};


export default connect(mapStateToProps, mapDispatchToProps)(NewSchedulePatternPage);

