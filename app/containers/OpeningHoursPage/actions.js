/*
 *
 * ResourcesPage actions
 *
 */

import {
  FETCH_PATTERN_REQUEST,
  FETCH_PATTERN_LIST_REQUEST,
  CREATE_PATTERN_REQUEST,
  UPDATE_PATTERN_REQUEST,
  DELETE_PATTERN_REQUEST,
  FETCH_WORKING_HOURS_REQUEST,
  HIDE_OVERBOOKING_MODAL,
} from './constants';


export const createSchedulePattern = (params) => ({ type: CREATE_PATTERN_REQUEST, payload: params });

export const fetchSchedulePattern = (params) => ({ type: FETCH_PATTERN_REQUEST, payload: params });

export const fetchSchedulePatternList = (params) => ({ type: FETCH_PATTERN_LIST_REQUEST, payload: params });

export const updateSchedulePattern = (params) => ({ type: UPDATE_PATTERN_REQUEST, payload: params });

export const deleteSchedulePattern = (params) => ({ type: DELETE_PATTERN_REQUEST, payload: params });

export const fetchWorkingHours = (params) => ({ type: FETCH_WORKING_HOURS_REQUEST, payload: params });

export const hideOverbookingModal = () => ({ type: HIDE_OVERBOOKING_MODAL });
