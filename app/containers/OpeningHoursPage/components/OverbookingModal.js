import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { CardFooter as CardFooterComponent } from 'components/Card';
import Modal from 'components/Modal';
import { GreyButton, Button } from 'components/common/Button';
import messages from '../messages';


const ModalHeader = styled.div`
  width: 100%;
  height: 50px;
  color: #546E7A;
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
  text-align: center;
  padding-top: 26px;
  text-align: center;
`;

const ModalBody = styled.div`
  width: 100%;
  color: #3B4950;
  font-size: ${fontSetting.baseSizePlus1}px;
  line-height: 19px;
  padding: 20px 50px 25px;
  text-align: center;
`;

const Centered = styled.div`
  margin: auto;
`;

const CardFooter = styled(CardFooterComponent)`
  padding-left: 38px;
  padding-right: 38px;
  background-color: #F6F6F6;
`;

const OrangeButton = styled(Button)`
	background-color: #D46659;
	color: #FFFFFF;
`;
const CancelButton = styled(GreyButton)`
	background-color: #FFFFFF;
	color: #5C6678;
`;


const OverbookingModal = (props, context) => {
  const { onSubmit, onCancel } = props;
  const { formatMessage }= context.intl;

  return (
    <Modal width={400}>
      <ModalHeader>{formatMessage(messages.OverbookingAlert)}</ModalHeader>
      <ModalBody>{formatMessage(messages.OverbookingAlertMessage)}</ModalBody>
      <CardFooter>
        <Centered>
          <CancelButton onClick={onCancel}>{formatMessage(messages.Cancel)}</CancelButton>
          <OrangeButton onClick={onSubmit} style={{float: 'right'}}>{formatMessage(messages.ContinueAnyway)}</OrangeButton>
        </Centered>
      </CardFooter>
    </Modal>
  );
};

OverbookingModal.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
OverbookingModal.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};


export default OverbookingModal;
