import fontSetting from 'components/styles/font';
import styled from 'styled-components';



export const ButtonWrapper = styled.div`
  width: 100%;
  //margin-left: 15px;
  overflow: auto;
`;

export const NavigationButton = styled.button`
  border: ${props => props.disabled ? '' : '1px solid #728CC9'};
  border-radius: 4px;
  background-color: ${props => props.disabled ? '#D6DAE1' : '#FFFFFF'};
  color: ${props => props.disabled ? '#5C6678' : '#4568B0'};
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
	text-align: center;
  float: right;
  padding: 9px 20px;
  margin-bottom: 10px;
  box-sizing: border-box;
  width: 100%;
`;
