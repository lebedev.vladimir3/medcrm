/*
 *
 * StaffPage
 *
 */

import React, { PropTypes } from 'react';
import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import messages from './messages';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

const OpeningHoursPage = (props, context) => (
  <div>
    {React.cloneElement(React.Children.toArray(props.children)[0], {
      headerPanel: (
        <HeaderPanel>
          <HeaderItem link={'/main/opening-hours'}>
            {`${context.intl.formatMessage(messages.OpeningHours)}`}</HeaderItem>
        </HeaderPanel>)
    })}
  </div>
);

OpeningHoursPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

OpeningHoursPage.propTypes = { children: PropTypes.object };


const mapDispatchToProps = (dispatch) => ({ dispatch, push });

export default connect(null, mapDispatchToProps)(OpeningHoursPage);
