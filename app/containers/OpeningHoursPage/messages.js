import { defineMessages } from 'react-intl';

export default defineMessages({
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  ContinueAnyway: {
    id: 'ContinueAnyway',
    defaultMessage: 'Continue Anyway',
  },
  OpeningSchedule: {
    id: 'OpeningSchedule',
    defaultMessage: 'Opening Schedule',
  },
  SchedulePatterns: {
    id: 'SchedulePatterns',
    defaultMessage: 'Schedule Patterns',
  },
  Week: {
    id: 'Week',
    defaultMessage: 'Week',
  },
  Month: {
    id: 'Month',
    defaultMessage: 'Month',
  },
  OpeningHours: {
    id: 'OpeningHours',
    defaultMessage: 'Opening Hours',
  },
  NewSchedulePattern: {
    id: 'NewSchedulePattern',
    defaultMessage: 'New Schedule Pattern',
  },
  OverbookingAlert: {
    id: 'OverbookingAlert',
    defaultMessage: 'Overbooking Alert',
  },
  OverbookingAlertMessage: {
    id: 'OverbookingAlertMessage',
    defaultMessage: 'There is at least one appointment or event that will be left outside the opening hours.',
  },
  DeleteModalTitle: {
    id: 'SchedulePatternForm.DeleteModal.Title',
    defaultMessage: 'Delete Schedule Pattern',
  },
  DeleteModalText: {
    id: 'SchedulePatternForm.DeleteModal.Text',
    defaultMessage: 'Are you sure you want to delete this pattern?',
  },
});
