/*
 *
 * ResourcesPage reducer
 *
 */
import {
  FETCH_PATTERN_LIST_SUCCESS,
  FETCH_PATTERN_SUCCESS,
  FETCH_WORKING_HOURS_SUCCESS,
  SHOW_OVERBOOKING_MODAL,
  HIDE_OVERBOOKING_MODAL,
} from './constants';


const initialState = {
  patterns: [],
  workingHours: [],
  pattern: null,
  overbookingModalOpen: false,
};


function resourcesPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PATTERN_LIST_SUCCESS:
      return { ...state, patterns: action.payload };
    case FETCH_PATTERN_SUCCESS:
      return { ...state, pattern: action.payload };
    case FETCH_WORKING_HOURS_SUCCESS:
      return { ...state, workingHours: action.payload };
    case SHOW_OVERBOOKING_MODAL:
      return { ...state, overbookingModalOpen: true };
    case HIDE_OVERBOOKING_MODAL:
      return { ...state, overbookingModalOpen: false };
    default:
      return state;
  }
}

export default resourcesPageReducer;
