import { takeLatest, take, fork, put, cancel } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';
import {
  CREATE_PATTERN_REQUEST,
  CREATE_PATTERN_SUCCESS,
  CREATE_PATTERN_FAILURE,
  FETCH_PATTERN_LIST_REQUEST,
  FETCH_PATTERN_LIST_SUCCESS,
  FETCH_PATTERN_LIST_FAILURE,
  FETCH_PATTERN_REQUEST,
  FETCH_PATTERN_SUCCESS,
  FETCH_PATTERN_FAILURE,
  UPDATE_PATTERN_REQUEST,
  UPDATE_PATTERN_SUCCESS,
  UPDATE_PATTERN_FAILURE,
  DELETE_PATTERN_REQUEST,
  DELETE_PATTERN_SUCCESS,
  DELETE_PATTERN_FAILURE,
  FETCH_WORKING_HOURS_REQUEST,
  FETCH_WORKING_HOURS_SUCCESS,
  FETCH_WORKING_HOURS_FAILURE,
  SHOW_OVERBOOKING_MODAL,
} from './constants';


const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';


function* createSchedulePattern(action) {
  try {
    const { data } = action.payload;
    const response = yield callApi(axios.post, `/api/schedule-patterns`, data);

    if (response.data.overbooking) {
      yield put({ type: SHOW_OVERBOOKING_MODAL });
    } else {
      yield put({ type: CREATE_PATTERN_SUCCESS });
      browserHistory.push('/main/opening-hours#patterns');
    }
  } catch (error) {
    yield put({ type: CREATE_PATTERN_FAILURE, error: error.message });
  }
}


function* fetchSchedulePatternList() {
  try {
    const response = yield callApi(axios.get, `/api/schedule-patterns`);
    yield put({ type: FETCH_PATTERN_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PATTERN_LIST_FAILURE, error: error.message });
  }
}


function* fetchSchedulePattern(action) {
  try {
    const { patternId } = action.payload;
    const response = yield callApi(axios.get, `/api/schedule-patterns/${patternId}`);
    yield put({ type: FETCH_PATTERN_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PATTERN_FAILURE, error: error.message });
  }
}


function* updateSchedulePattern(action) {
  try {
    const { patternId, data } = action.payload;
    const response = yield callApi(axios.put, `/api/schedule-patterns/${patternId}`, data);
    if (response.data.overbooking) {
      yield put({ type: SHOW_OVERBOOKING_MODAL });
    } else {
      yield put({ type: UPDATE_PATTERN_SUCCESS, categoryList: response.data });
      browserHistory.push('/main/opening-hours#patterns');
    }
  } catch (error) {
    yield put({ type: UPDATE_PATTERN_FAILURE, error: error.message });
  }
}


function* deleteSchedulePattern(action) {
  try {
    const { patternId } = action.payload;
    const response = yield callApi(axios.delete, `/api/schedule-patterns/${patternId}`);
    yield put({ type: DELETE_PATTERN_SUCCESS, payload: response.data });
    browserHistory.push('/main/opening-hours#patterns');
  } catch (error) {
    yield put({ type: DELETE_PATTERN_FAILURE, error: error.message });
  }
}


function* fetchWorkingHours(action) {
  try {
    const response = yield callApi(axios.get, '/api/working-hours', { params: action.payload });
    yield put({ type: FETCH_WORKING_HOURS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_WORKING_HOURS_FAILURE, error: error.message });
  }
}


function* createSchedulePatternSaga() {
  const saga = yield fork(takeLatest, CREATE_PATTERN_REQUEST, createSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(saga);
}
function* fetchSchedulePatternListSaga() {
  const watcher = yield fork(takeLatest, FETCH_PATTERN_LIST_REQUEST, fetchSchedulePatternList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, FETCH_PATTERN_REQUEST, fetchSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* updateSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, UPDATE_PATTERN_REQUEST, updateSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* deleteSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, DELETE_PATTERN_REQUEST, deleteSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchWorkingHoursSaga() {
  const watcher = yield fork(takeLatest, FETCH_WORKING_HOURS_REQUEST, fetchWorkingHours);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}


export default [
  createSchedulePatternSaga,
  fetchSchedulePatternListSaga,
  fetchSchedulePatternSaga,
  updateSchedulePatternSaga,
  deleteSchedulePatternSaga,
  fetchWorkingHoursSaga,
];
