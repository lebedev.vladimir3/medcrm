import { reduxForm, formValueSelector } from 'redux-form';
import AppointmentFinderForm, { validate } from 'components/AppointmentFinderForm';
import { connect } from 'react-redux';
const FORM_NAME = 'appointmentFinder';

let CardForm = reduxForm({
  form: FORM_NAME,
  validate,
})(AppointmentFinderForm);


const selector = formValueSelector(FORM_NAME);

CardForm = connect(
  state => {
    const prescriptionId = selector(state, 'prescriptionId');
    const providedServices = selector(state, 'providedServices');
    const preferredTimeFrom = selector(state, 'preferredTimeFrom');
    const preferredTimeTo = selector(state, 'preferredTimeTo');
    const findAlternative = selector(state, 'findAlternative');
    return { prescriptionId, providedServices, preferredTimeFrom, preferredTimeTo, findAlternative };
  }
)(CardForm);

export { CardForm as AppointmentFinderForm, selector };
