import {
  CREATE_PRESCRIPTION,
  LOAD_PATIENT,
  LOAD_USERS,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_PRESCRIPTION_LIST_REQUEST,
  FETCH_APPOINTMENTS_SUGGESTION_REQUEST,
  FETCH_APPOINTMENTS_REQUEST,
  TOGGLE_APPOINTMENT_CHECKED,
  POST_APPOINTMENTS,
  CLEAR_APPOINTMENTS_SUGGESTION,
  TOGGLE_ALL_APPOINTMENTS_CHECKED,
  FIND_ALTERNATIVE_WITH_SAME_DAY,
  CANCELL_APPOINTMENTS_REQUEST,
} from './constants';

export const createPrescription = (data) => ({ type: CREATE_PRESCRIPTION, payload: data });

export const loadPatient = (id) => ({ type: LOAD_PATIENT, payload: id });
export const loadUsers = () => ({ type: LOAD_USERS });
export const loadMedSevices = () => ({ type: FETCH_SERVICE_LIST_REQUEST });
export const fetchPrescriptionList = (params) => ({ type: FETCH_PRESCRIPTION_LIST_REQUEST, payload: params });
export const getAppointmentsSuggestion = (data) => ({ type: FETCH_APPOINTMENTS_SUGGESTION_REQUEST, payload: data });
export const clearAppointmentsSuggestion = () => ({ type: CLEAR_APPOINTMENTS_SUGGESTION });
export const loadAppointments = (data) => ({ type: FETCH_APPOINTMENTS_REQUEST, payload: data });

export const toggleAppointmentChecked = (id) => ({ type: TOGGLE_APPOINTMENT_CHECKED, payload: id });
export const confirmSchedulingToServer = (data) => ({ type: POST_APPOINTMENTS, payload: data });

export const toggleAllAppointmentsChecked = () => ({ type: TOGGLE_ALL_APPOINTMENTS_CHECKED });

export const getAppointmentsWithSameStaffAction = (data) => ({ type: POST_APPOINTMENTS, payload: data });

export const findAlternativeSameDayAction = (data) => ({ type: FIND_ALTERNATIVE_WITH_SAME_DAY, payload: data });

export const cancelAppointments = (data) => ({ type: CANCELL_APPOINTMENTS_REQUEST, payload: data });

export function fetchCategoryList() {
  return { type: FETCH_CATEGORY_LIST_REQUEST };
}