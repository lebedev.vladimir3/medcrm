export const CREATE_PRESCRIPTION = 'app/AppointmentFinder/CREATE_PRESCRIPTION';
export const REQUEST_ERROR = 'app/AppointmentFinder/REQUEST_ERROR';
export const FETCH_PATIENT = 'app/AppointmentFinder/FETCH_PATIENT';
export const LOAD_PATIENT = 'app/AppointmentFinder/LOAD_PATIENT';
export const LOAD_USERS = 'app/AppointmentFinder/LOAD_USERS';
export const FETCH_USERS = 'app/AppointmentFinder/FETCH_USERS';
export const FETCH_SERVICE_LIST_REQUEST = 'app/AppointmentFinder/FETCH_USERS';
export const FETCH_SERVICE_LIST_FAILURE = 'app/AppointmentFinder/FETCH_SERVICE_LIST_FAILURE';
export const FETCH_SERVICE_LIST_SUCCESS = 'app/AppointmentFinder/FETCH_SERVICE_LIST_SUCCESS';

export const FETCH_CATEGORY_LIST_REQUEST = 'app/AppointmentFinder/FETCH_CATEGORY_LIST_REQUEST';
export const FETCH_CATEGORY_LIST_FAILURE = 'app/AppointmentFinder/FETCH_CATEGORY_LIST_FAILURE';
export const FETCH_CATEGORY_LIST_SUCCESS = 'app/AppointmentFinder/FETCH_CATEGORY_LIST_SUCCESS';


export const FETCH_PRESCRIPTION_LIST_REQUEST = 'app/AppointmentFinder/FETCH_PRESCRIPTION_LIST_REQUEST';
export const FETCH_PRESCRIPTION_LIST_SUCCESS = 'app/AppointmentFinder/FETCH_PRESCRIPTION_LIST_SUCCESS';
export const FETCH_PRESCRIPTION_LIST_FAILURE = 'app/AppointmentFinder/FETCH_PRESCRIPTION_LIST_FAILURE';

export const FETCH_APPOINTMENTS_REQUEST = 'app/AppointmentFinder/FETCH_APPOINTMENTS_REQUEST';
export const FETCH_APPOINTMENTS_SUCCESS = 'app/AppointmentFinder/FETCH_APPOINTMENTS_SUCCESS';
export const FETCH_APPOINTMENTS_FAILURE = 'app/AppointmentFinder/FETCH_APPOINTMENTS_FAILURE';

export const FETCH_APPOINTMENTS_SUGGESTION_REQUEST = 'app/AppointmentFinder/FETCH_APPOINTMENTS_SUGGESTION_REQUEST';
export const FETCH_APPOINTMENTS_SUGGESTION_SUCCESS = 'app/AppointmentFinder/FETCH_APPOINTMENTS_SUGGESTION_SUCCESS';
export const FETCH_APPOINTMENTS_SUGGESTION_FAILURE = 'app/AppointmentFinder/FETCH_APPOINTMENTS_SUGGESTION_FAILURE';

export const TOGGLE_APPOINTMENT_CHECKED = 'app/AppointmentFinder/TOGGLE_APPOINTMENT_CHECKED';
export const TOGGLE_ALL_APPOINTMENTS_CHECKED = 'app/AppointmentFinder/TOGGLE_ALL_APPOINTMENTS_CHECKED';

export const POST_APPOINTMENTS = 'app/AppointmentFinder/POST_APPOINTMENTS';
export const CLEAR_APPOINTMENTS_SUGGESTION = 'app/AppointmentFinder/CLEAR_APPOINTMENTS_SUGGESTION';

export const FIND_ALTERNATIVE_WITH_SAME_DAY = 'app/AppointmentFinder/FIND_ALTERNATIVE_WITH_SAME_DAY';
export const FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS = 'app/AppointmentFinder/FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS';
export const FIND_ALTERNATIVE_WITH_SAME_DAY_FAILURE = 'app/AppointmentFinder/FIND_ALTERNATIVE_WITH_SAME_DAY_FAILURE';

export const CANCELL_APPOINTMENTS_REQUEST = 'app/AppointmentFinder/CANCELL_APPOINTMENTS_REQUEST';
export const DELETE_APPOINTMENTS_SUCCESS = 'app/AppointmentFinder/DELETE_APPOINTMENTS_SUCCESS';
export const DELETE_APPOINTMENTS_FAILURE = 'app/AppointmentFinder/DELETE_APPOINTMENTS_FAILURE';

export const REMOVE_CONFIRMED_SUGGESTION = 'app/AppointmentFinder/REMOVE_CONFIRMED_SUGGESTION';
export const ADD_CONFIRMED_APPOINTMENTS = 'app/AppointmentFinder/ADD_CONFIRMED_APPOINTMENTS';
