import React from 'react';
import { connect } from 'react-redux';
import FormLegend from 'components/FormLegend';
import PersonAdd from 'react-icons/lib/md/person-add';
import MenuIcon from 'react-icons/lib/ti/th-large-outline';
import { push } from 'react-router-redux';
import { initialize, reset } from 'redux-form';
import moment from 'moment';
import messages from '../../messages';

import MiniCardPrescription from '../../components/MiniCardPrescription';
import Card from './FormCard';
import { AppointmentFinderForm } from './Form';
import {
  loadPatient,
  loadUsers,
  loadMedSevices,
  fetchCategoryList,
  fetchPrescriptionList,
  loadAppointments,
  getAppointmentsSuggestion,
  toggleAppointmentChecked,
  confirmSchedulingToServer,
  clearAppointmentsSuggestion,
  toggleAllAppointmentsChecked,
  getAppointmentsWithSameStaffAction,
  findAlternativeSameDayAction,
  cancelAppointments,
} from './actions';
import { fetchAppointmentList }from '../actions';

class AppointmentFinderPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
    this.changePayer = this.changePayer.bind(this);
    this.confirmScheduling = this.confirmScheduling.bind(this);
    this.getUnscheduledCountQuantityForService = this.getUnscheduledCountQuantityForService.bind(this);
    this.findAlternativeSameDay = this.findAlternativeSameDay.bind(this);

    this.state = {
      selectedTab: 1,
      selectedPayer: props.currentPatient,
      didInitialized: false,
    };
  }

  componentDidMount() {
    this.props.loadPatient(this.props.params.id);
    this.props.loadUsers();
    this.props.loadMedSevices();
    this.props.fetchCategoryList();
    this.props.fetchPrescriptionList();
    this.props.loadAppointments();
    this.props.fetchAppointmentList({ patient: this.props.routeParams.id });
  }

  componentDidUpdate() {
    const { prescriptions, currentPatient } = this.props;
    const { selectedPayer } = this.state;
    if (prescriptions.length !== 0 && (!this.state.didInitialized || selectedPayer._id !== currentPatient._id)) {
      let filteredPrescriptions = prescriptions.filter(pres =>
        pres.patient === currentPatient._id
      );
      let pres = filteredPrescriptions.reduce((pres1, pres2) => {

        if (!pres2.issuedDate) {
          return pres1;
        } else {
          if (!pres1.issuedDate) {
            return pres2;
          }
        }
        if (pres1.issuedDate && pres2.issuedDate) {
          return pres1.issuedDate < pres2.issuedDate ? pres1 : pres2;
        }
      });
      let options = [];
      pres.providedServices.forEach((providedService) => {
        providedService.service ? options.push(providedService._id) : '';
      });

      let maxService = this.findMaxFreqMultipleQuantity(pres.providedServices);
      let preferredDateTo = new Date();
      preferredDateTo.setDate(preferredDateTo.getDate() + maxService);
      this.props.initialize('appointmentFinder',
        {
          issuedDate: pres.issuedDate,
          prescriptionId: pres._id,
          providedServices: options.join(","),
          preferredDateFrom: new Date(),
          preferredDateTo: preferredDateTo,
          preferredTimeFrom: 0,
          preferredTimeTo: 23 * 60,
        },
      );
      this.setState({ didInitialized: true, selectedPayer: currentPatient });
    }

  }

  findMaxFreqMultipleQuantity(services) {
    let max = 0;
    services.forEach(service => {
      max = service.frequencyMin * service.quantity > max ? service.frequencyMin * service.quantity : max;
    });
    return max;
  }

  findPrescriptionById(id) {
    return this.props.prescriptions.find((p) => {
      return p._id === id;
    });
  }

  onTabClick(tabNumber) {
    this.setState({ selectedTab: tabNumber });
  }

  changePayer(payer) {
    this.setState({ selectedPayer: payer });
  }

  confirmScheduling() {
    let indexes = [];
    let appFilter = this.props.appointmentsSuggestion.filter((a, index) => {
        if (a.checked) {
          indexes.push(index);
        }
        return a.checked;
      }
    );

    appFilter = appFilter.map((a) => {
      a.date = a.date.toDate();
      return a;
    });

    this.props.confirmSchedulingToServer({ appointments: appFilter, indexes: indexes });
  }

  getUnscheduledCountQuantityForService(provService, prescriptionId) {
    const {
      appointments,
    } = this.props;

    let scheduled = 0;

    appointments.forEach(appointment => {
      const isRight = appointment.prescription && provService.service &&
        appointment.service && appointment._id === provService._id &&
        appointment.prescription._id === prescriptionId &&
        appointment.status === 'SCHEDULED';

      scheduled = scheduled + (isRight ? 1 : 0);
    });
    return (provService.quantity - scheduled) > 0 ? (provService.quantity - scheduled) : 0;
  }

  handleSubmit(formData) {
    formData.services = [];
    let pres = this.findPrescriptionById(formData.prescriptionId);

    let services = Array.isArray(formData.providedServices) ? formData.providedServices : formData.providedServices.split(",");

    if (services[0].length !== 0) {
      services.forEach(serviceId => {
        let foundProvService = pres.providedServices.find(provService => {
          return provService.service && provService._id === serviceId
        });
        let serviceCount = this.getUnscheduledCountQuantityForService(foundProvService, formData.prescriptionId);
        if (serviceCount > 0) {
          formData.services.push({ id: foundProvService.service._id, count: serviceCount });
        }
      });
    }

    formData.preferredDateFrom = moment(formData.preferredDateFrom).format();
    formData.preferredDateTo = moment(formData.preferredDateTo).format();
    console.log('formData');
    console.log(formData);

    this.props.getAppointmentsSuggestion(formData);
  }

  findAlternativeSameDay(data) {
    this.props.findAlternativeSameDayAction(data);
  }

  render() {
    return (
      <Card style={{ borderTopLeftRadius: '0px' }}>
        <AppointmentFinderForm
          onSubmit={this.handleSubmit}
          locale={this.props.locale}
          currentPatient={this.props.currentPatient}
          users={this.props.users}
          services={this.props.services}
          isEdit
          onTabClick={this.onTabClick}
          changePayer={this.changePayer}
          prescriptions={this.props.prescriptions}
          appointments={this.props.appointments}
          toggleAppointmentChecked={this.props.toggleAppointmentChecked}
          push={this.props.push}
          random={Math.random()}
          confirmScheduling={this.confirmScheduling}
          clearAppointmentsSuggestion={this.props.clearAppointmentsSuggestion}
          toggleAllAppointmentsChecked={this.props.toggleAllAppointmentsChecked}
          allAppointmentsSuggestionChecked={this.props.allAppointmentsSuggestionChecked}
          findAlternativeSameDay={this.findAlternativeSameDay}
          appointmentsSuggestion={this.props.appointmentsSuggestion}
          cancelAppointments={this.props.cancelAppointments}
        />
      </Card>
    );
  }
}

AppointmentFinderPage.propTypes = {
  loadPatient: React.PropTypes.func,
  currentPatient: React.PropTypes.object,
  locale: React.PropTypes.string,
  users: React.PropTypes.array.isRequired,
};
AppointmentFinderPage.contextTypes = {
  intl: React.PropTypes.object.isRequired
}
AppointmentFinderPage.defaultProps = {};

function mapStateToProps(state) {
  return {
    locale: state.language.locale,
    currentPatient: state.AppointmentFinder.patient ? state.AppointmentFinder.patient : {},
    users: state.AppointmentFinder.users ? state.AppointmentFinder.users : [],
    services: state.AppointmentFinder.services ? state.AppointmentFinder.services : [],
    categories: state.AppointmentFinder.categories ? state.AppointmentFinder.categories : [],
    prescriptions: state.AppointmentFinder.prescriptions ? state.AppointmentFinder.prescriptions : [],
    appointments: state.AppointmentFinder.appointments,
    appointmentsSuggestion: state.AppointmentFinder.appointmentsSuggestion,
    allAppointmentsSuggestionChecked: state.AppointmentFinder.allAppointmentsSuggestionChecked,
  };
}

const mapDispatchToProps = {
  push,
  loadPatient,
  loadUsers,
  loadMedSevices,
  getAppointmentsSuggestion,
  fetchCategoryList,
  fetchPrescriptionList,
  loadAppointments,
  toggleAppointmentChecked,
  confirmSchedulingToServer,
  clearAppointmentsSuggestion,
  toggleAllAppointmentsChecked,
  getAppointmentsWithSameStaffAction,
  findAlternativeSameDayAction,
  cancelAppointments,
  initialize,
  fetchAppointmentList,
  reset,
}


export default connect(mapStateToProps, mapDispatchToProps)(AppointmentFinderPage);

