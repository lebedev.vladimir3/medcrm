import moment from 'moment';

import {
  FETCH_PATIENT,
  FETCH_USERS,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_PRESCRIPTION_LIST_SUCCESS,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_APPOINTMENTS_SUGGESTION_SUCCESS,
  TOGGLE_APPOINTMENT_CHECKED,
  TOGGLE_ALL_APPOINTMENTS_CHECKED,
  CLEAR_APPOINTMENTS_SUGGESTION,
  FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS,
  REMOVE_CONFIRMED_SUGGESTION,
  DELETE_APPOINTMENTS_SUCCESS,
  ADD_CONFIRMED_APPOINTMENTS,
} from './constants';

const initialState = {
  patient: {},
  users: [],
  categories: [],
  prescriptions: [],
  appointments: [],
  appointmentsSuggestion: [],
  allAppointmentsSuggestionChecked: false,
};

function appointmentFinderReducer(state = initialState, action) {
  let index;
  let newAppointments = [];
  switch (action.type) {
    case FETCH_PATIENT:
      return { ...state, patient: action.data };
    case FETCH_USERS:
      return { ...state, users: action.data };
    case FETCH_SERVICE_LIST_SUCCESS:
      return { ...state, services: action.data };
    case FETCH_CATEGORY_LIST_SUCCESS:
      return { ...state, categories: action.categoryList };
    case FETCH_PRESCRIPTION_LIST_SUCCESS:
      return { ...state, prescriptions: action.data };
    case FETCH_APPOINTMENTS_SUCCESS:
      return { ...state, appointments: action.data };
    case REMOVE_CONFIRMED_SUGGESTION:
      let appSuggestion = [ ...state.appointmentsSuggestion ];
      appSuggestion = appSuggestion.filter((app, index) =>
        !action.data.some(id => index === id) && !app.isRadio
      );
      return { ...state, appointmentsSuggestion: appSuggestion, allAppointmentsSuggestionChecked: false };
    case DELETE_APPOINTMENTS_SUCCESS:
      let aps = [ ...state.appointments ];
      aps = aps.filter((app, index) =>
        !action.data.some(appRemove => appRemove._id === app._id)
      );
      return { ...state, appointments: aps };
    case FETCH_APPOINTMENTS_SUGGESTION_SUCCESS:
      let array = action.data
        .map((item) => ({ ...item, date: moment(item.date) }))
        .sort((a, b) =>
          a.date - b.date
        );
      return { ...state, appointmentsSuggestion: array };
    case ADD_CONFIRMED_APPOINTMENTS:
      return { ...state, appointments: action.data.concat(state.appointments) };
    case CLEAR_APPOINTMENTS_SUGGESTION:
      return { ...state, appointmentsSuggestion: [] };
    case FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS:
      let aIndex = state.appointmentsSuggestion.findIndex(a => a.checked);
      let newAppointmentsSuggestion = [ ...state.appointmentsSuggestion ];
      newAppointmentsSuggestion[ aIndex ].isRadio = true;
      let alternative = action.data.map(app => {
        app.isRadio = true;
        return app;
      });
      let joinArray = alternative.concat(newAppointmentsSuggestion);
      // (a.isRadio && !b.isRadio ? aIndex > joinArray.indexOf()
      joinArray = joinArray
        .map((item) => ({ ...item, date: moment(item.date) }))
        .sort((a, b) => {
          if (a.isRadio && b.isRadio) {
            return a.date - b.date;
          } else {
            if (a.date.isSame(b.date, 'day') && (a.isRadio || b.isRadio)) {
              return a.isRadio ? 1 : -1;
            } else {
              return a.date - b.date;
            }
          }
        });
      return {
        ...state,
        appointmentsSuggestion: joinArray,
      };
    case TOGGLE_APPOINTMENT_CHECKED:
      index = action.payload;
      newAppointments = [ ...state.appointmentsSuggestion ];
      if (newAppointments[ index ].isRadio) {
        newAppointments = newAppointments.map(app => {
          app.checked = app.isRadio ? false : app.checked;
          return app;
        });
      }
      newAppointments[ index ].checked = !state.appointmentsSuggestion[ index ].checked;
      return {
        ...state,
        appointmentsSuggestion: newAppointments,
        allAppointmentsSuggestionChecked: newAppointments.every((item) => item.checked),
      };
    case TOGGLE_ALL_APPOINTMENTS_CHECKED:
      const { allAppointmentsSuggestionChecked, appointmentsSuggestion } = state;
      return {
        ...state,
        allAppointmentsSuggestionChecked: !allAppointmentsSuggestionChecked,
        appointmentsSuggestion: appointmentsSuggestion.map((item) => ({
          ...item,
          checked: !allAppointmentsSuggestionChecked
        })),
      };
    default:
      return state;
  }
}

export default appointmentFinderReducer;
