import { put, take, cancel, takeLatest, fork } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';

import {
  REQUEST_ERROR,
  LOAD_PATIENT,
  FETCH_PATIENT,
  CREATE_PRESCRIPTION,
  LOAD_USERS,
  FETCH_USERS,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_FAILURE,
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_FAILURE,
  FETCH_PRESCRIPTION_LIST_REQUEST,
  FETCH_PRESCRIPTION_LIST_SUCCESS,
  FETCH_PRESCRIPTION_LIST_FAILURE,
  FETCH_APPOINTMENTS_REQUEST,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_APPOINTMENTS_FAILURE,
  FETCH_APPOINTMENTS_SUGGESTION_REQUEST,
  FETCH_APPOINTMENTS_SUGGESTION_SUCCESS,
  FETCH_APPOINTMENTS_SUGGESTION_FAILURE,
  POST_APPOINTMENTS,
  FIND_ALTERNATIVE_WITH_SAME_DAY,
  FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS,
  FIND_ALTERNATIVE_WITH_SAME_DAY_FAILURE,
  CANCELL_APPOINTMENTS_REQUEST,
  DELETE_APPOINTMENTS_SUCCESS,
  DELETE_APPOINTMENTS_FAILURE,
  REMOVE_CONFIRMED_SUGGESTION,
  ADD_CONFIRMED_APPOINTMENTS,
} from './constants';


function* createPrescription(action) {
  try {
    yield callApi(axios.post, '/api/prescriptions', action.payload);
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* newPrescriptionSagaCreate() {
  const create = yield takeLatest(CREATE_PRESCRIPTION, createPrescription);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}


export function* loadPatient(action) {
  try {
    const response = yield callApi(axios.get, `/api/patients/${action.payload}`);

    yield put({ type: FETCH_PATIENT, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* editPatientSagaLoad() {
  const load = yield takeLatest(LOAD_PATIENT, loadPatient);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}


function* loadUsers() {
  try {
    const response = yield callApi(axios.get, '/api/users');

    yield put({ type: FETCH_USERS, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* fetchServiceList() {
  try {
    const serviceResponse = yield callApi(axios.get, '/api/med-services');

    yield put({ type: FETCH_SERVICE_LIST_SUCCESS, data: serviceResponse.data });

  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_FAILURE, error: error.message });
  }
}
function* fetchCategoryList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services/categories');
    yield put({ type: FETCH_CATEGORY_LIST_SUCCESS, categoryList: response.data });
  } catch (error) {
    yield put({ type: FETCH_CATEGORY_LIST_FAILURE, error: error.message });
  }
}

function* fetchServiceListSaga() {
  const saga = yield takeLatest(FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}
function* fetchCategoryListSaga() {
  const saga = yield takeLatest(FETCH_CATEGORY_LIST_REQUEST, fetchCategoryList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}
function* LoadUsersSaga() {
  const load = yield takeLatest(LOAD_USERS, loadUsers);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}

function* fetchPrescriptionList(action) {
  try {
    const response = yield callApi(axios.get, '/api/prescriptions', { params: action.payload });
    yield put({ type: FETCH_PRESCRIPTION_LIST_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_PRESCRIPTION_LIST_FAILURE, error: error.message });
  }
}

function* fetchAppointmentsSuggestions(action) {
  try {
    /*const response = yield callApi(axios.post, '/api/appointments/suggestions', {
     services: [{ "id": "59354d0808dd1721dc366a57", "count": 3 }],
     preferredDateFrom: "2017-06-06T20:35:55.974Z",
     preferredDateTo: "2017-08-25T20:36:13.450Z",
     preferredTimeFrom: 32000,
     preferredTimeTo: 72000,
     prescriptionId: "59382d558426132447ab0194",
     });*/
    const response = yield callApi(axios.post, '/api/appointments/suggestions', action.payload);
    console.log('response');
    console.log(response);
    let suggestionAppointments = response.data.map(appointment=> {
      appointment.checked = false;
      return appointment;
    });
    yield put({ type: FETCH_APPOINTMENTS_SUGGESTION_SUCCESS, data: suggestionAppointments });

  } catch (error) {
    yield put({ type: FETCH_APPOINTMENTS_SUGGESTION_FAILURE, data: response.data });
  }
}

function* fetchAppointments() {
  try {
    const response = yield callApi(axios.get, '/api/appointments');
    yield put({ type: FETCH_APPOINTMENTS_SUCCESS, data: response.data });

  } catch (error) {
    yield put({ type: FETCH_APPOINTMENTS_FAILURE, data: response.data });
  }
}

function* postAppointments(action) {
  try {
    const respose = yield callApi(axios.post, '/api/appointments/confirm', action.payload.appointments);

    yield put({ type: REMOVE_CONFIRMED_SUGGESTION, data: action.payload.indexes });
    yield put({ type: ADD_CONFIRMED_APPOINTMENTS, data: respose.data });

  } catch (error) {
    // yield put({ type: FETCH_APPOINTMENTS_FAILURE, data: response.data });
  }
}

function* findAlternativeSameDay(action) {
  try {
    const response = yield callApi(axios.post, '/api/appointments/alternatives', action.payload);
    yield put({ type: FIND_ALTERNATIVE_WITH_SAME_DAY_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FIND_ALTERNATIVE_WITH_SAME_DAY_FAILURE, data: response.data });
  }
}

function* cancelAppointments(action) {
  try {
    yield callApi(axios.put, '/api/appointments', action.payload);
    yield put({ type: DELETE_APPOINTMENTS_SUCCESS, data: action.payload.scheduledAppointments });
  } catch (error) {
    yield put({ type: DELETE_APPOINTMENTS_FAILURE, error: error.message });
  }
}

function* fetchPrescriptionListSaga() {
  const watcher = yield fork(takeLatest, FETCH_PRESCRIPTION_LIST_REQUEST, fetchPrescriptionList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}
function* fetchAppointmentsSaga() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENTS_REQUEST, fetchAppointments);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}
function* fetchAppointmentsSuggestionSaga() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENTS_SUGGESTION_REQUEST, fetchAppointmentsSuggestions);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

function* postAppointmentsConfirmedSaga() {
  const watcher = yield fork(takeLatest, POST_APPOINTMENTS, postAppointments);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}
function* findAlternativeSameDaySaga() {
  const watcher = yield fork(takeLatest, FIND_ALTERNATIVE_WITH_SAME_DAY, findAlternativeSameDay);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

function* deleteAppointmentSaga() {
  const watcher = yield fork(takeLatest, CANCELL_APPOINTMENTS_REQUEST, cancelAppointments);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  newPrescriptionSagaCreate,
  editPatientSagaLoad,
  LoadUsersSaga,
  fetchServiceListSaga,
  fetchCategoryListSaga,
  fetchPrescriptionListSaga,
  fetchAppointmentsSaga,
  fetchAppointmentsSuggestionSaga,
  postAppointmentsConfirmedSaga,
  findAlternativeSameDaySaga,
  deleteAppointmentSaga,
];
