import styled from 'styled-components';


const ProvidedServices = styled.div`
  font-weight: 600;
  color: #9D8021;
`;

export default ProvidedServices;
