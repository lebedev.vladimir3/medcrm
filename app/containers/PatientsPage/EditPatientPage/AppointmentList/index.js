import React, { PropTypes } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { ContentWrap } from "../../../../components/styles/Containers";
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { MAIN } from 'constants/routes';
const APPOINTMENT = MAIN.APPOINTMENTS.STATUSES;

import Card from 'components/Card';
import { push } from 'react-router-redux';
import { Checkbox, StatusSelect } from 'components/Input';
import { connect } from 'react-redux';
import AppointmentTable from 'components/AppointmentTable';
import { AppointmentStatuses, STATUS_NAMES } from '../../../../../common/constants/statuses';
import { InputButton, PrintButton, EmailButton } from 'components/common/Button';
import {
  fetchAppointmentList,
  updateAppointmentList,
  toggleAppointmentChecked,
  setAppointmentsChecked,
  fetchPrescriptionList,
} from '../actions';
import AppointmentsToolbar from '../../components/AppointmentsToolbar';
import messages from '../messages';

import { APPOINTMENT_TABLE_COLUMN_NAME as columnNames } from 'constants/tableColumns';

const ONE_DAY = 24 * 60 * 60 * 1000;

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.handleStatusChange = this.handleStatusChange.bind(this);
    this.filterAppointments = this.filterAppointments.bind(this);
    this.state = {};
  }

  componentDidMount() {
    this.props.fetchAppointmentList({ patient: this.props.routeParams.id });
    this.props.fetchPrescriptionList({ patient: this.props.routeParams.id });
  }

  handleStatusChange(status) {
    this.props.updateAppointmentList(
      this.props.appointments
        .filter(this.filterAppointments)
        .reduce((acc, val) => {
          const newAcc = [...acc];
          if (val.checked) { newAcc.push(val._id); }
          return newAcc;
        },
        []
      ),
      { status },
    );
  }

  handleTabClick(index) {
    this.props.push(`${this.props.location.pathname}${index}`);
  }

  filterAppointments(item) {
    const { status } = item;
    switch (this.props.location.hash) {
      case APPOINTMENT.DONE:
        return status === STATUS_NAMES.PROVIDED;
      case APPOINTMENT.CANCELLED:
        return status === STATUS_NAMES.CANCELLED;
      default:
        return status !== STATUS_NAMES.PROVIDED && status !== STATUS_NAMES.CANCELLED;
    }
  }

  isPossibleToChangeStatus(checkedAppointments) {
    if (checkedAppointments.length !== 1) { return false; }
    const dateDelta = moment(checkedAppointments[0].date) - (new Date());
    return dateDelta < ONE_DAY && dateDelta > 0;
  }

  getColumnsData(){
    return [
      {
        id: columnNames.TIME,
        width: 9,
      },
      {
        id: columnNames.SERVICE,
        width: 7,
      },
      {
        id: columnNames.DURATION,
        width: 9,
      },
      {
        id: columnNames.STAFF,
        width: 17,
      },
      {
        id: columnNames.ROOM,
        width: 9,
      },
      {
        id: columnNames.PRESCRIPTION,
        width: 9,
      },
      {
        id: columnNames.STATUS,
        width: 15,
      },
      {
        id: columnNames.FINANCIAL_STATUS,
        width: 15,
      },
    ];
  }

  render() {
    const { hash } = this.props.location;
    const { formatMessage } = this.context.intl;
    // Remove cancelled status from select options
    const selectStatuses = [...AppointmentStatuses];
    selectStatuses.pop();
    const filteredAppointments = this.props.appointments.filter(this.filterAppointments);
    const checkedAppointments = filteredAppointments.filter((item) => item.checked);
    const isNothingChecked = checkedAppointments.length === 0;
    const isAllChecked = filteredAppointments.every((item) => item.checked);

    return (
      <Card>
        <Tabs
          theme={TabsThemes.STATUS}
          onTabClick={this.handleTabClick}
          value={hash}
          rightRemainArea={[
            <EmailButton key="0" gray/>,
            <PrintButton key="1" gray/>
          ]}
        >
          <Tab
            value={hash ? APPOINTMENT.PENDING: ''}
            label={formatMessage(messages.Pending)}
          />
          <Tab
            value={APPOINTMENT.DONE}
            label={formatMessage(messages.Done)}
          />
          <Tab
            value={APPOINTMENT.CANCELLED}
            label={formatMessage(messages.Cancelled)}
          />
        </Tabs>
        <AppointmentTable
          push={this.props.push}
          items={filteredAppointments}
          patient={this.props.editPatient}
          onToggleAppointmentChecked={(id) => this.props.toggleAppointmentChecked(id)}
          isEdit={false}

          orderColumns={[
            columnNames.TIME,
            columnNames.SERVICE,
            columnNames.DURATION,
            columnNames.STAFF,
            columnNames.ROOM,
            columnNames.PRESCRIPTION,
            columnNames.STATUS,
            columnNames.FINANCIAL_STATUS
          ]}
          dataColumns={this.getColumnsData()}
        />
      </Card>
    );
  }
}


AppointmentList.defaultProps = {
  appointments: [],
};

AppointmentList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

AppointmentList.propTypes = {
  appointments: PropTypes.array,
  editPatient: PropTypes.object,
  push: PropTypes.func,
  routeParams: PropTypes.object,
  fetchAppointmentList: PropTypes.func,
  updateAppointmentList: PropTypes.func,
  toggleAppointmentChecked: PropTypes.func,
  setAppointmentsChecked: PropTypes.func,
  location: PropTypes.object,
};


function mapStateToProps(state) {
  const { appointments, editPatient } = state.editPatientPage;
  return { appointments, editPatient };
}


const mapDispatchToProps = {
  push,
  fetchAppointmentList,
  updateAppointmentList,
  toggleAppointmentChecked,
  setAppointmentsChecked,
  fetchPrescriptionList,
};


export default connect(mapStateToProps, mapDispatchToProps)(AppointmentList);
