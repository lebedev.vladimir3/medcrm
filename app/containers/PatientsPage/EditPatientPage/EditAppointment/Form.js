import { reduxForm, formValueSelector } from 'redux-form';
import AppointmentCardForm, { validate } from 'components/AppointmentForm/index';
import { connect } from 'react-redux';
//import AppointmentStatusMiniCard from '../../components/AppointmentStatusMiniCard';
import MiniCard, { MiniCardPropsBinder } from 'components/common/Card';
import { types as cardTypes } from 'components/common/Card/constants';

export const FORM_NAME = 'editAppointmentForm';


const AppointmentForm = reduxForm({
  form: FORM_NAME,
  validate,
})(AppointmentCardForm);

const selector = formValueSelector(FORM_NAME);


const ConnectedForm = connect(
  (state) => ({
    chosenDate: selector(state, 'date'),
    chosenServiceCategory: selector(state, 'service.category'),
    chosenService: selector(state, 'service._id'),
    chosenRole: selector(state, 'staff.role'),
  })
)(AppointmentForm);


const AppointmentStatusForm = reduxForm({
  form: FORM_NAME,
  validate,
})(MiniCardPropsBinder({ type: cardTypes.APPOINTMENT_STATUS_CHANGER }));


export { ConnectedForm as Form, AppointmentStatusForm };
