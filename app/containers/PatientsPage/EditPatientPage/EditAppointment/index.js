import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize, reset, getFormValues } from 'redux-form';
import { push } from 'react-router-redux';
import DeleteModal from 'components/DeleteModal';
import warnAboutUnsavedChanges from 'components/WarnAboutUnsavedChanges';
import { PrintButton, EmailButton } from 'components/common/Button';
import { Form, FORM_NAME } from './Form';
import {
  fetchAppointment,
  fetchAppointmentList,
  updateAppointment,
  deleteAppointment,
  changePayer,
  fetchUserList,
  fetchServiceList,
  fetchCategoryList,
  fetchRoleList,
  fetchEquipmentList,
  fetchRoomList,
  hideOverbookingModal,
  setMessage,
} from '../actions';
import { Card } from 'components/styles/Card';
import AppointmentHeader from '../../components/AppointmentHeader';
import OverbookingModal from '../../components/OverbookingModal';
import messages from '../messages';
import roles from '../../../../../server/utils/authorization/roles';
import { HeaderPanel, HeaderItem, SecondaryHeaderPanel } from 'components/common/HeaderPanel';
import { StyledLink } from 'components/styles/common';


class EditAppointment extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.getAppointmentStatus = this.getAppointmentStatus.bind(this);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleForceSubmit = this.handleForceSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = { activeTab: 1, showModal: false, initialized: false };
  }

  componentWillMount() {
    this.init();

    this.props.setMessage({
      type: 'Note',
      text: this.context.intl.formatMessage(messages.MayCause),
    });
  }

  init() {
    this.props.fetchAppointment(this.props.routeParams.appointmentId);
    this.props.fetchServiceList();
    this.props.fetchCategoryList();
    this.props.fetchUserList();
    this.props.fetchRoleList();
    this.props.fetchEquipmentList();
    this.props.fetchRoomList();
    this.props.hideOverbookingModal();
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.activeTab !== nextState.activeTab && nextState.activeTab === 2) {
      this.props.setMessage({
        type: 'Hint',
        text: this.context.intl.formatMessage(messages.CombinedTreatment),
      });
    } else {
      this.props.setMessage({
        type: 'Note',
        text: this.context.intl.formatMessage(messages.UpdatingScheduled),
      });
    }
  }

  componentDidUpdate() {
    const { editAppointment, routeParams, pending } = this.props;
    const isEverythingFetched =
      !pending.appointments && !pending.services && !pending.categories && !pending.users && !pending.roles && !pending.equipmentList && !pending.rooms;

    if (editAppointment._id === routeParams.appointmentId && !this.state.initialized && isEverythingFetched) {
      this.props.initialize(FORM_NAME, editAppointment);
      this.props.initialize('editPrescriptionForm', editAppointment.prescription);
      this.setState({ initialized: true });
    } else if (editAppointment._id !== routeParams.appointmentId && this.state.initialized) {
      this.init();
      this.setState({ initialized: false });
    }
  }

  componentWillUnmount() {
    this.props.setMessage(null);
  }

  getAppointmentStatus() {
    switch (this.props.editAppointment.status) {
      case 'COMPLETED':
        return 'Done';
      case 'CANCELLED':
        return 'Cancelled';
      default:
        return 'Pending';
    }
  }

  handleTabClick(index) {
    this.setState({ activeTab: index });
  }

  handleSubmit(formData) {
    this.props.updateAppointment(formData);
  }

  handleForceSubmit() {
    this.props.updateAppointment({ ...this.props.formValues, force: true });
  }

  handleDelete() {
    this.props.deleteAppointment({
      appointmentId: this.props.editAppointment._id,
      patientId: this.props.routeParams.id,
    });
  }

  handleCancel(e) {
    e.preventDefault();
    e.stopPropagation();
    const hash = this.getAppointmentStatus().toLowerCase();
    this.props.push(`/main/patients/${this.props.routeParams.id}/appointments#${hash}`);
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  renderTitle(){
    const { editPatient, editAppointment } = this.props;
    const status = this.getAppointmentStatus();
    return (
      <StyledLink to={`/main/patients/${editPatient._id}/appointments#${status.toLowerCase()}`}>
        {status}
      </StyledLink>
    );
  }

  render() {
    const { overbookingModal, editAppointment, editPatient } = this.props;
    const isUserAllowed = this.props.user && this.props.user.role.name === roles.ADMIN;
    const { formatMessage } = this.context.intl;

    return (
      <div>
        {overbookingModal.isOpen &&
        <OverbookingModal
          onCancel={this.props.hideOverbookingModal}
          onSubmit={this.handleForceSubmit}
          patientIsVacant={overbookingModal.patientIsVacant}
          staffIsVacant={overbookingModal.staffIsVacant}
          roomIsVacant={overbookingModal.roomIsVacant}
        />
        }
        {this.state.showModal &&
        <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.toggleModal}
          headerText={this.context.intl.formatMessage(messages.CancelAppointment)}
          bodyText={this.context.intl.formatMessage(messages.QuestAppointment)}
          confirmButtonText={this.context.intl.formatMessage(messages.Continue)}
        />
        }
        <Card>
          <SecondaryHeaderPanel
            brandTitle={this.renderTitle()}
            rightSide={[
              <EmailButton key="0"/>,
              <PrintButton key="1"/>
            ]}
          >
            <HeaderItem>
              <span>{formatMessage(messages.AppointmentID)}</span>
              {` ${editPatient.id}-${(editAppointment.prescription && editAppointment.prescription.id) || ''}-${editAppointment.id || ''}`}
            </HeaderItem>
          </SecondaryHeaderPanel>
          <Form
            patient={this.props.editPatient}
            appointmentId={this.props.editAppointment._id}
            appointment={this.props.editAppointment}
            onSubmit={this.handleSubmit}
            onDelete={this.toggleModal}
            onCancel={this.handleCancel}
            roles={this.props.roles}
            users={this.props.users}
            services={this.props.services}
            serviceCategories={this.props.serviceCategories}
            equipmentList={this.props.equipmentList}
            rooms={this.props.rooms}
            appointments={this.props.appointments}
            locale={this.props.locale}
            changePayer={this.props.changePayer}
            isEdit
            onTabClick={this.handleTabClick}
            activeTab={this.state.activeTab}
            isAdmin={isUserAllowed}
          />
        </Card>
      </div>
    );
  }
}

EditAppointment.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EditAppointment.propTypes = {
  editPatient: PropTypes.object,
  editAppointment: PropTypes.object,
  routeParams: PropTypes.object,
  fetchAppointment: PropTypes.func,
  fetchAppointmentList: PropTypes.func,
  setMessage: PropTypes.func,
  updateAppointment: PropTypes.func,
  deleteAppointment: PropTypes.func,
  fetchServiceList: PropTypes.func,
  fetchCategoryList: PropTypes.func,
  fetchUserList: PropTypes.func,
  changePayer: PropTypes.func,
  fetchRoleList: PropTypes.func,
  fetchEquipmentList: PropTypes.func,
  fetchRoomList: PropTypes.func,
  initialize: PropTypes.func,
  hideOverbookingModal: PropTypes.func,
  push: PropTypes.func,
  locale: PropTypes.string,
  roles: PropTypes.array,
  users: PropTypes.array,
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  rooms: PropTypes.array,
  appointments: PropTypes.array,
  equipmentList: PropTypes.array,
  overbookingModal: PropTypes.object,
};


EditAppointment.defaultProps = {
  editPatient: {},
  prescription: {},
  categories: [],
};


function mapStateToProps(state) {
  const {
    editPatient,
    editAppointment,
    services,
    serviceCategories,
    users,
    roles,
    equipmentList,
    rooms,
    appointments,
    overbookingModal,
    pending,
  } = state.editPatientPage;

  return {
    editPatient,
    editAppointment,
    services,
    serviceCategories,
    users,
    roles,
    equipmentList,
    rooms,
    appointments,
    overbookingModal,
    pending,
    locale: state.language.locale,
    user: state.App.user,
    formValues: getFormValues('editAppointmentForm')(state),
    // initialValues: getFormInitialValues(FORM_NAME)(state),
  };
}

const mapDispatchToProps = {
  push,
  initialize,
  fetchAppointment,
  updateAppointment,
  deleteAppointment,
  changePayer,
  fetchUserList,
  fetchServiceList,
  fetchCategoryList,
  fetchRoleList,
  fetchEquipmentList,
  fetchRoomList,
  fetchAppointmentList,
  reset,
  hideOverbookingModal,
  setMessage,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditAppointment);
