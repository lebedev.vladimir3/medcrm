import { reduxForm, formValueSelector } from 'redux-form';
import {
  PrescriptionCardForm,
  PrescriptionServicesForm,
  PrescriptionReportsForm,
  validate,
} from 'components/PrescriptionForm';
import { connect } from 'react-redux';

import { MiniCardPropsBinder } from 'components/common/Card';
import { types as cardTypes } from 'components/common/Card/constants';

export const FORM_NAME = 'editPrescriptionForm';


let CardForm = reduxForm({
  form: FORM_NAME,
  destroyOnUnmount: false,
  validate,
})(PrescriptionCardForm);


const selector = formValueSelector(FORM_NAME);

CardForm = connect(
  state => {
    const payerName = selector(state, 'payerName');
    const doctorNameLast = selector(state, 'doctorNameLast');
    return { payerName, doctorNameLast };
  }
)(CardForm);


let ServicesForm = reduxForm({
  form: FORM_NAME,
  destroyOnUnmount: false,
  validate,
})(PrescriptionServicesForm);


ServicesForm = connect(
  state => {
    const payerName = selector(state, 'payerName');
    const serviceName = selector(state, 'name.defaultName');
    const hasPreferredStaff = selector(state, 'hasPreferredStaff');
    const providedServicesField = selector(state, 'providedServicesField');
    const providedServices = selector(state, 'providedServices');
    const prescribedServices = selector(state, 'prescribedServices');
    const isAsPrescribed = selector(state, 'isAsPrescribed');
    return {
      payerName,
      serviceName,
      hasPreferredStaff,
      providedServicesField,
      providedServices,
      prescribedServices,
      isAsPrescribed,
    };
  }
)(ServicesForm);


let PrescriptionStatusForm = reduxForm({
  form: FORM_NAME,
  destroyOnUnmount: false,
  validate,
})(MiniCardPropsBinder({ type: cardTypes.PRESCRIPTION_STATUS_CHANGER }));


PrescriptionStatusForm = connect(
  (state) => {
    return {
      statusChangedAt: selector(state, 'statusChangedAt'),
      financialStatusChangedAt: selector(state, 'financialStatusChangedAt')
    };
  }
)(PrescriptionStatusForm);


let ReportsForm = reduxForm({
  form: FORM_NAME,
  destroyOnUnmount: false,
  validate,
})(PrescriptionReportsForm);


export { CardForm, ServicesForm, ReportsForm, PrescriptionStatusForm };
