import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize, getFormInitialValues, reset } from 'redux-form';
import { push } from 'react-router-redux';
import AppointmentTable from 'components/AppointmentTable';
import DeleteModal from 'components/DeleteModal';
import { ContentWrap } from "../../../../components/styles/Containers";
import {
  //CardForm,
  //ServicesForm,
  ReportsForm,
  FORM_NAME
} from './Form';
import { PrintButton, EmailButton } from 'components/common/Button';
import {
  fetchPrescription,
  updatePrescription,
  deletePrescription,
  changePayer,
  fetchUserList,
  fetchServiceList,
  fetchCategoryList,
} from '../actions';
import PrescriptionHeader from '../../components/PrescriptionHeader';
import Card from '../../components/FormCard';
import messages from '../messages';
import roles from '../../../../../server/utils/authorization/roles';
import { STATUS_NAMES } from '../../../../../common/constants/statuses';
import { MAIN } from 'constants/routes';
const PRESCRIPTIONS = MAIN.PRESCRIPTIONS.EDIT;
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { HeaderPanel, HeaderItem, SecondaryHeaderPanel } from 'components/common/HeaderPanel';
import { APPOINTMENT_TABLE_COLUMN_NAME as columnNames } from 'constants/tableColumns';
import { StyledLink } from 'components/styles/common';

class EditPrescription extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.getPrescriptionStatus = this.getPrescriptionStatus.bind(this);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = { activeTab: 0, showModal: false, initialized: false };
  }

  componentWillMount() {
    this.props.fetchPrescription(this.props.routeParams.prescriptionId);
    this.props.fetchServiceList();
    this.props.fetchCategoryList();
    this.props.fetchUserList();
  }

  componentDidUpdate() {
    const { users, services, prescription, routeParams, pending } = this.props;

    const isEverythingFetched =
      !pending.editPrescription && !pending.services && !pending.categories && !pending.users;

    if (prescription._id === routeParams.prescriptionId && !this.state.initialized && isEverythingFetched) {
      const user = prescription.staff;
      if (user) {
        this.props.initialize(
          FORM_NAME,
          {
            ...prescription,
            title: {
              honorific: user.title ? user.title.honorific : null,
              personal: user.title ? user.title.personal : null,
            },
            doctorNameFirst: user ? user.firstName : null,
            doctorNameLast: user ? user.lastName : null,
          }
        );
      } else {
        this.props.initialize(FORM_NAME, prescription);
      }

      this.setState({ initialized: true });
    }
  }

  getPrescriptionStatus() {
    switch (this.props.prescription.status && this.props.prescription.status.type) {
      case STATUS_NAMES.PROVIDED:
        return 'Completed';
      case STATUS_NAMES.CANCELLED:
        return 'Canceled';
      default:
        return 'Open';
    }
  }

  handleTabClick(index) {
    this.setState({ activeTab: index });
  }

  handleSubmit(formData) {
    const { status, initialValues } = this.props;

    this.props.updatePrescription({
      ...formData,
    });
  }

  handleDelete() {
    this.props.deletePrescription(this.props.prescription);
  }

  handleCancel() {
    const hash = this.getPrescriptionStatus().toLowerCase();
    this.props.push(`/main/patients/${this.props.patient._id}/prescriptions#${hash}`);
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  getColumnsData(){
    return [
      {
        id: columnNames.TIME,
        width: 10,
      },
      {
        id: columnNames.SERVICE,
        width: 10,
      },
      {
        id: columnNames.DURATION,
        width: 10,
      },
      {
        id: columnNames.STAFF,
        width: 26.667,
        cell: {
          inline: true
        }
      },
      {
        id: columnNames.ROOM,
        width: 10,
      },
      {
        id: columnNames.STATUS,
        width: 16.6667,
      },
      {
        id: columnNames.FINANCIAL_STATUS,
        width: 16.6667,
      },
    ];
  }

  renderTitle(){
    const status = this.getPrescriptionStatus();
    return (
      <StyledLink to={`/main/patients/${this.props.patient._id}/prescriptions#${status.toLowerCase()}`}>
        {status}
      </StyledLink>
    );
  }


  render() {
    const { patient, prescription, currentUser } = this.props;
    const { activeTab } = this.state;
    const status = this.getPrescriptionStatus();
    const isUserAllowed = this.props.user && this.props.user.role.name === roles.ADMIN;
    const formatMessage = this.context.intl.formatMessage;

    return (
      <div>
        {this.state.showModal &&
        <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.toggleModal}
          headerText={this.context.intl.formatMessage(messages.DeletePrescription)}
          bodyText={this.context.intl.formatMessage(messages.QuestPrescription)}
        />
        }
        <Card>
          <SecondaryHeaderPanel
            brandTitle={this.renderTitle()}
            rightSide={[
              <EmailButton key="0"/>,
              <PrintButton key="1"/>
            ]}
          >
            <HeaderItem>
              <span>{formatMessage(messages.PrescriptionID)}</span>
              {` ${this.props.patient.id || ''}-${this.props.prescription.id || ''}`}
            </HeaderItem>
          </SecondaryHeaderPanel>
          <Tabs theme={TabsThemes.SVG} onTabClick={this.handleTabClick} value={activeTab}>
            <Tab value={0} label={formatMessage(messages.PrescriptionCard)} />
            {/*<Tab value={1} label={formatMessage(messages.MedicalServices)} />*/}
            <Tab value={3} label={formatMessage(messages.Appointments)} />
            <Tab value={2} label={formatMessage(messages.TreatmentReports)} />
          </Tabs>
          { activeTab === 0 && ('1')}
          {/*<CardForm*/}
            {/*currentPatient={patient}*/}
            {/*onSubmit={this.handleSubmit}*/}
            {/*onDelete={this.toggleModal}*/}
            {/*onCancel={this.handleCancel}*/}
            {/*users={this.props.users}*/}
            {/*locale={this.props.locale}*/}
            {/*changePayer={this.props.changePayer}*/}
            {/*isEdit*/}
            {/*isAdmin={isUserAllowed}*/}
          {/*/>*/}
          {/*{ activeTab === 1 &&*/}
          {/*<ServicesForm*/}
            {/*currentPatient={patient}*/}
            {/*onSubmit={this.handleSubmit}*/}
            {/*onDelete={this.toggleModal}*/}
            {/*onCancel={this.handleCancel}*/}
            {/*isEdit*/}
            {/*services={this.props.services}*/}
            {/*categories={this.props.categories}*/}
            {/*locale={this.props.locale}*/}
            {/*isAdmin={isUserAllowed}*/}
          {/*/>}*/}
          { activeTab === 2 &&
          <ReportsForm
            currentUser={currentUser}
            currentPatient={patient}
            prescription={prescription}
            onSubmit={this.handleSubmit}
            onDelete={this.toggleModal}
            onCancel={this.handleCancel}
            isEdit
            locale={this.props.locale}
            isAdmin={isUserAllowed}
          />}
          {
            activeTab === 3 &&
            <ContentWrap padding="20px 0px 0px 0px">
              <AppointmentTable
                push={this.props.push}
                items={prescription.appointments}
                patient={this.props.patient}
                onToggleAppointmentChecked={(id) => this.props.toggleAppointmentChecked(id)}
                isEdit={false}
                showPrescription={false}

                orderColumns={[
                  columnNames.TIME,
                  columnNames.SERVICE,
                  columnNames.DURATION,
                  columnNames.STAFF,
                  columnNames.ROOM,
                  columnNames.STATUS,
                  columnNames.FINANCIAL_STATUS
                ]}
                dataColumns={this.getColumnsData()}
              />
            </ContentWrap>
          }
        </Card>
      </div>
    );
  }
}

EditPrescription.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EditPrescription.propTypes = {
  patient: PropTypes.object,
  currentUser: PropTypes.object,
  prescription: PropTypes.object,
  routeParams: PropTypes.object,
  fetchPrescription: PropTypes.func,
  updatePrescription: PropTypes.func,
  deletePrescription: PropTypes.func,
  fetchServiceList: PropTypes.func,
  fetchCategoryList: PropTypes.func,
  fetchUserList: PropTypes.func,
  changePayer: PropTypes.func,
  initialize: PropTypes.func,
  push: PropTypes.func,
  locale: PropTypes.string,
  services: PropTypes.array,
  categories: PropTypes.array,
};

EditPrescription.defaultProps = {
  editPatient: {},
  prescription: {},
  categories: [],
};


function mapStateToProps(state) {
  return {
    patient: state.editPatientPage.editPatient,
    currentUser: state.global.user,
    prescription: state.editPatientPage.editPrescription,
    services: state.editPatientPage.services,
    categories: state.editPatientPage.serviceCategories,
    users: state.editPatientPage.users,
    pending: state.editPatientPage.pending,
    locale: state.language.locale,
    initialValues: getFormInitialValues(FORM_NAME)(state),
    status: state.form[FORM_NAME] ? state.form[FORM_NAME].status : null,
    user: state.App.user,
  };
}

const mapDispatchToProps = {
  push,
  initialize,
  fetchPrescription,
  updatePrescription,
  deletePrescription,
  changePayer,
  fetchUserList,
  fetchServiceList,
  fetchCategoryList,
  reset,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditPrescription);
