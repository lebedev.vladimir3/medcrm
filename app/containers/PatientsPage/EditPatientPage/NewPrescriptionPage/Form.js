// import { reduxForm, formValueSelector } from 'redux-form';
// import { PrescriptionCardForm, PrescriptionServicesForm, validate } from 'components/PrescriptionForm';
// import { connect } from 'react-redux';
// import { injectIntl } from 'react-intl';
//
// let CardForm = reduxForm({
//   form: 'addPrescriptionFormStep',
//   validate,
//   destroyOnUnmount: false,
// })(PrescriptionCardForm);
//
//
// const selector = formValueSelector('addPrescriptionFormStep');
//
// CardForm = connect(
//   state => {
//     const payerName = selector(state, 'payerName');
//     const doctorNameLast = selector(state, 'doctorNameLast');
//     return { payerName, doctorNameLast };
//   }
// )(CardForm);
//
//
// let ServicesForm = reduxForm({
//   form: 'addPrescriptionFormStep',
//   destroyOnUnmount: false,
//   validate,
// })(PrescriptionServicesForm);
//
// const selector2 = formValueSelector('addPrescriptionFormStep');
//
//
// ServicesForm = connect(
//   state => {
//     const payerName = selector(state, 'payerName');
//     const serviceName = selector2(state, 'name.defaultName');
//     const hasPreferredStaff = selector2(state, 'hasPreferredStaff');
//     const providedServicesField = selector2(state, 'providedServicesField');
//     const providedServices = selector2(state, 'providedServices');
//     const prescribedServices = selector2(state, 'prescribedServices');
//     const isAsPrescribed = selector2(state, 'isAsPrescribed');
//     return {
//       payerName,
//       serviceName,
//       hasPreferredStaff,
//       providedServicesField,
//       providedServices,
//       prescribedServices,
//       isAsPrescribed,
//     };
//   }
// )(ServicesForm);
//
//
// export { CardForm, ServicesForm, selector };
