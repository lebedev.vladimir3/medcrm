import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PhysiotherapieForm, validate } from 'components/PrescriptionForm';

let PrescriptionPhysiotherapieForm = injectIntl(reduxForm({
  form: 'addPrescriptionForm',
  validate,
  destroyOnUnmount: false,
})(PhysiotherapieForm));

PrescriptionPhysiotherapieForm = connect(
  state => ({})
)(PrescriptionPhysiotherapieForm);

export { PrescriptionPhysiotherapieForm };
