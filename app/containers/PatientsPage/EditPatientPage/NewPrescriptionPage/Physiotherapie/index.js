import fontSetting from 'components/styles/font';
import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { initialize, reset } from 'redux-form';

import { Card, CardFooterCommon } from 'components/styles/Card';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
//import { CardForm, ServicesForm }from './Form';
import { createPrescription, loadPatient, loadUsers, loadMedSevices, fetchCategoryList } from './../actions';
import { fetchPrescriptionList } from '../../../EditPatientPage/actions';
//import messages from '../../messages';

import renderHelper from 'components/common/renderHelper';
import { HeaderPanel, HeaderItem, SecondaryHeaderPanel } from 'components/common/HeaderPanel';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { ContentWrap, PrescriptionCategory as Category } from "components/styles/Containers";
import { Row, Col, LabelCol, Form, FieldsWrapCol, BorderRightCol } from 'components/styles/Grid';
import { FormLabel } from 'components/styles/common';

import { PrescriptionPhysiotherapieForm } from './Form';

class NewPrescriptionErgotherapiePage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
    this.changePayer = this.changePayer.bind(this);

    this.state = {
      selectedTab: 1,
      selectedPayer: props.currentPatient,
    };
  }

  componentDidMount() {
    this.props.dispatch(this.props.loadPatient(this.props.params.id));
    this.props.dispatch(this.props.loadUsers());
    this.props.dispatch(this.props.loadMedSevices());
    this.props.dispatch(this.props.fetchCategoryList());
  }

  onTabClick(tabNumber) {
    this.setState({ selectedTab: tabNumber });
  }

  changePayer(payer) {
    this.setState({ selectedPayer: payer });
  }

  handleSubmit(formData) {
    this.props.dispatch(this.props.reset('addPrescriptionFormStep'));
    this.props.dispatch(this.props.createPrescription(formData));
    this.props.dispatch(this.props.fetchPrescriptionList({ patient: this.props.routeParams.id }));
    this.props.dispatch(this.props.push('/main/patients/' + this.props.currentPatient._id));
  }

  render() {
    //const { intl: { formatMessage } } = this.props;
    return <PrescriptionPhysiotherapieForm/>;
  }
}

NewPrescriptionErgotherapiePage.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  createPrescription: React.PropTypes.func.isRequired,
  loadPatient: React.PropTypes.func,
  currentPatient: React.PropTypes.object,
  locale: React.PropTypes.string,
  users: React.PropTypes.array.isRequired,
};

NewPrescriptionErgotherapiePage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

NewPrescriptionErgotherapiePage.defaultProps = {};

function mapStateToProps(state) {
  return {
    locale: state.language.locale,
    currentPatient: state.newPrescription.patient ? state.newPrescription.patient : {},
    users: state.newPrescription.users ? state.newPrescription.users : [],
    services: state.newPrescription.services ? state.newPrescription.services : [],
    categories: state.newPrescription.categories ? state.newPrescription.categories : [],
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    createPrescription,
    push,
    loadPatient,
    loadUsers,
    loadMedSevices,
    fetchCategoryList,
    fetchPrescriptionList,
    initialize,
    reset,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(NewPrescriptionErgotherapiePage);
