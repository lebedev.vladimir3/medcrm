/*
 *
 * NewPRESCRIPTIONPage actions
 *
 */

import { CREATE_PRESCRIPTION, LOAD_PATIENT, LOAD_USERS, FETCH_SERVICE_LIST_REQUEST, FETCH_CATEGORY_LIST_REQUEST } from './constants';

export const createPrescription = (data) => ({ type: CREATE_PRESCRIPTION, payload: data });

export const loadPatient = (id) => ({ type: LOAD_PATIENT, payload: id });
export const loadUsers = () => ({ type: LOAD_USERS });
export const loadMedSevices = () => ({ type: FETCH_SERVICE_LIST_REQUEST });
export function fetchCategoryList() {
  return { type: FETCH_CATEGORY_LIST_REQUEST };
}