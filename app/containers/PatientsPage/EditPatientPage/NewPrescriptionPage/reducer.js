import { FETCH_PATIENT, FETCH_USERS, FETCH_SERVICE_LIST_SUCCESS, FETCH_CATEGORY_LIST_SUCCESS } from './constants';

const initialState = { patient: {}, users: [], categories: [] };

function newPatientPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PATIENT:
      return { ...state, patient: action.data };
    case FETCH_USERS:
      return { ...state, users: action.data };
    case FETCH_SERVICE_LIST_SUCCESS:
      return { ...state, services: action.data };
    case FETCH_CATEGORY_LIST_SUCCESS:
      return { ...state, categories: action.categoryList };
    default:
      return state;
  }
}

export default newPatientPageReducer;
