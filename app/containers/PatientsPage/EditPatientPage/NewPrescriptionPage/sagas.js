import { put, take, cancel, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';

import {
  REQUEST_ERROR,
  LOAD_PATIENT,
  FETCH_PATIENT,
  CREATE_PRESCRIPTION,
  LOAD_USERS,
  FETCH_USERS,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_FAILURE,
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_FAILURE,
} from './constants';


function* createPrescription(action) {
  try {
    yield callApi(axios.post, '/api/prescriptions', action.payload);
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* newPrescriptionSagaCreate() {
  const create = yield takeLatest(CREATE_PRESCRIPTION, createPrescription);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}


export function* loadPatient(action) {
  try {
    const response = yield callApi(axios.get, `/api/patients/${action.payload}`);

    yield put({ type: FETCH_PATIENT, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* editPatientSagaLoad() {
  const load = yield takeLatest(LOAD_PATIENT, loadPatient);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}


function* loadUsers() {
  try {
    const response = yield callApi(axios.get, '/api/users');

    yield put({ type: FETCH_USERS, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* fetchServiceList() {
  try {
    const serviceResponse = yield callApi(axios.get, '/api/med-services/get/withFullStaff');

    yield put({ type: FETCH_SERVICE_LIST_SUCCESS, data: serviceResponse.data });
    
  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_FAILURE, error: error.message });
  }
}
function* fetchCategoryList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services/categories');
    yield put({ type: FETCH_CATEGORY_LIST_SUCCESS, categoryList: response.data });
  } catch (error) {
    yield put({ type: FETCH_CATEGORY_LIST_FAILURE, error: error.message });
  }
}

function* fetchServiceListSaga() {
  const saga = yield takeLatest(FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}
function* fetchCategoryListSaga() {
  const saga = yield takeLatest(FETCH_CATEGORY_LIST_REQUEST, fetchCategoryList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}
function* LoadUsersSaga() {
  const load = yield takeLatest(LOAD_USERS, loadUsers);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}

// All sagas to be loaded
export default [
  newPrescriptionSagaCreate,
  editPatientSagaLoad,
  LoadUsersSaga,
  fetchServiceListSaga,
  fetchCategoryListSaga,
];
