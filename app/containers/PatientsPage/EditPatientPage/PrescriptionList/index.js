import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { Card } from 'components/styles/Card';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { TimeTable, Table, TR, TD, TH } from 'components/Table';
import moment from 'moment';
import { PRESCRIPTION_ALL, STATUS_NAMES }  from '../../../../../common/constants/statuses';
import { fetchPrescriptionList, fetchAppointmentList } from '../actions';
import messages from '../messages';
import _ from 'lodash';

import { MAIN } from 'constants/routes';
const PRESCRIPTIONS = MAIN.PRESCRIPTIONS.STATUSES;
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { Row } from 'components/styles/Grid';

const THExt = styled(TH)` height: 67px`;
const TDExt = styled(TD)` height: 76px;`;
const ProvidedServices = styled.div`
  font-weight: 600;
  color: #555459;
`;

const TableRow = ({ item, index, patient, onClick, appointments, isCancelledActive, link }) => {
  const status = PRESCRIPTION_ALL.GENERAL.find((status) => item.status && status.value === item.status);
  const financialStatus = PRESCRIPTION_ALL.FINANCIAL.find((status) => item.financialStatus && status.value === item.financialStatus);
  const statusComponent = status && <span>{status.icon} {status.label}</span>;
  const financialStatusComponent = financialStatus && <span>{financialStatus.icon} {financialStatus.label}</span>;
  const appointmentCount = appointments.filter((appointment) => appointment.prescription._id === item._id).length;
  const createdAt = moment(item.createdAt).format('DD.MM.YYYY');
  const serviceMap = (service) => `${service.quantity || '?'}×${service.service ? service.service.name.shortName : '?'}`;
  const providedServicesString = item.providedServices.map(serviceMap).join(' ');
  const prescribedServicesString = item.prescribedServices.map(serviceMap).join(' ');
  return (
    <TR link={link}>
      <TDExt>{patient.id}-{item.id}</TDExt>
      <TDExt>
        <ProvidedServices>
          <span>
            {providedServicesString}
          </span>
        </ProvidedServices>
        <div>
          <i>
            {item.isAsPrescribed ? null : prescribedServicesString}
          </i>
        </div>
      </TDExt>
      <TDExt>{createdAt}</TDExt>
      <TDExt>
        <Row paddingBottom="3px" bold>{statusComponent}</Row>
        <div style={{ paddingLeft: 20 }}>{item.statusChangedAt ? moment(item.statusChangedAt).format('DD.MM.YYYY') : createdAt}</div>
      </TDExt>
      <TDExt>
        <Row paddingBottom="3px" bold>{financialStatusComponent}</Row>
        <div style={{ paddingLeft: 20 }}>{item.financialStatusChangedAt ? moment(item.financialStatusChangedAt).format('DD.MM.YYYY') : createdAt}</div>
      </TDExt>
      {/*{ !isCancelledActive &&*/}
        {/*<TDExt centered>*/}
          {/*{appointmentCount !== 0 ? appointmentCount : '-'}*/}
        {/*</TDExt>*/}
      {/*}*/}
    </TR>
  );
};


class PrescriptionList extends React.Component {
  constructor(props) {
    super(props);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.filterPrescriptions = this.filterPrescriptions.bind(this);
  }

  componentDidMount() {
    this.props.fetchPrescriptionList({ patient: this.props.routeParams.id });
    this.props.fetchAppointmentList({ patient: this.props.routeParams.id });
  }

  handleTabClick(index) {
    const hashes = [
      PRESCRIPTIONS.OPEN,
      PRESCRIPTIONS.COMPLETED,
      PRESCRIPTIONS.CANCELLED
    ];
    this.props.push(`${this.props.location.pathname}${hashes.find((x) => x === index)}`);
  }

  filterPrescriptions(item) {
    const { status } = item;
    switch (this.props.location.hash) {
      case PRESCRIPTIONS.COMPLETED:
        return status === STATUS_NAMES.PROVIDED;
      case PRESCRIPTIONS.CANCELLED:
        return status === STATUS_NAMES.CANCELLED;
      default:
        return status !== STATUS_NAMES.PROVIDED && status !== STATUS_NAMES.CANCELLED;
    }
  }

  render() {
    const { hash } = this.props.location;
    const isCancelledActive = hash === PRESCRIPTIONS.CANCELLED;
    const { formatMessage } = this.context.intl;

    let colWidths = [78, 255, 120, 200];

    return (
      <Card>
        <Tabs theme={TabsThemes.STATUS} onTabClick={this.handleTabClick} value={hash}>
          <Tab value={hash ? PRESCRIPTIONS.OPEN: ''} label={formatMessage(messages.Open)}/>
          <Tab value={PRESCRIPTIONS.COMPLETED} label={formatMessage(messages.Completed)} />
          <Tab value={PRESCRIPTIONS.CANCELLED} label={formatMessage(messages.Cancelled)} />
        </Tabs>
        <TimeTable>
          {colWidths.map((width, index) => <col key={index} width={`${width}px`}/>)}
          <thead>
            <TR>
              <THExt>ID</THExt>
              <THExt>
                <strong>{formatMessage(messages.Services)}</strong>
              </THExt>
              <THExt>{formatMessage(messages.DateIssued)}</THExt>
              <THExt>
                <div style={{ paddingLeft: 20 }}>
                  <div>{formatMessage(messages.Prescription)}</div>
                  <div>{formatMessage(messages.Status)}</div>
                </div>
              </THExt>
              <THExt>
                <div style={{ paddingLeft: 20 }}>
                  <div>{formatMessage(messages.Financial)}</div>
                  <div>{formatMessage(messages.Status)}</div>
                </div>
              </THExt>
              {/*{ !isCancelledActive && <THExt centered>{formatMessage(messages.ScheduledAppointments)}</THExt> }*/}
            </TR>
          </thead>
          <tbody>
            {this.props.prescriptions
              .filter(this.filterPrescriptions)
              .map((item, index) =>
                <TableRow
                  key={index}
                  item={item}
                  link={`${this.props.location.pathname}/${item._id}`}
                  patient={this.props.editPatient}
                  appointments={this.props.appointments}
                  isCancelledActive={isCancelledActive}
                />
              )
            }
          </tbody>
        </TimeTable>
      </Card>
    );
  }
}


PrescriptionList.defaultProps = {
  prescriptions: [],
};

PrescriptionList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
PrescriptionList.propTypes = {
  appointments: PropTypes.array,
  prescriptions: PropTypes.array,
  push: PropTypes.func,
  location: PropTypes.object,
};


function mapStateToProps(state) {
  return {
    prescriptions: state.editPatientPage.prescriptions,
    appointments: state.editPatientPage.appointments,
    editPatient: state.editPatientPage.editPatient,
  };
}


const mapDispatchToProps = {
  push,
  fetchPrescriptionList,
  fetchAppointmentList,
};


export default connect(mapStateToProps, mapDispatchToProps)(PrescriptionList);
