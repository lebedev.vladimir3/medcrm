import { reduxForm, formValueSelector } from 'redux-form';
import PatientForm, { validate } from 'components/PatientForm';
import { connect } from 'react-redux';


let Form = reduxForm({
  form: 'PatientForm',
  validate,
})(PatientForm);


const selector = formValueSelector('PatientForm');


Form = connect(
  state => {
    const birth = selector(state, 'birth');
    const payerStatusValue = selector(state, 'payerStatus');
    return { payerStatusValue, birth, isEdit: true };
  }
)(Form);


export default Form;
