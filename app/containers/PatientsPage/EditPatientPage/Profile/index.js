import React, { PropTypes } from 'react';
import { push } from 'react-router-redux';
import { initialize } from 'redux-form';
import { connect } from 'react-redux';
import Form from './Form';
import { loadPatient, updatePatient, deletePatient } from '../actions';


class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    // Why there are no props.routeParams?
    if (this.props.editPatient._id === this.props.location.pathname.split('/')['3']) {
      this.props.initialize('PatientForm', this.props.editPatient);
    }
  }

  componentWillReceiveProps(nextProps) {
    // Why there are no props.routeParams?
    if (nextProps.editPatient._id === this.props.location.pathname.split('/')['3']) {
      this.props.initialize('PatientForm', nextProps.editPatient);
    }
  }

  handleUpdate(formData) {
    if (this.props.editPatient._id) { // eslint-disable-line no-underscore-dangle
      this.props.updatePatient({
        patients: this.props.patients,
        id: this.props.editPatient._id, // eslint-disable-line no-underscore-dangle
        formData,
      });
      this.props.push('/main/patients');
    }
  }

  handleDelete() {
    if (this.props.editPatient._id) { // eslint-disable-line no-underscore-dangle
      this.props.deletePatient({
        patients: this.props.patients,
        id: this.props.editPatient._id, // eslint-disable-line no-underscore-dangle
      });
    }
  }

  handleCancel() {
    this.props.push('/main/patients');
  }

  render() {
    return (
      <div>
        <Form
          onDelete={this.handleDelete}
          onSubmit={this.handleUpdate}
          onCancel={this.handleCancel}
          locale={this.props.locale}
          payers={this.props.payers}
          isEdit
        />
      </div>
    );
  }
}


Profile.propTypes = {
  editPatient: PropTypes.object,
  location: PropTypes.object,
  initialize: PropTypes.func,
  updatePatient: PropTypes.func.isRequired,
  deletePatient: PropTypes.func.isRequired,
  locale: PropTypes.string,
  payers: PropTypes.array,
  patients: PropTypes.array,
};


Profile.defaultProps = {
  editPatient: {},
};


function mapStateToProps(state) {
  return {
    editPatient: state.editPatientPage.editPatient,
    patients: state.main.patients,
    locale: state.language.locale,
    payers: state.editPatientPage.payers,
  };
}

const mapDispatchToProps = {
  push,
  loadPatient,
  updatePatient,
  deletePatient,
  initialize,
};


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
