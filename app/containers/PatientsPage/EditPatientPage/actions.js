/*
 *
 * EditPatientPage actions
 *
 */

import {
  LOAD_PATIENT,
  EDIT_PAGE_UPDATE_PATIENT,
  EDIT_PAGE_DELETE_PATIENT,
  FETCH_PAYER_LIST_REQUEST,
  FETCH_PRESCRIPTION_REQUEST,
  FETCH_PRESCRIPTION_LIST_REQUEST,
  UPDATE_PRESCRIPTION_REQUEST,
  DELETE_PRESCRIPTION_REQUEST,
  FETCH_APPOINTMENT_REQUEST,
  FETCH_APPOINTMENT_LIST_REQUEST,
  UPDATE_APPOINTMENT_REQUEST,
  UPDATE_APPOINTMENT_LIST_REQUEST,
  DELETE_APPOINTMENT_REQUEST,
  FETCH_USER_LIST_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_ROOM_LIST_REQUEST,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_CATEGORY_LIST_REQUEST,
  CHANGE_PAYER,
  TOGGLE_APPOINTMENT_CHECKED,
  TOGGLE_ALL_APPOINTMENTS_CHECKED,
  HIDE_OVERBOOKING_MODAL,
  SET_MESSAGE,
} from './constants';


export const loadPatient = (id) => ({ type: LOAD_PATIENT, payload: id });

export const updatePatient = ({ patients, id, formData }) => ({ type: EDIT_PAGE_UPDATE_PATIENT, payload: { patients, id, formData } });

export const deletePatient = ({ patients, id }) => ({ type: EDIT_PAGE_DELETE_PATIENT, payload: { patients, id } });

export const fetchPayerList = () => ({ type: FETCH_PAYER_LIST_REQUEST });

export const fetchPrescriptionList = (params) => ({ type: FETCH_PRESCRIPTION_LIST_REQUEST, payload: params });

export const fetchPrescription = (id) => ({ type: FETCH_PRESCRIPTION_REQUEST, payload: id });

export const updatePrescription = (formData) => ({ type: UPDATE_PRESCRIPTION_REQUEST, payload: formData });

export const deletePrescription = (params) => ({ type: DELETE_PRESCRIPTION_REQUEST, payload: params });

export const fetchAppointmentList = (params) => ({ type: FETCH_APPOINTMENT_LIST_REQUEST, payload: params });

export const fetchAppointment = (id) => ({ type: FETCH_APPOINTMENT_REQUEST, payload: id });

export const updateAppointment = (formData) => ({ type: UPDATE_APPOINTMENT_REQUEST, payload: formData });

export const updateAppointmentList = (ids, data) => ({ type: UPDATE_APPOINTMENT_LIST_REQUEST, payload: { ids, data } });

export const deleteAppointment = (params) => ({ type: DELETE_APPOINTMENT_REQUEST, payload: params });

export const changePayer = (payload) => ({ type: CHANGE_PAYER, payload });

export const fetchServiceList = () => ({ type: FETCH_SERVICE_LIST_REQUEST });

export const fetchUserList = () => ({ type: FETCH_USER_LIST_REQUEST });

export const fetchRoleList = () => ({ type: FETCH_ROLE_LIST_REQUEST });

export const fetchEquipmentList = () => ({ type: FETCH_EQUIPMENT_LIST_REQUEST });

export const fetchRoomList = () => ({ type: FETCH_ROOM_LIST_REQUEST });

export const fetchCategoryList = () => ({ type: FETCH_CATEGORY_LIST_REQUEST });

export const toggleAppointmentChecked = (id) => ({ type: TOGGLE_APPOINTMENT_CHECKED, payload: id });

export const setAppointmentsChecked = (ids, value) => ({ type: TOGGLE_ALL_APPOINTMENTS_CHECKED, payload: { ids, value } });

export const hideOverbookingModal = (ids, value) => ({ type: HIDE_OVERBOOKING_MODAL });

export const setMessage = (message) => ({ type: SET_MESSAGE, payload: message });
