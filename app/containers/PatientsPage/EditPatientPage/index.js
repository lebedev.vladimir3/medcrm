import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import Helmet from 'react-helmet';
import { push } from 'react-router-redux';
import FormLegend from 'components/FormLegend';

import AppointmentsIcon from 'assets/icons/MainTab/Green/Appointments.svg';
import PDetailsIcon from 'assets/icons/MainTab/Green/P.Details.svg';
import PrescriptionIcon from 'assets/icons/MainTab/Green/Prescriptions.svg';

import styled from 'styled-components';
import Patients from 'assets/icons/Title/Patient_Profile.svg';

import { STATUS_NAMES }  from '../../../../common/constants/statuses';
import Calendar from 'components/WorkingHoursCalendar';

import { MAIN } from 'constants/routes';
const PATIENTS = MAIN.PATIENTS;

import {
  StyledCard, Row,
  RightPart, Divider,
} from 'components/styles/Card/Mini';

import { BlueButton, SecondaryButton } from 'components/common/Button';
import BlueArrowDown from 'assets/icons/Arrows/Select/Blue.svg'

import { Tabs, Tab } from 'components/common/Tabs';
import {
  loadPatient,
  updatePatient,
  deletePatient,
  fetchPayerList,
  fetchPrescriptionList,
  fetchAppointmentList,
  setMessage,
} from './actions';
import MiniCard from '../components/MiniCard';
import MiniCardPrescription from '../components/MiniCardPrescription';
import PrescriptionsInfoMiniCard from '../components/PrescriptionsInfoMiniCard';
import NextAppointmentCard from '../components/NextAppointmentCard';
import { TableContent, RightSide, LeftSide, PaddingTop } from 'components/styles/Grid';

import { PrescriptionStatusForm } from './EditPrescription/Form';
import { AppointmentStatusForm } from './EditAppointment/Form';
import messages from '../messages';
import Dropdown from 'components/common/Dropdown';
import { DropdownStyledLink } from "../../../components/styles/common";

const ButtonWrapper = styled.div.attrs({
  className: 'button-wrap'
})`
  width: 100%;
  //overflow: auto;
  margin-bottom: 17px;
`;

class EditPatientPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.getCurrentRoute = this.getCurrentRoute.bind(this);
    this.getSelectedTab = this.getSelectedTab.bind(this);
  }

  componentWillMount() {
    const splittedPathname = window.location.pathname.split('/');
    this.props.loadPatient(this.props.params.id);
    if (splittedPathname.length === 4 || splittedPathname.length === 6 || splittedPathname.includes('appointment-finder')) {
      this.props.fetchPrescriptionList({ patient: this.props.routeParams.id });
    }
    if (splittedPathname.length === 4 || splittedPathname.length === 6) {
      this.props.fetchAppointmentList({ patient: this.props.routeParams.id });
    }

    this.props.fetchPayerList();

    const { prescriptions } = this.props;
    const patientHasPrescriptions = prescriptions.length > 0;

    if (patientHasPrescriptions) {
      if (patientHasPrescriptions) {
        this.props.setMessage({
          type: 'Note',
          text: this.context.intl.formatMessage(messages.UnscheduledFinderAvailable),
        });
      } else {
        this.props.setMessage({
          type: 'Hint',
          text: this.context.intl.formatMessage(messages.AppointmentsWithoutPrescr),
        });
      }
    }
  }

  componentWillUnmount() {
    this.props.setMessage(null);
  }

  handleTabClick(key) {
    const { prescriptions } = this.props;
    const patientHasPrescriptions = prescriptions.length > 0;

    const urlBegin = `/main/patients/${this.props.routeParams.id}`;
    switch (key) {
      case PATIENTS.HOME:
        this.props.push(urlBegin);
        break;
      case PATIENTS.PRESCRIPTIONS:
        let targetRoute = patientHasPrescriptions ?
          PATIENTS.PRESCRIPTIONS : PATIENTS.NEW_PRESCRIPTION;
        this.props.push(`${urlBegin}/${targetRoute}`);
        break;
      case PATIENTS.APPOINTMENTS:
        this.props.push(`${urlBegin}/${PATIENTS.APPOINTMENTS}`);
        break;
      default:
        this.props.push(urlBegin);
    }
  }

  getSelectedTab() {
    switch (this.props.location.pathname.split('/')[4]) {
      case PATIENTS.PRESCRIPTIONS:
        return PATIENTS.PRESCRIPTIONS;
      case PATIENTS.NEW_PRESCRIPTION:
        return PATIENTS.PRESCRIPTIONS;
      case PATIENTS.APPOINTMENTS:
        return PATIENTS.APPOINTMENTS;
      case PATIENTS.APPOINTMENT_FINDER:
        return PATIENTS.APPOINTMENTS;
      default:
        return PATIENTS.HOME;
    }
  }

  getCurrentRoute() {
    const { router } = this.props;
    if (this.props.route) {
      console.log(this.props.route)
      const currentRoute = this.props.route.childRoutes.filter((route) => route.path && router.isActive(this.props.location, true));
      return currentRoute[0];
    }
    return null;
  }

  render() {
    const {
      patient,
      prescriptions,
      prescription,
      appointment,
      appointments,
      location,
      message,
    } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const firstname = patient.firstName;
    const lastname = patient.lastName;
    const selectedTab = this.getSelectedTab();

    const pathnameComponents = location.pathname.split('/');
    const isPrescriptionCard = prescription._id && pathnameComponents[5] === prescription._id;
    const isAppointmentCard = appointment._id && pathnameComponents[5] === appointment._id;
    const isAppointmentList = pathnameComponents[4] === 'appointments' && !pathnameComponents[5];
    const isPrescriptionsList = pathnameComponents[4] === 'prescriptions';
    const isAppointmentFinder = pathnameComponents[4] === 'appointment-finder';
    const isNewPrescription = pathnameComponents[4] === 'new-prescription';
    const isPersonalDetails = pathnameComponents[4] === undefined;

    const currentPrescription = isAppointmentCard ? appointment.prescription : prescription;
    const prescriptionReadableId = isPrescriptionCard ? `${patient.id}-${currentPrescription.id}` : null;
    const patientHasPrescriptions = prescriptions.length > 0;
    const isCancelledPrescription = prescription.status === STATUS_NAMES.CANCELLED;
    const isCancelledAppointment = appointment.status === STATUS_NAMES.CANCELLED;

    return (
      <PaddingTop>
        <Helmet title="MedORG - Patient Profile" />
        <div>
          <TableContent>
            <LeftSide>
              <FormLegend
                Icon={Patients}
                text={formatMessage(messages.PatientProfile)}
                secondText={patient && patient.title && lastname && firstname ? (patient.title.personal +
                  ' ' + lastname + ',  ' + firstname) : ''}
                iconColor="#00896D"
              />
              <Tabs onTabClick={this.handleTabClick} value={selectedTab}>
                <Tab
                  value={PATIENTS.HOME}
                  icon={<PDetailsIcon />}
                  label={formatMessage(messages.PersonalDetails)}
                />
                <Tab
                  value={PATIENTS.PRESCRIPTIONS}
                  icon={<PrescriptionIcon />}
                  label={formatMessage(messages.Prescriptions)}
                />
                <Tab
                  value={PATIENTS.APPOINTMENTS}
                  disabled={!patientHasPrescriptions}
                  icon={<AppointmentsIcon />}
                  label={formatMessage(messages.Appointments)}
                />
              </Tabs>
              {this.props.children}
            </LeftSide>
            <RightSide>
              {  (isAppointmentList || isAppointmentFinder || isAppointmentCard) &&
              <ButtonWrapper>
                <SecondaryButton
                  fullWidth
                  link={`/main/patients/${this.props.routeParams.id}/appointment-finder`}
                  disabled={isAppointmentFinder}
                >
                  {formatMessage(messages.AppointmentScheduler)}
                </SecondaryButton>
              </ButtonWrapper>
              }
              {  (isPersonalDetails) &&
              <ButtonWrapper style={{visibility:'hidden'}}>
                <SecondaryButton fullWidth>
                  {formatMessage(messages.AppointmentScheduler)}
                </SecondaryButton>
              </ButtonWrapper>
              }
              {(isNewPrescription || isPrescriptionCard || isPrescriptionsList) &&
              <ButtonWrapper>
                <Dropdown
                  menuStyle={{
                    top: '35px',
                    right: '9px',
                    width: '260px'
                  }}
                  optionStyle={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  options={[
                    <DropdownStyledLink
                      to={`/main/patients/${this.props.routeParams.id}/new-prescription/physiotherapie`}
                    >
                      {formatMessage(messages.Physiotherpie)}
                    </DropdownStyledLink>,
                    <DropdownStyledLink to={`/main/patients/${this.props.routeParams.id}/new-prescription/ergotherapie`}>
                      {formatMessage(messages.Ergotherapie)}
                    </DropdownStyledLink>
                  ]}
                >
                  <SecondaryButton
                    fullWidth
                    //link={`/main/patients/${this.props.routeParams.id}/new-prescription`}
                    //disabled={isNewPrescription}
                  >
                    <span style={{ marginRight: '6px' }}>{formatMessage(messages.NewPrescription)}</span>
                    <BlueArrowDown />
                  </SecondaryButton>
                </Dropdown>
              </ButtonWrapper>
              }
              {isPrescriptionCard &&
                <PrescriptionStatusForm
                  id={prescriptionReadableId}
                  isCancelled={isCancelledPrescription}
                  prescription={prescription}
                />
              }
              {isAppointmentCard &&
                <AppointmentStatusForm
                  id={`${patient.id}-${currentPrescription.id}/${appointment.id}`}
                  isCancelled={isCancelledAppointment}
                  showSpecificContent
                />
              }
              {isAppointmentCard &&
              <PrescriptionStatusForm
                showFinancial={false}
                id={`${patient.id}-${currentPrescription.id}/${appointment.id}`}
                disableGeneral
                appointment={appointment}
                isAppointmentCard
                showSpecificContent
              />}
              {isPersonalDetails && <MiniCard patient={patient} prescriptions={prescriptions} appointments={appointments} />}
              { (isAppointmentCard || isPrescriptionCard || isPrescriptionsList || isAppointmentFinder || isNewPrescription || isAppointmentList) &&
              <MiniCardPrescription
                patient={patient}
                showSZ={this.props.selectedPayer !== patient.payerStatus}
                style={{marginTop: isPersonalDetails ? '200px' : '0px'}}
              />
              }
              {(isAppointmentList || isPersonalDetails) && <PrescriptionsInfoMiniCard prescriptions={prescriptions} />}
              {(isPrescriptionsList || isPersonalDetails || isPrescriptionCard) && <NextAppointmentCard appointments={appointments} />}
              {!isPrescriptionCard && !isPrescriptionsList && !isPersonalDetails && !isAppointmentCard &&
              <Calendar/>
              }
            </RightSide>
          </TableContent>
        </div>
      </PaddingTop>
    );
  }
}

EditPatientPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EditPatientPage.propTypes = {
  push: PropTypes.func.isRequired,
  loadPatient: PropTypes.func.isRequired,
  patient: PropTypes.object,
  prescription: PropTypes.object,
  params: PropTypes.objectOf(PropTypes.string),
  fetchPrescriptionList: PropTypes.func,
  fetchPayerList: PropTypes.func,
  fetchAppointmentList: PropTypes.func,
  children: PropTypes.any,
  prescriptions: PropTypes.array,
  location: PropTypes.object,
  setMessage: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    patient: state.editPatientPage.editPatient,
    prescription: state.editPatientPage.editPrescription,
    appointment: state.editPatientPage.editAppointment,
    prescriptions: state.editPatientPage.prescriptions,
    selectedPayer: state.editPatientPage.selectedPayer,
    appointments: state.editPatientPage.appointments,
    locale: state.language.locale,
    payers: state.editPatientPage.payers,
    patients: state.main.patients,
    message: state.editPatientPage.message,
    company: state.main.company,
  };
}

const mapDispatchToProps = {
  push,
  loadPatient,
  updatePatient,
  deletePatient,
  fetchPayerList,
  fetchPrescriptionList,
  fetchAppointmentList,
  initialize,
  setMessage,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditPatientPage);
