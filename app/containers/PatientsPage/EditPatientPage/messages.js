import { defineMessages } from 'react-intl';

export default defineMessages({
  NextAppointment: {
    id: 'EditPatientsPage.NextAppointment',
    defaultMessage: 'Next Appointment',
  },
  MayCause: {
    id: 'EditPatientsPage.MayCause',
    defaultMessage: 'Updating the scheduled appointment may cause overbooking!',
  },
  CombinedTreatment: {
    id: 'EditPatientsPage.CombinedTreatment',
    defaultMessage: 'Combined Treatment Reports are available within the appropriate tab of the related Prescription Card',
  },
  UpdatingScheduled: {
    id: 'EditPatientsPage.UpdatingScheduled',
    defaultMessage: 'Updating the scheduled appointment may cause overbooking!',
  },
  Pending: {
    id: 'Pending',
    defaultMessage: 'Pending',
  },
  DeleteAppointment: {
    id: 'DeleteAppointment',
    defaultMessage: 'Delete Appointment',
  },
  CancelAppointment: {
    id: 'CancelAppointment',
    defaultMessage: 'Cancel Appointment',
  },
  DeletePrescription: {
    id: 'DeletePrescription',
    defaultMessage: 'Delete Prescription',
  },
  CancelPrescription: {
    id: 'CancelPrescription',
    defaultMessage: 'Cancel Prescription',
  },
  QuestPrescription: {
    id: 'QuestPrescription',
    defaultMessage: 'Are you sure you want to cancel this prescription?',
  },
  QuestAppointment: {
    id: 'QuestAppointment',
    defaultMessage: 'Are you sure you want to cancel this appointment?',
  },
  Done: {
    id: 'Done',
    defaultMessage: 'Done',
  },
  Cancelled: {
    id: 'Cancelled',
    defaultMessage: 'Cancelled',
  },
  Archived: {
    id: 'Archived',
    defaultMessage: 'Archived',
  },
  MarkAsPaid: {
    id: 'MarkAsPaid',
    defaultMessage: 'Mark as Paid',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  PrescriptionCard: {
    id: 'PrescriptionCard',
    defaultMessage: 'Prescription Card',
  },
  MedicalServices: {
    id: 'MedicalServices',
    defaultMessage: 'Medical Services',
  },
  Services: {
    id: 'Services',
    defaultMessage: 'Services',
  },
  Reports: {
    id: 'Reports',
    defaultMessage: 'Reports',
  },
  TreatmentReports: {
    id: 'TreatmentReports',
    defaultMessage: 'Treatment Reports',
  },
  Open: {
    id: 'Open',
    defaultMessage: 'Open',
  },
  Completed: {
    id: 'Completed',
    defaultMessage: 'Completed',
  },
  DateIssued: {
    id: 'DateIssued',
    defaultMessage: 'Date Issued',
  },
  Status: {
    id: 'Status',
    defaultMessage: 'Status',
  },
  ScheduledAppointments: {
    id: 'ScheduledAppointments',
    defaultMessage: 'Scheduled Appointments',
  },
  Patients: {
    id: 'Patients',
    defaultMessage: 'Patients',
  },
  PatientsDirectory: {
    id: 'PatientsDirectory',
    defaultMessage: 'Patients Directory',
  },
  Appointments: {
    id: 'Appointments',
    defaultMessage: 'Appointments',
  },
  NewPatient: {
    id: 'NewPatient',
    defaultMessage: 'New Patient',
  },
  AppointmentScheduler: {
    id: 'AppointmentScheduler',
    defaultMessage: 'Appointment Scheduler',
  },
  Financial: {
    id: 'Financial',
    defaultMessage: 'Financial',
  },
  Continue: {
    id: 'Continue',
    defaultMessage: 'Continue',
  },
  Prescription: {
    id: 'Prescription',
    defaultMessage: 'Prescription',
  },
  ProvidedServices: {
    id: 'ProvidedServices',
    defaultMessage: 'Provided Services',
  },
  PrescribedServices: {
    id: 'PrescribedServices',
    defaultMessage: 'Prescribed Services',
  },
  AppointmentID: {
    id: 'AppointmentID',
    defaultMessage: 'Appointment ID ',
  },
  PrescriptionID: {
    id: 'PrescriptionID',
    defaultMessage: 'Prescription ID ',
  },

});
