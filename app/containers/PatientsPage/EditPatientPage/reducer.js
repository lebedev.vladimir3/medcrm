/*
 *
 * EditPatientPage reducer
 *
 */

import {
  FETCH_PATIENT,
  FETCH_PAYER_LIST_SUCCESS,
  // FETCH_PAYER_LIST_REQUEST,
  // FETCH_PAYER_LIST_FAILURE,
  FETCH_PRESCRIPTION_SUCCESS,
  FETCH_PRESCRIPTION_REQUEST,
  FETCH_PRESCRIPTION_FAILURE,
  FETCH_PRESCRIPTION_LIST_SUCCESS,
  FETCH_PRESCRIPTION_LIST_REQUEST,
  FETCH_PRESCRIPTION_LIST_FAILURE,
  FETCH_APPOINTMENT_SUCCESS,
  // FETCH_APPOINTMENT_REQUEST,
  // FETCH_APPOINTMENT_FAILURE,
  FETCH_APPOINTMENT_LIST_SUCCESS,
  FETCH_APPOINTMENT_LIST_REQUEST,
  FETCH_APPOINTMENT_LIST_FAILURE,
  FETCH_USER_LIST_SUCCESS,
  FETCH_USER_LIST_REQUEST,
  FETCH_USER_LIST_FAILURE,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_FAILURE,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_EQUIPMENT_LIST_FAILURE,
  FETCH_ROOM_LIST_SUCCESS,
  FETCH_ROOM_LIST_REQUEST,
  FETCH_ROOM_LIST_FAILURE,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_FAILURE,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_CATEGORY_LIST_FAILURE,
  UPDATE_APPOINTMENT_FAILURE,
  TOGGLE_APPOINTMENT_CHECKED,
  TOGGLE_ALL_APPOINTMENTS_CHECKED,
  UPDATE_APPOINTMENT_LIST_SUCCESS,
  CHANGE_PAYER,
  SHOW_OVERBOOKING_MODAL,
  HIDE_OVERBOOKING_MODAL,
  SET_MESSAGE,
} from './constants';

const initialState = {
  editPatient: {},
  editPrescription: {},
  editAppointment: {},
  payers: [],
  prescriptions: [],
  appointments: [],
  services: [],
  categories: [],
  users: [],
  pending: {
    editPatient: false,
    editPrescription: false,
    editAppointment: false,
    payers: false,
    prescriptions: false,
    appointments: false,
    services: false,
    categories: false,
    users: false,
  },
  selectedPayer: {},
  overbookingModal: {
    isOpen: false,
    patientIsVacant: true,
    staffIsVacant: true,
    roomIsVacant: true,
  },
  showOverbookingModal: false,
  message: null,
};

function editPatientPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PATIENT:
      return { ...state, pending: { ...state.pending, editPatient: false }, editPatient: action.payload };
    case FETCH_PAYER_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, payers: false }, payers: action.payload };
    case FETCH_PRESCRIPTION_SUCCESS:
      return { ...state, pending: { ...state.pending, editPrescription: false }, editPrescription: action.payload };
    case FETCH_PRESCRIPTION_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, prescriptions: false }, prescriptions: action.payload };
    case FETCH_APPOINTMENT_SUCCESS:
      return { ...state, pending: { ...state.pending, editAppointment: false }, editAppointment: action.payload };
    case FETCH_APPOINTMENT_LIST_SUCCESS:
      return {
        ...state,
        appointments: action.payload.map((item) => ({ ...item, checked: false })),
        pending: { ...state.pending, appointments: false },
      };
    case UPDATE_APPOINTMENT_FAILURE:
      return { ...state, overbookingModal: { ...state.overbookingModal, isOpen: false } };
    case UPDATE_APPOINTMENT_LIST_SUCCESS:
      return {
        ...state,
        appointments: state.appointments.map((item) => {
          const newItem = { ...item };
          if (action.payload.ids.indexOf(item._id) > -1) {
            return { ...newItem, ...action.payload.data };
          }
          return { ...newItem };
        }),
      };
    case CHANGE_PAYER:
      return { ...state, payerStatus: action.payload };
    case FETCH_USER_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, users: false }, users: action.payload };
    case FETCH_ROLE_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, roles: false }, roles: action.payload };
    case FETCH_EQUIPMENT_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, equipmentList: false }, equipmentList: action.payload };
    case FETCH_ROOM_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, rooms: false }, rooms: action.payload };
    case FETCH_SERVICE_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, services: false }, services: action.payload };
    case FETCH_CATEGORY_LIST_SUCCESS:
      return { ...state, pending: { ...state.pending, categories: false }, serviceCategories: action.payload };
    case TOGGLE_APPOINTMENT_CHECKED:
      const index = state.appointments.findIndex((item) => item._id === action.payload);
      const newAppointments = [...state.appointments];
      newAppointments[index].checked = !state.appointments[index].checked;
      return {
        ...state,
        appointments: newAppointments,
      };
    case TOGGLE_ALL_APPOINTMENTS_CHECKED:
      return {
        ...state,
        appointments: state.appointments.map((item) => {
          if (action.payload.ids.indexOf(item._id) > -1) {
            return { ...item, checked: action.payload.value };
          }
          return item;
        }),
      };
    case SHOW_OVERBOOKING_MODAL:
      return {
        ...state,
        overbookingModal: {
          isOpen: true,
          patientIsVacant: action.payload.patientIsVacant,
          staffIsVacant: action.payload.staffIsVacant,
          roomIsVacant: action.payload.roomIsVacant,
        },
      };
    case HIDE_OVERBOOKING_MODAL:
      return { ...state, overbookingModal: { ...state.overbookingModal, isOpen: false } };
    case FETCH_PRESCRIPTION_REQUEST:
      return { ...state, pending: { ...state.pending, editPrescription: true } };
    case FETCH_PRESCRIPTION_FAILURE:
      return { ...state, pending: { ...state.pending, editPrescription: false } };
    case FETCH_PRESCRIPTION_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, prescriptions: true } };
    case FETCH_PRESCRIPTION_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, prescriptions: false } };
    case FETCH_APPOINTMENT_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, appointments: true } };
    case FETCH_APPOINTMENT_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, appointments: false } };
    case FETCH_USER_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, users: true } };
    case FETCH_USER_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, users: false } };
    case FETCH_ROLE_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, roles: true } };
    case FETCH_ROLE_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, roles: false } };
    case FETCH_EQUIPMENT_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, equipmentList: true } };
    case FETCH_EQUIPMENT_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, equipmentList: false } };
    case FETCH_ROOM_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, rooms: true } };
    case FETCH_ROOM_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, rooms: false } };
    case FETCH_SERVICE_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, services: true } };
    case FETCH_SERVICE_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, services: false } };
    case FETCH_CATEGORY_LIST_REQUEST:
      return { ...state, pending: { ...state.pending, categories: true } };
    case FETCH_CATEGORY_LIST_FAILURE:
      return { ...state, pending: { ...state.pending, categories: false } };
    case SET_MESSAGE:
      return { ...state, message: action.payload };
    default:
      return state;
  }
}

export default editPatientPageReducer;
