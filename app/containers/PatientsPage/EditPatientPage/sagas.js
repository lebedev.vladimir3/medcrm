/*
 *
 * EditPatientPage sagas
 *
 */

import { put, fork, take, cancel, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';

import {
  REQUEST_ERROR,
  FETCH_PATIENT,
  LOAD_PATIENT,
  EDIT_PAGE_UPDATE_PATIENT,
  EDIT_PAGE_DELETE_PATIENT,
  UPDATE_PATIENT,
  DELETE_PATIENT,
  FETCH_PAYER_LIST_REQUEST,
  FETCH_PAYER_LIST_SUCCESS,
  FETCH_PAYER_LIST_FAILURE,
  FETCH_PRESCRIPTION_LIST_REQUEST,
  FETCH_PRESCRIPTION_LIST_SUCCESS,
  FETCH_PRESCRIPTION_LIST_FAILURE,
  FETCH_PRESCRIPTION_REQUEST,
  FETCH_PRESCRIPTION_SUCCESS,
  FETCH_PRESCRIPTION_FAILURE,
  UPDATE_PRESCRIPTION_REQUEST,
  UPDATE_PRESCRIPTION_SUCCESS,
  UPDATE_PRESCRIPTION_FAILURE,
  DELETE_PRESCRIPTION_REQUEST,
  DELETE_PRESCRIPTION_SUCCESS,
  DELETE_PRESCRIPTION_FAILURE,
  FETCH_APPOINTMENT_LIST_REQUEST,
  FETCH_APPOINTMENT_LIST_SUCCESS,
  FETCH_APPOINTMENT_LIST_FAILURE,
  FETCH_APPOINTMENT_REQUEST,
  FETCH_APPOINTMENT_SUCCESS,
  FETCH_APPOINTMENT_FAILURE,
  UPDATE_APPOINTMENT_REQUEST,
  UPDATE_APPOINTMENT_SUCCESS,
  UPDATE_APPOINTMENT_FAILURE,
  UPDATE_APPOINTMENT_LIST_REQUEST,
  UPDATE_APPOINTMENT_LIST_SUCCESS,
  UPDATE_APPOINTMENT_LIST_FAILURE,
  DELETE_APPOINTMENT_REQUEST,
  DELETE_APPOINTMENT_SUCCESS,
  DELETE_APPOINTMENT_FAILURE,
  FETCH_USER_LIST_REQUEST,
  FETCH_USER_LIST_SUCCESS,
  FETCH_USER_LIST_FAILURE,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_FAILURE,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_FAILURE,
  FETCH_CATEGORY_LIST_REQUEST,
  FETCH_CATEGORY_LIST_SUCCESS,
  FETCH_CATEGORY_LIST_FAILURE,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_EQUIPMENT_LIST_FAILURE,
  FETCH_ROOM_LIST_REQUEST,
  FETCH_ROOM_LIST_SUCCESS,
  FETCH_ROOM_LIST_FAILURE,
  SHOW_OVERBOOKING_MODAL,
} from './constants';

const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';


function* loadPatient(action) {
  const id = action.payload;
  try {
    const response = yield callApi(axios.get, `/api/patients/${id}`);
    yield put({ type: FETCH_PATIENT, payload: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* updatePatient(action) {
  try {
    const { patients, id, formData } = action.payload;
    const { data } = yield callApi(axios.put, `/api/patients/${id}`, formData);
    yield put({ type: FETCH_PATIENT, data });
    const newPatientsArray = patients.filter((patient) => patient._id !== id);
    newPatientsArray.push(data);
    yield put({ type: UPDATE_PATIENT, payload: newPatientsArray });
    browserHistory.push('/main/patients');
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* deletePatient(action) {
  try {
    const { patients, id } = action.payload;
    yield callApi(axios.delete, `/api/patients/${id}`);
    yield put({ type: FETCH_PATIENT, payload: {} });
    const newPatientsArray = patients.filter((patient) => patient._id !== id);
    yield put({ type: DELETE_PATIENT, data: newPatientsArray });
    browserHistory.push('/main/patients');
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* fetchPayerList() {
  try {
    const response = yield callApi(axios.get, '/api/payers');
    yield put({ type: FETCH_PAYER_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PAYER_LIST_FAILURE, error: error.message });
  }
}

function* updatePrescription(action) {
  try {
    const response = yield callApi(axios.put, `/api/prescriptions/${action.payload._id}`, action.payload);
    yield put({ type: UPDATE_PRESCRIPTION_SUCCESS, payload: response.data });
    browserHistory.push(`/main/patients/${action.payload.patient}/prescriptions`);
  } catch (error) {
    yield put({ type: UPDATE_PRESCRIPTION_FAILURE, error: error.message });
  }
}

function* fetchPrescription(action) {
  try {
    const response = yield callApi(axios.get, `/api/prescriptions/${action.payload}`);
    yield put({ type: FETCH_PRESCRIPTION_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PRESCRIPTION_FAILURE, error: error.message });
  }
}

function* fetchPrescriptionList(action) {
  try {
    const response = yield callApi(axios.get, '/api/prescriptions', { params: action.payload });
    yield put({ type: FETCH_PRESCRIPTION_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PRESCRIPTION_LIST_FAILURE, error: error.message });
  }
}

function* deletePrescription(action) {
  try {
    const response = yield callApi(axios.delete, `/api/prescriptions/${action.payload._id}`);
    yield put({ type: DELETE_PRESCRIPTION_SUCCESS, payload: response.data });
    browserHistory.push(`/main/patients/${action.payload.patient}/prescriptions`);
  } catch (error) {
    yield put({ type: DELETE_PRESCRIPTION_FAILURE, error: error.message });
  }
}

function* updateAppointmentList(action) {
  try {
    const response = yield callApi(axios.put, '/api/appointments/', action.payload);
    yield put({ type: UPDATE_APPOINTMENT_LIST_SUCCESS, payload: action.payload });
  } catch (error) {
    yield put({ type: UPDATE_APPOINTMENT_LIST_FAILURE, error: error.message });
  }
}

function* updateAppointment(action) {
  try {
    const response = yield callApi(axios.put, `/api/appointments/${action.payload._id}`, action.payload);
    if (response.data.overbooking) {
      yield put({ type: SHOW_OVERBOOKING_MODAL, payload: response.data });
    } else {
      yield put({ type: UPDATE_APPOINTMENT_SUCCESS, payload: response.data });
      browserHistory.push(`/main/patients/${action.payload.prescription.patient}/appointments`);
    }
  } catch (error) {
    yield put({ type: UPDATE_APPOINTMENT_FAILURE, error: error.message });
  }
}

function* fetchAppointment(action) {
  try {
    const response = yield callApi(axios.get, `/api/appointments/${action.payload}`);
    yield put({ type: FETCH_APPOINTMENT_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_APPOINTMENT_FAILURE, error: error.message });
  }
}

function* fetchAppointmentList(action) {
  try {
    const response = yield callApi(axios.get, '/api/appointments', { params: action.payload });
    yield put({ type: FETCH_APPOINTMENT_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_APPOINTMENT_LIST_FAILURE, error: error.message });
  }
}

function* deleteAppointment(action) {
  try {
    const response = yield callApi(axios.delete, `/api/appointments/${action.payload.appointmentId}`);
    yield put({ type: DELETE_APPOINTMENT_SUCCESS, payload: response.data });
    browserHistory.push(`/main/patients/${action.payload.patientId}/appointments`);
  } catch (error) {
    yield put({ type: DELETE_APPOINTMENT_FAILURE, error: error.message });
  }
}

function* fetchUserList() {
  try {
    const response = yield callApi(axios.get, '/api/users');
    yield put({ type: FETCH_USER_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_USER_LIST_FAILURE, error: error.message });
  }
}

function* fetchRoleList() {
  try {
    const response = yield callApi(axios.get, '/api/roles');
    yield put({ type: FETCH_ROLE_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_FAILURE, error: error.message });
  }
}

function* fetchEquipmentList() {
  try {
    const response = yield callApi(axios.get, '/api/equipment');
    yield put({ type: FETCH_EQUIPMENT_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENT_LIST_FAILURE, error: error.message });
  }
}

function* fetchRoomList() {
  try {
    const response = yield callApi(axios.get, '/api/rooms');
    yield put({ type: FETCH_ROOM_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROOM_LIST_FAILURE, error: error.message });
  }
}

function* fetchServiceList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services/get/withFullStaff');
    yield put({ type: FETCH_SERVICE_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_FAILURE, error: error.message });
  }
}

function* fetchCategoryList() {
  try {
    const response = yield callApi(axios.get, '/api/med-services/categories');
    yield put({ type: FETCH_CATEGORY_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_CATEGORY_LIST_FAILURE, error: error.message });
  }
}

// Watchers
function* editPatientSagaUpdate() {
  const watcher = yield fork(takeLatest, EDIT_PAGE_UPDATE_PATIENT, updatePatient);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* editPatientSagaDelete() {
  const watcher = yield fork(takeLatest, EDIT_PAGE_DELETE_PATIENT, deletePatient);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* editPatientSagaLoad() {
  const watcher = yield fork(takeLatest, LOAD_PATIENT, loadPatient);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchPayerListSaga() {
  const watcher = yield fork(takeLatest, FETCH_PAYER_LIST_REQUEST, fetchPayerList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchPrescriptionSaga() {
  const watcher = yield fork(takeLatest, FETCH_PRESCRIPTION_REQUEST, fetchPrescription);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchPrescriptionListSaga() {
  const watcher = yield fork(takeLatest, FETCH_PRESCRIPTION_LIST_REQUEST, fetchPrescriptionList);
  //yield take(LOCATION_CHANGE);
  //yield cancel(watcher);
}

function* updatePrescriptionSaga() {
  const watcher = yield fork(takeLatest, UPDATE_PRESCRIPTION_REQUEST, updatePrescription);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* deletePrescriptionSaga() {
  const watcher = yield fork(takeLatest, DELETE_PRESCRIPTION_REQUEST, deletePrescription);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchAppointmentSaga() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENT_REQUEST, fetchAppointment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchAppointmentListSaga() {
  const watcher = yield fork(takeLatest, FETCH_APPOINTMENT_LIST_REQUEST, fetchAppointmentList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* updateAppointmentSaga() {
  const watcher = yield fork(takeLatest, UPDATE_APPOINTMENT_REQUEST, updateAppointment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* updateAppointmentListSaga() {
  const watcher = yield fork(takeLatest, UPDATE_APPOINTMENT_LIST_REQUEST, updateAppointmentList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* deleteAppointmentSaga() {
  const watcher = yield fork(takeLatest, DELETE_APPOINTMENT_REQUEST, deleteAppointment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchUserListSaga() {
  const watcher = yield fork(takeLatest, FETCH_USER_LIST_REQUEST, fetchUserList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchRoleListSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROLE_LIST_REQUEST, fetchRoleList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchServiceListSaga() {
  const watcher = yield fork(takeLatest, FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchCategoryListSaga() {
  const watcher = yield fork(takeLatest, FETCH_CATEGORY_LIST_REQUEST, fetchCategoryList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchEquipmentListSaga() {
  const watcher = yield fork(takeLatest, FETCH_EQUIPMENT_LIST_REQUEST, fetchEquipmentList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchRoomListSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROOM_LIST_REQUEST, fetchRoomList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}


export default [
  editPatientSagaUpdate,
  editPatientSagaDelete,
  editPatientSagaLoad,
  fetchPayerListSaga,
  fetchPrescriptionSaga,
  fetchPrescriptionListSaga,
  updatePrescriptionSaga,
  deletePrescriptionSaga,
  fetchAppointmentSaga,
  fetchAppointmentListSaga,
  updateAppointmentSaga,
  updateAppointmentListSaga,
  deleteAppointmentSaga,
  fetchUserListSaga,
  fetchRoleListSaga,
  fetchServiceListSaga,
  fetchCategoryListSaga,
  fetchEquipmentListSaga,
  fetchRoomListSaga,
];
