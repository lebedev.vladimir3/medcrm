/*
 *
 * NewPatientPage actions
 *
 */

import { CREATE_PATIENT, FETCH_PAYER_LIST_REQUEST } from './constants';


export const createPatient = (data) => ({ type: CREATE_PATIENT, payload: data });

export const fetchPayerList = () => ({ type: FETCH_PAYER_LIST_REQUEST });