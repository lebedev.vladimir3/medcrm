/*
 *
 * NewPatientPage constants
 *
 */
export { ADD_NEW_PATIENT } from 'containers/MainContainer/constants';

export const CREATE_PATIENT = 'app/NewPatientPage/CREATE_PATIENT';
export const REQUEST_ERROR = 'app/NewPatientPage/REQUEST_ERROR';
// export const FETCH_PATIENT = 'app/NewPatientPage/FETCH_PATIENT';


export const FETCH_PAYER_LIST_REQUEST = 'app/NewPatientPage/FETCH_PAYER_LIST_REQUEST';
export const FETCH_PAYER_LIST_SUCCESS = 'app/NewPatientPage/FETCH_PAYER_LIST_SUCCESS';
export const FETCH_PAYER_LIST_FAILURE = 'app/NewPatientPage/FETCH_PAYER_LIST_FAILURE';

