/*
 *
 * NewPatientPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import FormLegend from 'components/FormLegend';
import PersonAdd from 'assets/icons/Title/New_Patient.svg';
import MenuIcon from 'react-icons/lib/ti/th-large-outline';
import { FormWrapper } from 'components/styles/Grid';

import Form from './Form';
import { createPatient } from './actions';
import { fetchPayerList } from './actions';
import messages from '../messages';
import { personal, honorifics } from './../../../constants/users';

class NewPatientPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(fetchPayerList());
    this.props.initializeNewPatient(this.getInitState());
  }

  componentWillReceiveProps() {
    this.props.initializeNewPatient(this.getInitState());
  }

  componentWillUpdate() {
    this.props.initializeNewPatient(this.getInitState());
  }

  getInitState(){
    const { stateMain } = this.props;
    const { formatMessage } = this.context.intl;
    return {
      ...stateMain,
      title: {
        personal: personal.MR,
        honorific: '',
      }
    }
  }

  handleSubmit(formData) {
    this.props.dispatch(this.props.createPatient(formData));
    this.props.dispatch(this.props.push('/main/patients'));
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.dispatch(this.props.push('/main/patients'));
  }

  render() {
    return (
      <div>
        <Helmet
          title="MedORG - Add New Patient"
          meta={[
            { name: 'description', content: 'Add New Patient' },
          ]}
        />
        <FormWrapper>
          <FormLegend
            Icon={PersonAdd}
            text={this.context.intl.formatMessage(messages.NewPatient)}
            iconColor="#00896D"
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            locale={this.props.locale}
            payers={this.props.payers}
          />
        </FormWrapper>
      </div>
    );
  }
}

NewPatientPage.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  push: React.PropTypes.func,
  createPatient: React.PropTypes.func.isRequired,
  stateMain: React.PropTypes.object,
  locale: React.PropTypes.string,
  payers: React.PropTypes.array,
};

NewPatientPage.contextTypes ={
  intl: React.PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    stateMain: state.main,
    locale: state.language.locale,
    payers: state.newPatientPage.payers,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push,
    dispatch,
    createPatient,
    initializeNewPatient: (stateMain) => {
      const newPatientInitForm = {};
      const { newPatientName, company, title } = stateMain;
      newPatientInitForm.lastName = newPatientName;
      newPatientInitForm.company = company ? company._id : ''; // eslint-disable-line no-underscore-dangle
      newPatientInitForm.title = title ? title: { personal: '', honorific: '' };
      dispatch(initialize('PatientForm', newPatientInitForm));
    },
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(NewPatientPage);

