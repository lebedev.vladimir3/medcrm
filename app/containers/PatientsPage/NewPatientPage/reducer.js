/*
 *
 * NewPatientPage reducer
 *
 */

import { FETCH_PAYER_LIST_SUCCESS } from './constants';

const initialState = { payers: [] };

function newPatientPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PAYER_LIST_SUCCESS:
      console.log(action);
      return { ...state, payers: action.data };
    default:
      return state;
  }
}

export default newPatientPageReducer;
