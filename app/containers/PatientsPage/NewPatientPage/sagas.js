/*
 *
 * NewPatientPage sagas
 *
 */

import { put, call, take, cancel, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';

import {
  REQUEST_ERROR,
  ADD_NEW_PATIENT,
  CREATE_PATIENT,
  FETCH_PAYER_LIST_REQUEST,
  FETCH_PAYER_LIST_SUCCESS,
  FETCH_PAYER_LIST_FAILURE,
} from './constants';


function* createPatient(action) {
  try {
    const response = yield callApi(axios.post, '/api/patients', action.payload);
    yield put({ type: ADD_NEW_PATIENT, data: response.data });
    browserHistory.push('/main/patients'); // eslint-disable-line no-underscore-dangle
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

function* newPatientSagaCreate() {
  const create = yield takeLatest(CREATE_PATIENT, createPatient);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}


function* fetchPayerList() {
  try {
    const response = yield callApi(axios.get, '/api/payers');
    yield put({ type: FETCH_PAYER_LIST_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: FETCH_PAYER_LIST_FAILURE, error: error.message });
  }
}


function* fetchPayerListSaga() {
  yield takeLatest(FETCH_PAYER_LIST_REQUEST, fetchPayerList);
}


// All sagas to be loaded
export default [
  newPatientSagaCreate,
  fetchPayerListSaga,
];
