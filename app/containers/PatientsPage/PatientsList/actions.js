/*
 *
 * PatientsList actions
 *
 */

import {
  LOAD_PATIENTS,
} from './constants';


export const loadPatients = (id) => ({ type: LOAD_PATIENTS, payload: id });
