/*
 *
 * PatientsList constants
 *
 */
export { DELETE_PATIENT, UPDATE_PATIENT } from 'containers/MainContainer/constants';

export const LOAD_PATIENTS = 'app/PatientPage/LOAD_PATIENTS';
export const PATIENTS_FETCH_ERROR = 'app/PatientPage/PATIENTS_FETCH_ERROR';
export const PATIENTS_FETCH_SUCCESS= 'app/PatientPage/PATIENTS_FETCH_SUCCESS';
