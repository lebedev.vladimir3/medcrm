import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Card } from 'components/styles/Card';
import { Table, TR, TD, TH, SortArrows } from 'components/Table';

import PersonAdd from 'react-icons/lib/md/person-add';
import Patients from 'assets/icons/Title/Patients.svg';
import FormLegend from 'components/FormLegend';
import styled from 'styled-components';
import MdReceipt from 'assets/icons/Note.svg';
import MenuIcon from 'react-icons/lib/ti/th-large-outline';
import moment from 'moment';
import messages from '../messages';
import { PageWrapper } from 'components/styles/Grid';
import { loadPatients } from './actions';
import { fetchPatients } from '../../MainContainer/actions';

import { HonorificParser, genderParser } from './../../../utils';
import Helmet from 'react-helmet';
import { Link } from 'react-router';

const NotesDiv = styled.div`
  width: 20px;
  height: 20px;
  display: flex;
  align-items: center;`;

const Notes = notesProps => <NotesDiv>{notesProps.icon &&
<MdReceipt />}</NotesDiv>;


export class PatientList extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.onPatientSorted = this.onPatientSorted.bind(this);
    this.onIDSorted = this.onIDSorted.bind(this);

    this.state = {
      patients: this.props.patients,
      patientSorted: 0,
      idSorted: 0,
    };
  }

  componentDidMount() {
    this.props.dispatch(this.props.loadPatients());
    this.props.dispatch(this.props.fetchPatients());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.patients !== this.state.patients) {
      this.setState({
        patients: nextProps.patients,
      });
    }
  }

  onPatientSorted() {
    const newPatient = this.state.patients;
    let patientSorted = this.state.patientSorted;
    if (patientSorted === -1 || patientSorted === 0) {
      newPatient.sort((a, b) => {
        if (a.lastName > b.lastName) return 1;
        if (b.lastName > a.lastName) return -1;
        return 0;
      });
      patientSorted = 1;
    } else {
      newPatient.sort((a, b) => {
        if (a.lastName < b.lastName) return 1;
        if (b.lastName < a.lastName) return -1;
        return 0;
      });
      patientSorted = -1;
    }
    this.setState({
      patients: newPatient,
      patientSorted,
    });
  }

  onIDSorted() {
    const newPatient = this.state.patients;
    let idSorted = this.state.idSorted;
    if (idSorted === -1 || idSorted === 0) {
      newPatient.sort((a, b) => {
        if (a._id > b._id) return 1;
        if (b._id > a._id) return -1;
        return 0;
      });
      idSorted = 1;
    } else {
      newPatient.sort((a, b) => {
        if (a._id < b._id) return 1;
        if (b._id < a._id) return -1;
        return 0;
      });
      idSorted = -1;
    }
    this.setState({
      patients: newPatient,
      idSorted,
    });
  }

  calculateAge(birth) {
    const time = new Date();
    const year = time.getFullYear();
    const month = time.getMonth() + 1;
    const day = time.getDate();
    let res = year - parseFloat(birth.yyyy);
    if (parseFloat(birth.mm) < month) {
      return res;
    } else {
      if (parseFloat(birth.mm) > month) {
        return res - 1;
      } else {
        if (parseFloat(birth.dd) < day) {
          return res;
        } else {
          return res - 1;
        }
      }
    }
  }

  render() {
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Helmet
          title="MedORG - Patients Directory"
          meta={[
            { name: 'description', content: 'Add New Patient' },
          ]}
        />
        <PageWrapper>
          <FormLegend
            Icon={Patients}
            text={formatMessage(messages.PatientsDirectory)}
            iconColor="#00896D"
            iconProps={{
              viewBox: "0 0 26 23"
            }}
          />
          <Card>
            <Table>
              <thead>
                <TR>
                  <TH onClick={this.onIDSorted} selected={this.state.idSorted}>
                    ID
                  </TH>
                  <TH onClick={this.onPatientSorted} style={{paddingLeft: '50px'}} selected={this.state.patientSorted}>
                    {formatMessage(messages.PatientName)}
                  </TH>
                  <TH>{formatMessage(messages.Age)}</TH>
                  <TH>{formatMessage(messages.PhoneNumber)}</TH>
                  <TH centered>{formatMessage(messages.Open)}<br />{formatMessage(messages.Prescriptions)}</TH>
                  <TH centered>{formatMessage(messages.Next)}<br />{formatMessage(messages.Appointment)}</TH>
                </TR>
              </thead>
              <tbody>
                {this.state.patients.map((row, index) => {
                  //const date = new Date(row.birth.yyyy + '-' + row.birth.mm + '-' + row.birth.dd);
                  const honoricTitle = row.title.honorific ? row.title.honorific : '';
                  let honor = HonorificParser.toClientSide(honoricTitle, formatMessage);

                  const prescriptionCount = row.prescriptions.filter((item) =>
                    row.status !== 'cancelled' && row.status !== 'completed'
                  ).length;

                  const now = moment();
                  const nextAppointmentDate = row.appointments.reduce((acc, val) => {
                    const appointmentDate = moment(val.date);
                    if (appointmentDate.isAfter(now)) {
                      if (!acc || appointmentDate.isBefore(acc)) {
                        return appointmentDate;
                      }
                    }
                    return acc;
                  }, null);

                  const age = this.calculateAge(row.birth);

                  console.log(row.title.personal);
                  return (
                    <TR key={index} link={`/main/patients/${row._id}`}>
                      <TD centered>{row.id}&nbsp;&nbsp;&nbsp;</TD>
                      <TD>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <Notes icon={row.notes} />
                          <div>
                            {
                              honor &&
                              <span style={{ fontWeight: 'normal' }}>{honor}&nbsp;</span>
                            }
                            <span style={{ fontWeight: 600 }}>{row.lastName},&nbsp;</span>
                            <span>{row.firstName}</span>
                            <span> {row.title && genderParser(row.title.personal)}</span>
                            {age < 18 && <strong> K</strong>}
                          </div>
                        </div>
                      </TD>
                      <TD>{age}</TD>
                      <TD>{row.phone && row.phone.main}</TD>
                      <TD centered>{prescriptionCount}</TD>
                      <TD centered>{nextAppointmentDate && nextAppointmentDate.format('DD.MM.YYYY')}</TD>
                    </TR>
                  );
                })}
              </tbody>
            </Table>
          </Card>
        </PageWrapper>
      </div>
    );
  }
}

PatientList.propTypes = {
  patients: PropTypes.array,
};

PatientList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    patients: state.PatientPage.patients,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    push,
    dispatch,
    loadPatients,
    fetchPatients,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(PatientList);
