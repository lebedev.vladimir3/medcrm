/*
 *
 * PatientsList reducer
 *
 */

import * as actionTypes from './constants';

const initialState = { patients: [] };

function editPatientPageReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.PATIENTS_FETCH_SUCCESS:
      return { ...state, patients: action.data };
    default:
      return state;
  }
}

export default editPatientPageReducer;
