/*
 *
 * PatientsList sagas
 *
 */

import { put, call, take, cancel, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';

import {
  LOAD_PATIENTS,
  PATIENTS_FETCH_SUCCESS,
  PATIENTS_FETCH_ERROR,
} from './constants';


function* loadPatients() {
  try {
    const response = yield callApi(axios.get, '/api/patients');
    yield put({ type: PATIENTS_FETCH_SUCCESS, data: response.data });
  } catch (error) {
    yield put({ type: PATIENTS_FETCH_ERROR, error: error.message });
  }
}


function* loadPatientSaga() {
  const load = yield takeLatest(LOAD_PATIENTS, loadPatients);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}

export default [
  loadPatientSaga,
];
