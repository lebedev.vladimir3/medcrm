import fontSetting from 'components/styles/font';
import React from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';
import MdChevronRight from 'react-icons/lib/md/chevron-right';
import messages from '../messages';

const Wrapper = styled.div`
  height: 50px;
  padding-top: 15px;
  padding-left: 30px;
  color: #347A64;
  
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
`;

const ChevronRight = styled(MdChevronRight)`
  color: #E5E7EE;
  size: 18px;
`;

const StyledLink = styled(Link)`
  color: #49545A;
  text-decoration: underline;
`;


const PrescriptionHeader = ({ patient, status, appointment }, context) => (
  <Wrapper>
    <StyledLink to={`/main/patients/${patient._id}/appointments#${status.toLowerCase()}`}>
      {status}
    </StyledLink>
    <ChevronRight />
    <span>{context.intl.formatMessage(messages.AppointmentID)}</span>
    <strong>
      {` ${patient.id}-${(appointment.prescription && appointment.prescription.id) || ''}/${appointment.id || ''}`}
    </strong>
  </Wrapper>
);

PrescriptionHeader.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default PrescriptionHeader;
