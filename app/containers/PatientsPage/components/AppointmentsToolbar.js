import styled from 'styled-components';


export default styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 30px;
  padding-right: 15px;
  box-sizing: border-box;
  height: 52px;
  width: 100%;
  border-top: 1px solid #DBDFEA;
  background-color: #E5E7EE;
`;
