import styled from 'styled-components';
import Card from 'components/Card';


const StyledCard = styled(Card)`
  width: 830px;
  margin: auto;
  @media (max-width: 1160px) {
    width: auto;
  }
`;

export default StyledCard;
