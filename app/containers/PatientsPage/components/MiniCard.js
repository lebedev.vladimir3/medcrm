import React from 'react';
import { StyledCard } from 'components/styles/Card/Mini';
import moment from 'moment';
import styled from 'styled-components';
import messages from '../messages';
import _calculateAge from './../../../utils/calculateAge';
import { genderParser } from './../../../utils';

const Name = styled.span`
  color: #464749;
`;

const Divider = styled.div`
  border-bottom: 1px #DBDFEA dashed;
  width: 100%;
  margin-bottom: 16px;
`;

const Row = styled.div`
  padding-bottom: 16px;
  &:last-child{
    padding: 0px;
  }
`;


const MiniCard = (props, context) => {
  const { patient, prescriptions, appointments } = props;
  const firstname = patient ? patient.firstName : '';
  const lastname = patient ? patient.lastName : '';

  const openPrescriptions = prescriptions.filter((item) => (
    item.status !== 'CANCELLED' && item.status !== 'COMPLETED')
  );
  const now = moment();

  const nextAppointmentDate = appointments.reduce((acc, val) => {
    const appointmentDate = moment(val.date);
    if (appointmentDate.isAfter(now)) {
      if (!acc || appointmentDate.isBefore(acc)) {
        return appointmentDate;
      }
    }
    return acc;
  }, null);

  const date = new Date(patient.birth ? (patient.birth.yyyy + '-' + patient.birth.mm + '-' + patient.birth.dd)
    : '');

  return (
    <StyledCard>
      <Row style={{ color: '#667083', fontWeight: 600 }}>
        <span>{context.intl.formatMessage(messages.PatientID)}</span>
        <strong style={{ float: 'right' }}>{patient && patient.id}</strong>
      </Row>
      <Row>
        <Name>
          <strong>{lastname}, </strong>
          <span>{firstname} {patient.title && genderParser(patient.title.personal)}</span>
        </Name>
      </Row>
      <Row>
        <span>{patient && patient.birth && (patient.birth.dd + '.' + patient.birth.mm + '.' + patient.birth.yyyy)}</span>
        <div style={{ float: 'right' }}>{_calculateAge(date)}&nbsp;{context.intl.formatMessage(messages.Years)}</div>
      </Row>
      <Divider />
      <Row>
        <span>{context.intl.formatMessage(messages.Registered)}</span>
        <strong style={{ float: 'right' }}>{patient && moment(patient.createdAt).format('DD.MM.YYYY')}</strong>
      </Row>
    </StyledCard>
  );
};
MiniCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MiniCard;
