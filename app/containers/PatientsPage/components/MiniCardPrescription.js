import React from 'react';
import styled from 'styled-components';
import messages from '../messages';
import { StyledCard } from 'components/styles/Card/Mini';
import _calculateAge from './../../../utils/calculateAge';
import { genderParser } from './../../../utils';
import theme from 'components/styles/colorScheme';

const Name = styled.span`
  color: #464749;
`;

const Divider = styled.div`
  border-top: 1px #DBDFEA dashed;
  width: 210px;
  padding-bottom: 14px;
  margin: auto;
`;

const Row = styled.div`
  padding-bottom: 16px;
  &:last-child{
    padding: 0px;
  }
`;

const MiniCard = (props, context) => {
  const { patient } = props;
  const firstname = patient ? patient.firstName : '';
  const lastname = patient ? patient.lastName : '';

  const date = new Date(patient.birth ? (patient.birth.yyyy + '-' + patient.birth.mm + '-' + patient.birth.dd)
    : '');
  return (
    <StyledCard>
      <Row style={{ color: '#667083', fontWeight: 600 }}>
        <span>{context.intl.formatMessage(messages.PatientID)}</span>
        <strong style={{ float: 'right' }}>{patient && patient.id}</strong>
      </Row>
      <Row>
        <Name>
          <strong>{lastname}, </strong>
          <span>{firstname} {patient.title && genderParser(patient.title.personal)}</span>
        </Name>
      </Row>
      <Row>
        <span>{patient && patient.birth && (patient.birth.dd + '.' + patient.birth.mm + '.' + patient.birth.yyyy)}</span>
        <div style={{ float: 'right', fontWeight: 600 }}>{_calculateAge(date)}&nbsp;{context.intl.formatMessage(messages.Years)}</div>
      </Row>
      <Divider />
      <Row>
        <strong>{props.patient.phone && props.patient.phone.main }</strong>
      </Row>
      {props.patient.email && <Row>
        <span style={{color: theme.Link.base }}>{props.patient.email }</span>
      </Row>}
      {props.patient.email && <Divider />}
      <Row>
        <strong>{context.intl.formatMessage(messages.PayerStatus)}</strong>
        {props.showSZ
          ? <div style={{ float: 'right' }}>
          <strong>SZ&nbsp;</strong>
          ( {props.patient.payerStatus} )
        </div>
          : <strong style={{ float: 'right' }}>{props.patient.payerStatus}</strong>

        }
      </Row>
    </StyledCard>
  );
};
MiniCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MiniCard;
