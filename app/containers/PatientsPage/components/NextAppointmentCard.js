import React from 'react';
import Card from 'components/Card';
import moment from 'moment';
import styled from 'styled-components';
import messages from '../messages';

import {
  StyledCard, Row,
  RightPart, Divider,
} from 'components/styles/Card/Mini';

const NextAppointmentCard = (props, context) => {
  const { patient, prescriptions, appointments } = props;
  const now = moment();

  const nextAppointmentDate = appointments.reduce((acc, val) => {
    const appointmentDate = moment(val.date);
    if (appointmentDate.isAfter(now)) {
      if (!acc || appointmentDate.isBefore(acc)) {
        return appointmentDate;
      }
    }
    return acc;
  }, null);

  return (
    <StyledCard>
      <Row>
        <span>{context.intl.formatMessage(messages.NextAppointment)}</span>
        <RightPart>{nextAppointmentDate ? nextAppointmentDate.format('DD.MM.YYYY') : '—'}</RightPart>
      </Row>
    </StyledCard>
  );
};
NextAppointmentCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default NextAppointmentCard;
