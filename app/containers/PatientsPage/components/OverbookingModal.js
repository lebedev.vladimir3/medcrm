import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import { CardFooter } from 'components/Card';
import Modal from 'components/Modal';
import { GreyButton, GreenButton } from 'components/common/Button';
import messages from '../messages';


const ModalHeader = styled.div`
  width: 100%;
  height: 50px;
  color: #546E7A;
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
  text-align: center;
  padding: 25px 0px 20px 0px;
`;

const ModalBody = styled.div`
  width: 100%;
  color: #3B4950;
  font-size: ${fontSetting.baseSizePlus1}px;
  line-height: 19px;
`;

const Centered = styled.div`
  margin: auto;
`;

const OverbookingModal = (props, context) => {
  const { onSubmit, onCancel, patientIsVacant, staffIsVacant, roomIsVacant } = props;
  return (
    <Modal width={550}>
      <ModalHeader>
        Overbooking Alert
      </ModalHeader>
      <ModalBody>
        <ul>
          { !patientIsVacant && <li>{context.intl.formatMessage(messages.PatientNotAvailable)}</li> }
          { !patientIsVacant && <br /> }
          { !staffIsVacant && <li>{context.intl.formatMessage(messages.StaffNotAvailable)}</li> }
          { !staffIsVacant && <br /> }
          { !roomIsVacant && <li>{context.intl.formatMessage(messages.RoomNotAvailable)}</li> }
          { !roomIsVacant && <br /> }
        </ul>
      </ModalBody>
      <CardFooter style={{ marginTop: 0 }}>
        <Centered>
          <GreyButton onClick={onCancel}>{context.intl.formatMessage(messages.Cancel)}</GreyButton>
          <GreenButton onClick={onSubmit}>{context.intl.formatMessage(messages.Submit)}</GreenButton>
        </Centered>
      </CardFooter>
    </Modal>
  );
};

OverbookingModal.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
OverbookingModal.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  patientIsVacant: PropTypes.bool,
  staffIsVacant: PropTypes.bool,
  roomIsVacant: PropTypes.bool,
};


export default OverbookingModal;
