import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';
import MdChevronRight from 'react-icons/lib/md/chevron-right';
import messages from '../messages';


const Wrapper = styled.div`
  height: 50px;
  padding-top: 15px;
  padding-left: 30px;
  color: #347A64;
  
  font-size: ${fontSetting.baseSizePlus3}px;
  font-weight: 600;
  line-height: 22px;
`;

const ChevronRight = styled(MdChevronRight)`
  color: #E5E7EE;
  size: 18px;
`;

const StyledLink = styled(Link)`
  color: #49545A;
  text-decoration: underline;
`;


const PrescriptionHeader = (props, context) => (
  <Wrapper>
    <StyledLink to={`/main/patients/${props.patient._id}/prescriptions#${props.status.toLowerCase()}`}>
      {props.status}
    </StyledLink>
    <ChevronRight />
    <span>{context.intl.formatMessage(messages.PrescriptionID)}</span>
    <strong>{` ${props.patient.id || ''}-${props.prescription.id || ''}`}</strong>
  </Wrapper>
);


PrescriptionHeader.propTypes = {
  patient: PropTypes.object,
  prescription: PropTypes.object,
};


PrescriptionHeader.contextTypes = {
  intl: PropTypes.object.isRequired,
};


export default PrescriptionHeader;
