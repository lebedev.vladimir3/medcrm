import React from 'react';
import styled from 'styled-components';
import messages from '../messages';

import {
  StyledCard, Row,
  RightPart as Count,
  AccentColorRow,
} from 'components/styles/Card/Mini';

import { Oval } from 'components/styles/common';

const Name = styled(AccentColorRow)` font-weight: 600; `;

const CountName = styled(Count)` color: #667083; `;

const OvalCount = Oval(Count);

const MiniCard = (props, context) => {
  const { prescriptions } = props;
  const openPrescriptions = prescriptions.filter((item) => (
    item.status !== 'CANCELLED' && item.status !== 'COMPLETED')
  );
  const scheduledNumber = prescriptions.filter((item) => (
    item.appointmentCount !== 0 && item.appointmentCount === item.providedServices.length
  )).length || '—';
  const partlyScheduledNumber = prescriptions.filter((item) => (
    item.appointmentCount !== 0 && item.appointmentCount !== item.providedServices.length
  )).length || '—';
  const unscheduledNumber = prescriptions.filter((item) => item.appointmentCount === 0).length || '—';
  const UnscheduledNumberComponent = Number(unscheduledNumber) ? OvalCount: Count;
  const isOpenPrescriptionsExists = openPrescriptions.length > 0;
  return (
    <StyledCard>
      <Row>
        <Name>{context.intl.formatMessage(messages.OpenPrescriptions)}</Name>
        <CountName value={openPrescriptions.length}>{openPrescriptions.length || '—'}</CountName>
      </Row>
      {
        isOpenPrescriptionsExists ? [
          <Row key="1">
            <span>{context.intl.formatMessage(messages.Scheduled)}</span>
            <Count value={Number(scheduledNumber)}>{scheduledNumber}</Count>
          </Row>,
          <Row key="2">
            <span>{context.intl.formatMessage(messages.PartlyScheduled)}</span>
            <Count value={Number(partlyScheduledNumber)}>{partlyScheduledNumber}</Count>
          </Row>,
          <Row key="3">
            <span>{context.intl.formatMessage(messages.Unscheduled)}</span>
            <UnscheduledNumberComponent
              value={Number(unscheduledNumber)}>{unscheduledNumber}
            </UnscheduledNumberComponent>
          </Row>
        ]: null
      }
    </StyledCard>
  );
};

MiniCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

export default MiniCard;
