import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import { BlueButton, SecondaryButton } from "components/common/Button";
import messages from './messages';

const PatientsPage = (props, context) => {
  return (
    <div>
      <HeaderPanel
        rightSide={[
        <BlueButton
          link={'/main/patients/new'}
          key={1}
          disabled={props.location.pathname === '/main/patients/new'}
        >{context.intl.formatMessage(messages.NewPatient)}
        </BlueButton>
      ]}
      >
        <HeaderItem link={'/main/patients'}>{`${context.intl.formatMessage(messages.PatientsDirectory)}`}</HeaderItem>
      </HeaderPanel>
      {props.children}</div>
  );
}

PatientsPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

PatientsPage.propTypes = {
  children: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => ({ dispatch, push });

export default connect(null, mapDispatchToProps)(PatientsPage);
