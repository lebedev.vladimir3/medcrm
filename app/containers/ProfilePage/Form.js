import { reduxForm } from 'redux-form';
import CompanyProfileForm, { validate } from 'components/CompanyProfileForm';


export default reduxForm({
  form: 'profileForm',
  validate,
})(CompanyProfileForm);
