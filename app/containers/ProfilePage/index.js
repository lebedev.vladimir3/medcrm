/*
 *
 * ProfilePage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import LocationProfileIcon from 'assets/icons/Title/LocationProfile.svg';
import FormLegend from 'components/FormLegend';
import styled from 'styled-components';
import Helmet from 'react-helmet';

import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';

import MessageAllowed from 'components/NotAllowed';
import roles from '../../../server/utils/authorization/roles';

import Form from './Form';
import { updateOrCreateCompany } from './../../containers/MainContainer/actions';
import messages from './messages';
import { FormWrapper } from 'components/styles/Grid';


class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.initializeCompany(this.props.company);
  }

  componentDidUpdate() {
    this.props.initializeCompany(this.props.company);
  }

  handleSubmit(formData) {
    this.props.dispatch(this.props.updateOrCreateCompany(formData));
  }

  render() {
    const isUserAllowed = this.props.user && this.props.user.role.name === roles.ADMIN;
    return isUserAllowed ? (
      <div>
        <Helmet title="MedORG - Location Profile" />
        <HeaderPanel>
          <HeaderItem>{`${this.context.intl.formatMessage(messages.LocationProfile)}`}</HeaderItem>
        </HeaderPanel>
        <FormWrapper>
          <FormLegend
            Icon={LocationProfileIcon}
            text={this.context.intl.formatMessage(messages.LocationProfile)}
          />
          <Form onSubmit={this.handleSubmit} />
        </FormWrapper>
      </div>
    ) : <MessageAllowed />;
  }
}
ProfilePage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
ProfilePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  updateOrCreateCompany: PropTypes.func.isRequired,
  initializeCompany: PropTypes.func.isRequired,
  company: PropTypes.object,
  user: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    company: state.main.company,
    user: state.App.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    updateOrCreateCompany,
    initializeCompany: company => {
      dispatch(initialize('profileForm', company));
    },
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
