/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  LocationProfile: {
    id: 'ProfilePage.LocationProfile',
    defaultMessage: 'Location Profile',
  },
});
