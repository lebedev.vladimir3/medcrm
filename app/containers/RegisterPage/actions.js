/*
 *
 * AuthPage actions
 *
 */
import axios from 'axios';
import callApi from 'utils/callApi';

import {
  DEFAULT_ACTION,
  REGISTER_REQUEST,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


export function auth(...data) {
  return { type: REGISTER_REQUEST, payload: data[0] };
}

export function register(email, password) {
  return axios.post('/api/auth/register', { email, password })
  .then((response) => response)
  .catch((err) => {
    console.log(err);
  });
}
