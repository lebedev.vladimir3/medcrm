/*
 *
 * AuthPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AuthPage/DEFAULT_ACTION';
export const REGISTER_REQUEST = 'app/AuthPage/REGISTER_REQUEST';
export const REQUEST_ERROR = 'app/AuthPage/REQUEST_ERROR';
