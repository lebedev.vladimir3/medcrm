/*
 *
 * AuthPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import styled from 'styled-components';

import { auth } from './actions';

import Logo from './../../components/Logo';
const AuthWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color:#283543;
  height: 100%;
  width: 100%;
  flex-direction: column;
`;

const TopPart = styled.div`
  display: flex;
  height: 216px;
  width: 350px;
  border-radius: 4px 4px 0 0;
  background-color: #FFFFFF;
  flex-direction: column;
  align-items: center;
`;

const BottomPart = styled.div`
  height: 49px;
  width: 350px;
  border-radius: 0 0 4px 4px;
  background-color: #F8FAFC;
  box-shadow: 0 -1px 0 0 #E7E9ED;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #7F8FA4;
`;

const Icon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  margin-top: -35px;
  height: 70px;
  width: 70px;
  background-color: #0066A6;  
  margin-bottom: 15px;

`;


const Input = styled.input`
  height: 36px;
  width:290px;
  border: 1px solid lightgray;
  border-radius: 5px;
  background-color: #F8FAFC;
  padding-left: 15px;
  margin-bottom: 15px;
`;

const Button = styled.button`
  height: 36px;
  width:290px;
  border-radius: 5px;
  background-color: #00AF73;
  color:white;
`;


export class AuthPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.clickSubmit = this.clickSubmit.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);

    this.state = {
      email: '',
      password: '',
    };
  }

  clickSubmit() {
    this.props.dispatch(auth({ email: this.state.email, password: this.state.password }));
  }

  handleEmail(e) {
    this.setState({ email: e.target.value });
  }

  handlePassword(e) {
    this.setState({ password: e.target.value });
  }

  render() {
    return (
      <AuthWrapper>
        <Helmet
          title="AuthPage"
          meta={[
            { name: 'description', content: 'Description of AuthPage' },
          ]}
        />
        <TopPart>

          <Icon>
            <Logo color="#0066A6" />
          </Icon>
          <Input placeholder="E-mail" value={this.state.email} onChange={this.handleEmail} />
          <Input placeholder="Password" value={this.state.password} onChange={this.handlePassword} />
          <Button placeholder="Password" onClick={this.clickSubmit} >
            <span>Register User</span>
          </Button>
        </TopPart>
        <BottomPart><span>Forgot Password?</span></BottomPart>
      </AuthWrapper>
    );
  }
}

AuthPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapStateToProps() {
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    auth,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
