import { call, put, takeLatest } from 'redux-saga/effects';
import { register } from './actions';
import { REGISTER_REQUEST, REQUEST_ERROR } from './constants';
// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
}

export function* authorize(action) {
  const request = action.payload;
  try {
    yield call(register, request.email, request.password);
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}


export function* topSaga() {
  yield takeLatest(REGISTER_REQUEST, authorize);
}

export default [
  topSaga,
];
