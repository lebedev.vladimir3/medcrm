import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import EquipmentForm, { validate } from 'components/EquipmentForm';


let Form = reduxForm({
  form: 'equipmentForm',
  validate,
})(EquipmentForm);


const selector = formValueSelector('equipmentForm');


Form = connect(
  state => {
    const roomValue = selector(state, 'room');
    const serviceCategoriesValue = selector(state, 'serviceCategories');
    const allServicesValue = selector(state, 'allServices');
    const allRoomsAreAlternativeValue = selector(state, 'allRoomsAreAlternative');

    return {
      roomValue,
      serviceCategoriesValue,
      allServicesValue,
      allRoomsAreAlternativeValue,
      mobility: selector(state, 'mobility') === 'true'
    };
  }
)(Form);


export default Form;
