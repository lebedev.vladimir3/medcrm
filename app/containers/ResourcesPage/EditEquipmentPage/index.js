/*
 *
 * NewRoomPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import DeleteModal from 'components/DeleteModal';
import { initialize } from 'redux-form';
import EquipmentProfileIcon from 'assets/icons/Title/EquipmentProfile.svg';
import FormLegend from 'components/FormLegend';
import PersonAdd from 'react-icons/lib/md/person-add';
import Form from './Form';
import messages from '../messages';
import { FormWrapper } from 'components/styles/Grid';

import {
  fetchEquipment,
  updateEquipment,
  deleteEquipment,
  fetchRoomList,
  fetchMedServiceList,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
} from '../actions';


export class NewRoomPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);

    this.state = { showModal: false };
  }

  componentDidMount() {
    this.props.fetchRoomList();
    this.props.fetchMedServiceList();
    this.props.fetchEquipment(this.props.routeParams.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.equipment._id === this.props.routeParams.id) {
      this.props.initialize(
        'equipmentForm',
        {
          ...nextProps.equipment,
          mobility: nextProps.equipment.mobility ? 'true' : 'false',
        }
      );
    }
  }

  handleSubmit(params) {
    this.props.updateEquipment({
      ...params,
      mobility: params.mobility === 'true',
      services: this.props.chosenServices,
    });
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.push('/main/resources');
  }

  handleDelete(e) {
    e.preventDefault();
    this.props.deleteEquipment(this.props.routeParams.id);
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { equipment, headerPanel } = this.props;
    return (
      <div>
        <Helmet
          title="MedORG - Resources - Equipment Profile"
          meta={[
            { name: 'description', content: 'Description of NewRoomPage' },
          ]}
        />
        { headerPanel }
        <FormWrapper>
          <FormLegend
            Icon={EquipmentProfileIcon}
            text={this.context.intl.formatMessage(messages.EditEquipment)}
            secondText={equipment && equipment.name}
            iconColor="#3e7dbc"
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            onDelete={this.showModal}
            rooms={this.props.rooms}
            services={this.props.services}
            serviceCategories={this.props.serviceCategories}
            handleChangeService={this.props.handleChangeService}
            handleChangeServiceCategory={this.props.handleChangeServiceCategory}
            handleChangeAllServices={this.props.handleChangeAllServices}
            chosenServices={this.props.chosenServices}
            chosenServiceCategories={this.props.chosenServiceCategories}
            allServices={this.props.allServices}
            isEdit
          />
        </FormWrapper>
        {this.state.showModal && <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.hideModal}
          headerText={this.context.intl.formatMessage(messages.DeleteEquipment)}
          bodyText={this.context.intl.formatMessage(messages.QuestEquipment)}
        />}
      </div>
    );
  }
}
NewRoomPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

NewRoomPage.propTypes = {
  rooms: PropTypes.array,
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  push: PropTypes.func,
  fetchEquipment: PropTypes.func,
  updateEquipment: PropTypes.func,
  deleteEquipment: PropTypes.func,
  fetchRoomList: PropTypes.func,
  fetchMedServiceList: PropTypes.func,
  initialize: PropTypes.func,
  routeParams: PropTypes.object,
  equipment: PropTypes.object,
};


function mapStateToProps(state) {
  return {
    services: state.resourcesPage.services,
    serviceCategories: state.resourcesPage.serviceCategories,
    rooms: state.resourcesPage.rooms,
    equipment: state.resourcesPage.editEquipment,
    chosenServices: state.resourcesPage.chosenServices,
    chosenServiceCategories: state.resourcesPage.chosenServiceCategories,
    allServices: state.resourcesPage.allServices,
  };
}


const mapDispatchToProps = {
  push,
  fetchEquipment,
  updateEquipment,
  deleteEquipment,
  fetchRoomList,
  fetchMedServiceList,
  initialize,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
};


export default connect(mapStateToProps, mapDispatchToProps)(NewRoomPage);
