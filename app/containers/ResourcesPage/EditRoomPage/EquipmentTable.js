import React, { PropTypes } from 'react';
import { Table, TR, TD, TH } from 'components/Table';
import MdCheck from 'assets/icons/CheckDark.svg';
import { Card } from 'components/styles/Card';
import messages from '../messages';
import { EMPTY_STRING } from "constants/fillers";

const EquipementList = (props, context) => {
  const { items, push, roomId } = props;

  const rows = items.map((row, index) => (
    <TR key={index} onClick={() => push(`/main/resources/equipment/${row._id}`)}>
      <TD>
        <strong>{row.name}</strong>
      </TD>
      <TD centered>
        { (row.room && row.room._id === roomId) ? <MdCheck /> : EMPTY_STRING}
      </TD>
      <TD centered>{row.mobility ? <MdCheck /> : EMPTY_STRING}</TD>
    </TR>
  ));


  return (
    <Card>
      <Table>
        <thead>
        <TR>
          <TH style={{width: '33%'}}>
            {context.intl.formatMessage(messages.EquipmentName)}
          </TH>
          <TH centered>
            <div>{context.intl.formatMessage(messages.Based)}</div>
            <div><small>{context.intl.formatMessage(messages.InThisRoom)}</small></div>
          </TH>
          <TH centered>{context.intl.formatMessage(messages.Mobility)}</TH>
        </TR>
        </thead>
        <tbody>
        {rows}
        </tbody>
      </Table>
    </Card>
  );
};
EquipementList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EquipementList.propTypes = {
  items: PropTypes.array,
  push: PropTypes.func,
};


EquipementList.defaultProps = {
  items: [],
};


export default EquipementList;
