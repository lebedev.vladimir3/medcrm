import { reduxForm } from 'redux-form';
import RoomForm from 'components/RoomForm';


export default reduxForm({
  form: 'RoomForm',
})(RoomForm);
