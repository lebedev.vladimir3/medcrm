import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import { Link } from 'components/common/HeaderPanel';
import DeleteModal from 'components/DeleteModal';
import { initialize } from 'redux-form';
import Form from './Form';
import EquipmentTable from './EquipmentTable';
import { fetchRoom, updateRoom, deleteRoom, fetchEquipmentList } from '../actions';
import FormLegend from 'components/FormLegend';
import LinkedAppointmentsIcon from 'assets/icons/Title/LinkedEquipment.svg';
import RoomProfileIcon from 'assets/icons/Title/RoomProfile.svg';
import messages from '../messages';
import { FormWrapper } from 'components/styles/Grid';


export class NewRoomPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);

    this.state = { showModal: false };
  }

  componentDidMount() {
    this.props.fetchRoom(this.props.routeParams.id);
    this.props.fetchEquipmentList();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.room._id === this.props.routeParams.id) {
      this.props.initialize('RoomForm', nextProps.room);
    }
  }

  handleSubmit(params) {
    this.props.updateRoom(params);
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.push('/main/resources');
  }

  handleDelete(e) {
    e.preventDefault();
    this.props.deleteRoom(this.props.routeParams.id);
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { room, equipment, headerPanel } = this.props;

    const roomEquipment = equipment.filter((item) => {
      const isBased = item.room && item.room._id === room._id;
      const isAlternative = Array.isArray(item.alternativeRooms) && item.alternativeRooms.filter((item) => item._id === room._id);
      return isBased || isAlternative.length > 0;
    });

    return (
      <div>
        <Helmet
          title="MedORG - Resources - Room Profile"
          meta={[
            { name: 'description', content: 'Description of NewRoomPage' },
          ]}
        />
        { headerPanel }
        <FormWrapper>
          <FormLegend
            Icon={RoomProfileIcon}
            text={this.context.intl.formatMessage(messages.RoomProfile)}
            secondText={(room && room.name)}
            iconColor="#4A7DBA"
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            onDelete={this.showModal}
            isEdit
          />
        </FormWrapper>
        {roomEquipment.length > 0 && (
          <FormWrapper>
            <FormLegend
              Icon={LinkedAppointmentsIcon}
              text={this.context.intl.formatMessage(messages.LinkedEquipment)}
              iconColor="#4A7DBA"
            />
            <EquipmentTable roomId={room._id} items={roomEquipment} />
          </FormWrapper>
        )}
        {this.state.showModal && <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.hideModal}
          headerText={this.context.intl.formatMessage(messages.DeleteRoom)}
          bodyText={this.context.intl.formatMessage(messages.QuestRoom)}
        />}
      </div>
    );
  }
}
NewRoomPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

NewRoomPage.propTypes = {
  push: PropTypes.func.isRequired,
  updateRoom: PropTypes.func,
  room: PropTypes.object,
  fetchRoom: PropTypes.func,
  deleteRoom: PropTypes.func,
  initialize: PropTypes.func,
  fetchEquipmentList: PropTypes.func,
  routeParams: PropTypes.object,
  equipment: PropTypes.array,
};


function mapStateToProps(state) {
  return {
    room: state.resourcesPage.editRoom,
    equipment: state.resourcesPage.equipment,
  };
}


const mapDispatchToProps = {
  push,
  initialize,
  fetchRoom,
  updateRoom,
  deleteRoom,
  fetchEquipmentList,
};


export default connect(mapStateToProps, mapDispatchToProps)(NewRoomPage);
