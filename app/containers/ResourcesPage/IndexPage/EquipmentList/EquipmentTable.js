import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { Table, TR, TD, TH } from 'components/Table';
import MdCheck from 'assets/icons/CheckDark.svg';
import styled from 'styled-components';
import messages from '../../messages';
import { EMPTY_STRING } from "constants/fillers";
import { stringComporator } from "../../../../utils/index";
import theme from 'components/styles/colorScheme';

const BasedRoomNumber = styled.span`
  color: #455A64;
  font-size: ${fontSetting.baseSizePlus1}px;
  font-weight: 600;
  line-height: 19px;
`;

const TRExt = TR.extend`
  background-color: #F5F6F7;
  border-bottom: 1px solid #DADFEA;
  border-top: 1px solid #DADFEA;
  height: auto;
  thead > & {
    border-bottom: 1px solid #DADFEA;
    border-top: 1px solid #DADFEA;
  };
  &:first-child {
    border-bottom: none;
  }
  &:last-child {
    border-top: none;
  }
`;

const THExt = TH.extend`
`

const THead = styled.thead`
  //height: 70px;
`;

const getColumnWidth = () => ([
  { name: 'Equipment Name', value: 25 },
  { name: 'Mobility', value: 25 },
  { name: 'Primary', value: 25 },
  { name: 'Alternative', value: 25 },
].map(item => item.value));

const EquipmentList = (props, context) => {
  const { items, push } = props;
  const { formatMessage } = context.intl;

  const rows = items.sort(stringComporator("name","asc")).map((row, index) => (
    <TR key={index} link={`/main/resources/equipment/${row._id}`}>
      <TD><strong>{row.shortName}</strong></TD>
      <TD centered>{row.mobility ? <MdCheck /> : EMPTY_STRING}</TD>
      <TD centered>
        <BasedRoomNumber>{row.room ? row.room.number : ''}</BasedRoomNumber>
      </TD>
      <TD centered>
        <span>
          { (row.room && row.alternativeRooms.length > 0) ? row.alternativeRooms.map((item) => item.number).join(', '): EMPTY_STRING}
        </span>
      </TD>
    </TR>
  ));


  return (
    <Table style={{ borderTop: '1px solid ' + theme.Table.header.border.accentTab }}>
      {getColumnWidth(props.isEdit).map((width,index) => <col key={index} width={`${width}%`}/>)}
      <THead>
        <TRExt>
          <THExt>{formatMessage(messages.ShortName)}</THExt>
          <THExt centered >{formatMessage(messages.Mobility)}</THExt>
          <THExt centered >
            <div>{formatMessage(messages.Primary)}</div>
            <div>{formatMessage(messages.RoomNo)}</div>
          </THExt>
          <THExt centered >
            <div>{formatMessage(messages.Alternative)}</div>
            <div>{formatMessage(messages.RoomNo)}</div>
          </THExt>
        </TRExt>
        <TRExt>

        </TRExt>
      </THead>
      <tbody>
        {rows}
      </tbody>
    </Table>
  );
};

EquipmentList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EquipmentList.propTypes = {
  items: PropTypes.array,
  push: PropTypes.func,
};


EquipmentList.defaultProps = {
  items: [],
};


export default EquipmentList;
