import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { fetchRoomList, fetchEquipmentList, sortRoomList } from '../../actions';
import EquipmentTable from './EquipmentTable';

export class EquipmentListPage extends React.Component {
  render() {
    const { equipment, push} = this.props;
    return (
      <EquipmentTable items={equipment} push={this.props.push} />
    );
  }
}

EquipmentListPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EquipmentListPage.propTypes = {
  location: PropTypes.object,
  rooms: PropTypes.array,
  equipment: PropTypes.array,
  push: PropTypes.func,
  fetchEquipmentList: PropTypes.func,
  fetchRoomList: PropTypes.func,
  sortRoomList: PropTypes.func,
  roomSorting: PropTypes.object,
};


function mapStateToProps(state) {
  return {
    equipment: state.resourcesPage.equipment,
    rooms: state.resourcesPage.rooms,
    equipmentSorting: state.resourcesPage.equipmentSorting,
    roomSorting: state.resourcesPage.roomSorting,
  };
}


const mapDispatchToProps = {
  push,
  fetchEquipmentList,
  fetchRoomList,
  sortRoomList,
};


export default connect(mapStateToProps, mapDispatchToProps)(EquipmentListPage);
