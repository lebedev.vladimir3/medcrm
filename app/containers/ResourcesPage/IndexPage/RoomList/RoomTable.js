import React, { PropTypes } from 'react';
import { Table, TR, TD, TH, SortArrows } from 'components/Table';
import messages from '../../messages';
import theme from 'components/styles/colorScheme';

const TRExt = TR.extend`
  background-color: #F5F6F7;
  border-bottom: 1px solid #DADFEA;
  border-top: 1px solid #DADFEA;
  height: auto;
  thead > & {
    border-bottom: 1px solid #DADFEA;
    border-top: 1px solid #DADFEA;
  };
  &:first-child {
    border-bottom: none;
  }
  &:last-child {
    border-top: none;
  }
`;

const THExt = TH.extend`
`

const getColumnWidth = () => ([
  { name: 'Room No.', value: 20 },
  { name: 'Room Name', value: 20 },
  { name: 'Capacity', value: 20 },
  { name: 'Based', value: 20 },
  { name: 'Mobile', value: 20 },
].map(item => item.value));

const RoomList = (props, context) => {
  const { equipment, sortRoomList, roomSorting, push } = props;
  const { formatMessage } = context.intl;
  return (
    <Table style={{ borderTop: '1px solid ' + theme.Table.header.border.accentTab }}>
      {getColumnWidth(props.isEdit).map((width,index) => <col key={index} width={`${width}%`}/>)}
      <thead>
        <TRExt>
          <THExt onClick={() => sortRoomList('number')} selected={roomSorting ? roomSorting.number || -1 : -1}>
            {formatMessage(messages.RoomNo)}
          </THExt>
          <THExt onClick={() => sortRoomList('name')} selected={roomSorting ? roomSorting.name || 0: 0}>
            {formatMessage(messages.ShortName)}
          </THExt>
          <THExt centered>
            <div>{formatMessage(messages.CapacityPatient)}</div>
            <div style={{ fontWeight: 'normal' }}>{formatMessage(messages.Patients)}</div>
          </THExt>
          <THExt centered>
            <div>{formatMessage(messages.BasedEquipment)}</div>
            <div>{formatMessage(messages.Equipment)}</div>
          </THExt>
          <THExt centered>
            <div>{formatMessage(messages.MobileEquipment)}</div>
            <div>{formatMessage(messages.Equipment)}</div>
          </THExt>
        </TRExt>
      </thead>
      <tbody>
        {props.items.map((row, index) => {
          return (
          <TR key={index} link={`/main/resources/rooms/${row._id}`}>
            <TD><strong>{row.number}</strong></TD>
            <TD><strong>{row.name}</strong></TD>
            <TD centered>{row.capacity}</TD>
            <TD centered>{equipment.filter((item) => item.room && item.room._id === row._id).length}</TD>
            <TD centered>{equipment.filter((item) => item.alternativeRooms && item.alternativeRooms.find(alternativeRoom => alternativeRoom._id === row._id)).length}</TD>
          </TR>
        )})}
      </tbody>
    </Table>
  );
};


RoomList.defaultProps = {
  roomSorting: {
    number: 0,
    name: 0,
  },
};

RoomList.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

RoomList.propTypes = {
  items: PropTypes.array,
  equipment: PropTypes.array,
  sortRoomList: PropTypes.func,
  roomSorting: PropTypes.object,
  push: PropTypes.func,
};


RoomList.defaultProps = {
  items: [],
  equipment: [],
};


export default RoomList;
