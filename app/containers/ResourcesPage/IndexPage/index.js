import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import Card from 'components/Card';
import FormLegend from 'components/FormLegend';
import ResourcesIcon from 'assets/icons/Title/Resources.svg';
import { fetchRoomList, fetchEquipmentList, sortRoomList } from '../actions';
import messages from '../messages';
import { PageWrapper } from 'components/styles/Grid';

import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { MAIN } from 'constants/routes';
const RESOURCES = MAIN.RESOURCES;
import { getLastRoute } from "../../../utils";

export class ResourcesIndexPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleEquipmentTabClick = this.handleEquipmentTabClick.bind(this);
    this.handleRoomsTabClick = this.handleRoomsTabClick.bind(this);
    this.state = {
      currentTab: 1,
    };
  }

  componentDidMount() {
    this.props.fetchRoomList();
    this.props.fetchEquipmentList();
  }

  handleEquipmentTabClick() {
    this.props.push(`/main/resources/equipments/`);
  }

  handleRoomsTabClick() {
    this.props.push(`/main/resources/rooms/`);
  }

  render() {
    const { equipment, rooms, roomSorting, headerPanel } = this.props;
    const { currentTab } = this.state;
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Helmet
          title="MedORG - Resources"
          meta={[
            { name: 'description', content: 'Resources' },
          ]}
        />
        { headerPanel }
        <PageWrapper>
          <FormLegend
            Icon={ResourcesIcon}
            text={formatMessage(messages.Resources)}
            iconProps={{
              style: { marginRight: '10px' }
            }}
          />
          <Card>
            <Tabs theme="status" value={getLastRoute(this.props.location.pathname)} activeColor="#738DC8">
              <Tab
                value={RESOURCES.EQUIPMENTS}
                onClick={this.handleEquipmentTabClick}
                label={formatMessage(messages.Equipment) + ` ( ${equipment.length} ) `}
              />
              <Tab
                value={RESOURCES.ROOMS}
                onClick={this.handleRoomsTabClick}
                label={formatMessage(messages.Rooms) + ` ( ${rooms.length} ) `}
              />
            </Tabs>
            {this.props.children}
          </Card>
        </PageWrapper>
      </div>
    );
  }
}

ResourcesIndexPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

ResourcesIndexPage.propTypes = {
  location: PropTypes.object,
  rooms: PropTypes.array,
  equipment: PropTypes.array,
  push: PropTypes.func,
  fetchEquipmentList: PropTypes.func,
  fetchRoomList: PropTypes.func,
  sortRoomList: PropTypes.func,
  roomSorting: PropTypes.object,
};


function mapStateToProps(state) {
  return {
    equipment: state.resourcesPage.equipment,
    rooms: state.resourcesPage.rooms,
    equipmentSorting: state.resourcesPage.equipmentSorting,
    roomSorting: state.resourcesPage.roomSorting,
  };
}


const mapDispatchToProps = {
  push,
  fetchEquipmentList,
  fetchRoomList,
  sortRoomList,
};


export default connect(mapStateToProps, mapDispatchToProps)(ResourcesIndexPage);
