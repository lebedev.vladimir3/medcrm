import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import EquipmentForm, { validate } from 'components/EquipmentForm';


let Form = reduxForm({
  form: 'newEquipmentForm',
  validate,
  initialValues: { mobility: 'false' },
})(EquipmentForm);


const selector = formValueSelector('newEquipmentForm');


Form = connect(
  state => {
    const roomValue = selector(state, 'room');
    const serviceCategoriesValue = selector(state, 'serviceCategories');
    const allServicesValue = selector(state, 'allServices');
    const allRoomsAreAlternativeValue = selector(state, 'allRoomsAreAlternative');

    return {
      roomValue,
      serviceCategoriesValue,
      allServicesValue,
      allRoomsAreAlternativeValue,
      mobility: selector(state, 'mobility') === 'true'
    };
  }
)(Form);


export default Form;
