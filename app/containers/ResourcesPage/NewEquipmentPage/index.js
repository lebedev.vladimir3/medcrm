/*
 *
 * NewRoomPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import { Link } from 'components/common/HeaderPanel';
import FormLegend from 'components/FormLegend';
import NewEquipmentIcon from 'assets/icons/Title/NewEquipment.svg';
import Mdpeople from 'react-icons/lib/md/people';
import PersonAdd from 'react-icons/lib/md/person-add';
import Form from './Form';
import messages from '../messages';
import { HeaderPanelHelper } from 'components/common/HeaderPanel';
import { FormWrapper } from 'components/styles/Grid';

import {
  createEquipment,
  fetchRoomList,
  fetchMedServiceList,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
} from '../actions';


export class NewRoomPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    this.props.fetchRoomList();
    this.props.fetchMedServiceList();
  }

  handleSubmit(params) {
    this.props.createEquipment({
      ...params,
      mobility: params.mobility === 'true',
      services: this.props.chosenServices,
    });
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.push('/main/resources');
  }

  render() {
    const { headerPanel } = this.props;
    return (
      <div>
        <Helmet
          title="MedORG - Resources - New Equipment"
          meta={[
            { name: 'description', content: 'Description of NewRoomPage' },
          ]}
        />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 0})}
        <FormWrapper>
          <FormLegend
            Icon={NewEquipmentIcon}
            text={this.context.intl.formatMessage(messages.NewEquipment)}
            iconProps={{
              style: { marginRight: '10px' }
            }}
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            rooms={this.props.rooms}
            services={this.props.services}
            serviceCategories={this.props.serviceCategories}
            handleChangeService={this.props.handleChangeService}
            handleChangeServiceCategory={this.props.handleChangeServiceCategory}
            handleChangeAllServices={this.props.handleChangeAllServices}
            chosenServices={this.props.chosenServices}
            chosenServiceCategories={this.props.chosenServiceCategories}
            allServices={this.props.allServices}
          />
        </FormWrapper>
      </div>
    );
  }
}

NewRoomPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

NewRoomPage.propTypes = {
  rooms: PropTypes.array,
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  push: PropTypes.func,
  createEquipment: PropTypes.func,
  fetchRoomList: PropTypes.func,
  fetchServiceCategoryList: PropTypes.func,
  fetchMedServiceList: PropTypes.func,
  chosenServices: PropTypes.array,
  chosenServiceCategories: PropTypes.array,
  allServices: PropTypes.bool,
  handleChangeService: PropTypes.func,
  handleChangeServiceCategory: PropTypes.func,
  handleChangeAllServices: PropTypes.func,
};


function mapStateToProps(state) {
  return {
    services: state.resourcesPage.services,
    serviceCategories: state.resourcesPage.serviceCategories,
    rooms: state.resourcesPage.rooms,
    chosenServices: state.resourcesPage.chosenServices,
    chosenServiceCategories: state.resourcesPage.chosenServiceCategories,
    allServices: state.resourcesPage.allServices,
  };
}


const mapDispatchToProps = {
  push,
  createEquipment,
  fetchRoomList,
  fetchMedServiceList,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
};


export default connect(mapStateToProps, mapDispatchToProps)(NewRoomPage);
