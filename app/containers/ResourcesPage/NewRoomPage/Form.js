import { reduxForm } from 'redux-form';
import RoomForm, { validate } from 'components/RoomForm';
import { injectIntl } from 'react-intl';

export default injectIntl(reduxForm({
  form: 'NewRoomForm',
  validate,
  initialValues: {
    capacity: 1
  }
})(RoomForm));
