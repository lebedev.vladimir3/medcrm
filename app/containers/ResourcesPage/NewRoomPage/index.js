import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import { Link } from 'components/common/HeaderPanel';
import FormLegend from 'components/FormLegend';
import NewRoomIcon from 'assets/icons/Title/NewRoom.svg';
import Form from './Form';
import { FormWrapper } from 'components/styles/Grid';

import { fetchRoomList, createRoom } from '../actions';
import messages from '../messages';
import { HeaderPanelHelper } from 'components/common/HeaderPanel';

export class NewRoomPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount(){
    const { rooms, fetchRoomList } = this.props;
    if(rooms && rooms.length === 0){
      fetchRoomList();
    }
  }
  handleSubmit(params) {
    this.props.createRoom(params);
  }

  handleCancel(e) {
    e.preventDefault();
    this.props.push('/main/resources');
  }

  render() {
    const { headerPanel, rooms } = this.props;
    console.log('New Room Page: ', this.props);
    return (
      <div>
        <Helmet
          title="MedORG - Resources - New Room"
          meta={[
            { name: 'description', content: 'Description of NewRoomPage' },
          ]}
        />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 1})}
        <FormWrapper>
          <FormLegend
            Icon={NewRoomIcon}
            text={this.context.intl.formatMessage(messages.NewRoom)}
          />
          <Form onSubmit={this.handleSubmit} onCancel={this.handleCancel} rooms={rooms} />
        </FormWrapper>
      </div>
    );
  }
}

NewRoomPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

NewRoomPage.propTypes = {
  push: PropTypes.func.isRequired,
  createRoom: PropTypes.func,
};


const mapDispatchToProps = {
  push,
  createRoom,
  fetchRoomList,
};

const mapStateToProps = (state) => ({
  rooms: state.resourcesPage.rooms
});

export default connect(mapStateToProps, mapDispatchToProps)(NewRoomPage);
