/*
 *
 * ResourcesPage actions
 *
 */

import {
  FETCH_ROOM_REQUEST,
  FETCH_ROOM_LIST_REQUEST,
  CREATE_ROOM_REQUEST,
  UPDATE_ROOM_REQUEST,
  DELETE_ROOM_REQUEST,

  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_EQUIPMENT_REQUEST,
  CREATE_EQUIPMENT_REQUEST,
  UPDATE_EQUIPMENT_REQUEST,
  DELETE_EQUIPMENT_REQUEST,

  FETCH_SERVICE_LIST_REQUEST,

  SORT_ROOM_LIST,

  SET_ALL_SERVICES_VALUE,
  SET_SERVICE_CATEGORY_VALUE,
  SET_SERVICE_VALUE,
} from './constants';


export const createRoom = (params) => ({ type: CREATE_ROOM_REQUEST, payload: params });

export const fetchRoom = (id) => ({ type: FETCH_ROOM_REQUEST, id });

export const fetchRoomList = () => ({ type: FETCH_ROOM_LIST_REQUEST });

export const updateRoom = (params) => ({ type: UPDATE_ROOM_REQUEST, payload: params });

export const deleteRoom = (id) => ({ type: DELETE_ROOM_REQUEST, id });


export const createEquipment = (params) => ({ type: CREATE_EQUIPMENT_REQUEST, payload: params });

export const fetchEquipment = (id) => ({ type: FETCH_EQUIPMENT_REQUEST, id });

export const fetchEquipmentList = () => ({ type: FETCH_EQUIPMENT_LIST_REQUEST });

export const updateEquipment = (params) => ({ type: UPDATE_EQUIPMENT_REQUEST, payload: params });

export const deleteEquipment = (id) => ({ type: DELETE_EQUIPMENT_REQUEST, id });


export const fetchMedServiceList = () => ({ type: FETCH_SERVICE_LIST_REQUEST });


export const sortRoomList = (params) => ({ type: SORT_ROOM_LIST, payload: params });


export const handleChangeService = (id, value) => ({
  type: SET_SERVICE_VALUE,
  payload: { id, value },
});

export const handleChangeServiceCategory = (id, value) => ({
  type: SET_SERVICE_CATEGORY_VALUE,
  payload: { id, value },
});

export const handleChangeAllServices = (value) => ({
  type: SET_ALL_SERVICES_VALUE,
  payload: value,
});
