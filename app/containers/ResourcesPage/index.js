/*
 *
 * ResourcesPage
 *
 */

import React, { PropTypes } from 'react';
import MessageAllowed from 'components/NotAllowed';
import { connect } from 'react-redux';
import roles from '../../../server/utils/authorization/roles';

import { push } from 'react-router-redux';
import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import { ButtonContent, ButtonText } from "components/common/Button/HeaderPanel";
import { BlueButton } from "components/common/Button";
import messages from './messages';

class ResourcesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    let isUserAllowed = this.props.user && this.props.user.role.name === roles.ADMIN;
    const { push }= this.props;
    return (isUserAllowed ? <div>{React.cloneElement(React.Children.toArray(this.props.children)[0], {
      headerPanel: (
        <HeaderPanel
          rightSide={[
            <BlueButton
              link={'/main/resources/equipment/new'}
              key={1}
            >
              <ButtonContent>
                <ButtonText >{this.context.intl.formatMessage(messages.AddNewEquipment)}</ButtonText>
              </ButtonContent>
            </BlueButton>,
            <BlueButton
              link={'/main/resources/rooms/new'}
              key={2}
            >
              <ButtonContent>
                <ButtonText >{this.context.intl.formatMessage(messages.AddNewRoom)}</ButtonText>
              </ButtonContent>
            </BlueButton>
          ]}
        >
          <HeaderItem link={'/main/resources'}>
            {`${this.context.intl.formatMessage(messages.Resources)}`}</HeaderItem>
        </HeaderPanel>)
    })}</div> : <MessageAllowed />);
  }
}

ResourcesPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

ResourcesPage.propTypes = {
  children: PropTypes.any,
};

const mapDispatchToProps = (dispatch) => ({ dispatch, push });

function mapStateToProps(state) {
  return {
    user: state.App.user,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ResourcesPage);
