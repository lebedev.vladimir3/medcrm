/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  NewEquipment: {
    id: 'ResourcesPage.NewEquipment',
    defaultMessage: 'New Equipment',
  },
  AddNewEquipment: {
    id: 'ResourcesPage.AddNewEquipment',
    defaultMessage: 'New Equipment',
  },
  AddNewRoom: {
    id: 'ResourcesPage.AddNewRoom',
    defaultMessage: 'New Room',
  },
  NewRoom: {
    id: 'ResourcesPage.NewRoom',
    defaultMessage: 'New Room',
  },
  RoomProfile: {
    id: 'ResourcesPage.RoomProfile',
    defaultMessage: 'Room Profile',
  },
  DeleteEquipment: {
    id: 'ResourcesPage.DeleteEquipment',
    defaultMessage: 'Delete Equipment',
  },
  QuestEquipment: {
    id: 'ResourcesPage.QuestEquipment',
    defaultMessage: 'Are you sure you want to delete this equipment?',
  },
  QuestRoom: {
    id: 'ResourcesPage.QuestRoom',
    defaultMessage: 'Are you sure you want to delete this room?',
  },
  LinkedEquipment: {
    id: 'ResourcesPage.LinkedEquipment',
    defaultMessage: 'Linked Equipment',
  },
  DeleteRoom: {
    id: 'ResourcesPage.DeleteRoom',
    defaultMessage: 'Delete Room',
  },
  Based: {
    id: 'ResourcesPage.Based',
    defaultMessage: 'Based',
  },
  InThisRoom: {
    id: 'ResourcesPage.InThisRoom',
    defaultMessage: '(in this room)',
  },
  MedServ: {
    id: 'Med.MedServ',
    defaultMessage: 'Med. Services',
  },
  CapacityPatient: {
    id: 'Med.CapacityPatient',
    defaultMessage: 'Capacity (patients)',
  },
  BasedEquipment: {
    id: 'Med.BasedEquipment',
    defaultMessage: 'Based',
  },
  Patients: {
    id: 'Med.Patients',
    defaultMessage: '(patients)',
  },
  MobileEquipment: {
    id: 'Med.MobileEquipment',
    defaultMessage: 'Mobile',
  },
  Colour: {
    id: 'Colour',
    defaultMessage: 'Colour',
  },
  Payers: {
    id: 'Payers',
    defaultMessage: 'Payers',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  Price: {
    id: 'Price',
    defaultMessage: 'Price',
  },
  Category: {
    id: 'Category',
    defaultMessage: 'Category',
  },
  Resources: {
    id: 'Resources',
    defaultMessage: 'Resources',
  },
  Equipment: {
    id: 'Equipment',
    defaultMessage: 'Equipment',
  },
  EditEquipment: {
    id: 'EditEquipment',
    defaultMessage: 'Edit Equipment',
  },
  EquipmentName: {
    id: 'EquipmentName',
    defaultMessage: 'Equipment Name',
  },
  Rooms: {
    id: 'Rooms',
    defaultMessage: 'Rooms',
  },
  Mobile: {
    id: 'Mobile',
    defaultMessage: 'Mobile',
  },
  Mobility: {
    id: 'ResourcesPage.Mobility',
    defaultMessage: 'Mobility'
  },
  Primary: {
    id: 'ResourcesPage.Primary',
    defineMessages: 'Primary',
    defaultMessage: 'Primary'
  },
  Alternative: {
    id: 'ResourcesPage.Alternative',
    defineMessages: 'Alternative',
    defaultMessage: 'Alternative'
  },
  ShortName: {
    id: 'RoomForm.ShortName',
    defineMessages: 'Short Name',
    defaultMessage: 'Short Name'
  },
  RoomNo: {
    id: 'ResourcesPage.RoomNo',
    defineMessages: 'Room No.',
    defaultMessage: 'Room No.'
  }
});
