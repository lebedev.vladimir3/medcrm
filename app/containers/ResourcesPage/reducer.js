/*
 *
 * ResourcesPage reducer
 *
 */

import {
 setServiceValue,
 setServiceCategoryValue,
 setAllServicesValue,
} from 'components/MedServiceFormWidget/utils';


import {
  FETCH_ROOM_LIST_SUCCESS,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_ROOM_SUCCESS,
  FETCH_EQUIPMENT_SUCCESS,
  FETCH_SERVICE_LIST_SUCCESS,
  SORT_ROOM_LIST,
  SET_ALL_SERVICES_VALUE,
  SET_SERVICE_CATEGORY_VALUE,
  SET_SERVICE_VALUE,
} from './constants';


const initialState = {
  equipment: [],
  rooms: [],
  serviceCategories: [],
  roomSorting: {},
  editRoom: {},
  editEquipment: {},
  chosenServices: [],
  chosenServiceCategories: [],
  allServices: false,
  allRoomsAreAlternativeValue: false,
};


function resourcesPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ROOM_LIST_SUCCESS:
      return { ...state, rooms: action.payload };

    case FETCH_EQUIPMENT_LIST_SUCCESS:
      return { ...state, equipment: action.payload };

    case FETCH_ROOM_SUCCESS:
      return { ...state, editRoom: action.payload };

    case FETCH_EQUIPMENT_SUCCESS:
      return { ...state, editEquipment: action.payload, chosenServices: action.payload.services };

    case FETCH_SERVICE_LIST_SUCCESS:
      return { ...state, serviceCategories: action.payload };

    case SORT_ROOM_LIST:
      const paramToSort = action.payload;
      const newRoomSorting = action.roomSorting ? action.roomSorting: state.roomSorting;
      const newSortingValue = newRoomSorting[action.payload] === 1 ? -1 : 1;
      newRoomSorting[action.payload] = newSortingValue;

      const newRooms = state.rooms.slice(0).sort((a, b) => {
        if (typeof a[paramToSort] === 'undefined' && typeof b[paramToSort] !== 'undefined') {
          return newSortingValue === 1 ? -1 : 1;
        }
        if (typeof a[paramToSort] !== 'undefined' && typeof b[paramToSort] === 'undefined') {
          return newSortingValue === 1 ? 1 : -1;
        }
        if (typeof a[paramToSort] === 'undefined' && typeof b[paramToSort] === 'undefined') {
          return 0;
        }
        if (a[paramToSort] > b[paramToSort]) return newSortingValue === 1 ? -1 : 1;
        if (b[paramToSort] > a[paramToSort]) return newSortingValue === 1 ? 1 : -1;
        return 0;
      });

      return { ...state, rooms: newRooms, roomSorting: newRoomSorting };

    case SET_SERVICE_VALUE:
      return setServiceValue(state, action);

    case SET_SERVICE_CATEGORY_VALUE:
      return setServiceCategoryValue(state, action);

    case SET_ALL_SERVICES_VALUE:
      return setAllServicesValue(state, action);

    default:
      return state;
  }
}

export default resourcesPageReducer;
