import { takeLatest, take, fork, put, cancel } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import {
  CREATE_ROOM_REQUEST,
  CREATE_ROOM_SUCCESS,
  CREATE_ROOM_FAILURE,
  FETCH_ROOM_LIST_REQUEST,
  FETCH_ROOM_LIST_SUCCESS,
  FETCH_ROOM_LIST_FAILURE,
  FETCH_ROOM_REQUEST,
  FETCH_ROOM_SUCCESS,
  FETCH_ROOM_FAILURE,
  UPDATE_ROOM_REQUEST,
  UPDATE_ROOM_SUCCESS,
  UPDATE_ROOM_FAILURE,
  DELETE_ROOM_REQUEST,
  DELETE_ROOM_SUCCESS,
  DELETE_ROOM_FAILURE,

  CREATE_EQUIPMENT_REQUEST,
  CREATE_EQUIPMENT_SUCCESS,
  CREATE_EQUIPMENT_FAILURE,
  FETCH_EQUIPMENT_LIST_REQUEST,
  FETCH_EQUIPMENT_LIST_SUCCESS,
  FETCH_EQUIPMENT_LIST_FAILURE,
  FETCH_EQUIPMENT_REQUEST,
  FETCH_EQUIPMENT_SUCCESS,
  FETCH_EQUIPMENT_FAILURE,
  UPDATE_EQUIPMENT_REQUEST,
  UPDATE_EQUIPMENT_SUCCESS,
  UPDATE_EQUIPMENT_FAILURE,
  DELETE_EQUIPMENT_REQUEST,
  DELETE_EQUIPMENT_SUCCESS,
  DELETE_EQUIPMENT_FAILURE,

  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_FAILURE,

  SORT_ROOM_LIST,
} from './constants';


const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';
const ROOM_ROUTE = '/main/resources/rooms/';
const EQUIP_ROUTE = '/main/resources/equipments/';

function* createRoom(action) {
  try {
    yield callApi(axios.post, '/api/rooms', action.payload);
    yield put({ type: CREATE_ROOM_SUCCESS });
    yield put(push(ROOM_ROUTE));
  } catch (error) {
    yield put({ type: CREATE_ROOM_FAILURE, error: error.message });
  }
}


function* fetchRoomList() {
  try {
    const response = yield callApi(axios.get, '/api/rooms');
    yield put({ type: FETCH_ROOM_LIST_SUCCESS, payload: response.data });
    yield put({ type: SORT_ROOM_LIST, payload: "number", roomSorting: { number: 1 } });
  } catch (error) {
    yield put({ type: FETCH_ROOM_LIST_FAILURE, error: error.message });
  }
}


function* fetchRoom(action) {
  try {
    const response = yield callApi(axios.get, `/api/rooms/${action.id}`);
    yield put({ type: FETCH_ROOM_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROOM_FAILURE, error: error.message });
  }
}


function* updateRoom(action) {
  try {
    const response = yield callApi(axios.put, `/api/rooms/${action.payload._id}`, action.payload);
    yield put({ type: UPDATE_ROOM_SUCCESS, categoryList: response.data });
    yield put(push(ROOM_ROUTE));
  } catch (error) {
    yield put({ type: UPDATE_ROOM_FAILURE, error: error.message });
  }
}


function* deleteRoom(action) {
  try {
    const response = yield callApi(axios.delete, `/api/rooms/${action.id}`);
    yield put({ type: DELETE_ROOM_SUCCESS, payload: response.data });
    yield put(push(ROOM_ROUTE));
  } catch (error) {
    yield put({ type: DELETE_ROOM_FAILURE, error: error.message });
  }
}


function* createEquipment(action) {
  try {
    let response = yield callApi(axios.post, '/api/equipment', action.payload);
    if(response.data.code && response.data.errmsg) throw response;
    yield put({ type: CREATE_EQUIPMENT_SUCCESS });
    browserHistory.push('/main/resources');
  } catch (error) {
    yield put({ type: CREATE_EQUIPMENT_FAILURE, error: error.message });
  }
}


function* fetchEquipmentList() {
  try {
    const response = yield callApi(axios.get, '/api/equipment');
    yield put({ type: FETCH_EQUIPMENT_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENT_LIST_FAILURE, error: error.message });
  }
}


function* fetchEquipment(action) {
  try {
    const response = yield callApi(axios.get, `/api/equipment/${action.id}`);
    yield put({ type: FETCH_EQUIPMENT_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_EQUIPMENT_FAILURE, error: error.message });
  }
}


function* updateEquipment(action) {
  try {
    const response = yield callApi(axios.put, `/api/equipment/${action.payload._id}`, action.payload);
    yield put({ type: UPDATE_EQUIPMENT_SUCCESS, payload: response.data });
    browserHistory.push('/main/resources');
  } catch (error) {
    yield put({ type: UPDATE_EQUIPMENT_FAILURE, error: error.message });
  }
}


function* deleteEquipment(action) {
  try {
    const response = yield callApi(axios.delete, `/api/equipment/${action.id}`);
    yield put({ type: DELETE_EQUIPMENT_SUCCESS, payload: response.data });
    browserHistory.push('/main/resources');
  } catch (error) {
    yield put({ type: DELETE_EQUIPMENT_FAILURE, error: error.message });
  }
}


function* fetchServiceList() {
  try {
    const serviceResponse = yield callApi(axios.get, '/api/med-services');
    const categoryResponse = yield callApi(axios.get, '/api/med-services/categories');

    yield put({
      type: FETCH_SERVICE_LIST_SUCCESS,
      payload: categoryResponse.data.map((category) => ({
        ...category,
        services: serviceResponse.data.filter((service) => service.category === category._id),
      })),
    });
  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_FAILURE, error: error.message });
  }
}


function* createRoomSaga() {
  const saga = yield fork(takeLatest, CREATE_ROOM_REQUEST, createRoom);
  yield take(LOCATION_CHANGE);
  yield cancel(saga);
}
function* fetchRoomListSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROOM_LIST_REQUEST, fetchRoomList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchRoomSaga() {
  const watcher = yield fork(takeLatest, FETCH_ROOM_REQUEST, fetchRoom);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* updateRoomSaga() {
  const watcher = yield fork(takeLatest, UPDATE_ROOM_REQUEST, updateRoom);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* deleteRoomSaga() {
  const watcher = yield fork(takeLatest, DELETE_ROOM_REQUEST, deleteRoom);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* createEquipmentSaga() {
  const watcher = yield fork(takeLatest, CREATE_EQUIPMENT_REQUEST, createEquipment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchEquipmentListSaga() {
  const watcher = yield fork(takeLatest, FETCH_EQUIPMENT_LIST_REQUEST, fetchEquipmentList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchEquipmentSaga() {
  const watcher = yield fork(takeLatest, FETCH_EQUIPMENT_REQUEST, fetchEquipment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* updateEquipmentSaga() {
  const watcher = yield fork(takeLatest, UPDATE_EQUIPMENT_REQUEST, updateEquipment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* deleteEquipmentSaga() {
  const watcher = yield fork(takeLatest, DELETE_EQUIPMENT_REQUEST, deleteEquipment);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchServiceListSaga() {
  const watcher = yield fork(takeLatest, FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

export default [
  createRoomSaga,
  fetchRoomListSaga,
  fetchRoomSaga,
  updateRoomSaga,
  deleteRoomSaga,
  createEquipmentSaga,
  fetchEquipmentListSaga,
  fetchEquipmentSaga,
  updateEquipmentSaga,
  deleteEquipmentSaga,
  fetchServiceListSaga,
];
