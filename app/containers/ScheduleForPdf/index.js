/*
 *
 * StaffPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Female from 'react-icons/lib/fa/venus';
import Male from 'react-icons/lib/fa/mars';
import FaSquare from 'react-icons/lib/fa/square';
import { Checkbox } from 'react-icheck';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import 'icheck/skins/all.css'; // or single skin css
import * as jsPDF  from 'jspdf'
import SortArrow from './../../components/SortArrows';

import './style.scss';
import { setSelectedDay } from  './../MainContainer/actions';

const TableWrapper = styled.div`
  padding: 30px 30px 0 30px;
  height: 85%;
  width: 100%;
`;

export class StaffPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props, context) {
    super(props, context);
    let selectedTab = 0;
    switch (window.location.pathname) {
      case '/main/staff':
        selectedTab = 2;
        break;
      case '/main/profile':
        selectedTab = 1;
        break;
      default:
        selectedTab = 0;
        break;
    }
    this.toggleMenuStaff = this.toggleMenuStaff.bind(this);
    this.changeAgenda = this.changeAgenda.bind(this);
    this.calendarSelect = this.calendarSelect.bind(this);
    this.setToday = this.setToday.bind(this);
    this.setPrevDay = this.setPrevDay.bind(this);
    this.setNextDay = this.setNextDay.bind(this);
    this.onStatusChange = this.onStatusChange.bind(this);
    this.onTimeSorted = this.onTimeSorted.bind(this);
    this.dispatchSelectedDay = this.dispatchSelectedDay.bind(this);

    this.state = {
      selectedTab,
      toggleStaff: true,
      currentAgenda: 1,
      timeSorted: -1,
      appointments: this.props.appointments ? this.props.appointments : [],
    };
  }

  componentDidMount() {
    console.log(document.getElementById('table-wrapper-print'));

    let pdf = new this.props.jsPDF();
    pdf.fromHTML(document.getElementById('table-wrapper-print'));
    pdf.save('Test.pdf');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.appointments !== this.state.appointments) {
      this.setState({
        appointments: nextProps.appointments,
      });
    }
  }

  dispatchSelectedDay(date) {
    this.props.dispatch(this.props.setSelectedDay(date));
  }

  onStatusChange(status, id) {
    const statusId = {};
    statusId[`status-${id}`] = status;
    this.setState(
      statusId
    );
  }

  onTimeSorted() {
    const appointments = this.state.appointments;
    let timeSorted = this.state.timeSorted;

    if (timeSorted === -1 || timeSorted === 0) {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        if (date1 > date2) return 1;
        if (date2 > date1) return -1;
        return 0;
      });
      timeSorted = 1;
    } else {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);

        if (date1 < date2) return 1;
        if (date2 < date1) return -1;
        return 0;
      });
      timeSorted = 0;
    }

    this.setState({
      staffList: appointments,
      timeSorted,
    });
  }

  setNextDay() {
    const tomorrow = new Date();
    tomorrow.setDate(this.props.selectedDay.getDate() + 1);
    this.dispatchSelectedDay(tomorrow);
  }

  setToday() {
    this.dispatchSelectedDay(new Date());
  }

  setPrevDay() {
    const yest = new Date();
    yest.setDate(this.props.selectedDay.getDate() - 1);
    this.dispatchSelectedDay(yest);
  }

  changeAgenda() {
    this.setState({
      currentAgenda: (this.state.currentAgenda + 1) % 2,
    });
  }

  toggleMenuStaff() {
    this.setState({
      toggleStaff: !this.state.toggleStaff,
    });
  }

  calendarSelect(date) {
    this.setState({
      selectedDay: date,
    });
  }

  filterAppointmentsByDay(appoinments, date) {
    date = new Date(date);
    const dateStart = new Date(date.getTime());
    dateStart.setHours(0, 0, 0, 0);
    const dateEnd = new Date(date.getTime());
    dateEnd.setHours(23, 59, 59, 999);

    return appoinments.filter((appoinment) => {
        return new Date(appoinment.date) >= dateStart && new Date(appoinment.date) <= dateEnd;
      }
    );
  }

  filterAppointmentsByActive(appoinments, staffs) {

    return appoinments.filter((appoinment) => {
        return staffs.some((staff)=> staff._id === appoinment.staff._id);
      }
    );
  }

  render() {
    const day = new Date(this.props.selectedDay).toString().split(' ');
    const dayToShow = `${day[0]}, ${day[2]} ${day[1]}`;

    const staffUser = {
      firstname: 'mark',
      lastname: 'grey',
      email: 'a@s331.com',
    };

    const testPatients = [{
      _id: '58ebd81911996f1c6e5e763c',
      firstname: 'petya',
      lastname: 'Petrov ',
      email: 'petrov.com',
      gender: 'male',
      isPrivate: true,
      isNew: true,
    }];
    testPatients.push({
      _id: '58ebd81911996f1c6e5e761c',
      firstname: 'Petra',
      lastname: 'Dohmann ',
      email: 'a@s331.com',
      gender: 'female',
      isPrivate: false,
      isNew: false,
    });
    const options = [
      'SCHEDULED',
      'RE_SCHEDULED',
      'CONFIRMED',
      'NO_SHOW',
      'CANCELLED',
      'PERFORMING',
      'AWAITING',
      'COMPLETE',
    ].map((item) => ({
      value: item, label: item,
    }));

    let localAppoinments = this.filterAppointmentsByDay(this.state.appointments, this.props.selectedDay);
    localAppoinments = this.filterAppointmentsByActive(localAppoinments, this.props.activeStaff);

    const rows = localAppoinments.map((a, index) => {
      const backgroundType = this.state[`status-${a.patient._id}`] ? this.state[`status-${a.patient._id}`].value : '';
      const rowClass = `table-row ${backgroundType}`;


      return (
        <tr className={rowClass} key={index} >
          <td className="table-row__cell  col1" >
            <div className="table-row__cell--text just-center" >

              {`${new Date(a.date).getHours()}:${new Date(a.date).getMinutes() > 9 ? new Date(a.date).getMinutes() : ('0' + new Date(a.date).getMinutes())}` }
            </div>
          </td>
          <td className="table-row__cell col2" >
            <div className="table-row__cell--text just-center" >

              {a.duration}
            </div>
          </td>
          <td className="table-row__cell col3 " >
            <div className="table-row__cell--text just-center" >
              {a.staff.firstname}
            </div>
          </td>
          <td className="table-row__cell col4" >
            <div className="table-row__cell--text just-center" >
              {a.resourcesAllocation.room}
            </div>
          </td>
          <td className="table-row__cell col5" >
            <div className="table-row__cell--text" >
              <div className="patient" >
                <span style={{ fontWeight: '600', color: '#586371' }} >{a.patient.lastName} </span>
                <span style={{ marginRight: '5px' }} >{a.patient.firstName}</span>
                {a.patient.title && a.patient.title.personal === 'mr' ?
                  <Male /> :
                  <Female />}
              </div>
            </div>
            {
              !a.patient.isPrivate &&
              <div className="private-user" >P</div>
            }
            {
              !a.patient.isNew &&
              <div className="new-user" >N</div>
            }
          </td>
          <td className="table-row__cell col6 status" >
            <div className="table-row__cell--text " >

              <Select
                name="form-field-name"
                options={options}
                onChange={(status) => this.onStatusChange(status, a.patient._id)}
                value={this.state[`status-${a.patient._id}`]}
                searchable={false}
                clearable={false}
              />
            </div>
          </td>
          <td className="table-row__cell col7" >
            <div className="table-row__cell--text " >
              <FaSquare color="#6AB63C" style={{ marginRight: '10px' }} />
              {a.treatment}
            </div>
          </td>
          <td className="table-row__cell notes-style col8" >
            <div className="table-row__cell--text " >

              {a.notes}
              {a.notes}
              {a.notes}
            </div>
          </td>
          <td className="table-row__cell notes-style col8" >
            <div className="table-row__cell--text " >

              {a.notes}
              {a.notes}
              {a.notes}
            </div>
          </td>
          <td className="table-row__cell notes-style col8" >
            <div className="table-row__cell--text " >

              {a.notes}
              {a.notes}
              {a.notes}
            </div>
          </td>
        </tr>
      );
    });

    return (

      <div className="table-wrapper-print" id="table-wrapper-print" >
        <table className="table" >
          <tbody>
            <tr className="table-row header-table" >
              <td className="table-row__cell   col1 " >
                <div className="table-row__cell--text just-center" onClick={this.onTimeSorted} >
                  Time
                  <SortArrow selected={this.state.timeSorted} />
                </div>
              </td>
              <td className="table-row__cell col2 just-center" >
                <div className="table-row__cell--text just-center" >
                  Duration
                </div>
              </td>
              <td className="table-row__cell col3" >
                <div className="table-row__cell--text just-center" >
                  Staff
                </div>
              </td>
              <td className="table-row__cell col4" >
                <div className="table-row__cell--text just-center" >
                  Resource
                </div>
              </td>
              <td className="table-row__cell col5 " >
                <div className="table-row__cell--text just-center" >
                  Patient
                </div>
              </td>
              <td className="table-row__cell col6 " >
                <div className="table-row__cell--text just-center" >
                  Status
                </div>
              </td>
              <td className="table-row__cell col7 " >
                <div className="table-row__cell--text just-center" >
                  Treatment
                </div>
              </td>
              <td className="table-row__cell col8 " >
                <div className="table-row__cell--text just-center" >
                  Note
                </div>
              </td>
              <td className="table-row__cell col8 " >
                <div className="table-row__cell--text just-center" >
                  Note
                </div>
              </td>
              <td className="table-row__cell col8 " >
                <div className="table-row__cell--text just-center" >
                  Note
                </div>
              </td>
            </tr>
            {rows}
            {rows}
            {rows}
          </tbody>
        </table>
      </div>
    );
  }
}

StaffPage.propTypes = {
  selectedDay: PropTypes.object,
  appointments: PropTypes.array,
};

function mapStateToProps(state) {
  return {
    users: state.main.users ? state.main.users : [],
    appointments: state.main.appointments ? state.main.appointments : [],
    selectedDay: state.main.selectedDay ? state.main.selectedDay : new Date(),
    activeStaff: state.main.activeStaff,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    setSelectedDay,
    jsPDF,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StaffPage);
