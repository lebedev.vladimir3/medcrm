/*
 *
 * SchedulePage actions
 *
 */
import axios from 'axios';

import {
  LOAD_USERS,
  LOAD_APPOINTMENTS,
} from './constants';

export function loadUsers() {
  return { type: LOAD_USERS };
}

export function loadAppointments(data) {
  return { type: LOAD_APPOINTMENTS, data: data };
}

export function updateAppointment(id, data) {
  axios.put(`/api/appointments/${id}`, { ...data, force: true });
}