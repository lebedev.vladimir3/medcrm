/*
 *
 * SchedulePage constants
 *
 */

export const LOAD_USERS = 'app/Schedule/LOAD_USERS';
export const LOAD_APPOINTMENTS = 'app/Schedule/LOAD_APPOINTMENTS';
export const REQUEST_ERROR = 'app/Schedule/REQUEST_ERROR';
export const FETCH_USERS = 'app/Schedule/FETCH_USERS';
export const FETCH_APPOINTMENTS = 'app/Schedule/FETCH_APPOINTMENTS';
