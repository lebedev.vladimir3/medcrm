import fontSetting from 'components/styles/font';
/*
 *
 * SchedulePage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Caretright from 'react-icons/lib/fa/caret-right';
import Caretleft from 'react-icons/lib/fa/caret-left';
import Female from 'react-icons/lib/fa/venus';
import Male from 'react-icons/lib/fa/mars';
import FaPrint from 'react-icons/lib/fa/print';
import FaSquare from 'react-icons/lib/fa/square';
import Select from 'react-select';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';
import 'react-select/dist/react-select.css';
import 'icheck/skins/all.css'; // or single skin css
import moment from 'moment';

import SortArrow from './../../components/SortArrows';

import './style.scss';
import { fetchAppointments, setSelectedDay } from './../MainContainer/actions';
import { updateAppointment, loadAppointments, loadUsers } from './actions';
import messages from './messages';
import Helmet from 'react-helmet';
const TableWrapper = styled.div`
  margin: 0 auto;
  height: 85%;
  width: 100%;
  max-width: 1400px;
  padding: 14px 30px 0 24px;
`;
const WeekTitle = styled.div`
    color: #596372;
    
    font-size: ${fontSetting.baseSizePlus1}px;
    font-weight: 600;
    line-height: 17px;
    text-align: center;
`;
const options = [
  'SCHEDULED',
  'RE_SCHEDULED',
  'CONFIRMED',
  'NO_SHOW',
  'CANCELLED',
  'PERFORMING',
  'AWAITING',
  'COMPLETED',
].map((item) => ({
  value: item, label: item,
}));

export class SchedulePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props, context) {
    super(props, context);
    let selectedTab = 0;
    switch (window.location.pathname) {
      case '/main/staff':
        selectedTab = 2;
        break;
      case '/main/profile':
        selectedTab = 1;
        break;
      default:
        selectedTab = 0;
        break;
    }
    this.changeAgenda = this.changeAgenda.bind(this);
    this.calendarSelect = this.calendarSelect.bind(this);
    this.setToday = this.setToday.bind(this);
    this.setPrevDay = this.setPrevDay.bind(this);
    this.setNextDay = this.setNextDay.bind(this);
    this.onStatusChange = this.onStatusChange.bind(this);
    this.onTimeSorted = this.onTimeSorted.bind(this);
    this.dispatchSelectedDay = this.dispatchSelectedDay.bind(this);
    this.onPdfClick = this.onPdfClick.bind(this);
    this.changeView = this.changeView.bind(this);
    this.setNextWeek = this.setNextWeek.bind(this);
    this.setPrevWeek = this.setPrevWeek.bind(this);
    this.setPrevMonth = this.setPrevMonth.bind(this);
    this.setNextMonth = this.setNextMonth.bind(this);

    this.state = {
      selectedTab,
      toggleStaff: true,
      currentAgenda: 1,
      timeSorted: -1,
      appointments: this.props.appointments ? this.props.appointments : [],
      users: this.props.users,
      currentView: 0,
    };
  }

  componentDidMount() {
    this.props.dispatch(this.props.loadAppointments());
    this.props.dispatch(this.props.loadUsers());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.appointments !== this.state.appointments || nextProps.users !== this.state.users) {
      this.setState({
        appointments: nextProps.appointments,
        users: nextProps.users,
      });
    }
  }

  dispatchSelectedDay(date) {
    this.props.dispatch(this.props.setSelectedDay(date));
  }

  onStatusChange(status, app, index) {
    const appointments = this.state.appointments;
    let appIndex = appointments.findIndex(a => a._id === app._id);
    appointments[appIndex].status = status.value;
    this.props.dispatch(this.props.fetchAppointments(appointments));
    this.props.updateAppointment(appointments[appIndex]._id, appointments[appIndex]);

    this.setState({
      appointments: appointments,
    });
  }

  onTimeSorted() {
    const appointments = this.state.appointments;
    let timeSorted = this.state.timeSorted;

    if (timeSorted === -1 || timeSorted === 0) {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        if (date1 > date2) return 1;
        if (date2 > date1) return -1;
        return 0;
      });
      timeSorted = 1;
    } else {
      appointments.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);

        if (date1 < date2) return 1;
        if (date2 < date1) return -1;
        return 0;
      });
      timeSorted = 0;
    }

    this.setState({
      staffList: appointments,
      timeSorted,
    });
  }

  setNextDay() {
    const tomorrow = new Date(this.props.selectedDay.getTime() + 24 * 60 * 60 * 1000 * 1);
    this.dispatchSelectedDay(tomorrow);
  }

  setNextWeek() {
    const nextWeek = new Date(this.props.selectedDay.getTime() + 24 * 60 * 60 * 1000 * 7);
    this.dispatchSelectedDay(nextWeek);
  }

  setNextMonth() {
    const NextMonth = moment(new Date(this.props.selectedDay)).add(1, 'month').toDate();
    this.dispatchSelectedDay(NextMonth);
  }

  setToday() {
    this.dispatchSelectedDay(new Date());
  }

  setPrevDay() {
    const yest = new Date(this.props.selectedDay.getTime() - 24 * 60 * 60 * 1000 * 1);
    this.dispatchSelectedDay(yest);
  }

  setPrevWeek() {
    const prevWeek = new Date(this.props.selectedDay.getTime() - 24 * 60 * 60 * 1000 * 7);
    this.dispatchSelectedDay(prevWeek);
  }

  setPrevMonth() {
    const prevMonth = moment(new Date(this.props.selectedDay)).subtract(1, 'month').toDate();
    this.dispatchSelectedDay(prevMonth);
  }

  changeAgenda() {
    this.setState({
      currentAgenda: (this.state.currentAgenda + 1) % 2,
    });
  }

  calendarSelect(date) {
    this.setState({
      selectedDay: date,
    });
  }

  filterAppointmentsByDay(appoinments, date) {
    date = new Date(date);
    const dateStart = new Date(date.getTime());
    dateStart.setHours(0, 0, 0, 0);
    const dateEnd = new Date(date.getTime());
    dateEnd.setHours(23, 59, 59, 999);

    return appoinments.filter((appoinment) => new Date(appoinment.date) >= dateStart && new Date(appoinment.date) <= dateEnd);
  }

  filterAppointmentsByActive(appoinments, staffs) {
    return appoinments.filter((appoinment) => staffs.some((staff) => appoinment.staff && staff._id === appoinment.staff._id && staff.active));
  }

  filterAppointmentsByPatientName(appoinments, patientName) {
    if (patientName !== '') {
      return appoinments.filter((appoinment) => {
        const array = patientName.split(' ');
        if (array.length === 1) {
          return appoinment.prescription ? appoinment.prescription.patient.lastName.toLowerCase().indexOf(array[0].toLowerCase()) === 0 : false;
        } else if (array.length === 2) {
          return (appoinment.prescription && appoinment.prescription.patient.lastName.toLowerCase().indexOf(array[0].toLowerCase()) === 0) &&
            (appoinment.prescription && appoinment.prescription.patient.firstName.toLowerCase().indexOf(array[1].toLowerCase()) === 0);
        }
      });
    }
    return appoinments;
  }

  onPdfClick(rows) {
    const columns = ['Time',
      'Duration',
      'Staff',
      'Resource',
      'Patient',
      'Status',
      'Treatment',
      'Note'];
    let rowArray = [];
    const doc = new this.props.jspdf('p', 'pt');
    doc.setFontSize(7);
    rows.forEach(({ localAppoinments, title }, index)=> {
      let rows = localAppoinments.map((a) => {
        rowArray = [];
        rowArray.push(`${new Date(a.date).getHours()}:${new Date(a.date).getMinutes() > 9 ? new Date(a.date).getMinutes() : ('0' + new Date(a.date).getMinutes())}`);
        rowArray.push(a.duration);
        rowArray.push(a.staff ? a.staff.firstName : '');
        rowArray.push(a.room ? a.room.name : '');
        rowArray.push(a.prescription ? (a.prescription.patient.lastName + ' ' + a.prescription.patient.firstName) : '');
        rowArray.push(a.status);
        rowArray.push(a.treatment);
        rowArray.push(a.notes);
        return rowArray;
      });

      const tableOptions = {
        styles: {
          overflow: 'linebreak',
          valign: 'middle',
          fontSize: 7,
          minCellWidth: 40,
        },
        columnStyles: {
          0: { columnWidth: 30 },
          1: { columnWidth: 40 },
          7: { columnWidth: 130 },
        },
      };
      doc.text(title, 270, doc.autoTable.previous.finalY - doc.autoTable.previous.height - 5);
      doc.autoTable(columns, rows, {
        ...tableOptions,
        startY: index === 0 ? 25 : doc.autoTable.previous.finalY + 25,
      });
    });
    doc.save('table.pdf');
  }

  changeView(index) {
    this.setState({ currentView: index });
  }

  getTable(rows, index) {
    return (<table className="table" style={{marginBottom:index!==0?'20px':'0px'}}>
      <tbody>
        <tr className="table-row header-table">
          <td className="table-row__cell   col1 ">
            <div className="table-row__cell--text just-center" onClick={this.onTimeSorted}>
              {this.context.intl.formatMessage(messages.Time)}
              <SortArrow selected={this.state.timeSorted} />
            </div>
          </td>
          <td className="table-row__cell col2 just-center">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Duration)}
            </div>
          </td>
          <td className="table-row__cell col3">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Staff)}
            </div>
          </td>
          <td className="table-row__cell col4">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Resource)}
            </div>
          </td>
          <td className="table-row__cell col5 ">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Patient)}
            </div>
          </td>
          <td className="table-row__cell col6 ">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Status)}
            </div>
          </td>
          <td className="table-row__cell col7 ">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Treatment)}
            </div>
          </td>
          <td className="table-row__cell col8 ">
            <div className="table-row__cell--text just-center">
              {this.context.intl.formatMessage(messages.Note)}
            </div>
          </td>
        </tr>
        {rows}
      </tbody>
    </table>);
  }

  getRows(localAppoinments) {
    return localAppoinments.map((a, index) => {
      const backgroundType = a.status ? a.status : '';
      const rowClass = `table-row ${backgroundType}`;

      const patientHasFirstName =  a.prescription && a.prescription.patient && a.prescription.patient.firstName && true;
      const patientHasLastName = a.prescription && a.prescription.patient && a.prescription.patient.lastName && true;

      return (
        <tr className={rowClass} key={index}>
          <td className="table-row__cell  col1">
            <div className="table-row__cell--text just-center">
              {`${new Date(a.date).getHours()}:${new Date(a.date).getMinutes() > 9 ? new Date(a.date).getMinutes() : ('0' + new Date(a.date).getMinutes())}` }
            </div>
          </td>
          <td className="table-row__cell col2">
            <div className="table-row__cell--text just-center">
              {a.duration}
            </div>
          </td>
          <td className="table-row__cell col3 ">
            <div className="table-row__cell--text just-center">
              {a.staff ? a.staff.firstName: '—'}
            </div>
          </td>
          <td className="table-row__cell col4">
            <div className="table-row__cell--text just-center">
              {a.room ? a.room.name: '—'}
            </div>
          </td>
          <td className="table-row__cell col5">
            <div className="table-row__cell--text">
              <div className="patient">
                {patientHasFirstName && (
                    <span style={{ marginRight: '5px' }}>{a.prescription ? a.prescription.patient.firstName : ''}</span>)}
                {patientHasLastName && (
                  <span style={{ fontWeight: '600', color: '#586371' }}>{a.prescription ? a.prescription.patient.lastName : ''} </span>)}
                {patientHasLastName || patientHasFirstName ? a.prescription && a.prescription.patient.title.personal === 'mr' ?
                  <Male /> : <Female />: ''}
              </div>
            </div>
            {
              a.prescription && !a.prescription.patient.isPrivate &&
              <div className="private-user">P</div>
            }
            {
              a.prescription && !a.prescription.patient.isNew &&
              <div className="new-user">N</div>
            }
          </td>
          <td className="table-row__cell col6 status">
            <div className="table-row__cell--text ">
              <Select
                name="form-field-name"
                options={options}
                onChange={(status) => this.onStatusChange(status, a, index)}
                value={a.status ? a.status : ''}
                searchable={false}
                clearable={false}
              />
            </div>
          </td>
          <td className="table-row__cell col7">
            <div className="table-row__cell--text ">
              {a.treatment && <FaSquare color="#6AB63C" style={{ marginRight: '10px' }} />}
              {a.treatment}
            </div>
          </td>
          <td className="table-row__cell notes-style col8">
            <div className="table-row__cell--text ">
              {a.notes}
            </div>
          </td>
        </tr>
      );
    });
  }

  render() {
    const day = new Date(this.props.selectedDay).toString().split(' ');
    const monday = moment(new Date(this.props.selectedDay)).startOf('isoweek');
    const sunday = moment(new Date(this.props.selectedDay)).endOf('isoweek');
    const dayToShow = `${day[0]}, ${day[2]} ${day[1]}`;
    const weekToShow = monday.format('MMM Do') + ' - ' + sunday.format('MMM Do');
    const monthToShow = moment(new Date(this.props.selectedDay)).format('MMMM');


    let localAppoinments = [];
    let weeksTable = [];
    let monthsTable = [];
    let rows = [];
    let nextDate = new Date();
    let i = 0;
    let weekToPdf = [];
    for (i = 0; i < 7; i++) {
      nextDate = new Date();
      nextDate.setDate(monday.toDate().getDate() + i);
      localAppoinments = this.filterAppointmentsByDay(this.state.appointments, nextDate);
      localAppoinments = this.filterAppointmentsByActive(localAppoinments, this.props.users);
      localAppoinments = this.filterAppointmentsByPatientName(localAppoinments, this.props.patientSearchName);
      rows = this.getRows(localAppoinments);
      if (rows.length > 0) {
        weekToPdf.push({ localAppoinments: localAppoinments, title: moment(nextDate).format('MMM Do') });
        weeksTable.push(
          <div key={i}>
            <WeekTitle>{moment(nextDate).format('MMM Do')}</WeekTitle>
            {this.getTable(rows, this.state.currentView)}
          </div>);
      }
    }
    let startOfMonthIteration = moment(new Date(this.props.selectedDay)).startOf('month');
    let endOfMonth = moment(new Date(this.props.selectedDay)).endOf('month');
    i = 0;
    let monthToPdf = [];
    while (startOfMonthIteration < endOfMonth) {
      localAppoinments = this.filterAppointmentsByDay(this.state.appointments, startOfMonthIteration);
      localAppoinments = this.filterAppointmentsByActive(localAppoinments, this.props.users);
      localAppoinments = this.filterAppointmentsByPatientName(localAppoinments, this.props.patientSearchName);
      rows = this.getRows(localAppoinments);
      if (rows.length > 0) {
        monthToPdf.push({ localAppoinments: localAppoinments, title: moment(startOfMonthIteration).format('MMM Do') });
        monthsTable.push(
          <div key={i}>
            <WeekTitle>{moment(startOfMonthIteration).format('MMM Do')}</WeekTitle>
            {this.getTable(rows, this.state.currentView)}
          </div>);
      }
      i++;
      startOfMonthIteration = startOfMonthIteration.add(1, 'days');
    }

    localAppoinments = this.filterAppointmentsByDay(this.state.appointments, this.props.selectedDay);
    localAppoinments = this.filterAppointmentsByActive(localAppoinments, this.props.users);
    localAppoinments = this.filterAppointmentsByPatientName(localAppoinments, this.props.patientSearchName);
    rows = this.getRows(localAppoinments);
    let rowsToPdf = [];
    switch (this.state.currentView) {
      case 0:
        rowsToPdf = [{ localAppoinments, title: moment(this.props.selectedDay).format('MMM Do') }];
        break;
      case 1:
        rowsToPdf = weekToPdf;
        break;
      case 2:
        rowsToPdf = monthToPdf;
        break;
      default:
        rowsToPdf = [];
        break;
    }
    return (
      <TableWrapper>
        <Helmet
          title="MedORG - Agenda"
          meta={[
            { name: 'description', content: 'Description of Agenda' },
          ]}
        />
        <div className="header-agenda">
          {this.state.currentView === 0 && <div className="navigation">
            <div className="icon-nav" onClick={this.setPrevDay}>
              <Caretleft />
            </div>
            <div style={{ marginRight: '15px' }}>
              {dayToShow}
            </div>
            <div className="icon-nav" onClick={this.setNextDay}>
              <Caretright />
            </div>
            <div className="nav-button" onClick={this.setToday}>
              {this.context.intl.formatMessage(messages.Today)}
            </div>
          </div>}

          {this.state.currentView === 1 && <div className="navigation">
            <div className="icon-nav" onClick={this.setPrevWeek}>
              <Caretleft />
            </div>
            <div style={{ marginRight: '15px' }}>
              {weekToShow}
            </div>
            <div className="icon-nav" onClick={this.setNextWeek}>
              <Caretright />
            </div>
            <div className="nav-button" onClick={this.setToday}>
              {this.context.intl.formatMessage(messages.CurrentWeek)}
            </div>
          </div>}

          {this.state.currentView === 2 && <div className="navigation">
            <div className="icon-nav" onClick={this.setPrevMonth}>
              <Caretleft />
            </div>
            <div style={{ marginRight: '15px' }}>
              {monthToShow}
            </div>
            <div className="icon-nav" onClick={this.setNextMonth}>
              <Caretright />
            </div>
            <div className="nav-button" onClick={this.setToday}>
              {this.context.intl.formatMessage(messages.CurrentMonth)}
            </div>
          </div>}

          <div className="navigation">
            <div className="nav-button-group">
              <div className={'nav-button' + (this.state.currentView === 0 ? '--active' : '')} onClick={()=>this.changeView(0)}>
                {this.context.intl.formatMessage(messages.Day)}
              </div>
              <div className={'nav-button' + (this.state.currentView === 1 ? '--active' : '')} onClick={()=>this.changeView(1)}>
                {this.context.intl.formatMessage(messages.Week)}
              </div>
              <div className={'nav-button' + (this.state.currentView === 2 ? '--active' : '')} onClick={()=>this.changeView(2)}>
                {this.context.intl.formatMessage(messages.Month)}
              </div>
            </div>
            <div className="divider" />
            <div className="icon-nav" onClick={() => this.onPdfClick(rowsToPdf)}>
              <FaPrint />
            </div>
          </div>
        </div>
        <div className="table-wrapper" style={{ height: window.innerHeight * 0.8 }}>
          {this.state.currentView === 0 && this.getTable(rows, this.state.currentView)}
          {this.state.currentView === 1 ? weeksTable.length > 0 ? weeksTable :
            <WeekTitle>{this.context.intl.formatMessage(messages.NoAppointments)}</WeekTitle> : ''}
          {
            this.state.currentView === 2 ? monthsTable.length > 0 ? monthsTable :
              <WeekTitle>{this.context.intl.formatMessage(messages.NoAppointments)}</WeekTitle> : ''
          }
        </div>
      </TableWrapper>
    );
  }
}

SchedulePage.propTypes = {
  selectedDay: PropTypes.object,
  appointments: PropTypes.array,
  users: PropTypes.array,
  setSelectedDay: PropTypes.func,
  dispatch: PropTypes.func,
  fetchAppointments: PropTypes.func,
};
SchedulePage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    users: state.main.users ? state.main.users : [],
    appointments: state.SchedulePage.appointments ? state.SchedulePage.appointments : [],
    selectedDay: state.main.selectedDay ? state.main.selectedDay : new Date(),
    changedStaff: state.main.changedStaff,
    patientSearchName: state.main.patientSearchName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    setSelectedDay,
    updateAppointment,
    fetchAppointments,
    loadAppointments,
    loadUsers,
    jspdf,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SchedulePage);
