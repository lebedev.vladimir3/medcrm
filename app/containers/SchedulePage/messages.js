/*
 * PatientsPage Messages
 *
 * This contains all the text for the PatientsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  ServiceCategories: {
    id: 'SchedulePage.ServiceCategories',
    defaultMessage: 'Service Categories',
  },
  Today: {
    id: 'Today',
    defaultMessage: 'Today',
  },
  Day: {
    id: 'Day',
    defaultMessage: 'Day',
  },
  Week: {
    id: 'Week',
    defaultMessage: 'Week',
  },
  CurrentWeek: {
    id: 'CurrentWeek',
    defaultMessage: 'Current Week',
  },
  CurrentMonth: {
    id: 'CurrentMonth',
    defaultMessage: 'Current Month',
  },
  Month: {
    id: 'Month',
    defaultMessage: 'Month',
  },
  NoAppointments: {
    id: 'NoAppointments',
    defaultMessage: 'No appointments in this week',
  },
  Time: {
    id: 'Time',
    defaultMessage: 'Time',
  },
  Duration: {
    id: 'Duration',
    defaultMessage: 'Duration',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  Resource: {
    id: 'Resource',
    defaultMessage: 'Resource',
  },
  Patient: {
    id: 'Patient',
    defaultMessage: 'Patient',
  },
  Status: {
    id: 'Status',
    defaultMessage: 'Status',
  },
  Treatment: {
    id: 'Treatment',
    defaultMessage: 'Treatment',
  },
  Note: {
    id: 'Note',
    defaultMessage: 'Note',
  },
});
