/*
 *
 * SchedulePage reducer
 *
 */

import {
  FETCH_USERS,
  FETCH_APPOINTMENTS,
} from './constants';

const initialState = {
  users: [],
  appointments: [],
};

function SchedulePageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS:
      return { ...state, users: action.data };
    case FETCH_APPOINTMENTS:
      return { ...state, appointments: action.data };
    default:
      return state;
  }
}

export default SchedulePageReducer;
