import { put, call, take, cancel, takeLatest, fork } from 'redux-saga/effects';

import axios from 'axios';
import callApi from 'utils/callApi';

import { LOAD_USERS, FETCH_USERS, REQUEST_ERROR, LOAD_APPOINTMENTS, FETCH_APPOINTMENTS } from './constants';


export function loadUsersRequest() {
  return axios.get('/api/users');
}
export function loadAppointmentsRequest() {
  return axios.get('/api/appointments');
}

function* loadUsers() {
  try {
    const response = yield callApi(loadUsersRequest);

    const users = response.data.map((user) => {
      user.active = true;
      return user;
    });

    yield put({ type: FETCH_USERS, data: users });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

export function* loadAppointments() {
  try {
    const response = yield callApi(axios.post, '/api/appointments/withPatient');
    yield put({ type: FETCH_APPOINTMENTS, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}
function* loadAppointmentsSage() {
  const watcher = yield fork(takeLatest, LOAD_APPOINTMENTS, loadAppointments);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

function* loadUsersSage() {
  const watcher = yield fork(takeLatest, LOAD_USERS, loadUsers);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}


// All sagas to be loaded
export default [
  loadAppointmentsSage,
  loadUsersSage,
];
