//@flow
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { reduxForm, Field } from 'redux-form';
import Helmet from 'react-helmet';
import FormLegend from 'components/FormLegend';
import SettingsIcon from 'assets/icons/Title/Settings.svg';

//import { fetchRoomList, fetchEquipmentList, sortRoomList } from '../actions';

import messages from './messages';
import { PageWrapper } from 'components/styles/Grid';

import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import renderHelper from 'components/common/renderHelper';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { ContentWrap } from 'components/styles/Containers';
import { Card, CardFooter } from 'components/styles/Card';
import { RowsWrap, Row, Col, LabelCol, Form, FieldsWrapCol } from 'components/styles/Grid';
import { FormLabel, RequiredStar } from 'components/styles/common';
import { GreyButton, GreenButton, DeleteButton } from 'components/common/Button';
import Divider from 'components/common/Divider';

import { updateOrCreateCompany } from 'containers/MainContainer/actions';

class SettingsPage extends React.Component<{}> {
  componentDidMount() {
    this.props.initialize(this.props.company.settings);
  }
  handleSubmit(formData){
    this.props.updateOrCreateCompany({ "settings.appointment": formData.appointment})
  }
  render() {
    const { handleSubmit, onSubmit, onCancel, pristine, onDelete } = this.props;
    const { formatMessage } = this.context.intl;
    return (
      <div>
        <Helmet
          title={`MedORG - formatMessage(messages.Settings)`}
          meta={[
            { name: 'description', content: formatMessage(messages.Settings) },
          ]}
        />
        <HeaderPanel>
          <HeaderItem link={'/main/settings'}>
            {`${formatMessage(messages.Settings)}`}</HeaderItem>
        </HeaderPanel>
        <PageWrapper short>
          <FormLegend
            Icon={SettingsIcon}
            text={formatMessage(messages.Settings)}
            iconProps={{
              style: { marginRight: '10px' }
            }}
          />
          <Card>
            <Form onSubmit={handleSubmit(::this.handleSubmit)}>
              <ContentWrap padding="10px 30px 10px 30px" style={{ minHeight: '280px' }}>
                <Divider title={formatMessage(messages.AppointmentScheduler)}/>
                <Row center>
                  <LabelCol width="402px" padding={[0,20,0,0]}>
                    <FormLabel>{formatMessage(messages.MaximumShift)}</FormLabel>
                  </LabelCol>
                  <FieldsWrapCol>
                    <Row>
                      <Col width="82px">
                        <Field
                          name="appointment.maximumShiftBetweenServices"
                          component={renderHelper.inputField}
                          //placeholder={formatMessage(messages.ShortNamePlaceholder)}
                          required />
                      </Col>
                      <Col width="75%" />
                    </Row>
                  </FieldsWrapCol>
                </Row>
                <Row center>
                  <LabelCol width="402px" padding={[0,20,0,0]} textLeft>
                    <FormLabel>{formatMessage(messages.MinimumShift)}</FormLabel>
                  </LabelCol>
                  <FieldsWrapCol>
                    <Row>
                      <Col width="82px">
                        <Field
                          name="appointment.minimumShiftBetweenProposed"
                          component={renderHelper.inputField}
                          //placeholder={formatMessage(messages.ShortNamePlaceholder)}
                          required />
                      </Col>
                      <Col width="75%" />
                    </Row>
                  </FieldsWrapCol>
                </Row>
              </ContentWrap>
              <CardFooter>
                <ContentWrap padding="10px 30px 10px 30px">
                  <Row justifyContent="space-between" center noPadding>
                    <Col />
                    <Col>
                      <div>
                        <GreyButton onClick={onCancel}>{formatMessage(messages.Cancel)}</GreyButton>
                        <GreenButton type="submit" disabled={pristine}>
                          {formatMessage(messages.SaveChanges)}
                        </GreenButton>
                      </div>
                    </Col>
                  </Row>
                </ContentWrap>
              </CardFooter>
            </Form>
          </Card>
        </PageWrapper>
      </div>
    );
  }
}

SettingsPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

SettingsPage = reduxForm({
  form: 'SettingsForm',
})(SettingsPage);

const mapStateToProps = (state) => ({
  company: state.main.company
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  updateOrCreateCompany
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
