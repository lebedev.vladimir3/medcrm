import { defineMessages } from 'react-intl';

export default defineMessages({
  Settings: {
    id: 'MainContainer.mainsettings.Settings',
    defaultMessage: 'Settings',
  },
  AppointmentScheduler: {
    id: 'AppointmentScheduler',
    defaultMessage: 'Appointment Scheduler',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  SaveChanges: {
    id: 'SaveChanges',
    defaultMessage: 'Save Changes',
  },
  MaximumShift: {
    id: 'Settings.AppointmentScheduler.MaximumShiftBetweenServices',
    defaultMessage: 'Maximum shift between services during same day, min',
  },
  MinimumShift: {
    id: 'Settings.AppointmentScheduler.MinimumShiftBetweenProposed',
    defaultMessage: 'Minimum shift between proposed appointments, min',
  },
});
