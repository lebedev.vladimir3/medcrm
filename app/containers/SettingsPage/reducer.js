import * as actionTypes from './constants';

const initialState = {};

function settingsPageReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default settingsPageReducer;
