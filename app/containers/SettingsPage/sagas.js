import { call, put, takeLatest } from 'redux-saga/effects';
import * as actionsTypes from './actions';
import * as constants from './constants';

// export function* authorize(action) {
//   const request = action.payload;
//   try {
//     yield call(register, request.email, request.password);
//   } catch (error) {
//     yield put({ type: REQUEST_ERROR, error: error.message });
//   }
// }

export function* emptySaga(){}

export default [
  emptySaga,
];
