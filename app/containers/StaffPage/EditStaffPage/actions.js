/*
 *
 * EditStaffPage actions
 *
 */

import {
  // FETCH_USER_REQUEST,
  UPDATE_USER_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
  DELETE_USER_REQUEST,
  LOAD_USER,
  FETCH_SERVICE_LIST_REQUEST,
  FETCH_CATEGORY_LIST_REQUEST,
  SET_SERVICE_VALUE,
  SET_SERVICE_CATEGORY_VALUE,
  SET_ALL_SERVICES_VALUE,
} from './constants';


// export function fetchUser(userId) {
//   return { type: FETCH_USER_REQUEST, id: userId };
// }

export function updateUser({ users, user }) {
  return { type: UPDATE_USER_REQUEST, payload: { users, user } };
}

export function deleteUser({ users, userId }) {
  return { type: DELETE_USER_REQUEST, payload: { users, id: userId } };
}

export function fetchRoles() {
  return { type: FETCH_ROLE_LIST_REQUEST };
}


export const fetchMedServiceList = () => ({ type: FETCH_SERVICE_LIST_REQUEST });


export const fetchServiceCategoryList = () => ({ type: FETCH_CATEGORY_LIST_REQUEST });

export const loadUser = (id) => ({ type: LOAD_USER, payload: id });


export const handleChangeService = (id, value, categoryId) => ({
  type: SET_SERVICE_VALUE,
  payload: { id, value, categoryId },
});

export const handleChangeServiceCategory = (id, value) => ({
  type: SET_SERVICE_CATEGORY_VALUE,
  payload: { id, value },
});

export const handleChangeAllServices = (value) => ({
  type: SET_ALL_SERVICES_VALUE,
  payload: value,
});
