/*
 *
 * EditStaffPage constants
 *
 */


export const FETCH_USER_REQUEST = 'app/EditStaffPage/FETCH_USER_REQUEST';
export const FETCH_USER_SUCCESS = 'app/EditStaffPage/FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'app/EditStaffPage/FETCH_USER_FAILURE';

export const UPDATE_USER_REQUEST = 'app/EditStaffPage/UPDATE_USER_REQUEST';
export const UPDATE_USER_SUCCESS = 'app/EditStaffPage/UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAILURE = 'app/EditStaffPage/UPDATE_USER_FAILURE';

export const DELETE_USER_REQUEST = 'app/EditStaffPage/DELETE_USER_REQUEST';
export const DELETE_USER_SUCCESS = 'app/EditStaffPage/DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'app/EditStaffPage/DELETE_USER_FAILURE';

export const FETCH_ROLE_LIST_REQUEST = 'app/EditStaffPage/FETCH_ROLE_LIST_REQUEST';
export const FETCH_ROLE_LIST_SUCCESS = 'app/EditStaffPage/FETCH_ROLE_LIST_SUCCESS';
export const FETCH_ROLE_LIST_FAILURE = 'app/EditStaffPage/FETCH_ROLE_LIST_FAILURE';

export { UPDATE_USER_IN_STATE } from 'containers/MainContainer/constants';
export { DELETE_USER_FROM_STATE } from 'containers/MainContainer/constants';
export const LOAD_USER = 'app/EditStaffPage/LOAD_USER';


export const FETCH_SERVICE_LIST_REQUEST = 'app/EditStaffPage/FETCH_SERVICE_LIST_REQUEST';
export const FETCH_SERVICE_LIST_SUCCESS = 'app/EditStaffPage/FETCH_SERVICE_LIST_SUCCESS';
export const FETCH_SERVICE_LIST_FAILURE = 'app/EditStaffPage/FETCH_SERVICE_LIST_FAILURE';

export const FETCH_CATEGORY_LIST_REQUEST = 'app/EditStaffPage/FETCH_CATEGORY_LIST_REQUEST';
export const FETCH_CATEGORY_LIST_SUCCESS = 'app/EditStaffPage/FETCH_CATEGORY_LIST_SUCCESS';
export const FETCH_CATEGORY_LIST_FAILURE = 'app/EditStaffPage/FETCH_CATEGORY_LIST_FAILURE';


export const SET_ALL_SERVICES_VALUE = 'app/EditStaffPage/SET_ALL_SERVICES_VALUE';
export const SET_SERVICE_CATEGORY_VALUE = 'app/EditStaffPage/SET_SERVICE_CATEGORY_VALUE';
export const SET_SERVICE_VALUE = 'app/EditStaffPage/SET_SERVICE_VALUE';
