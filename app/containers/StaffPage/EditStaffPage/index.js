/*
 *
 * NewStaffPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { initialize } from 'redux-form';
import Helmet from 'react-helmet';
import StaffProfileIcon from 'assets/icons/Title/StaffProfile.svg';
import DeleteModal from 'components/DeleteModal';
import { SecondaryButton } from 'components/common/Button';
import Calendar from 'components/WorkingHoursCalendar';
import MessageAllowed from 'components/NotAllowed';
import FormLegend from 'components/FormLegend';
import { TableContent, RightSide, LeftSide, PaddingTop } from 'components/styles/Grid';

import PersonalDetailsIcon from 'assets/icons/MainTab/Blue/P.Details.svg';
import MedicalServiceIcon from 'assets/icons/MainTab/Blue/MedicalServices.svg';
import WorkingHoursIcon from 'assets/icons/MainTab/Blue/WorkingHours.svg';

import FaUser from 'react-icons/lib/fa/user';
import FaClcok from 'react-icons/lib/fa/clock-o';
import MiniCard from '../components/MiniCard';
import Form from './Form';
import theme from 'components/styles/colorScheme';
import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { MAIN } from 'constants/routes';
const STAFF = MAIN.STAFF;

import {
  updateUser,
  deleteUser,
  loadUser,
  fetchMedServiceList,
  fetchServiceCategoryList,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
} from './actions';
import messages from '../messages';
import roles from '../../../../server/utils/authorization/roles';

export class EditStaffPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
    this.syncStateWithLocation = this.syncStateWithLocation.bind(this);

    this.state = {
      showModal: false,
      selectedTab: 0,
    };
  }

  componentDidMount() {
    this.props.loadUser({ id: this.props.routeParams.id });
    this.props.fetchMedServiceList();
    this.props.fetchServiceCategoryList();
    this.syncStateWithLocation();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.editStaff._id === this.props.routeParams.id) {
      nextProps.editStaff.repeatPassword = nextProps.editStaff.password;
      this.props.initialize('StaffForm', nextProps.editStaff);
    }
  }

  onTabClick(tabNumber) {
    this.setState({ selectedTab: tabNumber });

    if (tabNumber === STAFF.WORKING_HOURS) {
      this.props.push(`/main/staff/${this.props.routeParams.id}/working-hours`);
    } else {
      this.props.push(`/main/staff/${this.props.routeParams.id}`);
    }
  }

  syncStateWithLocation() {
    if (this.props.location.pathname.split('/')[4] === 'working-hours') {
      this.setState({ selectedTab: STAFF.WORKING_HOURS });
    }
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  handleCancel() {
    this.props.push('/main/staff');
  }

  handleSubmit(params) {
    this.props.updateUser({
      users: this.props.users,
      user: { ...params, services: this.props.chosenServices },
    });
    this.props.push('/main/staff');
  }

  handleDelete() {
    this.props.deleteUser({ users: this.props.users, userId: this.props.routeParams.id });
  }

  render() {
    const { formatMessage } = this.context.intl;
    const { showModal, selectedTab } = this.state;
    const { editStaff, user, headerPanel } = this.props;
    const firstname = editStaff ? editStaff.firstName : '';
    const lastname = editStaff ? editStaff.lastName : '';
    const honoricTitle = this.props.editStaff && this.props.editStaff.title ? this.props.editStaff.title.honorific : '';
    let honor = '';
    switch (honoricTitle) {
      case 'DR':
        honor = 'Dr.';
        break;
      case 'PROFF':
        honor = 'Proff.';
        break;
      case 'DR_MED':
        honor = 'Dr. Med.';
        break;
      default:
        break;
    }

    const isUserAllowed = user && (this.props.routeParams.id === user._id || user.role.name === roles.ADMIN);
    return isUserAllowed ? (
      <div>
        {showModal && <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.toggleModal}
          headerText="Delete Staff Profile"
          bodyText="Are you sure you want to delete this profile?"
        />}
        { headerPanel }
        <Helmet title="MedORG - Staff Directory - Staff Profile" />
        <PaddingTop>
          <TableContent>
            <LeftSide>
              <FormLegend
                Icon={StaffProfileIcon}
                text={this.context.intl.formatMessage(messages.StaffProfile)}
                secondText={lastname && firstname ? `${honor} ${lastname}, ${firstname}` : ''}
              />
              <Tabs
                onTabClick={this.onTabClick}
                value={selectedTab}
                iconActiveColor={theme.Tab.MainEntity.Staff.icon.color}
              >
                <Tab
                  value={0}
                  icon={<PersonalDetailsIcon />}
                  label={formatMessage(messages.PersonalDetails)}
                />
                <Tab
                  value={1}
                  icon={<MedicalServiceIcon />}
                  label={formatMessage(messages.MedicalServices)}
                />
                <Tab
                  value={STAFF.WORKING_HOURS}
                  icon={<WorkingHoursIcon />}
                  label={formatMessage(messages.WorkingHours)}
                >
                </Tab>
              </Tabs>
              {
                selectedTab !== STAFF.WORKING_HOURS ?
                  <Form
                    onSubmit={this.handleSubmit}
                    onCancel={this.handleCancel}
                    roles={this.props.roles}
                    services={this.props.services}
                    serviceCategories={this.props.serviceCategories}
                    isEdit
                    handleChangeService={this.props.handleChangeService}
                    handleChangeServiceCategory={this.props.handleChangeServiceCategory}
                    handleChangeAllServices={this.props.handleChangeAllServices}
                    chosenServices={this.props.chosenServices}
                    chosenServiceCategories={this.props.chosenServiceCategories}
                    allServices={this.props.allServices}
                    selectedTab={selectedTab}
                    onDelete={this.toggleModal}
                    isAdmin={this.props.user && this.props.user.role.name === roles.ADMIN}
                  /> :
                  this.props.children
              }
            </LeftSide>
            <RightSide>
              {
                <SecondaryButton
                  disabled={this.props.location.pathname.includes('/new')}
                  style={{ margin: '0 0 8px',
                  width: 260,
                  visibility: this.state.selectedTab === STAFF.WORKING_HOURS ? 'visible' : 'hidden',
                  marginBottom: '17px',
                 }}
                  link={`/main/staff/${this.props.routeParams.id}/working-hours/patterns/new`}
                >
                  {formatMessage(messages.NewSchedulePattern)}
                </SecondaryButton>
              }
              <MiniCard staff={this.props.editStaff} withContacts={selectedTab !== 0} />
              { this.state.selectedTab === STAFF.WORKING_HOURS && <Calendar style={{ marginBottom: 20 }} />}
            </RightSide>
          </TableContent>
        </PaddingTop>
      </div>
    ) : <MessageAllowed />;
  }
}


EditStaffPage.propTypes = {
  editStaff: PropTypes.object,
  roles: PropTypes.array,
  routeParams: PropTypes.object,
  users: PropTypes.array,
  push: PropTypes.func,
  initialize: PropTypes.func.isRequired,
  loadUser: PropTypes.func,
  fetchMedServiceList: PropTypes.func,
  fetchServiceCategoryList: PropTypes.func,
  children: PropTypes.any,
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  handleChangeService: PropTypes.func,
  handleChangeServiceCategory: PropTypes.func,
  handleChangeAllServices: PropTypes.func,
  chosenServices: PropTypes.array,
  chosenServiceCategories: PropTypes.array,
  allServices: PropTypes.bool,
  updateUser: PropTypes.func,
  deleteUser: PropTypes.func,
  location: PropTypes.object,
  user: PropTypes.object,
};


EditStaffPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};


function mapStateToProps(state) {
  return {
    editStaff: state.editStaffPage.editStaff,
    roles: state.main.roleList,
    users: state.main.users,
    services: state.editStaffPage.services,
    serviceCategories: state.editStaffPage.serviceCategories,
    chosenServices: state.editStaffPage.chosenServices,
    chosenServiceCategories: state.editStaffPage.chosenServiceCategories,
    allServices: state.editStaffPage.allServices,
    user: state.App.user,
    company: state.main.company,
  };
}


const mapDispatchToProps = {
  loadUser,
  updateUser,
  deleteUser,
  fetchMedServiceList,
  fetchServiceCategoryList,
  push,
  initialize,
  handleChangeService,
  handleChangeServiceCategory,
  handleChangeAllServices,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditStaffPage);
