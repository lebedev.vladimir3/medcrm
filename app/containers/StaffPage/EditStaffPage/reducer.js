/*
 *
 * EditStaffPage reducer
 *
 */

import {
  setServiceValue,
  setServiceCategoryValue,
  setAllServicesValue,
} from 'components/MedServiceFormWidget/utils';

import {
  FETCH_USER_SUCCESS,
  FETCH_SERVICE_LIST_SUCCESS,
  // FETCH_CATEGORY_LIST_SUCCESS,
  SET_SERVICE_VALUE,
  SET_SERVICE_CATEGORY_VALUE,
  SET_ALL_SERVICES_VALUE,
  // FETCH_ROLE_LIST_SUCCESS,
} from './constants';

// const initialState = { editStaff: {}, roles: [] };
const initialState = {
  editStaff: {},
  serviceCategories: [],
  chosenServices: [],
  chosenServiceCategories: [],
  allServices: false,
};

function editStaffPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER_SUCCESS:
      return { ...state, editStaff: action.user, chosenServices: action.user.services };

    case FETCH_SERVICE_LIST_SUCCESS:
      return { ...state, serviceCategories: action.payload };

    // case FETCH_ROLE_LIST_SUCCESS:
    //   return { ...state, roles: action.roles };

    case SET_SERVICE_VALUE:
      return setServiceValue(state, action);

    case SET_SERVICE_CATEGORY_VALUE:
      return setServiceCategoryValue(state, action);

    case SET_ALL_SERVICES_VALUE:
      return setAllServicesValue(state, action);

    default:
      return state;
  }
}

export default editStaffPageReducer;
