import { takeLatest, call, put, take, cancel } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';

import {
  // FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  UPDATE_USER_REQUEST,
  // UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  DELETE_USER_REQUEST,
  // DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
  // FETCH_ROLE_LIST_REQUEST,
  // FETCH_ROLE_LIST_SUCCESS,
  // FETCH_ROLE_LIST_FAILURE,
  UPDATE_USER_IN_STATE,
  DELETE_USER_FROM_STATE,
  LOAD_USER,

  FETCH_SERVICE_LIST_REQUEST,
  FETCH_SERVICE_LIST_SUCCESS,
  FETCH_SERVICE_LIST_FAILURE,
} from './constants';


// function* loadUserFromStateById({ payload }) {
//   const searchUserById = (users, id) => users && id ? users.find((user) => user._id === id) : null;
//   try {
//     const user = searchUserById(payload.users, payload.id);
//     yield put({ type: FETCH_USER_SUCCESS, user });
//   } catch (error) {
//     yield put({ type: FETCH_USER_FAILURE, error: error.message });
//   }
// }

export function* fetchUserSaga(action) {
  const id = action.payload.id;

  try {
    const response = yield callApi(axios.get, `/api/users/${id}`);
    yield put({ type: FETCH_USER_SUCCESS, user: response.data });
  } catch (error) {
    yield put({ type: FETCH_USER_FAILURE, error: error.message });
  }
}


export function* updateUserSaga(action) {
  const { users, user } = action.payload;
  const id = user._id;
  try {
    const response = yield callApi(axios.put, `/api/users/${id}`, user);
    yield put({ type: FETCH_USER_SUCCESS, user: response.data });
    const newUsersArray = users.filter((usr) => usr._id !== id);
    newUsersArray.push(response.data);
    yield put({ type: UPDATE_USER_IN_STATE, data: newUsersArray });
  } catch (error) {
    yield put({ type: UPDATE_USER_FAILURE, error: error.message });
  }
}


export function* deleteUserSaga(action) {
  const { users, id } = action.payload;
  try {
    const response = yield callApi(axios.delete, `/api/users/${id}`, action.user);
    yield put({ type: FETCH_USER_SUCCESS, user: response.data });
    const newUsersArray = users.filter((user) => user._id !== id);
    yield put({ type: DELETE_USER_FROM_STATE, data: newUsersArray });
    browserHistory.push('/main/staff');
  } catch (error) {
    yield put({ type: DELETE_USER_FAILURE, error: error.message });
  }
}


// export function* fetchRoleListSaga(action) {
//   try {
//     const response = yield callApi(axios.get, '/api/roles/');
//     yield put({ type: FETCH_ROLE_LIST_SUCCESS, roles: response.data });
//     console.log('roles,', response)
//   } catch (error) {
//     yield put({ type: FETCH_ROLE_LIST_FAILURE, error: error.message });
//   }
// }


function* fetchServiceList() {
  try {
    const serviceResponse = yield callApi(axios.get, '/api/med-services');
    const categoryResponse = yield callApi(axios.get, '/api/med-services/categories');

    yield put({
      type: FETCH_SERVICE_LIST_SUCCESS,
      payload: categoryResponse.data.map((category) => ({
        ...category,
        services: serviceResponse.data.filter((service) => service.category === category._id)
      })),
    });
  } catch (error) {
    yield put({ type: FETCH_SERVICE_LIST_FAILURE, error: error.message });
  }
}


// export function* defaultSaga() {
//   yield takeLatest(FETCH_USER_REQUEST, fetchUserSaga);
//   yield takeLatest(UPDATE_USER_REQUEST, updateUserSaga);
//   yield takeLatest(DELETE_USER_REQUEST, deleteUserSaga);
//   yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleListSaga);
// }


function* updateUserRequestSaga() {
  const update = yield takeLatest(UPDATE_USER_REQUEST, updateUserSaga);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(update);
}

function* deleteUserRequestSaga() {
  const del = yield takeLatest(DELETE_USER_REQUEST, deleteUserSaga);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(del);
}




// function* fetchRoleRequestSaga() {
//   const fetch = yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleListSaga);
//   yield take('@@router/LOCATION_CHANGE');
//   yield cancel(fetch);
// }

function* loadUser() {
  const load = yield takeLatest(LOAD_USER, fetchUserSaga);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(load);
}


function* fetchServiceListSaga() {
  const saga = yield takeLatest(FETCH_SERVICE_LIST_REQUEST, fetchServiceList);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(saga);
}


export default [
  // defaultSaga,
  // fetchUserRequestSaga,
  updateUserRequestSaga,
  deleteUserRequestSaga,
  fetchServiceListSaga,
  // fetchRoleRequestSaga,
  loadUser,
];
