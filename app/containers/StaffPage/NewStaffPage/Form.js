import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import StaffForm, { validate } from 'components/StaffForm';


let Form = reduxForm({
  form: 'StaffForm',
  validate,
})(StaffForm);


const selector = formValueSelector('StaffForm');


Form = connect(
  state => {
    const birth = selector(state, 'birth');
    return { birth };
  }
)(Form);


export default Form;
