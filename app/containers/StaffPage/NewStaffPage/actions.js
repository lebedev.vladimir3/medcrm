/*
 *
 * NewStaffPage actions
 *
 */



import {
  CREATE_USER_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
} from './constants';

export function createUser(params) {
  return { type: CREATE_USER_REQUEST, payload: params };
}

export function fetchRoles() {
  return { type: FETCH_ROLE_LIST_REQUEST };
}
