/*
 *
 * NewStaffPage constants
 *
 */

export const CREATE_USER_REQUEST = 'app/NewStaffPage/CREATE_USER_REQUEST';
export const CREATE_USER_SUCCESS = 'app/NewStaffPage/CREATE_USER_SUCCESS';
export const CREATE_USER_FAILURE = 'app/NewStaffPage/CREATE_USER_FAILURE';

export const FETCH_ROLE_LIST_REQUEST = 'app/EditStaffPage/FETCH_ROLE_LIST_REQUEST';
export const FETCH_ROLE_LIST_SUCCESS = 'app/EditStaffPage/FETCH_ROLE_LIST_SUCCESS';
export const FETCH_ROLE_LIST_FAILURE = 'app/EditStaffPage/FETCH_ROLE_LIST_FAILURE';

export { ADD_NEW_USER_TO_STATE } from 'containers/MainContainer/constants';


export const SET_ALL_SERVICES_VALUE = 'app/NewStaffPage/SET_ALL_SERVICES_VALUE';
export const SET_SERVICE_CATEGORY_VALUE = 'app/NewStaffPage/SET_SERVICE_CATEGORY_VALUE';
export const SET_SERVICE_VALUE = 'app/NewStaffPage/SET_SERVICE_VALUE';
