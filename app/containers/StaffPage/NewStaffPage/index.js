/*
 *
 * NewStaffPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { push } from 'react-router-redux';
import { Link } from 'components/common/HeaderPanel';
import AddNewStaffIcon from 'assets/icons/Title/AddNewStaff.svg';
import MessageAllowed from 'components/NotAllowed';
import FormLegend from 'components/FormLegend';
import { initialize, change } from 'redux-form';

import Form from './Form';
import { createUser } from './actions';
import messages from '../messages';
import roles from '../../../../server/utils/authorization/roles';
import { HeaderPanelHelper } from 'components/common/HeaderPanel';
import { FormWrapper } from 'components/styles/Grid';


export class NewStaffPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  handleCancel() {
    this.props.dispatch(push('/main/staff'));
  }

  handleSubmit(formData) {
    this.props.dispatch(createUser(formData));
    this.props.dispatch(this.props.push('/main/staff'));
  }

  componentDidMount() {
    this.props.dispatch(initialize('StaffForm', { title: { personal: 'mr' }, birth: {} }));
  }

  render() {
    const {
      user,
      headerPanel
    } = this.props;
    const { formatMessage } = this.context.intl;

    const isUserAllowed = user && (this.props.routeParams.id === user._id || user.role.name === roles.ADMIN);
    return isUserAllowed ? (
      <div>
        <Helmet
          title="MedORG - Staff Directory - New Staff"
          meta={[
            { name: 'description', content: 'Description of NewStaffPage' },
          ]}
        />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 1 })}
        <FormWrapper>
          <FormLegend
            Icon={AddNewStaffIcon}
            text={formatMessage(messages.NewStaff)}
          />
          <Form
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            roles={this.props.roles}
            isEdit={false}
            isAdmin={this.props.user && this.props.user.role.name === roles.ADMIN}
          />
        </FormWrapper>
      </div>
    ) : <MessageAllowed />;
  }
}

NewStaffPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  roles: PropTypes.array,
};

NewStaffPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    roles: state.main.roleList,
    user: state.App.user,
    company: state.main.company,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    push,
    initialize,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(NewStaffPage);

