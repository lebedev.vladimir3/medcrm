/*
 *
 * NewStaffPage reducer
 *
 */

import {
  FETCH_ROLE_LIST_SUCCESS,
} from './constants';


const initialState = { roles: [] };

function newStaffPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ROLE_LIST_SUCCESS:
      return { ...state, roles: action.roles };
    default:
      return state;
  }
}

export default newStaffPageReducer;
