import axios from 'axios';
import callApi from 'utils/callApi';
import { call, put, take, cancel, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import {
  CREATE_USER_REQUEST,
  CREATE_USER_FAILURE,
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_FAILURE,
  ADD_NEW_USER_TO_STATE,
} from './constants';


export function* fetchUserSaga(action) {
  const params = action.payload;
  try {
    const response = yield callApi(axios.post, '/api/users/', params);
    yield put({ type: ADD_NEW_USER_TO_STATE, data: response.data });
    browserHistory.push(`/main/staff/${response.data._id}`); // eslint-disable-line no-underscore-dangle
  } catch (error) {
    yield put({ type: CREATE_USER_FAILURE, error: error.message });
  }
}


export function* fetchRoleListrSaga(action) {
  try {
    yield callApi(axios.post, '/api/roles/');
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_FAILURE, error: error.message });
  }
}


export function* fetchRoleListSaga(action) {
  try {
    const response = yield callApi(axios.get, '/api/roles/');
    yield put({ type: FETCH_ROLE_LIST_SUCCESS, roles: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_FAILURE, error: error.message });
  }
}


// export function* defaultSaga() {
//   yield takeLatest(CREATE_USER_REQUEST, fetchUserSaga);
//   yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleListSaga);
// }

export function* fetchRolesSaga() {
  const fetchRoles = yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleListSaga);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(fetchRoles);
}

export function* createUserSaga() {
  const create = yield takeLatest(CREATE_USER_REQUEST, fetchUserSaga);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(create);
}

export default [
//   defaultSaga,
  fetchRolesSaga,
  createUserSaga,
];
