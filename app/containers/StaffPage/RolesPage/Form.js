import { reduxForm } from 'redux-form';
import RoleListForm from 'components/RoleListForm';


export default reduxForm({
  form: 'rolesForm'
})(RoleListForm);
