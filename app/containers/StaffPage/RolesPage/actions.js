/*
 *
 * RolesPage actions
 *
 */

import {
  CREATE_ROLE_REQUEST,
  FETCH_ROLE_LIST_REQUEST,
  UPDATE_ROLE_LIST_REQUEST,
  DELETE_ROLE_REQUEST,
} from './constants';


export function createRole(role) {
  return {
    type: CREATE_ROLE_REQUEST,
    role,
  };
}


export function fetchRoleList(roleId) {
  return {
    type: FETCH_ROLE_LIST_REQUEST,
  };
}


export function updateRoleList(role) {
  return {
    type: UPDATE_ROLE_LIST_REQUEST,
    role,
  };
}


export function deleteRole(roleId) {
  return {
    type: DELETE_ROLE_REQUEST,
    id: roleId,
  };
}