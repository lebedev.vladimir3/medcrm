/*
 *
 * RolesPage constants
 *
 */

export const CREATE_ROLE_REQUEST = 'app/NewStaffPage/CREATE_ROLE_REQUEST';
export const CREATE_ROLE_SUCCESS = 'app/NewStaffPage/CREATE_ROLE_SUCCESS';
export const CREATE_ROLE_FAILURE = 'app/NewStaffPage/CREATE_ROLE_FAILURE';

export const FETCH_ROLE_LIST_REQUEST = 'app/NewStaffPage/FETCH_ROLE_LIST_REQUEST';
export const FETCH_ROLE_LIST_SUCCESS = 'app/NewStaffPage/FETCH_ROLE_LIST_SUCCESS';
export const FETCH_ROLE_LIST_FAILURE = 'app/NewStaffPage/FETCH_ROLE_LIST_FAILURE';

export const UPDATE_ROLE_LIST_REQUEST = 'app/NewStaffPage/UPDATE_ROLE_LIST_REQUEST';
export const UPDATE_ROLE_LIST_SUCCESS = 'app/NewStaffPage/UPDATE_ROLE_LIST_SUCCESS';
export const UPDATE_ROLE_LIST_FAILURE = 'app/NewStaffPage/UPDATE_ROLE_LIST_FAILURE';

export const DELETE_ROLE_REQUEST = 'app/NewStaffPage/DELETE_ROLE_REQUEST';
export const DELETE_ROLE_SUCCESS = 'app/NewStaffPage/DELETE_ROLE_SUCCESS';
export const DELETE_ROLE_FAILURE = 'app/NewStaffPage/DELETE_ROLE_FAILURE';
