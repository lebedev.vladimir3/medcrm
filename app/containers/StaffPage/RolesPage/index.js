/*
 *
 * RolesPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { initialize } from 'redux-form';
import { push } from 'react-router-redux';
import { Link } from 'components/common/HeaderPanel';

import { HeaderPanelHelper } from 'components/common/HeaderPanel';

import PersonAdd from 'react-icons/lib/md/person-add';
import ManageStaffRolesIcons from 'assets/icons/Title/ManageStaffRoles.svg';
import FormLegend from 'components/FormLegend';
import MessageAllowed from 'components/NotAllowed';

import { fetchRoleList, updateRoleList } from './actions';
import Form from './Form';
import messages from '../messages';
import { PageWrapper } from 'components/styles/Grid';
import roles from '../../../../server/utils/authorization/roles';


export class RolesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(fetchRoleList());
  }

  componentDidUpdate() {
    if (this.props.roleList.length > 0) {
      this.props.initializeRoles(this.props.roleList);
    }
  }

  handleSubmit(params) {
    this.props.dispatch(updateRoleList(params));
  }

  handleCancel() {
    this.props.dispatch(push('/main/staff'));
  }

  render() {
    const { headerPanel, user } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const isUserAllowed = user && (this.props.routeParams.id === user._id || user.role.name === roles.ADMIN);


    return isUserAllowed ? (
      <div>
        <Helmet title="MedORG - Staff Directory - Roles Management" />
        {HeaderPanelHelper.disableRightButtonInOrder({ component: headerPanel, buttonNumber: 0 })}
        <PageWrapper>
          <FormLegend
            Icon={ManageStaffRolesIcons}
            text={formatMessage(messages.ManageStaffRoles)}
          />
          <Form onSubmit={this.handleSubmit} onCancel={this.handleCancel} roles={this.props.roleList} />
        </PageWrapper>
      </div>
    ) : <MessageAllowed />;
  }
}

RolesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
RolesPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    roleList: state.rolesPage.roleList,
    company: state.main.company,
    user: state.App.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    initializeRoles: roles => {
      console.log(roles)
      dispatch(initialize('rolesForm', roles));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RolesPage);
