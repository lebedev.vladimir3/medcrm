/*
 *
 * RolesPage reducer
 *
 */

import {
  FETCH_ROLE_LIST_SUCCESS,
} from './constants';

const initialState = {
  roleList: [],
};

function rolesPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ROLE_LIST_SUCCESS:
      return { ...state, roleList: action.roleList };
    default:
      return state;
  }
}

export default rolesPageReducer;
