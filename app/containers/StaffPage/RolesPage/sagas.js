import { takeLatest, call, put } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';

import {
  FETCH_ROLE_LIST_REQUEST,
  FETCH_ROLE_LIST_SUCCESS,
  FETCH_ROLE_LIST_FAILURE,
  UPDATE_ROLE_LIST_REQUEST,
  UPDATE_ROLE_LIST_SUCCESS,
  UPDATE_ROLE_LIST_FAILURE,
  DELETE_ROLE_REQUEST,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_FAILURE,
} from './constants';


export function* fetchRoleListSaga(action) {
  const id = action.id;

  try {
    const response = yield callApi(axios.get, `/api/roles/`);
    yield put({ type: FETCH_ROLE_LIST_SUCCESS, roleList: response.data });
  } catch (error) {
    yield put({ type: FETCH_ROLE_LIST_FAILURE, error: error.message });
  }
}


export function* updateRoleSaga(action) {
  const id = action.role._id;

  try {
    const response = yield callApi(axios.put, `/api/roles/`, action.role);
    yield put({ type: UPDATE_ROLE_LIST_SUCCESS, role: response.data });
  } catch (error) {
    yield put({ type: UPDATE_ROLE_LIST_FAILURE, error: error.message });
  }
}


export function* deleteRoleSaga(action) {
  const id = action.id;

  try {
    const response = yield callApi(axios.delete, `/api/roles/${id}`, action.role);
    yield put({ type: DELETE_ROLE_SUCCESS, role: response.data });
  } catch (error) {
    yield put({ type: DELETE_ROLE_FAILURE, error: error.message });
  }
}


export function* defaultSaga() {
  yield takeLatest(FETCH_ROLE_LIST_REQUEST, fetchRoleListSaga);
  yield takeLatest(UPDATE_ROLE_LIST_REQUEST, updateRoleSaga);
  yield takeLatest(DELETE_ROLE_REQUEST, deleteRoleSaga);
}


export default [
  defaultSaga,
];
