/*
 *
 * StaffPage constants
 *
 */

export const LOAD_USERS = 'app/StaffPage/LOAD_USERS';
export const REQUEST_ERROR = 'app/StaffPage/REQUEST_ERROR';
export const FETCH_USERS = 'app/StaffPage/FETCH_USERS';
