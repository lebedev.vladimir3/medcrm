/*
 *
 * StaffPage
 *
 */

import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { push } from 'react-router-redux';
import MdReceipt from 'assets/icons/Note.svg';
import FormLegend from 'components/FormLegend';
import Card from 'components/Card';
import { Table, TR, TD, TH, SortArrows } from 'components/Table';
import MaleIcon from 'react-icons/lib/fa/mars-stroke-v';
import Venus from 'react-icons/lib/fa/venus';
import StaffDirectoryIcon from 'assets/icons/Title/StaffDirectory.svg';
import { loadUsers } from './actions';
import messages from '../messages';
import { PageWrapper } from 'components/styles/Grid';
import { HonorificParser, genderParser } from './../../../utils';

import './style.scss';

const MailLink = styled.a`
  color: inherit;
  text-decoration: none;
  font-weight: normal;
  color: #215686;
`;

const NotesDiv = styled.div`
  display: inline-block;
  width: 25px;
`;

const Notes = notesProps => <NotesDiv>{notesProps.icon && <MdReceipt />}</NotesDiv>;

export class StaffPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props, context) {
    super(props, context);

    this.onStaffSorted = this.onStaffSorted.bind(this);
    this.onRoleSorted = this.onRoleSorted.bind(this);
    this.state = {
      selectedTab: 1,
      staffList: this.props.users ? this.props.users : [],
      staffSorted: 0,
      roleSorted: 0,
    };
  }

  componentWillMount() {
    this.props.loadUsers();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.users !== this.state.staffList) {
      this.setState({
        staffList: nextProps.users,
      });
    }
  }

  onStaffSorted() {
    const newStaff = this.state.staffList;
    let staffSorted = this.state.staffSorted;

    if (staffSorted === -1 || staffSorted === 0) {
      newStaff.sort((a, b) => {
        if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) return 1;
        if (b.lastName.toLowerCase() > a.lastName.toLowerCase()) return -1;
        return 0;
      });
      staffSorted = 1;
    } else {
      newStaff.sort((a, b) => {
        if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) return 1;
        if (b.lastName.toLowerCase() < a.lastName.toLowerCase()) return -1;
        return 0;
      });
      staffSorted = -1;
    }
    this.setState({
      staffList: newStaff,
      staffSorted,
      roleSorted: 0,
    });
  }

  onRoleSorted() {
    const newStaff = this.state.staffList;
    let roleSorted = this.state.roleSorted;

    if (roleSorted === -1 || roleSorted === 0) {
      newStaff.sort((a, b) => {
        if (typeof a.role === 'undefined' && typeof b.role !== 'undefined') return 1;
        if (typeof a.role !== 'undefined' && typeof b.role === 'undefined') return -1;
        if (typeof a.role === 'undefined' && typeof b.role === 'undefined') return 0;
        if (a.role.name > b.role.name) return 1;
        if (b.role.name > a.role.name) return -1;
        return 0;
      });
      roleSorted = 1;
    } else {
      newStaff.sort((a, b) => {
        if (typeof a.role === 'undefined' && typeof b.role !== 'undefined') return -1;
        if (typeof a.role !== 'undefined' && typeof b.role === 'undefined') return 1;
        if (typeof a.role === 'undefined' && typeof b.role === 'undefined') return 0;

        if (a.role.name < b.role.name) return 1;
        if (b.role.name < a.role.name) return -1;
        return 0;
      });
      roleSorted = -1;
    }

    this.setState({
      staffList: newStaff,
      roleSorted,
      staffSorted: 0,
    });
  }

  render() {
    const { headerPanel } = this.props;
    const formatMessage = this.context.intl.formatMessage;
    const tableRows = this.state.staffList.map((row, index) => {
      const honoricTitle = row.title.honorific ? row.title.honorific : '';
      let honor = HonorificParser.toClientSide(honoricTitle, formatMessage);
      return (
        <TR key={index} link={`/main/staff/${row._id}`}>
          <TD>
            <Notes icon={row.notes} />
          <span>
          {
            honor &&
            <span style={{ fontWeight: 'normal' }}>{honor}&nbsp;</span>
          }
            <span style={{ fontWeight: 600 }}>{row.lastName},&nbsp;</span>
          <span style={{ fontWeight: 'normal' }}>{row.firstName}</span>
          </span>
            <span> {row.title && genderParser(row.title.personal)}</span>
          </TD>
          <TD>{row.role ? row.role.name : ''}</TD>
          <TD><MailLink href={`mailto:${row.email ? row.email : ''}`} onClick={(e) => e.stopPropagation()}>{row.email}</MailLink></TD>
          <TD>{row.phone && row.phone.work ? row.phone.work : ''}</TD>
        </TR>
      );
    });

    return (
      <div>
        <Helmet title="MedORG - Staff Directory" />
        { headerPanel }
        <PageWrapper>
          <FormLegend
            Icon={StaffDirectoryIcon}
            text={formatMessage(messages.StaffDirectory)}
          />
          <Card>
            <Table>
              <thead>
                <TR style={{ borderTop: 'none' }}>
                  <TH onClick={this.onStaffSorted} selected={this.state.staffSorted}>
                    <Notes />
                    {formatMessage(messages.Staff)}
                  </TH>
                  <TH onClick={this.onRoleSorted} selected={this.state.roleSorted}>
                    {formatMessage(messages.Role)}
                  </TH>
                  <TH>{formatMessage(messages.Email)}</TH>
                  <TH>{formatMessage(messages.PhoneNumber)}</TH>
                </TR>
              </thead>
              <tbody>
                {tableRows}
              </tbody>
            </Table>
          </Card>
        </PageWrapper>
      </div>
    );
  }
}

StaffPage.propTypes = {
  push: PropTypes.func.isRequired,
  loadUsers: PropTypes.func.isRequired,
  users: PropTypes.array,
};

StaffPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};
function mapStateToProps(state) {
  return {
    users: state.StaffList.users,
    company: state.main.company,
  };
}

const mapDispatchToProps = {
  loadUsers,
  push,
};

export default connect(mapStateToProps, mapDispatchToProps)(StaffPage);
