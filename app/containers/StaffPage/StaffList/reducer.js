/*
 *
 * StaffPage reducer
 *
 */

import {
  FETCH_USERS,
} from './constants';

const initialState = {
  users: [],
};

function staffPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS:
      return { ...state, users: action.data };
    default:
      return state;
  }
}

export default staffPageReducer;
