import { put, call, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';

import { LOAD_USERS, FETCH_USERS, REQUEST_ERROR } from './constants';


export function loadUsersRequest() {
  return axios.get('/api/users');
}

export function* loadUsers() {
  try {
    const response = yield callApi(loadUsersRequest);
    yield put({ type: FETCH_USERS, data: response.data });
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message });
  }
}

export function* staffSaga() {
  yield takeLatest(LOAD_USERS, loadUsers);
}

// All sagas to be loaded
export default [
  staffSaga,
];
