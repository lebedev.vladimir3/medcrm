import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import SchedulePatternForm, { validate } from 'components/SchedulePatternForm';


const Form = reduxForm({
  form: 'staffSchedulePatternForm',
  validate,
})(SchedulePatternForm);


const selector = formValueSelector('staffSchedulePatternForm');


const ConnectedForm = connect(
  (state) => {
    const repeated = selector(state, 'repeated');
    const singleDay = selector(state, 'singleDay');
    const withEndDate = selector(state, 'withEndDate');
    const type = selector(state, 'type');
    const repeatOn = selector(state, 'repeatOn');
    const hasEndDate = selector(state, 'hasEndDate');
    
    return { repeated, singleDay, withEndDate, type, repeatOn, hasEndDate  };
  }
)(Form);


export default ConnectedForm;
