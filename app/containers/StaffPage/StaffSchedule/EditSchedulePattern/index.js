import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { initialize } from 'redux-form';
import DeleteModal from 'components/DeleteModal';
import OverbookingModal from '../../components/OverbookingModal';
import roles from '../../../../../server/utils/authorization/roles';
import Form from './Form';
import {
  updateUserSchedulePattern,
  deleteUserSchedulePattern,
  fetchUserSchedulePattern,
  hideOverbookingModal,
} from '../actions';
import messages from './../../messages';

export class EditSchedulePattern extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleForceSubmit = this.handleForceSubmit.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);

    this.state = { showModal: false, initialized: false, formData: {}, isFormSubmitted: false };
    this.props.hideOverbookingModal();
  }

  componentWillMount() {
    const { id, patternId } = this.props.routeParams;
    this.props.fetchUserSchedulePattern({ id, patternId });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pattern._id === this.props.routeParams.patternId && !this.state.initialized) {
      this.props.initialize('staffSchedulePatternForm', nextProps.pattern);
      this.setState({ initialized: true });
    }
  }

  handleCancel() {
    this.props.push(`/main/staff/${this.props.routeParams.id}/working-hours#patterns`);
  }

  onSubmitClick() {
    this.setState({ isFormSubmitted: true });
  }
  
  handleSubmit(formData) {
    const { id, patternId } = this.props.routeParams;
    this.props.updateUserSchedulePattern({
      userId: id,
      patternId,
      data: formData,
    });
    this.setState({ formData });
  }

  handleDelete() {
    const { id, patternId } = this.props.routeParams;
    this.props.deleteUserSchedulePattern({ userId: id, patternId });
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  handleForceSubmit() {
    const { id, patternId } = this.props.routeParams;
    this.props.updateUserSchedulePattern({
      userId: id,
      patternId,
      data: { ...this.state.formData, force: true },
    });
  }

  render() {
    const isUserAllowed = this.props.user && this.props.user.role.canSetOwnSchedule;
    const { overbookingModalOpen } = this.props;
    const { formatMessage } = this.context.intl;

    return (
      <div>
        <Helmet title="MedORG - Staff Directory - Staff Profile - New Schedule Pattern" />
        {overbookingModalOpen &&
        <OverbookingModal
          onCancel={this.props.hideOverbookingModal}
          onSubmit={this.handleForceSubmit}
        />
        }
        <Form
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          onDelete={this.showModal}
          forStaff
          canEdit={isUserAllowed}
          isEdit
          onSubmitClick={this.onSubmitClick}
          isFormSubmitted={this.state.isFormSubmitted}
          schedulesLink={'/main/staff/'+this.props.params.id+'/working-hours#patterns'}
        />
        {this.state.showModal && isUserAllowed && <DeleteModal
          handleDelete={this.handleDelete}
          handleCancel={this.hideModal}
          headerText={formatMessage(messages.DeleteModalTitle)}
          bodyText={formatMessage(messages.DeleteModalText)}
        />}
      </div>
    );
  }
}

EditSchedulePattern.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

EditSchedulePattern.propTypes = {
  routeParams: PropTypes.object,
  push: PropTypes.func,
  initialize: PropTypes.func.isRequired,
  fetchUserSchedulePattern: PropTypes.func,
  updateUserSchedulePattern: PropTypes.func,
  deleteUserSchedulePattern: PropTypes.func,
  hideOverbookingModal: PropTypes.func,
  overbookingModalOpen: PropTypes.bool,
};


function mapStateToProps(state) {
  return {
    pattern: state.mySchedulePage.pattern,
    overbookingModalOpen: state.mySchedulePage.overbookingModalOpen,
    user: state.App.user,
  };
}


const mapDispatchToProps = {
  fetchUserSchedulePattern,
  updateUserSchedulePattern,
  deleteUserSchedulePattern,
  hideOverbookingModal,
  push,
  initialize,
};


export default connect(mapStateToProps, mapDispatchToProps)(EditSchedulePattern);
