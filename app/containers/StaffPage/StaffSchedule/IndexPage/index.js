import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { push } from 'react-router-redux';
import moment from 'moment';
import Card, { CardFooter } from 'components/Card';
import { calculateSummaryFromMinutes, calculateWorkingHours } from 'utils';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';
//import { Tab, TabList, Divider } from 'components/InnerTabs';
import Helmet from 'react-helmet';
import { InputButton } from 'components/common/Button';
import WorkingHoursTable from 'components/WorkingHoursTable';
import SchedulePatternTable from 'components/SchedulePatternTable';
import { fetchUserSchedulePatternList, fetchWorkingHours, fetchOpeningHours } from '../actions';
import FaAngelLeft from 'assets/icons/Arrows/DarkIcons/ArrowLeft.svg';
import FaAngelRight from 'assets/icons/Arrows/DarkIcons/ArrowRight.svg';
import messages from './../../messages';

import { Tabs, Tab, TabsThemes } from 'components/common/Tabs';
import { MAIN } from 'constants/routes';
import FaPrint from 'react-icons/lib/fa/print';
const SCHEDULE = MAIN.SCHEDULE;

const StyledCardFooter = styled(CardFooter)`
  margin-top: 0px;
  display: flex;
  justify-content: space-between;
  background-color: #F2F2F2;
`;
const PaginationButton = styled(InputButton)`
  height: 40px;
  width: 40px;
  text-align: center;
  margin-left: 0px;
`;

const NavButtonGroup = styled.div`
  display: flex;
  margin-left: auto;
`;

const NavButton = styled.div`
  height: 40px;
  margin-top: auto;
  margin-bottom: auto;
	padding: 9px 14px 12px 17px;
	border-radius: 4px 0 0 4px;
	border: 1px solid #ABB2C4;
	background-color: ${props => props.active ? '#8B96AD' : "#F6F7F9"};
	color: ${props => props.active ? '#FFFFFF' : '#4A5360'};
	font-size: ${fontSetting.baseSizePlus1}px;
	font-weight: 600;
	line-height: 19px;
	text-align: center;
`;

const IconNav = styled.div`
	height: 40px;
	width: 40px;
	border: 1px solid #ABB2C4;
	border-radius: 4px;
	background-color: #F6F7F9;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: auto;
  margin-bottom: auto;
  margin-right: 20px;
`;

export class MyScheduleIndexPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    //this.handleFirstTabClick = this.handleFirstTabClick.bind(this);
    //this.handleSecondTabClick = this.handleSecondTabClick.bind(this);
    this.handleTabClick = ::this.handleTabClick;
    this.getDateRange = this.getDateRange.bind(this);
    this.handlePeriodChange = this.handlePeriodChange.bind(this);
    this.updateWorkingHours = this.updateWorkingHours.bind(this);
    this.handleOffsetIncrement = this.handleOffsetIncrement.bind(this);
    this.handleOffsetDecrement = this.handleOffsetDecrement.bind(this);
    this.onPdfClick = this.onPdfClick.bind(this);

    this.state = { period: 'week', offset: 0 };
  }

  componentWillMount() {
    this.props.fetchUserSchedulePatternList({ userId: this.props.params.id });
    this.updateWorkingHours();
  }

  getDateRange(value) {
    const { offset } = this.state;
    switch (value) {
      case 'week':
      default:
        return {
          dateFrom: moment().add(offset, value).startOf('isoweek').format('YYYY-MM-DD'),
          dateTo: moment().add(offset, value).endOf('isoweek').format('YYYY-MM-DD'),
        };
      case 'month':
        return {
          dateFrom: moment().add(offset, value).startOf('month').startOf('isoweek').format('YYYY-MM-DD'),
          dateTo: moment().add(offset, value).endOf('month').endOf('isoweek').format('YYYY-MM-DD'),
        };
    }
  }

  updateWorkingHours() {
    this.props.fetchWorkingHours({
      userId: this.props.params.id,
      ...this.getDateRange(this.state.period),
    });
    this.props.fetchOpeningHours({
      userId: this.props.params.id,
      ...this.getDateRange(this.state.period),
    });

  }

  handleTabClick(key) {
    let url = this.props.location.pathname;
    switch (key) {
      case SCHEDULE.WORKING_HOURS:
      {
        url += SCHEDULE.WORKING_HOURS;
        break;
      }
      case SCHEDULE.PATTERNS:
      {
        url += SCHEDULE.PATTERNS;
        break;
      }
    }
    this.props.push(url);
  }

  handlePeriodChange(value) {
    this.setState({ period: value, offset: 0 }, () => {
      this.updateWorkingHours();
    });
  }

  handleOffsetIncrement() {
    this.setState({ offset: this.state.offset + 1 }, () => {
      this.updateWorkingHours();
    });
  }

  handleOffsetDecrement() {
    this.setState({ offset: this.state.offset - 1 }, () => {
      this.updateWorkingHours();
    });
  }

  onPdfClick(table, openingHours) {

    const columns = [moment(table[0].date).format('YYYY'),
      'Date',
      'Opening Hours',
      'Working Hours',
      'Title',
      'Summary Hours',
    ];
    let rows = [],
      week = [],
      workedHours = 0,
      hoursWorkedInWeek = [];
    table.forEach((row)=> {
      if (moment(row.date).day() === 1) {
        hoursWorkedInWeek.push(workedHours);
        workedHours = 0;
      }
      workedHours += calculateWorkingHours(row.workingHours);
    });
    hoursWorkedInWeek.push(workedHours);
    let iterateOnHoursWorkedInWeek = 1;

    table.forEach((row, index)=> {
      let rowArray = [];
      const isWorkingDay = row.workingHours.length > 0;

      const workingHours = row.workingHours;
      if (moment(row.date).day() === 1) {
        week = [];
        week.push(`Week ${moment(row.date).week()}`);
        week.push('');
        week.push('');
        week.push('');
        week.push('');
        week.push(calculateSummaryFromMinutes(hoursWorkedInWeek[iterateOnHoursWorkedInWeek]));
        rows.push(week);
        iterateOnHoursWorkedInWeek++;
      }

      let workingHoursString = workingHours.length > 0 ? workingHours.map((item, index1) => {
        const timeFrom = moment.utc().startOf('day').add(item.from, 'minutes');
        const timeTo = moment.utc().startOf('day').add(item.to, 'minutes');
        return `${timeFrom.format('HH:mm')} - ${timeTo.format('HH:mm')}`;
      }).join(' ') : '—';

      let openingHoursString = openingHours[index].workingHours.length > 0 ? openingHours[index].workingHours.map((item, index1) => {
        const timeFrom = moment.utc().startOf('day').add(item.from, 'minutes');
        const timeTo = moment.utc().startOf('day').add(item.to, 'minutes');
        return `${timeFrom.format('HH:mm')} - ${timeTo.format('HH:mm')}`        ;
      }) : '—';

      rowArray.push(moment(row.date).format('dddd'));
      rowArray.push(moment(row.date).format('D. MMMM'));

      rowArray.push(openingHoursString);
      rowArray.push(workingHoursString);
      rowArray.push(row.workingHours.length>0 ? row.workingHours[0].title : '—');
      rowArray.push(isWorkingDay ? calculateSummaryFromMinutes(row.summaryMinutes) : '—');
      rows.push(rowArray);
    });
    const tableOptions = {
      styles: {
        overflow: 'linebreak',
        valign: 'middle',
        fontSize: 7,
        minCellWidth: 40,
      },
      columnStyles: {},
    };
    const doc = new jspdf('p', 'pt');
    doc.setFontSize(7);

    doc.text('wowowow', 270, doc.autoTable.previous.finalY - doc.autoTable.previous.height - 5);
    doc.autoTable(columns, rows, {
      ...tableOptions,
      startY: 25,
    });

    doc.save('table.pdf');
  }

  render() {
    const { workingHours, schedulePatterns, push, openingHours } = this.props;
    const { hash } = this.props.location;
    const formatMessage = this.context.intl.formatMessage;
    return (
      <div>
        <Helmet title="MedORG - Staff Directory - Staff Profile - Working Hours" />
        <Card>
          <Tabs
            theme={TabsThemes.STATUS}
            onTabClick={this.handleTabClick}
            value={hash}
            activeColor="#738DC8"
            rightRemainArea={hash !== '#patterns' &&
            <NavButtonGroup>
              <NavButton
                active={this.state.period === 'week'}
                onClick={() => this.handlePeriodChange('week')}
              >
                {formatMessage(messages.Week)}
              </NavButton>
              <NavButton
                active={this.state.period === 'month'}
                onClick={() => this.handlePeriodChange('month')}
                style={{marginRight: '20px'}}
              >
                {formatMessage(messages.Month)}
              </NavButton>
              <IconNav onClick={() => this.onPdfClick(workingHours,openingHours )}><FaPrint /></IconNav>
            </NavButtonGroup>
            }>
            <Tab
              value={hash ? SCHEDULE.WORKING_HOURS: ''}
              label={formatMessage(messages.OpeningSchedule)}
            />
            <Tab
              value={SCHEDULE.PATTERNS}
              label={formatMessage(messages.SchedulePatterns)}
            />
          </Tabs>
          {
            hash !== SCHEDULE.PATTERNS &&
            <WorkingHoursTable
              items={workingHours}
              openingHours={openingHours}
              onPeriodChange={this.handlePeriodChange}
              period={this.state.period}
            />
          }
          {
            hash === SCHEDULE.PATTERNS &&
            <SchedulePatternTable
              items={schedulePatterns}
              workingHours={workingHours}
              onRowClick={(id) => this.props.push(`${this.props.location.pathname}/patterns/${id}`)}
            />
          }

        </Card>
        {
          hash !== SCHEDULE.PATTERNS &&
          <StyledCardFooter>
            <PaginationButton onClick={this.handleOffsetDecrement}>
              <FaAngelLeft />
            </PaginationButton>
            <PaginationButton onClick={this.handleOffsetIncrement}>
              <FaAngelRight />
            </PaginationButton>
          </StyledCardFooter>
        }
      </div>
    );
  }
}

MyScheduleIndexPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

MyScheduleIndexPage.propTypes = {
  location: PropTypes.object,
  workingHours: PropTypes.array,
  schedulePatterns: PropTypes.array,
  push: PropTypes.func,
  fetchWorkingHours: PropTypes.func,
  fetchUserSchedulePatternList: PropTypes.func,
};


function mapStateToProps(state) {
  return {
    workingHours: state.mySchedulePage.workingHours,
    schedulePatterns: state.mySchedulePage.patterns,
    openingHours: state.mySchedulePage.openingHours,
  };
}


const mapDispatchToProps = {
  push,
  fetchUserSchedulePatternList,
  fetchWorkingHours,
  fetchOpeningHours,
};


export default connect(mapStateToProps, mapDispatchToProps)(MyScheduleIndexPage);
