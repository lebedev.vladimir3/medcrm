/*
 *
 * NewSchedulePatternPage
 *
 */
import Helmet from 'react-helmet';

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import roles from '../../../../../server/utils/authorization/roles';
import Form from './Form';
import { createUserSchedulePattern, hideOverbookingModal } from '../actions';
import OverbookingModal from '../../components/OverbookingModal';


export class NewSchedulePatternPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleForceSubmit = this.handleForceSubmit.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);

    this.state = {
      formData: {},
      isFormSubmitted: false
    };
    this.props.hideOverbookingModal();

  }

  handleCancel() {
    this.props.push(`/main/staff/${this.props.routeParams.id}/working-hours#patterns`);
  }

  onSubmitClick() {
    this.setState({ isFormSubmitted: true });
  }

  handleForceSubmit() {
    const { id, patternId } = this.props.routeParams;
    this.props.createUserSchedulePattern({
      userId: id,
      data: { ...this.state.formData, force: true },
    });
  }

  handleSubmit(formData) {
    const { id } = this.props.routeParams;

    this.props.createUserSchedulePattern({ userId: id, data: formData });
    this.setState({ formData });
  }

  render() {
    const isUserAllowed = this.props.user && this.props.user.role.canSetOwnSchedule;

    const { overbookingModalOpen } = this.props;
    return (
      <div>
        <Helmet title="MedORG - Staff Directory - Staff Profile - New Schedule Pattern" />
        {overbookingModalOpen &&
        <OverbookingModal
          onCancel={this.props.hideOverbookingModal}
          onSubmit={this.handleForceSubmit}
        />
        }
        <Form
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          forStaff
          canEdit={isUserAllowed}
          isEdit={false}
          onSubmitClick={this.onSubmitClick}
          isFormSubmitted={this.state.isFormSubmitted}
          schedulesLink={'/main/staff/'+this.props.params.id+'/working-hours#patterns'}
        />
      </div>);
  }
}

NewSchedulePatternPage.propTypes = {
  push: PropTypes.func,
  createUserSchedulePattern: PropTypes.func,
};


function mapStateToProps(state) {
  return {
    user: state.App.user,
    overbookingModalOpen: state.mySchedulePage.overbookingModalOpen,
  };
}

const mapDispatchToProps = {
  push,
  createUserSchedulePattern,
  hideOverbookingModal,
};


export default connect(mapStateToProps, mapDispatchToProps)(NewSchedulePatternPage);

