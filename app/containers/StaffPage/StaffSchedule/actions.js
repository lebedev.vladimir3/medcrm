import {
  FETCH_PATTERN_REQUEST,
  FETCH_PATTERN_LIST_REQUEST,
  CREATE_PATTERN_REQUEST,
  UPDATE_PATTERN_REQUEST,
  DELETE_PATTERN_REQUEST,
  FETCH_WORKING_HOURS_REQUEST,
  HIDE_OVERBOOKING_MODAL,
  FETCH_OPENING_HOURS_REQUEST,
} from './constants';


export const createUserSchedulePattern = (params) => ({ type: CREATE_PATTERN_REQUEST, payload: params });

export const fetchUserSchedulePattern = (params) => ({ type: FETCH_PATTERN_REQUEST, payload: params });

export const fetchUserSchedulePatternList = (params) => ({ type: FETCH_PATTERN_LIST_REQUEST, payload: params });

export const updateUserSchedulePattern = (params) => ({ type: UPDATE_PATTERN_REQUEST, payload: params });

export const deleteUserSchedulePattern = (params) => ({ type: DELETE_PATTERN_REQUEST, payload: params });

export const fetchWorkingHours = (params) => ({ type: FETCH_WORKING_HOURS_REQUEST, payload: params });

export const hideOverbookingModal = () => ({ type: HIDE_OVERBOOKING_MODAL });

export const fetchOpeningHours = (params) => ({ type: FETCH_OPENING_HOURS_REQUEST, payload: params });
