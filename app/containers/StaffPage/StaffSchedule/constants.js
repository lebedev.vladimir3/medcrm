/*
 *
 * ResourcesPage constants
 *
 */

export const FETCH_PATTERN_REQUEST = 'app/STAFF_SCHEDULE/FETCH_PATTERN_REQUEST';
export const FETCH_PATTERN_SUCCESS = 'app/STAFF_SCHEDULE/FETCH_PATTERN_SUCCESS';
export const FETCH_PATTERN_FAILURE = 'app/STAFF_SCHEDULE/FETCH_PATTERN_FAILURE';

export const FETCH_PATTERN_LIST_REQUEST = 'app/STAFF_SCHEDULE/FETCH_PATTERN_LIST_REQUEST';
export const FETCH_PATTERN_LIST_SUCCESS = 'app/STAFF_SCHEDULE/FETCH_PATTERN_LIST_SUCCESS';
export const FETCH_PATTERN_LIST_FAILURE = 'app/STAFF_SCHEDULE/FETCH_PATTERN_LIST_FAILURE';

export const CREATE_PATTERN_REQUEST = 'app/STAFF_SCHEDULE/CREATE_PATTERN_REQUEST';
export const CREATE_PATTERN_SUCCESS = 'app/STAFF_SCHEDULE/CREATE_PATTERN_SUCCESS';
export const CREATE_PATTERN_FAILURE = 'app/STAFF_SCHEDULE/CREATE_PATTERN_FAILURE';

export const UPDATE_PATTERN_REQUEST = 'app/STAFF_SCHEDULE/UPDATE_PATTERN_REQUEST';
export const UPDATE_PATTERN_SUCCESS = 'app/STAFF_SCHEDULE/UPDATE_PATTERN_SUCCESS';
export const UPDATE_PATTERN_FAILURE = 'app/STAFF_SCHEDULE/UPDATE_PATTERN_FAILURE';

export const DELETE_PATTERN_REQUEST = 'app/STAFF_SCHEDULE/DELETE_PATTERN_REQUEST';
export const DELETE_PATTERN_SUCCESS = 'app/STAFF_SCHEDULE/DELETE_PATTERN_SUCCESS';
export const DELETE_PATTERN_FAILURE = 'app/STAFF_SCHEDULE/DELETE_PATTERN_FAILURE';

export const FETCH_WORKING_HOURS_REQUEST = 'app/STAFF_SCHEDULE/FETCH_WORKING_HOURS_REQUEST';
export const FETCH_WORKING_HOURS_SUCCESS = 'app/STAFF_SCHEDULE/FETCH_WORKING_HOURS_SUCCESS';
export const FETCH_WORKING_HOURS_FAILURE = 'app/STAFF_SCHEDULE/FETCH_WORKING_HOURS_FAILURE';

export const FETCH_OPENING_HOURS_REQUEST = 'app/STAFF_SCHEDULE/FETCH_OPENING_HOURS_REQUEST';
export const FETCH_OPENING_HOURS_SUCCESS = 'app/STAFF_SCHEDULE/FETCH_OPENING_HOURS_SUCCESS';
export const FETCH_OPENING_HOURS_FAILURE = 'app/STAFF_SCHEDULE/FETCH_OPENING_HOURS_FAILURE';

export const SHOW_OVERBOOKING_MODAL = 'app/STAFF_SCHEDULE/SHOW_OVERBOOKING_MODAL_SUCCESS';
export const HIDE_OVERBOOKING_MODAL = 'app/STAFF_SCHEDULE/HIDE_OVERBOOKING_MODAL';
