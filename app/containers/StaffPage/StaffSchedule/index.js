/*
 *
 * StaffPage
 *
 */

import React, { PropTypes } from 'react';


const StaffPage = (props) => (
  <div>{props.children}</div>
);

StaffPage.propTypes = {
  children: PropTypes.object,
};


export default StaffPage;
