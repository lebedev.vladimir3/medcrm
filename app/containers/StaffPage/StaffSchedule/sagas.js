import { takeLatest, take, fork, put, cancel } from 'redux-saga/effects';
import axios from 'axios';
import callApi from 'utils/callApi';
import { browserHistory } from 'react-router';
import {
  CREATE_PATTERN_REQUEST,
  CREATE_PATTERN_SUCCESS,
  CREATE_PATTERN_FAILURE,
  FETCH_PATTERN_LIST_REQUEST,
  FETCH_PATTERN_LIST_SUCCESS,
  FETCH_PATTERN_LIST_FAILURE,
  FETCH_PATTERN_REQUEST,
  FETCH_PATTERN_SUCCESS,
  FETCH_PATTERN_FAILURE,
  UPDATE_PATTERN_REQUEST,
  UPDATE_PATTERN_SUCCESS,
  UPDATE_PATTERN_FAILURE,
  DELETE_PATTERN_REQUEST,
  DELETE_PATTERN_SUCCESS,
  DELETE_PATTERN_FAILURE,
  FETCH_WORKING_HOURS_REQUEST,
  FETCH_WORKING_HOURS_SUCCESS,
  FETCH_WORKING_HOURS_FAILURE,
  SHOW_OVERBOOKING_MODAL,
  FETCH_OPENING_HOURS_REQUEST,
  FETCH_OPENING_HOURS_SUCCESS,
  FETCH_OPENING_HOURS_FAILURE,
} from './constants';


const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';


function* createUserSchedulePattern(action) {
  try {
    const { userId, data } = action.payload;
    const response = yield callApi(axios.post, `/api/users/${userId}/schedule-patterns`, data);

    if (response.data.overbooking) {
      yield put({ type: SHOW_OVERBOOKING_MODAL });
    } else {
      yield put({ type: CREATE_PATTERN_SUCCESS });
      browserHistory.push(`/main/staff/${userId}/working-hours#patterns`);
    }
  } catch (error) {
    yield put({ type: CREATE_PATTERN_FAILURE, error: error.message });
  }
}


function* fetchUserSchedulePatternList(action) {
  try {
    const response = yield callApi(axios.get, `/api/users/${action.payload.userId}/schedule-patterns`);
    yield put({ type: FETCH_PATTERN_LIST_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PATTERN_LIST_FAILURE, error: error.message });
  }
}


function* fetchUserSchedulePattern(action) {
  try {
    const { id, patternId } = action.payload;
    const response = yield callApi(axios.get, `/api/users/${id}/schedule-patterns/${patternId}`);
    yield put({ type: FETCH_PATTERN_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_PATTERN_FAILURE, error: error.message });
  }
}


function* updateUserSchedulePattern(action) {
  try {
    const { userId, patternId, data } = action.payload;
    const response = yield callApi(
      axios.put,
      `/api/users/${userId}/schedule-patterns/${patternId}`,
      data
    );

    if (response.data.overbooking) {
      yield put({ type: SHOW_OVERBOOKING_MODAL });
    } else {
      yield put({ type: UPDATE_PATTERN_SUCCESS, categoryList: response.data });
      browserHistory.push(`/main/staff/${userId}/working-hours#patterns`);
    }
  } catch (error) {
    yield put({ type: UPDATE_PATTERN_FAILURE, error: error.message });
  }
}


function* deleteUserSchedulePattern(action) {
  try {
    const { userId, patternId } = action.payload;
    const response = yield callApi(
      axios.delete,
      `/api/users/${userId}/schedule-patterns/${patternId}`
    );
    yield put({ type: DELETE_PATTERN_SUCCESS, payload: response.data });
    browserHistory.push(`/main/staff/${userId}/working-hours#patterns`);
  } catch (error) {
    yield put({ type: DELETE_PATTERN_FAILURE, error: error.message });
  }
}


function* fetchWorkingHours(action) {
  try {
    const response = yield callApi(
      axios.get,
      `/api/users/${action.payload.userId}/working-hours`,
      { params: action.payload }
    );
    yield put({ type: FETCH_WORKING_HOURS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_WORKING_HOURS_FAILURE, error: error.message });
  }
}

function* fetchOpeningHours(action) {
  try {
    const response = yield callApi(axios.get, '/api/working-hours', { params: action.payload });
    yield put({ type: FETCH_OPENING_HOURS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: FETCH_OPENING_HOURS_FAILURE, error: error.message });
  }
}


function* createUserSchedulePatternSaga() {
  const saga = yield fork(takeLatest, CREATE_PATTERN_REQUEST, createUserSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(saga);
}
function* fetchUserSchedulePatternListSaga() {
  const watcher = yield fork(takeLatest, FETCH_PATTERN_LIST_REQUEST, fetchUserSchedulePatternList);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchUserSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, FETCH_PATTERN_REQUEST, fetchUserSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* updateUserSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, UPDATE_PATTERN_REQUEST, updateUserSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* deleteUserSchedulePatternSaga() {
  const watcher = yield fork(takeLatest, DELETE_PATTERN_REQUEST, deleteUserSchedulePattern);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
function* fetchWorkingHoursSaga() {
  const watcher = yield fork(takeLatest, FETCH_WORKING_HOURS_REQUEST, fetchWorkingHours);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

function* fetchOpeningHoursSaga() {
  const watcher = yield fork(takeLatest, FETCH_OPENING_HOURS_REQUEST, fetchOpeningHours);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

export default [
  createUserSchedulePatternSaga,
  fetchUserSchedulePatternListSaga,
  fetchUserSchedulePatternSaga,
  updateUserSchedulePatternSaga,
  deleteUserSchedulePatternSaga,
  fetchWorkingHoursSaga,
  fetchOpeningHoursSaga,
];
