import fontSetting from 'components/styles/font';
import React, { PropTypes } from 'react';
import { Card } from 'components/styles/Card';
import styled from 'styled-components';
import moment from 'moment';
import { HonorificParser } from './../../../utils';

const StyledCard = styled(Card)`
  width: 100%;
  padding: 15px 20px;
  color: #3B4950;
  font-size: ${fontSetting.baseSize}px; 
  line-height: 18px;
`;

const Name = styled.span`
  color: #397165;
`;

const Row = styled.div`
  padding: 5px 0;
  display: flex;
  justify-content: space-between;
`;

const Divider = styled.div`
  margin: 7px 0;
  border-bottom: 1px #DBDFEA dashed;
  width: 100%;
`;

const MailLink = styled.a`
  color: inherit;
  text-decoration: none;
  font-weight: normal;
  color: #215686;
`;


const MiniCard = (props, context) => {
  const { staff, withContacts } = props;
  const { _id, id, firstName, lastName, title, speciality, birth, gender, created, role } = staff;
  const dob = birth ? moment().year(birth.yyyy).month(birth.mm).day(birth.dd) : null;

  const honoricTitle = staff && staff.title ? staff.title.honorific : '';
  let honor = HonorificParser.toClientSide(honoricTitle, context.intl.formatMessage);
  return (
    <StyledCard>
      <Row>
        <span>Staff ID</span>
        <span><strong>{id}</strong></span>
      </Row>
      <Row>
        <span>{honor && honor}&nbsp;{lastName}, {firstName} {title && title.personal === 'mr' ? '♂' : '♀'}</span>
        <strong>
        </strong>
      </Row>
      <Row>
        <span>{role && role.name}</span>
        <i>
          {speciality}
        </i>
      </Row>
      <Row>
        <span>{birth && dob.format('DD.MM.YYYY')}</span>
        <span>
          {birth && moment().diff(dob, 'years')} years
        </span>
      </Row>
      <Divider />
      {
        withContacts ?
          <div>
            {
              staff.phone && staff.phone.work &&
              <Row>
                <span>{staff.phone.work}</span>
              </Row>
            }
            <Row>
              <MailLink href={`mailto:${staff.email}`} onClick={(e) => e.stopPropagation()}>
                {staff.email}
              </MailLink>
            </Row>
          </div>
          :
          <Row>
            <span>Registered</span>
            <span>{moment(created).format('DD.MM.YYYY')}</span>
          </Row>
      }
    </StyledCard>
  );
};

MiniCard.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

MiniCard.propTypes = {
  staff: PropTypes.object,
  withContacts: PropTypes.bool,
};

export default MiniCard;
