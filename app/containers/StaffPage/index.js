/*
 *
 * StaffPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import MessageAllowed from 'components/NotAllowed';
import roles from '../../../server/utils/authorization/roles';

import { push } from 'react-router-redux';
import HeaderPanel, { HeaderItem } from 'components/common/HeaderPanel';
import { ButtonContent, ButtonText } from "components/common/Button/HeaderPanel";
import { BlueButton, SecondaryButton } from "components/common/Button";
import messages from './messages';


const StaffPage = (props, context) => {
  const isUserAllowed = props.user && props.user.role.name === roles.ADMIN;
  return (<div>{React.cloneElement(React.Children.toArray(props.children)[0], {
    headerPanel: (
      <HeaderPanel
        rightSide={[
          <SecondaryButton
            link={'/main/staff/roles'}
            key={1}
          >
            <ButtonContent>
              <ButtonText >{context.intl.formatMessage(messages.ManageStaffRoles)}</ButtonText>
            </ButtonContent>
          </SecondaryButton>,
          <BlueButton
            link={'/main/staff/new'}
            key={2}
          >
            <ButtonContent>
              <ButtonText >{context.intl.formatMessage(messages.NewStaff)}</ButtonText>
            </ButtonContent>
          </BlueButton>
        ]}
      >
        <HeaderItem link={'/main/staff'}>{`${context.intl.formatMessage(messages.StaffDirectory)}`}</HeaderItem>
      </HeaderPanel>)
  })}</div>);
};

const mapDispatchToProps = (dispatch) => ({ dispatch, push });

function mapStateToProps(state) {
  return {
    user: state.App.user,
  };
}

StaffPage.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

StaffPage.propTypes = {
  children: PropTypes.object,
  user: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(StaffPage);
