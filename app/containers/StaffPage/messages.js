import { defineMessages } from 'react-intl';

export default defineMessages({
  NewPatient: {
    id: 'NewPatient',
    defaultMessage: 'New Patient',
  },
  NewStaff: {
    id: 'NewStaff',
    defaultMessage: 'New Staff',
  },
  NewPrescription: {
    id: 'NewPrescription',
    defaultMessage: 'New Prescription',
  },
  Step2: {
    id: 'Step2',
    defaultMessage: 'Step 2 - Medical Services',
  },
  PatientsDirectory: {
    id: 'PatientsPage.PatientsDirectory',
    defaultMessage: 'Patients Directory',
  },
  StaffDirectory: {
    id: 'StaffPage.StaffDirectory',
    defaultMessage: 'Staff Directory',
  },
  ManageStaffRoles: {
    id: 'ManageStaffRoles',
    defaultMessage: 'Manage Staff Roles',
  },
  StaffRoles: {
    id: 'StaffRoles',
    defaultMessage: 'Staff Roles',
  },
  Patients: {
    id: 'Patients',
    defaultMessage: 'Patients',
  },
  WorkingHours: {
    id: 'WorkingHours',
    defaultMessage: 'Working Hours',
  },
  OpeningSchedule: {
    id: 'OpeningSchedule',
    defaultMessage: 'Opening Schedule',
  },
  Role: {
    id: 'Role',
    defaultMessage: 'Role',
  },
  MedicalServices: {
    id: 'MedicalServices',
    defaultMessage: 'Medical Services',
  },
  Email: {
    id: 'Email',
    defaultMessage: 'E-mail',
  },
  Staff: {
    id: 'Staff',
    defaultMessage: 'Staff',
  },
  AddNewPatient: {
    id: 'AddNewPatient',
    defaultMessage: 'Add New Patient',
  },
  AddNewStaff: {
    id: 'AddNewStaff',
    defaultMessage: 'Add New Staff',
  },
  AppointmentsFinder: {
    id: 'AppointmentsFinder',
    defaultMessage: 'Appointments Finder',
  },
  AppointmentID: {
    id: 'AppointmentID',
    defaultMessage: 'Appointment ID ',
  },
  PrescriptionID: {
    id: 'PrescriptionID',
    defaultMessage: 'Prescription ID ',
  },
  StatusUpdated: {
    id: 'StatusUpdated',
    defaultMessage: 'Status Updated',
  },
  PatientID: {
    id: 'PatientID',
    defaultMessage: "Patient's ID",
  },
  OpenPrescriptions: {
    id: 'OpenPrescriptions',
    defaultMessage: 'Open Prescriptions',
  },
  PersonalDetails: {
    id: 'PersonalDetails',
    defaultMessage: 'Personal Details',
  },
  StaffProfile: {
    id: 'StaffProfile',
    defaultMessage: 'Staff Profile',
  },
  Registered: {
    id: 'Registered',
    defaultMessage: 'Registered',
  },
  Years: {
    id: 'years',
    defaultMessage: 'years',
  },
  PayerStatus: {
    id: 'PayerStatus',
    defaultMessage: 'Payer Status',
  },
  PatientName: {
    id: 'PatientName',
    defaultMessage: 'Patient Name',
  },
  Age: {
    id: 'Age',
    defaultMessage: 'Age',
  },
  PhoneNumber: {
    id: 'PhoneNumber',
    defaultMessage: 'Phone Number',
  },
  Cancel: {
    id: 'Cancel',
    defaultMessage: 'Cancel',
  },
  Submit: {
    id: 'Submit',
    defaultMessage: 'Submit',
  },
  Week: {
    id: 'Week',
    defaultMessage: 'Week',
  },
  Month: {
    id: 'Month',
    defaultMessage: 'Month',
  },
  Appointments: {
    id: 'Appointments',
    defaultMessage: 'Appointments',
  },
  PatientProfile: {
    id: 'PatientProfile',
    defaultMessage: 'Patient Profile',
  },
  Prescriptions: {
    id: 'Prescriptions',
    defaultMessage: 'Prescriptions',
  },
  NewSchedulePattern: {
    id: 'NewSchedulePattern',
    defaultMessage: 'New Schedule Pattern',
  },
  ContinueAnyway: {
    id: 'ContinueAnyway',
    defaultMessage: 'Continue Anyway',
  },
  OverbookingAlert: {
    id: 'OverbookingAlert',
    defaultMessage: 'Overbooking Alert',
  },
  OverbookingAlertMessageStaff: {
    id: 'OverbookingAlertMessageStaff',
    defaultMessage: 'You have at least one appointment or event that will be left outside your working hours.',
  },
  SchedulePatterns: {
    id: 'SchedulePatterns',
    defaultMessage: 'Schedule Patterns',
  },
  DeleteModalTitle: {
    id: 'SchedulePatternForm.DeleteModal.Title',
    defaultMessage: 'Delete Schedule Pattern',
  },
  DeleteModalText: {
    id: 'SchedulePatternForm.DeleteModal.Text',
    defaultMessage: 'Are you sure you want to delete this pattern?',
  },
});
