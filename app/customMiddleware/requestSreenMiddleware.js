import { increaseRequests, decreaseRequests, clearNumberRequests, changeLocation } from './../containers/App/actions';
export const requestScreenMiddleWare = store => next => action => {
  if (store.getState().App) {
    const pendingRequests = store.getState().App.pendingRequests;
    const splittedActionType = action.type.split('_');
    if (splittedActionType.includes('REQUEST')) {
      store.dispatch(increaseRequests(pendingRequests));
    }
    if (pendingRequests === 1) {
      setTimeout(function () {
        store.dispatch(clearNumberRequests());
      }, 2000);
    }
    if (pendingRequests > 0 && (splittedActionType.includes('SUCCESS') || splittedActionType.includes('FAILURE'))) {
      store.dispatch(decreaseRequests(pendingRequests));
    }
  }
  next(action);
};
