import { injectGlobal } from 'styled-components';
import fontSetting from 'components/styles/font';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    font-size: ${fontSetting.baseSize}px;
    height: 100%;
    width: 100%;
  }

  body, button, textarea, input {
    font-family: ${fontSetting.fontFamily}
  }

  body.fontLoaded {
    font-family: ${fontSetting.fontFamily}
  }

  #app {
    background-color: #EEEEEE;
    height: 100%;
    width: 100%;
  }

  p,
  label {
    font-family: ${fontSetting.fontFamily}
    line-height: 1.5em;
  }
  #app > div{
    height: 100%;
    width: 100%;
  }
  button:focus { outline: 0; }
  div:focus { outline: 0; }
  
  .Select-menu-outer {
    z-index: 2000 !important;
  }
`;
