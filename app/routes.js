// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import React from 'react';
/* eslint-disable */
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};


function loggedIn() {
  return !!localStorage.medcrm;
}


function requireAuth(nextState, replace) {
  if (!loggedIn()) {
    replace({
      pathname: '/auth'
    })
  }
}


function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'index',
      onEnter: (nextState, replace) => { replace({pathname: '/main/staff'}) },
    },
    {
      path: '/main',
      name: 'main',
      onEnter: (nextState, replace) => { replace({pathname: '/main/staff'}) },
    },
    {
      path: '/auth',
      name: 'auth',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
        System.import('containers/AuthPage/reducer'),
        System.import('containers/AuthPage/sagas'),
        System.import('containers/AuthPage'),
      ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('auth', reducer.default);
          injectSagas(sagas.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/register',
      name: 'register',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
        System.import('containers/RegisterPage/reducer'),
        System.import('containers/RegisterPage/sagas'),
        System.import('containers/RegisterPage'),
      ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('register', reducer.default);
          injectSagas(sagas.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/main',
      name: 'main',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
        System.import('containers/MainContainer/reducer'),
        System.import('containers/MainContainer/sagas'),
        System.import('containers/MainContainer'),
      ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('main', reducer.default);
          injectSagas(sagas.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
      onEnter: requireAuth,
      childRoutes: [
        {
          path: '/main/staff',
          name: 'mainstaff',
          getComponent(nextState, cb) {
            System.import('containers/StaffPage')
              .then(loadModule(cb))
              .catch(errorLoading);
          },
          indexRoute: {
            getComponent(nextState, cb) {
              const importModules = Promise.all([
              System.import('containers/StaffPage/StaffList/reducer'),
              System.import('containers/StaffPage/StaffList/sagas'),
              System.import('containers/StaffPage/StaffList'),
            ]);

              const renderRoute = loadModule(cb);

              importModules.then(([reducer, sagas, component]) => {
                injectReducer('StaffList', reducer.default);
                injectSagas(sagas.default);
                renderRoute(component);
              });

              importModules.catch(errorLoading);
            },
          },
          childRoutes: [
            {
              path: '/main/staff/new',
              name: 'newStaffPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                System.import('containers/StaffPage/NewStaffPage/reducer'),
                System.import('containers/StaffPage/NewStaffPage/sagas'),
                System.import('containers/StaffPage/NewStaffPage'),
              ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('newStaffPage', reducer.default);
                  injectSagas(sagas.default);
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/staff/roles',
              name: 'rolesPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/StaffPage/RolesPage/reducer'),
                  import('containers/StaffPage/RolesPage/sagas'),
                  import('containers/StaffPage/RolesPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('rolesPage', reducer.default);
                  injectSagas(sagas.default);
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/staff/:id',
              name: 'editStaffPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  System.import('containers/StaffPage/EditStaffPage/reducer'),
                  System.import('containers/StaffPage/EditStaffPage/sagas'),
                  System.import('containers/StaffPage/EditStaffPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('editStaffPage', reducer.default);
                  injectSagas(sagas.default);
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
              childRoutes: [
                {
                  path: '/main/staff/:id/working-hours',
                  name: 'staffWorkingHours',
                  getComponent(nextState, cb) {
                    const importModules = Promise.all([
                      System.import('containers/StaffPage/StaffSchedule/reducer'),
                      System.import('containers/StaffPage/StaffSchedule/sagas'),
                      System.import('containers/StaffPage/StaffSchedule/'),
                    ]);

                    const renderRoute = loadModule(cb);

                    importModules.then(([reducer, sagas, component]) => {
                      injectReducer('mySchedulePage', reducer.default);
                      injectSagas(sagas.default);
                      renderRoute(component);
                    });

                    importModules.catch(errorLoading);
                  },
                  indexRoute: {
                    getComponent(nextState, cb) {
                      System.import('containers/StaffPage/StaffSchedule/IndexPage')
                        .then(loadModule(cb))
                        .catch(errorLoading);
                    },
                  },
                  childRoutes: [
                    {
                      path: '/main/staff/:id/working-hours/patterns/new',
                      name: 'newStaffSchedulePattern',
                      getComponent(nextState, cb) {
                        System.import('containers/StaffPage/StaffSchedule/NewSchedulePattern')
                          .then(loadModule(cb))
                          .catch(errorLoading);
                      },
                    },
                    {
                      path: '/main/staff/:id/working-hours/patterns/:patternId',
                      name: 'editStaffSchedulePattern',
                      getComponent(nextState, cb) {
                        System.import('containers/StaffPage/StaffSchedule/EditSchedulePattern')
                          .then(loadModule(cb))
                          .catch(errorLoading);
                      },
                    },
                  ],
                },
              ]
            },
          ]
        },
        {
          path: '/main/profile',
          name: '/main/profile',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
            System.import('containers/ProfilePage'),
          ]);

            const renderRoute = loadModule(cb);

            importModules.then(([component]) => {
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
        },
        {
          path: '/main/med-services',
          name: '/main/med-services',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/MedicalServicesContainer/reducer'),
              System.import('containers/MedicalServicesContainer/sagas'),
              System.import('containers/MedicalServicesContainer'),
            ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectSagas(sagas.default);
              injectReducer('medicalservice', reducer.default);

              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
          indexRoute: {
            getComponent(nextState, cb) {
              const importModules = Promise.all([
                System.import('containers/MedicalServicesContainer/ServicesPage'),
              ]);

              const renderRoute = loadModule(cb);

              importModules.then(([component]) => {
                renderRoute(component);
              });

              importModules.catch(errorLoading);
            },
          },
          childRoutes: [
            {
              path: '/main/med-services/new',
              name: '/main/med-services/new',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  // System.import('containers/StaffPage/NewStaffPage/reducer'),
                  System.import('containers/MedicalServicesContainer/AddNewService'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  // importModules.then(([reducer, sagas, component]) => {
                  // injectReducer('newPatientPage', reducer.default);
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },{
              path: '/main/med-services/edit/:id',
              name: '/main/med-services/edit',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  System.import('containers/MedicalServicesContainer/EditService/reducer'),
                  System.import('containers/MedicalServicesContainer/EditService/sagas'),
                  System.import('containers/MedicalServicesContainer/EditService'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('EditMedService', reducer.default);
                  injectSagas(sagas.default);
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },{
              path: '/main/med-services/categories',
              name: 'serviceCategoriesPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  // import('containers/MedicalServicesContainer/ServiceCategoriesPage/reducer'),
                  // import('containers/MedicalServicesContainer/ServiceCategoriesPage/sagas'),
                  import('containers/MedicalServicesContainer/ServiceCategoriesPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
          ]
        },
        {
          path: '/main/agenda',
          name: '/main/agenda',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
            System.import('containers/SchedulePage/reducer'),
            System.import('containers/SchedulePage/sagas'),
            System.import('containers/SchedulePage'),
          ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectReducer('SchedulePage', reducer.default);
              injectSagas(sagas.default);
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
        },
        {
          path: '/main/calendar',
          name: '/main/calendar',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/CalendarPage/reducer'),
              System.import('containers/CalendarPage/sagas'),
              System.import('containers/CalendarPage'),
            ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectReducer('CalendarPage', reducer.default);
              injectSagas(sagas.default);
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
        },
        {
          path: '/main/patients',
          name: 'main/patients',
          getComponent(nextState, cb) {
            System.import('containers/PatientsPage')
              .then(loadModule(cb))
              .catch(errorLoading);
          },
          indexRoute: {
            getComponent(nextState, cb) {
              const importModules = Promise.all([
              System.import('containers/PatientsPage/PatientsList/reducer'),
              System.import('containers/PatientsPage/PatientsList/sagas'),
              System.import('containers/PatientsPage/PatientsList'),
            ]);

              const renderRoute = loadModule(cb);

              importModules.then(([reducer, sagas, component]) => {
                injectReducer('PatientPage', reducer.default);
                injectSagas(sagas.default);

                renderRoute(component);
              });

              importModules.catch(errorLoading);
            },
          },
          childRoutes: [
            {
              path: '/main/patients/new',
              name: 'newPatientPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  System.import('containers/PatientsPage/NewPatientPage/reducer'),
                  System.import('containers/PatientsPage/NewPatientPage/sagas'),
                  System.import('containers/PatientsPage/NewPatientPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('newPatientPage', reducer.default);
                  injectSagas(sagas.default);
                  // cb(null, class extends React.Component {
                  //   constructor(props){
                  //     super(props);
                  //   }
                  //   render(){
                  //     return <component.default testProps="sdadasd"/>
                  //   }
                  // });
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/patients/:id',
              name: 'editPatientPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  System.import('containers/PatientsPage/EditPatientPage/reducer'),
                  System.import('containers/PatientsPage/EditPatientPage/sagas'),
                  System.import('containers/PatientsPage/EditPatientPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([reducer, sagas, component]) => {
                  injectReducer('editPatientPage', reducer.default);
                  injectSagas(sagas.default);

                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
              indexRoute: {
                name: 'patientProfile',
                getComponent(nextState, cb) {
                  System.import('containers/PatientsPage/EditPatientPage/Profile')
                    .then(loadModule(cb))
                    .catch(errorLoading);
                },
              },
              childRoutes: [
                {
                  path: '/main/patients/:id',
                  name: 'patientCard',
                  getComponent(nextState, cb) {
                    System.import('containers/PatientsPage/EditPatientPage/Profile')
                      .then(loadModule(cb))
                      .catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/prescriptions',
                  name: 'prescriptions',
                  getComponent(nextState, cb) {
                    System.import('containers/PatientsPage/EditPatientPage/PrescriptionList')
                      .then(loadModule(cb))
                      .catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/prescriptions/:prescriptionId',
                  name: 'editPrescription',
                  getComponent(nextState, cb) {
                    System.import('containers/PatientsPage/EditPatientPage/EditPrescription')
                      .then(loadModule(cb))
                      .catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/appointments',
                  name: 'appointments',
                  getComponent(nextState, cb) {
                     System.import('containers/PatientsPage/EditPatientPage/AppointmentList')
                      .then(loadModule(cb))
                      .catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/appointments/:appointmentId',
                  name: 'editAppointment',
                  getComponent(nextState, cb) {
                    System.import('containers/PatientsPage/EditPatientPage/EditAppointment')
                      .then(loadModule(cb))
                      .catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/appointment-finder',
                  name: 'appointment-finder',
                  getComponent(nextState, cb) {
                    const importModules = Promise.all([
                      System.import('containers/PatientsPage/EditPatientPage/AppointmentFinder/reducer'),
                      System.import('containers/PatientsPage/EditPatientPage/AppointmentFinder/sagas'),
                      System.import('containers/PatientsPage/EditPatientPage/AppointmentFinder'),
                    ]);

                    const renderRoute = loadModule(cb);

                    importModules.then(([reducer, sagas, component]) => {
                      injectReducer('AppointmentFinder', reducer.default);
                      injectSagas(sagas.default);
                      renderRoute(component);
                    });

                    importModules.catch(errorLoading);
                  },
                },
                {
                  path: '/main/patients/:id/new-prescription',
                  name: 'new-prescription',
                  getComponent(nextState, cb) {
                    const importModules = Promise.all([
                      System.import('containers/PatientsPage/EditPatientPage/NewPrescriptionPage/reducer'),
                      System.import('containers/PatientsPage/EditPatientPage/NewPrescriptionPage/sagas'),
                      System.import('containers/PatientsPage/EditPatientPage/NewPrescriptionPage'),
                    ]);

                    const renderRoute = loadModule(cb);

                    importModules.then(([reducer, sagas, component]) => {
                      injectReducer('newPrescription', reducer.default);
                      injectSagas(sagas.default);
                      renderRoute(component);
                    });

                    importModules.catch(errorLoading);
                  },
                  childRoutes: [
                    {
                      path: '/main/patients/:id/new-prescription/physiotherapie',
                      name: 'new-prescription-physiotherapie',
                      getComponent(nextState, cb) {
                        System.import('containers/PatientsPage/EditPatientPage/NewPrescriptionPage/Physiotherapie')
                          .then(loadModule(cb))
                          .catch(errorLoading);
                      },
                    },
                    {
                      path: '/main/patients/:id/new-prescription/ergotherapie',
                      name: 'new-prescription-ergotherapie',
                      getComponent(nextState, cb) {
                        System.import('containers/PatientsPage/EditPatientPage/NewPrescriptionPage/Ergotherapie')
                          .then(loadModule(cb))
                          .catch(errorLoading);
                      },
                    }
                  ]
                },
              ],
            },

          ]
        },
        {
          path: '/main/resources',
          name: 'resourcesPage',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              import('containers/ResourcesPage/reducer'),
              import('containers/ResourcesPage/sagas'),
              import('containers/ResourcesPage'),
            ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectReducer('resourcesPage', reducer.default);
              injectSagas(sagas.default);
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
          indexRoute: {
            onEnter: ({ params }, replace) => replace(`/main/resources/equipments`)
          },
          childRoutes: [
            {
              path: '/main/resources/index',
              name: 'resourcesIndexPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/ResourcesPage/IndexPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
              childRoutes: [
                {
                  path: '/main/resources/equipments',
                  name: 'equipmentList',
                  getComponent(nextState, cb) {
                    const importModules = Promise.all([
                      import('containers/ResourcesPage/IndexPage/EquipmentList'),
                    ]);

                    const renderRoute = loadModule(cb);

                    importModules.then(([component]) => {
                      renderRoute(component);
                    });

                    importModules.catch(errorLoading);
                  }
                },
                {
                  path: '/main/resources/rooms',
                  name: 'equipmentList',
                  getComponent(nextState, cb) {
                    const importModules = Promise.all([
                      import('containers/ResourcesPage/IndexPage/RoomList'),
                    ]);

                    const renderRoute = loadModule(cb);

                    importModules.then(([component]) => {
                      renderRoute(component);
                    });

                    importModules.catch(errorLoading);
                  }
                },
              ]
            },
            {
              path: '/main/resources/rooms/new',
              name: 'newRoomPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/ResourcesPage/NewRoomPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/resources/equipment/new',
              name: 'newRoomPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/ResourcesPage/NewEquipmentPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/resources/rooms/:id',
              name: 'editRoomPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/ResourcesPage/EditRoomPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
            {
              path: '/main/resources/equipment/:id',
              name: 'editRoomPage',
              getComponent(nextState, cb) {
                const importModules = Promise.all([
                  import('containers/ResourcesPage/EditEquipmentPage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([component]) => {
                  renderRoute(component);
                });

                importModules.catch(errorLoading);
              },
            },
          ]
        },
        {
          path: '/main/opening-hours',
          name: 'schedulePatterns',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/OpeningHoursPage/reducer'),
              System.import('containers/OpeningHoursPage/sagas'),
              System.import('containers/OpeningHoursPage/'),
            ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectReducer('generalSchedulePage', reducer.default);
              injectSagas(sagas.default);
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
          indexRoute: {
            getComponent(nextState, cb) {
              System.import('containers/OpeningHoursPage/IndexPage')
                .then(loadModule(cb))
                .catch(errorLoading);
            },
          },
          childRoutes: [
            {
              path: '/main/opening-hours/patterns/new',
              name: 'newSchedulePattern',
              getComponent(nextState, cb) {
                System.import('containers/OpeningHoursPage/NewGeneralSchedulePattern')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
            },
            {
              path: '/main/opening-hours/patterns/:id',
              name: 'newSchedulePattern',
              getComponent(nextState, cb) {
                System.import('containers/OpeningHoursPage/EditGeneralSchedulePattern')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
            },
          ],
        },
        {
          path: '/main/settings',
          name: 'settings',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/SettingsPage/reducer'),
              System.import('containers/SettingsPage/sagas'),
              System.import('containers/SettingsPage/'),
            ]);

            const renderRoute = loadModule(cb);

            importModules.then(([reducer, sagas, component]) => {
              injectReducer('settingsPage', reducer.default);
              injectSagas(sagas.default);
              renderRoute(component);
            });

            importModules.catch(errorLoading);
          },
          indexRoute: {
            getComponent(nextState, cb) {
              System.import('containers/SettingsPage/')
                .then(loadModule(cb))
                .catch(errorLoading);
            },
          },
        },
      ]
    },
    {
      path: '/main/agenda-pdf',
      name: '/main/agenda-pdf',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/ScheduleForPdf'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '*',
      name: 'notfound',
      // onEnter: requireAuth,
      getComponent(nextState, cb) {
        System.import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}


export default function createRootComponent(store) {
  const { injectReducer, injectSagas } = getAsyncInjectors(store);
  return {
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/App/reducer'),
        import('containers/App/sagas'),
        import('containers/App'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([reducer, sagas, component]) => {
        injectSagas(sagas.default);
        injectReducer('App', reducer.default);

        renderRoute(component);
      });

      importModules.catch(errorLoading);
    },
    childRoutes: createRoutes(store),
  };
}
