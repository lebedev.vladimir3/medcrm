import moment from 'moment';
export default (birthday) => moment().diff(moment(birthday), 'years')
