import { call, put } from 'redux-saga/effects';
import { resourceUnauthorized } from '../containers/App/actions';


// https://github.com/redux-saga/redux-saga/issues/110
export function* callApi(fn, ...rest) {
  let response;
  try {
    response = yield call(fn, ...rest);
    return response;
  } catch (error) {
    if (error.response.status === 401) {
      yield put(resourceUnauthorized());
    }
    return error;
  }
}


export default callApi;
