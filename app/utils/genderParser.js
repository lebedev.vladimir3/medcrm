import React from 'react';
import { personal } from 'constants/users'
import FaVenus from 'assets/icons/Sex/Female.svg';
import FaMars from 'assets/icons/Sex/Male.svg';
import theme from 'components/styles/colorScheme';

export const genderParser = (inPersonal) => {
  if(inPersonal){
    return inPersonal === personal.HERR ?
      <FaMars
        //color={theme.Gender.Male}
      /> :
      <FaVenus
        //color={theme.Gender.Female}
      />;
  } else {
    return false;
  }
};
