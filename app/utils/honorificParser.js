import { defineMessages } from 'react-intl';
import { honorifics } from 'constants/users';

const messages = defineMessages({
  DR: {
    id: 'DR',
    defaultMessage: 'Dr.',
  },
  DR_MED: {
    id: 'DR_MED',
    defaultMessage: 'Dr. Med.',
  },
  PROFF: {
    id: 'PROFF',
    defaultMessage: 'Proff.',
  },
});

export default class HonorificParser {
  static toClientSide(honorificServerSide, formatMessage){
    switch (honorificServerSide) {
      case honorifics.DR:
        return formatMessage(messages[honorifics.DR]);
      case honorifics.PROFF:
        return formatMessage(messages[honorifics.PROFF]);
      case honorifics.DR_MED:
        return formatMessage(messages[honorifics.DR_MED]);
    }
  }
}
