export { genderParser } from './genderParser';
export HonorificParser from './honorificParser';
export { numbericComporator, stringComporator, dateComporator } from './sortComporators';
export { calculateSummaryFromMinutes, calculateWorkingHours } from './printerHelper';

export const getCurrentRoute = (pathname) => {
  // let result = {};
  // pathname.split('/').filter(x => x).forEach(item => {
  //   result[item] = true;
  // });
  return pathname.split('/').filter(x => x);
};

export const getLastRoute = (pathname) => {
  const route = pathname.split('/').filter(x => x);
  return route[route.length - 1];
};
