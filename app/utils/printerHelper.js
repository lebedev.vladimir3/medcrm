export const calculateSummaryFromMinutes = (summaryMinutes) => {
  const minutesReminder = summaryMinutes % 60;
  const fullHours = (summaryMinutes - minutesReminder) / 60;
  let summary = `${fullHours} h`;
  if (minutesReminder) {
    summary = `${summary} ${minutesReminder} min`;
  }
  return summary;
};

export const calculateWorkingHours = (workingHours) => workingHours.reduce(
  (acc, cur) => acc + (cur.to ? (cur.to - cur.from) : 0), 0
);
