export const numbericComporator = (field, order = "asc") => {
  let multiply = order === "asc"? 1: -1;
  return (a,b) => {
    let A = Number(a[field]),
      B = Number(b[field]);
    if (A > B) return multiply;
    if (A < B) return -1 * multiply;
    return 0;
  }
};

export const stringComporator = (field, order = "asc") => {
  let multiply = order === "asc"? 1: -1;
  return (a,b) => {
    let nameA = (a[field] + "").toUpperCase();
    let nameB = (b[field] + "").toUpperCase();
    if (nameA < nameB) {
      return -1 * multiply;
    }
    if (nameA > nameB) {
      return multiply;
    }
    return 0;
  }
};

export const dateComporator = (field,sortMultiply,dateFormat = "DD.MM.YYYY") => {
  return (a,b) => {
    let t1 = dateFormat ? moment(a[field],dateFormat): moment(a[field]),
      t2 = dateFormat ? moment(b[field],dateFormat): moment(b[field]);
    if(t1.diff(t2) > 0 || (t1.isValid() && !t2.isValid())){
      return sortMultiply;
    } else if(t1.diff(t2) < 0 || (!t1.isValid() && t2.isValid())) {
      return -1 * sortMultiply;
    } else if (!t1.isValid() && !t2.isValid()) return 0;
    return 0;
  }
}
