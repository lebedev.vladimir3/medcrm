const React = require('react');
const utils = require('./utils');
const names = require('./statusNames');

let APPOINTMENT = {
  FINANCIAL: [
    { value: names.DRAFT, label: 'Draft', default: true },
    { value: names.ISSUED, label: 'Issued' },
    { value: names.SENT, label: 'Sent' },
    { value: names.PAID, label: 'Paid' },
  ],
  GENERAL: [
    { value: names.SCHEDULED, label: 'Scheduled', default: true },
    { value: names.RE_SCHEDULED, label: 'Re-Scheduled' },
    { value: names.NO_SHOW, label: 'No-Show' },
    { value: names.ARRIVED, label: 'Arrived' },
    { value: names.PERFORMING, label: 'Performing' },
    { value: names.PROVIDED, label: 'Provided' },
    { value: names.CANCELLED, label: 'Cancelled', canSelect: false },
  ]
};

if(utils.isBrowser()){
  const icons = require('./../../../app/components/styles/icons');
  const ICON_BIND = {
    [names.DRAFT]: React.createElement(icons.Draft),
    [names.ISSUED]: React.createElement(icons.Issued),
    [names.SENT]: React.createElement(icons.Sent),
    [names.PAID]: React.createElement(icons.Paid),

    [names.SCHEDULED]: React.createElement(icons.Scheduled),
    [names.RE_SCHEDULED]: React.createElement(icons.Scheduled),
    [names.NO_SHOW]: React.createElement(icons.NoShow),
    [names.ARRIVED]: React.createElement(icons.Arrived),
    [names.PERFORMING]: React.createElement(icons.Performing),
    [names.PROVIDED]: React.createElement(icons.Provided),
    [names.CANCELLED]: React.createElement(icons.Cancelled),
  };
  APPOINTMENT = utils.editCategory(APPOINTMENT, (category) => {
    category = category.map(item => {
      item.icon = ICON_BIND[item.value];
      return item;
    });
    return category;
  });
}

const onlyCanSelectAppointment = utils.onlyCanSelect(APPOINTMENT);
module.exports.default = onlyCanSelectAppointment;

module.exports.ALL = APPOINTMENT;

module.exports.STATUSES = {
  FINANCIAL: APPOINTMENT.FINANCIAL.map(status => status.value),
  GENERAL: APPOINTMENT.GENERAL.map(status => status.value)
};

module.exports.GENERAL_DEFAULT = utils.getDefault(APPOINTMENT.GENERAL);
module.exports.FINANCIAL_DEFAULT = utils.getDefault(APPOINTMENT.FINANCIAL);
