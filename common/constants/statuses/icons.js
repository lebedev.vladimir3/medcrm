const styled = require('styled-components').default;
const colorScheme = require('../../../app/components/styles/colorScheme').Statuses;

const MdNotifications = require('react-icons/lib/md/notifications');
const MdAccessTime = require('react-icons/lib/md/access-time');
const MdCheck = require('react-icons/lib/go/check');
const FaPaperPlane = require('./../../../common/icons/react-svg/paperplane');
const EuroIcon = require('./../../../common/icons/react-svg/euro');
const MdReceipt = require('react-icons/lib/md/receipt');
const MdClose = require('react-icons/lib/md/close');
const FlagIcon = require('./../../../common/icons/react-svg/flag');
const FaRefresh = require('react-icons/lib/fa/refresh');
const FaMinusCircle = require('react-icons/lib/fa/minus-circle');

module.exports.Unscheduled = styled(MdNotifications)`color: ${() => colorScheme.UNSCHEDULED};`;
module.exports.PartiallyScheduled = styled(MdNotifications)` color: ${() => colorScheme.PARTIALLY_SCHEDULED}; `;
module.exports.Scheduled = styled(MdAccessTime).attrs({ viewBox: '2 2 35 35' })` color: ${() => colorScheme.SCHEDULED} `;
module.exports.Draft = styled(MdReceipt)` color: ${() => colorScheme.DRAFT} `;
module.exports.Issued = styled(MdReceipt)` color: ${() => colorScheme.ISSUED} `;
//module.exports.PaperPlane = styled(FaPaperPlane)` color: ${() => colorScheme.SENT } `;
module.exports.Arrived = styled(FlagIcon)` color: ${() => colorScheme.ARRIVED } `;
module.exports.Performing = styled(FaRefresh)` color: ${() => colorScheme.PERFORMING }; transform: rotateX(180deg) rotate(-13deg); `;
module.exports.Provided = styled(MdCheck)` color: ${() => colorScheme.PROVIDED} `;
module.exports.Cancelled = styled(MdClose)` color: ${() => colorScheme.CANCELLED } `;
module.exports.NoShow = styled(FaMinusCircle)` color: ${() => colorScheme.NO_SHOW} `;
module.exports.Sent = styled(FaPaperPlane).attrs({ style: { width: '13px', height: '13px'}})` color: ${() => colorScheme.SENT} `;
module.exports.Paid = styled(EuroIcon).attrs({ style: { width: '13px', height: '13px'}})` color: ${() => colorScheme.PAID}; `;
