const prescription = require('./prescriptions');
const appointment = require('./appointments');

module.exports = {
  STATUS_NAMES: require('./statusNames'),
  PRESCRIPTION: prescription.default,
  PrescriptionStatuses: prescription.default,
  PRESCRIPTION_ALL: prescription.ALL,
  PRESCRIPTION_STATUSES: prescription.STATUSES,
  PRESCRIPTION_GENERAL_DEFAULT: prescription.GENERAL_DEFAULT,
  PRESCRIPTION_FINANCIAL_DEFAULT: prescription.FINANCIAL_DEFAULT,

  APPOINTMENT: appointment.default,
  AppointmentStatuses: appointment.default,
  APPOINTMENT_STATUSES: appointment.STATUSES,
  APPOINTMENT_GENERAL_DEFAULT: appointment.GENERAL_DEFAULT,
  APPOINTMENT_FINANCIAL_DEFAULT: appointment.FINANCIAL_DEFAULT,
  APPOINTMENT_ALL: appointment.ALL,
};








