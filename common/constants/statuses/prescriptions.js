const React = require('react');
const utils = require('./utils');
const names = require('./statusNames');

let PRESCRIPTION = {
  FINANCIAL: [
    { value: names.DRAFT, label: 'Draft' },
    { value: names.PARTIALLY_ISSUED, label: 'Partially Issued' },
    { value: names.ISSUED, label: 'Issued' },
    { value: names.PARTIALLY_SENT, label: 'Partially Sent' },
    { value: names.SENT, label: 'Sent' },
    { value: names.PARTIALLY_PAID, label: 'Partially Paid' },
    { value: names.PAID, label: 'Paid' },
  ],
  GENERAL: [
    { value: names.UNSCHEDULED, label: 'Unscheduled' },
    { value: names.PARTIALLY_SCHEDULED, label: 'Partially Scheduled' },
    { value: names.SCHEDULED, label: 'Scheduled' },
    { value: names.PROVIDED, label: 'Provided' },
    { value: names.CANCELLED, label: 'Cancelled', canSelect: false}
  ]
};

if(utils.isBrowser()){
  const icons = require('./../../../app/components/styles/icons');
  const ICON_BIND = {
    [names.DRAFT]: React.createElement(icons.Draft),
    [names.PARTIALLY_ISSUED]: React.createElement(icons.Issued),
    [names.ISSUED]: React.createElement(icons.Issued),
    [names.PARTIALLY_SENT]: React.createElement(icons.Sent),
    [names.SENT]: React.createElement(icons.Sent),
    [names.PARTIALLY_PAID]: React.createElement(icons.Paid),
    [names.PAID]: React.createElement(icons.Paid),

    [names.UNSCHEDULED]: React.createElement(icons.Unscheduled),
    [names.PARTIALLY_SCHEDULED]: React.createElement(icons.PartiallyScheduled),
    [names.SCHEDULED]: React.createElement(icons.Scheduled),
    [names.PROVIDED]: React.createElement(icons.Provided),
    [names.CANCELLED]: React.createElement(icons.Cancelled),
  };
  PRESCRIPTION = utils.editCategory(PRESCRIPTION, (category) => {
    category = category.map(item => {
      item.icon = ICON_BIND[item.value];
      return item;
    });
    return category;
  });
}

const onlyCanSelectPrescription = utils.onlyCanSelect(PRESCRIPTION);
module.exports.default = onlyCanSelectPrescription;

module.exports.ALL = PRESCRIPTION;

module.exports.STATUSES = {
  FINANCIAL: PRESCRIPTION.FINANCIAL.map(status => status.value),
  GENERAL: PRESCRIPTION.GENERAL.map(status => status.value)
};

module.exports.GENERAL_DEFAULT = utils.getDefault(PRESCRIPTION.GENERAL);
module.exports.FINANCIAL_DEFAULT = utils.getDefault(PRESCRIPTION.FINANCIAL);
