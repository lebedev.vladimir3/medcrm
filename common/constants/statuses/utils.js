const editCategory = (entity, cb) => {
  const categories = Object.keys(entity);
  let result = { ...entity };
  for(let category of categories){
    result[category] = cb(result[category]);
  }
  return result;
};

module.exports.editCategory = editCategory;

module.exports.getDefault = (category) => {
  let defaultStatus = category.find(item => item.default === true);
  if(!defaultStatus && category.length > 0) return category[0].value;
  return defaultStatus.value;
};

module.exports.onlyCanSelect = (entity) => editCategory(entity, (item) => item.filter(item => item.canSelect !== false));

module.exports.isBrowser =  () => {
  try {
    if(window) return true;
  } catch(e) {
    return false;
  }
};
module.exports.isNode = new Function("try {return this===global;}catch(e){return false;}");
