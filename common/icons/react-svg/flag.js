const React = require('react');

const FlagIcon = function FlagIcon(props) {
  return React.createElement(
    'svg',
    {
      width: '1em',
      height: '1em',
      viewBox: '79.3 123.3 436.6 595.3',
      ...props
    },
    React.createElement(
      'g',
      null,
      React.createElement('path', {
        d: `M509.6,317.2l-368.4-163l0,0c0-17.1-13.8-30.9-30.9-30.9s-30.9,13.9-30.9,30.9v533.4c0,17.1,13.8,30.9,30.9,30.9
	c17.1,0,30.9-13.9,30.9-30.9V510.9l368.4-175.5c3.8-1.4,6.3-5,6.3-9.1C515.9,322.2,513.4,318.6,509.6,317.2z`
      })
    )
  );
};

exports.default = FlagIcon;
module.exports = exports['default'];
