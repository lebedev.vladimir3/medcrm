const React = require('react');

const PaperplaneIcon = function PaperplaneIcon(props) {
  return React.createElement(
    'svg',
    {
      width: '1em',
      height: '1em',
      viewBox: '0 0 229.766 229.766',
      ...props
    },
    React.createElement(
      'g',
      null,
      [
        React.createElement('path', {
          d: `M7.196,119.892c-2.19-0.436-3.867-2.208-4.177-4.421c-0.316-2.214,0.806-4.386,2.781-5.412
      L218.292,0L93.769,137.041L7.196,119.892z`
        }),
        React.createElement('path', {
          d: `M136.17,226.526c-0.817,1.969-2.727,3.24-4.857,3.24h-0.292c-2.22-0.125-4.129-1.641-4.756-3.789
		l-23.677-80.893L226.8,8.378L136.17,226.526z`
        })
      ]
    )
  );
};

exports.default = PaperplaneIcon;
module.exports = exports['default'];
