import os
import sys
import re

regex = re.compile(r"font-size:\s*([0-9]*)(px);", re.IGNORECASE)

walk_dir = sys.argv[1]
modify = bool(sys.argv[2]) if len(sys.argv) >= 3 else False

counter = 0
for root, subdirs, files in os.walk(walk_dir):
    # print(root)
    for filename in files:
        file_path = os.path.join(root, filename)
        file_name, file_ext = os.path.splitext(file_path)

        if file_ext == ".js":
            with open(file_path, 'r') as f:
                lines = f.readlines()
                f.close()

                newDogFile = open(file_path, 'w')
                file_counter = 0
                need_to_write = []
                for line in lines:
                    result_search = regex.search(line)
                    new_line = line
                    if result_search is not None:
                        base_string = "font-size: ${fontSetting.baseSize"
                        value = int(result_search.group(1))
                        new_value = 14 - (value - 1)
                        if new_value < 0:
                            base_string = base_string + (" + %s" % abs(new_value))
                        elif new_value > 0:
                            base_string = base_string + (" - %s" % abs(new_value))

                        if modify == True:
                            new_line = regex.sub(base_string + "}px;", line)

                        file_counter = file_counter + 1
                        counter = counter + 1
                    need_to_write.append(new_line)
                    # newDogFile.write(new_line)

                if file_counter > 0:
                    level = len(file_path.split('\\'))
                    import_str = './'
                    for x in range(0, level - 2):
                        import_str = import_str + '../'
                    if modify == True:
                        need_to_write = [
                        "import fontSetting from 'components/styles/font';\n",
                        #"@import('" + import_str + "'assets/styles/variables.scss');\n",
                         *need_to_write
                         ]

                for str_to_write in need_to_write:
                    newDogFile.write(str_to_write)
                #print(need_to_write)
                newDogFile.close()
print('Matches: %s' % counter)
