// needed for regenerator-runtime
// (ES7 generator support is required by redux-saga)
import 'babel-polyfill';
const context = require.context('../../', true, /^^((?!(app|reducers|routes|assets)).)*\.js$/);