const co = require('co');
const mongoose = require('mongoose');
const Appointment = require('./model');
const Room = require('../rooms/model');
const Prescription = require('../prescriptions/model');
const Service = require('../medServices/model');
const User = require('../users/model');
const toMongoId = (id) => mongoose.Types.ObjectId(id);
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const utils = require('./utils/appointmentSuggestion');
const calculateWorkingHours = require('../appointments/utils/workingHours').calculateWorkingHours;
const logger = require('../logger');

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;
const Logging = require('../logging/model');
const asyncMiddleware = require('../utils/asyncMiddleware');

const STATUS_NAMES = require('../../common/constants/statuses').STATUS_NAMES;

exports.getChangesHistory = asyncMiddleware(async (req, res, next) => {
  res.json(await Logging.find({ modelType: logConsts.APPOINTMENT }).lean());
});

exports.getAppointment = (req, res, next) => {
  co(function* () {
    try {
      const appointment = yield Appointment
        .findById(toMongoId(req.params.id))
        .populate('staff prescription service')
        .exec();

      res.status(200).json(appointment);
    } catch (e) {
      next(e);
    }
  });
};


exports.getAppointments = (req, res, next) => {
  co(function* () {
    try {
      const patientId = req.query.patient;
      const query = {};

      if (patientId) {
        const prescriptions = yield Prescription.find({ patient: toMongoId(patientId) });
        query.prescription = { $in: prescriptions.map((item) => item._id) };
      }

      const appointments = yield Appointment
        .find(query).and(req.body.query)
        .populate('staff prescription room service')
        .exec();

      res.status(200).json(appointments);
    } catch (e) {
      next(e);
    }
  });
};

exports.getAppointmentsWithPatient = (req, res, next) => {
  co(function* () {
    try {
      const query = {};

      const appointments = yield Appointment
        .find(query).and(req.body.query)
        .populate({
          path: 'staff prescription room service',
          populate: {
            path: 'patient',
            model: 'Patient',
          },
        }).exec();
      res.status(200).json(appointments);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateAppointmentList = (req, res, next) => {
  const dbLogger = req.dbLogger;
  co(function* () {
    try {
      const user = yield Appointment.update(
        { _id: { $in: req.body.ids.map((item) => toMongoId(item)) } },
        { $set: req.body.data },
        { multi: true }
      );

      let editedScheduled = req.body.scheduledAppointments;
      if(!editedScheduled){
        editedScheduled = yield Promise.all(req.body.ids.map(id => Appointment.findById(toMongoId(id)).lean()));
      }
      for (let i = 0; i < editedScheduled.length; i++) {
        dbLogger.add(logConsts.APPOINTMENT, logActions.UPDATE, {
          ...req,
          body: editedScheduled[i]
        });
      }

      req.resultStatus = 200;
      req.result = user;
      next();
    } catch (e) {
      next(e);
    }
  });
};


exports.createAppointments = (req, res, next) => {
  co(function* () {
    try {
      req.result = yield Appointment.create(req.body);
      next();
    } catch (e) {
      next(e);
    }
  });
};

exports.createConfirmedAppointments = (req, res, next) => {
  const dbLogger = req.dbLogger;
  co(function* () {
    try {
      let appointments = [];

      if (req.body.length > 0) {
        const prescription = yield Prescription.findById(req.body[0].prescription).exec();

        let appointmentsCreate = [];
        for (let i = 0; i < req.body.length; i++) {
          appointmentsCreate.push(Appointment.create({
            ...req.body[i],
            id: prescription.appointmentCount + i + 1
          }));
        }
        let createdAppointments = yield Promise.all(appointmentsCreate);
        appointments = yield createdAppointments.map((doc, index) =>
          Appointment.findById(toMongoId(doc._id)).populate('staff prescription service'));

        for (let i = 0; i < req.body.length; i++) {
          dbLogger.add(logConsts.APPOINTMENT, logActions.CREATE, {
            ...req,
            body: {
              ...req.body[i],
              id: prescription.appointmentCount + i + 1,
            }
          });
        }

        prescription.appointmentCount += req.body.length;
        prescription.save();
      }

      req.result = appointments;
      next();
    } catch (e) {
      next(e);
    }
  });
};

exports.cancelAppointment = (req, res, next) => {
  co(function*() {
    try {
      const id = toMongoId(req.params.id);
      yield Appointment.findByIdAndUpdate(id,
        { $set: { status: STATUS_NAMES.CANCELLED } });
      req.resultStatus = 204;
      next();
    } catch (e) {
      next(e);
    }
  });
};
// exports.deleteAppointment = (req, res, next) => {
//   co(function* () {
//     try {
//       yield Appointment.findByIdAndUpdate(toMongoId(req.params.id));
//       req.resultStatus = 204;
//       next();
//     } catch (e) {
//       next(e);
//     }
//   });
// };

exports.deleteAppointments = (req, res, next) => {
  co(function* () {
    try {
      for (let i = 0; i < req.body.length; i++) {
        yield Appointment.findByIdAndUpdate(toMongoId(req.body[i]._id), {$set: {isDeleted: true}});
      }
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};


// const ApiExample1 = {
//   services:[{
//     serviceId: '123213',
//     count: 3,
//   }],
//   preferredDateFrom: (new Date()).toJSON(),
//   preferredDateTo: (new Date()).toJSON(),
//   preferredTimeFrom: 123,
//   preferredTimeTo: 523,
//   staffGender: 'Male',
//   prescriptionId: '2124',
// };


// const ApiExample2 = {
//   serviceId: 'serviceId',
//   date: (new Date()).toJSON(),
//   preferredTimeFrom: 123,
//   preferredTimeTo: 523,
//   staffId: '123123',
// };


exports.updateAppointment = (req, res, next) => {
  co(function* () {
    try {
      let appointment;
      const { force, date, duration, prescription } = req.body;

      if (!force) {
        const roomId = req.body.room;
        const staffId = req.body.staff._id;

        const patientPrescriptions = yield Prescription.find({
          patient: toMongoId(prescription.patient),
        }).lean().exec();

        const {
          patientAppointments,
          room,
          roomAppointments,
          staff,
          staffAppointments,
        } = yield {
          patientAppointments: Appointment.find({
            prescription: { $in: patientPrescriptions.map((item) => item._id) },
            _id: { $nin: req.params.id },
          }).lean().exec(),
          room: Room.findById(toMongoId(roomId)).lean().exec(),
          roomAppointments: Appointment.find({
            room: toMongoId(roomId),
            _id: { $nin: req.params.id },
          }).lean().exec(),
          staff: User.findById(toMongoId(staffId)).lean().exec(),
          staffAppointments: Appointment.find({
            staff: toMongoId(staffId),
            _id: { $nin: req.params.id },
          }).lean().exec(),
        };
        const patientIsVacant = utils.isPatientVacant(patientAppointments, date, +duration);
        const staffIsVacant = utils.isStaffVacant(staffAppointments, date, +duration,
                                                    calculateWorkingHours(moment(date), staff.schedulePaterrns || []), room);
        const roomIsVacant = utils.isRoomVacant(roomAppointments, date, +duration, room.capacity);

        if (!patientIsVacant || !staffIsVacant || !roomIsVacant) {
          res.status(200).json({ patientIsVacant, staffIsVacant, roomIsVacant, overbooking: true });
          return;
        }
      }

      appointment = yield Appointment.findByIdAndUpdate(
        req.params.id,
        { $set: req.body },
        { new: true }
      );
      req.resultStatus = 200;
      req.result = appointment;
      next();

    } catch (e) {
      next(e);
    }
  });
};


const getPrescriptionAggregationPipeline = (prescriptionId) => [
  { $match: { _id: prescriptionId } },
  { $lookup: {
    from: 'patients',
    localField: 'patient',
    foreignField: '_id',
    as: 'patient',
  } },
  { $unwind: { path: '$patient', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'appointments',
    localField: '_id',
    foreignField: 'prescription',
    as: 'patient.appointments',
  } },
];


const getServiceAggregationPipeline = (serviceIds) => [
  { $match: { _id: { $in: serviceIds } } },
  { $unwind: { path: '$staffAllocation', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'users',
    localField: 'staffAllocation.staff',
    foreignField: '_id',
    as: 'staff1',
  } },
  { $lookup: {
    from: 'roles',
    localField: 'staffAllocation.role',
    foreignField: '_id',
    as: 'staffRole',
  } },
  { $unwind: { path: '$staffRole', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'users',
    localField: 'staffRole._id',
    foreignField: 'role',
    as: 'staff2',
  } },
  { $match: {
    'staff1._id': { $exists: true, $ne: null },
    'staff2._id': { $exists: true, $ne: null },
  } },
  { $project: {
    staff: {
      $cond: {
        if: { $eq: ['$staffAllocation.isAnyStaff', false] },
        then: '$staff1',
        else: '$staff2',
      },
    },
    name: 1,
    duration: 1,
    equipmentAllocation: 1,
    roomAllocation: 1,
    payers: 1,
  } },
  { $unwind: { path: '$staff', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'appointments',
    localField: 'staff._id',
    foreignField: 'staff',
    as: 'staff.appointments',
  } },
  { $group: {
    _id: '$_id',
    name: { $first: '$name' },
    payers: { $first: '$payers' },
    equipmentAllocation: { $first: '$equipmentAllocation' },
    roomAllocation: { $first: '$roomAllocation' },
    staff: { $push: '$staff' },
  } },
  { $unwind: { path: '$roomAllocation', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'rooms',
    localField: 'roomAllocation.room',
    foreignField: '_id',
    as: 'rooms',
  } },
  { $unwind: { path: '$rooms', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'appointments',
    localField: 'rooms._id',
    foreignField: 'room',
    as: 'rooms.appointments',
  } },
  { $group: {
    _id: '$_id',
    name: { $first: '$name' },
    payers: { $first: '$payers' },
    staff: { $first: '$staff' },
    rooms: { $push: '$rooms' },
    equipmentAllocation: { $first: '$equipmentAllocation' },
  } },
  { $unwind: { path: '$equipmentAllocation', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'equipment',
    localField: 'equipmentAllocation.equipment',
    foreignField: '_id',
    as: 'equipmentList',
  } },
  { $unwind: { path: '$equipmentList', preserveNullAndEmptyArrays: true } },
  { $lookup: {
    from: 'appointments',
    localField: 'equipmentList._id',
    foreignField: 'equipment',
    as: 'equipmentList.appointments',
  } },
  { $group: {
    _id: '$_id',
    name: { $first: '$name' },
    payers: { $first: '$payers' },
    staff: { $first: '$staff' },
    rooms: { $first: '$rooms' },
    equipmentList: { $push: '$equipmentList' },
  } },
];


exports.getAppointmentSuggestions = (req, res, next) => {
  co(function* () {
    try {
      const {
        prescriptionId,
        preferredDateFrom,
        preferredDateTo,
        preferredTimeFrom,
        preferredTimeTo,
        staffGender,
        isAlternativeSearch,
      } = req.body;

      const servicesToSuggest = req.body.services;

      const { prescriptions, agServices } = yield {
        prescriptions: Prescription
          .aggregate(getPrescriptionAggregationPipeline(toMongoId(prescriptionId)))
          .exec(),
        agServices: Service
          .aggregate(getServiceAggregationPipeline(
            servicesToSuggest.map((item) => toMongoId(item.id)),
            {
              preferredDateFrom,
              preferredDateTo,
              preferredTimeFrom,
              preferredTimeTo,
            }
          ))
          .exec(),
      };

      const suggestedAppointments = utils.suggestAppointments(
        {
          preferredDateFrom,
          preferredDateTo,
          preferredTimeFrom,
          preferredTimeTo,
          staffGender,
          isAlternativeSearch,
          servicesToSuggest,
        },
        prescriptions[0],
        agServices
      );

      res.status(200).json(suggestedAppointments);
    } catch (e) {
      next(e);
    }
  });
};


exports.getAppointmentAlternatives = (req, res, next) => {
  co(function* () {
    try {
      const {
        prescriptionId,
        preferredTimeFrom,
        preferredTimeTo,
        serviceId,
        staffId,
        count,
        appointmentsSelected,
        date,
        staffGender,
      } = req.body;

      const { prescriptions, agServices } = yield {
        prescriptions: Prescription
          .aggregate(getPrescriptionAggregationPipeline(toMongoId(prescriptionId)))
          .exec(),
        agServices: Service
          .aggregate(getServiceAggregationPipeline([toMongoId(serviceId)]))
          .exec(),
      };

      const suggestedAppointments = utils.suggestAppointments(
        {
          preferredDateFrom: date,
          preferredDateTo: moment(date).add(3, 'days'),
          preferredTimeFrom,
          preferredTimeTo,
          staffGender,
          isAlternativeSearch: true,
          servicesToSuggest: [{ id: serviceId, count }],
          staffId,
          appointmentsSelected,
        },
        prescriptions[0],
        agServices
      );

      res.json(suggestedAppointments);
    } catch (e) {
      next(e);
    }
  });
};
