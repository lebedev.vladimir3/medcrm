const mongoose = require('mongoose');
const STATUSES = require('../../common/constants/statuses');
const APPOINTMENT_STATUSES = STATUSES.APPOINTMENT_STATUSES;
const APPOINTMENT_FINANCIAL_DEFAULT = STATUSES.APPOINTMENT_FINANCIAL_DEFAULT;
const APPOINTMENT_GENERAL_DEFAULT = STATUSES.APPOINTMENT_GENERAL_DEFAULT;

const appointmentSchema = new mongoose.Schema({
  id: { type: Number },
  date: Date,
  duration: Number,
  isDeleted: {type: Boolean, default: false},
  status: {
    type: String,
    default: APPOINTMENT_GENERAL_DEFAULT,
    enum: APPOINTMENT_STATUSES.GENERAL,
  },
  statusChangedAt: { type: Date },
  financialStatus: {
    type: String,
    default: APPOINTMENT_FINANCIAL_DEFAULT,
    enum: APPOINTMENT_STATUSES.FINANCIAL,
  },
  financialStatusChangedAt: { type: Date },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
  },
  equipment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Equipment',
  },
  notes: String,
  treatmentReport: String,
  staff: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  service: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MedService',
  },
  prescription: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Prescription',
  },
}, { timestamps: true, virtuals: true });


const Appointment = mongoose.model('Appointment', appointmentSchema);

module.exports = Appointment;
