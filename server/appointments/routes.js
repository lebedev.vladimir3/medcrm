const Router = require('express').Router;
const appointmentsController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const actions = authorization.actions;
const roles = authorization.roles;

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;

module.exports = () => {
  const appointmentsRoutes = new Router();

  appointmentsRoutes
    .get('/', appointmentsController.getAppointments)
    .get('/changes-history',  appointmentsController.getChangesHistory)
    .post('/', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.createAppointments, logMiddleware(logConsts.APPOINTMENT, logActions.CREATE)) //not used
    .put('/', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.updateAppointmentList, logMiddleware())
    .get('/:id', appointmentsController.getAppointment)
    .put('/:id', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.updateAppointment, logMiddleware(logConsts.APPOINTMENT, logActions.UPDATE))
    //.delete('/:id', user.is(roles.ADMIN), appointmentsController.deleteAppointment, logMiddleware(logConsts.APPOINTMENT, logActions.DELETE))
    .delete('/:id', user.is(roles.ADMIN), appointmentsController.cancelAppointment, logMiddleware(logConsts.APPOINTMENT, logActions.CANCEL))
    .post('/suggestions', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.getAppointmentSuggestions)
    .post('/confirm', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.createConfirmedAppointments, logMiddleware())
    .post('/array_delete', user.is(roles.ADMIN), appointmentsController.deleteAppointments) //not used
    .post('/alternatives', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.getAppointmentAlternatives)
    .post('/withPatient', user.can(actions.CAN_CHANGE_APPOINTMENTS), appointmentsController.getAppointmentsWithPatient);

  return appointmentsRoutes;
};
