const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const logger = require('../../logger');
const calculateWorkingHours = require('./workingHours').calculateWorkingHours;
const _ = require('lodash');

const isPatientVacant = (appointments, suggestedTime, duration) => {
  const range1 = moment.range(
    moment(suggestedTime),
    moment(suggestedTime).add(duration, 'minutes')
  );

  for (let i = 0; i < appointments.length; i++) {
    const existedTime = appointments[i].date;
    if (moment(existedTime).isSame(moment.appointments)) { return false; }

    const range2 = moment.range(
      moment(existedTime),
      moment(existedTime).add(appointments[i].duration, 'minutes')
    );

    if (range1.overlaps(range2)) { return false; }
  }
  return true;
};

exports.isPatientVacant = isPatientVacant;


const isEquipmentVacant = (appointments, suggestedTime, duration) => {
  const range1 = moment.range(
    moment(suggestedTime),
    moment(suggestedTime).add(duration, 'minutes')
  );

  for (let i = 0; i < appointments.length; i++) {
    const existedTime = appointments[i].date;
    const range2 = moment.range(
      moment(existedTime),
      moment(existedTime).add(appointments[i].duration, 'minutes')
    );
    if (range1.overlaps(range2)) { return false; }
  }
  return true;
};

exports.isEquipmentVacant = isEquipmentVacant;


const isRoomVacant = (appointments, suggestedTime, duration, capacity) => {
  if (capacity<=0) { return false; }

  const range1 = moment.range(
    moment(suggestedTime),
    moment(suggestedTime).add(duration, 'minutes')
  );

  let overlappingAppointmentCount = 0;

  for (let i = 0; i < appointments.length; i++) {
    const existedTime = appointments[i].date;
    const range2 = moment.range(
      moment(existedTime),
      moment(existedTime).add(appointments[i].duration, 'minutes')
    );

    if (range1.overlaps(range2)) { overlappingAppointmentCount++; }

    if (overlappingAppointmentCount >= capacity) { return false; }
  }
  return true;
};

exports.isRoomVacant = isRoomVacant;


const isStaffVacant = (staff, suggestedTime, duration, workHours, room) => {
  if(workHours.length === 0){
    return true;
  }
  const appointments = staff.appointments;
  const range1 = moment.range(
    moment(suggestedTime),
    moment(suggestedTime).add(duration, 'minutes')
  );

  // Check if staff does not have appointments at this time
  for (let i = 0; i < appointments.length; i++) {
    const existedTime = appointments[i].date;
    const range2 = moment.range(
      moment(existedTime),
      moment(existedTime).add(appointments[i].duration, 'minutes')
    );
    if (range1.overlaps(range2) && room._id && appointments[i].room && room._id.toString() !== appointments[i].room.toString()) { return false; }
  }

  const minutes = (suggestedTime.hours() * 60) + suggestedTime.minutes();
  // Check if staff have work hours at this time
  for (let i = 0; i < workHours.length; i++) {
    if (minutes >= workHours[i].from && minutes + duration <= workHours[i].to) {
      return true;
    }
  }
  return false;
};

exports.isStaffVacant = isStaffVacant;


const isGenderAppropriate = (staff, staffGender) => (
  staff.title && (
    (staff.title.personal === 'mr' && staffGender === 'Male') ||
    ((staff.title.personal === 'mrs' || staff.title.personal === 'ms') &&
      staffGender === 'Female'
    )
  )
);


// Must return array of equipment
const chooseEquipment = (suggestedDateTime, equipmentList, duration) => {
  const filteredList = [];

  for (let n = 0, l = equipmentList.length; n < l; n++) {
    const equipment = equipmentList[n];
    const capacity = equipment.capacity;

    const equipmentIsVacant = isRoomVacant(
      equipment.appointments,
      suggestedDateTime,
      duration,
      capacity
    );

    if (equipmentIsVacant) { filteredList.push(equipment); }
  }
  return filteredList;
};


const chooseRoom = (suggestedDateTime, rooms, duration) => {
  if (!rooms) { return null; }

  for (let n = 0, l = rooms.length; n < l; n++) {
    const room = rooms[n];
    const capacity = room.capacity;

    const roomIsVacant = isRoomVacant(
      room.appointments,
      suggestedDateTime,
      duration,
      capacity
    );

    if (roomIsVacant) { return room; }
  }
  return null;
};


const chooseStaff = (suggestedDateTime, staffOption, duration, gender, room) => {
  const staffList = _.shuffle(staffOption);
  for (let n = 0, l = staffList.length; n < l; n++) {
    const genderIsAppropriate = gender ? isGenderAppropriate(staffList[n], gender) : true;

    const staff = staffList[n];
    const dayWorkHours = staff.schedulePatterns ?
      calculateWorkingHours(suggestedDateTime, staff.schedulePatterns) :
      null;

    // If staff does not work this week day, break;
    const staffIsVacant = isStaffVacant(
      staff,
      suggestedDateTime,
      duration,
      dayWorkHours,
      room
    );
    if (staffIsVacant && genderIsAppropriate) { return staff; }
  }
  return null;
};


const getEquipmentRooms = (equipment, rooms) => {
  if (!rooms) { return null; }
  // If equipment is mobile or equipment is not specified, you can choose any room
  if (equipment.mobility || !equipment.room) {
    return rooms;
  }
  const equipmentRoom = rooms.find((item) => item._id.toString() === equipment.room.toString());
  if (equipmentRoom) { return [equipmentRoom]; }
  return null;
};


const computeDateWithTime = (date, minutes) => {
  const duration = moment.duration(minutes, 'minutes');
  return moment(date).set({
    hour: duration.hours(),
    minute: duration.minutes(),
  });
};


const areSuggestionsMade = (suggestedAppointments, serviceToSuggest) => (
  suggestedAppointments.reduce(
    (acc, item) => acc + (item.service._id.toString() === serviceToSuggest.id ? 1 : 0),
    0
  ) === serviceToSuggest.count
);


const filterAppointmentsByDate = (appointments, preferredDateFrom, preferredDateTo) =>
  appointments.filter((a) =>
      moment(a.date) >= moment(preferredDateFrom).startOf('day') &&
      moment(a.date) <= moment(preferredDateTo).endOf('day')
  );


const getMinutesByDate = (dt) => dt.getMinutes() + (60 * dt.getHours());


const filterAppointmentsByTime = (appointments, preferredTimeFrom, preferredTimeTo) => (
  appointments.filter((a) =>
    getMinutesByDate(a.date) + a.duration > preferredTimeFrom &&
    getMinutesByDate(a.date) < preferredTimeTo
  )
);


const getAggregatedServicesWithFilteredAppointments = (agServices, filteringParams) => {
  const { preferredDateFrom, preferredDateTo, preferredTimeFrom, preferredTimeTo } = filteringParams;
  return agServices.map((agService) => {
    const newAgService = agService;

    newAgService.equipmentList = newAgService.equipmentList.map((e) => {
      const eq = e;
      eq.appointments = filterAppointmentsByDate(eq.appointments, preferredDateFrom, preferredDateTo);
      eq.appointments = filterAppointmentsByTime(eq.appointments, preferredTimeFrom, preferredTimeTo);

      return eq;
    });
    newAgService.rooms = newAgService.rooms.map((r) => {
      const room = r;
      room.appointments = filterAppointmentsByDate(room.appointments, preferredDateFrom, preferredDateTo);
      room.appointments = filterAppointmentsByTime(room.appointments, preferredTimeFrom, preferredTimeTo);

      return room;
    });
    newAgService.staff = newAgService.staff.map((u) => {
      const user = u;
      user.appointments = filterAppointmentsByDate(user.appointments, preferredDateFrom, preferredDateTo);
      user.appointments = filterAppointmentsByTime(user.appointments, preferredTimeFrom, preferredTimeTo);
      return user;
    });

    return newAgService;
  });
};


const getStaffSets = (serviceDescription, staff) => {
  const { preferredStaff1, preferredStaff2, preferredStaff3 } = serviceDescription;
  if (serviceDescription.hasPreferredStaff) {
    const staffGroupedByPriority = [];
    let staff1;
    let staff2;
    let staff3;
    const restStaff = [];

    staff.forEach((item) => {
      if (preferredStaff1 && item._id.toString() === preferredStaff1.toString()) {
        staff1 = [item];
      } else if (preferredStaff2 && item._id.toString() === preferredStaff2.toString()) {
        staff2 = [item];
      } else if (preferredStaff3 && item._id.toString() === preferredStaff3.toString()) {
        staff3 = [item];
      } else {
        restStaff.push(item);
      }
    });

    if (staff1) { staffGroupedByPriority.push(staff1); }
    if (staff2) { staffGroupedByPriority.push(staff2); }
    if (staff3) { staffGroupedByPriority.push(staff3); }
    staffGroupedByPriority.push(restStaff);

    return staffGroupedByPriority;
  }

  return [staff];
};

const getAppointmentsAmountOfCurrentWeek = (currentPatient, serviceId, suggestedDate) => {

  const monday = moment(suggestedDate).startOf('isoweek');
  const sunday = moment(suggestedDate).endOf('isoweek');

  const appointmentsAmount = currentPatient.appointments.filter((appointment) => appointment.service.toString() === serviceId &&
    moment(appointment.date) > moment(monday) &&
    moment(appointment.date) < moment(sunday)
  ).length;

  return appointmentsAmount;
};

const isServiceSuggestedOnThisDay = (suggestedAppointments, suggestedDate, serviceId) => {
  return suggestedAppointments.find((item) => (
    moment(item.date).isSame(suggestedDate, 'day') && item.service._id.toString() === serviceId
  ));
};

const isServiceExistOnThisDay = (suggestedAppointments, suggestedDate, serviceId) => {
  return suggestedAppointments.find((item) =>
    moment(item.date).isSame(suggestedDate, 'day') && item.service.toString() === serviceId
  );
};


exports.suggestAppointments = (params, prescription, aggregatedServices) => {
  const {
    preferredDateFrom,
    preferredDateTo,
    preferredTimeFrom,
    preferredTimeTo,
    staffGender,
    isAlternativeSearch,
    servicesToSuggest,
    staffId,
  } = params;

  const agServices = getAggregatedServicesWithFilteredAppointments(aggregatedServices, params);
  const appointmentsSelected = params.appointmentsSelected || [];
  const dayDiff = moment(preferredDateTo).diff(moment(preferredDateFrom), 'days') + 1;
  const suggestedAppointments = [];
  const patient = prescription.patient;
  patient.appointments = filterAppointmentsByDate(patient.appointments, preferredDateFrom, preferredDateTo);

  // Convert array to object because it makes easier to get object by _id;
  const agServicesDict = agServices.reduce((acc, val) => {
    const temp = {};
    temp[val._id] = val;
    return Object.assign({}, acc, temp);
  }, {});

  servicesToSuggest.forEach((serviceToSuggest) => {
    const serviceId = serviceToSuggest.id;
    // If service doesn't have attached staff, move to the next service
    const agService = agServicesDict[serviceId];
    //TODO: Maybe, Is some kind of error message needed?..
    if (!agService || !agService.staff) { return; }

    const serviceDescription = prescription.providedServices.find((item) =>
      item.service.toString() === serviceId
    );
    //TODO: ...and here
    if (!serviceDescription) { return; }
    const { duration } = serviceDescription;

    // Because prescription can have 3 preffered staff options
    // there is an outer loop iterating on staff grouped by priorities.
    // If we have a staffId option gotten from alternative suggistion request
    // there would be only one set contains this id
    const preferredStaff = staffId ?
      [[agService.staff.find((item) => item._id.toString() === staffId)]]
      : null;
    const staffSets = preferredStaff || getStaffSets(serviceDescription, agService.staff);
    console.log('STAFFSETS', staffSets);

    for (let k = 0, l = staffSets.length; k < l; k++) {
      const staffOptions = staffSets[k];
      let appointmentsAmountOfCurrentWeek = getAppointmentsAmountOfCurrentWeek(patient, serviceId, moment(preferredDateFrom));
      let currentWeekNumber = moment(preferredDateFrom).isoWeek();
      for (let i = 0; i < dayDiff; i++) {
        // If all services of this type are suggested, break
        if (areSuggestionsMade(suggestedAppointments, serviceToSuggest)) { break; }

        const suggestedDate = moment(preferredDateFrom).add(i, 'day');
        let serviceSuggestedOnThisDay = isServiceSuggestedOnThisDay(suggestedAppointments, suggestedDate, serviceId);
        let serviceExistedOnThisDay = isServiceExistOnThisDay(patient.appointments, suggestedDate, serviceId);

        if (serviceSuggestedOnThisDay || serviceExistedOnThisDay) { continue; }
        if (suggestedDate.isoWeek() !== currentWeekNumber) {
          currentWeekNumber = suggestedDate.isoWeek();
          appointmentsAmountOfCurrentWeek = getAppointmentsAmountOfCurrentWeek(patient, serviceId, suggestedDate);
        }

        const preferredDateTimeFrom = computeDateWithTime(suggestedDate, +preferredTimeFrom);
        const preferredDateTimeTo = computeDateWithTime(suggestedDate, +preferredTimeTo);
        const suggestedDateTime = moment(preferredDateTimeFrom);

        let appointmentHasBeenSuggested = false;
        while (!appointmentHasBeenSuggested && suggestedDateTime.isSameOrBefore(preferredDateTimeTo)) {
          const currentService = prescription.providedServices.find((service) => service.service.toString() === serviceToSuggest.id);
          if (currentService && currentService.frequencyMax <= appointmentsAmountOfCurrentWeek) { break; }

          const patientIsVacant = isPatientVacant(
            [...patient.appointments, ...suggestedAppointments],
            suggestedDateTime,
            duration
          );

          if (isAlternativeSearch && !staffId && suggestedDateTime > moment(preferredDateFrom).endOf('day')) {
            break;
          }
          if (patientIsVacant) {
            let room;
            let equipment;

            if (agService.equipmentList) {
              // Choose list of equipment that is available at suggested datetime
              const equipmentList = chooseEquipment(suggestedDateTime, agService.equipmentList, duration);
              for (let m = 0, l1 = equipmentList.length; m < l1; m++) {
                const rooms = getEquipmentRooms(equipmentList[m], agService.rooms);
                room = chooseRoom(suggestedDateTime, rooms, duration);
                if (room) {
                  equipment = equipmentList[m];
                  break;
                }
              }
            } else {
              room = chooseRoom(suggestedDateTime, agService.rooms, duration);
            }

            if (room) {
              const staff = chooseStaff(suggestedDateTime, staffOptions, duration, staffGender, room);
              const isTimeSelected = appointmentsSelected.some((app) => (
                suggestedDateTime.toDate().getTime() === moment(app.date).toDate().getTime()
              ));

              if (staff && !isTimeSelected) {
                suggestedAppointments.push({
                  date: moment(suggestedDateTime),
                  prescription: { ...prescription, patient: undefined },
                  service: { ...agService, staff: undefined, rooms: undefined, equipmentList: undefined },
                  staff: { ...staff, appointments: undefined },
                  room: { ...room, appointments: undefined },
                  equipment: equipment ? { ...equipment, appointments: undefined } : undefined,
                  duration,
                });
                let lastAppointment = suggestedAppointments[suggestedAppointments.length - 1];
                if(lastAppointment &&
                  Object.keys(lastAppointment.room).length === 1 &&
                  Object.keys(lastAppointment.room)[0] === 'appointments' &&
                  lastAppointment.room.appointments === undefined
                ){
                  delete lastAppointment.room;
                }
                appointmentsAmountOfCurrentWeek += 1;
                appointmentHasBeenSuggested = !isAlternativeSearch ?
                  true :
                  areSuggestionsMade(suggestedAppointments, serviceToSuggest, serviceId);
              }
            }
          }

          suggestedDateTime.add(15, 'minute');
        }
      }
    }
  });

  // logger.debug(suggestedAppointments);
  return suggestedAppointments;
};
