const utils = require('./appointmentSuggestion');


describe('isRoomVacant()', () => {
  it('it returns false if room has appointment at this time', () => {
    const appointments = [{
      date: '2018-01-01 09:00',
      duration: 60,
    }];

    expect(utils.isRoomVacant(appointments, '2018-01-01 08:00', 60, 1)).toBeTruthy();
    expect(utils.isRoomVacant(appointments, '2018-01-01 08:30', 60, 1)).not.toBeTruthy();
    expect(utils.isRoomVacant(appointments, '2018-01-01 09:00', 60, 1)).not.toBeTruthy();
    expect(utils.isRoomVacant(appointments, '2018-01-01 09:30', 60, 1)).not.toBeTruthy();
    expect(utils.isRoomVacant(appointments, '2018-01-01 10:00', 60, 1)).toBeTruthy();
  });

  it('it returns true if room has appointment at this time but capacity is greater than 1', () => {
    const appointments = [{
      date: '2018-01-01 09:00',
      duration: 60,
    }];
    const suggestedTime = '2018-01-01 09:00';

    expect(utils.isRoomVacant(appointments, suggestedTime, 60, 2)).toBeTruthy();
    expect(utils.isRoomVacant(appointments, suggestedTime, 60, 3)).toBeTruthy();
  });
});


describe('suggestAppointments()', () => {
  it('returns staff with proper gender', () => {
    const params = {
      preferredDateFrom: '2018-01-01',
      preferredDateTo: '2018-01-02',
      preferredTimeFrom: 0,
      preferredTimeTo: 10000,
      staffGender: 'Female',
      servicesToSuggest: [{ id: 'serviceId', count: 1 }],
    };

    const prescription = {
      _id: '5966212bedda642ab9f42ef5',
      id: 4,
      patient: {
        _id: '591f767a9545f944ec246ef9',
        appointments: [],
      },
      isAsPrescribed: true,
      appointmentCount: 0,
      status: 'unscheduled',
      providedServices: [{
        _id: 'someId',
        shortName: 'SCon',
        serviceName: 'Consultation',
        duration: 45,
        service: 'serviceId',
        quantity: 2,
        frequencyMin: 3,
        frequencyMax: 5,
      }],
    };

    const maleStaff = {
      _id: '1',
      schedulePatterns: [{
        type: 'WORKING_HOURS',
        startDate: '2018-01-01',
        timeFrom: 0,
        timeTo: 100000,
      }],
      title: { personal: 'mr' },
      appointments: [],
    };

    const femaleStaff = { ...maleStaff, title: { personal: 'ms' } };

    const agServicesWithMaleStaff = [{
      _id: 'serviceId',
      staff: [],
      rooms: [{
        _id: '591b1518c11dab03f8572efc',
        capacity: 1,
        appointments: [],
      }],
      equipmentList: [],
    }];

    const agServicesWithFemaleStaff = [{ ...agServicesWithMaleStaff[0], staff: [femaleStaff] }];

    expect(utils.suggestAppointments(params, prescription, agServicesWithMaleStaff).length).toBe(0);
    expect(utils.suggestAppointments(params, prescription, agServicesWithFemaleStaff)[0].staff.title.personal).toBe('ms');
  });
});
