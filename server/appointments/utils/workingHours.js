const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const logger = require('../../logger');


const isDateMatchingPattern = (strDate, pattern) => {
  const date = moment(strDate);
  const { repeated, repeatOn, frequency, singleDay } = pattern;
  const startDate = moment(pattern.startDate);
  const endDate = !singleDay ? moment(pattern.endDate) : null;

  const isStartDate = date.isSame(moment(startDate), 'day');
  let beforeEndDate = true;
  const afterStartDate = date.isSameOrAfter(moment(startDate), 'day');

  if (!repeated && pattern.type !== 'ABSENSE' && pattern.type !== 'PUBLIC_HOLIDAY') {
    return isStartDate;
  } else if (pattern.type === 'ABSENSE' || pattern.type === 'PUBLIC_HOLIDAY') {
    beforeEndDate = date.isSameOrBefore(moment(pattern.endDate));

    return endDate ? afterStartDate && beforeEndDate : isStartDate;
  }
  beforeEndDate = !pattern.endDate || date.isSameOrBefore(moment(pattern.endDate));

  const matchesFrequency = (date.week() - moment(startDate).week()) % frequency === 0;
  const matchesDayOfWeek = repeatOn[date.day() - 1];


  return (afterStartDate && matchesFrequency && matchesDayOfWeek && beforeEndDate);
};
exports.isDateMatchingPattern = isDateMatchingPattern;

exports.calculateSummaryMinutes = (workingHours) => workingHours.reduce(
  (acc, cur) => acc + (cur.to - cur.from), 0
);

// Could it be simpler?
const addRangeToWorkingHours = (workingHours, r) => {
  const result = [];
  let added = false;

  workingHours.forEach((c) => {
    if (result.length === 0) {
      if (r.to < c.from) {
        result.push(r);
        result.push(c);
        added = true;
      } else if (r.from <= c.to) {
        result.push({
          from: Math.min(r.from, c.from),
          to: Math.max(r.to, c.to),
        });
        added = true;
      } else if (r.from > c.to) {
        result.push(c);
        result.push(r);
        added = true;
      }
    } else {
      const resultLength = result.length;
      const lower = result[resultLength - 1];

      if (r.from <= lower.to && !added) {
        const newMin = Math.min(lower.from, r.from);
        const newMax = Math.max(lower.to, r.to);

        if (c.from <= newMax) {
          result[resultLength - 1] = { from: newMin, to: Math.max(c.to, newMax) };
        } else {
          result[resultLength - 1] = { from: newMin, to: newMax };
          result.push(c);
        }
        added = true;
      } else {
        result.push(c);
      }
    }
  });

  if (!added) {
    result.push(r);
  }
  return result;
};


const subtractRangeFromWorkingHours = (workingHours, r) => {
  const result = [];
  workingHours.forEach((c) => {
    if (c.to < r.from) {
      result.push(c);
    } else if (c.from > r.to && c.to > r.to) {
      result.push({ from: r.to, to: c.to });
    } else if (r.from > c.from && r.to < c.to) {
      result.push({ from: c.from, to: r.from });
      result.push({ from: r.to, to: c.to });
    } else if (r.from > c.from && r.to > c.to) {
      result.push({ from: c.from, to: r.from });
    } else if (r.from < c.from && r.to < c.to && r.to > c.from) {
      result.push({ from: r.to, to: c.to });
    }
  });

  return result;
};


// date must be moment object
exports.calculateWorkingHours = (date, schedulePatterns) => {
  let patterns = schedulePatterns.sort((a, b)=>moment(b.startDate).isSameOrBefore(moment(a.startDate), 'day'));
  patterns = patterns.filter((pattern)=>isDateMatchingPattern(date, pattern));

  let workPatterns = patterns.filter((a)=>a.type === 'WORKING_HOURS');
  let nonworkPatterns = patterns.filter((a)=>a.type !== 'WORKING_HOURS');

  let slicedPatterns = workPatterns.length !== 0 ? [workPatterns[workPatterns.length - 1]].concat(nonworkPatterns) : [];
  return slicedPatterns.reduce((hours, pattern) => {
    if (isDateMatchingPattern(date, pattern)) {
      const { timeFrom, timeTo } = pattern;
      switch (pattern.type) {
        case 'WORKING_HOURS':
          return addRangeToWorkingHours(hours, { from: timeFrom, to: timeTo, title: pattern.title });
        case 'NON_WORKING_HOURS':
          return subtractRangeFromWorkingHours(hours, { from: timeFrom, to: timeTo });
        case 'PUBLIC_HOLIDAY':
          return subtractRangeFromWorkingHours(hours, { from: timeFrom, to: timeTo });
        case 'ABSENSE':
          return subtractRangeFromWorkingHours(hours, { from: timeFrom, to: timeTo });
      }
    }

    return hours;
  }, []);
};

exports.isDateAHoliday = (date, schedulePatterns) =>
  schedulePatterns.find((pattern) => {
    return isDateMatchingPattern(date, pattern) && pattern.type === 'PUBLIC_HOLIDAY';
  });
