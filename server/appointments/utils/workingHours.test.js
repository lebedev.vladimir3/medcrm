const workingHours = require('./workingHours');


describe('calculateWorkingHours()', () => {
  // Monday
  const date = '2018-01-01';

  // если если до этого был рабочий день с 9 до 14 и мы добавили новый паттерн с 15 до 20
  // Если нет пересечения рабочего времени, то нерабочее так и останется.
  // Т.е. в итоге должно сохраниться 2 отрезка: с 9 до 14 и с 15 до 20
  it('calculates working hours from multiple schedule patterns', () => {
    const schedulePatterns = [
      {
        type: 'WORKING_HOURS',
        startDate: date,
        timeFrom: 9,
        timeTo: 14,
      },
      {
        type: 'WORKING_HOURS',
        startDate: date,
        timeFrom: 15,
        timeTo: 20,
      },
    ];

    const result = [
      { from: 9, to: 14 },
      { from: 15, to: 20 },
    ];

    expect(workingHours.calculateWorkingHours(date, schedulePatterns)).toEqual(result);
  });

  // с любым пересечением, должен сохраниться один отрезок с крайними значениями, т.е. с 9 до 20.
  it('calculates working hours from multiple schedule patterns', () => {
    const schedulePatterns = [
      {
        type: 'WORKING_HOURS',
        startDate: date,
        timeFrom: 9,
        timeTo: 14,
      },
      {
        type: 'WORKING_HOURS',
        startDate: date,
        timeFrom: 14,
        timeTo: 20,
      },
    ];

    const result = [{ from: 9, to: 20 }];

    expect(workingHours.calculateWorkingHours(date, schedulePatterns)).toEqual(result);
  });

  it('calculates working hours with non working hours', () => {
    const schedulePatterns = [
      {
        type: 'WORKING_HOURS',
        startDate: date,
        timeFrom: 9,
        timeTo: 14,
      },
      {
        type: 'NON_WORKING_HOURS',
        startDate: date,
        timeFrom: 10,
        timeTo: 11,
      },
    ];

    const result = [{ from: 9, to: 10 }, { from: 11, to: 14 }];

    expect(workingHours.calculateWorkingHours(date, schedulePatterns)).toEqual(result);
  });
});
