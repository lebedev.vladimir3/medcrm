const passport = require('passport');
const User = require('../users/model');
const Token = require('./model');
const co = require('co');
const mongoose = require('mongoose');
const logger = require('../logger');

const toMongoId = (id) => mongoose.Types.ObjectId(id);

module.exports.login = function (req, res, next) {
  passport.authenticate('local', (err, user, token) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.sendStatus(400);
    }
    req.logIn(user, (err) => {
      if (err) {
        return next(err);
      }
      logger.info('success', { msg: 'Success! You are logged in.' });

      return token ? res.json({ user: user, token: token }) : res.json({ user: user });
    });
  })(req, res, next);
};


module.exports.logout = function (req, res) {
  req.logout();
  res.redirect('/');
};

module.exports.isAuth = function (req, res, next) {
  co(function*() {
    try {
      if (req.user){
        res.json({ user: req.user });
      } else {
        const token = yield Token.findOne({ token: req.body.token });
        if (token == null) {
          res.json({ user: null });
        } else {
          const user = yield User
            .findOne({ _id: token.user })
            .populate('role')
            .select('-password')
            .exec();

          req.logIn(user, (err) => {
            if (err) {
              return next(err);
            }
            return res.json({ user: user });
          });
        }
      }
    } catch (e) {
      next(e);
    }
  });
};


module.exports.register = function (req, res, next) {
  const user = new User({ email: req.body.email, password: req.body.password });
  user.save((err) => err ?
    next(err) :
    req.logIn(user, (innerErr) => innerErr ?
      next(innerErr) :
      res.json(user)));
};
