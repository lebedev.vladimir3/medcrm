const Router = require('express').Router;
const authController = require('./controllers');


module.exports = () => {
  const authRoutes = new Router();

  authRoutes
    .post('/login', authController.login)
    .post('/register', authController.register)
    .post('/isAuth', authController.isAuth)
    .post('/logout', authController.logout);

  return authRoutes;
};
