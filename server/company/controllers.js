const co = require('co');
const Company = require('./model');
const Patient = require('../patients/model');


exports.getCompany = function getCompany(req, res, next) {
  co(function* () {
    try {
      const company = yield Company.findOne().exec();
      res.json(company);
      // global.console.log('.get /company', company);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateOrCreateCompany = function updateOrCreateCompany(req, res, next) {
  co(function* () {
    try {
      // global.console.log('.post /company', req.body);
      const company = yield Company.findOneAndUpdate(
        {}, { $set: req.body }, { upsert: true, new: true }
      );
      res.json(company);
    } catch (e) {
      next(e);
    }
  });
};

exports.getPatientsByCompanyId = async (req, res, next) => {
  try {
    /*исправить на находждение пациентов по id компании*/
    const patients = await Patient.find({});
    res.json(patients);
  } catch (e) {
    next(e);
  }
};


exports.createUserSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const { id } = req.params;
      const user = yield User.findByIdAndUpdate(
        id,
        { $push: { schedulePatterns: req.body } },
        { new: true }
      );
      res.status(201).json(user);
    } catch (e) {
      next(e);
    }
  });
};


// =======================
// General Schedule patterns
// =======================

exports.createGeneralSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const { id } = req.params;
      const user = yield Company.findByIdAndUpdate(
        id,
        { $push: { schedulePatterns: req.body } },
        { new: true }
      );
      res.status(201).json(user);
    } catch (e) {
      next(e);
    }
  });
};


exports.getGeneralSchedulePatternList = (req, res, next) => {
  co(function* () {
    try {
      const user = yield Company.findById(req.params.id).lean().exec();
      res.status(200).json(user.schedulePatterns);
    } catch (e) {
      next(e);
    }
  });
};


exports.getGeneralSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const user = yield Company.findById(req.params.id).exec();
      const pattern = user.schedulePatterns.id(req.params.patternId);
      res.status(200).json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateGeneralSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const user = yield Company.update(
        { _id: req.params.id, 'schedulePatterns._id': req.params.patternId },
        { $set: { 'schedulePatterns.$': req.body } },
        { new: true }
      ).exec();
      // const pattern = yield user.schedulePatterns.id(req.params.patternId);
      res.status(200).json(user);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteGeneralSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      yield Company.findByIdAndUpdate(
        req.params.id,
        { $pull: { schedulePatterns: { _id: req.params.patternId } } }
      ).exec();
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};
