const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema({
  title: String,
  businessName: String,
  specialization: String,
  description: String,
  phone: String,
  fax: String,
  isDeleted: {type: Boolean, default: false},
  address: {
    street: String,
    post: String,
    city: String,
    region: String,
    country: String,
  },
  schedulePatterns: [{
    type: {
      type: String,
      enum: [
        'WORKING_HOURS',
        'NON_WORKING_HOURS',
        'PUBLIC_HOLIDAY',
      ],
    },
    title: String,
    startDate: Date,
    endDate: Date,
    hasEndDate: Boolean,
    allDay: Boolean,
    timeFrom: Number,
    timeTo: Number,
    repeated: Boolean,
    // 7 elements in array are 7 week days
    repeatOn: [
      // 0
      { type: Boolean },
      // 1
      { type: Boolean },
      // 2
      { type: Boolean },
      // 3
      { type: Boolean },
      // 4
      { type: Boolean },
      // 5
      { type: Boolean },
      // 6
      { type: Boolean },
    ],
    frequency: {
      type: Number,
      default: 1,
      min: 1,
      max: 12,
    },
  }],
  email: String,
  website: String,
  settings: {
    appointment: {
      maximumShiftBetweenServices: {
        type: Number,
        min: 0,
        default: 0
      },
      minimumShiftBetweenProposed: {
        type: Number,
        min: 0,
        default: 0
      }
    },
  }
}, { timestamps: true, virtuals: true });


const Company = mongoose.model('Company', CompanySchema);

module.exports = Company;
