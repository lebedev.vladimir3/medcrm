const Router = require('express').Router;
const companyController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;


module.exports = () => {
  const companyRoutes = new Router();

  companyRoutes
    .get('/', companyController.getCompany)
    .get('/:id/patients', companyController.getPatientsByCompanyId)
    .post('/', user.is(roles.ADMIN), companyController.updateOrCreateCompany);

  return companyRoutes;
};
