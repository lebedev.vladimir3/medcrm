/*eslint-disable */

const passport = require('passport');
const request = require('request');
const crypto = require('crypto');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../users/model');
const Role = require('../roles/model');
const Token = require('../auth/model');
const cnf = require('./local-config');
const secretOrKey = cnf.st.token_salt;

let hashToken = token => crypto.createHash('md5').update(token).digest('hex');

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    if(err || !user){
      done(err);
    }else {
      Role.findById(user.role, (err, role) => {
        user.role = role;
        done(err, user);
      });
    }
  });
});


/**
 * Sign in using Email and Password.
 */
passport.use(new LocalStrategy({ usernameField: 'email', passReqToCallback: true }, (req, email, password, done) => {
  User.findOne({ email: email.toLowerCase() }, (err, user) => {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { msg: `Email ${email} not found.` });
    }

    user.comparePassword(password, (err, isMatch) => {
      if (err) {
        return done(err);
      }
      if (isMatch) {
        return Role.findById(user.role, (err, role) => {
          user.role = role;
          const token = {};
          token.date = new Date();
          token.user = user._id;
          token.userAgent = req.get('User-Agent');
          token.token = (hashToken(token.userAgent + token.date.getTime().toString() + Math.random().toString() + secretOrKey)).toString();
          Token.create(token);
          return done(null, user, token.token);
        })
      }
      return done(null, false, { msg: 'Invalid email or password.' });
    });
  }).select('+password');
}));


/**
 * Login Required middleware.
 */
exports.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    if (req.headers.referer) {
      req.body.query = {};
      if (req.method == 'GET') {
        req.body.query = {$or: [{isDeleted: {$exists: false}}, {isDeleted: false}]};
      }
      return next();
    } else {
      res.status(404).json({ message: 'Page not found' });
    }
  } else {
    res.status(401).json({ message: 'Not authorized' });
  }
};

/**
 * Authorization Required middleware.
 */
exports.isAuthorized = (req, res, next) => {
  const provider = req.path.split('/').slice(-1)[ 0 ];
  const token = req.user.tokens.find((token) => token.kind === provider);
  if (token) {
    next();
  } else {
    res.status(401).json({ message: 'Not authorized' });
  }
};
