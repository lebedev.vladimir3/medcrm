const co = require('co');
const mongoose = require('mongoose');
const Equipment = require('./model');

const toMongoId = (id) => mongoose.Types.ObjectId(id);

exports.getEquipmentList = (req, res, next) => {
  co(function*() {
    try {
      const equipment = yield Equipment.find(req.body.query).populate(['room', 'alternativeRooms']).exec();
      res.json(equipment);
    } catch (e) {
      next(e);
    }
  });
};


exports.getEquipment = (req, res, next) => {
  co(function*() {
    try {
      const equipment = yield Equipment.findById(req.params.id).exec();
      res.json(equipment);
    } catch (e) {
      next(e);
    }
  });
};


exports.createEquipment = (req, res, next) => {
  co(function* () {
    try {
      const equipment = new Equipment(req.body);
      yield equipment.save();
      res
        .status(201)
        .json(equipment);
    } catch (e) {
      res
        .status(201)
        .json(e);
    }
  });
};


exports.updateEquipment = (req, res, next) => {
  co(function* () {
    try {
      const equipment = yield Equipment.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });
      res.json(equipment);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteEquipment = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield Equipment.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};
