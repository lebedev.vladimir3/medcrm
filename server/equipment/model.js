const mongoose = require('mongoose');


const EquipmentSchema = new mongoose.Schema({
  name: { type: String, required: true },
  shortName: { type: String, required: true, unique: true },
  specification: { type: String },
  mobility: { type: Boolean, default: false },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
  },
  alternativeRooms: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
  }],
  allRoomsAreAlternative: { type: Boolean },
  isDeleted: {type: Boolean, default: false},
}, { timestamps: true, virtuals: true });


const Equipment = mongoose.model('Equipment', EquipmentSchema);
module.exports = Equipment;
