const Router = require('express').Router;
const equipmentsController = require('./controllers');
const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const patientsRoutes = new Router();

  patientsRoutes
    .get('/', equipmentsController.getEquipmentList)
    .post('/', user.is(roles.ADMIN), equipmentsController.createEquipment)
    .get('/:id', equipmentsController.getEquipment)
    .put('/:id', user.is(roles.ADMIN), equipmentsController.updateEquipment)
    .delete('/:id', user.is(roles.ADMIN), equipmentsController.deleteEquipment);

  return patientsRoutes;
};
