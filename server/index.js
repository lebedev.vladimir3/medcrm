/* eslint consistent-return:0 */

require('dotenv').config();
const express = require('express');
const logger = require('./logger');
const expressWinston = require('express-winston');
const session = require('express-session');
const argv = require('minimist')(process.argv.slice(2));
const setup = require('./middlewares/frontendMiddleware');
const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const chalk = require('chalk');
const ip = require('ip');
const authorization = require('./utils/authorization/authorization');
const cnf = require('./config/local-config');


const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(cnf.db.mongo_uri, { server: { socketOptions: { connectTimeoutMS: 20000 } } });
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: cnf.st.session_salt,
  store: new MongoStore({
    url: cnf.db.mongo_uri,
    autoReconnect: true,
    clear_interval: 3600,
  }),
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(authorization.user.middleware());

app.use(expressWinston.logger({
  winstonInstance: logger,
  msg: '{{res.statusCode}} {{req.method}} {{res.responseTime}}ms userId: {{req.user ? req.user._id : "-"}} {{req.url}}',
  colorize: true,
  meta: false,
}));

app.use(require('./routes')(app));

app.use(expressWinston.errorLogger({
  winstonInstance: logger,
  colorize: true,
}));

// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

const port = argv.port || process.env.PORT || 3000;

// Start your app.
app.listen(port, host, (err) => {
  if (err) {
    return logger.fatal(err.message);
  }

  // Connect to ngrok in dev mode
  logger.info('\nServer started ! ✓');
  logger.info('Access URLs:');
  logger.info(`Localhost: http://${prettyHost}:${port}`);
  logger.info(`LAN: http://${ip.address()}:${port}`);
  if (ngrok) {
    ngrok.connect(port, (innerErr, url) => {
      if (innerErr) {
        return logger.fatal(innerErr.message);
      }

      logger.info('Tunnel initialised ✓)}');
      logger.info(`\n    Proxy: ${url}`);
    });
  }
});

