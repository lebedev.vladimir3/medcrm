/* eslint-disable no-console */
const winston = require('winston');
const isDev = process.env.NODE_ENV !== 'production';

// set default log level.
const logLevel = isDev ? 'debug' : 'info';


const logger = new (winston.Logger)({
  level: logLevel,
  transports: [
    new (winston.transports.Console)({
      colorize: true,
      timestamp: true,
      prettyPrint: true,
    }),
    new (winston.transports.File)({
      filename: `/tmp/medcrm-${new Date().toISOString().slice(0,10)}.log`,
      prettyPrint: true,
    }),
  ],
});


module.exports = logger;
