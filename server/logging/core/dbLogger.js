const LoggingFactory = require('./loggingFactory');
const Logging = require('../model');

class DbLogger {
  constructor(){
    this.reset();
  }
  reset(){
    this.queue = [];
  }
  add(modelType, actionType, req){
    this.queue.push(LoggingFactory.create(modelType, actionType, req))
  }
  async executeAll(){
    return await Promise.all(this.queue.map(log => Logging.create(log.objectParams)))
  }
}

const middleware = (req, res, next) => {
  req.dbLogger = new DbLogger();
  next();
};
module.exports.middleware = middleware;
module.exports.class = DbLogger;

