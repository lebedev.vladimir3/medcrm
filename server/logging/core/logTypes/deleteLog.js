const Log = require('./log');

class DeleteLog extends Log {
  constructor(modelType, actionType, req){
    super(modelType, actionType, req);
    this.targetId = this.req.params.id;
  }
}

module.exports = DeleteLog;
