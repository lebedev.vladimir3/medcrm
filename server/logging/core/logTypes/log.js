class Log {
  constructor(modelType, actionType, req){
    this.targetId = req.body._id;
    this.modelType = modelType;
    this.sourceUserId = req.user && req.user._id;
    this.actionType = actionType;
    this.req = req;
  }
  get objectParams(){
    return {
      targetId: this.targetId,
      modelType: this.modelType,
      sourceUserId: this.sourceUserId,
      actionType: this.actionType
    }
  }
}

module.exports = Log;
