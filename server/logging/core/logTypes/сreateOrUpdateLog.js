const Log = require('./log');

class CreateOrUpdateLog extends Log {
  constructor(modelType, actionType, req){
    super(modelType, actionType, req);
    this.newValue = req.body;
    this.oldValue = '';
  }
  get objectParams(){
    const parentParams = super.objectParams;
    return {
      ...parentParams,
      newValue: this.newValue,
      oldValue: this.oldValue
    }
  }
}

module.exports = CreateOrUpdateLog;
