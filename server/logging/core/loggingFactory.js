const DeleteLog = require('./logTypes/deleteLog');
const CreateOrUpdateLog = require('./logTypes/сreateOrUpdateLog');
const actions = require('../actions');

class LoggingFactory {
  static create(modelType, actionType, req){
    switch (actionType) {
      case actions.DELETE: {
        return new DeleteLog(modelType, actionType, req)
      }
      default: {
        return new CreateOrUpdateLog(modelType, actionType, req)
      }
    }
  }
}

module.exports = LoggingFactory;
