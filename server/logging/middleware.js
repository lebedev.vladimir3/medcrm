const co = require('co');
const Logging = require('./model');
const LoggingFactory = require('./core/loggingFactory');

const asyncMiddleware = require('../utils/asyncMiddleware');

module.exports = (modelType, actionType) => asyncMiddleware(async (req, res, next) => {
  if(modelType && actionType){
    Logging.create(LoggingFactory.create(modelType, actionType, req).objectParams);
  } else if (!modelType && !actionType) {
    req.dbLogger.executeAll();
  }
  if(req.result){
    res.status(req.resultStatus || 201).json(req.result);
  } else {
    res.sendStatus(req.resultStatus || 201)
  }
});
