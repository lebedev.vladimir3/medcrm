const mongoose = require('mongoose');

const LoggingSchema = new mongoose.Schema({
  date: { type: Date, default: new Date },
  modelType: { type: String, required: true },
  actionType: { type: String, required: true },
  sourceUserId: { type: mongoose.Schema.Types.ObjectId, required: true },
  targetId: { type: mongoose.Schema.Types.Mixed, default: '' },
  oldValue: { type: mongoose.Schema.Types.Mixed, default: '' },
  newValue: { type: mongoose.Schema.Types.Mixed, default: '' },
}, { timestamps: true });

const loggingSchema = mongoose.model('Logging', LoggingSchema);

module.exports = loggingSchema;
