const co = require('co');
const mongoose = require('mongoose');
const MedServiceCategory = require('./model');

const toMongoId = (id) => mongoose.Types.ObjectId(id);


exports.getMedServiceCategoryList = (req, res, next) => {
  co(function* () {
    try {
      const roles = yield MedServiceCategory.find(req.body.query).sort('name').exec();

      res.json(roles);
    } catch (e) {
      next(e);
    }
  });
};


exports.createMedServiceCategory = (req, res, next) => {
  co(function* () {
    try {
      const role = new MedServiceCategory(req.body);

      role.save((err) => err
          ? next(err)
          : req.logIn(role, (innerErr) => innerErr
              ? next(innerErr)
              : res.json(role)));
    } catch (e) {
      next(e);
    }
  });
};


exports.updateMedServiceCategories = (req, res, next) => {
  co(function* () {
    try {
      // Are there any better ways?
      yield MedServiceCategory.find({ _id: { $nin: req.body.map((obj) => obj._id) } }).remove();
      yield* req.body.map((obj) => {
        if (!obj._id) {
          return MedServiceCategory.create(obj);
        }
        return MedServiceCategory.update({ _id: obj._id }, { $set: obj });
      });

      const roles = yield MedServiceCategory.find().exec();
      res.json(roles);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteMedServiceCategory = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield MedServiceCategory.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};