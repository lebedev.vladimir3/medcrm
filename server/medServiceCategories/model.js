const mongoose = require('mongoose');

const MedServiceCategorySchema = new mongoose.Schema({
  name: { type: String, required: true },
  color: { type: String },
  isDeleted: {type: Boolean, default: false},
}, { timestamps: true, virtuals: true });


const MedServiceCategory = mongoose.model('MedServiceCategory', MedServiceCategorySchema);
module.exports = MedServiceCategory;
