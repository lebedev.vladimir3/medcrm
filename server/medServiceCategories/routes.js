const Router = require('express').Router;
const categoryController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const authRoutes = new Router();

  authRoutes
    .get('/', categoryController.getMedServiceCategoryList)
    .post('/', user.is(roles.ADMIN), categoryController.createMedServiceCategory)
    .put('/', user.is(roles.ADMIN), categoryController.updateMedServiceCategories)
    .delete('/', user.is(roles.ADMIN), categoryController.deleteMedServiceCategory);

  return authRoutes;
};
