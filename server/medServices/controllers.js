const co = require('co');
const MedService = require('./model');


exports.fetchMedservice = (req, res, next) => {
  co(function*() {
    try {
      const medServices = yield MedService.find(req.body.query).sort('name').exec();
      res
        .status(201)
        .json(medServices);
    } catch (e) {
      next(e);
    }
  });
};


exports.createMedservice = (req, res, next) => {
  co(function*() {
    try {
      const medService = yield MedService.create(req.body);

      res
        .status(201)
        .json(medService);
    } catch (e) {
      next(e);
    }
  });
};

exports.getMedService = (req, res, next) => {
  co(function*() {
    try {
      const medService = yield MedService.findById(req.params.id)
        .populate('staffAllocation.staff')
        .lean()
        .exec();

      if (medService.staffAllocation) {
        medService.staffAllocation = medService.staffAllocation.map((item) => ({
          ...item,
          // Staff's role could have been changed
          // or even have not been set here
          // therefore set it explicitly
          role: (item.isAnyStaff || !item.staff) ? item.role : item.staff.role,
          staff: item.staff ? item.staff._id : item.staff,
        }));
      }

      res
        .status(200)
        .json({
          ...medService,
          equipmentAllocation: medService.equipmentAllocation.map((item) => ({ _id: item.equipment })),
        });
    } catch (e) {
      next(e);
    }
  });
};

exports.updateMedService = (req, res, next) => {
  co(function*() {
    try {
      const medservice = yield MedService.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });

      res.json(medservice);
    } catch (e) {
      next(e);
    }
  });
};

exports.deleteMedService = (req, res, next) => {
  co(function*() {
    try {
      yield MedService.findByIdAndUpdate(req.params.id, {$set: {isDeleted: true}});

      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};

exports.medServicesWithStaff = (req, res, next) => {
  co(function*() {
    try {

      const medServices = yield MedService.find()
        .populate({
          path: 'staffAllocation.staff',
          model: 'User',
        })
        .exec();

      res
        .status(201)
        .json(medServices);
    } catch (e) {
      next(e);
    }
  });
};

