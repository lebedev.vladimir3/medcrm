const mongoose = require('mongoose');

const MedServiceSchema = new mongoose.Schema({
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MedServiceCategory',
  },
  categoryColor: {
    type: String,
  },
  company: { type: mongoose.SchemaTypes.ObjectId, required: true },
  name: {
    defaultName: { type: String, required: true },
    shortName: { type: String, unique: true, required: true },
    longName: { type: String },
  },
  payers: [{
    payer: { type: String, enum: ['ALL', '1', '3', '5', '7', 'P'] },
    duration: { type: Number },
    price: { type: Number },
  }],
  isDeleted: {type: Boolean, default: false},
  staffAllocation: [{
    isAnyStaff: { type: Boolean },
    role: {
      type: String,
      ref: 'Role',
    },
    staff: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    priority: { type: Number },
  }],
  equipmentAllocation: [{
    equipment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Equipment',
    },
  }],
  roomAllocation: [{
    room: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Room',
    },
  }],
  isAnyRoom: Boolean,
}, { timestamps: true, virtuals: true });


const MedService = mongoose.model('MedService', MedServiceSchema);

module.exports = MedService;

