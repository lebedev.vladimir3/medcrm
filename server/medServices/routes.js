const Router = require('express').Router;
const medserviceController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const patientsRoutes = new Router();

  patientsRoutes
    .get('/', medserviceController.fetchMedservice)
    .post('/', user.is(roles.ADMIN), medserviceController.createMedservice)
    .get('/id/:id', medserviceController.getMedService)
    .put('/id/:id', user.is(roles.ADMIN), medserviceController.updateMedService)
    .delete('/id/:id', user.is(roles.ADMIN), medserviceController.deleteMedService)
    .get('/get/withFullStaff', medserviceController.medServicesWithStaff);


  return patientsRoutes;
};
