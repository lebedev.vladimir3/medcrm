const co = require('co');
const moment = require('moment');
const GeneralSchedulePattern = require('../schedulePatterns/model');
const calculateWorkingHours = require('../appointments/utils/workingHours').calculateWorkingHours;
const calculateSummaryMinutes = require('../appointments/utils/workingHours').calculateSummaryMinutes;
const isDateAHoliday = require('../appointments/utils/workingHours').isDateAHoliday;


exports.getWorkingHours = (req, res, next) => {
  co(function*() {
    try {
      const { dateFrom, dateTo } = req.query;
      const patterns = yield GeneralSchedulePattern.find(req.body.query).lean().exec();
      const days = [];

      for (let dateIterate = moment(dateFrom); dateIterate.isSameOrBefore(moment(dateTo)); dateIterate.add(1, 'days')) {
        const isHoliday = isDateAHoliday(dateIterate, patterns);
        const workingHours = calculateWorkingHours(dateIterate, patterns);

        days.push({
          workingHours,
          summaryMinutes: calculateSummaryMinutes(workingHours),
          date: moment(dateIterate),
          holiday: isHoliday ? isHoliday : ''
        });
      }

      res.status(200).json(days);
    } catch (e) {
      next(e);
    }
  });
};
