const Router = require('express').Router;
const passportConfig = require('../config/passport');
const controller = require('./controllers');


module.exports = () => {
  const routes = new Router();

  routes.get('/', controller.getWorkingHours);

  return routes;
};
