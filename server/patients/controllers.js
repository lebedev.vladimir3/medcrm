const co = require('co');
const Patient = require('./model');
const Prescription = require('../prescriptions/model');
const UsersLogging = require('../userslogging/model');
const Payer = require('../payers/model');
const Logging = require('../logging/model');
const logConsts = require('../logging').constants;

const mongoose = require('mongoose');
const logger = require('../logger');

const asyncMiddleware = require('../utils/asyncMiddleware');

const findByIdAndUpdateUsersLoggs = (req, objectModel,method )=>{
  return UsersLogging.findByIdAndUpdate(req.user ? req.user._id : '-',
    {$push:
    {"actions":
    {date: new Date,
      data: req.body,
      objectModel: objectModel,
      method: method }}
    }, { $new: true,upsert: true,setDefaultsOnInsert: true });
}

exports.getPatient = (req, res, next) => {
  co(function* () {
    try {
      const patient = yield Patient.findById(req.params.id).lean().exec();
      res.status(200).json(patient);
    } catch (e) {
      next(e);
    }
  });
};

exports.getPatients = (req, res, next) => {
  co(function* () {
    try {
      const patients = yield Patient.aggregate([
        { $match: {$or: [{isDeleted: {$exists: false}}, {isDeleted: false}]}},
        { $lookup: {
          from: 'prescriptions',
          localField: '_id',
          foreignField: 'patient',
          as: 'prescriptions',
        } },
        { $unwind: { path: '$prescriptions', preserveNullAndEmptyArrays: true } },
        { $lookup: {
          from: 'appointments',
          localField: 'prescriptions._id',
          foreignField: 'prescription',
          as: 'appointments',
        } },
        { $unwind: { path: '$appointments', preserveNullAndEmptyArrays: true } },
        { $group: {
          _id: '$_id',
          id: { $first: '$id' },
          firstName: { $first: '$firstName' },
          lastName: { $first: '$lastName' },
          phone: { $first: '$phone' },
          birth: { $first: '$birth' },
          title: { $first: '$title' },
          notes: { $first: '$notes' },
          prescriptions: { $push: '$prescriptions' },
          appointments: { $push: '$appointments' },
        } },
      ]).exec();

      res.json(patients);
    } catch (e) {
      next(e);
    }
  });
};

exports.createPatient = (req, res, next) => {
  co(function* () {
    try {
      const patient = new Patient(req.body);
      yield patient.save();

      const { payerName } = req.body;
      if (payerName) {
        yield Payer.update({
          name: payerName },
          { $setOnInsert: { name: payerName } },
          { upsert: true }
        );
      }

      req.result = patient;
      next();
    } catch (e) {
      next(e);
    }
  });
};

exports.updatePatient = (req, res, next) => {
  co(function* () {
    try {
      const patient = yield Patient.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });

      const { payerName } = req.body;
      if (payerName) {
        yield Payer.update({
          name: payerName },
          { $setOnInsert: { name: payerName } },
          { upsert: true }
        );
      }

      req.result = patient;
      next();
      //res.json(patient);
    } catch (e) {
      next(e);
    }
  });
};

exports.deletePatient = (req, res, next) => {
  co(function* () {
    try {
      yield Patient.findByIdAndUpdate(req.params.id, {$set: {isDeleted: true}});
      req.resultStatus = 204;
      next();
    } catch (e) {
      next(e);
    }
  });
};

exports.getChangesHistory = asyncMiddleware(async (req, res, next) => {
  res.json(await Logging.find({ modelType: logConsts.PATIENT }).lean());
});
