const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;
const logSchema = require('../logging').schema;

const PatientSchema = new mongoose.Schema({
  id: { type: Number },
  company: { type: mongoose.SchemaTypes.ObjectId, required: true },
  title: {
    personal: { type: String, enum: ['mr', 'mrs', 'ms'], required: true },
    honorific: { type: String },
  },
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  birth: {
    dd: { type: String, required: true },
    mm: { type: String, required: true },
    yyyy: { type: String, required: true },
  },
  gender: { type: String, enum: ['male', 'female'], required: true },
  email: String,
  phone: {
    main: { type: String },
    secondary: { type: String },
  },
  address: {
    street: String,
    postbox: String,
    city: String,
    region: String,
    country: String,
  },
  payerStatus: {
    type: String,
    enum: ['1', '3', '5', '7', 'P'],
  },
  payerName: String,
  isDeleted: {type: Boolean, default: false},
  insuranceNumber: String,
  notes: String,
  prescriptionCount: { type: Number, default: 0 },
  createdAt: { type: Date, required: true, default: Date.now },
}, { timestamps: true, virtuals: true });


const addGenderField = patientData => {
  const patient = patientData;
  if (patient.title && patient.title.personal) {
    patient.gender = patient.title.personal === 'Herr' ? 'male' : 'female';
  }
  return patient;
};

PatientSchema.pre('findOneAndUpdate', function preUpdate(next) {
  addGenderField(this._update.$set); // eslint-disable-line no-underscore-dangle
  return next();
});

PatientSchema.pre('validate', function preValidate(next) {
  addGenderField(this);
  return next();
});

autoIncrement.initialize(mongoose.connection);
PatientSchema.plugin(autoIncrement.plugin, { model: 'Patient', field: 'id', startAt: 1 });

const Patient = mongoose.model('Patient', PatientSchema);

module.exports = Patient;
