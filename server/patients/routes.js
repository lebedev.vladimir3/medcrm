const Router = require('express').Router;
const patientsController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const actions = authorization.actions;

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;

module.exports = () => {
  const patientsRoutes = new Router();

  patientsRoutes
    .get('/', patientsController.getPatients)
    .post('/', user.can(actions.CAN_CHANGE_PATIENTS), patientsController.createPatient, logMiddleware(logConsts.PATIENT, logActions.CREATE))
    .get('/changes-history', patientsController.getChangesHistory)
    .get('/:id', user.can(actions.CAN_CHANGE_PATIENTS), patientsController.getPatient)
    .put('/:id', user.can(actions.CAN_CHANGE_PATIENTS),  patientsController.updatePatient, logMiddleware(logConsts.PATIENT, logActions.UPDATE))
    .delete('/:id', user.can(actions.CAN_CHANGE_PATIENTS), patientsController.deletePatient, logMiddleware(logConsts.PATIENT, logActions.DELETE))
  // .delete('/', passportConfig.isAuthenticated, patientsController.deletePatients);

  return patientsRoutes;
};
