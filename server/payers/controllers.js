const co = require('co');
const Payer = require('./model');


exports.getPayerList = (req, res, next) => {
  co(function* () {
    try {
      const payers = yield Payer.find(req.body.query).exec();
      res.status(200).json(payers);
    } catch (e) {
      next(e);
    }
  });
};
