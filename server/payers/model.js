const mongoose = require('mongoose');


const PayerSchema = new mongoose.Schema({
  name: { type: String, required: true },
  isDeleted: {type: Boolean, default: false},
}, { timestamps: true, virtuals: true });


const Payer = mongoose.model('Payer', PayerSchema);
module.exports = Payer;
