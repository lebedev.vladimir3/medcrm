const Router = require('express').Router;
const payersController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const payersRoutes = new Router();

  payersRoutes
    .get('/', payersController.getPayerList);

  return payersRoutes;
};
