const co = require('co');
const mongoose = require('mongoose');
const Prescription = require('./model');
const Appointment = require('../appointments/model');
const User = require('../users/model');
const toMongoId = (id) => mongoose.Types.ObjectId(id);

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;
const Logging = require('../logging/model');
const asyncMiddleware = require('../utils/asyncMiddleware');

const STATUS_NAMES = require('../../common/constants/statuses').STATUS_NAMES;

exports.loadPrescription = (req, res, next) => {
  co(function*() {
    try {
      const prescription = yield Prescription.findById(req.params.id)
        .populate({
          path: 'providedServices.service prescribedServices.service',
          model: 'MedService',
          populate: {
            path: 'staffAllocation.staff',
            model: 'User',
          },
        })
        .exec();

      const appointments = yield Appointment
        .find({ prescription: toMongoId(req.params.id) })
        .populate('staff service prescription room')
        .lean()
        .exec();
      res
        .status(201)
        .json(Object.assign({}, prescription.toJSON(), { appointments }));
    } catch (e) {
      next(e);
    }
  });
};


exports.loadPrescriptions = (req, res, next) => {
  co(function*() {
    try {
      const prescriptions = yield Prescription
        .find(req.query)
        .populate('providedServices.service prescribedServices.service')
        .exec();

      res
        .status(201)
        .json(prescriptions);
    } catch (e) {
      next(e);
    }
  });
};


exports.createPrescription = (req, res, next) => {
  co(function*() {
    try {
      req.result = yield Prescription.create(req.body);
      next();
    } catch (e) {
      next(e);
    }
  });
};


exports.updatePrescription = (req, res, next) => {
  const dbLogger = req.dbLogger;
  co(function*() {
    try {

      const oldPrescription = yield Prescription.findById(req.params.id);
      const needToCheckEqual = ['status', 'financialStatus'];
      for(let key of needToCheckEqual){
        if(req.body[key] !== oldPrescription[key]) {
          req.body[key + 'ChangedAt'] = new Date();
        }
      }

      const prescription = yield Prescription.findByIdAndUpdate(
        req.params.id,
        { $set: req.body },
        { new: true }
      ).lean();

      dbLogger.add(logConsts.PRESCRIPTION, logActions.UPDATE, {
        ...req,
        body: { prescription }
      });

      if (req.body.appointments) {
        let appointmentsRequest = [];
        req.body.appointments.forEach((item) => {
          appointmentsRequest.push(Appointment.findByIdAndUpdate(
            item._id,
            { $set: { treatmentReport: item.treatmentReport } },
            { new: true }
          ));
        });
        try {
          yield Promise.all(appointmentsRequest);
          req.body.appointments.forEach((item) => {
            dbLogger.add(logConsts.APPOINTMENT, logActions.UPDATE, {
              ...req,
              body: item
            });
          });
        } catch (e){
          next(e);
        }
      }

      req.result = prescription;
      next();
    } catch (e) {
      next(e);
    }
  });
};

exports.cancelPrescription = (req, res, next) => {
  co(function*() {
    try {
      const id = toMongoId(req.params.id);
      yield Prescription.findByIdAndUpdate(id,
        { $set: { status: STATUS_NAMES.CANCELLED } });
      req.resultStatus = 204;
      next();
    } catch (e) {
      next(e);
    }
  });
};

// exports.deletePrescription = (req, res, next) => {
//   co(function*() {
//     try {
//       const id = toMongoId(req.params.id);
//       yield Prescription.findByIdAndUpdate(id);
//       req.resultStatus = 204;
//       next();
//     } catch (e) {
//       next(e);
//     }
//   });
// };

exports.getChangesHistory = asyncMiddleware(async (req, res, next) => {
  res.json(await Logging.find({ modelType: logConsts.PRESCRIPTION }).lean());
});
