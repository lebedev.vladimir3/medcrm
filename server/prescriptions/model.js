const mongoose = require('mongoose');
const Patient = require('../patients/model');
const STATUSES = require('../../common/constants/statuses');
const PRESCRIPTION_STATUSES = STATUSES.PRESCRIPTION_STATUSES;
const PRESCRIPTION_FINANCIAL_DEFAULT = STATUSES.PRESCRIPTION_FINANCIAL_DEFAULT;
const PRESCRIPTION_GENERAL_DEFAULT = STATUSES.PRESCRIPTION_GENERAL_DEFAULT;

const ServiceSubSchema = {
  service: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MedService',
  },
  preferredStaff1: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  preferredStaff2: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  preferredStaff3: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  serviceName: String,
  isDeleted: {type: Boolean, default: false},
  shortName: String,
  frequencyMin: Number,
  frequencyMax: Number,
  duration: Number,
  quantity: Number,
  hasPreferredStaff: Boolean,
};


const PrescriptionSchema = new mongoose.Schema({
  id: Number,
  payerName: String,
  isBefreit: Boolean,
  isAsPrescribed: Boolean,
  issuedDate: Date,
  maxStartDate: Date,
  icd1: String,
  icd2: String,
  regulationType: String,
  indicationCode: String,
  isHouseVisit: Boolean,
  isGroup: Boolean,
  isReport: Boolean,
  diagnosis: String,
  notes: String,
  taxationType: String,
  bsnr: String,
  lanr: String,
  providedServicesField: String,
  prescribedServicesField: String,
  status: {
    type: String,
    //required: true,
    enum: PRESCRIPTION_STATUSES.GENERAL,
    default: PRESCRIPTION_GENERAL_DEFAULT,
  },
  statusChangedAt: {
    type: Date,
    default: new Date()
  },
  financialStatus: {
    type: String,
    default: PRESCRIPTION_FINANCIAL_DEFAULT,
    enum: PRESCRIPTION_STATUSES.FINANCIAL,
  },
  financialStatusChangedAt: {
    type: Date,
    default: new Date()
  },
  staff: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  patient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient',
  },
  providedServices: [ServiceSubSchema],
  prescribedServices: [ServiceSubSchema],
  appointmentCount: { type: Number, default: 0 },
}, { timestamps: true, virtuals: true });


PrescriptionSchema.pre('save', function preSave(next) {
  const prescription = this;

  if (prescription.isNew) {
    Patient.findById(prescription.patient, (err, patient) => {
      patient.prescriptionCount++;
      prescription.id = patient.prescriptionCount;
      patient.save();
      return next();
    });
  }
});

const autoPopulateStaff = function(next){
  this.populate('staff');
  next();
};

PrescriptionSchema.
  pre('findOne', autoPopulateStaff).
  pre('find', autoPopulateStaff);


const Prescription = mongoose.model('Prescription', PrescriptionSchema);

module.exports = Prescription;
