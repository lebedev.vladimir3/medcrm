const Router = require('express').Router;
const prescriptionController = require('./controllers');
const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;
const actions = authorization.actions;

const logActions = require('../logging').actions;
const logMiddleware = require('../logging').middleware;
const logConsts = require('../logging').constants;

module.exports = () => {
  const prescriptionRoutes = new Router();

  prescriptionRoutes
    .get('/changes-history', prescriptionController.getChangesHistory)
    .get('/:id', prescriptionController.loadPrescription)
    .get('/', prescriptionController.loadPrescriptions)
    .post('/', user.can(actions.CAN_CHANGE_PRESCRIPTIONS), prescriptionController.createPrescription, logMiddleware(logConsts.PRESCRIPTION, logActions.CREATE))
    .put('/:id', user.can(actions.CAN_CHANGE_PRESCRIPTIONS), prescriptionController.updatePrescription, logMiddleware())
    //.delete('/:id', user.is(roles.ADMIN), prescriptionController.deletePrescription, logMiddleware(logConsts.PRESCRIPTION, logActions.DELETE));
    .delete('/:id', user.is(roles.ADMIN), prescriptionController.cancelPrescription, logMiddleware(logConsts.PRESCRIPTION, logActions.CANCEL));

  return prescriptionRoutes;
};
