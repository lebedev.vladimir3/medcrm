const co = require('co');
const mongoose = require('mongoose');
const Role = require('./model');

const toMongoId = (id) => mongoose.Types.ObjectId(id);


exports.getRoleList = (req, res, next) => {
  co(function* () {
    try {
      const roles = yield Role.find(req.body.query).sort('name').exec();

      res.json(roles);
    } catch (e) {
      next(e);
    }
  });
};


exports.createRole = (req, res, next) => {
  co(function* () {
    try {
      const role = new Role(req.body);

      role.save((err) => err
          ? next(err)
          : req.logIn(role, (innerErr) => innerErr
              ? next(innerErr)
              : res.json(role)));
    } catch (e) {
      next(e);
    }
  });
};


exports.updateRoles = (req, res, next) => {
  co(function* () {
    try {
      // Are there any better ways?
      yield* req.body.map((obj) => Role.update({ _id: obj._id }, { $set: obj }));

      const roles = yield Role.find().exec();
      res.json(roles);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteRole = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield Role.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};
