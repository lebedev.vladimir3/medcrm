const mongoose = require('mongoose');

const RoleSchema = new mongoose.Schema({
  _id: String,
  name: String,
  accessToSettings: Boolean,
  canMakeAppointments: Boolean,
  canSetOwnSchedule: Boolean,
  isDeleted: {type: Boolean, default: false}
}, { timestamps: true, virtuals: true });


const Role = mongoose.model('Role', RoleSchema);

module.exports = Role;
