const Router = require('express').Router;
const roleController = require('./controllers');
const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const authRoutes = new Router();

  authRoutes
    .get('/', roleController.getRoleList)
    .post('/', user.is(roles.ADMIN), roleController.createRole)
    .put('/', user.is(roles.ADMIN), roleController.updateRoles);

  return authRoutes;
};
