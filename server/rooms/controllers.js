const co = require('co');
const mongoose = require('mongoose');
const Room = require('./model');


const toMongoId = (id) => mongoose.Types.ObjectId(id);


exports.getRoomList = (req, res, next) => {
  co(function* () {
    try {
      const rooms = yield Room.find(req.body.query).sort('name').exec();
      console.log(rooms)
      res.json(rooms);
    } catch (e) {
      next(e);
    }
  });
};


exports.getRoom = (req, res, next) => {
  co(function* () {
    try {
      const room = yield Room.findById(req.params.id).exec();
      res.json(room);
    } catch (e) {
      next(e);
    }
  });
};


exports.createRoom = (req, res, next) => {
  co(function* () {
    try {
      const medService = new Room(req.body);
      yield medService.save();
      res
        .status(201)
        .json(medService);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateRoom = (req, res, next) => {
  co(function* () {
    try {
      const room = yield Room.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });
      res.json(room);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteRoom = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield Room.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};
