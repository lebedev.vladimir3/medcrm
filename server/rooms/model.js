const mongoose = require('mongoose');


const RoomSchema = new mongoose.Schema({
  number: { type: String, unique: true, required: true },
  name: { type: String, required: true, unique: true },
  description: { type: String },
  capacity: { type: Number, default: 1, min: 1, max: 10 },
  isDeleted: {type: Boolean, default: false}
}, { timestamps: true, virtuals: true });

module.exports = mongoose.model('Room', RoomSchema);
