const Router = require('express').Router;
const roomsController = require('./controllers');

const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const roomsRoutes = new Router();

  roomsRoutes
    .get('/', roomsController.getRoomList)
    .post('/', user.is(roles.ADMIN), roomsController.createRoom)
    .get('/:id', roomsController.getRoom)
    .put('/:id', user.is(roles.ADMIN), roomsController.updateRoom)
    .delete('/:id', user.is(roles.ADMIN), roomsController.deleteRoom);

  return roomsRoutes;
};
