const Router = require('express').Router;
const authRoutes = require('./auth/routes');
const userRoutes = require('./users/routes');
const companyRoutes = require('./company/routes');
const appointmentsRoutes = require('./appointments/routes');
const patientsRoutes = require('./patients/routes');
const rolesRoutes = require('./roles/routes');
const equipmentRoutes = require('./equipment/routes');
const roomsRoutes = require('./rooms/routes');
const medserviceRoutes = require('./medServices/routes');
const categoryRoutes = require('./medServiceCategories/routes');
const payerRoutes = require('./payers/routes');
const prescriptionsRoutes = require('./prescriptions/routes');
const generalSchedulePatternRoutes = require('./schedulePatterns/routes');
const generalWorkingHourRoutes = require('./openingHours/routes');
const passportConfig = require('./config/passport');
const DbLogger = require('./logging/core/dbLogger').middleware;


module.exports = (app) => {
  const rootRoutes = new Router();

  rootRoutes
    .use(DbLogger)
    .use('/api/auth', authRoutes(app))
    .use('/api/users', passportConfig.isAuthenticated, userRoutes(app))
    .use('/api/roles', passportConfig.isAuthenticated, rolesRoutes(app))
    .use('/api/company', passportConfig.isAuthenticated, companyRoutes(app))
    .use('/api/appointments', passportConfig.isAuthenticated, appointmentsRoutes(app))
    .use('/api/patients', passportConfig.isAuthenticated, patientsRoutes(app))
    .use('/api/equipment', passportConfig.isAuthenticated, equipmentRoutes(app))
    .use('/api/rooms', passportConfig.isAuthenticated, roomsRoutes(app))
    .use('/api/med-services', passportConfig.isAuthenticated, medserviceRoutes(app))
    .use('/api/med-services/categories', passportConfig.isAuthenticated, categoryRoutes(app))
    .use('/api/payers', passportConfig.isAuthenticated, payerRoutes(app))
    .use('/api/prescriptions', passportConfig.isAuthenticated, prescriptionsRoutes(app))
    .use('/api/schedule-patterns', passportConfig.isAuthenticated, generalSchedulePatternRoutes(app))
    .use('/api/working-hours', passportConfig.isAuthenticated, generalWorkingHourRoutes(app))
  ;

  return rootRoutes;
};
