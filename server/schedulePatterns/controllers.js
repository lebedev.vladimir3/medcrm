const co = require('co');
const mongoose = require('mongoose');
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const GeneralSchedulePattern = require('./model');
const Appointment = require('../appointments/model');
const calculateWorkingHours = require('../appointments/utils/workingHours').calculateWorkingHours;
const utils = require('./utils');
const toMongoId = (id) => mongoose.Types.ObjectId(id);


exports.getSchedulePatternList = (req, res, next) => {
  co(function*() {
    try {
      const pattern = yield GeneralSchedulePattern.find(req.body.query).lean().exec();
      res.status(200).json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.getSchedulePattern = (req, res, next) => {
  co(function*() {
    try {
      const pattern = yield GeneralSchedulePattern.findById(req.params.id).lean().exec();
      res.status(200).json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.createSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      if (!req.body.force) {
        const [appointmentsGroupedByDay, patterns] = yield [
          Appointment.aggregate([
            { $match: { date: { $gte: new Date() } } },
            { $group: {
              _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' } },
              appointments: { $push: '$$ROOT' },
            } },
          ]).exec(),
          GeneralSchedulePattern.find({}).lean().exec(),
        ];

        const patternsWithUpdate = [...patterns, req.body];

        for (let i = 0, l1 = appointmentsGroupedByDay.length; i < l1; i++) {
          const group = appointmentsGroupedByDay[i];
          const workingHours = calculateWorkingHours(moment(group._id), patternsWithUpdate);

          const workingHoursRanges = workingHours.map((item) => {
            const dFrom = moment.duration(item.from, 'minutes');
            const dTo = moment.duration(item.to, 'minutes');
            return moment.range(
              moment(group._id).hours(dFrom.hours()).minutes(dFrom.minutes()),
              moment(group._id).hours(dTo.hours()).minutes(dTo.minutes())
            );
          });
          if (!utils.areAppointmentsInWorkingHours(group.appointments, workingHoursRanges)) {
            res.json({ overbooking: true });
            return;
          }
        }
      }
      const pattern = new GeneralSchedulePattern(req.body);
      yield pattern.save();
      res.status(201).json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      if (!req.body.force) {
        const [appointmentsGroupedByDay, patterns] = yield [
          Appointment.aggregate([
            { $match: { date: { $gte: new Date() } } },
            { $group: {
              _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' } },
              appointments: { $push: '$$ROOT' },
            } },
          ]).exec(),
          GeneralSchedulePattern.find({ _id: { $ne: req.params.id } }).lean().exec(),
        ];

        const patternsWithUpdate = [...patterns, req.body];

        for (let i = 0, l1 = appointmentsGroupedByDay.length; i < l1; i++) {
          const group = appointmentsGroupedByDay[i];
          const workingHours = calculateWorkingHours(moment(group._id), patternsWithUpdate);

          const workingHoursRanges = workingHours.map((item) => {
            const dFrom = moment.duration(item.from, 'minutes');
            const dTo = moment.duration(item.to, 'minutes');
            return moment.range(
              moment(group._id).hours(dFrom.hours()).minutes(dFrom.minutes()),
              moment(group._id).hours(dTo.hours()).minutes(dTo.minutes())
            );
          });

          if (!utils.areAppointmentsInWorkingHours(group.appointments, workingHoursRanges)) {
            res.json({ overbooking: true });
            return;
          }
        }
      }

      const pattern = yield GeneralSchedulePattern.findByIdAndUpdate(
        req.params.id,
        { $set: req.body },
        { new: true }
      );
      res.json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield GeneralSchedulePattern.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};
