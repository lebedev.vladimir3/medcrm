const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const SchedulePatternSchema = new mongoose.Schema({
  id: { type: Number },
  type: {
    type: String,
    enum: [
      'WORKING_HOURS',
      'NON_WORKING_HOURS',
      'PUBLIC_HOLIDAY',
    ],
  },
  title: String,
  startDate: Date,
  endDate: Date,
  hasEndDate: Boolean,
  allDay: Boolean,
  singleDay: Boolean,
  timeFrom: Number,
  timeTo: Number,
  repeated: Boolean,
  isDeleted: {type: Boolean, default: false},
  // 7 elements in array are 7 week days
  repeatOn: [
    // 0
    { type: Boolean, default: false },
    // 1
    { type: Boolean, default: false },
    // 2
    { type: Boolean, default: false },
    // 3
    { type: Boolean, default: false },
    // 4
    { type: Boolean, default: false },
    // 5
    { type: Boolean, default: false },
    // 6
    { type: Boolean, default: false },
  ],
  frequency: {
    type: Number,
    default: 1,
    min: 1,
    max: 12,
  },
}, { timestamps: true, virtuals: true });


autoIncrement.initialize(mongoose.connection);
SchedulePatternSchema.plugin(autoIncrement.plugin, { model: 'GeneralSchedulePattern', field: 'id', startAt: 1 });

const GeneralSchedulePattern = mongoose.model('GeneralSchedulePattern', SchedulePatternSchema);

module.exports = GeneralSchedulePattern;
