const Router = require('express').Router;
const controller = require('./controllers');
const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;

module.exports = () => {
  const patientsRoutes = new Router();

  patientsRoutes
    .get('/', controller.getSchedulePatternList)
    .post('/', user.is(roles.ADMIN), controller.createSchedulePattern)
    .get('/:id', controller.getSchedulePattern)
    .put('/:id', user.is(roles.ADMIN), controller.updateSchedulePattern)
    .delete('/:id', user.is(roles.ADMIN), controller.deleteSchedulePattern);

  return patientsRoutes;
};
