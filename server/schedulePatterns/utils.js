const moment = require('moment');


exports.areAppointmentsInWorkingHours = (appointments, workingHoursRanges) => (
  appointments.every((appointment) => {
    for (let k = 0, l2 = workingHoursRanges.length; k < l2; k++) {
      const appointmentStart = moment(appointment.date);
      const appointmentEnd = moment(appointment.date).add(appointment.duration, 'minutes');

      const startIsInWorkingRange = workingHoursRanges[k].contains(appointmentStart);
      const endIsInWorkingRange = workingHoursRanges[k].contains(appointmentEnd);

      if (startIsInWorkingRange && endIsInWorkingRange) {
        return true;
      }
    }
    return false;
  })
);
