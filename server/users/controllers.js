const co = require('co');
const moment = require('moment');
const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const User = require('./model');
const Service = require('../medServices/model');
const logger = require('../logger');
const Appointment = require('../appointments/model');
const calculateWorkingHours = require('../appointments/utils/workingHours').calculateWorkingHours;
const calculateSummaryMinutes = require('../appointments/utils/workingHours').calculateSummaryMinutes;
const utils = require('../schedulePatterns/utils');


const toMongoId = (id) => mongoose.Types.ObjectId(id);


exports.getUserList = (req, res, next) => {

  co(function* () {
    try {
      const users = yield User.find(req.body.query).sort('lastName').populate('role').exec();

      res.json(users);
    } catch (e) {
      next(e);
    }
  });
};


exports.createUser = (req, res, next) => {
  co(function* () {
    try {
      const user = new User(req.body);
      yield user.save();
      res.status(201).json(user);
    } catch (e) {
      next(e);
    }
  });
};


exports.getUser = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      const user = yield User.findById(id).populate('role').exec();

      const services = yield Service
        .find({
          $or: [
            { 'staffAllocation.staff': id },
            { 'staffAllocation.role': user.role._id, 'staffAllocation.isAnyStaff': true },
          ],
        })
        .lean()
        .distinct('_id')
        .exec();

      res.json(Object.assign({}, user.toObject(), { services }));
    } catch (e) {
      next(e);
    }
  });
};


exports.updateUser = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);

      const userPrev = yield User.findById(req.params.id).select('+password').lean().exec();
      if (!req.body.password || bcrypt.compareSync(req.body.password, userPrev.password) || userPrev.password === req.body.password) {
        req.body.password = userPrev.password;
      } else {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.password, salt);
        req.body.password = hash;
      }

      const user = yield User.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });
      const serviceIds = req.body.services.map((item) => toMongoId(item));

      Service.update(
        {
          _id: { $in: serviceIds },
          // Update only if there is no subdoc with staff === id
          'staffAllocation.staff': { $ne: id },
        },
        { $addToSet: { staffAllocation: { staff: id } } },
        { multi: true },
        (e, d) => { logger.debug(e, d); });

      Service.update(
        { _id: { $nin: serviceIds } },
        { $pullAll: { 'staffAllocation.$.staff': [id] } },
        { multi: true },
        (e, d) => { logger.debug(e, d); });

      res.json(user);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteUser = (req, res, next) => {
  co(function* () {
    try {
      const id = toMongoId(req.params.id);
      yield User.findByIdAndUpdate(req.params.id, {$set: {isDeleted: true}});

      Service.update(
        {},
        { $pullAll: { staffAllocation: [id] } },
        { multi: true });

      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};


// Controllers for user`s schedule
exports.createUserSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const { id } = req.params;
      if (!req.body.force) {
        const [appointmentsGroupedByDay, users] = yield [
          Appointment.aggregate([
            { $match: {
              staff: toMongoId(req.params.id),
              date: { $gte: new Date() },
            } },
            { $group: {
              _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' } },
              appointments: { $push: '$$ROOT' },
            } },
          ]).exec(),
          User.aggregate([
            { $match: { _id: toMongoId(req.params.id) } },
            { $unwind: '$schedulePatterns' },
            { $group: { _id: '$_id', schedulePatterns: { $push: '$schedulePatterns' } } },
          ]).exec(),
        ];
        const patterns = users && users.length > 0 ? users[0].schedulePatterns : [];
        const patternsWithUpdate = [...patterns, req.body];

        for (let i = 0, l1 = appointmentsGroupedByDay.length; i < l1; i++) {
          const group = appointmentsGroupedByDay[i];
          const workingHours = calculateWorkingHours(moment(group._id), patternsWithUpdate);

          const workingHoursRanges = workingHours.map((item) => {
            const dFrom = moment.duration(item.from, 'minutes');
            const dTo = moment.duration(item.to, 'minutes');
            return moment.range(
              moment(group._id).hours(dFrom.hours()).minutes(dFrom.minutes()),
              moment(group._id).hours(dTo.hours()).minutes(dTo.minutes())
            );
          });

          if (!utils.areAppointmentsInWorkingHours(group.appointments, workingHoursRanges)) {
            res.json({ overbooking: true });
            return;
          }
        }
      }

      const user = yield User.findByIdAndUpdate(
        id,
        { $push: { schedulePatterns: req.body } },
        { new: true }
      );
      res.status(201).json(user);
    } catch (e) {
      next(e);
    }
  });
};


exports.getUserSchedulePatternList = (req, res, next) => {
  co(function* () {
    try {
      const user = yield User.findById(req.params.id).lean().exec();
      res.status(200).json(user.schedulePatterns);
    } catch (e) {
      next(e);
    }
  });
};


exports.getUserSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      const user = yield User.findById(req.params.id).exec();
      const pattern = user.schedulePatterns.id(req.params.patternId);
      res.status(200).json(pattern);
    } catch (e) {
      next(e);
    }
  });
};


exports.updateUserSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      if (!req.body.force) {
        const [appointmentsGroupedByDay, users] = yield [
          Appointment.aggregate([
            { $match: {
              staff: toMongoId(req.params.id),
              date: { $gte: new Date() },
            } },
            { $group: {
              _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' } },
              appointments: { $push: '$$ROOT' },
            } },
          ]).exec(),
          User.aggregate([
            { $match: { _id: toMongoId(req.params.id) } },
            { $unwind: '$schedulePatterns' },
            { $match: { 'schedulePatterns._id': toMongoId(req.params.patternId) } },
            { $group: { _id: '$_id', schedulePatterns: { $push: '$schedulePatterns' } } },
          ]).exec(),
        ];

        const patterns = users[0].schedulePatterns;
        const patternsWithUpdate = [...patterns, req.body];

        for (let i = 0, l1 = appointmentsGroupedByDay.length; i < l1; i++) {
          const group = appointmentsGroupedByDay[i];
          const workingHours = calculateWorkingHours(moment(group._id), patternsWithUpdate);

          const workingHoursRanges = workingHours.map((item) => {
            const dFrom = moment.duration(item.from, 'minutes');
            const dTo = moment.duration(item.to, 'minutes');
            return moment.range(
              moment(group._id).hours(dFrom.hours()).minutes(dFrom.minutes()),
              moment(group._id).hours(dTo.hours()).minutes(dTo.minutes())
            );
          });

          if (!utils.areAppointmentsInWorkingHours(group.appointments, workingHoursRanges)) {
            res.json({ overbooking: true });
            return;
          }
        }
      }

      const updatedUser = yield User.update(
        { _id: req.params.id, 'schedulePatterns._id': req.params.patternId },
        { $set: { 'schedulePatterns.$': req.body } },
        { new: true }
      ).lean().exec();

      console.log('UUU', updatedUser);

      res.status(200).json(updatedUser);
    } catch (e) {
      next(e);
    }
  });
};


exports.deleteUserSchedulePattern = (req, res, next) => {
  co(function* () {
    try {
      yield User.findByIdAndUpdate(
        req.params.id,
        { $pull: { schedulePatterns: { _id: req.params.patternId } } }
      ).exec();
      res.sendStatus(204);
    } catch (e) {
      next(e);
    }
  });
};


exports.getWorkingHours = (req, res, next) => {
  co(function* () {
    try {
      const { dateFrom, dateTo } = req.query;
      const user = yield User.findById(req.params.id).lean().exec();
      const days = [];

      for (let iterateDate = moment(dateFrom); iterateDate.isSameOrBefore(moment(dateTo)); iterateDate.add(1, 'days')) {
        const workingHours = calculateWorkingHours(iterateDate, user.schedulePatterns);
        days.push({
          workingHours,
          summaryMinutes: calculateSummaryMinutes(workingHours),
          date: moment(iterateDate),
        });
      }

      res.status(200).json(days);
    } catch (e) {
      next(e);
    }
  });
};
