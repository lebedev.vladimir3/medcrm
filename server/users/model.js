const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const generatePassword = require('password-generator');
const sendMail = require('../utils/mailgun');
const autoIncrement = require('mongoose-auto-increment');

const options = { timestamps: true, virtuals: true };


const userSchema = new mongoose.Schema({
  id: { type: Number },
  password: { type: String, select: false },
  passwordResetToken: String,
  passwordResetExpires: Date,
  title: {
    personal: { type: String, enum: ['mr', 'mrs', 'ms'], required: true },
    honorific: String,
  },
  firstName: String,
  lastName: String,
  isDeleted: {type: Boolean, default: false},
  birth: {
    dd: String,
    mm: String,
    yyyy: String,
  },
  email: { type: String, unique: true },
  personalEmail: String,
  phone: {
    work: String,
    personal: String,
  },
  address: {
    street: String,
    post: String,
    city: String,
    region: String,
    country: String,
  },
  notes: String,
  role: {
    type: String,
    ref: 'Role',
    required: true,
  },
  // For Lean
  // services: [{
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'MedService',
  // }],
  speciality: String,
  // Outer array items are week days from 1 to 7 as Mongo's $dayOfWeek returns
  // Inner items are work periods during day (Because staff could have pauses)
  schedulePatterns: [{
    type: {
      type: String,
      enum: [
        'WORKING_HOURS',
        'NON_WORKING_HOURS',
        'ABSENSE',
      ],
    },
    title: String,
    startDate: Date,
    endDate: Date,
    hasEndDate: Boolean,
    allDay: Boolean,
    timeFrom: Number,
    timeTo: Number,
    repeated: Boolean,
    singleDay: Boolean,
    // 7 elements in array are 7 week days
    repeatOn: [
      // 0
      { type: Boolean },
      // 1
      { type: Boolean },
      // 2
      { type: Boolean },
      // 3
      { type: Boolean },
      // 4
      { type: Boolean },
      // 5
      { type: Boolean },
      // 6
      { type: Boolean },
    ],
    frequency: {
      type: Number,
      default: 1,
      min: 1,
      max: 12,
    },
  }],
  createdAt: { type: Date, required: true, default: Date.now },
}, options);


userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};


userSchema.pre('save', function save(next) {
  const user = this;
  const isAdmin = user.email === 'admin';

  User.find({ email: user.email }, (err, docs) => {
    if (!docs.length) {
      bcrypt.genSalt(10, (err1, salt) => {
        if (err1) {
          next(err1);
        } else {
          const password = isAdmin ? user.password : generatePassword();

          bcrypt.hash(password, salt, null, (err2, hash) => {
            if (err2) { return next(err2); }

            user.password = hash;

            if (!isAdmin) {
              sendMail({
                from: 'Admin <postmaster@mg.medcrmdev.com>',
                to: user.email,
                subject: 'Your password',
                text: `Your email: ${user.email}; Your password: ${password}`,
              });
            }

            return next();
          });
        }
      });
    } else {
      console.log('user exists: ', user.name);
      next(new Error('User exists!'));
    }
  });
});


autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'User', field: 'id', startAt: 1 });

const User = mongoose.model('User', userSchema);

module.exports = User;

