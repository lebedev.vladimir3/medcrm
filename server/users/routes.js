const Router = require('express').Router;
const userController = require('./controllers');
const authorization = require('../utils/authorization/authorization');
const user = authorization.user;
const roles = authorization.roles;
const actions = authorization.actions;

module.exports = () => {
  const authRoutes = new Router();

  authRoutes
    .get('/', userController.getUserList)
    .post('/', user.is(roles.ADMIN), userController.createUser)
    .get('/:id', userController.getUser)
    .put('/:id', user.is(roles.ADMIN), userController.updateUser)
    .delete('/:id', user.is(roles.ADMIN), userController.deleteUser)
    .post('/:id/schedule-patterns', user.can(actions.CAN_CHANGE_OWN_SCHEDULE), userController.createUserSchedulePattern)
    .get('/:id/schedule-patterns', userController.getUserSchedulePatternList)
    .get('/:id/schedule-patterns/:patternId', userController.getUserSchedulePattern)
    .put('/:id/schedule-patterns/:patternId', user.can(actions.CAN_CHANGE_OWN_SCHEDULE), userController.updateUserSchedulePattern)
    .delete('/:id/schedule-patterns/:patternId', user.can(actions.CAN_CHANGE_OWN_SCHEDULE), userController.deleteUserSchedulePattern)
    .get('/:id/working-hours', userController.getWorkingHours);


  return authRoutes;
};
