const mongoose = require('mongoose');


const LoggingSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  actions: [{
    data: {
      type: Object,
    },
    method: String,
    objectModel: String,
  }],
}, { timestamps: true });

const UsersLoggingSchema = mongoose.model('UsersLogging', LoggingSchema);

module.exports = UsersLoggingSchema;

