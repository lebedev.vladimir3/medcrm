const ConnectRoles = require('connect-roles');
const roles = require('./roles');
const actions = require('./actions');

const user = new ConnectRoles({
  async: true,
  failureHandler: function (req, res, action) {
    //  optional function to customise code that runs when
    //  user fails authorisation
    console.log(action);
    const accept = req.headers.accept || '';
    res.status(403);
    if (~accept.indexOf('html')) {
      res.render('access-denied', { action: action });
    } else {
      res.send('Access Denied - You don\'t have permission to: ' + action);
    }
  }
});

// --------START--------ROLES--------
user.use(roles.ADMIN, function (req) {
  return req.user.role.name === roles.ADMIN;
});

// doctor access
user.use(roles.DOCTOR, function (req) {
  return req.user.role.name === roles.DOCTOR;
});

// --------END--------ROLES--------

// --------START--------ACTIONS--------
user.use(actions.CAN_CHANGE_APPOINTMENTS, function (req) {
  return req.user.role.name === roles.DOCTOR || req.user.role.name === roles.ADMIN;
});
user.use(actions.CAN_CHANGE_PATIENTS, function (req) {
  return req.user.role.name === roles.DOCTOR || req.user.role.name === roles.ADMIN;
});
user.use(actions.CAN_CHANGE_PRESCRIPTIONS, function (req) {
  return req.user.role.name === roles.DOCTOR || req.user.role.name === roles.ADMIN;
});
user.use(actions.CAN_CHANGE_OWN_SCHEDULE, function (req) {
  return req.user.role.canSetOwnSchedule || req.user.role.name === roles.ADMIN;
});
// --------END--------ACTIONS--------

exports.user = user;
exports.roles = roles;
exports.actions = actions;
