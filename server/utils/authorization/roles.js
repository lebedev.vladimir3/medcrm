const roles = {
  ADMIN: 'Admin',
  DOCTOR: 'Doctor',
};

module.exports = roles;
