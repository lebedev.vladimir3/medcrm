require('dotenv').config();
const mongoose = require('mongoose');
const Role = require('../roles/model');
const User = require('../users/model');
const logger = require('../logger');
const co = require('co');
const cnf = require('../config/local-config');

// Example:npm run inidb --admin_password 555555
mongoose.Promise = global.Promise;
mongoose.connect(cnf.db.mongo_uri);
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

(() => {
  co(function*() {
    try {
      const roles = yield [
        Role.create({ _id: 'ADMIN', name: 'Admin' }),
        Role.create({ _id: 'DOCTOR', name: 'Doctor' }),
      ];

      console.log(roles);

      const user = yield User.create({
        email: 'admin',
        password: process.env.npm_config_admin_password || '111222333',
        role: roles[0]._id,
        title: { personal: 'mr' },
      });

      console.log(user);
      console.log('Success! Initial database records have been created.');
    } catch (e) {
      logger.error(e);
    }

    process.exit();
  });
})();










