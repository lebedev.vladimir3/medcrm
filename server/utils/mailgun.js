const cnf = require('../config/local-config');
const mailgun = require('mailgun-js')({ apiKey: cnf.mk.apk, domain: cnf.mk.dom });
const logger = require('../logger');


module.exports = function sendMail(data) {
  mailgun.messages().send(data, (error, body) => {
    logger.error(error);
    logger.info(body);
  });
};
