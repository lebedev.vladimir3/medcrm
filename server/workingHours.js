const co = require('co');
const moment = require('moment');
const GeneralSchedulePattern = require('../models/GeneralSchedulePattern');
const calculateWorkingHours = require('../utils/appointments/workingHours').calculateWorkingHours;


exports.getWorkingHours = (req, res, next) => {
  co(function* () {
    try {
      const { dateFrom, dateTo } = req.query;
      const patterns = yield GeneralSchedulePattern.find().lean().exec();
      const days = [];

      for (let m = moment(dateFrom); m.isSameOrBefore(moment(dateTo)); m.add(1, 'days')) {
        const workingHours = calculateWorkingHours(m, patterns);

        days.push({
          workingHours,
          date: moment(m),
        });
      }

      res.status(200).json(days);
    } catch (e) {
      next(e);
    }
  });
};
